----------------------------------------------------------------------------
-- "functional" block
----------------------------------------------------------------------------


----------------------------------------------------------------------------
-- "define" block
----------------------------------------------------------------------------


SLASH_ACRC1 = "/acr";
SLASH_FCSC1 = "/fcs";
SLASH_GUIC1 = "/gui";
SLASH_SNDC1 = "/snd";

GGM.IOC = {};
GGD.IOC = {};
GGD.IOC.ClusterName = "Input/Output Controller";
GGD.IOC.ClusterToken = "IOC";
GGD.IOC.ClusterVersion = GGD.BuildMeta.Version;
GGD.IOC.ClusterBuild = "AT-"..GGD.BuildMeta.Build;
GGD.IOC.ClusterMBI = "7102-7090";