-- Инициализация на стадии загрузки аддона
function GGM.IOC:OnAddonLoad()
	GGM.COM:RegisterInterComm(GGD.IOC.ClusterToken, GGM.IOC.EvokeController);

	GGM.IOC.OutputController:AddonWelcomeMessage({
		cluster = GGD.IOC.ClusterToken,
		version = GGD.IOC.ClusterVersion,
		build = GGD.IOC.ClusterBuild,
		mbi = GGD.IOC.ClusterMBI,
	});
end


SlashCmdList.ACRC = function(msg, self)
	GGM.IOC.InputController:SlashHandler("ACR", msg);
end
SlashCmdList.FCSC = function(msg, self)
	GGM.IOC.InputController:SlashHandler("FCS", msg);
end
SlashCmdList.GUIC = function(msg, self)
	GGM.IOC.InputController:SlashHandler("GUI", msg);
end
SlashCmdList.SNDC = function(msg, self)
	GGM.IOC.InputController:SlashHandler("SND", msg);
end