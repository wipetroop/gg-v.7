local This = GGF.ModuleGetWrapper("IOC-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- Вывод приветственного сообщение аддона(тех.) в консоль
function This:AddonWelcomeMessage(in_data) 
	self:addonWelcomeMessage(in_data);
end


-- Сырой вывод сообщения в консоль
function This:RawPrint(in_msg) 
	self:rawPrint(in_msg);
end


-- 
function This:CommonPrint(in_module, ...)
	self:commonPrint(in_module, ...);
end


-- 
function This:DebugPrint(in_clustertoken, in_moduleToken, in_debugstack, in_dLevel, in_msg) 
	self:debugPrint(in_clustertoken, in_moduleToken, in_debugstack, in_dLevel, in_msg);
end


-- 
function This:DebugCanvasPrint(...)
	self:debugCanvasPrint(...);
end


-- Вывод сообщения об ошибке(стандарт де-факто для дебага)
function This:DumpError(in_module, in_code, in_argline)
	self:dumpError(in_module, in_code, in_argline);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------