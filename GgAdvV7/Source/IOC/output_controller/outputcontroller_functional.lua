local This = GGF.ModuleGetWrapper("IOC-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Вывод приветственного сообщение аддона(тех.) в консоль
function This:addonWelcomeMessage(in_data)
	print(date("%d/%m/%y %H:%M:%S") .. " | "
	   .. in_data.cluster
	   .. " v" .. in_data.version
	   .. " #" .. in_data.build
	   .. " MBI:" .. in_data.mbi
	   .. " loaded");
end


-- api
-- Сырой вывод сообщения в консоль
function This:rawPrint(in_msg)
	print(in_msg);
end


-- api
-- Стандартный цветной вывод в консоль
function This:commonPrint(in_module, ...)
	local mdMeta = GGF.MDL.GetModuleMeta(in_module);
	print(GGF.StructColorToStringColor(GGD.Color.Normal) .. "---" .. GGF.StructColorToStringColor(GGD.Color.Warning) .. "---" .. GGF.StructColorToStringColor(GGD.Color.Critical) .. "---" .. GGF.FlushStringColor() .. "Console System Message" .. GGF.StructColorToStringColor(GGD.Color.Critical) .. "---" .. GGF.StructColorToStringColor(GGD.Color.Warning) .. "---" .. GGF.StructColorToStringColor(GGD.Color.Normal) .. "---" .. GGF.FlushStringColor());
	print("Module: " .. GGF.StructColorToStringColor(GGD.Color.Title) .. (mdMeta.name or "nil") .. GGF.FlushStringColor() .. " | Token: " .. GGF.StructColorToStringColor(GGD.Color.Title) .. (mdMeta.token or "nil") .. GGF.FlushStringColor() .. " | Cluster: " .. GGF.StructColorToStringColor(GGD.Color.Title) .. (mdMeta.cluster or "nil") .. GGF.FlushStringColor());
	for _,v in ipairs{...} do
		print(v);
	end
	print(GGF.StructColorToStringColor(GGD.Color.Critical) .. "---" .. GGF.StructColorToStringColor(GGD.Color.Warning) .. "---" .. GGF.StructColorToStringColor(GGD.Color.Normal) .. "---" .. GGF.FlushStringColor() .. "Console System Message" .. GGF.StructColorToStringColor(GGD.Color.Normal) .. "---" .. GGF.StructColorToStringColor(GGD.Color.Warning) .. "---" .. GGF.StructColorToStringColor(GGD.Color.Critical) .. "---" .. GGF.FlushStringColor());
end


-- api
-- Дебаг трейс с учетом линии, файла и уровня дебага
function This:debugPrint(in_clustertoken, in_moduleToken, in_debugstack, in_dLevel, in_msg)
	if (not GGM.FCS.RuntimeStorage:GetDebugModeFlag()) then
		return;
	end

	-- Чек уровня дебага dLevel
	if (GGM.FCS.RuntimeStorage:GetDebugLevel() < in_dLevel) then
		return;
	end

	local file, line, func = self:parseDebugStack(in_debugstack);
	-- TODO: чек на пустые значения(GG-57)

	String = " Dbg | Lv: " .. self.cs_debugLevels[in_dLevel] .. " | Fl: " .. file .. " | Fn: " .. func .. " | Ln: " .. line .. " | ";
	self:rawPrint(String);
	self:rawPrint(in_msg);
end


-- api
-- Вывод рандомного полотна текста в дебаге
function This:debugCanvasPrint(...)
	if (GGM.FCS.RuntimeStorage:GetDebugModeFlag()) then
		return
	end

	-- Чек уровня дебага dLevel
	if (GGM.FCS.RuntimeStorage:GetDebugLevel() < in_dLevel) then
		return;
	end
	local result = ""
	print("GG System Debugging Dump :")
	local arg = {...}
	for i,v in pairs(arg) do
		result = result .. "arg " .. tostring(i) .. " - " ..tostring(v).."\n"
	end
	print(result)
end


-- api
-- Вывод сообщения об ошибке
-- Используется только error_handler из GgAdvFCS(который уже убран)
function This:dumpError(in_module, in_code, in_argline)
	--local trl = ACR.colorCodes.CDSL[code];
	--print("acr CDSL / level : "..trl.."### |r/ ", ...);
	-- TODO: переделать...(убрать)(GG-3)
	--print(COM.errlist[in_module][tostring(in_code)]);
	--print(argline);
end


-- Сброс данных в чат
--function GGM.ACR.RuntimeInterceptor.chatMasterDump(ctype, msgs)
	--local rt = ""
	--local i = 1
	--for k,v in pairs(msgs) do
		--rt = v
		--for k,v in pairs(ACR.colorCodes.chat) do
			--rt = rt:gsub(k,"")
		--end
		--SendChatMessage("dhmt"..i.." "..rt, ctype, nil, 1)
		--i = i + 1
	--end		
--end


-- По сути просто набор регексов
function This:parseDebugStack(in_debugstack)
	-- NOTE: У близов тут с патча 1.9 функция в debugstack обрамлена разными кавычками(`' вместо ''); вот так и живем...
	return string.match(in_debugstack, "/([%w_]+).lua") .. ".lua", string.match(in_debugstack, ":(%d+):"), string.match(in_debugstack, "`([%l%u]+)'");
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------