local This = GGF.ModuleGetWrapper("IOC-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.1 (Miscellaneous Variables)
function This:MPRC_MVR_INITIALIZE()
	self.fn_slashCommandList = {
		ACR = {	-- GgAdvAnnouncer
			-- combat_logger
			dump = {
				runnable = function(in_aa) ACR_CL:DumpSlashCommand(in_aa) end,
				argpattern = "",
				description = "",
			},
			logtest = {
				runnable = function(in_aa) ACR_CL:FictiveLogSlashCommand(in_aa) end,
				argpattern = "",
				description = "",
			},
			info = {
				runnable = function(in_aa) ACR_CL:InfoSlashCommand(in_aa) end,
				argpattern = "",
				description = "",
			},

			-- runtime_interceptor
			kha_test = {
				runnable = function(in_aa) GGM.ACR.RuntimeInterceptor:KillHandlerAnnouncerTestSlashCommand(in_aa) end,
				argpattern = "",
				description = "",
			},
			temptest = {
				runnable = function(in_aa) GGM.ACR.CombatInterceptor:Temptest(in_aa) end,
				argpattern = "",
				description = "",
			},
			frcbgs = {
				runnable = function(in_aa) GGM.ACR.CombatInterceptor:Bgstarter(in_aa) end,
				argpattern = "",
				description = "",
			},
			frcbge = {
				runnable = function(in_aa) GGM.ACR.CombatInterceptor:Bgender(in_aa) end,
				argpattern = "",
				description = "",
			},
			dmp_test = {
				runnable = function(in_aa) GGM.ACR.RuntimeInterceptor:DumpTestSlashCommand(in_aa) end,
				argpattern = "",
				description = "",
			},

			-- kill_saa
			dth = {
				runnable = function(in_aa) ACR_KS:HeightChangeSlashCommand(in_aa) end,
				argpattern = "",
				description = "",
			},
		},
		FCS = {	-- GgAdvFCS
			-- runtime storage
			maint = {
				runnable = function(in_aa) GGM.FCS.RuntimeStorage:ProceedMaintSlashCommand(in_aa) end,
				argpattern = "",
				description = "",
			},
			debug = {
				runnable = function(in_aa) GGM.FCS.RuntimeStorage:DebugModeSlashCommand(in_aa) end,
				argpattern = "",
				description = "",
			},
			release = {
				runnable = function(in_aa) GGM.FCS.RuntimeStorage:ReleaseModeSlashCommand(in_aa) end,
				argpattern = "",
				description = "",
			},
		},
		IOC = {
			info = {
				runnable = function(in_aa) self:infoSlashCommand(in_aa) end,
				argpattern = "",
				description = "",
			},
		},
		GUI = {	-- GgAdvGUi
			fastview = {
				runnable = function(in_aa)
					-- TODO: найти, куда это было раньше прицеплено(GG-61)
					-- GUi.GGM.GUI.DataServiceSlashCommand(in_aa)
				end,
				argpattern = "",
				description = "",
			},
		},
		SND = {	-- GgAdvSound
			-- sound conroller
			dropstat = {
				runnable = function(in_aa) GGM.SND.SoundController:DropStatisticSlashCommand(in_aa) end,
				argpattern = "",
				description = "",
			},
			dumpstat = {
				runnable = function(in_aa) GGM.SND.SoundController:DumpStatisticSlashCommand(in_aa) end,
				argpattern = "",
				description = "",
			},
			savestat = {
				runnable = function(in_aa) GGM.SND.SoundController:SaveStatisticSlashCommand(in_aa) end,
				argpattern = "",
				description = "",
			},
		},
	};
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------