local This = GGF.ModuleGetWrapper("IOC-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Обработчик слэш-команд
-- TODO: реализовать заготовку из даты(GG-61)
function This:slashHandler(in_addon, in_argline)
	local dspec = {};
	i = 1;
	for string in in_argline:gmatch("%S+") do
		dspec[i] = string;
		i = i + 1;
	end
	
	-- TODO: разобраться с этим куском кода(GG-61)
	-- какой-то старый дебаг
	--print(#COM["ACR"].slashCommandList);
	--print(#COM["SND"].slashCommandList);
	--print(#COM["FCS"].slashCommandList);
	--print(#COM["GUi"].slashCommandList);
	--for k,v in pairs(COM["ACR"].slashCommandList) do
		--print(k, v);
	--end

	local commandMetadata = self.fn_slashCommandList[in_addon][dspec[1]];

	if (commandMetadata ~= nil) then
		local runnableFunc = commandMetadata.runnable;
		runnableFunc(dspec);
	else
		GGM.FCS.ErrorHandler:DumpError(GGD.IOC.ClusterToken, 100, in_addon .. " " .. in_argline);
	end
end


-- CC: /ioc info
-- Вывод хелпа
function This:infoSlashCommand(in_dspec)
	print("this part of addon is temporarily unavailable")
	--print("/acr maint - maintenance information(Service, triggers, etc.)")
	--print("/acr debug - switches addon to debug mode")
	--print("/acr release - switches addon to Normal mode")
	--print("/acr kha_test - test all type of kills till 1 to 11")
	--print("/acr frcbgs - force battleground start(in case of stucked handler)")
	--print("/acr frcbge - force battleground end(in case of stucked handler)")
	--print("/acr dmp_test - creates test data in logCluster, that can be dumped")
	--print("/acr dump [c,g,i,p,ra,rw,s,w,y] [a,d,u] [k,s,m] [al,ab,b,de,do,e,i,m,p,rf,rs] - force master table dump to choosen chat")
end



-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------