local This = GGF.ModuleGetWrapper("FCS-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- api
--
function This:setEnabledFlag(in_enb)
	self.vs_severeStorage.common.isEnabled = in_enb;
end


-- api
--
function This:setDebugModeFlag(in_dmd)
	self.vs_severeStorage.common.isInDebugMode = in_dmd;
end


-- api
--
function This:setDebugLevel(in_dlvl)
	self.vs_severeStorage.common.debugLevel = in_dlvl;
end


-- api
--
function This:setPlayerClassLoc(in_pcl)
	self.vs_severeStorage.common.playerClassLoc = in_pcl;
end


-- api
--
function This:setPlayerClassStd(in_pcs)
	self.vs_severeStorage.common.playerClassStd = in_pcs;
end


-- api
--
function This:setPlayerClassIndex(in_pci)
	self.vs_severeStorage.common.playerClassIndex = in_pci;
end


-- api
--
function This:setPlayerName(in_nm)
	self.vs_severeStorage.common.playerClassName = in_nm;
end


-- api
--
function This:setPlayerGender(in_gnd)
	self.vs_severeStorage.common.playerGender = in_gnd;
end


-- api
--
function This:setPlayerRealm(in_rlm)
	self.vs_severeStorage.common.playerRealm = in_rlm;
end


-- api
--
function This:setPlayerFactionGroup(in_grp)
	self.vs_severeStorage.common.playerFactionGroup = in_grp;
end


-- api
--
function This:setPlayerFactionGroupID(in_gri)
	self.vs_severeStorage.common.playerFactionGroupID = in_gri;
end


-- api
--
function This:setScreenWidth(in_scw)
	self.vs_severeStorage.common.screenWidth = in_scw;
end


-- api
--
function This:setScreenHeight(in_hgt)
	self.vs_severeStorage.common.screenHeight = in_hgt;
end


-- api
--
function This:setACREnabled(in_enb)
	self.vs_severeStorage.ACR.enabled = in_enb;
end


-- api
--
function This:setACRCombatLogging(in_clg)
	self.vs_severeStorage.ACR.isCombatLogging = in_clg;
end


-- api
--
function This:setACRCombatStatistic(in_stt)
	self.vs_severeStorage.ACR.isCombatStatistic = in_stt;
end


-- api
--
function This:setACRCriticalBlocked(in_blk)
	self.vs_severeStorage.ACR.isCriticalBlocked = in_blk;
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
--
function This:getEnabledFlag()
	return self.vs_severeStorage.common.isEnabled;
end


-- api
--
function This:getDebugModeFlag()
	return self.vs_severeStorage.common.isInDebugMode;
end


-- api
--
function This:getDebugLevel()
	return self.vs_severeStorage.common.debugLevel;
end


-- api
--
function This:getPlayerClassLoc()
	return self.vs_severeStorage.common.playerClassLoc;
end


-- api
--
function This:getPlayerClassStd()
	return self.vs_severeStorage.common.playerClassStd;
end


-- api
--
function This:getPlayerClassIndex()
	return self.vs_severeStorage.common.playerClassIndex;
end


-- api
--
function This:getPlayerName()
	return self.vs_severeStorage.common.playerClassName;
end


-- api
--
function This:getPlayerGender()
	return self.vs_severeStorage.common.playerGender;
end


-- api
--
function This:getPlayerRealm()
	return self.vs_severeStorage.common.playerRealm;
end


-- api
--
function This:getPlayerFactionGroup()
	return self.vs_severeStorage.common.playerFactionGroup;
end


-- api
--
function This:getPlayerFactionGroupID()
	return self.vs_severeStorage.common.playerFactionGroupID;
end


-- api
--
function This:getScreenWidth()
	return self.vs_severeStorage.common.screenWidth;
end


-- api
--
function This:getScreenHeight()
	return self.vs_severeStorage.common.screenHeight;
end


-- api
--
function This:getACREnabled()
	return self.vs_severeStorage.ACR.enabled;
end


-- api
--
function This:getACRCombatLogging()
	-- NOTE: комбат лочится обычным флагом
	return self.vs_severeStorage.ACR.isCombatLogging;
end


-- api
--
function This:getACRCombatStatistic()
	-- NOTE: комбат лочится обычным флагом
	return self.vs_severeStorage.ACR.isCombatStatistic;
end


-- api
--
function This:getACRCriticalBlocked()
	return self.vs_severeStorage.ACR.isCriticalBlocked;
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- NOTE: при частичном реинжиниринге системы запуска будет сделано и это
-- TODO: придумать шедул для загрузки, т.к. сейчас флаг дебага меняется после начала дампа дебажной инфы(GG-19)
-- api
-- Трейс иницализатора модуля
function This:traceModuleInitialize(in_parentModuleToken, in_moduleToken, in_moduleName, in_phases)
	local parentModule, traversePath = self:findModule(in_parentModuleToken, self.vs_maintenanceManifest.modules.data);
	if (not parentModule) then
		-- TODO: реализовать то, что под вызовом
		--GGM.FCS.ErrorHandler:DumpError(GGD.ECP.ClusterToken, 100, in_parentModuleToken);
		return;
	end
	parentModule[in_moduleToken] = in_moduleName;
	local loadTrace = traversePath .. " -> " .. in_moduleToken .. " initialized " .. in_phases;
	GGM.IOC.OutputController:RawPrint(loadTrace);
	--_G["GGM.IOC.OutputController.external:debugPrint"](GGD.ECP.ClusterToken, This.maintinfo.moduletoken, debugstack(1, 1, 0), 2, loadTrace);
	self.vs_maintenanceManifest.modules.loaded = self.vs_maintenanceManifest.modules.loaded + 1;
end


-- api
-- Трейс финишера модуля
function This:traceModuleFinish(in_parentModuleToken, in_moduletoken, in_modulename)
	local parentModule, traversePath = self:findModule(in_parentModuleToken, self.vs_maintenanceManifest.modules.data);
	if (not parentModule) then
		GGM.FCS.ErrorHandler:DumpError(GGD.ECP.ClusterToken, 101, in_parentModuleToken);
		return;
	end
	local loadTrace = traversePath .. " -> " .. in_moduleToken .. " finished";
	GGM.IOC.OutputController:RawPrint(loadTrace);
	--_G["GGM.IOC.OutputController.external:debugPrint"](GGD.ECP.ClusterToken, This.maintinfo.moduletoken, debugstack(1, 1, 0), 2, loadTrace);
end


-- Рекурсивный поиск модуля по токену
function This:findModule(in_moduleToken, in_manifest)
	-- Помимо таблиц там имена модулей валяются
	if (type(in_manifest) ~= 'table') then
		return nil, nil;
	end
	for k,v in pairs(in_manifest) do
		if (k == in_moduleToken) then
			return v, k;
		else
			local res, trp = self:findModule(in_moduleToken, v);
			if (res) then
				return res, k .. trp;
			end
		end
	end
	return nil, nil;
end


-- api
-- Переход системы в отладочный режим
function This:debugModeSlashCommand(in_dspec)
	self:setDebugModeFlag(true);
	local str = "";
	str = str .. "setting debug mode of GG System to " .. tostring(true);
	--print(str);
	GGM.IOC.OutputController:RawPrint(str);
end


-- api
-- Переход системы в стандартный режим
function This:releaseModeSlashCommand(in_dspec)
	self:setDebugModeFlag(false);
	local str = "";
	str = str .. "setting debug mode of GG System to " .. tostring(false);
	--print(str);
	GGM.IOC.OutputController:RawPrint(str);
end


-- api
-- Не реализовано
function This:proceedMaintSlashCommand(in_dspec)
	local str = "Maintenance dump isn't implemented";
	--print("Maint");
	GGM.IOC.OutputController:RawPrint(str);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------