local This = GGF.ModuleGetWrapper("FCS-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


--
function This:SetEnabledFlag(in_enb)
	self:setEnabledFlag(in_enb);
end


--
function This:SetDebugModeFlag(in_dmd)
	self:setDebugModeFlag(in_dmd);
end


--
function This:SetDebugLevel(in_dlvl)
	self:setDebugLevel(in_dlvl);
end


--
function This:SetPlayerClassLoc(in_pcl)
	self:setPlayerClassLoc(in_pcl);
end


--
function This:SetPlayerClassStd(in_pcs)
	self:setPlayerClassStd(in_pcs);
end


--
function This:SetPlayerClassIndex(in_pci)
	self:setPlayerClassIndex(in_pci);
end


--
function This:SetPlayerName(in_nm)
	self:setPlayerName(in_nm);
end


--
function This:SetPlayerGender(in_gnd)
	self:setPlayerGender(in_gnd);
end


--
function This:SetPlayerRealm(in_rlm) 
	self:setPlayerRealm(in_rlm);
end


--
function This:SetPlayerFactionGroup(in_grp)
	self:setPlayerFactionGroup(in_grp);
end


--
function This:SetPlayerFactionGroupID(in_gri)
	self:setPlayerFactionGroupID(in_gri);
end


--
function This:SetScreenWidth(in_scw)
	self:setScreenWidth(in_scw);
end


--
function This:SetScreenHeight(in_hgt)
	self:setScreenHeight(in_hgt);
end


--
function This:SetACREnabled(in_enb)
	self:setACREnabled(in_enb);
end


--
function This:SetACRCombatLogging(in_clg)
	self:setACRCombatLogging(in_clg);
end


--
function This:SetACRCombatStatistic(in_stt)
	self:setACRCombatStatistic(in_stt);
end


--
function This:SetACRCriticalBlocked(in_blk)
	self:setACRCriticalBlocked(in_blk);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


--
function This:GetEnabledFlag()
	return self:getEnabledFlag();
end


--
function This:GetDebugModeFlag()
	return self:getDebugModeFlag();
end


--
function This:GetDebugLevel()
	return self:getDebugLevel();
end


--
function This:GetPlayerClassLoc()
	return self:getPlayerClassLoc();
end


--
function This:GetPlayerClassStd()
	return self:getPlayerClassStd();
end


--
function This:GetPlayerClassIndex()
	return self:getPlayerClassIndex();
end


--
function This:GetPlayerName()
	return self:getPlayerName();
end


--
function This:GetPlayerGender()
	return self:getPlayerGender();
end


--
function This:GetPlayerRealm()
	return self:getPlayerRealm();
end


--
function This:GetPlayerFactionGroup()
	return self:getPlayerFactionGroup();
end


--
function This:GetPlayerFactionGroupID()
	return self:getPlayerFactionGroupID();
end


--
function This:GetScreenWidth()
	return self:getScreenWidth();
end


--
function This:GetScreenHeight()
	return self:getScreenHeight();
end


--
function This:GetACREnabled()
	return self:getACREnabled();
end


--
function This:GetACRCombatLogging()
	return self:getACRCombatLogging();
end

--
function This:GetACRCombatStatistic()
	return self:getACRCombatStatistic();
end


--
function This:GetACRCriticalBlocked()
	return self:getACRCriticalBlocked();
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- Трейс иницализатора модуля
function This:TraceModuleInitialize(in_parentModuleToken, in_moduleToken, in_moduleName, in_phases)
	self:traceModuleInitialize(in_parentModuleToken, in_moduleToken, in_moduleName, in_phases);
end


-- Трейс финишера модуля
function This:TraceModuleFinish(in_parentModuleToken, in_moduleToken, in_moduleName)
	self:traceModuleFinish(in_parentModuleToken, in_moduleToken, in_moduleName);
end


-- SC: /fcs debug
-- Переход системы в отладочный режим
function This:DebugModeSlashCommand(in_dspec)
	self:debugModeSlashCommand(in_dspec);
end


-- SC: /fcs release
-- Переход системы в стандартный режим
function This:ReleaseModeSlashCommand(in_dspec)
	self:releaseModeSlashCommand(in_dspec);
end


-- SC: /fcs maint
-- Не реализовано
function This:ProceedMaintSlashCommand(in_dspec)
	self:proceedMaintSlashCommand(in_dspec);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------