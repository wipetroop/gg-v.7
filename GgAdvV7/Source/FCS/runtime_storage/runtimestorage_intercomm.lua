local This = GGF.ModuleGetWrapper("FCS-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.0 (System Variables)
function This:MPRC_SVR_INITIALIZE()
	self:setEnabledFlag(true);
	self:setDebugModeFlag(true);
	self:setDebugLevel(2);

	local pcl, pcs, pci = UnitClass("player");

	self:setPlayerClassLoc(pcl);
	self:setPlayerClassStd(pcs);
	self:setPlayerClassIndex(pci);


	self:setPlayerName(UnitName("player"));
	self:setPlayerGender(UnitSex("player"));

	self:setPlayerRealm(GetRealmName());

	local pfg, pfgi = UnitFactionGroup("player");

	self:setPlayerFactionGroup(pfg);
	self:setPlayerFactionGroupID(pfgi);

	self:setScreenWidth(UIParent:GetWidth());
	self:setScreenHeight(UIParent:GetHeight());
end


-- п.2.1 (Miscellaneous Variables)
function This:MPRC_MVR_INITIALIZE()
	-- Убираем львов
	-- TODO: убрать львов...в другом месте..(GG-60)
	GGF.VersionSplitterProc({
		{
			versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.Draenor },
			action = function()
				MainMenuBarLeftEndCap:Hide();
				MainMenuBarRightEndCap:Hide();
			end,
		},
		{
			versionInterval = { GGD.TocVersionList.Legion, GGD.TocVersionList.Invalid },
			action = function()
				MainMenuBarArtFrame.LeftEndCap:Hide();
				MainMenuBarArtFrame.RightEndCap:Hide();
			end,
		},
	});
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------