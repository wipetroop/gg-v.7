local This = GGF.ModuleGetWrapper("FCS-5");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.1 (Miscellaneous Variables)
function This:MPRC_MVR_INITIALIZE()
	self.vs_timeData.loginTime = time();
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-- п.3.0 (Addon System Timer)
function This:MPRC_AST_RUNTIME(in_elapsed)
	-- NOTE: отключил для теста уёв
	GGF.Timer(self.vs_latRefreshTimer, in_elapsed, function()
		local down, up, lagHome, lagWorld = GetNetStats();
		GGM.GUI.CombatInstrumentation:SetLatency(lagHome, lagWorld);
	end);

	GGF.Timer(self.vs_frmRefreshTimer, in_elapsed, function()
		local framerate = GetFramerate();
		GGM.GUI.CombatInstrumentation:SetFrm(framerate);
		if (framerate >= 60) then
			GGM.GUI.CombatInstrumentation:SetFPSState("idle");
		else
			GGM.GUI.CombatInstrumentation:SetFPSState("triggered");
		end
	end);

	GGF.Timer(self.vs_ramRefreshTimer, in_elapsed, function()
		local amu_kb = GetAddOnMemoryUsage(GGD.AddonName);

		local amu_mb = amu_kb/1024.0;
		local amu_gb = amu_mb/1024.0;

		local amu = amu_kb/1024.0;
		local sz;

		if (amu >= 1024) then
			sz = string.format("%.2f GB", amu_gb);
		elseif (amu >= 1) then
			sz = string.format("%.1f MB", amu_mb);
		else
			sz = string.format("%d KB", amu_kb);
		end

		-- NOTE: временно отключен напрочь
		-- NOTE: отсюда запрос на чек
		--GGM.GUI.CombatInstrumentation:SetRamUsage(sz);
		GGM.GUI.CombatInstrumentation:SetRamUsageMeterValue(amu_mb);

		if (amu <= self.cs_ramLoadBorders.light) then
			sz = GGF.StructColorToStringColor(GGD.Color.Neutral) .. sz .. GGF.FlushStringColor();
		elseif (amu <= self.cs_ramLoadBorders.medium) then
			sz = GGF.StructColorToStringColor(GGD.Color.Normal) .. sz .. GGF.FlushStringColor();
		elseif (amu <= self.cs_ramLoadBorders.heavy) then
			sz = GGF.StructColorToStringColor(GGD.Color.Warning) .. sz .. GGF.FlushStringColor();
		else
			sz = GGF.StructColorToStringColor(GGD.Color.Critical) .. sz .. GGF.FlushStringColor();
		end

		local rData, rCount, rCur = self.vs_ramStatistic.data, self.vs_ramStatistic.dataCount, self.vs_ramStatistic.current;
		if (#rData < rCount) then
			for idx = #rData, rCount do
				rData[idx] = amu_mb;
			end
		end
		rData[rCur] = amu_mb;
		self.vs_ramStatistic.current = self.vs_ramStatistic.current + 1;
		if (rCur == 100) then
			self.vs_ramStatistic.current = 1;
		end

		local avgStr;
		local summ = 0;
		for idx = 1,#rData do
			summ = summ + rData[idx];
		end

		local avg = summ/rCount;
		local avg_kb, avg_mb, avg_gb = avg*1024, avg, avg/1024;

		if (avg >= 1024) then
			avgStr = string.format("%.2f GB", avg_gb);
		elseif (avg >= 1) then
			avgStr = string.format("%.1f MB", avg_mb);
		else
			avgStr = string.format("%d KB", avg_kb);
		end

		if (avg <= self.cs_ramLoadBorders.light) then
			avgStr = GGF.StructColorToStringColor(GGD.Color.Neutral) .. avgStr .. GGF.FlushStringColor();
		elseif (avg <= self.cs_ramLoadBorders.medium) then
			avgStr = GGF.StructColorToStringColor(GGD.Color.Normal) .. avgStr .. GGF.FlushStringColor();
		elseif (avg <= self.cs_ramLoadBorders.heavy) then
			avgStr = GGF.StructColorToStringColor(GGD.Color.Warning) .. avgStr .. GGF.FlushStringColor();
		else
			avgStr = GGF.StructColorToStringColor(GGD.Color.Critical) .. avgStr .. GGF.FlushStringColor();
		end

		GGM.GUI.CombatInstrumentation:SetRamUsageMeterAverageValue(avgStr);
	end);

	GGF.Timer(self.vs_clockRefreshTimer, in_elapsed, function()
		local difference = time() - self.vs_timeData.loginTime;
		local sec = math.fmod(difference, 60);
		local mtmp = (difference - sec)/60;
		local min = math.fmod(mtmp, 60);
		local hrs = (mtmp - min)/60;

		-- тут будут часы
		GGM.GUI.CombatInstrumentation:SetLoginSessionUptime(hrs, min, sec);
		if (GGM.ACR.RuntimeLogger:GetLoggingFlag()) then
			difference = time() - self.vs_timeData.logStartTime;
			sec = math.fmod(difference, 60);
			mtmp = (difference - sec)/60;
			min = math.fmod(mtmp, 60);
			hrs = (mtmp - min)/60;
			-- тут будут часы
			GGM.GUI.CombatInstrumentation:setBattleSessionUptime(hrs, min, sec);
		end
	end);

	GGF.Timer(self.vs_speedTelemetryTimer, in_elapsed, function()
		-- NOTE: появилась на 6.0.2
		local X, Y = 0, 0;
		GGF.VersionSplitterProc({
			{
				versionInterval = {GGD.TocVersionList.Vanilla, GGD.TocVersionList.Legion},
				action = function()
					local x, y = GetPlayerMapPosition("player");
					X = x;
					Y = y;
				end,
			},
			{
				versionInterval = {GGD.TocVersionList.BFA, GGD.TocVersionList.Invalid},
				action = function()
					-- WARNING: с 7.0.1 возвращает nil, если игрок в инсте
					--local x, y = UnitPosition("Player");--GetPlayerMapPosition("Player");
					local mapID = C_Map.GetBestMapForUnit("player");
					local mapPos = mapID and C_Map.GetPlayerMapPosition(mapID, "player");
					if (mapPos) then
						X, Y = mapPos:GetXY();
					end
				end,
			},
		});
		local pX, pY, t = self.vs_coordinateWatchdog.prevcoordinates.X, self.vs_coordinateWatchdog.prevcoordinates.Y, self.vs_coordinateWatchdog.interval;
		local speed = ((X - pX)^2 + (Y - pY)^2)^0.5/t;
		--speed = speed * 1000;
		--local facing = math.atan((pX - X)/(Y - pY));
		local cur, _, _, _ = GetUnitSpeed("player");
		local pFact, pTheor = speed*2000/self.vs_percentMultiplier, cur/self.vs_percentMultiplier;
		self.vs_coordinateWatchdog.prevcoordinates.X = X;
		self.vs_coordinateWatchdog.prevcoordinates.Y = Y;
		self.vs_coordinateWatchdog.currentSpeed.fact = pFact;
		self.vs_coordinateWatchdog.currentSpeed.theor = pTheor;

		-- TODO: исправить косяк расчета реальных скоростей при старте с нулей
		--print("spd(fact):", speed, self.vs_percentMultiplier, X, pX, Y, pY);
		--print("spd(theor, fact):", pTheor, pFact);
		--print("pitch:", GetUnitPitch("Player"), floor((((cur / 7) * 100) * math.cos(GetUnitPitch("Player"))) + .5));
		--GGM.GUI.SystemDashboard:SetMoveSpeedMeterValue(pFact);

		GGM.GUI.SystemInstrumentation:SetMoveSpeedMeterValue(pTheor);
	end);

	GGF.Timer(self.vs_speedStatisticTimer, in_elapsed, function()
		local rData, rCount, rCur = self.vs_speedStatistic.data, self.vs_speedStatistic.dataCount, self.vs_speedStatistic.current;
		if (#rData < rCount) then
			for idx = #rData, rCount do
				rData[idx] = self.vs_coordinateWatchdog.currentSpeed.fact;
			end
		end
		rData[rCur] = self.vs_coordinateWatchdog.currentSpeed.fact;
		self.vs_speedStatistic.current = self.vs_speedStatistic.current + 1;
		if (rCur == rCount) then
			self.vs_speedStatistic.current = 1;
		end

		local avgStr;
		local summ = 0;
		for idx = 1,#rData do
			summ = summ + rData[idx];
		end

		avgStr = GGF.StructColorToStringColor(GGD.Color.Neutral) .. string.format("%.2f km/h", summ/rCount) .. GGF.FlushStringColor();

		GGM.GUI.SystemInstrumentation:SetMoveSpeedMeterAverageValue(avgStr);
	end);
end


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------