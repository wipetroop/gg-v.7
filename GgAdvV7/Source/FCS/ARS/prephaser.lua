----------------------------------------------------------------------------
-- "functional" block
----------------------------------------------------------------------------


----------------------------------------------------------------------------
-- "define" block
----------------------------------------------------------------------------

GGM.FCS = {};
GGD.FCS = {};
GGD.FCS.ClusterName = "Flight Control System";
GGD.FCS.ClusterToken = "FCS";
GGD.FCS.ClusterVersion = GGD.BuildMeta.Version;
GGD.FCS.ClusterBuild = "AT-"..GGD.BuildMeta.Build;
GGD.FCS.ClusterMBI = "7102-7071";