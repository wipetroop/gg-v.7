-- Инициализация на стадии загрузки аддона
function GGM.FCS:OnAddonLoad()
	GGM.COM:RegisterInterComm(GGD.FCS.ClusterToken, GGM.FCS.EvokeController);

	GGM.IOC.OutputController:AddonWelcomeMessage({
		cluster = GGD.FCS.ClusterToken,
		version = GGD.FCS.ClusterVersion,
		build = GGD.FCS.ClusterBuild,
		mbi = GGD.FCS.ClusterMBI,
	});
end