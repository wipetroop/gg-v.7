local This = GGF.ModuleGetWrapper("FCS-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- NOTE: перевызвать сеттер во внешнем классе
-- api
-- Смена переменной отвечающей за настройку для конкретного персонажа
function This:setCharspec(in_mdpath, in_prtoken, in_charspec)
	local stc, stp = self:findParameterModule(in_mdpath);

	stp[in_prtoken].charspec = in_charspec;
end


-- api
-- Смена значения
function This:setValue(in_mdpath, in_prtoken, in_value)
	local stc, stp = self:findParameterModule(in_mdpath);

	if (stp[in_prtoken].charspec) then
		stp[in_prtoken].value = in_value;
	else
		stc[in_prtoken].value = in_value;
	end
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Взятие переменной отвечающей за настройку для конкретного персонажа
function This:getCharspec(in_mdpath, in_prtoken)
	local stc, stp = self:findParameterModule(in_mdpath);

	if (not stc[in_prtoken]) then
		-- TODO: сюда код ошибки присобачить(GG-57)
		return nil;
	end

	return stp[in_prtoken].charspec;
end


-- api
-- Взятие значения
function This:getValue(in_mdpath, in_prtoken)
	local stc, stp = self:findParameterModule(in_mdpath);

	if (not stc[in_prtoken]) then
		-- TODO: сюда код ошибки присобачить(GG-57)
		return nil;
	end

	return GGF.TernExpSingle(stp[in_prtoken].charspec, stp[in_prtoken].value, stc[in_prtoken].value);
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------

-- NOTE: конфиг уже работает, но достаточно криво(GG-58)
-- TODO: переделать механику конфига и хотя бы где-нибудь описать её
-- Структура:
-- ExplicitConfigCommon
-- -- <addon token>
-- -- -- <module token>
-- -- -- -- <parameter name>
-- ...
-- ExplicitConfigParticular
-- ...


-- Проверка с инициализацией конфига(первый	запуск)
function This:checkPrepareConfig()
	if (self.cs_configVariable.common == nil) then
		_G[self.cs_configVariableName.common] = {};
		self.cs_configVariable.common = _G[self.cs_configVariableName.common];
	end
	if (self.cs_configVariable.charspec == nil) then
		_G[self.cs_configVariableName.charspec] = {};
		self.cs_configVariable.charspec = _G[self.cs_configVariableName.charspec];
	end

	local moduleMap = GGM.COM.ModuleInterrogator:GetModuleMaintenanceMap();

	self:recursiveCheck(moduleMap, self.cs_configVariable.common);
	self:recursiveCheck(moduleMap, self.cs_configVariable.charspec);
end


-- Рекурсивный чек с заполнением конфига пустыми слотами под модули
function This:recursiveCheck(in_moduleMap, in_configPath, in_token)
	local moduleList = in_moduleMap.cluster or in_moduleMap.module;

	if (not moduleList) then
		return;
	end

	for k,v in pairs(moduleList) do
		if in_configPath[k] == nil then
			in_configPath[k] = {
				config = {},
			};
		end
		self:recursiveCheck(v, in_configPath[k]);
	end
end


-- api
-- Проверка наличия параметра во внешнем конфиге
function This:checkParameter(in_mdpath, in_prtoken)
	local stc, stp = self:findParameterModule(in_mdpath);
	return (stc[in_prtoken] ~= nil);
end


-- api
-- Создание параметра конфигурации
function This:createParameter(in_mdpath, in_prtoken, in_value, in_charspec)
	local stc, stp = self:findParameterModule(in_mdpath);
	if (not stc[in_prtoken]) then
		stc[in_prtoken] = {
			value = in_value,
		};
		stp[in_prtoken] = {
			value = in_value,
			-- эта переменная тут, иначе одна одна получится на всех...
			charspec = in_charspec,
		};
	else
		-- TODO: присобачить код ошибки(GG-57)
	end
end


-- Поиск модуля в общей переменной конфигурации по токену модуля
-- NOTE: не самой переменной, а именно модуля, которому она принадлежит
function This:findParameterModule(in_module)
	local stc = self.cs_configVariable.common;
	local stp = self.cs_configVariable.charspec;
	for pp in in_module:gmatch("(.-)_") do
		stc = stc[pp];
		stp = stp[pp];
		if (not stc) then
			-- TODO: присобачить код ошибки(GG-57)
			return nil, nil;
		end
	end

	return stc.config, stp.config;
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------