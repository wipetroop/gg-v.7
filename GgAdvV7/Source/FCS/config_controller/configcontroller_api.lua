local This = GGF.ModuleGetWrapper("FCS-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Смена переменной отвечающей за настройку для конкретного персонажа
function This:SetCharspec(in_mdpath, in_prtoken, in_charspec)
	self:setCharspec(in_mdpath, in_prtoken, in_charspec);
end


-- Смена значения
function This:SetValue(in_mdpath, in_prtoken, in_value)
	self:setValue(in_mdpath, in_prtoken, in_value);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие переменной отвечающей за настройку для конкретного персонажа
function This:GetCharspec(in_mdpath, in_prtoken)
	return self:getCharspec(in_mdpath, in_prtoken);
end


-- Взятие значения
function This:GetValue(in_mdpath, in_prtoken)
	return self:getValue(in_mdpath, in_prtoken);
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- Проверка наличия параметра во внешнем конфиге
function This:CheckParameter(in_mdpath, in_prtoken)
	return self:checkParameter(in_mdpath, in_prtoken);
end


-- Создание параметра конфигурации
function This:CreateParameter(in_mdpath, in_prtoken, in_value, in_charspec)
	self:createParameter(in_mdpath, in_prtoken, in_value, in_charspec);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------