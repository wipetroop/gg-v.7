local This = GGF.ModuleGetWrapper("FCS-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.3 (MDT Check)
function This:MS_CHECK_MDT_0()
	return 1;
end


-------------------------------------------------------------------------------
-- "multitudinous data" block
-------------------------------------------------------------------------------


This.md_errlist = {
	ACR = {
		-- Начало с 100, иначе мб редкий баг с вылетом игры(#BlizzardBug)
		-- P.S. я хз как воспроизвести этот баг, он какой-то рандомный...так что тп помочь никак...
		["100"] = "",
	},
	COM = {

	},
	FCS = {
		["100"] = "Module initialize trace exception: Invalid parent token",
		["101"] = "Module finish trace exception: Invalid parent token",
	},
	IOC = {
		["100"] = "Undefined command line argument",
	},
	GUI = {

	},
	SND = {

	},
};