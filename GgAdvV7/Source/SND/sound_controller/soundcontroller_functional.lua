local This = GGF.ModuleGetWrapper("SND-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- Пересчет хп после изменения максимума
function This:recountHp()
	if (self.vf_override == true) then
		return
	end

	self.vn_health = UnitHealthMax("Player");
	if (self.vn_health <= self.cn_hpEpsilon) then
		local ctm = C_Timer.NewTimer(5, function() self:recountHp() end);
		-- TODO: если не прокатило - ставить чек(GG-65)
		return;
	end
	local hpLine = "";
	for k,v in pairs(self.vs_dmgBorder) do
		self.vs_dmgBorder[k] = self.cs_dmgBorderSeed[k]*self.vn_health;
	end
	hpLine = "|rHB:|cFF093CFF" .. self.vs_dmgBorder[1] .. " |rHMB:|cFF07F9D5" .. self.vs_dmgBorder[2] .. " |rMB:|cFF1CF405" .. self.vs_dmgBorder[3] .. " |rMLB:|cFFEEE403" .. self.vs_dmgBorder[4] .. " |rLB:|cFFE90400" .. self.vs_dmgBorder[5];
	GGM.IOC.OutputController:CommonPrint(self, hpLine);
end


-- Создаем 
function This:createReversedTable()
   for id, data in pairs(self.md_defaultSpellSoundMap) do
      local spellName = select(1, GetSpellInfo(id));
      if spellName ~= nil then
         self.md_reversedSpellSoundMap[spellName] = id;
      end
   end
   local specSpellName = select(1, GetSpellInfo(self.cn_rangeAutoAttackID));
   self.md_reversedSpellSoundMap[specSpellName] = self.cn_rangeAutoAttackID
end


-- Обработчик выстрелов
function This:spellCastHandler(...)
	local argMap = {...};
	local unitName = argMap[self.cs_spellCastArgMap["UNITNAME"]];
	local sid = 0;
	GGF.VersionSplitterProc({
		{
			versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.WotLK },
			action = function()
            sid = self.md_reversedSpellSoundMap[argMap[2]]
			end,
		},
		{
			versionInterval = { GGD.TocVersionList.Cata, GGD.TocVersionList.Invalid },
			action = function()
            sid = argMap[self.cs_spellCastArgMap["SPELLID"]]
			end,
		}
	});
	--print("unit - ", unitName, "| sid - ", sid, "|", ...);
	if (unitName == "player") then
		-- TODO: подумать, можно ли запихать бурст в тернарный оператор...(GG-63)
		if (sid == self.cn_rangeAutoAttackID) and (self.vf_burstFlag == true) then
			PlaySoundFile(self.cl_burstedAutoshotSound);
		else
			if (self.vs_runtimeSpellSoundMap[sid]) then
			 	if (self.vs_runtimeSpellSoundMap[sid].override) then
					PlaySoundFile(self.vs_runtimeSpellSoundMap[sid].spellSound);
				else
					PlaySoundFile(self.md_defaultSpellSoundMap[sid].spellSound);
				end
			end
		end
	end
end


-- Обработчик кривых кастов(видимо)(отключен)
--function This.dpm_skillsoundM(timestamp, daMageType, hideCaster, sourceID, sourceName, sourceFlag, sourceRaidFlag, targetID, targetName, targetFlag, targetRaidFlag, spellID, spellName, spellSchool, amount, overkill, daMageSchool, isResisted, isBlocked, isAbsorbed, isCritical, isGlancing, isCrushing, isOffHand, isMultistrike)
function This:spellCastHandlerM(in_timestamp, in_daMageType, in_hideCaster, in_sourceID, in_sourceName, in_sourceFlag, in_sourceRaidFlag, in_targetID, in_targetName, in_targetFlag, in_targetRaidFlag, in_spellID, in_spellName, in_spellSchool, in_amount, in_overkill, in_daMageSchool, in_isResisted, in_isBlocked, in_isAbsorbed, in_isCritical, in_isGlancing, in_isCrushing, in_isOffHand, in_isMultistrike)
	if (in_sourceName == self.cl_PlayerName) then
		if (in_daMageType == "SPELL_CAST_SUCCESS") then
			print_dbg(in_timestamp, in_daMageType, in_hideCaster, in_sourceID, in_sourceName, in_sourceFlag, in_sourceRaidFlag, in_targetID, in_targetName, in_targetFlag, in_targetRaidFlag, in_spellID, in_spellName, in_spellSchool, in_amount, in_overkill, in_daMageSchool, in_isResisted, in_isBlocked, in_isAbsorbed, in_isCritical, in_isGlancing, in_isCrushing, in_isOffHand, in_isMultistrike);
		end
	end
end


-- Обработчик аур
--function This.proc_handler(_, message, _, _, sourceName, _, _, destGUiD, destName, _, _, spellID, spellName, _, auraType, amount)
function This:auraHandler(_, in_message, _, _, in_sourceName, _, _, in_destGUiD, in_destName, _, _, in_spellID, in_spellName, _, in_auraType, in_amount)
	local PlayerName = self.cl_PlayerName;

	if (in_message == "SPELL_AURA_APPLIED" or in_message == "SPELL_AURA_REFRESH") then
		if (in_sourceName == PlayerName and in_destName == PlayerName and in_spellID == self.vn_burstAuraID) then
			--PlaySoundFile("Interface\\Addons\\GgAdvV7\\Sounds\\SND\\ogg\\spell_trinket_proc.ogg")
			if (UnitAura("Player", This.burstAuraID) ~= nil) then
				This.burstFlag = true;
			end
		end
	elseif (in_message == "SPELL_AURA_REMOVED") then
		if (in_sourceName == PlayerName and in_destName == PlayerName) then
			if (UnitAura("Player", self.vn_burstAuraID) == false) then
				self.vf_burstFlag = false;
			end
		end
	end
end


-- Обработчик нанесения урона(именно урона)
function This:damageHandler(in_timestamp, in_damageType, in_hideCaster, in_sourceID, in_sourceName, in_sourceFlag, in_sourceRaidFlag, in_targetID, in_targetName, in_targetFlag, in_targetRaidFlag, in_spellID, in_spellName, in_spellSchool, in_amount, in_overkill, in_daMageSchool, in_isResisted, in_isBlocked, in_isAbsorbed, in_isCritical, in_isGlancing, in_isCrushing, in_isOffHand, in_isMultistrike)
	-- TODO: отрефакторить имена на номернутые параметры а потом поименовать, чтобы урон не лежал в резисте...(GG-17)
	-- NOTE: в рамках другой задачи этот обработчик будет убран и заменен вызовом из ACR
	--arguments :
	--timestamp
	--daMageType
	--hideCaster
	--sourceID
	--sourceName 
	--sourceFlag 
	--sourceRaidFlag 
	--targetID 
	--targetName 
	--targetFlag 
	--targetRaidFlag 
	--spellID 
	--spellName 
	--spellSchool
	--amount
	--overkill 
	--daMageSchool 
	--isResisted 
	--isBlocked 
	--isAbsorbed 
	--isCritical 
	--isGlancing 
	--isCrushing 
	--isOffHand 
	--isMultistrike	

	local dsAmount = 0;
	local dsSpellID = 0;
	local dsSourceID = 0;
	local dsSourceName = "";
	local dsIsCritical = 0;
	local dsIsMultistrike = 0;
	local PlayerName = self.cs_PlayerName;
	
	-- Тут только залив дамага в стату и сброс всего, что не в игрока ушло
	if (in_targetName ~= PlayerName) then
		if ((in_daMageType == "SPELL_DAMAGE") or (in_daMageType == "RANGE_DAMAGE") or (in_daMageType == "SWING_DAMAGE")) then
			if (in_daMageType == "SWING_DAMAGE") then -- если тип урона SWING_DAMage - дамаг хранится в spellID
				dsAmount = in_spellID;
				dsSpellID = self.cn_meleeAutoAttackID; 
				dsSourceName = in_sourceName;
				dsIsCritical = in_isResisted;
				dsIsMultistrike = in_isGlancing;
			end

			if ((in_daMageType == "RANGE_DAMAGE") or (in_daMageType == "SPELL_DAMAGE")) then
				dsAmount = in_amount;
				dsSpellID = in_spellID;
				dsSourceName = in_sourceName;
				dsIsCritical = in_isCritical;
				dsIsMultistrike = in_isMultistrike;
			end
			self:commitStatisticDaMage(dsSourceName, dsSpellID, dsAmount, dsIsCritical, dsIsMultistrike);
		end
		return
	end

	-- Тут все промахи
	if ((in_daMageType == "SPELL_MISSED") or (in_daMageType == "RANGE_MISSED") or (in_daMageType == "SWING_MISSED")  or (in_daMageType == "SPELL_BUILDING_MISSED") or (in_daMageType == "ENVIRONMENTAL_MISSED")) then
		if ((in_daMageType == "SPELL_BUILDING_MISSED") or (in_daMageType == "ENVIRONMENTAL_MISSED")) then
			--print_dbg(timestamp, daMageType, hideCaster, sourceID, sourceName, sourceFlag, sourceRaidFlag, targetID, targetName, targetFlag, targetRaidFlag, spellID, spellName, spellSchool, amount, overkill, daMageSchool, isResisted, isBlocked, isAbsorbed, isCritical, isGlancing, isCrushing, isOffHand, isMultistrike)
			return
		end
		
		-- Ну а зачем тут миссы??...
		if (in_amount == "MISS") then
			--print_dbg(timestamp, daMageType, hideCaster, sourceID, sourceName, sourceFlag, sourceRaidFlag, targetID, targetName, targetFlag, targetRaidFlag, spellID, spellName, spellSchool, amount, overkill, daMageSchool, isResisted, isBlocked, isAbsorbed, isCritical, isGlancing, isCrushing, isOffHand, isMultistrike)
			return
		end

		-- Временное(убрано)
		--if ((amount == "PARRY") or  (amount == "ABSORB") or (amount == "BLOCK") or (amount == "IMMUNE") or (amount == "RESIST")) then
			--print_dbg(timestaendmp, daMageType, hideCaster, sourceID, sourceName, sourceFlag, sourceRaidFlag, targetID, targetName, targetFlag, targetRaidFlag, spellID, spellName, spellSchool, amount, overkill, daMageSchool, isResisted, isBlocked, isAbsorbed, isCritical, isGlancing, isCrushing, isOffHand, isMultistrike)
		--
		self:ricochetSubHandler(in_damageType, in_spellID, in_amount, in_isResisted);
		
		self:notHitSubHandler(in_damageType, in_spellID, in_amount, in_isResisted);
		--print_dbg(timestamp, daMageType, hideCaster, sourceID, sourceName, sourceFlag, sourceRaidFlag, targetID, targetName, targetFlag, targetRaidFlag, spellID, spellName, spellSchool, amount, overkill, daMageSchool, isResisted, isBlocked, isAbsorbed, isCritical, isGlancing, isCrushing, isOffHand, isMultistrike)
	else
		self:hitSubHandler(in_damageType, in_sourceName, in_spellID, in_amount, in_isCritical, in_isGlancing, in_isMultistrike);
	end
end


-- TODO: залинковать ифы снизу...
-- Обработчик попаданий, нанесших урон
function This:hitSubHandler(in_damageType, in_sourceName, in_spellID, in_amount, in_isCritical, in_isGlancing, in_isMultistrike)
	local dsAmount = 0;
	local dsSpellID = 0;
	local dsSourceID = 0;
	local dsSourceName = "";
	local dsIsCritical = 0;
	local dsIsMultistrike = 0;
	local PlayerName = self.cl_PlayerName;

	if ((in_daMageType == "SPELL_DAMAGE") or (in_daMageType == "RANGE_DAMAGE") or (in_daMageType == "SWING_DAMAGE")  or (daMageType == "SPELL_BUILDING_DAMAGE") or (daMageType == "ENVIRONMENTAL_DAMAGE")) then
		if ((in_daMageType == "SPELL_BUILDING_DAMAGE") or (in_daMageType == "ENVIRONMENTAL_DAMAGE")) then
			--print_dbg_special(timestamp, daMageType, hideCaster, sourceID, sourceName, sourceFlag, sourceRaidFlag, targetID, targetName, targetFlag, targetRaidFlag, spellID, spellName, spellSchool, amount, overkill, daMageSchool, isResisted, isBlocked, isAbsorbed, isCritical, isGlancing, isCrushing, isOffHand, isMultistrike)
			return
		end
		
		if (in_daMageType == "SWING_DAMAGE") then -- если тип урона SWING_DAMage - дамаг хранится в spellID
			dsAmount = in_spellID;
			dsSpellID = self.cn_meleeAutoAttackID;
			dsSourceName = in_sourceName;
			dsIsCritical = in_isResisted;
			dsIsMultistrike = in_isGlancing;
		end

		if ((in_daMageType == "RANGE_DAMAGE") or (in_daMageType == "SPELL_DAMAGE")) then
			dsAmount = in_amount;
			dsSpellID = in_spellID;
			dsSourceName = in_sourceName;
			dsIsCritical = in_isCritical;
			dsIsMultistrike = in_isMultistrike;
		end
		
		self:commitStatisticDaMage(dsSourceName, dsSpellID, dsAmount, dsIsCritical, dsIsMultistrike);
		
		--print_dbg(timestamp, daMageType, hideCaster, sourceID, sourceName, sourceFlag, sourceRaidFlag, targetID, targetName, targetFlag, targetRaidFlag, spellID, spellName, spellSchool, amount, overkill, daMageSchool, isResisted, isBlocked, isAbsorbed, isCritical, isGlancing, isCrushing, isOffHand, isMultistrike)			
		if (dsAmount >= self.vs_dmgBorder[1]) then	--inf-25%
			PlaySoundFile(self.md_hitMap.huge[random(1,3)]);
			--print("d "..dmgBorder[1])			
		elseif (dsAmount >= self.vs_dmgBorder[2]) then	--25-20%
			PlaySoundFile(self.md_hitMap.large[random(1,3)]);
			--print("d "..dmgBorder[2])
		elseif (dsAmount >= self.vs_dmgBorder[3]) then	--20-15%
			PlaySoundFile(self.md_hitMap.main[random(1,4)]);
			--print("d "..dmgBorder[3])	
		elseif (dsAmount >= self.vs_dmgBorder[4]) then	--15-10%
			PlaySoundFile(self.md_hitMap.medium[random(1,5)]);
			--print("d "..dmgBorder[4])
		elseif (dsAmount >= self.vs_dmgBorder[5]) then	--10-5%
			PlaySoundFile(self.md_hitMap.small[random(1,5)]);
			--print("d "..dmgBorder[5])
		else	--5-0%
			PlaySoundFile(self.md_hitMap.automatic[random(1,5)]);
			--print("d 0")
		end
 	end
end


-- Обработчик попаданий, не нанесших урон
function This:notHitSubHandler(in_daMageType, in_spellID, in_amount, in_isResisted)
	local dsAmount = 0;
	local dsSpellID = 0;
	local dsSourceID = 0;
	local dsSourceName = "";
	local dsIsCritical = 0;
	local dsIsMultistrike = 0;
	local PlayerName = self.cl_PlayerName;

	-- Так-с..
		-- spellID - тип промаха
		-- amount - урон, если там абсорб
	if (in_daMageType == "SWING_MISSED") then
		dsSpellID = self.cn_meleeAutoAttackID;
		dsAmount = in_amount;
		-- пока что не нужен
	else
		dsSpellID = in_spellID;
		dsAmount = in_isResisted;
	end

	if ((in_amount == "ABSORB") or (in_amount == "BLOCK") or (in_amount == "DEFLECT") or (in_amount == "IMMUNE") or (in_amount == "RESIST")) then
		local statAvg = 0;
		if (in_amount == "ABSORB") then
			statAvg = dsAmount;
		else
			if (self.vs_runtimeADSC[dsSpellID] == nil) then
				-- TODO: убрать магическое значени 1.2(GG-63)
				statAvg = random(0, self.vs_dmgBorder[1]*1.2);
			else
				local unitHM = 0;

				if (UnitHealthMax(dsSourceName) == 0) then
					unitHM = self.cn_spikeUHM;
				else
					unitHM = UnitHealthMax(dsSourceName);
				end
				statAvg = self.vs_runtimeADSC[dsSpellID].multiplier * unitHM;
			end
		end
		if (statAvg == nil) then
			statAvg = self.vs_dmgBorder[1];
		end
		if (statAvg >= self.vs_dmgBorder[1]) then	--inf-25%
			PlaySoundFile(self.md_nhitMap.huge[random(1,3)]);
			--print("n "..dmgBorder[1])			
		elseif (statAvg >= self.vs_dmgBorder[2]) then	--25-20%
			PlaySoundFile(self.md_nhitMap.large[random(1,3)]);
			--print("n "..dmgBorder[2])
		elseif (statAvg >= self.vs_dmgBorder[3]) then	--20-15%
			PlaySoundFile(self.md_nhitMap.main[random(1,4)]);
			--print("n "..dmgBorder[3])	
		elseif (statAvg >= self.vs_dmgBorder[4]) then	--15-10%
			PlaySoundFile(self.md_nhitMap.medium[random(1,5)]);
			--print("n "..dmgBorder[4])
		elseif (statAvg >= self.vs_dmgBorder[5]) then	--10-5%
			PlaySoundFile(self.md_nhitMap.small[random(1,5)]);
			--print("n "..dmgBorder[5])
		else	--5-0%
			PlaySoundFile(self.md_nhitMap.automatic[random(1,5)]);
			--print("n 0")
		end
	end
end


-- Обработчик отрикошетивших попаданий
function This:ricochetSubHandler(in_daMageType, in_spellID, in_amount, in_isResisted)
	local dsAmount = 0;
	local dsSpellID = 0;
	local dsSourceID = 0;
	local dsSourceName = "";
	local dsIsCritical = 0;
	local dsIsMultistrike = 0;
	local PlayerName = self.cl_PlayerName;  

	-- Так-с..
		-- spellID - тип промаха
		-- amount - урон, если там абсорб
	if (in_daMageType == "SWING_MISSED") then
		dsSpellID = self.cn_meleeAutoAttackID;
		dsAmount = in_amount;
		-- пока что не нужен
	else
		dsSpellID = in_spellID;
		dsAmount = in_isResisted;
	end

	if ((in_amount == "REFLECT") or (in_amount == "PARRY")) then
		local statAvg = 0;
		if (self.vs_runtimeADSC[dsSpellID] == nil) then
			statAvg = random(0, self.vs_dmgBorder[1]*1.2);
		else
			local unitHM = 0;

			if (UnitHealthMax(dsSourceName) == 0) then
				unitHM = self.cn_spikeUHM;
			else
				unitHM = UnitHealthMax(dsSourceName);
			end
		 	statAvg = self.vs_runtimeADSC[dsSpellID].multiplier * unitHM;
		end
		--if ((dsAmount >= dmgBorder[1])) then --inf-25%
		if (statAvg >= self.vs_dmgBorder[1]) then
			PlaySoundFile(self.md_ricochetMap.huge[random(1,3)]);
			--print("r-i-25")			
		--elseif (dsAmount >= dmgBorder[2]) then	--25-20%
		elseif (statAvg >= self.vs_dmgBorder[2]) then
			PlaySoundFile(self.md_ricochetMap.large[random(1,3)]);
			--print("r-25-20")
		--elseif (dsAmount >= dmgBorder[3]) then	--20-15%
		elseif (statAvg >= self.vs_dmgBorder[3]) then
			PlaySoundFile(self.md_ricochetMap.main[random(1,3)]);
			--print("r-20-15")	
		--elseif (dsAmount >= dmgBorder[4]) then	--15-10%
		elseif (statAvg >= self.vs_dmgBorder[4]) then
			PlaySoundFile(self.md_ricochetMap.medium[random(1,3)]);
			--print("r-15-10")
		--elseif (dsAmount >= dmgBorder[5]) then	--10-5%
		elseif (statAvg >= self.vs_dmgBorder[5]) then
			PlaySoundFile(self.md_ricochetMap.small[random(1,3)]);
			--print("r-10-5")
		else	--5-0%
			PlaySoundFile(self.md_ricochetMap.automatic[random(1,5)]);
			--print("r-5-0")
		end
	end
end


-- 
function This:commitStatisticDamage(in_sourceName, in_spellID, in_amount, in_isCritical, in_isMultistrike)
	local mltplrDmgS, dmgEncS, mltplrDmg = 0, 0, 0;
	local spikeMultiplier = 0;
	local unitHM = 0;

	if ((in_sourceName == "unit") or (in_sourceName == nil)) then
		return
	end

	if (UnitHealthMax(in_sourceName) == 0) then
		unitHM = self.cn_spikeUHM;
	else
		unitHM = UnitHealthMax(in_sourceName);
	end

	mltplrDmg = in_amount / unitHM;
	-- Каждый мультикаст = 30% основного урона
	if (in_isMultistrike) then 
		mltplrDmg = mltplrDmg * 3;
	end
	-- Каждый крит =
	-- 		пвп : 150% основного + модификаторы(на них придется забить...)
	-- 		пве : 200% основного + модификаторы(на них придется забить...)
	if (in_isCritical) then
		mltplrDmg = mltplrDmg / 1.75;	-- ну..усреднил, да...че, нельзя чтоль?
	end

	if (self.vs_runtimeADSC[in_spellID] ~= nil) then
		mltplrDmgS = self.vs_runtimeADSC[in_spellID].multiplier;
		dmgEncS = self.vs_runtimeADSC[in_spellID].count;
	else
		self.vs_runtimeADSC[in_spellID] = {};
		self.vs_runtimeADSC[in_spellID].multiplier = mltplrDmg;
		self.vs_runtimeADSC[in_spellID].count = 1;
		return
	end

	if (dmgEncS < self.cn_statisticCountUpperBound) then
		self.vs_runtimeADSC[in_spellID].count = dmgEncS + 1;
	end

	-- TODO: Как-нибудь проверить...хоть в маткаде, хоть где..
	spikeMultiplier = mltplrDmg*dmgEncS*(dmgEncS + 1)/(mltplrDmg*dmgEncS - mltplrDmgS*dmgEncS);
	self.vs_runtimeADSC[in_spellID].multiplier = mltplrDmgS + mltplrDmg/spikeMultiplier;
end


-- Дамп статистики в чат
function This:dumpStatistic()
	--print(SNT_runtimeADSC[1])
	for k,v in pairs(self.vs_runtimeADSC) do
		print("spell id : "..k.." / mltpr : "..v.multiplier.." / count : "..v.count.." / avg dmg : "..v.multiplier*self.cn_spikeUHM)
	end
end


-- Обработчик загрузки внешних переменных
function This:loadParameterVariables()
	self.vs_runtimeSpellSoundMap = GGF.TableCopy(self.md_defaultSpellSoundMap);
	
	-- Merge
	if (GGSpellSound == nil or GGSpellSound == "") then
	else
		self.vs_runtimeSpellSoundMap = GGF.TableMerge(self.vs_runtimeSpellSoundMap, GGSpellSound);
	end

	if (GGM.ACR.DamageStatistic == nil or GGM.ACR.DamageStatistic == "") then
	else
		self.vs_runtimeADSC = GGF.TableCopy(GGM.ACR.DamageStatistic);
	end
end


-- Обработчик сохранения данных во внешних переменных
function This:saveParameterVariables()
	GGM.ACR.DamageStatistic = GGF.TableCopy(self.vs_runtimeADSC);
	GGSpellSound = GGF.TableCopy(self.vs_runtimeSpellSoundMap);
	--SNT_avgDmgStatisticCluster = SNT_runtimeADSC;	
end


-- 
function This:dropStatistic()
	-- тут пока непонятно ничего
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-- Обработчик эвента измененения кол-ва хп
function This:onHpChange(in_table, in_event, in_unitID)
	if (in_unitID == "Player") then
		self:recountHp();
		--print(health)
		if (health == 0) then
			local ctm = C_Timer.NewTimer(5, function() self:recountHp() end);
		end
	end
end


-- Обработчик нескольких эвентов
function This:eventDispatcher(in_table, in_event, ...)
	--if (in_event == "COMBAT_LOG_EVENT_UNFILTERed") then
		--self:auraHandler(...);
		--self:damageHandler(...);
		
		--spellCastHandlerM(...)
	--elseif (in_event == "UNIT_SPELLCAST_SUCCEEDED") then
		self:spellCastHandler(...);
	--end
end


-------------------------------------------------------------------------------
-- там дальше какая-то дичь
-------------------------------------------------------------------------------


-- TODO: убрать дичь...(GG-3)



local function print_dbg_special(timestamp, damageType, hideCaster, sourceID, sourceName, sourceFlag, sourceRaidFlag, targetID, targetName, targetFlag, targetRaidFlag, spellID, spellName, spellSchool, amount, overkill, damageSchool, isResisted, isBlocked, isAbsorbed, isCritical, isGlancing, isCrushing, isOffHand, isMultistrike)
	local pname = {"timestamp", "damageType", "hideCaster", "sourceID", "sourceName", "sourceFlag", "sourceRaidFlag", "targetID", "targetName", "targetFlag", "targetRaidFlag", "spellID", "spellName", "spellSchool", "amount", "overkill", "damageSchool", "isResisted", "isBlocked", "isAbsorbed", "isCritical", "isGlancing", "isCrushing", "isOffHand", "isMultistrike"}
	--local printResult = ""
	local arg={timestamp, damageType, hideCaster, sourceID, sourceName, sourceFlag, sourceRaidFlag, targetID, targetName, targetFlag, targetRaidFlag, spellID, spellName, spellSchool, amount, overkill, damageSchool, isResisted, isBlocked, isAbsorbed, isCritical, isGlancing, isCrushing, isOffHand, isMultistrike}
	--print(...)
	print("dbg print")
	for i,v in pairs(pname) do
		--printResult = printResult .. tostring(v) .. " "
		print(tostring(v), arg[i])
    end
    print("dbg eof")
end

local function print_dbg(...)
	local pname = {"timestamp", "damageType", "hideCaster", "sourceID", "sourceName", "sourceFlag", "sourceRaidFlag", "targetID", "targetName", "targetFlag", "targetRaidFlag", "spellID", "spellName", "spellSchool", "amount", "overkill", "damageSchool", "isResisted", "isBlocked", "isAbsorbed", "isCritical", "isGlancing", "isCrushing", "isOffHand", "isMultistrike"}
	--local printResult = ""
	local arg={...}
	--print(...)
	print("dbg print")
	for i,v in ipairs(arg) do
		--printResult = printResult .. tostring(v) .. " "
		print(pname[i] .. " : " .. tostring(v))
	end
	print("dbg eof")
end


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------