local This = GGF.ModuleGetWrapper("SND-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- TODO: возможно убрать
-- CC: /snd dropstat
-- 
function This:DropStatisticSlashCommand()
	self:dropStatistic();
end


-- CC: /snd dumpstat
--
function This:DumpStatisticSlashCommand()
	self:dumpStatistic();
end


-- TODO: реализовать(при необходимости)
-- CC: /snd savestat
--
function This:SaveStatisticSlashCommand()
	--self:saveStatistic();
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------