local This = GGF.ModuleGetWrapper("SND-3");

 -------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.1 (Miscellaneous Variables)
function This:MPRC_MVR_INITIALIZE()
	self.cs_playerName = GGM.FCS.RuntimeStorage:GetPlayerName();
	self:createReversedTable();
	self:recountHp();
	self:loadParameterVariables();
end


-- п.2.6 (Main Event Callbacks)
function This:MPRC_MEC_INITIALIZE()
	-- Смена макс. значения хп
	self.fr_healthChanger:RegisterEvent("UNIT_MAXHEALTH");
	-- Успешное завершение каста спелла(инкастовые выстрелы считаются)
	self.fr_soundInvoker:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED")
	-- Все эвенты лога боя
	--self.fr_soundInvoker:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERed")

	self.fr_healthChanger:SetScript("OnEvent", function(...) self:onHpChange(...) end);
	self.fr_soundInvoker:SetScript("OnEvent", function(...) self:eventDispatcher(...) end);
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-- п.4.1 (Coup De Module)
function This:MPRC_CDM_TERMINATE()
	self:saveParameterVariables();
end


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------