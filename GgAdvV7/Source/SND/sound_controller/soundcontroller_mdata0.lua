local This = GGF.ModuleGetWrapper("SND-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.3 (MDT Check)
function This:MS_CHECK_MDT_0()
	return 1;
end


-------------------------------------------------------------------------------
-- "multitudinous data" block
-------------------------------------------------------------------------------


-- Концепция:
-- Система дает инфу по спеллу через ид
-- Звук можно переопределить через GUi
-- В этом случае override станет true
-- For Legion
This.md_defaultSpellSoundMap = {
	[157708] = { -- removed in Legion
		spellName = "Killshot",
		spellSound = GGD.SND.OggSoundPath .. "spell_killshot.ogg",
		override = false,
	},
	[53351] = { -- Panda
		spellName = "Killshot",
		spellSound = GGD.SND.OggSoundPath .. "spell_killshot.ogg",
		override = false,
	},
	[53209] = {	-- moved in Legion(only bm for now)
		spellName = "Chimera Shot",
		spellSound = GGD.SND.OggSoundPath .. "large_gun_01_01.ogg",
		override = false,
	},
	[53353] = {
		spellName = "Chimera Shot(Snake)",
		spellSound = GGD.SND.OggSoundPath .. "large_gun_01_01.ogg",
		override = false,
	},
	[53358] = {
		spellName = "Chimera Shot(Viper)",
		spellSound = GGD.SND.OggSoundPath .. "large_gun_01_01.ogg",
		override = false,
	},
	[53359] = {
		spellName = "Chimera Shot(Scorpid)",
		spellSound = GGD.SND.OggSoundPath .. "large_gun_01_01.ogg",
		override = false,
	},
	[53366] = {
		spellName = "Chimera Shot(Wyvern)",
		spellSound = GGD.SND.OggSoundPath .. "large_gun_01_01.ogg",
		override = false,
	},
	[342049] = {	-- new chimaera(from Shadowlands)
		spellName = "Chimera Shot",
		spellSound = GGD.SND.OggSoundPath .. "spell_sniper_shot.ogg",
		override = false,
	},
	[19434] = { 
		spellName = "Aimed Shot",
		spellSound = GGD.SND.OggSoundPath .. "spell_aim.ogg",
		override = false,
	},
	[213691] = {
		spellName = "Scatter Shot",
		spellSound = GGD.SND.Mp3SoundPath .. "glock18-1_03.mp3",
		override = false,
	},
	[19503] = {
		spellName = "Scatter Shot",
		spellSound = GGD.SND.Mp3SoundPath .. "glock18-1_03.mp3",
		override = false,
	},
	[185358] = {	-- Legion and later
		spellName = "Arcane Shot",
		spellSound = GGD.SND.Mp3SoundPath .. "gun_01.mp3",
		override = false,
	},
	[3044] = {	-- Panda
		spellName = "Arcane Shot",
		spellSound = GGD.SND.Mp3SoundPath .. "gun_01.mp3",
		override = false,
	},
	[75] = {
		spellName = "Autoshot",
		spellSound = GGD.SND.OggSoundPath .. "spell_autoshot_sq.ogg",
		override = false,
	},
	[2643] = {
		spellName = "Multishot",
		spellSound = GGD.SND.OggSoundPath .. "spell_multishot.ogg",
		override = false,
	},
	[56641] = {
		spellName = "Steady Shot",
		spellSound = GGD.SND.OggSoundPath .. "spell_steady.ogg",
		override = false,
	},
	[54998] = {	-- from 3.3.5a(engineering)
		spellName = "Hand Launcher",
		spellSound = GGD.SND.OggSoundPath .. "spell_hand_launcher.ogg",
		override = false,
	},
	[5116] = {
		spellName = "Concussive Shot",
		spellSound = GGD.SND.OggSoundPath .. "spell_concussive.ogg",
		override = false,
	},
	[19801] = {
		spellName = "Tranquilizing Shot",
		spellSound = GGD.SND.OggSoundPath .. "spell_tranquilizing.ogg",
		override = false,
	},
	[147362] = {
		spellName = "Counter Shot",
		spellSound = GGD.SND.OggSoundPath .. "spell_counter.ogg",
		override = false,
	},
	[34490] = {	-- Panda
		spellName = "Silencing Shot",
		spellSound = GGD.SND.OggSoundPath .. "spell_counter.ogg",
		override = false,
	},
	[77767] = {
		spellName = "Cobra Shot",
		spellSound = GGD.SND.OggSoundPath .. "spell_cobra.ogg",
		override = false,
	},
	[82928] = {	-- А хз че эт за спелл...вовхедом не чекается
		spellName = "Sniper Shot",
		spellSound = GGD.SND.OggSoundPath .. "spell_aim.ogg",
		override = false,
	},
	[82654] = {
		spellName = "Toxin Shot",
		spellSound = GGD.SND.OggSoundPath .. "spell_hand_launcher.ogg",
		override = false,
	},	
	[109259] = {
		spellName = "Powershot",
		spellSound = GGD.SND.OggSoundPath .. "huge_gun_01_01.ogg",
		override = false,
	},
	[76659] = {
		spellName = "Mastery Auto Attack",
		spellSound = GGD.SND.OggSoundPath .. "spell_autoshot_sq.ogg",
		override = false,
	},
	[19386] = {
		spellName = "Wyvern Sting",
		spellSound = GGD.SND.OggSoundPath .. "spell_wyvern_sting.ogg",
		override = false
	},
	[109248] = {
		spellName = "Binding Shot",
		spellSound = GGD.SND.OggSoundPath .. "spell_binding.ogg",
		override = false,
	},
	[187650] = {
		spellName = "Freezing Trap Launch",
		spellSound = GGD.SND.OggSoundPath .. "spell_binding.ogg",
		override = false,
	},
	[236776] = {
		spellName = "Explosive Trap Launch",
		spellSound = GGD.SND.OggSoundPath .. "spell_binding.ogg",
		override = false,
	},
	[82941] = {
		spellName = "Ice Trap Launch",
		spellSound = GGD.SND.OggSoundPath .. "spell_binding.ogg",
		override = false,
	},
	[60192] = {
		spellName = "Ice Trap Launch",
		spellSound = GGD.SND.OggSoundPath .. "spell_binding.ogg",
		override = false,
	},
	[187698] = {
		spellName = "Tar Trap Launch",
		spellSound = GGD.SND.OggSoundPath .. "spell_binding.ogg",
		override = false,
	},
	[116858] = {
		spellName = "Chaos Bolt",
		spellSound = GGD.SND.OggSoundPath .. "spell_chaos_bolt.ogg",
		override = false,
	},
	[198670] = {
		spellName = "Piercing Shot",
		spellSound = GGD.SND.Mp3SoundPath .. "Build_exp.mp3",
		override = false,
	},
	[203155] = { -- Legion Pvp Talent
		spellName = "Sniper Shot",
		spellSound = GGD.SND.OggSoundPath .. "f_000057_0912.ogg",
		override = false,
	},
	[257044] = {
		spellName = "Rapid Fire(Start)",
		spellSound = GGD.SND.OggSoundPath .. "Shot00_01.ogg",
		override = false,
	},
	[257045] = {
		spellName = "Rapid Fire(Channel)",
		spellSound = GGD.SND.OggSoundPath .. "Shot00_01.ogg",
		override = false,
	},
	[186387] = { 
		spellName = "Bursting Shot",
		spellSound = GGD.SND.Mp3SoundPath .. "m3-1.mp3",
		override = false,
	},
	[185901] = {
		spellName = "Marked Shot",
		spellSound = GGD.SND.Mp3SoundPath .. "xm1014-1.mp3",
		override = false,
	},
	[120360] = { -- пока не нужен
		spellName = "Barrage Start",
		spellSound = "",
		override = false,
	},
	[120361] = {
		spellName = "Barrage Shell",
		spellSound = GGD.SND.Mp3SoundPath .. "Shot00_01.mp3",
		override = false,
	},
	[199804] = { -- Outlaw Rogue Legion Talent
		spellName = "Between The Eyes",
		spellSound = GGD.SND.Mp3SoundPath .. "Shot06_01.mp3",
		override = false,
	},
	[185763] = { -- Outlaw Rogue Legion Talent
		spellName = "Pistol Shot",
		spellSound = GGD.SND.Mp3SoundPath .. "Shot00_01.mp3",
		override = false,
	},
	[421] = {
		spellName = "Chain Lightning Restoration",
		spellSound = GGD.SND.Mp3SoundPath .. "Shot09_01.mp3",
		override = false,
	},
	[188443] = {
		spellName = "Chain Lightning Elemental",
		spellSound = GGD.SND.Mp3SoundPath .. "Shot09_01.mp3",
		override = false,
	},
	[204147] = { -- Legion Artifact Ability
		spellName = "Spell Windburst",
		spellSound = GGD.SND.OggSoundPath .. "large_gun_01_01.ogg",
		override = false,
	},
	[194599] = {
		spellName = "Black Arrow",
		spellSound = GGD.SND.Mp3SoundPath .. "deagle-1_03.mp3",
		override = false,
	},
	[212431] = { -- Rocket Launch
		spellName = "Explosive Shot",
		spellSound = GGD.SND.Mp3SoundPath .. "Shot03_01.mp3",
		override = false,
	},
	[212679] = { -- Rocket Explode
		spellName = "Exposive Shot Detonate",
		spellSound = GGD.SND.Mp3SoundPath .. "boom2_e.mp3",
		override = false,
	},
	[1978] = { 
		spellName = "Serpent Sting",	-- Panda
		spellSound = GGD.SND.OggSoundPath .. "spell_sting.ogg",
		override = false,
	},
	[202797] = { 
		spellName = "Viper Sting",
		spellSound = GGD.SND.OggSoundPath .. "spell_sting.ogg",
		override = false,
	},
	[3034] = { 
		spellName = "Viper Sting",
		spellSound = GGD.SND.OggSoundPath .. "spell_sting.ogg",
		override = false,
	},
	[202914] = {
		spellName = "Spider Sting",
		spellSound = GGD.SND.OggSoundPath .. "spell_sting.ogg",
		override = false,
	},
	[202900] = {
		spellName = "Scorpid Sting",
		spellSound = GGD.SND.OggSoundPath .. "spell_sting.ogg",
		override = false,
	},
	[3043] = {
		spellName = "Scorpid Sting",
		spellSound = GGD.SND.OggSoundPath .. "spell_sting.ogg",
		override = false,
	},
};

This.md_reversedSpellSoundMap = {};