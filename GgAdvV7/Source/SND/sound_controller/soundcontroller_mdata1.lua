local This = GGF.ModuleGetWrapper("SND-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.3 (MDT Check)
function This:MS_CHECK_MDT_1()
	return 1;
end


-------------------------------------------------------------------------------
-- "multitudinous data" block
-------------------------------------------------------------------------------


This.md_hitMap = {
	huge = {
		GGD.SND.Mp3SoundPath .. "ap_armor_pierce_huge_01.mp3",
		GGD.SND.Mp3SoundPath .. "ap_armor_pierce_huge_02.mp3",
		GGD.SND.Mp3SoundPath .. "ap_armor_pierce_huge_03.mp3",
	},
	large = {
		GGD.SND.Mp3SoundPath .. "ap_armor_pierce_large_01.mp3",
		GGD.SND.Mp3SoundPath .. "ap_armor_pierce_large_02.mp3",
		GGD.SND.Mp3SoundPath .. "ap_armor_pierce_large_03.mp3",
	},
	main = {
		GGD.SND.OggSoundPath .. "ap_armor_pierce_main_01_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_pierce_main_02_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_pierce_main_03_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_pierce_main_04_01.ogg",
	},
	medium = {
		GGD.SND.OggSoundPath .. "ap_armor_pierce_medium_01_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_pierce_medium_02_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_pierce_medium_03_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_pierce_medium_04_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_pierce_medium_05_01.ogg",
	},
	small = {
		GGD.SND.OggSoundPath .. "ap_armor_pierce_small_01_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_pierce_small_02_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_pierce_small_03_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_pierce_small_04_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_pierce_small_05_01.ogg",
	},
	automatic = {
		GGD.SND.OggSoundPath .. "ap_armor_pierce_automatic_01_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_pierce_automatic_02_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_pierce_automatic_03_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_pierce_automatic_04_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_pierce_automatic_05_01.ogg",
	},
};


This.md_ricochetMap = {
	huge = {
		GGD.SND.Mp3SoundPath .. "ap_ricochet_huge_01.mp3",
		GGD.SND.Mp3SoundPath .. "ap_ricochet_huge_02.mp3",
		GGD.SND.Mp3SoundPath .. "ap_ricochet_huge_03.mp3",
	},
	large = {
		GGD.SND.Mp3SoundPath .. "ap_ricochet_large_01.mp3",
		GGD.SND.Mp3SoundPath .. "ap_ricochet_large_02.mp3",
		GGD.SND.Mp3SoundPath .. "ap_ricochet_large_03.mp3",
	},
	main = {
		GGD.SND.OggSoundPath .. "ap_ricochet_main_01_01.ogg",
		GGD.SND.OggSoundPath .. "ap_ricochet_main_02_01.ogg",
		GGD.SND.OggSoundPath .. "ap_ricochet_main_03_01.ogg",
	},
	medium = {
		GGD.SND.OggSoundPath .. "ap_ricochet_medium_01_01.ogg",
		GGD.SND.OggSoundPath .. "ap_ricochet_medium_02_01.ogg",
		GGD.SND.OggSoundPath .. "ap_ricochet_medium_03_01.ogg",
	},
	small = {
		GGD.SND.OggSoundPath .. "ap_ricochet_small_01_01.ogg",
		GGD.SND.OggSoundPath .. "ap_ricochet_small_02_01.ogg",
		GGD.SND.OggSoundPath .. "ap_ricochet_small_03_01.ogg",
	},
	automatic = {
		GGD.SND.OggSoundPath .. "ap_ricochet_automaric_01_01.ogg",
		GGD.SND.OggSoundPath .. "ap_ricochet_automatic_02_01.ogg",
		GGD.SND.OggSoundPath .. "ap_ricochet_automatic_03_01.ogg",
		GGD.SND.OggSoundPath .. "ap_ricochet_automatic_04_01.ogg",
		GGD.SND.OggSoundPath .. "ap_ricochet_automatic_05_01.ogg",
	},
};


This.md_nhitMap = {
	huge = {
		GGD.SND.Mp3SoundPath .. "ap_armor_not_pierce_huge_01.mp3",
		GGD.SND.Mp3SoundPath .. "ap_armor_not_pierce_huge_02.mp3",
		GGD.SND.Mp3SoundPath .. "ap_armor_not_pierce_huge_03.mp3",
	},
	large = {
		GGD.SND.Mp3SoundPath .. "ap_armor_not_pierce_large_01.mp3",
		GGD.SND.Mp3SoundPath .. "ap_armor_not_pierce_large_02.mp3",
		GGD.SND.Mp3SoundPath .. "ap_armor_not_pierce_large_03.mp3",
	},
	main = {
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_main_01_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_main_02_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_main_03_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_main_04_01.ogg",
	},
	medium = {
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_medium_01_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_medium_02_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_medium_03_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_medium_04_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_medium_05_01.ogg",
	},
	small = {
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_small_01_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_small_02_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_small_03_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_small_04_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_small_05_01.ogg",
	},
	automatic = {
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_automatic_01_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_automatic_02_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_automatic_03_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_automatic_04_01.ogg",
		GGD.SND.OggSoundPath .. "ap_armor_not_pierce_automatic_05_01.ogg",
	},
};