----------------------------------------------------------------------------
-- "functional" block
----------------------------------------------------------------------------


----------------------------------------------------------------------------
-- "define" block
----------------------------------------------------------------------------

GGM.SND = {};
GGD.SND = {};
GGD.SND.ClusterName = "Sound Native Dispatcher";
GGD.SND.ClusterToken = "SND";
GGD.SND.ClusterVersion = GGD.BuildMeta.Version;
GGD.SND.ClusterBuild = "AT-"..GGD.BuildMeta.Build;
GGD.SND.ClusterMBI = "6102-1052";

GGD.SND.SoundPath = "Interface\\AddOns\\" .. GGD.AddonName .. "\\Sounds\\SND\\";
GGD.SND.OggSoundPath = GGD.SND.SoundPath .. "ogg\\";
GGD.SND.Mp3SoundPath = GGD.SND.SoundPath .. "mp3\\";
GGD.SND.WavSoundPath = GGD.SND.SoundPath .. "wav\\";