-- Инициализация на стадии загрузки аддона
function GGM.SND:OnAddonLoad()
	GGM.COM:RegisterInterComm(GGD.SND.ClusterToken, GGM.SND.EvokeController);

	GGM.IOC.OutputController:AddonWelcomeMessage({
		cluster = GGD.SND.ClusterToken,
		version = GGD.SND.ClusterVersion,
		build = GGD.SND.ClusterBuild,
		mbi = GGD.SND.ClusterMBI,
	});
end