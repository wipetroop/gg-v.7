local function getItemInfo()
	local trinketId0 = GetInventorySlotInfo("Trinket0Slot")
	local trinketId1 = GetInventorySlotInfo("Trinket1Slot")
	local itemId0 = GetInventoryItemID("Player", trinketId0)
	local itemId1 = GetInventoryItemID("Player", trinketId1)
	
	print(itemId0)
	print(IsConsumableItem(itemId0))
	print(IsConsumableItem(itemId1))
end

local function isProcableTrinketId(spellID)
	--if (spellID == 85011 or spellID == 99748 or spellID == 99749 or spellID == 102439 or spellID == 102440 or spellID == 99718 or spellID == 105136 or spellID == 126707 or spellID == 182059 or spellID == 182060 or spellID == 99717 or spellID == 182068 or spellID == 92221 or spellID == 182072 or spellID == 92220 or spellID == 190026 or spellID == 85022 or spellID == 190028 or spellID == 105135) then
		--return true
	--end
	return false
end

local function isUsableTrinketId(spellID)
	--if (spellID == 85011 or spellID == 99748 or spellID == 99749 or spellID == 102439 or spellID == 102440 or spellID == 99718 or spellID == 105136 or spellID == 126707 or spellID == 182059 or spellID == 182060 or spellID == 99717 or spellID == 182068 or spellID == 92221 or spellID == 182072 or spellID == 92220 or spellID == 190026 or spellID == 85022 or spellID == 190028 or spellID == 105135) then
		--return true
	--end
	return false
end

local function isBloodLustId(spellID)
	--if (spellID == 85011 or spellID == 99748 or spellID == 99749 or spellID == 102439 or spellID == 102440 or spellID == 99718 or spellID == 105136 or spellID == 126707 or spellID == 182059 or spellID == 182060 or spellID == 99717 or spellID == 182068 or spellID == 92221 or spellID == 182072 or spellID == 92220 or spellID == 190026 or spellID == 85022 or spellID == 190028 or spellID == 105135) then
		--return true
	--end
	return false
end

local function isUnderUsableTrinket()
	--if (not(UnitAura("Player", 170397) == nil or UnitAura("Player", 182073) == nil)) then
		--return true
	--end
	return false
end

local function isUnderProcableTrinket()
	
	--for i,k in pairs(procArray) do		
		--if (not(UnitAura("Player", k) == nil)) then
			--return true
		--end
	--end
	return false
end

local function isUnderBloodLust()
	if (not(UnitAura("Player", 2825) == nil)) then
		return true
	end
	return false
end





















function table.val_to_str ( v )
  if "string" == type( v ) then
    v = string.gsub( v, "\n", "\\n" )
    if string.match( string.gsub(v,"[^'\"]",""), '^"+$' ) then
      return "'" .. v .. "'"
    end
    return '"' .. string.gsub(v,'"', '\\"' ) .. '"'
  else
    return "table" == type( v ) and table.tostring( v ) or
      tostring( v )
  end
end

function table.key_to_str ( k )
  if "string" == type( k ) and string.match( k, "^[_%a][_%a%d]*$" ) then
    return k
  else
    return "[" .. table.val_to_str( k ) .. "]"
  end
end

function table.tostring( tbl )
  local result, done = {}, {}
  for k, v in ipairs( tbl ) do
    table.insert( result, table.val_to_str( v ) )
    done[ k ] = true
  end
  for k, v in pairs( tbl ) do
    if not done[ k ] then
      table.insert( result,
        table.key_to_str( k ) .. "=" .. table.val_to_str( v ) )
    end
  end
  return "{" .. table.concat( result, "," ) .. "}"
end
