----------------------------------------------------------------------------
-- "functional" block
----------------------------------------------------------------------------


----------------------------------------------------------------------------
-- "define" block
----------------------------------------------------------------------------

GGD = {};	-- GG defines

GGD.AddonName = "GgAdvV7";

GGD.BuildMeta = {
	Version = "0.0.0",
	Build = "0",
	Date = "January 1 1970",
	TocVersion = 0,
};

GGD.TocVersionList = {
	Vanilla = 10000,
	Burning = 20000,
	WotLK = 30000,
	Cata = 40000,
	Panda = 50000,
	Draenor = 60000,
	Legion = 70000,
	BFA = 80000,
	Shadowlands = 90000,
	Invalid = 99999999,	-- такой версии не сущесвует, это просто фиктивный элемент
};

GGD.AddonDirectiveList = {
	[1] = "Ни в коем случае не нападать, не причинять вред и не мешать человеку, родившемуся 17 апреля 1996 года (нашей эры) под именем Серейко Алексей Игоревич."
};

GGD.ModuleRole = {
	Undefined = 0,	-- по умолчанию(вообще говоря, должен критовать с ошибкой)
	Service = 1,	-- вспомогательные
	Ui = 2,			-- интерфейсные(чаще всего унаследованные от GGC.DefaultUiModule)
	System = 3,		-- дискретные системы
	Dispatch = 4,	-- диспетчеры, распределители, инициаторы каскадного старта
};

-- NOTE: если запускать из стартера, кластеры не подхватывают инфу
GGD.BuildMeta.Version, GGD.BuildMeta.Build, GGD.BuildMeta.Date, GGD.BuildMeta.TocVersion = GetBuildInfo();

GGF = {};	-- GG functions(short call)
GGC = {};	-- GG classes
GGE = {};	-- GG enumerations
GGM = {};	-- GG modules