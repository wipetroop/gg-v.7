local addonLoad = CreateFrame("FRAME");

addonLoad:RegisterEvent("ADDON_LOADED");

-- Инициализация на стадии загрузки аддона
local function onAddonLoad(self, event, arg1)
	if (arg1 ~= GGD.AddonName) then
		return;
	end

	GGM.COM:OnAddonLoad();
	GGM.ECP:OnAddonLoad();
	GGM.IOC:OnAddonLoad();
	GGM.FCS:OnAddonLoad();
	GGM.SND:OnAddonLoad();
	GGM.ACR:OnAddonLoad();
	GGM.ASB:OnAddonLoad();
	GGM.GUI:OnAddonLoad();
end

addonLoad:SetScript("OnEvent", onAddonLoad);