----------------------------------------------------------------------------
-- "functional" block
----------------------------------------------------------------------------


-- Взятие всех данных по модулю
function GGF.GetGlobalModuleData(in_gToken)
   if (GGD.FastModuleLinkedList[in_gToken] ~= nil) then
      return GGD.FastModuleLinkedList[in_gToken];
   end

   local current = GGD.ModuleList;
   local cluster = "";
   for shard in string.gmatch(in_gToken, "%w+") do
      local location = current.modules;
      if (current == GGD.ModuleList) then
         location = current;
      end
      if (location ~= nil and location[shard] ~= nil) then
         current = location[shard];
      else
         print("no token:", shard);
         return nil;
      end
   end
   return current;
end

-- Взятие глобального имени модуля
function GGF.GetGlobalModuleName(in_gToken)
   return GGF.GetGlobalModuleData(in_gToken).name;
end

-- Взятие списка элеметнов модуля(каркас)
function GGF.GetGlobalModuleChassis(in_gToken)
   return GGF.GetGlobalModuleData(in_gToken).chassis;
end

-- Взятие каскада дочерних объектов модуля
function GGF.GetGlobalModuleCascade(in_gToken)
   return GGF.GetGlobalModuleData(in_gToken).cascade;
end

-- Взятие объектного каскада дочерних объектов модуля
function GGF.GetGlobalModuleCascadeObj(in_gToken)
   return GGF.GetGlobalModuleData(in_gToken).cascade_obj;
end

-- Взятие роли модуля
function GGF.GetGlobalModuleRole(in_gToken)
   return GGF.GetGlobalModuleData(in_gToken).role;
end

-- Взятие роли модуля
function GGF.GetGlobalModulePrototype(in_gToken)
   return GGF.GetGlobalModuleData(in_gToken).prototype();
end

-- Взятие токена кластера модуля
function GGF.GetGlobalModuleClusterToken(in_gToken)
   local current = GGD.ModuleList;
   local cluster = "";
   for shard in string.gmatch(in_gToken, "%w+") do
      local location = current.modules;
      if (current == GGD.ModuleList) then
         location = current;
         return shard;
      end
   end
   return nil;
end

-- Обертка создания произвольного модуля
function GGF.ModuleCreateWrapper(in_gToken, in_needTimer, in_config)
   ATLASSERT(in_needTimer == true or in_needTimer == false);
   local MODULE_NAME = GGF.GetGlobalModuleName(in_gToken);
   local OBJECT_NAME = GGF.TrimSpace(MODULE_NAME);
   local CLUSTER = GGF.GetGlobalModuleClusterToken(in_gToken);
   local PROTOTYPE = GGF.GetGlobalModulePrototype(in_gToken);
   if (PROTOTYPE == nil) then
      print("no proto:", in_gToken);
   end
   GGM[CLUSTER][OBJECT_NAME] = PROTOTYPE:Create({
      timer = in_needTimer,
      clustertoken = GGD[CLUSTER].ClusterToken,
      modulename = {
         standard = MODULE_NAME,
         object = OBJECT_NAME,
      },
      modulegtoken = in_gToken,
      modulerole = GGF.GetGlobalModuleRole(in_gToken),
      chassis = GGF.GetGlobalModuleChassis(in_gToken),
      cascade = GGF.GetGlobalModuleCascade(in_gToken),
      -- HACK: гонка времени...надо фиксить...
      config = in_config,
   });
   --print("create", MODULE_NAME);
   GGD.FastModuleList[in_gToken] = GGM[CLUSTER][OBJECT_NAME];
   GGD.FastModuleLinkedList[in_gToken] = GGF.GetGlobalModuleData(in_gToken);

   return GGM[CLUSTER][OBJECT_NAME];
end


-- Обертка взятия произвольного модуля
function GGF.ModuleGetWrapper(in_gToken)
   --local MODULE_NAME = GGF.GetGlobalModuleName(in_gToken);
   --local OBJECT_NAME = GGF.TrimSpace(MODULE_NAME);
   --local CLUSTER = GGF.GetGlobalModuleClusterToken(in_gToken);
   if (GGD.FastModuleList[in_gToken] == nil) then
      --print("apparent module get error:", in_gToken);
      ATLASSERT(false);
   end
   -- HACK: через линейку быстрее, хоть и зажор памяти увеличится
   return GGD.FastModuleList[in_gToken];--GGM[CLUSTER][OBJECT_NAME];
end


-- Подсовывание объектов в список инициализации
function GGF.PrepareModuleList()
   local current = GGD.ModuleList;
   for idx,cluster in pairs(current) do
      local location = cluster.modules;
      for midx,module in pairs(location) do
         GGF.PrepareModule(module);
      end
   end
end


function count(tbl)
	local cnt = 0;
	for k,v in pairs(tbl) do
		cnt = cnt + 1;
	end
	return cnt;
end


-- Функция рекурсивная
function GGF.PrepareModule(in_module)
	local looplist = {};
	local current = in_module;
	local stidx = 1;
	--print(in_module.name);
	while (current) do
		--print(current.name);
		local prev = current;
		if (current.modules) then
			local cnt = count(current.modules);
			for idx=stidx,cnt do
				local module = current.modules[tostring(idx)];
				looplist[#looplist + 1] = { obj = current, idx = idx };
				current = module;
				stidx = 1;
				break;
			end
		end
		if (current.cascade and not current.cascade_obj) then
			-- NOTE: костыльная инициализация
			current.cascade_obj = {};
			for idx,token in ipairs(current.cascade) do
				if (GGF.ModuleGetWrapper(token) == nil) then
					-- TODO: прицепить нормальный обработчик ошибки
					print("apparent module error:", token);
				end
				current.cascade_obj[#current.cascade_obj + 1] = GGF.ModuleGetWrapper(token);
			end
			--print(current.name, #current.cascade_obj);
		end
		if (prev == current) then
			if (#looplist ~= 0) then
				current = looplist[#looplist].obj;
				stidx = looplist[#looplist].idx + 1;
				looplist[#looplist] = nil;
			else
				current = nil;
			end
		end
	end

	--print("cs", GGF.GetGlobalModuleData("COM-2").cascade, GGF.GetGlobalModuleData("COM-2").cascade_obj);
	--print("cs", GGF.GetGlobalModuleData("ACR-4-1-1").cascade, GGF.GetGlobalModuleData("ACR-4-1-1").cascade_obj);
end


----------------------------------------------------------------------------
-- "define" block
----------------------------------------------------------------------------

-- NOTE: карта соответствует расположению файлов
-- WARNING: порядок загрузки совсем другой!!!
GGD.ModuleList = {
	ACR = {
		modules = {
			["1"] = {	-- ACR-1
				name = "Combat Information",
				role = GGD.ModuleRole.Service,
				prototype = function() return GGC.DefaultUiModule; end,
				chassis = { API = 1, FNC = 1, INT = 1, MDT = 1 },
			},
			["2"] = {	-- ACR-2
				name = "Common Service",
				role = GGD.ModuleRole.Service,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
			},
			["3"] = {	-- ACR-3
				name = "Evoke Controller",
				role = GGD.ModuleRole.Dispatch,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
				cascade = { "ACR-2", "ACR-4", "ACR-5", "ACR-6", "ACR-1", "ACR-7" },
			},
			["4"] = {	-- ACR-4
				name = "Runtime Interceptor",
				role = GGD.ModuleRole.Dispatch,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
				cascade = { "ACR-4-1" },
				modules = {
					["1"] = {	-- ACR-4-1
						name = "Combat Interceptor",
						role = GGD.ModuleRole.Dispatch,
						prototype = function() return GGC.BaseModule; end,
						chassis = { API = 1, FNC = 1, INT = 1, MDT = 1 },
						cascade = { "ACR-4-1-1", "ACR-4-1-2", "ACR-4-1-3", "ACR-4-1-4" },
						modules = {
							["1"] = {	-- ACR-4-1-1
								name = "Apparent Interceptor",
								role = GGD.ModuleRole.Dispatch,
								prototype = function() return GGC.BaseModule; end,
								chassis = { API = 1, FNC = 1, INT = 1 },
								cascade = { "ACR-4-1-1-1", "ACR-4-1-1-2", "ACR-4-1-1-3", "ACR-4-1-1-4", "ACR-4-1-1-5", "ACR-4-1-1-6", "ACR-4-1-1-7", "ACR-4-1-1-8", "ACR-4-1-1-9", "ACR-4-1-1-10", "ACR-4-1-1-11", "ACR-4-1-1-12", "ACR-4-1-1-13", "ACR-4-1-1-14", "ACR-4-1-1-15", "ACR-4-1-1-16" },
								modules = {
									["1"] = {	-- ACR-4-1-1-1
										name = "Aura Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["2"] = {	-- ACR-4-1-1-2
										name = "Brake Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["3"] = {	-- ACR-4-1-1-3
										name = "Cast Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["4"] = {	-- ACR-4-1-1-4
										name = "Create Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["5"] = {	-- ACR-4-1-1-5
										name = "Damage Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["6"] = {	-- ACR-4-1-1-6
										name = "Death Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["7"] = {	-- ACR-4-1-1-7
										name = "Dispel Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["8"] = {	-- ACR-4-1-1-8
										name = "Drain Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["9"] = {	-- ACR-4-1-1-9
										name = "Enchant Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["10"] = {	-- ACR-4-1-1-10
										name = "Energize Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["11"] = {	-- ACR-4-1-1-11
										name = "Heal Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["12"] = {	-- ACR-4-1-1-12
										name = "Interrupt Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["13"] = {	-- ACR-4-1-1-13
										name = "Leech Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["14"] = {	-- ACR-4-1-1-14
										name = "Miss Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["15"] = {	-- ACR-4-1-1-15
										name = "Steal Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["16"] = {	-- ACR-4-1-1-16
										name = "Summon Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
								},
							},
							["2"] = {	-- ACR-4-1-2
								name = "Complex Interceptor",
								role = GGD.ModuleRole.Dispatch,
								prototype = function() return GGC.BaseModule; end,
								chassis = { API = 1, FNC = 1, INT = 1 },
								cascade = { "ACR-4-1-2-1", "ACR-4-1-2-2", "ACR-4-1-2-3" },
								modules = {
									["1"] = {	-- ACR-4-1-2-1
										name = "Control Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["2"] = {	-- ACR-4-1-2-2
										name = "Kill Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["3"] = {	-- ACR-4-1-2-3
										name = "Reflect Interceptor",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.InterceptorModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
								},
							},
							["3"] = {	-- ACR-4-1-3
								name = "Emote Announcer",
								role = GGD.ModuleRole.System,
								prototype = function() return GGC.BaseModule; end,
								chassis = { API = 1, FNC = 1, INT = 1 },
							},
							["4"] = {	-- ACR-4-1-4
								name = "Event Interceptor",
								role = GGD.ModuleRole.System,
								prototype = function() return GGC.BaseModule; end,
								chassis = { API = 1, FNC = 1, INT = 1 },
							},
						},
					},
				},
			},
			["5"] = {	-- ACR-5
				name = "Runtime Logger",
				role = GGD.ModuleRole.Dispatch,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1, MDT = 1 },
				cascade = { "ACR-5-1" },
				modules = {
					["1"] = {	-- ACR-5-1
						name = "Combat Logger",
						role = GGD.ModuleRole.Dispatch,
						prototype = function() return GGC.BaseModule; end,
						chassis = { API = 1, FNC = 1, INT = 1 },
						cascade = { "ACR-5-1-1", "ACR-5-1-2" },
						modules = {
							["1"] = {	-- ACR-5-1-1
								name = "Apparent Logger",
								role = GGD.ModuleRole.Dispatch,
								prototype = function() return GGC.BaseModule; end,
								chassis = { API = 1, FNC = 1, INT = 1 },
								cascade = { "ACR-5-1-1-1", "ACR-5-1-1-2", "ACR-5-1-1-3", "ACR-5-1-1-4", "ACR-5-1-1-5", "ACR-5-1-1-6", "ACR-5-1-1-7", "ACR-5-1-1-8", "ACR-5-1-1-9", "ACR-5-1-1-10", "ACR-5-1-1-11", "ACR-5-1-1-12", "ACR-5-1-1-13", "ACR-5-1-1-14", "ACR-5-1-1-15", "ACR-5-1-1-16", "ACR-5-1-1-17" },
								modules = {
									["1"] = {	-- ACR-5-1-1-1
										name = "Aura Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["2"] = {	-- ACR-5-1-1-2
										name = "Brake Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["3"] = {	-- ACR-5-1-1-3
										name = "Cast Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["4"] = {	-- ACR-5-1-1-4
										name = "Create Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["5"] = {	-- ACR-5-1-1-5
										name = "Damage Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["6"] = {	-- ACR-5-1-1-6
										name = "Death Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["7"] = {	-- ACR-5-1-1-7
										name = "Dispel Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["8"] = {	-- ACR-5-1-1-8
										name = "Drain Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["9"] = {	-- ACR-5-1-1-9
										name = "Enchant Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["10"] = {	-- ACR-5-1-1-10
										name = "Energize Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["11"] = {	-- ACR-5-1-1-11
										name = "Heal Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["12"] = {	-- ACR-5-1-1-12
										name = "Interrupt Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["13"] = {	-- ACR-5-1-1-13
										name = "Leech Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["14"] = {	-- ACR-5-1-1-14
										name = "Miss Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["15"] = {	-- ACR-5-1-1-15
										name = "Steal Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["16"] = {	-- ACR-5-1-1-16
										name = "Logger Structure Assistant",
										role = GGD.ModuleRole.Service,
										prototype = function() return GGC.BaseModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["17"] = {	-- ACR-5-1-1-17
										name = "Summon Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
								},
							},
							["2"] = {	-- ACR-5-1-2
								name = "Complex Logger",
								role = GGD.ModuleRole.Dispatch,
								prototype = function() return GGC.BaseModule; end,
								chassis = { API = 1, FNC = 1, INT = 1 },
								cascade = { "ACR-5-1-2-1", "ACR-5-1-2-2", "ACR-5-1-2-3" },
								modules = {
									["1"] = {	-- ACR-5-1-2-1
										name = "Control Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["2"] = {	-- ACR-5-1-2-2
										name = "Kill Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
									["3"] = {	-- ACR-5-1-2-3
										name = "Reflect Logger",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.LoggerModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 1},
									},
								},
							},
						},
					},
				},
			},
			["6"] = {	-- ACR-6
				name = "Runtime Statistic",
				role = GGD.ModuleRole.Dispatch,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
				cascade = { "ACR-6-1" },
				modules = {
					["1"] = {	-- ACR-6-1
						name = "Combat Statistic",
						role = GGD.ModuleRole.Dispatch,
						prototype = function() return GGC.BaseModule; end,
						chassis = { API = 1, FNC = 1, INT = 1 },
						cascade = { "ACR-6-1-1", "ACR-6-1-2" },
						modules = {
							["1"] = {	-- ACR-6-1-1
								name = "Apparent Statistic",
								role = GGD.ModuleRole.Dispatch,
								prototype = function() return GGC.BaseModule; end,
								chassis = { API = 1, FNC = 1, INT = 1 },
								cascade = { "ACR-6-1-1-1", "ACR-6-1-1-2", "ACR-6-1-1-3", "ACR-6-1-1-4", "ACR-6-1-1-5", "ACR-6-1-1-6", "ACR-6-1-1-7", "ACR-6-1-1-8", "ACR-6-1-1-9", "ACR-6-1-1-10", "ACR-6-1-1-11", "ACR-6-1-1-12", "ACR-6-1-1-13", "ACR-6-1-1-14", "ACR-6-1-1-15", "ACR-6-1-1-16", "ACR-6-1-1-17" },
								modules = {
									["1"] = {	-- ACR-6-1-1-1
										name = "Aura Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["2"] = {	-- ACR-6-1-1-2
										name = "Brake Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["3"] = {	-- ACR-6-1-1-3
										name = "Cast Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["4"] = {	-- ACR-6-1-1-4
										name = "Create Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["5"] = {	-- ACR-6-1-1-5
										name = "Damage Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["6"] = {	-- ACR-6-1-1-6
										name = "Death Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["7"] = {	-- ACR-6-1-1-7
										name = "Dispel Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["8"] = {	-- ACR-6-1-1-8
										name = "Drain Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["9"] = {	-- ACR-6-1-1-9
										name = "Enchant Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["10"] = {	-- ACR-6-1-1-10
										name = "Energize Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["11"] = {	-- ACR-6-1-1-11
										name = "Heal Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["12"] = {	-- ACR-6-1-1-12
										name = "Interrupt Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["13"] = {	-- ACR-6-1-1-13
										name = "Leech Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["14"] = {	-- ACR-6-1-1-14
										name = "Miss Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["15"] = {	-- ACR-6-1-1-15
										name = "Steal Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["16"] = {	-- ACR-6-1-1-16
										name = "Statistic Structure Assistant",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, MDT = 0 },
									},
									["17"] = {	-- ACR-6-1-1-17
										name = "Summon Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
								},
							},
							["2"] = {	-- ACR-6-1-2
								name = "Complex Statistic",
								role = GGD.ModuleRole.Dispatch,
								prototype = function() return GGC.BaseModule; end,
								chassis = { API = 1, FNC = 1, INT = 1 },
								cascade = { "ACR-6-1-2-1", "ACR-6-1-2-2", "ACR-6-1-2-3" },
								modules = {
									["1"] = {	-- ACR-6-1-2-1
										name = "Control Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["2"] = {	-- ACR-6-1-2-2
										name = "Kill Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
									["3"] = {	-- ACR-6-1-2-3
										name = "Reflect Statistic",
										role = GGD.ModuleRole.System,
										prototype = function() return GGC.StatisticModule; end,
										chassis = { API = 1, FNC = 1, INT = 1 },
									},
								},
							},
						},
					},
				},
			},
			["7"] = {	-- ACR-7
				name = "Spec Dispatcher",
				role = GGD.ModuleRole.System,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1, MDT = 1 },
			},
		},
	},
	ASB = {
		modules = {
			["1"] = {	-- ASB-1
				name = "Drivecontrol System",
				role = GGD.ModuleRole.Dispatch,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
				cascade = { "ASB-1-1", "ASB-1-2", "ASB-1-3", "ASB-1-4" },
				modules = {
					["1"] = {	-- ASB-1-1
						-- Death by Drowning Early Warning System(DDEWS)
						-- Система Раннего Предупреждения Смерти от Потопления(СРПСП)
						name = "DDEWS Block",
						role = GGD.ModuleRole.System,
						prototype = function() return GGC.BaseModule; end,
						chassis = { API = 1, FNC = 1, INT = 1 },
					},
					["2"] = {	-- ASB-1-2
						-- Death by Fatigue Early Warning System(DFEWS)
						-- Система Раннего Предупреждения Смерти от Усталости(СРПСУ)
						name = "DFEWS Block",
						role = GGD.ModuleRole.System,
						prototype = function() return GGC.BaseModule; end,
						chassis = { API = 1, FNC = 1, INT = 1 },
					},
					["3"] = {	-- ASB-1-3
						-- Player Movement Proactive Control System(PMPCS)
						-- Система Проактивного Контроля Передвижения Игрока(СПКПИ)
						-- NOTE: полная индикация типов/видов/характера передвижения
						-- Стейты:
						--		SWS(swimState)
						--		FS(flyState)
						--		MNS(mountState)
						--		MVS(movementState)
						--		STS(stealthState)
						--		TS(taxiState)
						name = "PMPCS Block",
						role = GGD.ModuleRole.System,
						prototype = function() return GGC.BaseModule; end,
						chassis = { API = 1, FNC = 1, INT = 1 },
						cascade = { "ASB-1-3-1", "ASB-1-3-2" },
						modules = {
							["1"] = {	-- ASB-1-3-1
								-- Active Cruise Control Indicator System(ACCIS)
								-- Система Индикации Активного Круиз Контроля(СИАКК)
								name = "ACCIS Block",
								role = GGD.ModuleRole.System,
								prototype = function() return GGC.BaseModule; end,
								chassis = { API = 1, FNC = 1, INT = 1 },
							},
							["2"] = {	-- ASB-1-3-2
								-- Obstacle Collision Momentary Warning System(OCMWS)
								-- Система Моментного Предупреждения Столкновения с Препятствием(СМПСП)
								name = "OCMWS Block",
								role = GGD.ModuleRole.System,
								prototype = function() return GGC.BaseModule; end,
								chassis = { API = 1, FNC = 1, INT = 1 },
							},
						},
					},
					["4"] = {	-- ASB-1-4
						-- Terrain Collision Early Warning System(TCEWS)
						-- Система Раннего Предупреждения Столкновения с Землей(СРПСЗ)
						-- NOTE: только для случаев падения с высоты(впечататься в стену на флае не считается)
						-- Чеки:
						--		FLAR(fallAlertRed)
						--		FLAY(fallAlertYellow)
						name = "TCEWS Block",
						role = GGD.ModuleRole.System,
						prototype = function() return GGC.BaseModule; end,
						chassis = { API = 1, FNC = 1, INT = 1 },
					},
				},
			},
			["2"] = {	-- ASB-2
				name = "Evoke Controller",
				role = GGD.ModuleRole.Dispatch,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
				cascade = { "ASB-1", "ASB-3" },
			},
			["3"] = {	-- ASB-3
				name = "Navigation System",
				role = GGD.ModuleRole.Dispatch,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
				cascade = { "ASB-3-1" },
				modules = {
					["1"] = {	-- ASB-3-1
						name = "Geolocation Block",
						role = GGD.ModuleRole.System,
						prototype = function() return GGC.BaseModule; end,
						chassis = { API = 1, FNC = 1, INT = 1, MDT = 1 },
					},
				},
			},
		},
	},
	COM = {
		modules = {
			["1"] = {	-- COM-1
				name = "Common Service",
				role = GGD.ModuleRole.Service,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1, MDT = 1 },
			},
			["2"] = {	-- COM-2
				name = "Evoke Controller",
				role = GGD.ModuleRole.Service,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
				cascade = { "COM-1", "COM-3", "COM-4" },
			},
			["3"] = {	-- COM-3
				name = "Module Interrogator",
				role = GGD.ModuleRole.System,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1, MDT = 1 },
			},
			["4"] = {	-- COM-4
				name = "Profiling Tools",
				role = GGD.ModuleRole.Service,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
			},
		},
	},
	ECP = {
		modules = {
			["1"] = {	-- ECP-1
				name = "Evoke Controller",
				role = GGD.ModuleRole.Dispatch,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
			},
		},
	},
	FCS = {
		modules = {
			["1"] = {	-- FCS-1
				name = "Config Controller",
				role = GGD.ModuleRole.System,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
			},
			["2"] = {	-- FCS-2
				name = "Error Handler",
				role = GGD.ModuleRole.System,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1, MDT = 1 },
			},
			["3"] = {	-- FCS-3
				name = "Evoke Controller",
				role = GGD.ModuleRole.Dispatch,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
				cascade = { "FCS-1", "FCS-2", "FCS-4", "FCS-5" },
			},
			["4"] = {	-- FCS-4
				name = "Runtime Storage",
				role = GGD.ModuleRole.System,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
			},
			["5"] = {	-- FCS-5
				name = "Statistic Storage",
				role = GGD.ModuleRole.System,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
			},
		},
	},
	GUI = {
		modules = {
			["1"] = {	-- GUI-1
				name = "Main Dashboard",
				role = GGD.ModuleRole.Ui,
				prototype = function() return GGC.DefaultUiModule; end,
				chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
				cascade = { "GUI-1-1", "GUI-1-2", "GUI-1-3", "GUI-1-4" },
				modules = {
					["1"] = {	-- GUI-1-1
						name = "Combat Instrumentation",
						role = GGD.ModuleRole.Ui,
						prototype = function() return GGC.BaseModule; end,
						chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
					},
					["2"] = {	-- GUI-1-2
						name = "Drivecontrol Instrumentation",
						role = GGD.ModuleRole.Ui,
						prototype = function() return GGC.BaseModule; end,
						chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
					},
					["3"] = {	-- GUI-1-3
						name = "Maintenance Instrumentation",
						role = GGD.ModuleRole.Ui,
						prototype = function() return GGC.BaseModule; end,
						chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
					},
					["4"] = {	-- GUI-1-4
						name = "System Instrumentation",
						role = GGD.ModuleRole.Ui,
						prototype = function() return GGC.BaseModule; end,
						chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
					},
				},
			},
			["2"] = {	-- GUI-2
				name = "Common Service",
				role = GGD.ModuleRole.Service,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
			},
			["3"] = {	-- GUI-3
				name = "Evoke Controller",
				role = GGD.ModuleRole.Dispatch,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
				-- NOTE: если мв(GUI-6) не последний, то кританет настройка слайдеров
				-- TODO: перенести настройку слайдеров
				cascade = { "GUI-7", "GUI-1", "GUI-2", "GUI-4", "GUI-5", "GUI-6" },
			},
			["4"] = {	-- GUI-4
				name = "Kill Feed",
				role = GGD.ModuleRole.Ui,
				prototype = function() return GGC.DefaultUiModule; end,
				chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
				--cascade = { "GUI-4-1", "GUI-4-2", "GUI-4-3", "GUI-4-4", "GUI-4-5" },
				--modules = {
					--["1"] = {	-- GUI-4-1
						--name = "An Line",
						--role = GGD.ModuleRole.System,
						--prototype = function() return GGC.BaseModule; end,
						--chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
					--},
					--["2"] = {	-- GUI-4-2
						--name = "Muzzle Drawer",
						--role = GGD.ModuleRole.System,
						--prototype = function() return GGC.BaseModule; end,
						--chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
					--},
					--["3"] = {	-- GUI-4-3
						--name = "Pet Resolver",
						--role = GGD.ModuleRole.System,
						--prototype = function() return GGC.BaseModule; end,
						--chassis = { API = 1, FNC = 1, INT = 1 },
					--},
					--["4"] = {	-- GUI-4-4
						--name = "Player Cluster",
						--role = GGD.ModuleRole.System,
						--prototype = function() return GGC.BaseModule; end,
						--chassis = { API = 1, FNC = 1, INT = 1 },
					--},
					--["5"] = {	-- GUI-4-5
						--name = "TVP Factory",
						--role = GGD.ModuleRole.System,
						--prototype = function() return GGC.BaseModule; end,
						--chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
					--},
				--},
			},
			["5"] = {	-- GUI-5
				name = "Main Window",
				role = GGD.ModuleRole.Ui,
				prototype = function() return GGC.DefaultUiModule; end,
				chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
				cascade = { "GUI-5-1", "GUI-5-2", "GUI-5-3", "GUI-5-4" },
				modules = {
					["1"] = {	-- GUI-5-1
						name = "Alert Dispatcher",
						role = GGD.ModuleRole.Ui,
						prototype = function() return GGC.DefaultUiModule; end,
						chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
						cascade = { "GUI-5-1-1", "GUI-5-1-2" },
						modules = {
							["1"] = {	-- GUI-5-1-1
								name = "AD Table Engine",
								role = GGD.ModuleRole.Ui,
								prototype = function() return GGC.DefaultUiModule; end,
								chassis = { API = 1, FNC = 1, INT = 1, MDT = 1, OBC = 1 },
							},
							["2"] = {	-- GUI-5-1-2
								name = "AD Graphic Engine",
								role = GGD.ModuleRole.Ui,
								prototype = function() return GGC.DefaultUiModule; end,
								chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
							},
						},
					},
					["2"] = {	-- GUI-5-2
						name = "Control Panel",
						role = GGD.ModuleRole.Ui,
						prototype = function() return GGC.AceUiModule; end,
						chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
						cascade = { "GUI-5-2-1", "GUI-5-2-2", "GUI-5-2-3", "GUI-5-2-4" },
						modules = {
							["1"] = {	-- GUI-5-2-1
								name = "Common Settings",
								role = GGD.ModuleRole.Ui,
								prototype = function() return GGC.AceUiModule; end,
								chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
							},
							["2"] = {	-- GUI-5-2-2
								name = "Effect Manager",
								role = GGD.ModuleRole.Ui,
								prototype = function() return GGC.AceUiModule; end,
								chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
								cascade = { "GUI-5-2-2-1", "GUI-5-2-2-2" },
								modules = {
									["1"] = {	-- GUI-5-2-2-1
										name = "Audio Controller",
										role = GGD.ModuleRole.Ui,
										prototype = function() return GGC.AceUiModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
									},
									["2"] = {	-- GUI-5-2-2-2
										name = "Video Controller",
										role = GGD.ModuleRole.Ui,
										prototype = function() return GGC.AceUiModule; end,
										chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
									},
								},
							},
							["3"] = {	-- GUI-5-2-3
								name = "Hud Geometry",
								role = GGD.ModuleRole.Ui,
								prototype = function() return GGC.AceUiModule; end,
								chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
							},
							["4"] = {	-- GUI-5-2-4
								name = "Logging Manager",
								role = GGD.ModuleRole.Ui,
								prototype = function() return GGC.AceUiModule; end,
								chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
							},
						},
					},
					["3"] = {	-- GUI-5-3
						name = "Data Service",
						role = GGD.ModuleRole.Ui,
						prototype = function() return GGC.AceUiModule; end,
						chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
						cascade = { "GUI-5-3-1", "GUI-5-3-2", "GUI-5-3-3", "GUI-5-3-4", "GUI-5-3-5" },
						modules = {
							["1"] = {	-- GUI-5-3-1
								name = "Chat Report",
								role = GGD.ModuleRole.Ui,
								prototype = function() return GGC.DefaultUiModule; end,
								chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
							},
							["2"] = {	-- GUI-5-3-2
								name = "Data Filter",
								role = GGD.ModuleRole.Ui,
								prototype = function() return GGC.DefaultUiModule; end,
								chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
							},
							["3"] = {	-- GUI-5-3-3
								name = "DS Graphic Engine",
								role = GGD.ModuleRole.Ui,
								prototype = function() return GGC.DefaultUiModule; end,
								chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
							},
							["4"] = {	-- GUI-5-3-4
								name = "Log Filter",
								role = GGD.ModuleRole.Ui,
								prototype = function() return GGC.DefaultUiModule; end,
								chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
							},
							["5"] = {	-- GUI-5-3-5
								name = "DS Table Engine",
								role = GGD.ModuleRole.Ui,
								prototype = function() return GGC.DefaultUiModule; end,
								chassis = { API = 1, FNC = 1, INT = 1, MDT = 1, OBC = 1 },
							},
						},
					},
					["4"] = {	-- GUI-5-4
						name = "Information Panel",
						role = GGD.ModuleRole.Ui,
						prototype = function() return GGC.DefaultUiModule; end,
						chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
					},
				},
			},
			["6"] = {	-- GUI-6
				name = "Master Frame",
				role = GGD.ModuleRole.Ui,
				prototype = function() return GGC.DefaultUiModule; end,
				chassis = { API = 1, FNC = 1, INT = 1, OBC = 1 },
			},
			["7"] = {	-- GUI-7
				name = "Parameter Storage",
				role = GGD.ModuleRole.Service,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
			},
		},
	},
	IOC = {
		modules = {
			["1"] = {	-- IOC-1
				name = "Evoke Controller",
				role = GGD.ModuleRole.Dispatch,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
				cascade = { "IOC-2", "IOC-3" },
			},
			["2"] = {	-- IOC-2
				name = "Input Controller",
				role = GGD.ModuleRole.System,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
			},
			["3"] = {	-- IOC-3
				name = "Output Controller",
				role = GGD.ModuleRole.System,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
			},
		},
	},
	SND = {
		modules = {
			["1"] = {	-- SND-1
				name = "Common Service",
				role = GGD.ModuleRole.Service,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
			},
			["2"] = {	-- SND-2
				name = "Evoke Controller",
				role = GGD.ModuleRole.Dispatch,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1 },
				cascade = { "SND-1", "SND-3" },
			},
			["3"] = {	-- SND-3
				name = "Sound Controller",
				role = GGD.ModuleRole.System,
				prototype = function() return GGC.BaseModule; end,
				chassis = { API = 1, FNC = 1, INT = 1, MDT = 2 },
			},
		},
	},
};


-- NOTE: то же самое, только линейный объект
GGD.FastModuleLinkedList = {};


-- NOTE: список созданных объектов
GGD.FastModuleList = {};