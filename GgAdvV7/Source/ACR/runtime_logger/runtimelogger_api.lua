local This = GGF.ModuleGetWrapper("ACR-5");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Установка флага логгинга
function This:SetLoggingFlag(in_flag)
	return self:setLoggingFlag(in_flag);
end


-- Установка названия лога
function This:SetBattleLogZoneName(in_name)
	self:setBattleLogZoneName(in_name);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие флага логгинга
function This:GetLoggingFlag()
	return self:getLoggingFlag();
end


-- Возврат лог-кластера из мега лог-кластера по запросу метки времени(пока что так...)
function This:GetChoosenLogClusterByMark(in_mark)
	return self:getChoosenLogClusterByMark(in_mark);
end


-- Передача списка временных меток для меню выбора в уях
function This:GetAvailableLogList()
	return self:getAvailableLogList();
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- Старт логгирования
function This:StartLogging()
	self:startLogging();
end


-- Финиш логгирования
function This:FinishLogging()
	self:finishLogging();
end


-- Структура для логгирования детерминированных смертей юинтов(UNIT_DIED + *_DAMage)
function This:CreateConfirmedDeathStruct()
	return self:createConfirmedDeathStruct();
end


-- Структура для логгирования недетерминированных смертей юнитов(эвент UNIT_DIED)
function This:CreateUnitDiedRecordStruct()
	return self:createUnitDiedRecordStruct();
end


-- Структура для логгирования урона от окружения
function This:CreateEnvironmentalDaMageRecordStruct()
	return self:createEnvironmentalDaMageRecordStruct();
end


-- Универсальная структура для логгирования урона/миссов(до 2 объектов(source, target) за одну запись) 
function This:CreateUniversalJournalRecordStruct()
	return self:createUniversalJournalRecordStruct();
end


-- Логгирование подтвержденного кила(юнитом/окружением)(с источником)
function This:LogUnitKill(in_journalRecord)
	self:logUnitKill(in_journalRecord);
end


-- Логгирование смертей юнитов(без источника)
function This:LogUnitDeath(in_journalRecord)
	self:logUnitDeath(in_journalRecord);
end


-- Логгирование нанесенного урона(от всего в т.ч. от текстур)
function This:LogDaMageUniversal(in_type, in_journalRecord)
	self:logDaMageUniversal(in_type, in_journalRecord);
end


-- Логгирование "промахов"(блоки, мисы, парри, эвейды и т.п.)
function This:LogMissUniversal(in_type, in_sub_type, in_journalRecord)
	self:logMissUniversal(in_type, in_sub_type, in_journalRecord);
end


-- Логгирование шагового измерения статистики(средние и дифф. значения килов/хитов/миссов)
function This:LogStatistic(in_timeDifference, in_stat)
	self:logStatistic(in_timeDifference, in_stat);
end


-- CC: /acr dump []
-- Дамп выбранных данных в чат
function This:DumpSlashCommand(in_dspec)
	self:dumpSlashCommand(in_dspec);
end


-- CC: /acr logtest
-- Активация логгирования доп. данных
function This:FictiveLogSlashCommand(in_dspec)
	self:fictiveLogSlashCommand();
end


-- Глобальный трансивер(передатчик команд с уев на дампер в чат)
function This:TransferIncomingCommand(in_dmap) 
	self:transferIncomingCommand(in_dmap);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------