local This = GGF.ModuleGetWrapper("ACR-5");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- api
-- Установка флага логгинга
function This:setLoggingFlag(in_flag)
	GGM.FCS.RuntimeStorage:SetACRCombatLogging(in_flag);
end


-- api
-- Установка названия лога
function This:setBattleLogZoneName(in_name)
	if (self.vf_runtimeLogging) then
		self.vs_battleLogCluster.metaInformation.logZoneName = in_name;
	end
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Взятие флага логгинга
function This:getLoggingFlag()
	return GGM.FCS.RuntimeStorage:GetACRCombatLogging();
end


-- api
-- Возврат лог-кластера из мега лог-кластера по запросу метки времени(пока что так...)
function This:getChoosenLogClusterByMark(in_mark)
	return self.vs_battleLogMegaCluster[in_mark];
end


-- api
-- Передача списка временных меток для меню выбора в уях
function This:getAvailableLogList()
	return self.vs_combatLogMetaHistory;
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Старт логгирования
function This:startLogging()
	self.vs_combatLogMeta.startTime = date("%d/%m/%y %H:%M:%S");
	self.vs_combatLogMeta.identifyingToken = self:generateLogToken();

	local ai_new = 0;
	-- HACK: Принудительно меняем зону, иначе клинит автомат смены зоны геолокации и все ломается...
	GGF.VersionSplitterProc({
		{
			versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.Legion },
			action = function()
				SetMapToCurrentZone();
				ai_new = GetCurrentMapAreaID();
			end,
		},
		{
			versionInterval = { GGD.TocVersionList.BFA, GGD.TocVersionList.Invalid },
			action = function()
				ai_new = C_Map.GetBestMapForUnit("player");
			end,
		}
	});
	if (ai_new == nil) then
		ai_new = 0;
	end
	local zt_new = GGM.ASB.GeolocationBlock:GetZoneCluster()[tostring(ai_new)] or "WPZ";
	GGM.GUI.KillFeed:ProceedBattleZoneEnter(zt_new, self.vs_combatLogMeta.identifyingToken);
	self.vs_combatLogMeta.zoneName = zt_new;

	GGM.GUI.CombatInstrumentation:SetLogTokenSignature(self.vs_combatLogMeta.identifyingToken);

	GGM.GUI.CombatInstrumentation:SetLoggerRuntimeStatus(self.cl_runtimeLoggingStatusEnabled, self.cs_tumblerColor.on);

	self:setLoggingFlag(true);

	GGM.ACR.CombatInformation:DropCurrentStat();

	GGM.ACR.CombatLogger:StartLogging();	-- Combat Logger

	-- NOTE: Под шумок загрузки локации и лога
	-- WARNING: Каждый вызов этой фукнции лагает даже на топовых конфигах..
	UpdateAddOnMemoryUsage();
end


-- api
-- Финиш логгирования
function This:finishLogging()
	if (not self.getLoggingFlag()) then
		return;
	end

	GGM.GUI.CombatInstrumentation:SetLogTokenSignature("N/A");

	GGM.GUI.CombatInstrumentation:SetLoggerRuntimeStatus(self.cl_runtimeLoggingStatusDisabled, self.cs_tumblerColor.off);

	self:setLoggingFlag(false);

	self.vs_combatLogMeta.finishTime = date("%d/%m/%y %H:%M:%S")

	-- HACK: тут ссылка дублируется в мега кластер, а старая переводится на новый объект при след. старте логгирования
	self.vs_combatLogMetaHistory[self.vs_combatLogMeta.identifyingToken] = self.vs_combatLogMeta;

	GGM.ACR.CombatLogger:FinishLogging(self.vs_combatLogMeta.identifyingToken);	-- Combat Logger

	-- NOTE: Под шумок отгрузки с локации
	UpdateAddOnMemoryUsage();
end


-- Сброс логированных данных с рантайма
function This:refreshRuntimeLog()
	self.vs_battleLogCluster = {
		-- список дампов событий UNIT_DIED
		deathUniversalMasterTable = {
			data = {},
			index = 1
		},

		-- список подтвержденных киллов(все типы урона)(killed by falling как бы тоже считается)
		killUniversalMasterTable = {
			data = {},
			index = 1
		},

		--
		-- прототип иерархии : 
		--	Log
		-- 		type of daMage
		--			full log
		-- 			index of last

		-- список спелл-дампов нанесения урона(все типы) _DAMage
		damageUniversalMasterTable = {
		},

		-- список спелл-дампов промахов(все типы) _MISSED
		missUniversalMasterTable = {
		},

		parameterUniversalMasterTable = {
			AEN = {{0, 0}},	-- Actions
			SEN = {{0, 0}},

			AKD = {{0, 0}},	-- Kills
			SKD = {{0, 0}},

			ADD = {{0, 0}},	-- DaMages
			SDD = {{0, 0}},

			AMD = {{0, 0}},	-- Misses
			SMD = {{0, 0}},
		},

		metaInformation = {
			logStartTime,
			logFinishTime,
			logIdentifyingToken,
			logZoneName
		},
	};

	self.vs_battleLogCluster.damageUniversalMasterTable = GGF.TableCopy(self.md_damageTablePrototype);
	self.vs_battleLogCluster.missUniversalMasterTable = GGF.TableCopy(self.md_missTablePrototype);
end


-- Генерация токена текущего лога(идентификатор)
function This:generateLogToken()
	local token = ""
	while (token == "") do
		local array = {}
		for i = 1, 2 do
			array[i] = string.char(math.random(65, 90))
		end
		array[3] = "-"
		for i = 4, 6 do
			array[i] = string.char(math.random(65, 90))
		end
		token = table.concat(array);
		if self.vs_generatedLogTokens[token] then
			token = "";
		else
			self.vs_generatedLogTokens[token] = true;
		end
	end
	return token;
end


-- TODO: убрать юнит-тесты из рантайма!!!(GG-50)
-- TODO: пофиксить нерабочую тему с in_count
-- Форс первого тестового лога
function This:runFirstCombatLogUnitTest(in_count)
	if (in_count == 0) then
		return
	end

	in_count = in_count - 1;
	self:initializeLogCluster();

	local time = GGM.ACR.RuntimeInterceptor:ApplyRandomSuperUnitTestData()
	--GGM.ACR.RuntimeInterceptor:ApplyFirstUnitTestData();

	C_Timer.NewTimer(time + 2, function() self:commitLogCluster() end);
	--C_Timer.NewTimer(1.1, function() self:commitLogCluster() end);
	
	--C_Timer.NewTimer(1.2, function() self:applyAdditionalLog() end);
end


-- Форс второго тестового лога
function This:applyAdditionalLog()
	self:initializeLogCluster();

	GGM.ACR.RuntimeInterceptor:ApplySecondUnitTestData();

	C_Timer.NewTimer(1.1, function() self:commitLogCluster() end);
end


-- api
-- Структура для логгирования детерминированных смертей юинтов(UNIT_DIED + *_DAMage)
function This:createConfirmedDeathStruct()
	local struct = GGF.TableCopy(self.md_confirmedDeathStruct);

	return struct;
end


-- api
-- Структура для логгирования недетерминированных смертей юнитов(эвент UNIT_DIED)
function This:createUnitDiedRecordStruct()
	local struct = GGF.TableCopy(self.md_unitDiedRecordStruct);

	return struct;
end


-- api
-- Логгирование подтвержденного кила(юнитом/окружением)(с источником)
function This:logUnitKill(in_journalRecord)
	if (self:getLoggingFlag()) then
		self.vs_battleLogCluster.killUniversalMasterTable.data[self.vs_battleLogCluster.killUniversalMasterTable.index] = in_journalRecord;
		self.vs_battleLogCluster.killUniversalMasterTable.index = self.vs_battleLogCluster.killUniversalMasterTable.index + 1;
	end
end


-- api
-- Логгирование смертей юнитов(без источника)
function This:logUnitDeath(in_journalRecord)
	if (self:getLoggingFlag()) then
		self.vs_battleLogCluster.deathUniversalMasterTable.data[self.vs_battleLogCluster.deathUniversalMasterTable.index] = in_journalRecord;
		self.vs_battleLogCluster.deathUniversalMasterTable.index = self.vs_battleLogCluster.deathUniversalMasterTable.index + 1;
	end
end


-- api
-- Логгирование "промахов"(блоки, мисы, парри, эвейды и т.п.)
function This:logMissUniversal(in_type, in_sub_type, in_journalRecord)
	if (self:getLoggingFlag()) then
		self.vs_battleLogCluster.missUniversalMasterTable[in_sub_type][in_type].data[self.vs_battleLogCluster.missUniversalMasterTable[in_sub_type][in_type].index] = in_journalRecord;
		self.vs_battleLogCluster.missUniversalMasterTable[in_sub_type][in_type].index = self.vs_battleLogCluster.missUniversalMasterTable[in_sub_type][in_type].index + 1;
	end
end


-- Быстрая проверка аргументов на количество
function This:fastCheck(in_args)
	-- args[1] уже обработан
	if (in_args[2] == nil)	then -- пр. c
		--ACR.printCodeDebugStreamLine("#Warning#", "missing dump argument 2")
		return false
	end
	if (in_args[3] == nil) then -- пр. d
		--ACR.printCodeDebugStreamLine("#Warning#", "missing dump argument 3")
		return false
	end
	if (in_args[4] == nil) then -- пр. m
		--ACR.printCodeDebugStreamLine("#Warning#", "missing dump argument 4")
		return false
	end
	if ((in_args[4] ~= "k") and (in_args[5] == nil)) then -- пр. s
		--ACR.printCodeDebugStreamLine("#Warning#", "missing dump argument 5")
		return false
	end
	if ((in_args[4] == "m") and (in_args[6] == nil)) then -- пр. s
		--ACR.printCodeDebugStreamLine("#Warning#", "missing dump argument 6")
		return false
	end

	if (self.cs_logDumperChatterFilterSignature[in_args[2]] == nil) then
		--ACR.printCodeDebugStreamLine("#Warning#", "unknown dump argument 2 : '"..args[2].."'")
		return false
	end
	if (self.cs_logDumperDefinerFilterSignature[in_args[3]] == nil) then
		--ACR.printCodeDebugStreamLine("#Warning#", "unknown dump argument 3 : '"..args[3].."'")
		return false
	end
	if (self.cs_logDumperTyperFilterSignature[in_args[4]] == nil) then
		--ACR.printCodeDebugStreamLine("#Warning#", "unknown dump argument 4 : '"..args[4].."'")
		return false
	end

	if ((in_args[5] ~= nil) and (self.cs_logDumperCounterFilterSignature[in_args[5]] == nil)) then
		--ACR.printCodeDebugStreamLine("#Warning#", "unknown dump argument 5 : '"..args[5].."'")
		return false
	end
	if ((in_args[6] ~= nil) and (self.cs_logDumperMtyperFilterSignature[in_args[6]] == nil)) then
		--ACR.printCodeDebugStreamLine("#Warning#", "unknown dump argument 6 : '"..args[6].."'")
		return false
	end
end




-- Спец : dump	
	-- level 0 (dumper)
		-- c - chat
		-- s - Service
	-- level 1 (chatter)
		-- c - console
		-- g - gUild
		-- i - instance
		-- p - party
		-- ra - raid
		-- rw - raid Warning
		-- s - say
		-- w - whisper
		-- y - yell
	-- level 2 (definer)
		-- a - all
		-- d - defined
		-- u - Undefined
	-- level 3 (typer)
		-- k - kill
		-- s - spell
		-- m - miss
	-- level 4 (counter)(только для level 4 == miss || spell)
		-- f - full
		-- s - singeton
	-- level 5 (mtyper)(только для level 3 == miss)
		-- al - all
		-- ab - abosrb
		-- b - block
		-- de - deflect
		-- do - dodge
		-- e - evasion
		-- i - immune
		-- m - miss
		-- p - parry
		-- rf - reflect
		-- rs - resist

-- Обработка команд на дамп типа "/acr dump c a m f al"
function This:processDump(in_args)
	--print("stch")
	if (self:fastCheck(in_args) == false) then
		--print("error")
		return
	else
		--print("ok")
	end

	-- тут бы ассерт не помешал..
	-- TODO: ну да, ассерт не помешал бы...(GG-50)
	self.fn_logDumperChatterFilter[in_args[2]](in_args[3], in_args[4], in_args[5], in_args[6]);
end


-- TODO: убрать принты после отладки(вроде как отлажено же было, не?)
-- Дистанционное управление дампом
function This:proceedIncomingCommand(in_dmap)
	print("incoming data");
	local dstr = "";
	local lstr = "";

	local arg_str = {};
	arg_str[1] = "dump";

	print(dmap.length);

	-- Нет чеков, т.к. данные приходят от другого аддона
	-- TODO: Да, но ассерт не помешает.. 
	for k = 1,dmap.length do
		if (k ~= 1) then
			arg_str[k] = self.cs_GUI_Complies[in_dmap.level[k]][in_dmap.data[k]];
			dstr = dstr .. in_dmap.data[k] .. " ";
			lstr = lstr .. in_dmap.level[k] .. " ";
		end
	end

	print(dstr);
	print(lstr);

	self:processDump(arg_str);
end


-- api
-- Дамп выбранных данных в чат
function This:dumpSlashCommand(in_dspec)
	self:processDump(in_dspec);
end


-- api
-- Активация логгирования доп. данных
function This:fictiveLogSlashCommand()
	self:runFirstCombatLogUnitTest(1);
end


-- api
-- Глобальный трансивер(передатчик команд с уев на дампер в чат)
function This:transferIncomingCommand(in_dmap)
	self:proceedIncomingCommand(in_dmap);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------