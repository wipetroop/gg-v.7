local This = GGF.ModuleCreateWrapper("ACR-5", false);

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------


This.cl_runtimeLoggingStatusDisabled = "Stand By";
This.cl_runtimeLoggingStatusEnabled = "Running";


-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------


-- Цвета для индикаторных тумблеров
This.cs_tumblerColor = {
	on = GGD.Color.Green,
	off = GGD.Color.Red,
};


-- TODO: переназвать или симлинкануть или отреинжинирить(GG-48/GG-49)
This.cs_GUI_Complies = {
	["Lv0"] = {
		["Chat"] = "c",
		["Service"] = "s"
	},
	["Lv1"] = {
		["Console"] = "c",
		["GUild"] = "g",
		["Instance"] = "i",
		["Party"] = "p",
		["Raid"] = "ra",
		["RaidWarning"] = "rw",
		["Say"] = "s",
		["Whisper"] = "w",
		["Yell"] = "y"
	},
	["Lv2"] = {
		["Dall"] = "a",
		["Defined"] = "d",
		["Undefined"] = "u"
	},
	["Lv3"] = {
		["Kill"] = "k",
		["Spell"] = "s",
		["Miss"] = "m"
	},
	["Lv4"] = {
		["Singleton"] = "s",
		["Full"] = "f"
	},
	["Lv5"] = {
		["Mall"] = "al",
		["Absorb"] = "ab",
		["Block"] = "b",
		["Deflect"] = "de",
		["Dodge"] = "do",
		["Evade"] = "e",
		["Immune"] = "i",
		["Miss"] = "m",
		["Parry"] = "p",
		["Reflect"]	= "rf",
		["Resist"] = "rs"
	}
};


-- Массивы для предпроверки(вроде булевских подпространств)
This.cs_logDumperMtyperFilterSignature = {
	["al"] = 1,
	["ab"] = 1,
	["b"] = 1,
	["de"] = 1,
	["do"] = 1,
	["e"] = 1,
	["i"] = 1,
	["m"] = 1,
	["p"] = 1,
	["rf"] = 1,
	["rs"] = 1
};
This.cs_logDumperCounterFilterSignature = {
	["s"] = 1,
	["f"] = 1
};
This.cs_logDumperTyperFilterSignature = {
	["k"] = 1,
	["s"] = 1,
	["m"] = 1
};
This.cs_logDumperDefinerFilterSignature = {
	["a"] = 1,
	["d"] = 1,
	["u"] = 1
};
This.cs_logDumperChatterFilterSignature = {
	["c"] = 1,
	["g"] = 1,
	["i"] = 1,
	["p"] = 1,
	["ra"] = 1,
	["rw"] = 1,
	["s"] = 1,
	["w"] = 1,
	["y"] = 1
};


-------------------------------------------------------------------------------
-- Functors
-------------------------------------------------------------------------------

-- TODO: по возможности упростить конструкцию/логику(GG-49)
This.fn_logDumperMtyperFilter = {
	["al"] = function(table) rt = {} for k,v in pairs(table) do rt[k] = v end return rt end,
	["ab"] = function(table) rt = {} for k,v in pairs(table) do if ((string.find(v,"ABSORB")) or (string.find(v,"<<<"))) then rt[k] = v end end return rt end,
	["b"] = function(table) rt = {} for k,v in pairs(table) do if ((string.find(v,"BLOCK")) or (string.find(v,"<<<"))) then rt[k] = v end end return rt end,
	["de"] = function(table) rt = {} for k,v in pairs(table) do if ((string.find(v,"DEFLECT")) or (string.find(v,"<<<"))) then rt[k] = v end end return rt end,
	["do"] = function(table) rt = {} for k,v in pairs(table) do if ((string.find(v,"DODGE")) or (string.find(v,"<<<"))) then rt[k] = v end end return rt end,
	["e"] = function(table) rt = {} for k,v in pairs(table) do if ((string.find(v,"EVADE")) or (string.find(v,"<<<"))) then rt[k] = v end end return rt end,
	["i"] = function(table) rt = {} for k,v in pairs(table) do if ((string.find(v,"IMMUNE")) or (string.find(v,"<<<"))) then rt[k] = v end end return rt end,
	["m"] = function(table) rt = {} for k,v in pairs(table) do if ((string.find(v,"MISS")) or (string.find(v,"<<<"))) then rt[k] = v end end return rt end,
	["p"] = function(table) rt = {} for k,v in pairs(table) do if ((string.find(v,"PARRY")) or (string.find(v,"<<<"))) then rt[k] = v end end return rt end,
	["rf"] = function(table) rt = {} for k,v in pairs(table) do if ((string.find(v,"REFLECT")) or (string.find(v,"<<<"))) then rt[k] = v end end return rt end,
	["rs"] = function(table) rt = {} for k,v in pairs(table) do if ((string.find(v,"RESIST")) or (string.find(v,"<<<"))) then rt[k] = v end end return rt end,
};


This.fn_logDumperTyperFilter = {
	["k"] = function(mtyper, counter) return dbgLogCluster.dbgHookMasterTableSpellKillUniversal.data end,
	["s"] = function(mtyper, counter) return dbgLogCluster.dbgHookMasterTableSpellDaMageUniversal.data[counter] end,
	["m"] = function(mtyper, counter) return logDumperMtyperFilter[mtyper](dbgLogCluster.dbgHookMasterTableSpellMissUniversal.data[counter]) end
};


This.fn_logDumperDefinerFilter = {
	["a"] = function(typer,mtyper,counter) return ACR_CL.fn_logDumperTyperFilter[typer](mtyper,counter) end, 
	["d"] = function(typer,mtyper,counter) rt = {} msg = ACR_CL.fn_logDumperTyperFilter[typer](mtyper,counter) for k,v in pairs(msg) do if ((string.find(v,"Undefined") == nil) or (string.find(v,"<<<"))) then rt[k] = v end end return rt end,
	["u"] = function(typer,mtyper,counter) rt = {} msg = ACR_CLfn_logDumperTyperFilter[typer](mtyper,counter) for k,v in pairs(msg) do if ((string.find(v,"Undefined")) or (string.find(v,"<<<"))) then rt[k] = v end end return rt end,
};


-- TODO: выяснить, откуда те функции в хвосте взялись..(GG-49)
This.fn_logDumperChatterFilter = {
	["c"] = function(definer, typer, counter, mtyper) msgs = ACR_CL.fn_logDumperDefinerFilter[definer](typer,mtyper,counter) consoleMasterDump(msgs) end,
	["g"] = function(definer, typer, counter, mtyper) msgs = ACR_CL.fn_logDumperDefinerFilter[definer](typer,mtyper,counter) chatMasterDump("GUiLD",msgs) end,
	["i"] = function(definer, typer, counter, mtyper) msgs = ACR_CL.fn_logDumperDefinerFilter[definer](typer,mtyper,counter) chatMasterDump("INSTANCE_CHAT",msgs) end,
	["p"] = function(definer, typer, counter, mtyper) msgs = ACR_CL.fn_logDumperDefinerFilter[definer](typer,mtyper,counter) chatMasterDump("PARTY",msgs) end,
	["ra"] = function(definer, typer, counter, mtyper) msgs = ACR_CL.fn_logDumperDefinerFilter[definer](typer,mtyper,counter) chatMasterDump("RAID",msgs) end,
	["rw"] = function(definer, typer, counter, mtyper) msgs = ACR_CL.fn_logDumperDefinerFilter[definer](typer,mtyper,counter) chatMasterDump("RAID_Warning",msgs) end,
	["s"] = function(definer, typer, counter, mtyper) msgs = ACR_CL.fn_logDumperDefinerFilter[definer](typer,mtyper,counter) chatMasterDump("SAY",msgs) end,
	["w"] = function(definer, typer, counter, mtyper) msgs = ACR_CL.fn_logDumperDefinerFilter[definer](typer,mtyper,counter) chatMasterDump("WHISPER",msgs) end,
	["y"] = function(definer, typer, counter, mtyper) msgs = ACR_CL.fn_logDumperDefinerFilter[definer](typer,mtyper,counter) chatMasterDump("YELL",msgs) end
};

-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------


-- Метаданные логов
This.vs_combatLogMeta = {
	startTime = nil,		--prototype
	identifyingToken = nil,	--prototype
	zoneName = nil,			--prototype
	finishTime = nil,		--prototype
};
This.vs_combatLogMetaHistory = {};

-- Список сгенерированных токенов в пределах сессии(чтоб не повторялось)
This.vs_generatedLogTokens = {};


-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------