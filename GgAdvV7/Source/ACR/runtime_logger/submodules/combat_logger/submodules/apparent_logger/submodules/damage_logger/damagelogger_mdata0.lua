local This = GGF.ModuleGetWrapper("ACR-5-1-1-5");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.3 (MDT Check)
function This:MS_CHECK_MDT_0()
	return 1;
end


-------------------------------------------------------------------------------
-- "multitudinous data" block
-------------------------------------------------------------------------------


This.md_logRecord = {};



-- NOTE: старый код
-- TODO: выпилить лишнюю структуру(GG-17)
This.md_environmentalDamageRecordStruct = {
	-- технически, нужен только для сравнения по времени
	timestamp = {
		color = "#stamp#",
	},

	time = {
		color = "#stamp#",
	},

	edType = {
		color = "#standart#",
	},

	sourceName = {
		color = "#standart#",
	},

	sourceID = {
		color = "#standart#",
	},

	sourceFlag = {
		color = "#standart#",
	},

	sourceOwnerName = {
		color = "#standart#",
	},

	sourceOwnerID = {
		color = "#standart#",
	},

	sourceOwnerFlag = {
		color = "#standart#",
	},

	targetName = {
		color = "#standart#",
	},

	targetID = {
		color = "#standart#",
	},

	targetFlag = {
		color = "#standart#",
	},

	targetOwnerName = {
		color = "#standart#",
	},

	targetOwnerID = {
		color = "#standart#",
	},

	targetOwnerFlag = {
		color = "#standart#",
	},

	spellDefined = {
		color = "",
	},

	spellName = {
		color = "#standart#",
	},

	spellID = {
		color = "#standart#",
	},

	spellDaMage = {
		color = "#standart#",
	},

	edAmount = {
		color = "#standart#",
	},

	isCritical = {
		color = "#standart#",
	},

	-- ну эт только для шаманов(после выхода легиона то..)
	isMulticast = {
		color = "#standart#",
	},

	overkill = {
		color = "#standart#",
	},
};