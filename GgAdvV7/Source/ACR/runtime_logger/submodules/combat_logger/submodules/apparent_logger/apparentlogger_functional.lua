local This = GGF.ModuleGetWrapper("ACR-5-1-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Старт логгирования
function This:startLogging()
   GGF.MDL.EngageLog(GGM.ACR.AuraLogger, {});      -- Aura Logger
   GGF.MDL.EngageLog(GGM.ACR.BrakeLogger, {});     -- Brake Logger
   GGF.MDL.EngageLog(GGM.ACR.CastLogger, {});      -- Cast Logger
   GGF.MDL.EngageLog(GGM.ACR.CreateLogger, {});    -- Create Logger
   GGF.MDL.EngageLog(GGM.ACR.DamageLogger, {});    -- DaMage Logger
   GGF.MDL.EngageLog(GGM.ACR.DeathLogger, {});     -- Death Logger
   GGF.MDL.EngageLog(GGM.ACR.DispelLogger, {});    -- Dispel Logger
   GGF.MDL.EngageLog(GGM.ACR.DrainLogger, {});     -- Drain Logger
   GGF.MDL.EngageLog(GGM.ACR.EnchantLogger, {});   -- Enchant Logger
   GGF.MDL.EngageLog(GGM.ACR.EnergizeLogger, {});  -- Energize Logger
   GGF.MDL.EngageLog(GGM.ACR.HealLogger, {});      -- Heal Logger
   GGF.MDL.EngageLog(GGM.ACR.InterruptLogger, {}); -- Interrupt Logger
   GGF.MDL.EngageLog(GGM.ACR.LeechLogger, {});     -- Leech Logger
   GGF.MDL.EngageLog(GGM.ACR.MissLogger, {});      -- Miss Logger
   GGF.MDL.EngageLog(GGM.ACR.StealLogger, {});     -- Steal Logger
   GGF.MDL.EngageLog(GGM.ACR.SummonLogger, {});    -- Summon Logger
end


-- api
-- Финиш логгирования
function This:finishLogging(in_token)
   GGF.MDL.DisengageLog(GGM.ACR.AuraLogger, {}, in_token);     -- Aura Logger
   GGF.MDL.DisengageLog(GGM.ACR.BrakeLogger, {}, in_token);    -- Brake Logger
   GGF.MDL.DisengageLog(GGM.ACR.CastLogger, {}, in_token);     -- Cast Logger
   GGF.MDL.DisengageLog(GGM.ACR.CreateLogger, {}, in_token);   -- Create Logger
   GGF.MDL.DisengageLog(GGM.ACR.DamageLogger, {}, in_token);   -- DaMage Logger
   GGF.MDL.DisengageLog(GGM.ACR.DeathLogger, {}, in_token);    -- Death Logger
   GGF.MDL.DisengageLog(GGM.ACR.DispelLogger, {}, in_token);   -- Dispel Logger
   GGF.MDL.DisengageLog(GGM.ACR.DrainLogger, {}, in_token);    -- Drain Logger
   GGF.MDL.DisengageLog(GGM.ACR.EnchantLogger, {}, in_token);  -- Enchant Logger
   GGF.MDL.DisengageLog(GGM.ACR.EnergizeLogger, {}, in_token); -- Energize Logger
   GGF.MDL.DisengageLog(GGM.ACR.HealLogger, {}, in_token);     -- Heal Logger
   GGF.MDL.DisengageLog(GGM.ACR.InterruptLogger, {}, in_token);-- Interrupt Logger
   GGF.MDL.DisengageLog(GGM.ACR.LeechLogger, {}, in_token);    -- Leech Logger
   GGF.MDL.DisengageLog(GGM.ACR.MissLogger, {}, in_token);     -- Miss Logger
   GGF.MDL.DisengageLog(GGM.ACR.StealLogger, {}, in_token);    -- Steal Logger
   GGF.MDL.DisengageLog(GGM.ACR.SummonLogger, {}, in_token);   -- Summon Logger
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------