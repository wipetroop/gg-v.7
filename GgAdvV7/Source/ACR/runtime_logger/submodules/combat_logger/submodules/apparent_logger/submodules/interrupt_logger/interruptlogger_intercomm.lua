local This = GGF.ModuleGetWrapper("ACR-5-1-1-12");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.1 (Miscellaneous Variables)
function This:MPRC_MVR_INITIALIZE()
	self.md_logRecord = GGF.TableSafeExtend(self.md_logRecord, GGM.ACR.LoggerStructureAssistant:GetBaseConstructionSetLR());
	self.md_logRecord = GGF.TableSafeExtend(self.md_logRecord, GGM.ACR.LoggerStructureAssistant:GetExtraSpellDataLR());
	self.md_logRecord = GGF.TableSafeExtend(self.md_logRecord, GGM.ACR.LoggerStructureAssistant:GetEventTypeMetaLR());
	self.md_logRecord = GGF.TableSafeExtend(self.md_logRecord, GGM.ACR.LoggerStructureAssistant:GetMainSpellDataLR());
	self.md_logRecord = GGF.TableSafeExtend(self.md_logRecord, GGM.ACR.LoggerStructureAssistant:GetSourcePlayerMetaLR());
	self.md_logRecord = GGF.TableSafeExtend(self.md_logRecord, GGM.ACR.LoggerStructureAssistant:GetTargetPlayerMetaLR());
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------