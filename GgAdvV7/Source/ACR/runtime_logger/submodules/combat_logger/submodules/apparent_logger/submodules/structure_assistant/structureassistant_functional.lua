local This = GGF.ModuleGetWrapper("ACR-5-1-1-16");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Взятие базовой конструкции(имеется во всех записях лога)
function This:getBaseConstructionSetLR()
	return self.md_baseConstructionSetLR;
end


-- api
-- Взятие части структуры с информацией о дополнительнои заклинании эвента
function This:getExtraSpellDataLR()
	return self.md_extraSpellDataLR;
end


-- api
-- Взятие части структуры с метаинформацией об эвенте
function This:getEventTypeMetaLR()
	return self.md_eventTypeMetaLR;
end


-- api
-- Взятие части структуры с информацией об основном заклинании эвента
function This:getMainSpellDataLR()
	return self.md_mainSpellDataLR;
end


-- api
-- Взятие части структуры с информацией о сурсовом игроке
function This:getSourcePlayerMetaLR()
	return self.md_sourcePlayerMetaLR;
end


-- api
-- Взятие части структуры с информацией о таргетовом игроке
function This:getTargetPlayerMetaLR()
	return self.md_targetPlayerMetaLR;
end


-- api
-- Взятие сериализатора записей лога
function This:getLogSerializer()
	return self:getConfig().serializer;
end


-- api
-- Взятие десериализатора записей лога
function This:getLogDeserializer()
	return self:getConfig().deserializer;
end


-- api
-- Взятие чекера флага логгирования
function This:getLoggingCheck()
	return self:getConfig().loggingCheck;
end


-- api
-- Взятие названия поля объекта логгера по токену
function This:getFieldNameByToken(in_token)
	return self.md_tokenAssigner[in_token];
end


-- api
-- Взятие абсолютного приоритета поля
function This:getFieldPriority(in_fieldName)
	return self.md_fieldPriority[in_fieldName];
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Универсальный сериализатор записи лога
function This:serializeLR(in_data)
	-- TODO: реализовать в рамках таска GG-5(GG-5)
	-- NOTE: эта и следующая функции должны реализовывать биективное отображение
	return in_data;
end


-- api
-- Универсальный десериализатор записи лога
function This:deserializeLR(in_data)
	return in_data;
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------