local This = GGF.ModuleGetWrapper("ACR-5-1-1-16");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие базовой конструкции(имеется во всех записях лога)
function This:GetBaseConstructionSetLR()
	return self:getBaseConstructionSetLR();
end


-- Взятие части структуры с информацией о дополнительнои заклинании эвента
function This:GetExtraSpellDataLR()
	return self:getExtraSpellDataLR();
end


-- Взятие части структуры с метаинформацией об эвенте
function This:GetEventTypeMetaLR()
	return self:getEventTypeMetaLR();
end


-- Взятие части структуры с информацией об основном заклинании эвента
function This:GetMainSpellDataLR()
	return self:getMainSpellDataLR();
end


-- Взятие части структуры с информацией о сурсовом игроке
function This:GetSourcePlayerMetaLR()
	return self:getSourcePlayerMetaLR();
end


-- Взятие части структуры с информацией о таргетовом игроке
function This:GetTargetPlayerMetaLR()
	return self:getTargetPlayerMetaLR();
end


-- Взятие сериализатора записей лога
function This:GetLogSerializer()
	return self:getLogSerializer();
end


-- Взятие десериализатора записей лога
function This:GetLogDeserializer()
	return self:getLogDeserializer();
end


-- Взятие чекера флага логгирования
function This:GetLoggingCheck()
	return self:getLoggingCheck();
end


-- Взятие названия поля объекта логгера по токену
function This:GetFieldNameByToken(in_token)
	return self:getFieldNameByToken(in_token);
end


-- Взятие абсолютного приоритета поля
function This:GetFieldPriority(in_fieldName)
	return self:getFieldPriority(in_fieldName);
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- Универсальный сериализатор записи лога
function This:SerializeLR(in_data)
	return self:serializeLR(in_data);
end


-- Универсальный десериализатор записи лога
function This:DeserializeLR(in_data)
	return self:deserializeLR(in_data);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------