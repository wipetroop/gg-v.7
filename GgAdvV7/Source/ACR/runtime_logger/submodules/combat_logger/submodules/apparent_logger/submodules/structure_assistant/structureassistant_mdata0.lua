local This = GGF.ModuleGetWrapper("ACR-5-1-1-16");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.3 (MDT Check)
function This:MS_CHECK_MDT_0()
	return 1;
end


-------------------------------------------------------------------------------
-- "multitudinous data" block
-------------------------------------------------------------------------------


-- TODO: переделать цвет(GG-20)
-- Базовая мезоконструкция
This.md_baseConstructionSetLR = {
	LR_BCS = true,

	-- системная дата
	bcs_time = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,	-- TODO: переправить на дефолтную дату(GG-17)
	},

	-- временная метка события(внешняя)
	bcs_timestamp = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},
};


-- Информация о вторичном скилле(прерывания и т.п.)
This.md_extraSpellDataLR = {
	LR_ESD = true,

	-- флаг определенности
	esd_defined = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- id
	esd_id = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- название
	esd_name = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},
};


-- Метаинформация об эвенте
This.md_eventTypeMetaLR = {
	LR_ETM = true,

	-- подтип
		-- missType(ABSORB, BLOCK, DEFLECT, DODGE, EVADE, IMMUNE, MISS, PARRY, REFLECT, RESIST)
		-- auraType(BUFF, DEBUFF)
		-- failedType(<too many reasons of fail>)
		-- powerType(
			-- -2	health,
			-- 0	mana,
			-- 1	rage,
			-- 2	focus,
			-- 3	energy,
			-- 5	runes,
			-- 6	runic power,
			-- 7	soul shards,
			-- 8	lunar power,
			-- 9	holy power,
			-- 10	alternate power,
			-- 11	maelstrom,
			-- 12	chi,
			-- 13	insanity,
			-- 14	OBSOLETE,
			-- 15	OBSOLETE2,
			-- 16	arcane charges,
			-- 17	fury,
			-- 18	pain,
			--)
	etm_extraType = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- тип(пр.: RANGE_MISSED, SPELL_DAMage)
	etm_type = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},
};


-- Основная информация о скилле
This.md_mainSpellDataLR = {
	LR_MSD = true,

	-- заблоченное чем-то(дамаг/исцеление)
	msd_absorbed = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- количественный показатель(урон/отхил/стаки и т.д.)
	msd_amount = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- флаг определенности
	msd_defined = {
		color = "",
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- дополнительный количественный показатель(урон/отхил/стаки и т.д.)(используется крайне редко)
	msd_extraAmount = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- id
	msd_id = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- критический эффект
	msd_isCritical = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- многократные атаки
	-- ну эт только для шаманов(после выхода легиона то..)
	msd_isMulticast = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- спелл нанесен оффхендом(только для миссов)
	msd_isOffhand = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- название
	msd_name = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- оверхил
	msd_overheal = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- оверкилл(избыточный урон)
	msd_overkill = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},
};


-- Информация об источнике события
This.md_sourcePlayerMetaLR = {
	LR_SPM = true,

	-- флаг
	spm_flag = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- id
	spm_id = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- название
	spm_name = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

		-- флаг хозяина цели
	spm_ownerFlag = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- id хозяина
	spm_ownerId = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- имя хозяина
	spm_ownerName = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- свой/чужой
	spm_reaction = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},
};


-- Информация о цели события
This.md_targetPlayerMetaLR = {
	LR_TPM = true,

	-- флаг
	tpm_flag = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- id
	tpm_id = {
		color = "#standart#",
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- название
	tpm_name = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- флаг хозяина цели
	tpm_ownerFlag = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- id хозяина
	tpm_ownerId = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- имя хозяина
	tpm_ownerName = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},

	-- свой/чужой
	tpm_reaction = {
		color = GGD.Color.Neutral,
		data = GGM.ACR.StatisticStructureAssistant.cl_unusedFieldData,
	},
};


This.md_tokenAssigner = {
	["LR_B_TIME"] = "bcs_time",
	["LR_B_TMST"] = "bcs_timestamp",

	["LR_E_DFND"] = "esd_defined",
	["LR_E_IDID"] = "esd_id",
	["LR_E_NAME"] = "esd_name",

	["LR_E_EXTP"] = "etm_extraType",
	["LR_E_TYPE"] = "etm_type",

	["LR_M_ABSB"] = "msd_absorbed",
	["LR_M_AMNT"] = "msd_amount",
	["LR_M_DFND"] = "msd_defined",
	["LR_M_EXAM"] = "msd_extraAmount",
	["LR_M_IDID"] = "msd_id",
	["LR_M_ISCR"] = "msd_isCritical",
	["LR_M_ISMC"] = "msd_isMulticast",
	["LR_M_ISOH"] = "msd_isOffhand",
	["LR_M_NAME"] = "msd_name",
	["LR_M_OVHL"] = "msd_overheal",
	["LR_M_OVKL"] = "msd_overkill",

	["LR_S_FLAG"] = "spm_flag",
	["LR_S_IDID"] = "spm_id",
	["LR_S_NAME"] = "spm_name",
	["LR_S_OWFL"] = "spm_ownerFlag",
	["LR_S_OWID"] = "spm_ownerId",
	["LR_S_OWNM"] = "spm_ownerName",
	["LR_S_REAC"] = "spm_reaction",

	["LR_T_FLAG"] = "tpm_flag",
	["LR_T_IDID"] = "tpm_id",
	["LR_T_NAME"] = "tpm_name",
	["LR_T_OWFL"] = "tpm_ownerFlag",
	["LR_T_OWID"] = "tpm_ownerId",
	["LR_T_OWNM"] = "tpm_ownerName",
	["LR_T_REAC"] = "tpm_reaction",
};

This.md_fieldPriority = {
	["bcs_time"] = 1,
	["bcs_timestamp"] = 2,
	["etm_type"] = 3,
	["etm_extraType"] = 4,
	["spm_flag"] = 5,
	["spm_id"] = 6,
	["spm_name"] = 7,
	["spm_ownerFlag"] = 8,
	["spm_ownerId"] = 9,
	["spm_ownerName"] = 10,
	["spm_reaction"] = 11,
	["tpm_flag"] = 12,
	["tpm_id"] = 13,
	["tpm_name"] = 14,
	["tpm_ownerFlag"] = 15,
	["tpm_ownerId"] = 16,
	["tpm_ownerName"] = 17,
	["tpm_reaction"] = 18,
	["msd_absorbed"] = 19,
	["msd_amount"] = 20,
	["msd_defined"] = 21,
	["msd_extraAmount"] = 22,
	["msd_id"] = 23,
	["msd_isCritical"] = 24,
	["msd_isMulticast"] = 25,
	["msd_isOffhand"] = 26,
	["msd_name"] = 27,
	["msd_overheal"] = 28,
	["msd_overkill"] = 29,
	["esd_defined"] = 30,
	["esd_id"] = 31,
	["esd_name"] = 32,
};