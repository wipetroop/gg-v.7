local This = GGF.ModuleGetWrapper("ACR-5-1-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Старт логгирования
function This:startLogging()
	--GGM.ACR.ControlLogger:EngageLog();	-- Control Logger
	--GGM.ACR.KillLogger:EngageLog();	-- Kill Logger
	--GGM.ACR.ReflectLogger:EngageLog();	-- Reflect Logger
end


-- api
-- Финиш логгирования
function This:finishLogging(in_token)
	--GGM.ACR.ControlLogger:DisengageLog();	-- Control Logger
	--GGM.ACR.KillLogger:DisengageLog();	-- Kill Logger
	--GGM.ACR.ReflectLogger:DisengageLog();	-- Reflect Logger
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------