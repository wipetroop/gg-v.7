local This = GGF.ModuleGetWrapper("ACR-6-1-1-16");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.3 (MDT Check)
function This:MS_CHECK_MDT_0()
	return 1;
end


-------------------------------------------------------------------------------
-- "multitudinous data" block
-------------------------------------------------------------------------------


This.md_baseConstructionSetSR = {
	SR_BCS = true,

	-- системная дата
	bcs_time = {
		color = "#stamp#",
		data = 0,	-- TODO: переправить на дефолтную дату
	},

	-- количество действий в минуту
	bcs_apm = {
		color = "#stamp#",
		data = 0,
	},
};


This.md_daMageAverageIndexSR = {
	SR_DAI = true,

	-- количество действий нанесения урона в минуту
	dai_apm = {
		color = "#stamp#",
		data = 0,
	},

	-- общий средний урон
	dai_commonAverage = {
		color = "#stamp#",
		data = 0,
	},

	-- средний урон игрока
	dai_PlayerAverage = {
		color = "#stamp#",
		data = 0,
	},

	-- средний урон союзников(в т.ч. петов, прислужников и т.п.)
	dai_FriendlyAverage = {
		color = "#stamp#",
		data = 0,
	},

	-- средний урон противников(в т.ч. петов, прислужников и т.п.)
	dai_HostileAverage = {
		color = "#stamp#",
		data = 0,
	},
};


This.md_healAverageIndexSR = {
	SR_HAI = true,

	-- количество действий нанесения урона в минуту
	hai_apm = {
		color = "#stamp#",
		data = 0,
	},

	-- общее среднее исцеление
	hai_commonAverage = {
		color = "#stamp#",
		data = 0,
	},

	-- среднее исцеления игрока
	hai_PlayerAverage = {
		color = "#stamp#",
		data = 0,
	},

	-- среднее исцеление союзников(в т.ч. петов, прислужников и т.п.)
	hai_FriendlyAverage = {
		color = "#stamp#",
		data = 0,
	},

	-- среднее исцеление противников(в т.ч. петов, прислужников и т.п.)
	hai_HostileAverage = {
		color = "#stamp#",
		data = 0,
	},
};