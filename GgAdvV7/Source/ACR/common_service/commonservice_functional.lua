local This = GGF.ModuleGetWrapper("ACR-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Определитель типа юнита по флагу
function This:getUnitTypeByFlag(flag)
	if bit.band(flag, COMBATLOG_OBJECT_TYPE_PET) > 0 then
		return GGE.UnitType.Pet;
	elseif bit.band(flag, COMBATLOG_OBJECT_TYPE_PLAYER) > 0 then
		return GGE.UnitType.Player;
	elseif bit.band(flag, COMBATLOG_OBJECT_TYPE_OBJECT) > 0 then
		return GGE.UnitType.Object;
	else
		return GGE.UnitType.Mob;
		-- для теста
		--return
	end
end


-- Определитель реакции(система свой/чужой) юнита
function This:getReactionByFlag(in_flag)
	if bit.band(in_flag, COMBATLOG_OBJECT_REACTION_HOSTILE) > 0 then
		return GGE.UnitReaction.Hostile;
	else
		return GGE.UnitReaction.Friend;
	end
end


-- api
-- Определитель фракции персонажа(орда, альянс, нейтрал)
function This:getUnitFactionByFlag(in_flag)
	local reaction = self:getReactionByFlag(in_flag);

	local pfg = self.cs_factionTransceiver[GGM.FCS.RuntimeStorage:GetPlayerFactionGroup()];

	if (pfg == GGE.UnitFaction.Alliance and reaction == GGE.UnitReaction.Friendly) or (pfg == GGE.UnitFaction.Horde and reaction == GGE.UnitReaction.Hostile) then
		return GGE.UnitFaction.Alliance;
	elseif (pfg == GGE.UnitFaction.Alliance and reaction == GGE.UnitReaction.Hostile) or (pfg == GGE.UnitFaction.Horde and reaction == GGE.UnitReaction.Friendly) then
		return GGE.UnitFaction.Horde;
	else
		return GGE.UnitFaction.Default;
	end
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Проверка текущей зоны на флаг пвп(вроде бы активируется только на аренах и бг)
function This:isInBG()
	return select(2, IsInInstance()) == "pvp";
end


-- api
-- Определитель принадлежности управления юнитом игроку
function This:isPlayerControlled(in_sourceName, in_sourceID, in_sourceFlag)
	--print(sourceFlag)
	-- in_sourceID не пригодился?
	if in_sourceName == GGM.FCS.RuntimeStorage:GetPlayerName() or bit.band(in_sourceFlag, COMBATLOG_OBJECT_AFFILIATION_MINE) > 0 then
		return true;
	end
	return false;
end


-- api
-- Функция парсинга имени персонажа(сброс постфикса серва и т.д.)
function This:parseName(in_identifier)
	-- Шаблон ориентирован на схему "<имя>" или "<имя>-<сервер>"(чисто по офе сделано)
	-- Имя может состоять из любых символов кроме '-'(допущение модели)
	-- Сервер аналогично(хотя вот тут не факт)(сейчас чекается первый '-', все что после - сервер) 
	-- Если '-' нет в строке - чар с реалма игрока
	local name, server = strmatch(in_identifier, "^([^%-]+)-?(.*)");
	return name, server == "" and GetRealmName() or server;
end


-- api
-- Функция сброса постфикса с названием реалма
function This:truncRealm(in_name)
	local outName = "";
	local outRealm = nil; -- rezerv
	--for f,s in string.gmatch(name, "(%w+)-(%w+)") do
		--outName = f;
		--outRealm = s;
	--end
	local flag = false;
	for i = 1, #in_name do
		local c = in_name:sub(i, i);
		if c == "-" then
			flag = true;
		end
		if flag == false then
			outName = outName .. c;
		end
	end

	return outName;
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------