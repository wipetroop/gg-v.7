local This = GGF.ModuleGetWrapper("ACR-7");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Дублирование кластера игроков
function This:getSessionPlayerCluster()
	return self.vs_sessionPlayerCluster;
end	


-- api
-- Взятие кластированного игрока
function This:getClusteRedPlayer(in_name)
	local name, server = GGM.ACR.CommonService:ParseName(in_name);
	if (self.vs_sessionPlayerCluster[server] == nil) then
		return nil;
	end
	return self.vs_sessionPlayerCluster[server][name];
end


-- api
-- Взятие индекса звука
function This:getSoundIndex(in_class, in_spec)
	return self.vs_specCluster[in_class][in_spec];
end


-- api
-- Взятие кластера игроков
function This:getSessionPlayerCluster()
	return self.vs_sessionPlayerCluster;
end


-- api
-- 
function This:getRuntimePlayerInfoByName(in_name)
	local pname, pserver = GGM.ACR.CommonService:ParseName(in_name);
	if (self.vs_sessionPlayerCluster.name[pserver] ~= nil) then
		if (self.vs_sessionPlayerCluster.name[pserver][pname] ~= nil) then
			return self.vs_sessionPlayerCluster.name[pserver][pname];
		end
	end

	return nil;
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


--
function This:splitString(in_string)
	local delimiters = {
		["-"] = true,
		[" "] = true,
		["'"] = true,
	};
	local words = {};
	local saidx = 1;
	local tmp = "";
	for i = 1,#in_string do
		local c = in_string:sub(i, i);
		if (delimiters[c] == nil) then
			tmp = tmp .. c;
		else
			words[saidx] = tmp;
			tmp = "";
			saidx = saidx + 1;
		end
	end
	words[saidx] = tmp;

	return words;
end


-- Поиск по маске имени игрока в кластере
function This:checkInCluster(in_word)
	for sname, scluster in pairs(self.vs_sessionPlayerCluster.name) do
		for name,data in pairs(scluster) do
			if (name:find(in_word)) then
				return data;
			end
		end
	end
	return nil;
end


-- api
-- Адаптивный поиск игрока по исходнику вида "Питомец Агромании"
function This:adaptivePlayerSearch(in_fstring)
	local etalonTC = 100;
	local candidate = nil;
	local find = nil;
	for idx, word in ipairs(self:splitString(in_fstring)) do
		local name = word;
		local truncCount = 0;
		find = self:checkInCluster(name);
		while ((not find) and (#name > 0)) do
			name = name:sub(1, -2);
			find = self:checkInCluster(name);
			truncCount = truncCount + 1;
		end
		if (find) then
			if (truncCount < etalonTC) then
				etalonTC = truncCount;
				candidate = find;
			end
		end
	end

	if (candidate) then
		return candidate.guid, candidate.name;
	end
	return nil, nil;
end


-- api
-- Проверка наличия определнного класса в таблице
function This:checkSpecClusterClass(in_class)
	return self.vs_specCluster[in_class] ~= nil;
end


-- api
-- Проверка наличия индекса звука
function This:checkSoundIndex(in_class, in_spec)
	return self.vs_specCluster[in_class][in_spec] ~= nil;
end


-- Определитель спека игроков на поле боя
function This:specInspectorScript()
	-- Show scores for Players of both factions
	--SetBattlefieldScoreFaction()
	
	-- Build list of Players
	local players, specless = {}, 0;
	for i = 1, GetNumBattlefieldScores() do
		local name, killingBlows, honorableKills, deaths, honorGained, faction, race, class, classToken, damageDone, healingDone, bgRating, ratingChange, preMatchMMR, mmrChange, talentSpec = GetBattlefieldScore(i);
		local pname, pserver = GGM.ACR.CommonService:ParseName(name);

		if (pname ~= nil) and (classToken ~= nil) then

			if (self.vs_sessionPlayerCluster.name[pserver] == nil) then
				self.vs_sessionPlayerCluster.name[pserver] = {};
			end

			if (self.vs_sessionPlayerCluster.name[pserver][pname] == nil) then
				self.vs_sessionPlayerCluster.name[pserver][pname] = {
					name = pname,
					server = pserver,
					class = classToken,
					race = race,
					factionNumber = faction, -- 0 - Horde, 1 - Alliance
					guid = UnitGUID(name),
				};
				print("Added: ", pname, pserver, faction, race, classToken, UnitGUID(name));
			end

			if (self.vs_specCluster[classToken] == nil or self.vs_specCluster[classToken][talentSpec] == nil) then
				print("Unhandled spec : ", classToken, talentSpec);
			end
		end
	end
end


-- Вроде бы функция может определять игроков на арене(во всяком случае, похожий механизм работает в гладиусе)
function This:isValidUnit(in_unit)
	return strfind(in_unit, "arena") and not strfind(in_unit, "Pet");
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-- 
function This:battleScoreUpDate()
	if (self.vn_runtimeCD <= 0) then
		self:specInspectorScript();
		self.vn_runtimeCD = self.cn_specInscpectorCooldown;
	end
end


-- 
function This:arenaSpecsUpDate()
	if (self.vn_runtimeCD <= 0) then
		self:specInspectorScript();
		self.vn_runtimeCD = self.cn_specInscpectorCooldown;
	end
end


-- Собственно парсер...ну и собственно отключен...
function This:arenaSpecParser(in_self, event, unit)
	--print("unit action detected: ", unit)
	--if unit == nil or not IsValidUnit(unit) then
		--return
	--end


	--print("unit action detected: ", event, unit)
	

	--local id = string.match(unit, "arena(%d)")
	--local specID = GetArenaOpponentSpec(id)
	--if specID and specID > 0 then
		--local id, name, description, icon, background, role, class = GetSpecializationInfoByID(specID)	
		--print("dump ", name, class)
		--if arenaPlayerClassCluster[name] == nil then
			--arenaPlayerClassCluster[name] = class
		--end
	--end

	local _, instanceType = IsInInstance();
	if instanceType ~= "arena" or not strfind(unit, "arena") or strfind(unit, "Pet") then
		return;
	end
	if not UnitName(unit) then
		return;
	end
	local name = UnitName(unit);
	if name == UNKNOWN or not name then
		return;
	end
	--if not ACR_KS.enemy[unit] then
		--ACR_KS.Send(string.format("%s - %s", name, select(3,UnitClass(unit)) or ""), 2, unit)
		--ACR_KS.enemy[unit] = true
	--end
	if self.vs_arenaPlayerClassCluster[name] == nil then
		self.vs_arenaPlayerClassCluster[name] = select(3, UnitClass(unit));
	end
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------