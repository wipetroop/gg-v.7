local This = GGF.ModuleCreateWrapper("ACR-7", true);

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------

-- TODO: проверить производительность при таком частом апдейте
-- NOTE: да там норм было, инспектор это же не чек занимаемой памяти, так что фризов не вызывает
This.cn_specInscpectorCooldown = 10;

-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Functors
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------

This.vn_runtimeCD = 0;

-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Variable lines
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------

This.vs_sessionPlayerCluster = {
	guid = {},
	name = {},
};

This.vs_arenaPlayerClassCluster = {};

This.vs_specCluster = nil;

-- Таймер обновления спека
This.vs_specTimer = {
	tick = 0,
	interval = 60,
	stopped = false,
};

-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------

This.fr_arenaSpecDetector = CreateFrame("FRAME");
This.fr_specInspector = CreateFrame("FRAME");
This.fr_arenaSpecInspector = CreateFrame("FRAME");