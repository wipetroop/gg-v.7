local This = GGF.ModuleGetWrapper("ACR-7");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


--
function This:GetRuntimePlayerInfoByName(in_name)
	return self:getRuntimePlayerInfoByName(in_name);
end


-- Взятие кластера игроков
function This:GetSessionPlayerCluster()
	return self:getSessionPlayerCluster();
end


-- Взятие кластированного игрока
function This:GetClusteRedPlayer(in_token)
	return self:getClusteRedPlayer(in_token);
end


-- Взятие индекса звука
function This:GetSoundIndex(in_class, in_spec)
	return self:getSoundIndex(in_class, in_spec);
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- Адаптивный поиск игрока по исходнику вида "Питомец Агромании"
function This:AdaptivePlayerSearch(in_fstring)
	return self:adaptivePlayerSearch(in_fstring);
end


-- Проверка наличия определнного класса в таблице
function This:CheckSpecClusterClass(in_class)
	return self:checkSpecClusterClass(in_class);
end


-- Проверка наличия индекса звука
function This:CheckSoundIndex(in_class, in_spec)
	return self:checkSoundIndex(in_class, in_spec);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------