local This = GGF.ModuleGetWrapper("ACR-7");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.1 (Miscellaneous Variables)
function This:MPRC_MVR_INITIALIZE()
	-- TODO: вынести определение локали в сом и присобачить локализацию аддона
	if (GetLocale() == "ruRU") then
		self.vs_specCluster = self.md_specHugeCluster["ru"];
	else
		-- for the rest, usually english since it's the Default language
	end

	if (GGPlayerCluster == nil) or (GGPlayerCluster == "") then
		GGPlayerCluster = GGF.TableCopy(self.vs_sessionPlayerCluster);
	else
		self.vs_sessionPlayerCluster = GGF.TableCopy(GGPlayerCluster);
	end
end


-- п.2.6 (Main Event Callbacks)
function This:MPRC_MEC_INITIALIZE()
	self.fr_specInspector:RegisterEvent("UPDATE_BATTLEFIELD_SCORE");
	self.fr_arenaSpecInspector:RegisterEvent("ARENA_PREP_OPPONENT_SPECIALIZATIONS");

	self.fr_arenaSpecDetector:RegisterEvent("UNIT_NAME_UPDATE");

	self.fr_specInspector:SetScript("OnEvent", function(...) self:battleScoreUpDate(...) end);
	self.fr_arenaSpecInspector:SetScript("OnEvent", function(...) self:arenaSpecsUpDate(...) end);

	self.fr_arenaSpecDetector:SetScript("OnEvent", function(...) self:arenaSpecParser(...) end);
end


-- п.2.8 (After-load Settings)
function This:MPRC_ALS_INITIALIZE()
	-- NOTE: инициализируем кластер свой тушкой
	local name, server = GGM.FCS.RuntimeStorage:GetPlayerName(), GGM.FCS.RuntimeStorage:GetPlayerRealm();
	if (self.vs_sessionPlayerCluster.name[server] == nil) then
		self.vs_sessionPlayerCluster.name[server] = {};
	end

	if (self.vs_sessionPlayerCluster.name[server][name] == nil) then
		self.vs_sessionPlayerCluster.name[server][name] = {
			name = name,
			server = server,
			class = GGM.FCS.RuntimeStorage:GetPlayerClassStd(),
			race = "",
			factionNumber = GGM.FCS.RuntimeStorage:GetPlayerFactionGroupID(), -- 0 - Horde, 1 - Alliance
			gUid = UnitGUID(name),
		};
	end
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-- п.3.0 (Addon System Timer)
function This:MPRC_AST_RUNTIME(in_elapsed)
	GGF.Timer(self.vs_specTimer, in_elapsed, function()
		if (GGM.ACR.RuntimeLogger:GetLoggingFlag()) then
			self:specInspectorScript();
		end
	end);
end


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-- п.4.1 (Coup De Module)
function This:MPRC_CDM_TERMINATE()
	GGPlayerCluster = GGF.TableCopy(self.vs_sessionPlayerCluster);
end