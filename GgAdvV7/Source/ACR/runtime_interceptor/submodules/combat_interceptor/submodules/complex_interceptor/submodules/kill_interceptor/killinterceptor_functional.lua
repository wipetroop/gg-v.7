local This = GGF.ModuleGetWrapper("ACR-4-1-2-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Добавление в таблицу оверкилов от игрока
function This:addPlayerOverkill(in_jrnRec)
	local recordToken = in_jrnRec.spm_id.data.." "..in_jrnRec.tpm_id.data;
	local overkillTable = self.vs_runtimeKillInterceptor.overkillTable["p"];
   local recordCortege = overkillTable[recordToken];
	if (recordCortege == nil) then
		overkillTable[recordToken] = {};
      recordCortege = overkillTable[recordToken];
	end
	recordCortege.data = in_jrnRec;
	recordCortege.time = time();
	recordCortege.targetSP = in_jrnRec.tpm_id.data;
	self:runtimeKillCheck(in_jrnRec.spm_id.data, in_jrnRec.tpm_id.data);
end


-- api
-- Добавление в таблицу остальных оверкилов 
function This:addCommonOverkill(in_jrnRec)
   local recordToken = in_jrnRec.spm_id.data.." "..in_jrnRec.tpm_id.data;
   local overkillTable = self.vs_runtimeKillInterceptor.overkillTable["c"];
   local recordCortege = overkillTable[recordToken];
	--print(in_jrnRec.sourceID.data.." "..in_jrnRec.targetID.data)
	if (recordCortege == nil) then
		overkillTable[recordToken] = {};
      recordCortege = overkillTable[recordToken];
	end
	recordCortege.data = in_jrnRec;
	recordCortege.time = time();
	recordCortege.targetSP = in_jrnRec.tpm_id.data;

	self:runtimeKillCheck(in_jrnRec.spm_id.data, in_jrnRec.tpm_id.data);
end


-- api
-- Добавление сдохших юнитов(в т.ч. тотемы, машинки и т.д.)
function This:addUnitDied(in_jrnRec)
   local recordToken = in_jrnRec.tpm_id.data;
   local unitDiedTable = self.vs_runtimeKillInterceptor.unitDiedTable;
   local recordCortege = unitDiedTable[recordToken];
	if (recordCortege == nil) then
		unitDiedTable[recordToken] = {};
      recordCortege = unitDiedTable[recordToken];
	end
	recordCortege.data = in_jrnRec
	recordCortege.time = time();

	self:runtimeKillCheck(nil, in_jrnRec.tpm_id.data);
end


-- api
-- Добавление сдохших юнитов(в т.ч. тотемы, машинки и т.д.)
function This:addEnvironmentDamage(in_jrnRec)
   local recordToken = in_jrnRec.tpm_id.data;
   local envDamageTable = self.vs_runtimeKillInterceptor.envDamageTable;
   local recordCortege = envDamageTable[recordToken];
	if (recordCortege == nil) then
		envDamageTable[recordToken] = {};
      recordCortege = envDamageTable[recordToken];
	end
	recordCortege.data = in_jrnRec
	recordCortege.time = time();

	self:runtimeKillCheck(nil, in_jrnRec.tpm_id.data);
end


-- Проверка наличия двух упоминаний одной цели в логе оверкилов или логе урона от текстур и логе сдохших юнитов
-- Если первый арг пустой, то вызов был из сдохших юнитов или урона от текстур
function This:runtimeKillCheck(srcID, trgID)
	local mstt = "";
	--print("rkc start");
	if (srcID ~= nil) then
	 	mstt = srcID.." "..trgID;
	end

	if (self.vs_runtimeKillInterceptor.unitDiedTable[trgID] ~= nil) then
		if (srcID ~= nil) then
			if (self.vs_runtimeKillInterceptor.overkillTable["p"][mstt] ~= nil) then
				GGM.ACR.EmoteAnnouncer:ForcePlayerKill(self.vs_runtimeKillInterceptor.overkillTable["p"][mstt].data);
				self.vs_runtimeKillInterceptor.overkillTable["p"][mstt] = nil;
				self.vs_runtimeKillInterceptor.unitDiedTable[trgID] = nil;
				
				return;
			end
			if (self.vs_runtimeKillInterceptor.overkillTable["c"][mstt] ~= nil) then
				self:forceCommonKill(self.vs_runtimeKillInterceptor.overkillTable["c"][mstt].data);
				self.vs_runtimeKillInterceptor.overkillTable["c"][mstt] = nil;
				self.vs_runtimeKillInterceptor.unitDiedTable[trgID] = nil;
				
				return;
			end
		else
			local t_ptr = nil;
			local t_ind = 0;
			for k,v in pairs(self.vs_runtimeKillInterceptor.overkillTable["p"]) do
				if v.targetSP == trgID then
					t_ptr = v;
					t_ind = k;
				end
			end
			if (t_ptr ~= nil) then
				--print("pc kill Player : ", t_ptr.data.sourceName.data, t_ptr.data.sourceID.data, t_ptr.data.sourceFlag.data, t_ptr.data.targetName.data, t_ptr.data.targetID.data, t_ptr.data.targetFlag.data, t_ptr.data.spellID.data, t_ptr.data.isCritical.data, t_ptr.data.isMulticast.data);
				GGM.ACR.EmoteAnnouncer:ForcePlayerKill(t_ptr.data);
				self.vs_runtimeKillInterceptor.overkillTable["p"][t_ind] = nil;
				self.vs_runtimeKillInterceptor.unitDiedTable[trgID] = nil;

				return;
			end

			for k,v in pairs(self.vs_runtimeKillInterceptor.overkillTable["c"]) do
				if v.targetSP == trgID then
					t_ptr = v;
					t_ind = k;
				end
			end
			if (t_ptr ~= nil) then
				--print("pc kill common : ", t_ptr.data.sourceName.data, t_ptr.data.sourceID.data, t_ptr.data.sourceFlag.data, t_ptr.data.targetName.data, t_ptr.data.targetID.data, t_ptr.data.targetFlag.data, t_ptr.data.spellID.data, t_ptr.data.isCritical.data, t_ptr.data.isMulticast.data);
				self:forceCommonKill(t_ptr.data);
				self.vs_runtimeKillInterceptor.overkillTable["c"][t_ind] = nil;
				self.vs_runtimeKillInterceptor.unitDiedTable[trgID] = nil;

				return;
			end
			
		end

		if self.vs_runtimeKillInterceptor.envDamageTable[trgID] ~= nil then
			self:forceEnvironmentKill(self.vs_runtimeKillInterceptor.envDamageTable[trgID].data);
			self.vs_runtimeKillInterceptor.envDamageTable[trgID] = nil;
			self.vs_runtimeKillInterceptor.unitDiedTable[trgID] = nil;
		end
	end
end


-- Очистка таблицы килов...
-- Warning: Спорный момент: таймер у килов 2 сек, а проверка каждые 5...
function This:refreshRKITable()
	for k,v in pairs(self.vs_runtimeKillInterceptor.unitDiedTable) do
		if (time() - v.time > self.cn_interceptorTimeGate) then
			self.vs_runtimeKillInterceptor.unitDiedTable[k] = nil;
			--print(v.time, time());
		end
	end

	for k,v in pairs(self.vs_runtimeKillInterceptor.overkillTable["c"]) do
		if (time() - v.time > self.cn_interceptorTimeGate) then
			self.vs_runtimeKillInterceptor.overkillTable["c"][k] = nil;
			--print(v.time, time());
		end
	end

	for k,v in pairs(self.vs_runtimeKillInterceptor.overkillTable["p"]) do
		if (time() - v.time > self.cn_interceptorTimeGate) then
			self.vs_runtimeKillInterceptor.overkillTable["p"][k] = nil;
			--print(v.time, time());
		end
	end

	for k,v in pairs(self.vs_runtimeKillInterceptor.envDaMageTable) do
		if (time() - v.time > self.cn_interceptorTimeGate) then
			self.vs_runtimeKillInterceptor.envDaMageTable[k] = nil;
			--print(v.time, time());
		end
	end
end


-- Эмм....ну вообще обработчик подтвержденных килов...но по факту трансивер получился..
function This:forceCommonKill(in_data)
	--self:confirmUnitDeath(in_data);
	-- Туда кроме этих аргументов больше ничего передавать не надо!!
	--ACR_KS.proceedKill(in_data.sourceName.data, in_data.sourceID.data, in_data.sourceFlag.data, in_data.targetName.data, in_data.targetID.data, in_data.targetFlag.data, in_data.spellID.data, in_data.isCritical.data, in_data.isMulticast.data);
	--ACR_KS_TF:AnnounceKill(in_data);
	GGM.GUI.KillFeed:ProceedKill(in_data);
end


-- Вот тут не факт, что сработает...
function This:forceEnvironmentKill(in_data)
	--self:confirmUnitDeath(in_data);

	--ACR_KS_TF:AnnounceEnvDeath(in_data);
	GGM.GUI.KillFeed:ProceedKill(in_data);

	if (in_data.tpm_name.data == GGM.FCS.RuntimeStorage:GetPlayerName()) then
		GGM.ACR.EmoteAnnouncer:ForceDenied();
	end
end

-- api
-- Вывод сообщения об убийстве
function This:kMPrint(in_killText, in_killCount, in_killType, in_noType)
	if (true) then --(GGM.FCS.RuntimeStorage:GetACRCombatLogging()) then
		if (true) then --(GGM.FCS.RuntimeStorage:GetDebugModeFlag()) then
			GGM.IOC.OutputController:RawPrint(in_killText);
			GGM.IOC.OutputController:RawPrint(in_killCount);
			if (in_noType == 0) then
				GGM.IOC.OutputController:RawPrint(in_killType);
			end
		else
			SendChatMessage(in_killText, "EMOTE");
			SendChatMessage(in_killCount, "EMOTE");
			if (in_noType == 0) then
				SendChatMessage(in_killType, "EMOTE");
			end
		end
	end
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------