local This = GGF.ModuleGetWrapper("ACR-4-1-2-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- Перехватчик отраженных обратно спеллов(пока что всех(даже смерчей, хексов, шипов и т.д.))
function This:refTableAdd(in_sourceName, in_spellID)
	if (self.vs_reflectTable[in_sourceName] == nil) then
		self.vs_reflectTable[in_sourceName] = {};
	end
	self.vs_reflectTable[in_sourceName][in_spellID] = self.cn_maxDotTickTimeSpan;
end


-- Перехватчик самоубийств спеллами или периодическим спелл уроном(короче, тем, что отразиться обратно могло)
function This:reflectTableCheck(sourceName, spellID)
	if (self.vs_reflectTable[in_sourceName] == nil) then
		return false;
	end
	if (self.vs_reflectTable[in_sourceName][in_spellID] == nil) then
		return false;
	end
	return true;
end


-- Очистка таблицы отраженных
function This:tableRefresh()
	--reflectTable[sourceID][spellID].timer
	for k,v in pairs(self.vs_reflectTable) do
		for m,n in pairs(v) do
			if (n > 5) then
				n = n - 5;
			else
				v[m] = nil
			end
		end
	end
	--printCodeDebugStreamLine("#info#","reflect table refreshed")
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------