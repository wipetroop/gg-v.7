local This = GGF.ModuleGetWrapper("ACR-4-1-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- message prototype
-- <Player> killed <enemy unit>(BG) by <skill alt. name>(<skill ID>) deals <daMage pts.> daMage
-- <Player> gets <KillCount> kill(s) in series! <Combo nt.>
function This:eventMasterInterceptor(in_timestamp, in_eventType, ...)
	-- CLEU - COMBTA_LOG_EVENT_UNFILTERED
	-- arguments:							arg1			arg2			arg3			arg4			arg5			arg6			arg7			arg8			arg9			arg10			arg11			arg12			arg13			arg14			arg15			arg16			arg17			arg18			arg19				arg20			arg21			arg22			arg23			arg24			arg25			arg26			arg27
	-- raw									in_dataTable	in_event		in_timestamp	in_eventType	in_hideCaster	arg6			arg7			arg8			arg9			arg10			arg11			arg12			arg13			arg14			arg15			arg16			arg17			arg18			arg19				arg20			arg21			arg22			arg23			arg24			arg25			arg26			arg27
	-- п.1A		SWING_DAMage				N/A				CLEU			timestamp		daMageType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				daMage			overkill		daMageSchool	isResisted		isBlocked		isAbsorbed			isCritical		N/A				N/A				N/A				N/A				N/A				N/A				N/A

	-- п.1B		RANGE_DAMage				N/A				CLEU			timestamp		daMageType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool		daMage			overkill		daMageSchool		isResisted		isBlocked		isAbsorbed		isCritical		isGlancing		isCrushing		isOffHand		isMultistrike
	-- п.1B		SPELL_DAMage				N/A				CLEU			timestamp		daMageType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool		daMage			overkill		daMageSchool		isResisted		isBlocked		isAbsorbed		isCritical		isGlancing		isCrushing		isOffHand		isMultistrike
	-- п.1B		SWING_PERIODIC_DAMage		N/A				CLEU			timestamp		daMageType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool		daMage			overkill		daMageSchool		isResisted		isBlocked		isAbsorbed		isCritical		isGlancing		isCrushing		isOffHand		isMultistrike
	-- п.1B		SPELL_PERIODIC_DAMage		N/A				CLEU			timestamp		daMageType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool		daMage			overkill		daMageSchool		isResisted		isBlocked		isAbsorbed		isCritical		isGlancing		isCrushing		isOffHand		isMultistrike

	-- п.2		ENVIRONMENTAL_DAMage		N/A				CLEU			timestamp		N/A				N/A				N/A				N/A				N/A				N/A				targetGUiD		targetName		targetFlag		N/A				daMageType		daMage			N/A				N/A				N/A				N/A					N/A				N/A				N/A				N/A				N/A				N/A				N/A				N/A

	-- п.3		UNIT_DIED					N/A				CLEU			timestamp		N/A				N/A				N/A				N/A				N/A				N/A				targetGUiD		targetName		targetFlag		N/A				N/A				N/A				N/A				N/A				N/A				N/A					N/A				N/A				N/A				N/A				N/A				N/A				N/A				N/A
	-- п.3		UNIT_DESTROYED				N/A				CLEU			timestamp		N/A				N/A				N/A				N/A				N/A				N/A				targetGUiD		targetName		targetFlag		N/A				N/A				N/A				N/A				N/A				N/A				N/A					N/A				N/A				N/A				N/A				N/A				N/A				N/A				N/A

	-- п.4A		SWING_MISSED				N/A				CLEU			timestamp		missType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				missSubType		isOffHand		daMage			N/A				N/A				N/A					N/A				N/A				N/A				N/A				N/A				N/A				N/A				N/A

	-- п.4B		RANGE_MISSED				N/A				CLEU			timestamp		missType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool		missSubType		isOffHand		daMage				N/A				N/A				N/A				N/A				N/A				N/A				N/A				N/A
	-- п.4B		SPELL_MISSED				N/A				CLEU			timestamp		missType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool		missSubType		isOffHand		daMage				N/A				N/A				N/A				N/A				N/A				N/A				N/A				N/A
	-- п.4B		SPELL_PERIODIC_MISSED		N/A				CLEU			timestamp		missType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool		missSubType		isOffHand		daMage				N/A				N/A				N/A				N/A				N/A				N/A				N/A				N/A

	-- п.5		SPELL_HEAL					N/A				CLEU			timestamp		healType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool		heal			overheal		absorbed			isCritical		N/A				N/A				N/A				N/A				N/A				N/A				N/A
	-- п.5		SPELL_PERIODIC_HEAL			N/A				CLEU			timestamp		healType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool		heal			overheal		absorbed			isCritical		N/A				N/A				N/A				N/A				N/A				N/A				N/A

	-- п.6		SPELL_INTERRUPT				N/A				CLEU			timestamp		interruptType	N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool		extraSpellID	extraSpellName	extraSpellSchool	N/A				N/A				N/A				N/A				N/A				N/A				N/A				N/A

	-- п.7		SPELL_DISPEL				N/A				CLEU			timestamp		dispelType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool		extraSpellID	extraSpellName	extraSpellSchool	auraType		N/A				N/A				N/A				N/A				N/A				N/A				N/A
	-- п.7		SPELL_DISPEL_FAILED			N/A				CLEU			timestamp		dispelType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool		extraSpellID	extraSpellName	extraSpellSchool	auraType		N/A				N/A				N/A				N/A				N/A				N/A				N/A

	-- п.8		SPELL_STOLEN				N/A				CLEU			timestamp		stealType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool		extraSpellID	extraSpellName	extraSpellSchool	auraType		N/A				N/A				N/A				N/A				N/A				N/A				N/A

	-- п.9A		SPELL_AURA_BROKEN			N/A				CLEU			timestamp		actionType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool		extraSpellID	extraSpellName	extraSpellSchool	auraType		N/A				N/A				N/A				N/A				N/A				N/A				N/A

	-- п.9B		SPELL_AURA_BROKEN_SPELL		N/A				CLEU			timestamp		actionType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool		extraSpellID	extraSpellName	extraSpellSchool	auraType		N/A				N/A				N/A				N/A				N/A				N/A				N/A

	-- п.10		SPELL_AURA_APPLIED			N/A				CLEU			timestamp
	-- п.10		SPELL_AURA_APPLIED_DOSE		N/A				CLEU			timestamp
	-- п.10		SPELL_AURA_REMOVED			N/A				CLEU			timestamp
	-- п.10		SPELL_AURA_REMOVED_DOSE		N/A				CLEU			timestamp
	-- п.10		SPELL_AURA_REFRESH			N/A				CLEU			timestamp

	-- п.11		SPELL_CAST_START			N/A				CLEU			timestamp		castType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool
	-- п.11		SPELL_CAST_SUCCESS			N/A				CLEU			timestamp		castType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool
	-- п.11		SPELL_CAST_FAILED			N/A				CLEU			timestamp		castType		N/A				sourceGUiD		sourceName		sourceFlag		N/A				targetGUiD		targetName		targetFlag		N/A				spellID			spellName		spellSchool

	-- п.12		SPELL_SUMMON				N/A				CLEU			timestamp

	-- п.13		SPELL_CREATE				N/A				CLEU			timestamp

	-- п.14		SPELL_ENERGIZE				N/A				CLEU			timestamp
	-- п.14		SPELL_PERIODIC_ENERGIZE		N/A				CLEU			timestamp

	-- п.15		SPELL_DRAIN					N/A				CLEU			timestamp

	-- п.16		SPELL_LEECH					N/A				CLEU			timestamp

	-- п.17		ENCHANT_APPLIED				N/A				CLEU			timestamp
	-- п.17		ENCHANT_REMOVED				N/A				CLEU			timestamp

	-- п.18		PARTY_KILL					N/A				CLEU			timestamp


	-- arguments :				п.1								п.2									п.3							п.4						п.5								п.6									п.7							п.8						п.9						п.10					п.11						п.12						п.13
	-- combat log event:	SWING_DAMage		SPELL/SPELL_RERIODIC/RANGE_DAMage			ENVIRONMENT_DAMage			UNIT_DIED/DESTROYED			SWING_MISSED		SPELL/SPELL_RERIODIC/RANGE_MISSED		SPELL/SPELL_PERIODIC_HEAL		SPELL_INTERRUPT				SPELL_DISPEL		SPELL_DISPEL_FAILED			SPELL_STOLEN		SPELL_AURA_BROKEN_SPELL			SPELL_CAST_FAILED
	-- in_dataTable
	-- in_event(COMBAT_LOG_EVENT_UNFILTERed)
	-- in_timestamp			timestamp						timestamp							timestamp					timestamp				timestamp						timestamp							timestamp					timestamp				timestamp				timestamp				timestamp					timestamp					timestamp
	-- in_eventType			daMageType						daMageType																				missType						missType							healType					interruptType			dispelType				dispelType				stealType					actionType					castType
	-- in_hideCaster

	-- arg6					sourceGUiD						sourceGUiD																				sourceGUiD						sourceGUiD							sourceGUiD					sourceGUiD				sourceGUiD				sourceGUiD				sourceGUiD					sourceGUiD					sourceGUiD
	-- arg7					sourceName						sourceName																				sourceName						sourceName							sourceName					sourceName				sourceName				sourceName				sourceName					sourceName					sourceName
	-- arg8					sourceFlag						sourceFlag																				sourceFlag						sourceFlag							sourceFlag					sourceFlag				sourceFlag				sourceFlag				sourceFlag					sourceFlag					sourceFlag
	-- arg9																																																																																														
	-- arg10				targetGUiD						targetGUiD							targetGUiD					targetGUiD				targetGUiD						targetGUiD							targetGUiD					targetGUiD				targetGUiD				targetGUiD				targetGUiD					targetGUiD					targetGUiD
	-- arg11				targetName						targetName							targetName					targetName				targetName						targetName							targetName					targetName				targetName				targetName				targetName					targetName					targetName
	-- arg12				targetFlag						targetFlag							targetFlag					targetFlag				targetFlag						targetFlag							targetFlag					targetFlag				targetFlag				targetFlag				targetFlag					targetFlag					targetFlag
	-- arg13																																																																																													
	-- arg14				daMage							spellID								daMageType											missSubType						spellID								spellID						spellID					spellID					spellID					spellID						spellID						spellID
	-- arg15				overkill						spellName							daMage												isOffHand						spellName							spellName					spellName				spellName				spellName				spellName					spellName					spellName
	-- arg16				daMageSchool					spellSchool																				daMage							spellSchool							spellSchool					spellSchool				spellSchool				spellSchool				spellSchool					spellSchool					spellSchool
	-- arg17				isResisted						daMage																													missSubType							heal						extraSpellID			extraSpellID			extraSpellID			extraSpellID				extraSpellID				failedType
	-- arg18				isBlocked						overkill																												isOffHand							overheal					extraSpellName			extraSpellName			extraSpellName			extraSpellName				extraSpellName
	-- arg19				isAbsorbed						daMageSchool																											daMage								absorbed					extraSpellSchool		extraSpellSchool		extraSpellSchool		extraSpellSchool			extraSpellSchool
	-- arg20				isCritical						isResisted																																					isCritical											auraType										auraType					auraType
	-- arg21												isBlocked
	-- arg22												isAbsorbed
	-- arg23												isCritical
	-- arg24												isGlancing
	-- arg25												isCrushing
	-- arg26												isOffHand
	-- arg27												isMultistrike

	-- TODO: доделать для SPELL_ABSROBED, SPELL_INSTAKILL
	-- NOTE: тип эвента дублирован для более удобной отладки через дефолтный перехватчик
	--print("event", in_timestamp, in_eventType, ...);
	self.fn_interceptorMap[in_eventType](in_eventType, in_timestamp, ...);
end


-- Дефолтный обработчик эвента(дампер)(используется как дефолтный в таблице перехватчиков)
function This:DefaultInterceptor(...)
	-- NOTE: занулено до ремонта перехватчика
	--print("event interceptor default", ...);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------