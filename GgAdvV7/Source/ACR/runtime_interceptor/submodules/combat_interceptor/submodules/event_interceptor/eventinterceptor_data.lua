local This = GGF.ModuleCreateWrapper("ACR-4-1-4", false);

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Functors
-------------------------------------------------------------------------------

GGF.VersionSplitterProc({
	{
		versionInterval = {GGD.TocVersionList.Vanilla, GGD.TocVersionList.WotLK},
		action = function()
         This.fn_interceptorMap = {
         	-- п.1A
         	["SWING_DAMAGE"] = function(...) This.fn_interceptorMap.point1A(...) end,
         	point1A = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, in_arg14, in_arg15, _, _, _, _, in_arg20, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.DamageInterceptor, {
         			bcs_timestamp = in_timestamp,
         			etm_type = in_etype,
         			msd_amount = in_arg14,
         			msd_id = 6603,
         			msd_isCritical = in_arg20,
         			msd_overkill = in_arg15,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.1B
         	["RANGE_DAMAGE"] = function(...) This.fn_interceptorMap.point1B(...) end,
         	["SPELL_DAMAGE"] = function(...) This.fn_interceptorMap.point1B(...) end,
         	["SWING_PERIODIC_DAMAGE"] = function(...) This.fn_interceptorMap.point1B(...) end,
         	["SPELL_PERIODIC_DAMAGE"] = function(...) This.fn_interceptorMap.point1B(...) end,
         	point1B = function(in_etype, in_timestamp, in_arg3, in_arg4, in_arg5, in_arg6, in_arg7, in_arg8, in_arg9, in_arg10, in_arg11, in_arg12, in_arg13, in_arg14, ...) 
         		--print("point1B", in_etype, in_timestamp, "spm_id -", in_arg3, 'spm_name -', in_arg4, 'spm_flag -', in_arg5, 'tpm_id -', in_arg6, 'tpm_name -', in_arg7, 'tpm_flag -', in_arg8, 'msd_id -', in_arg9, 'spell_name -', in_arg10, 'msd_amount -', in_arg12, 'msd_overkill -', in_arg13, 'msd_isCritical -', in_arg14, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.DamageInterceptor, {
         			bcs_timestamp = in_timestamp,
         			etm_type = in_etype,
         			msd_amount = in_arg12,
         			msd_id = in_arg9,
         			msd_isCritical = in_arg14,
         			msd_overkill = in_arg13,
         			spm_flag = in_arg5,
         			spm_id = in_arg3,
         			spm_name = in_arg4,
         			tpm_flag = in_arg8,
         			tpm_id = in_arg6,
         			tpm_name = in_arg7,
         		});
         	end,

         	-- п.2
         	["ENVIRONMENTAL_DAMAGE"] = function(...) This.fn_interceptorMap.point2(...) end,
         	point2 = function(in_etype, in_timestamp, _, _, _, _, _, in_arg10, in_arg11, in_arg12, _, in_arg14, in_arg15, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.DamageInterceptor, {
         			bcs_timestamp = in_timestamp,
         			etm_extraType = in_arg14,
         			etm_type = in_etype,
         			msd_amount = in_arg15,
         			msd_id = in_arg14,	-- тут дублируется подтип дамага а не id(все типы должны быть в таблице спеллов)
         			msd_overkill = -1,
         			spm_flag = 0x0,
         			spm_id = 0,
         			spm_name = "SERVER",
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.3
         	["UNIT_DIED"] = function(...) This.fn_interceptorMap.point3(...) end,
         	["UNIT_DESTROYED"] = function(...) This.fn_interceptorMap.point3(...) end,
         	point3 = function(in_etype, in_timestamp, a1, a2, a3, in_arg10, in_arg11, in_arg12, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.DeathInterceptor, {
         			bcs_timestamp = in_timestamp,
         			etm_type = in_etype,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.4A
         	["SWING_MISSED"] = function(...) This.fn_interceptorMap.point4A(...) end,
         	point4A = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, _, in_arg15, in_arg16, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.MissInterceptor, {
         			bcs_timestamp = in_timestamp,
         			etm_extraType = in_arg14,
         			etm_type = in_etype,
         			msd_amount = in_arg16,
         			msd_id = 6603,
         			msd_isOffhand = in_arg15,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.4B
         	["RANGE_MISSED"] = function(...) This.fn_interceptorMap.point4B(...) end,
         	["SPELL_MISSED"] = function(...) This.fn_interceptorMap.point4B(...) end,
         	["SPELL_PERIODIC_MISSED"] = function(...) This.fn_interceptorMap.point4B(...) end,
         	point4B = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, _, in_arg18, in_arg19, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.MissInterceptor, {
         			bcs_timestamp = in_timestamp,
         			etm_extraType = in_arg17,
         			etm_type = in_etype,
         			msd_amount = in_arg19,
         			msd_id = in_arg14,
         			msd_isOffhand = in_arg18,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.5
         	["SPELL_HEAL"] = function(...) This.fn_interceptorMap.point5(...) end,
         	["SPELL_PERIODIC_HEAL"] = function(...) This.fn_interceptorMap.point5(...) end,
         	point5 = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, in_arg18, in_arg19, in_arg20, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.HealInterceptor, {
         			bcs_timestamp = in_timestamp,
         			etm_type = in_etype,
         			msd_absorbed = in_arg19,
         			msd_amount = in_arg17,
         			msd_id = in_arg14,
         			msd_isCritical = in_arg20,
         			msd_overheal = in_arg18,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.6
         	["SPELL_INTERRUPT"] = function(...) This.fn_interceptorMap.point6(...) end,
         	point6 = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.InterruptInterceptor, {
         			bcs_timestamp = in_timestamp,
         			esd_id = in_arg17,
         			etm_type = in_etype,
         			msd_id = in_arg14,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.7
         	["SPELL_DISPEL"] = function(...) This.fn_interceptorMap.point7(...) end,
         	["SPELL_DISPEL_FAILED"] = function(...) This.fn_interceptorMap.point7(...) end,
         	point7 = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, _, _, in_arg20, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.DispelInterceptor, {
         			bcs_timestamp = in_timestamp,
         			esd_id = in_arg17,
         			etm_extraType = in_arg20,
         			etm_type = in_etype,
         			msd_id = in_arg14,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.8
         	["SPELL_STOLEN"] = function(...) This.fn_interceptorMap.point8(...) end,
         	point8 = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, _, _, in_arg20, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.StealInterceptor, {
         			bcs_timestamp = in_timestamp,
         			esd_id = in_arg17,
         			etm_extraType = in_arg20,
         			etm_type = in_etype,
         			msd_id = in_arg14,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.9A
         	["SPELL_AURA_BROKEN"] = function(...) This.fn_interceptorMap.point9A(...) end,
         	point9A = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.BrakeInterceptor, {
         			bcs_timestamp = in_timestamp,
         			esd_id = 0,
         			etm_extraType = in_arg17,
         			etm_type = in_etype,
         			msd_id = in_arg14,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.9B
         	["SPELL_AURA_BROKEN_SPELL"] = function(...) This.fn_interceptorMap.point9B(...) end,
         	point9B = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, _, _, in_arg20, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.BrakeInterceptor, {
         			bcs_timestamp = in_timestamp,
         			esd_id = in_arg17,
         			etm_extraType = in_arg20,
         			etm_type = in_etype,
         			msd_id = in_arg14,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.10
         	["SPELL_AURA_APPLIED"] = function(...) This.fn_interceptorMap.point10(...) end,
         	["SPELL_AURA_APPLIED_DOSE"] = function(...) This.fn_interceptorMap.point10(...) end,
         	["SPELL_AURA_REMOVED"] = function(...) This.fn_interceptorMap.point10(...) end,
         	["SPELL_AURA_REMOVED_DOSE"] = function(...) This.fn_interceptorMap.point10(...) end,
         	["SPELL_AURA_REFRESH"] = function(...) This.fn_interceptorMap.point10(...) end,
         	point10 = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17,...)
         		GGF.MDL.InterceptEvent(GGM.ACR.AuraInterceptor, {
         			bcs_timestamp = in_timestamp,
         			etm_extraType = in_arg17,
         			etm_type = in_etype,
         			msd_amount = in_arg18,
         			msd_id = in_arg14,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.11
         	["SPELL_CAST_START"] = function(...) This.fn_interceptorMap.point11(...) end,
         	["SPELL_CAST_SUCCESS"] = function(...) This.fn_interceptorMap.point11(...) end,
         	["SPELL_CAST_FAILED"] = function(...) This.fn_interceptorMap.point11(...) end,
         	point11 = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.CastInterceptor, {
         			bcs_timestamp = in_timestamp,
         			etm_extraType = in_arg17,
         			etm_type = in_etype,
         			msd_id = in_arg14,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.12
         	["SPELL_SUMMON"] = function(...) This.fn_interceptorMap.point12(...) end,
         	point12 = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, in_arg14, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.SummonInterceptor, {
         			bcs_timestamp = in_timestamp,
         			etm_type = in_etype,
         			msd_id = in_arg14,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.13
         	["SPELL_CREATE"] = function(...) This.fn_interceptorMap.point13(...) end,
         	point13 = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, in_arg14, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.CreateInterceptor, {
         			bcs_timestamp = in_timestamp,
         			etm_type = in_etype,
         			msd_id = in_arg14,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.14
         	["SPELL_ENERGIZE"] = function(...) This.fn_interceptorMap.point14(...) end,
         	["SPELL_PERIODIC_ENERGIZE"] = function(...) This.fn_interceptorMap.point14(...) end,
         	point14 = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, in_arg18, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.EnergizeInterceptor, {
         			bcs_timestamp = in_timestamp,
         			etm_extraType = in_arg18,
         			etm_type = in_etype,
         			msd_amount = in_arg17,
         			msd_id = in_arg14,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.15
         	["SPELL_DRAIN"] = function(...) This.fn_interceptorMap.point15(...) end,
         	point15 = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, in_arg18, in_arg19, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.DrainInterceptor, {
         			bcs_timestamp = in_timestamp,
         			etm_extraType = in_arg18,
         			etm_type = in_etype,
         			msd_amount = in_arg17,
         			msd_extraAmount = in_arg19,
         			msd_id = in_arg14,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.16
         	["SPELL_LEECH"] = function(...) This.fn_interceptorMap.point16(...) end,
         	point16 = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, in_arg18, in_arg19, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.LeechInterceptor, {
         			bcs_timestamp = in_timestamp,
         			etm_extraType = in_arg18,
         			etm_type = in_etype,
         			msd_amount = in_arg17,
         			msd_extraAmount = in_arg19,
         			msd_id = in_arg14,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.17
         	["ENCHANT_APPLIED"] = function(...) This.fn_interceptorMap.point17(...) end,
         	["ENCHANT_REMOVED"] = function(...) This.fn_interceptorMap.point17(...) end,
         	point17 = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.EnchantInterceptor, {
         			bcs_timestamp = in_timestamp,
         			etm_type = in_etype,
         			msd_id = in_arg14,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	-- п.18
         	["PARTY_KILL"] = function(...) This.fn_interceptorMap.point18(...) end,
         	point18 = function(in_etype, in_timestamp, in_arg6, in_arg7, in_arg8, in_arg10, in_arg11, in_arg12, _, in_arg14, ...)
         		GGF.MDL.InterceptEvent(GGM.ACR.DeathInterceptor, {
         			bcs_timestamp = in_timestamp,
         			etm_type = in_etype,
         			msd_id = in_arg14,
         			spm_flag = in_arg8,
         			spm_id = in_arg6,
         			spm_name = in_arg7,
         			tpm_flag = in_arg12,
         			tpm_id = in_arg10,
         			tpm_name = in_arg11,
         		});
         	end,

         	Default = function(...) This:DefaultInterceptor(...) end,
         }
      end,
   },
   {
      versionInterval = {GGD.TocVersionList.Cata, GGD.TocVersionList.Invalid},
      action = function()
         This.fn_interceptorMap = {
            -- п.1A
            ["SWING_DAMAGE"] = function(...) This.fn_interceptorMap.point1A(...) end,
            point1A = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, in_arg15, _, _, _, _, in_arg20, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.DamageInterceptor, {
                  bcs_timestamp = in_timestamp,
                  etm_type = in_etype,
                  msd_amount = in_arg14,
                  msd_id = 6603,
                  msd_isCritical = in_arg20,
                  msd_overkill = in_arg15,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.1B
            ["RANGE_DAMAGE"] = function(...) This.fn_interceptorMap.point1B(...) end,
            ["SPELL_DAMAGE"] = function(...) This.fn_interceptorMap.point1B(...) end,
            ["SWING_PERIODIC_DAMAGE"] = function(...) This.fn_interceptorMap.point1B(...) end,
            ["SPELL_PERIODIC_DAMAGE"] = function(...) This.fn_interceptorMap.point1B(...) end,
            point1B = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, in_arg18, _, _, _, _, in_arg23, ...) 
               GGF.MDL.InterceptEvent(GGM.ACR.DamageInterceptor, {
                  bcs_timestamp = in_timestamp,
                  etm_type = in_etype,
                  msd_amount = in_arg17,
                  msd_id = in_arg14,
                  msd_isCritical = in_arg23,
                  msd_overkill = in_arg18,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.2
            ["ENVIRONMENTAL_DAMAGE"] = function(...) This.fn_interceptorMap.point2(...) end,
            point2 = function(in_etype, in_timestamp, _, _, _, _, _, in_arg10, in_arg11, in_arg12, _, in_arg14, in_arg15, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.DamageInterceptor, {
                  bcs_timestamp = in_timestamp,
                  etm_extraType = in_arg14,
                  etm_type = in_etype,
                  msd_amount = in_arg15,
                  msd_id = in_arg14,   -- тут дублируется подтип дамага а не id(все типы должны быть в таблице спеллов)
                  msd_overkill = -1,
                  spm_flag = 0x0,
                  spm_id = 0,
                  spm_name = "SERVER",
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.3
            ["UNIT_DIED"] = function(...) This.fn_interceptorMap.point3(...) end,
            ["UNIT_DESTROYED"] = function(...) This.fn_interceptorMap.point3(...) end,
            point3 = function(in_etype, in_timestamp, _, _, _, _, _, in_arg10, in_arg11, in_arg12, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.DeathInterceptor, {
                  bcs_timestamp = in_timestamp,
                  etm_type = in_etype,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.4A
            ["SWING_MISSED"] = function(...) This.fn_interceptorMap.point4A(...) end,
            point4A = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, _, in_arg15, in_arg16, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.MissInterceptor, {
                  bcs_timestamp = in_timestamp,
                  etm_extraType = in_arg14,
                  etm_type = in_etype,
                  msd_amount = in_arg16,
                  msd_id = 6603,
                  msd_isOffhand = in_arg15,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.4B
            ["RANGE_MISSED"] = function(...) This.fn_interceptorMap.point4B(...) end,
            ["SPELL_MISSED"] = function(...) This.fn_interceptorMap.point4B(...) end,
            ["SPELL_PERIODIC_MISSED"] = function(...) This.fn_interceptorMap.point4B(...) end,
            point4B = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, _, in_arg18, in_arg19, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.MissInterceptor, {
                  bcs_timestamp = in_timestamp,
                  etm_extraType = in_arg17,
                  etm_type = in_etype,
                  msd_amount = in_arg19,
                  msd_id = in_arg14,
                  msd_isOffhand = in_arg18,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.5
            ["SPELL_HEAL"] = function(...) This.fn_interceptorMap.point5(...) end,
            ["SPELL_PERIODIC_HEAL"] = function(...) This.fn_interceptorMap.point5(...) end,
            point5 = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, in_arg18, in_arg19, in_arg20, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.HealInterceptor, {
                  bcs_timestamp = in_timestamp,
                  etm_type = in_etype,
                  msd_absorbed = in_arg19,
                  msd_amount = in_arg17,
                  msd_id = in_arg14,
                  msd_isCritical = in_arg20,
                  msd_overheal = in_arg18,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.6
            ["SPELL_INTERRUPT"] = function(...) This.fn_interceptorMap.point6(...) end,
            point6 = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.InterruptInterceptor, {
                  bcs_timestamp = in_timestamp,
                  esd_id = in_arg17,
                  etm_type = in_etype,
                  msd_id = in_arg14,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.7
            ["SPELL_DISPEL"] = function(...) This.fn_interceptorMap.point7(...) end,
            ["SPELL_DISPEL_FAILED"] = function(...) This.fn_interceptorMap.point7(...) end,
            point7 = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, _, _, in_arg20, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.DispelInterceptor, {
                  bcs_timestamp = in_timestamp,
                  esd_id = in_arg17,
                  etm_extraType = in_arg20,
                  etm_type = in_etype,
                  msd_id = in_arg14,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.8
            ["SPELL_STOLEN"] = function(...) This.fn_interceptorMap.point8(...) end,
            point8 = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, _, _, in_arg20, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.StealInterceptor, {
                  bcs_timestamp = in_timestamp,
                  esd_id = in_arg17,
                  etm_extraType = in_arg20,
                  etm_type = in_etype,
                  msd_id = in_arg14,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.9A
            ["SPELL_AURA_BROKEN"] = function(...) This.fn_interceptorMap.point9A(...) end,
            point9A = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.BrakeInterceptor, {
                  bcs_timestamp = in_timestamp,
                  esd_id = 0,
                  etm_extraType = in_arg17,
                  etm_type = in_etype,
                  msd_id = in_arg14,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.9B
            ["SPELL_AURA_BROKEN_SPELL"] = function(...) This.fn_interceptorMap.point9B(...) end,
            point9B = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, _, _, in_arg20, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.BrakeInterceptor, {
                  bcs_timestamp = in_timestamp,
                  esd_id = in_arg17,
                  etm_extraType = in_arg20,
                  etm_type = in_etype,
                  msd_id = in_arg14,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.10
            ["SPELL_AURA_APPLIED"] = function(...) This.fn_interceptorMap.point10(...) end,
            ["SPELL_AURA_APPLIED_DOSE"] = function(...) This.fn_interceptorMap.point10(...) end,
            ["SPELL_AURA_REMOVED"] = function(...) This.fn_interceptorMap.point10(...) end,
            ["SPELL_AURA_REMOVED_DOSE"] = function(...) This.fn_interceptorMap.point10(...) end,
            ["SPELL_AURA_REFRESH"] = function(...) This.fn_interceptorMap.point10(...) end,
            point10 = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17,...)
               GGF.MDL.InterceptEvent(GGM.ACR.AuraInterceptor, {
                  bcs_timestamp = in_timestamp,
                  etm_extraType = in_arg17,
                  etm_type = in_etype,
                  msd_amount = in_arg18,
                  msd_id = in_arg14,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.11
            ["SPELL_CAST_START"] = function(...) This.fn_interceptorMap.point11(...) end,
            ["SPELL_CAST_SUCCESS"] = function(...) This.fn_interceptorMap.point11(...) end,
            ["SPELL_CAST_FAILED"] = function(...) This.fn_interceptorMap.point11(...) end,
            point11 = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.CastInterceptor, {
                  bcs_timestamp = in_timestamp,
                  etm_extraType = in_arg17,
                  etm_type = in_etype,
                  msd_id = in_arg14,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.12
            ["SPELL_SUMMON"] = function(...) This.fn_interceptorMap.point12(...) end,
            point12 = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.SummonInterceptor, {
                  bcs_timestamp = in_timestamp,
                  etm_type = in_etype,
                  msd_id = in_arg14,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.13
            ["SPELL_CREATE"] = function(...) This.fn_interceptorMap.point13(...) end,
            point13 = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.CreateInterceptor, {
                  bcs_timestamp = in_timestamp,
                  etm_type = in_etype,
                  msd_id = in_arg14,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.14
            ["SPELL_ENERGIZE"] = function(...) This.fn_interceptorMap.point14(...) end,
            ["SPELL_PERIODIC_ENERGIZE"] = function(...) This.fn_interceptorMap.point14(...) end,
            point14 = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, in_arg18, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.EnergizeInterceptor, {
                  bcs_timestamp = in_timestamp,
                  etm_extraType = in_arg18,
                  etm_type = in_etype,
                  msd_amount = in_arg17,
                  msd_id = in_arg14,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.15
            ["SPELL_DRAIN"] = function(...) This.fn_interceptorMap.point15(...) end,
            point15 = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, in_arg18, in_arg19, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.DrainInterceptor, {
                  bcs_timestamp = in_timestamp,
                  etm_extraType = in_arg18,
                  etm_type = in_etype,
                  msd_amount = in_arg17,
                  msd_extraAmount = in_arg19,
                  msd_id = in_arg14,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.16
            ["SPELL_LEECH"] = function(...) This.fn_interceptorMap.point16(...) end,
            point16 = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, in_arg18, in_arg19, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.LeechInterceptor, {
                  bcs_timestamp = in_timestamp,
                  etm_extraType = in_arg18,
                  etm_type = in_etype,
                  msd_amount = in_arg17,
                  msd_extraAmount = in_arg19,
                  msd_id = in_arg14,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.17
            ["ENCHANT_APPLIED"] = function(...) This.fn_interceptorMap.point17(...) end,
            ["ENCHANT_REMOVED"] = function(...) This.fn_interceptorMap.point17(...) end,
            point17 = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, _, _, in_arg17, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.EnchantInterceptor, {
                  bcs_timestamp = in_timestamp,
                  etm_type = in_etype,
                  msd_id = in_arg14,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            -- п.18
            ["PARTY_KILL"] = function(...) This.fn_interceptorMap.point18(...) end,
            point18 = function(in_etype, in_timestamp, _, in_arg6, in_arg7, in_arg8, _, in_arg10, in_arg11, in_arg12, _, in_arg14, ...)
               GGF.MDL.InterceptEvent(GGM.ACR.DeathInterceptor, {
                  bcs_timestamp = in_timestamp,
                  etm_type = in_etype,
                  msd_id = in_arg14,
                  spm_flag = in_arg8,
                  spm_id = in_arg6,
                  spm_name = in_arg7,
                  tpm_flag = in_arg12,
                  tpm_id = in_arg10,
                  tpm_name = in_arg11,
               });
            end,

            Default = function(...) This:DefaultInterceptor(...) end,
         }
      end,
   },
});

-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------