local This = GGF.ModuleCreateWrapper("ACR-4-1-1-16", false, {
	logger = function() return GGM.ACR.SummonLogger end,
	statistic = function() return GGM.ACR.SummonStatistic end,
});

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Functors
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------