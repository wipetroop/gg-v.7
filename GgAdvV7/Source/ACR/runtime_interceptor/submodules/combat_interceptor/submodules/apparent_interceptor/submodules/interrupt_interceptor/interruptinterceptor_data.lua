local This = GGF.ModuleCreateWrapper("ACR-4-1-1-12", false, {
	logger = function() return GGM.ACR.InterruptLogger end,
	statistic = function() return GGM.ACR.InterruptStatistic end,
});

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Functors
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------