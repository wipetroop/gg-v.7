local This = GGF.ModuleCreateWrapper("ACR-4-1-1-6", false, {
	logger = function() return GGM.ACR.DeathLogger end,
	statistic = function() return GGM.ACR.DeathStatistic end,
	additionalConditions = function(in_jrnRec)
      GGM.ACR.KillInterceptor:AddUnitDied(in_jrnRec);
	end
});

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Functors
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------