local This = GGF.ModuleCreateWrapper("ACR-4-1-3", false);

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------


This.cn_killSeriesTimeSpan = 18; -- 18 seconds to get a rampage

This.cn_seriesKillUpperBorder = 5;
This.cn_streakKillUpperBorder = 10;


-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------


This.cs_killSeriesDelay = {
	[1] = 2.7,
	[2] = 2,
	[3] = 2,
	[4] = 2,
};

This.cs_killStreakDelay = {
	[1] = 0,
	[2] = 0,
	[3] = 1.5,
	[4] = 1.5,
	[5] = 1.8,
	[6] = 1.5,
	[7] = 2,
	[8] = 2,
	[9] = 1.5,
	[10] = 2,
};

This.cs_killStreakMessage = {
	[1] = "",	--empty by the time
	[2] = "",	--empty by the time
	[3] = "is on Killing Spree",
	[4] = "is Dominating",
	[5] = "is on a Mega Kill streak",
	[6] = "is Unstoppable",
	[7] = "is Wicked Sick",
	[8] = "is on a Monster Kill streak",
	[9] = "is Godlike",
	[10] = "is beyond Godlike",
};

This.cs_killSeriesMessage = {
	[1] = "",
	[2] = "a Double Kill",
	[3] = "a Triple Kill",
	[4] = "an Ultra Kill",
	[5] = "a Rampage",
};


-------------------------------------------------------------------------------
-- Functors
-------------------------------------------------------------------------------


-- TODO: вынести в SND и присобачить управление в GUi(GG-47)
This.fn_deniedSound = function(x) PlaySoundFile("Interface\\AddOns\\GgAdvV7\\Sounds\\ACR\\D2\\announcer_denied.mp3") end

This.fn_killStreakSound = {
	[1] = function(x) end,	--empty by the time
	[2] = function(x) end,	--empty by the time
	[3] = function(x) PlaySoundFile("Interface\\AddOns\\GgAdvV7\\Sounds\\ACR\\D2\\announcer_kill_spree_01_enh.mp3") end,
	[4] = function(x) PlaySoundFile("Interface\\AddOns\\GgAdvV7\\Sounds\\ACR\\D2\\announcer_kill_dominate_01_enh.mp3") end,
	[5] = function(x) PlaySoundFile("Interface\\AddOns\\GgAdvV7\\Sounds\\ACR\\D2\\announcer_kill_mega_01_enh.mp3") end,
	[6] = function(x) PlaySoundFile("Interface\\AddOns\\GgAdvV7\\Sounds\\ACR\\D2\\announcer_kill_unstop_01_enh.mp3") end,
	[7] = function(x) PlaySoundFile("Interface\\AddOns\\GgAdvV7\\Sounds\\ACR\\D2\\announcer_kill_wicked_01_enh.mp3") end,
	[8] = function(x) PlaySoundFile("Interface\\AddOns\\GgAdvV7\\Sounds\\ACR\\D2\\announcer_kill_monster_01.mp3") end,
	[9] = function(x) PlaySoundFile("Interface\\AddOns\\GgAdvV7\\Sounds\\ACR\\D2\\announcer_kill_godlike_01_enh.mp3") end,
	[10] = function(x) PlaySoundFile("Interface\\AddOns\\GgAdvV7\\Sounds\\ACR\\D2\\announcer_kill_holy_01_enh.mp3") end,
};

This.fn_killSeriesSound = {
	[1] = function(x) end, --empty by the time
	[2] = function(x) PlaySoundFile("Interface\\AddOns\\GgAdvV7\\Sounds\\ACR\\D2\\announcer_kill_double_01_enh.mp3") end,
	[3] = function(x) PlaySoundFile("Interface\\AddOns\\GgAdvV7\\Sounds\\ACR\\D2\\announcer_kill_triple_01_enh.mp3") end,
	[4] = function(x) PlaySoundFile("Interface\\AddOns\\GgAdvV7\\Sounds\\ACR\\D2\\announcer_kill_ultra_01_enh.mp3") end,
	[5] = function(x) PlaySoundFile("Interface\\AddOns\\GgAdvV7\\Sounds\\ACR\\D2\\announcer_kill_rampage_01_enh.mp3") end,
};

This.fn_killSoundEffect = {
	[1] = function(x) PlaySoundFile("Interface\\AddOns\\GgAdvV7\\Sounds\\ACR\\mp3\\proceed_kill_01.mp3") end,
	[2] = function(x) PlaySoundFile("Interface\\AddOns\\GgAdvV7\\Sounds\\ACR\\mp3\\proceed_kill_02.mp3") end,
	[3] = function(x) PlaySoundFile("Interface\\AddOns\\GgAdvV7\\Sounds\\ACR\\mp3\\proceed_kill_03.mp3") end,
	[4] = function(x) PlaySoundFile("Interface\\AddOns\\GgAdvV7\\Sounds\\ACR\\mp3\\proceed_kill_04.mp3") end,
};


-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------


This.vn_seriesKillCount = 0;
This.vn_streakKillCount = 0;
-- TODO: какой-то странный спан...поправить бы..
This.vn_seriesTimeSpan = This.cn_killSeriesTimeSpan; -- timer(upDate)
This.vn_timeTotal = 0;


-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------


-- Таймер сброса серии убийств
This.vs_seriesTimer = {
	tick = 0,
	interval = 2,
	stopped = false,
};


-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------