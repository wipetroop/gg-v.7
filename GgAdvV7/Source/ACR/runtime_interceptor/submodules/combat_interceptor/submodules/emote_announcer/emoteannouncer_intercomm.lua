local This = GGF.ModuleGetWrapper("ACR-4-1-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-- п.3.0 (Addon System Timer)
function This:MPRC_AST_RUNTIME(in_elapsed)
	print("sss", self.vs_seriesTimer.stopped);
	GGF.Timer(self.vs_seriesTimer, in_elapsed, function()
		-- kill stream timer 18 --> 0(сброс)
		print("emote step");
		if (self.vn_seriesTimeSpan < 0) then
			self.vn_seriesKillCount = 0;
		else
			self.vn_seriesTimeSpan = self.vn_seriesTimeSpan - self.vs_seriesTimer.interval;
		end
	end);
end


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------