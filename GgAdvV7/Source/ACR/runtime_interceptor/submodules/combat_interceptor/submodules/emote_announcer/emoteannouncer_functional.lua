local This = GGF.ModuleGetWrapper("ACR-4-1-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Часть старого киллхандлера, отсюда идут сообщеньки в эмоут чат
function This:forcePlayerKill(in_data)

	--GGM.ACR.KillInterceptor:confirmUnitDeath(in_data);
	--ACR_KS.proceedKill(in_data.sourceName.data, in_data.sourceID.data, in_data.sourceFlag.data, in_data.targetName.data, in_data.targetID.data, in_data.targetFlag.data, in_data.spellID.data, in_data.isCritical.data, in_data.isMulticast.data);
	GGM.GUI.KillFeed:ProceedKill(in_data);

	if (in_data.tpm_name.data == GGM.FCS.RuntimeStorage:GetPlayerName()) then
		self:forceDenied();
		return;
	end

	local src_type_dbg = GGM.ACR.CommonService:GetUnitTypeByFlag(in_data.spm_flag.data);
	local type_dbg = GGM.ACR.CommonService:GetUnitTypeByFlag(in_data.tpm_flag.data);

	local killTextMSG = "killed " .. in_data.tpm_name.data;

	local kTyper = ""

	if (src_type_dbg == GGE.UnitType.Pet) then
		kTyper = "(Pet)";
	elseif (src_type_dbg == GGE.UnitType.Object) then
		kTyper = "(Vehicle)";
	end

	if (type_dbg == GGE.UnitType.Pet) then
		killTextMSG = killTextMSG .. "(Pet)";
		if (in_data.tpm_ownerName.data ~= nil) then
			killTextMSG = killTextMSG .. "(" .. in_data.tpm_ownerName.data .. ")";
		end
	elseif (type_dbg == GGE.UnitType.Object) then
		killTextMSG = killTextMSG .. "(Vehicle)";
	elseif (type_dbg == GGE.UnitType.Mob) then
		killTextMSG = killTextMSG .. "(Mob)";
		--return;
	end

	self.vn_streakKillCount = self.vn_streakKillCount + 1;
	self.vn_seriesKillCount = self.vn_seriesKillCount + 1;

	if (in_data.msd_defined.data == "Undefined") then
		killTextMSG = killTextMSG .. " by Undefined Spell" .. kTyper .. "(id:" .. in_data.msd_id.data .. ") deals " .. in_data.msd_amount.data .. "(" .. in_data.msd_overkill.data .. ") damage!"
	else
		killTextMSG = killTextMSG .. " by " .. in_data.msd_name.data .. kTyper .. "(id:" .. in_data.msd_id.data .. ") deals " .. in_data.msd_amount.data .. "(" .. in_data.msd_overkill.data .. ") damage!"
	end

	if (in_data.msd_isCritical.data) then
		killTextMSG = killTextMSG .. " Critical hit!"
	end

	if (in_data.msd_isMulticast.data) then
		killTextMSG = killTextMSG .. " Multicast x2!"
	end

	local killCountMSG = "gets " .. self.vn_streakKillCount .. " kill streak and " .. self.vn_seriesKillCount .. " kill series!";
	local killTypeMSG = "";

	local noTypeFlag = 0;

	killTypeMSG = self.cs_killStreakMessage[self:killStreakTrunc(self.vn_streakKillCount)];

	if (self.vn_streakKillCount >= 3) then 
		if (self.vn_seriesKillCount >= 2) then
			killTypeMSG = killTypeMSG .. " with ";
		end
	else
		if (self.vn_seriesKillCount >= 2) then
			killTypeMSG = killTypeMSG .. " got ";
		else
			noTypeFlag = 1;
		end
	end

	killTypeMSG = killTypeMSG .. self.cs_killSeriesMessage[self:killSeriesTrunc(self.vn_seriesKillCount)]

	self.vn_seriesTimeSpan = self.cn_killSeriesTimeSpan;

	killTypeMSG = killTypeMSG .. "!"
	GGM.ACR.KillInterceptor:KMPrint(killTextMSG, killCountMSG, killTypeMSG, noTypeFlag);
	local kStr = self:killStreakTrunc(self.vn_streakKillCount);
	local kSer = self:killSeriesTrunc(self.vn_seriesKillCount);
	--ACR.killStreakSound[kStr]()
	--killSeriesSound[killSeriesTrunc(killSeriesCount)]()
	-- сомнительный код....
	--local ksound = C_Timer.NewTimer(ACR.killStreakDelay[kStr], function() ACR.killSeriesSound[kSer]() end)

	local snd_ind;
	local clusteRedPlayer = GGM.ACR.SpecDispatcher:GetClusteRedPlayer(in_data.tpm_name.data);

	if (clusteRedPlayer == nil) then
		print("Player not clustered : ", in_data.tpm_name.data);
		snd_ind = math.random(4);
	else

		if (GGM.ACR.SpecDispatcher:CheckSpecClusterClass(clusteRedPlayer.class)) and (GGM.ACR.SpecDispatcher:CheckSoundIndex(clusteRedPlayer.class, clusteRedPlayer.spec)) then
			snd_ind = GGM.ACR.SpecDispatcher:GetSoundIndex(clusteRedPlayer.class, clusteRedPlayer.spec);
		else
			print("Unhandled spec exception : ", clusteredPlayer.class, clusteredPlayer.spec);
			snd_ind = math.random(4);
		end

	end

	self.fn_killSoundEffect[snd_ind]();
	--local ssound = C_Timer.NewTimer(self.cs_killSeriesDelay[snd_ind], function() self.fn_killStreakSound[kStr]() end);

	--local ksound = C_Timer.NewTimer(self.cs_killStreakDelay[kStr] + self.cs_killSeriesDelay[snd_ind], function() self.fn_killSeriesSound[kSer]() end);
end


-- api
-- Принудительный запуск звука деная
function This:forceDenied()
	self.fn_deniedSound();
end

-- HACK: // -- магия!!! не трогать!!! -- //
-- верхнее ограничение по количеству киллов(на самом деле звук на 10+ одинаковый просто)
function This:killStreakTrunc(count)
	if (count > self.cn_streakKillUpperBorder) then
		return self.cn_streakKillUpperBorder;
	end
	return count;
end


-- 
function This:killSeriesTrunc(count)
	if (count > self.cn_seriesKillUpperBorder) then
		return self.cn_seriesKillUpperBorder;
	end
	return count;
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------