local This = GGF.ModuleGetWrapper("ACR-4-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.1 (Miscellaneous Variables)
function This:MPRC_MVR_INITIALIZE()
	for k,v in pairs(self.md_spellNameTableHugeCluster) do 
		for m,n in pairs(v) do 
			self.vs_statisticSkillNameBT[m] = n;
		end
	end
end


-- п.2.6 (Main Event Callbacks)
function This:MPRC_MEC_INITIALIZE()
	-- NOTE: временно отключил для теста без уёв
	self.fr_deadframe:RegisterEvent("PLAYER_DEAD");
	self.fr_bgstart:RegisterEvent("PLAYER_ENTERING_BATTLEGROUND");
	self.fr_bgend:RegisterEvent("UPDATE_BATTLEFIELD_STATUS");
	self.fr_kill_enh:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");

	self.fr_deadframe:SetScript("OnEvent", function(...) self:deadess() end);
	self.fr_bgstart:SetScript("OnEvent", function(...) self:bgstarter() end);
	self.fr_bgend:SetScript("OnEvent", function(...) self:bgStatusUpdate() end);
	-- NOTE: пока что не отключаем интерсептор(отладка на ходу)
	-- TODO: поставить версионный разветвитель, т.к. это работает только с бфа
	GGF.VersionSplitterProc({
		{
			versionInterval = {GGD.TocVersionList.Vanilla, GGD.TocVersionList.Legion},
			action = function()
				self.fr_kill_enh:SetScript("OnEvent", function(in_eventType, in_metainf, ...) self:eventInterceptor(...) end);
			end,
		},
		{
			versionInterval = {GGD.TocVersionList.BFA, GGD.TocVersionList.Invalid},
			action = function()
				self.fr_kill_enh:SetScript("OnEvent", function() self:eventInterceptor(CombatLogGetCurrentEventInfo()) end);
			end,
		},
	});

	-- NOTE: резерв
	self.fr_kill_enh:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------


-- п.5.0 (Scenario Teardown Prepare)
function This:MPRC_STP_UNITTEST()
	self:armUnitTest(
		"Announcer Test №1",
		"Simple basic test for check announcer display of fixed log data",
		function() self:basicUnitTest() end,
		2
	);

	self:armUnitTest(
		"Announcer Test №2",
		"Test on random amount of data to check annoncer and GUi table/graphic working capacity",
		function() self:randomDataUnitTest() end,
		30
	);


	self:armUnitTest(
		"Emote Test №1",
		"Test on random amount of data to check annoncer and GUi table/graphic working capacity",
		function() self:killEmoteAnnouncerUnitTest() end,
		20
	);
end