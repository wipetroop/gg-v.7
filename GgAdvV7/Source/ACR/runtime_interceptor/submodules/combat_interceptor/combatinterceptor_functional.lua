local This = GGF.ModuleGetWrapper("ACR-4-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------

-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-- api
-- Взятие таблицы спеллов
function This:getStatisticSkillNameBT()
	return self.vs_statisticSkillNameBT;
end

-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------

-- TODO: убрать отсюда(GG-44)
-- Принудительный запуск механизма регистрации боя
function This:forceBattleGroundStart()
	print("<< Manual control override >>");
	print("ACR System information : ");
	-- TODO: очень плохой вызов(ну директ же...)
	self:deadess();

	if (GGM.FCS.RuntimeStorage:GetDebugModeFlag()) then
		print("<< Warning! Running in debug mode! Emote announcer disabled! >>");
	end
	--self:printCodeDebugStreamLine("#info#","enhanced k-handler enabled");	
	ACR_CL:SetRuntimeLoggingFlag(true);
end

-- Принудительное отключение механизма регистрации боя
function This:forceBattleGroundEnd()
	print("<< Manual control override >>");
	--ACR.printCodeDebugStreamLine("#info#","enhanced k-handler disabled");
	ACR_CL:SetRuntimeLoggingFlag(false);
end

-- Функция проверки лог механизма путем заполнения логов левыми данными
function This:dmpLoadFictiveLogData()
	ACR_CL:InitializeLogCluster();

	--arguments :
		--dataTable - игнор
		--event - игнор
		--timestamp - игнор
		--daMageType
		--hideCaster
		--sourceID
		--sourceName 
		--sourceFlag 
		--sourceRaidFlag 
		--targetID 
		--targetName 
		--targetFlag 
		--targetRaidFlag 
		--spellID 
		--spellName 
		--spellSchool
		--amount
		--overkill 
		--daMageSchool 
		--isResisted 
		--isBlocked 
		--isAbsorbed 
		--isCritical 
		--isGlancing 
		--isCrushing 
		--isOffHand 
		--isMultistrike
	-- TODO: тут то так можно, но надо в юнит-тесты потом вынести это дело(GG-45)
	self.killhandler_enh(nil, nil, nil, "SPELL_DAMAGE", nil, 1, "dmpTesterCaster", 0, 0, 1, "dmpTesterVictim", 0, 0, 163201, "spell", 1, 90005, -1, 0, false, false, false, true,  false, false, false, true);
	self.killhandler_enh(nil, nil, nil, "SPELL_DAMAGE", nil, 1, "dmpTesterCaster", 0, 0, 1, "dmpTesterVictim", 0, 0, 95601, "spell", 1, 20451, 1005, 0, false, false, false, false,  false, false, false, true);
	self.killhandler_enh(nil, nil, nil, "SWING_DAMAGE", nil, 1, "dmpTesterCaster", 0, 0, 1, "dmpTesterVictim", 0, 0, 77777, 556, 1, 0, -1, 0, true, false, false, false, true, false, false, false);
	self.killhandler_enh(nil, nil, nil, "SPELL_MISSED", nil, 1, "dmpTesterCaster", 0, 0, 1, "dmpTesterVictim", 0, 0, 43812, "spell", 1, "REFLECT", false, false, false, false, false, true,  false, false, false, true);


	ACR_CL:CommitLogCluster()

	-- TODO: переделать
	--ACR.printCodeDebugStreamLine("#info#", "dmp fictive data loaded")
end

-- Главный передатчик(трансиверного типа) эвентов
function This:eventInterceptor(...)
	GGM.ACR.EventInterceptor:EventMasterInterceptor(...);
end

-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------

-- Обработчик смерти персонажа
function This:deadess()
	self.vn_streakKillCount = 0;
	self.vn_seriesKillCount = 0;
end

-- api
-- CC: /acr frcbgs
-- Функция старта бг/арена/форса
function This:bgstarter()
	--print("ACR System information : ")
	-- TODO: еще плохой вызов
	self:deadess();
	--сбросить старые данные(главное не забыть их вывести до этого ><)
	GGM.ACR.RuntimeLogger:StartLogging();
end

-- api
-- CC: /acr frcbge
-- Функция финиша бг/арена/форса
function This:bgender()
	GGM.ACR.RuntimeLogger:FinishLogging();
end

-- api
-- CC: /acr temptest
-- Временный тест
function This:temptest()
   self:basicUnitTest();
end

-- Парсинг эвента смены статуса поля боя
function This:bgStatusUpdate()
	local winner = GetBattlefieldWinner();
	if (winner ~= nil) then
		self:bgender();
	end
end

-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------

-- TODO: переделать(GG-45)
-- Первые данные для юнит-теста
function This:basicUnitTest()
	-- commonMissAssistant(in_missType, in_missSubType, in_sourceName, in_sid, in_sourceFlag, in_targetName, in_tid, in_targetFlag, in_spellID, in_isOffHand, in_amount);

	-- commonDaMageAssistant(in_daMageType, in_sourceName, in_sid, in_sourceFlag, in_targetName, in_tid, in_targetFlag, in_spellID, in_spellDaMage, in_isCritical, in_overkill);

	-- commonEnvironmentalDaMageAssistant(in_timestamp, in_targetName, in_targetID, in_targetFlag, in_dmgType, in_amount);

	-- commonUnitDiedAssistant(in_timestamp, in_targetName, in_targetID, in_targetFlag);

	-- TODO: еще с 27.09.2016 - Дофигачить данные(нормальные, а не тяп-ляп)

	C_Timer.NewTimer(0, function() self:commonDaMageAssistant(0, "SPELL_DAMAGE", "Агромания", "Player-1927-0798C86B", 0x0518, "Bob", "Player-1602-07E31532", 0x0548, 198670, 370005, false, -1) end);
	C_Timer.NewTimer(0, function() self:commonDaMageAssistant(1, "SWING_DAMAGE", "Агромания", "Player-1927-0798C86B", 0x0518, "Bob", "Player-1602-07E31532", 0x0548, 6603, 170005, false, -1) end);
	C_Timer.NewTimer(0, function() self:commonMissAssistant(2, "SPELL_MISSED", "ABSORB", "Агромания", "Player-1927-0798C86B", 0x0518, "Bob", "Player-1602-07E31532", 0x0548, 3044, false, 30452) end);
	C_Timer.NewTimer(0, function() self:commonMissAssistant(3, "SWING_MISSED", "PARRY", "Агромания", "Player-1927-0798C86B", 0x0518, "Bob", "Player-1602-07E31532", 0x0548, 6603, false, nil) end);


	C_Timer.NewTimer(0, function() self:commonDaMageAssistant(4, "SPELL_DAMage", "Bob", "Player-1602-07E31532", 0x0548, "Charley", "Player-1605-0987D30F", 0x0548, 34026, 17300, true, -1) end);
	C_Timer.NewTimer(0, function() self:commonDaMageAssistant(5, "SWING_DAMage", "Charley", "Player-1605-0987D30F", 0x0548, "Bob", "Player-1602-07E31532", 0x0548, 6603, 4500, true, 234) end);
	--self:commonDaMageAssistant("SPELL_DAMage", "Source-3", "Player-1", 0x0548, "Target", "Player-2", 0x0548, 24601, 170005, false);
	--self:commonDaMageAssistant("SPELL_DAMage", "Source-4", "Player-1", 0x0548, "Target", "Player-2", 0x0548, 24601, 170005, false);
	--self:commonDaMageAssistant("SPELL_DAMage", "Source-5", "Player-1", 0x0548, "Target", "Player-2", 0x0548, 24601, 170005, false);
	--self:commonDaMageAssistant("SPELL_DAMage", "Source-6", "Player-1", 0x0548, "Target", "Player-2", 0x0548, 24601, 170005, false);
	--self:commonDaMageAssistant("SPELL_DAMage", "Source-7", "Player-1", 0x0548, "Target", "Player-2", 0x0548, 24601, 170005, false);
	--self:commonDaMageAssistant("SPELL_DAMage", "Source-8", "Player-1", 0x0548, "Target", "Player-2", 0x0548, 24601, 170005, false);
	--self:commonDaMageAssistant("SPELL_DAMage", "Source-9", "Player-1", 0x0548, "Target", "Player-2", 0x0548, 24601, 170005, false);
	--self:commonDaMageAssistant("SPELL_DAMage", "Source-10", "Player-1", 0x0548, "Target", "Player-2", 0x0548, 24601, 170005, false);
	--self:commonDaMageAssistant("SPELL_DAMage", "Source-11", "Player-1", 0x0548, "Target", "Player-2", 0x0548, 24601, 170005, false);
	
	--GGM.ACR.RuntimeInterceptor.commonOverkillAssistant("Source", "Player-1", 0x0548, "Target", "Player-2", 0x0548, 6603, 23760, true);
	C_Timer.NewTimer(0.15, function() self:commonUnitDiedAssistant(6, "Bob", "Player-1602-07E31532", 0x0548) end);


	C_Timer.NewTimer(0.20, function() self:commonEnvironmentalDaMageAssistant(7, "Charley", "Player-1605-0987D30F", 0x0548, "FALLING", 429007) end);
	--GGM.ACR.RuntimeInterceptor.commonUnitDiedAssistant(8, "Charley", "Player-1605-0987D30F", 0x0548);

	--C_Timer.NewTimer(4, function() GGM.ACR.RuntimeInterceptor.commonDaMageAssistant(9, "SPELL_DAMage", "Cat", "Pet-0-3769-1-8941-1860-0201B84CC3", 0x1248, "Alice", "Player-1927-0798C86B", 0x0518, 77451, 17300, true, 304) end);
	C_Timer.NewTimer(0.30, function() self:commonDaMageAssistant(9, "SPELL_DAMAGE", "Cat", "Pet-0-3769-1-8941-1860-0201B84CC3", 0x1248, "Агромания", "Player-1927-0798C86B", 0x0518, 77451, 17300, true, 304) end);
	C_Timer.NewTimer(0.30, function() self:commonUnitDiedAssistant(10, "Агромания", "Player-1927-0798C86B", 0x0518) end);
	C_Timer.NewTimer(0.40, function() self:commonUnitDiedAssistant(11, "Charley", "Player-1605-0987D30F", 0x0548) end);

	C_Timer.NewTimer(0.50, function() self:commonEnvironmentalDaMageAssistant(12, "Dave", "Player-1927-026AF674", 0x0548, "Undefined", 429007) end);
	C_Timer.NewTimer(0.50, function() self:commonUnitDiedAssistant(13, "Dave", "Player-1927-026AF674", 0x0548) end);

	C_Timer.NewTimer(0.60, function() self:commonEnvironmentalDaMageAssistant(13, "ёРоботик", "Player-1923-07DD55D5", 0x0548, "LAVA", 429007) end);
	C_Timer.NewTimer(0.60, function() self:commonUnitDiedAssistant(14, "ёРоботик", "Player-1923-07DD55D5", 0x0548) end);

	C_Timer.NewTimer(0.70, function() self:commonEnvironmentalDaMageAssistant(14, "Eve", "Player-1927-07DBEC10", 0x0548, "DROWNING", 429007) end);
	C_Timer.NewTimer(0.70, function() self:commonUnitDiedAssistant(15, "Eve", "Player-1927-07DBEC10", 0x0548) end);

	C_Timer.NewTimer(0.80, function() self:commonEnvironmentalDaMageAssistant(15, "Cat", "Pet-0-3769-1-8941-1860-0201B84CC3", 0x1248, "FIRE", 429007) end);
	C_Timer.NewTimer(0.80, function() self:commonUnitDiedAssistant(16, "Cat", "Pet-0-3769-1-8941-1860-0201B84CC3", 0x1248) end);

	C_Timer.NewTimer(0.90, function() self:commonEnvironmentalDaMageAssistant(16, "Chuck", "Player-1602-01222F9E", 0x0548, "SLIME", 429007) end);
	C_Timer.NewTimer(0.90, function() self:commonUnitDiedAssistant(17, "Chuck", "Player-1602-01222F9E", 0x0548) end);

	C_Timer.NewTimer(0.95, function() self:commonEnvironmentalDaMageAssistant(17, "Walter", "Player-1605-058040B3", 0x0548, "FATIGUE", 429007) end);
	C_Timer.NewTimer(0.95, function() self:commonUnitDiedAssistant(18, "Walter", "Player-1605-058040B3", 0x0548) end);
	--0x1248
	--local jrnSpell = GGM.ACR.RuntimeInterceptor.createFictiveSpell();
	--local jrnKill = GGM.ACR.RuntimeInterceptor.createFictiveKill();
	C_Timer.NewTimer(1, function() self:basicSupportUnitTest() end);
end

-- Вторые данные для юнит-теста
function This:basicSupportUnitTest()
	C_Timer.NewTimer(0, function() self:commonDaMageAssistant(0, "SPELL_DAMage", "Агромания", "Player-1927-0798C86B", 0x0518, "Bob", "Player-1602-07E31532", 0x0548, 198670, 370005, false, -1) end);
	C_Timer.NewTimer(0, function() self:commonDaMageAssistant(1, "SWING_DAMage", "Агромания", "Player-1927-0798C86B", 0x0518, "Bob", "Player-1602-07E31532", 0x0548, 6603, 170005, false, -1) end);
	C_Timer.NewTimer(0, function() self:commonMissAssistant(2, "SPELL_MISSED", "ABSORB", "Агромания", "Player-1927-0798C86B", 0x0518, "Bob", "Player-1602-07E31532", 0x0548, 3044, false, 30452) end);
	C_Timer.NewTimer(0, function() self:commonMissAssistant(3, "SWING_MISSED", "PARRY", "Агромания", "Player-1927-0798C86B", 0x0518, "Bob", "Player-1602-07E31532", 0x0548, 6603, false, nil) end);
		
	C_Timer.NewTimer(0, function() self:commonDaMageAssistant(4, "SPELL_DAMage", "Bob", "Player-1602-07E31532", 0x0548, "Charley", "Player-1605-0987D30F", 0x0548, 34026, 17300, true, -1) end);
	C_Timer.NewTimer(0, function() self:commonDaMageAssistant(5, "SWING_DAMage", "Charley", "Player-1605-0987D30F", 0x0548, "Bob", "Player-1602-07E31532", 0x0548, 6603, 4500, true, 234) end);
	
	C_Timer.NewTimer(0.15, function() self:commonUnitDiedAssistant(6, "Bob", "Player-1602-07E31532", 0x0548) end);


	C_Timer.NewTimer(0.20, function() self:commonEnvironmentalDaMageAssistant(7, "Charley", "Player-1605-0987D30F", 0x0548, "FALLING", 429007) end);
	

	C_Timer.NewTimer(0.30, function() self:commonDaMageAssistant(9, "SPELL_DAMage", "Cat", "Pet-0-3769-1-8941-1860-0201B84CC3", 0x1248, "Агромания", "Player-1927-0798C86B", 0x0518, 77451, 17300, true, 304) end);
	C_Timer.NewTimer(0.30, function() self:commonUnitDiedAssistant(10, "Агромания", "Player-1927-0798C86B", 0x0518) end);
	C_Timer.NewTimer(0.40, function() self:commonUnitDiedAssistant(11, "Charley", "Player-1605-0987D30F", 0x0548) end);

	C_Timer.NewTimer(0.50, function() self:commonEnvironmentalDaMageAssistant(12, "Dave", "Player-1927-026AF674", 0x0548, "Undefined", 429007) end);
	C_Timer.NewTimer(0.50, function() self:commonUnitDiedAssistant(13, "Dave", "Player-1927-026AF674", 0x0548) end);

	C_Timer.NewTimer(0.60, function() self:commonEnvironmentalDaMageAssistant(13, "ёРоботик", "Player-1923-07DD55D5", 0x0548, "LAVA", 429007) end);
	C_Timer.NewTimer(0.60, function() self:commonUnitDiedAssistant(14, "ёРоботик", "Player-1923-07DD55D5", 0x0548) end);

	C_Timer.NewTimer(0.70, function() self:commonEnvironmentalDaMageAssistant(14, "Eve", "Player-1927-07DBEC10", 0x0548, "DROWNING", 429007) end);
	C_Timer.NewTimer(0.70, function() self:commonUnitDiedAssistant(15, "Eve", "Player-1927-07DBEC10", 0x0548) end);

	C_Timer.NewTimer(0.80, function() self:commonEnvironmentalDaMageAssistant(15, "Cat", "Pet-0-3769-1-8941-1860-0201B84CC3", 0x1248, "FIRE", 429007) end);
	C_Timer.NewTimer(0.80, function() self:commonUnitDiedAssistant(16, "Cat", "Pet-0-3769-1-8941-1860-0201B84CC3", 0x1248) end);

	C_Timer.NewTimer(0.90, function() self:commonEnvironmentalDaMageAssistant(16, "Chuck", "Player-1602-01222F9E", 0x0548, "SLIME", 429007) end);
	C_Timer.NewTimer(0.90, function() self:commonUnitDiedAssistant(17, "Chuck", "Player-1602-01222F9E", 0x0548) end);

	C_Timer.NewTimer(1, function() self:commonEnvironmentalDaMageAssistant(17, "Walter", "Player-1605-058040B3", 0x0548, "FATIGUE", 429007) end);
	C_Timer.NewTimer(1, function() self:commonUnitDiedAssistant(18, "Walter", "Player-1605-058040B3", 0x0548) end);
end

-- Тест с рандомной генерацией данных
function This:randomDataUnitTest()
	local number = 0;	-- пока просто ноль
	local timeLength = 30;-- sec
	local actRate = 4;-- persec
	local victimList = {
		{"Агромания", "Player-1927-0798C86B", 0x0518, 0, {82928, 163485, 185358, 212621, 214581, 3674, 34026, 157708, 109259, 131900}},
		{"Donpal", "Player-1602-07E31532", 0x0548, 0, {6603, 184575, 205273, 53385, 157048, 216527, 228288, 42463}},
		--{"Мафи", "Pet-0-3769-1-8941-1860-0201B84CC3", 0x1248, 0, {6603, 16827}},
		{"Мафи", "Pet-0-3061-1220-1845-15651-0C01BA475E", 0x1248, 0, {6603, 16827}},
		{"Пятнистый кот", "Pet-0-3061-1220-1845-58456-0101BA475A", 0x1248, 0, {6603, 16827}},
		{"Elizi", "Player-1923-07DD55D5", 0x0548, 0, {210714, 188196, 188443, 45297, 117014, 120588, 51505, 77451, 196840, 8042, 51490}},
		{"Yoli", "Player-1927-026AF674", 0x0548, 0, {6603, 193315, 57841, 57842, 86392, 1752, 53, 1943, 8676, 8680}}
	};
	local daMageTypeList = {
		"SPELL_DAMage",
		"SWING_DAMage",
		"RANGE_DAMage",
		"SPELL_PERIODIC_DAMage",
	};
	local missTypeList = {
		"SPELL_MISSED",
		"SWING_MISSED",
		"RANGE_MISSED",
		"SPELL_PERIODIC_MISSED",
	};
	local missSubTypeList = {
		"ABSORB",
		"BLOCK",
		"DEFLECT",
		"DODGE",
		"EVADE",
		"IMMUNE",
		"MISS",
		"PARRY",
		"REFLECT",
		"RESIST",
	};
	local envDaMageTypeList = {
		"Undefined",
		"DROWNING",
		"FALLING",
		"FATIGUE",
		"FIRE",
		"LAVA",
		"SLIME",
	};

	local diff = 1/actRate;
	for i = 1, timeLength*actRate do
		local source = victimList[math.random(1, #victimList)];
		while (source[4] ~= 0) do
			source[4] = source[4] - 1;
			source = victimList[math.random(1, #victimList)];
		end
		source[4] = 5;
		local target = victimList[math.random(1, #victimList)];
		while (target[4] ~= 0) do
			target[4] = target[4] - 1;
			target = victimList[math.random(1, #victimList)];
		end
		target[4] = 5;
		local evType = math.random(1,3);

		local death = math.random(1,10) < 4;

		if (evType == 1) then	-- dam
			local damType = daMageTypeList[math.random(1, #daMageTypeList)];
			local spell;
			local amount;
			local daMage;
			if (damType == "SWING_DAMage") then
				spell = 6603;
			elseif(damType == "RANGE_DAMage") then
				spell = 75;
			else
				spell = source[5][math.random(1, #source[5])];
			end
			daMage = math.random(1, 2127001);
			if (death) then
				amount = math.random(1, daMage);
			else
				amount = -1;
			end
			C_Timer.NewTimer(i*diff, function() self:commonDaMageAssistant(number, damType, source[1], source[2], source[3], target[1], target[2], target[3], spell, daMage, math.random(1,5) == 3, amount) end);
		elseif (evType == 2) then	-- miss
			local missType = missTypeList[math.random(1, #missTypeList)];
			local missSubType = missSubTypeList[math.random(1, #missSubTypeList)];
			local spell;
			local amount;
			local crit = (math.random(1,5) == 1);
			if (missType == "SWING_MISSED") then
				spell = 6603;
			elseif (missType == "RANGE_MISSED") then
				spell = 75;
			else
				spell = source[5][math.random(1, #source[5])];
			end
			if (missType == "ABSORB")
				or (missType == "DEFLECT") then
				amount = math.random(1, 2127001);
			else
				amount = nil;
			end
			C_Timer.NewTimer(i*diff, function() self:commonMissAssistant(number, missType, missSubType, source[1], source[2], source[3], target[1], target[2], target[3], spell, false, amount) end);
		else 	-- env dam
			local eDamType = envDaMageTypeList[math.random(1, #envDaMageTypeList)];
			C_Timer.NewTimer(i*diff, function() self:commonEnvironmentalDaMageAssistant(number, target[1], target[2], target[3], eDamType, math.random(1, 2127001)) end);
		end

		if (death) and (evType ~= 2) then
			C_Timer.NewTimer(i*diff, function() self:commonUnitDiedAssistant(number, target[1], target[2], target[3]) end);
		end
	end

	return timeLength;
end

-- TODO: какой-то плохой код...исправить...
-- Скрипт для теста механизма обработки килов
function This:killEmoteAnnouncerUnitTest(i, l)
	if (i == l) then
		return;
	end
	
	local kStr = self:killStreakTrunc(i);
	local kSer = self:killSeriesTrunc(i);
	local killInfoTest = "killed TestTarget(abstract) by ACR console deals -1 daMage!";
	local killCountTest = "gets " .. i .. " kill streak and " .. i .. " kill series!";
	local killTyPetest;
	if (i == 1) then
		killTyPetest = "";
	elseif (i >= 3) then 
		killTyPetest = self.cs_killStreakMessage[kStr] .. " with " .. self.cs_killSeriesMessage[kSer];
	else
		killTyPetest = "got " .. self.cs_killSeriesMessage[kSer];
	end
	killTyPetest = killTyPetest .. "!";
	if (i == 1) then
		self:kMPrint(killInfoTest, killCountTest, killTyPetest, 1);
	else
		self:kMPrint(killInfoTest, killCountTest, killTyPetest, 0);
	end
	local tmp_ind = math.random(4);
	self.fn_killSoundEffect[tmp_ind]();
	local ssound = C_Timer.NewTimer(self.cs_killSeriesDelay[tmp_ind], function() self.fn_killStreakSound[kStr]() end);
	
	local ksound = C_Timer.NewTimer(self.cs_killStreakDelay[kStr] + self.cs_killSeriesDelay[tmp_ind], function() self.fn_killSeriesSound[kSer]() end);
	
	i = i + 1;
	-- TODO: чекнуть, передастся ли селф туда
	local next = C_Timer.NewTimer(6, function() self:khaTestOut(i, l) end);
end