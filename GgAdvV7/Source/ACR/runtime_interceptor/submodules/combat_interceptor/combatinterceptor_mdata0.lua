local This = GGF.ModuleGetWrapper("ACR-4-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.3 (MDT Check)
function This:MS_CHECK_MDT_0()
	return 1;
end


-------------------------------------------------------------------------------
-- "multitudinous data" block
-------------------------------------------------------------------------------


-- TODO: протестить...не, я серьезно...тут иначе никак..(GG-46)
--// GGM.ACR.RuntimeInterceptor.spellNameTableHugeCluster.SNT - Spell Name Table //--
This.md_spellNameTableHugeCluster = {
	--// Death Knight //--
	SNT_DeathKnight = {
		--// Blood //--
		[49998] = {
			spellName = "Death Strike",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_DeathKnight_Butcher2.blp",
		},
		[114866] = {
			spellName = "Soul Reaper",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_DeathKnight_SoulReaper.blp",
		},
		--// Frost //--
		[66196] = {
			spellName = "Frost Strike Off-Hand",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_DeathKnight_EmpowerRuneBlade2.blp",
		},
		[49143] = {
			spellName = "Frost Strike",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_DeathKnight_EmpowerRuneBlade2.blp",
		},
		[222026] = {
			spellName = "Frost Strike",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_DeathKnight_EmpowerRuneBlade2.blp",
		},	
		[222024] = {
			spellName = "Obliterate",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_DeathKnight_ClassIcon.blp",
		},	
		[66198] = {
			spellName = "Obliterate Off-Hand",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_DeathKnight_ClassIcon.blp",
		},
		[49184] = {
			spellName = "Howling Blast",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Frost_ArcticWinds.blp",
		},
		[237680] = {
			spellName = "Howling Blast",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Frost_ArcticWinds.blp",
		},
		[49020] = {
			spellName = "Obliterate",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_DeathKnight_ClassIcon.blp",
		},
		[51271] = {
			spellName = "Pillar of Frost",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\ability_DeathKnight_pillaroffrost.blp",
		},
		[157389] = {
			spellName = "Empowered Pillar of Frost",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\ability_DeathKnight_pillaroffrost.blp",
		},
		[130735] = {
			spellName = "Soul Reaper",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_DeathKnight_SoulReaper.blp",
		},
		[232752] = {
			spellName = "Glacial Advance",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_GlacialTrap.blp",
		},
		[228322] = {
			spellName = "Hypothermia",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Mage_FrostJaw.blp",
		},
		[196771] = {
			spellName = "Remorseless Winter",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_DeathKnight_RemorselessWinters2.blp",
		},

		--// Unholy //--
		[55090] = {
			spellName = "Scourge Strike",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_DeathKnight_ScourgeStrike.blp",
		},
		[85948] = {
			spellName = "Festering Strike",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_DeathKnight_Festering_strike.blp",
		},
		[130736] = {
			spellName = "Soul Reaper",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_DeathKnight_SoulReaper.blp",
		},

		--// Common //--
		[152279] = {
			spellName = "Breath of Sindragosa",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_DeathKnight_Breatheofsindragosa.blp",
		},
		[152280] = {
			spellName = "Defile",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_DeathKnight_Defile.blp",
		},
		[152281] = {
			spellName = "Necrotic Plague",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_DeathKnight_Necroticplague.blp",
		},
		[108196] = {
			spellName = "Death Siphon",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_DeathKnight_DeathSiphon2.blp",
		},
		[55095] = {
			spellName = "Frost Fever",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_DeathKnight_FrostFever.blp",
		},

		--// Talent //--

		--// Honor Talent //--
		[213726] = {
			spellName = "Cadaverous Pallor",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Holy_TurnUndead.blp",
		},

		--// Pet //--

		--// Disabled/deleted //--

		--// Unhandled //--

	},

	--// Demon Hunter //--
	SNT_DemonHunter = {
		--// Vengeance //--

		--// Havoc //--
		[162243] = {
			spellName = "Demon's Bite",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Weapon_Glave_01.blp",
		},
		[201628] = {
			spellName = "Fury of the Illidari",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_DeathKnight_PlagueStrike.blp",
		},
		[199547] = {
			spellName = "Chaos Strike",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Weapon_Shortblade_62.blp",
		},
		[192611] = {
			spellName = "Fel Rush",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Fire_BurningSpeed.blp",
		},
		[200166] = {
			spellName = "Metamorphosis",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Frost_Stun.blp",
		},
		[207690] = {
			spellName = "Bloodlet",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_DemonHunter_Bloodlet.blp",
		},
		[213243] = {
			spellName = "Felblade",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_DemonHunter_Felblade.blp",
		},
		[228478] = {
			spellName = "Soul Cleave",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_DemonHunter_SoulCleave.blp",
		},
		[217070] = {
			spellName = "Rage of the Illidari",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_DemonHunter_Torment.blp",
		},
		[227518] = {
			spellName = "Annihilation",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Glaive_1H_NPC_D_02.blp",
		},
		[235964] = {
			spellName = "Sever",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_DemonHunter_ManaBreak.blp",
		},
		[222031] = {
			spellName = "Chaos Strike",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Weapon_Shortblade_62.blp",
		},
		[225102] = {
			spellName = "Fel Eruption",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\ability_bossfellord_felspike.blp",
		},
		[211052] = {
			spellName = "Fel Barrage",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\ability_felarakkoa_feldetonation_Green.blp",
		},
		[210153] = {
			spellName = "Death Sweep",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\inv_glaive_1h_artifactaldrochi_d_01dual.blp",
		},
		[210155] = {
			spellName = "Death Sweep",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\inv_glaive_1h_artifactaldrochi_d_01dual.blp",
		},
		--// Common //--
		--// Talent //--
		--// Pet //--
		--// Disabled/deleted //--
		--// Unhandled //--
	},

	--// DrUid //--
	SNT_DrUid = {

		--// Balance //--
		[78674] = {
			spellName = "Starsurge",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Arcane_Arcane03.blp",
		},
		[2912] = {
			spellName = "Starfire",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Arcane_StarFire.blp",
		},
		[226104] = {
			spellName = "Echoing Stars",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_DrUid_Starfall.blp",
		},

		--// Feral Combat //--
		[77758] = {
			spellName = "Thrash",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\spell_DrUid_thrash.blp",
		},
		[213771] = {
			spellName = "Swipe",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Misc_MonsterClaw_03.blp",
		},

		--// Guradian //--

		--// Restoration //--

		--// Common //--
		[124991] = {
			spellName = "Nature's Vigil",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Achievement_Zone_Feralas.blp",
		},
		[164812] = {
			spellName = "Moonfire",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Nature_StarFall.blp",
		},
		[164815] = {
			spellName = "Sunfire",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Mage_FireStarter.blp",
		},

		--// Talent //--

		--// Pet //--

		--// Disabled/deleted //--

		--// Unhandled //--
	},

	--// Hunter //--
	SNT_Hunter = {
		--// Marksmanship //--
		[171454] = {
			spellName = "Chimaera Shot(Frost)",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_ChimeraShot2.blp",
		},
		[171457] = {
			spellName = "Chimaera Shot(Nature)",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_ChimeraShot2.blp",
		},
		[19434] = {
			spellName = "Aimed Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Spear_07.blp",
		},
		[82928] = {
			spellName = "Aimed Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Spear_07.blp",
		},
		[163485] = {
			spellName = "Focusing Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Hunter_FocusingShot.blp",
		},
		[185358] = {
			spellName = "Arcane Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_ImpalingBolt.blp",
		},
		[212621] = {
			spellName = "Marked Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_MarkedShot.blp",
		},
		[214581] = {
			spellName = "Sidewinders",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_SerpentSwiftness.blp",
		},

		--// Survival //--
		[200167] = {
			spellName = "Throwing Axes",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_ThrowingAxePvP320_07.blp",
		},
		[194279] = {
			spellName = "Caltrops",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_IronMaidens_IncindiaryDevice.blp",
		},
		[202800] = {
			spellName = "Flanking Strike",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\ABILITY_Hunter_INVIGERATION.blp",
		},
		[190928] = {
			spellName = "Mongoose Bite",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_MongooseBite.blp",
		},
		[53301] = {
			spellName = "Explosive Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_ExplosiveShot.blp",
		},
		[168980] = {
			spellName = "Lock and Load",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_LockAndLoad.blp",
		},
		[77767] = {
			spellName = "Cobra Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_CobraShot.blp",
		},
		[3674] = {
			spellName = "Black Arrow",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shadow_PainSpike.blp",
		},
		[87935] = {
			spellName = "Serpent Sting",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_SerpentSwiftness.blp",
		},
		[204081] = {
			spellName = "On the Trail",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\artifactability_SurvivalHunter_EaglesBite.blp",
		},
		[185855] = {
			spellName = "Lacerate",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_Laceration.blp",
		},

		--// Beast Mastery //--
		[83381] = {
			spellName = "Kill Command",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_KillCommand.blp",
		},
		[34026] = {
			spellName = "Kill Command",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_KillCommand.blp",
		},
		[152245] = {
			spellName = "Focusing Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Hunter_FocusingShot.blp",
		},

		--// Common //--
		[53351] = {
			spellName = "Kill Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_Assisanate2.blp",
		},
		[157708] = {
			spellName = "Kill Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_Assisanate2.blp",
		},
		[1978] = {
			spellName = "Serpent Sting",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_SerpentSwiftness.blp",
		},
		[2643] = {
			spellName = "Multi Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_UpgradeMoonGlaive.blp",
		},
		[56641] = {
			spellName = "Steady Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_SteadyShot.blp",
		},
		[75] = {
			spellName = "Auto Shot(Ranged)",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Marksmanship.blp",
		},
		[13813] = {
			spellName = "Explosive Trap",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Fire_SelfDestruct.blp",
		},
		[236777] = {
			spellName = "Hi-Explosive Trap",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Fire_SelfDestruct.blp",
		},

		--// Talent //--
		[109259] = {
			spellName = "Powershot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_ResistanceIsFutile.blp",
		},
		[131894] = {
			spellName = "Murder of Crows",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_MurderofCrows.blp",
		},
		[131900] = {
			spellName = "Murder of Crows",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_MurderofCrows.blp",
		},
		[117050] = {
			spellName = "Glaive Toss",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_GlaiveToss.blp",
		},
		[120360] = {
			spellName = "Barrage",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_RapidRegeneration.blp",
		},
		[120361] = {
			spellName = "Barrage",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_RapidRegeneration.blp",
		},
		[162536] = {
			spellName = "Incendiary Ammo",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Hunter_ExoticMunitions_Incendiary.blp",
		},
		[162537] = {
			spellName = "Poisoned Ammo",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Hunter_ExoticMunitions_Poisoned.blp",
		},
		[162539] = {
			spellName = "Frozen Ammo",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Hunter_ExoticMunitions_Frozen.blp",
		},

		[198670] = {
			spellName = "Piercing Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_CheapShot.blp",
		},

		--// Pet //--
		[49966] = {
			spellName = "Smack",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_DrUid_Bash.blp",
		},
		[17253] = {
			spellName = "Bite",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_DrUid_FerociousBite.blp",
		},
		[16827] = {
			spellName = "Claw",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_DrUid_Rake.blp",
		},

		--// Disabled/Deleted //--
		[19503] = {
			spellName = "Scatter Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Ammo_Bullet_02.blp",
		},
		[3044] = {
			spellName = "Arcane Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_ImpalingBolt.blp",
		},

		-- skillName_an["37551"] = "Viper Sting"
		-- skillName_an["18545"] = "Scorpid Sting"
		-- skillName_an["54998"] = "Hand Launcher"
		-- skillName_an["5116"] = "Concussive Shot"

		--// Unhandled //--
		[19801] = {
			spellName = "TranqUilizing Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Nature_Drowsy.blp",
		},
		[147362] = {
			spellName = "Counter Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Ammo_Arrow_03.blp",
		},
		[77767] = {
			spellName = "Cobra Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_CobraShot.blp",
		},

		[82654] = {
			spellName = "Toxin Shot",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Hunter_PotentVenom.blp",
		},
		-- skillName_an["23989"] = "Readinnes"

		-- skillName_an["76659"] = ""
		[76663] = {
			spellName = "Wild QUiver",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Whirlwind.blp",
		},
		[19386] = {
			spellName = "Wyvern Sting",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Spear_02.blp",
		},


		-- skillName_an[""] = ""
		-- skillName_an[""] = ""
	},

	--// Mage //--
	SNT_Mage = {
		--// Arcane //--
		[30451] = {
			spellName = "Arcane Blast",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Arcane_Blast.blp",
		},
		[44425] = {
			spellName = "Arcane Barrage",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Mage_ArcaneBarrage.blp",
		},
		[153640] = {
			spellName = "Arcane Orb",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\spell_Mage_arcaneorb.blp",
		},

		--// Fire //--
		[215775] = {
			spellName = "Phoenix Reborn",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Misc_PhoenixEgg.blp",
		},
		[31661] = {
			spellName = "Dragon's Breath",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Misc_Head_Dragon_01.blp",
		},
		[217694] = {
			spellName = "Living Bomb",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Mage_LivingBomb.blp",
		},
		[11366] = {
			spellName = "Pyroblast",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Fire_Fireball02.blp",
		},
		[203286] = {
			spellName = "Greater Pyroblast",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Mage_GreaterPyroblast.blp",
		},
		[12654] = {
			spellName = "Ignite",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Fire_Incinerate.blp",
		},
		[194858] = {
			spellName = "Dragonsfire Grenade",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Fire_Incinerate.blp",
		},

		--// Frost //--
		[120] = {
			spellName = "Cone of Cold",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Frost_Glacier.blp",
		},
		[122] = {
			spellName = "Frost Nova",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Frost_FrostNova.blp",
		},
		[228597] = {
			spellName = "Frostbolt",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Frost_FrostBolt02.blp",
		},
		[228598] = {
			spellName = "Ice Lance",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Frost_FrostBlast.blp",
		},
		[228599] = {
			spellName = "Ebonbolt",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Enchant_AbyssCrystal.blp",
		},
		[157997] = {
			spellName = "Ice Nova",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Mage_IceNova.blp",
		},
		[116] = {
			spellName = "Frostbolt",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Frost_FrostBolt02.blp",
		},
		[59638] = {
			spellName = "Frostbolt",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Frost_FrostBolt02.blp",
		},
		[30455] = {
			spellName = "Ice Lance",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Frost_FrostBlast.blp",
		},
		[228600] = {
			spellName = "Glacial Spike",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Frost_Frostbolt.blp",
		},
		[228354] = {
			spellName = "Flurry",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Warlock_BurningembersBlue.blp",
		},

		--// Common //--
		[148022] = {
			spellName = "Icicle",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Frost_IceShard.blp",
		},
		[153596] = {
			spellName = "Comet Storm",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Mage_CometStorm.blp",
		},

		--// Talent //--

		--// Pet //--
		[31707] = {
			spellName = "Waterbolt",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Frost_Frostbolt.blp",
		},

		--// Disabled/deleted //--

		--// Unhandled //--
	},


	SNT_Monk = {
		--// Monk //--

		--//  //--
		[229980] = {
			spellName = "Touch of Death",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Monk_TouchOfDeath.blp",
		},
		[185099] = {
			spellName = "Rising Sun Kick",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Monk_RisingSunKick.blp",
		},

		--//  //--

		--//  //--

		--// Common //--

		--// Talent //--

		--// Pet //--

		--// Disabled/deleted //--

		--// Unhandled //--
	},

	--// Paladin //--
	SNT_Paladin = {
		--// Holy //--

		--// Protection //--

		--// Retribution //--
		[184575] = {
			spellName = "Blade of Justice",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Paladin_BladeofJustice.blp",
		},
		[205273] = {
			spellName = "Wake of Ashes",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\inv_sword_2h_artifactashbringer_d_01.blp",
		},
		[20271] = {
			spellName = "Judgment",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Holy_RighteousFury.blp",
		},
		[217020] = {
			spellName = "Zeal",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Holy_SealOfBlood.blp",
		},
		[224266] = {
			spellName = "Templar's Verdict",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Paladin_templarverdict.blp",
		},
		[224239] = {
			spellName = "Divine Storm",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\ABILITY_Paladin_DIVINESTORM.blp",
		},
		[157048] = {
			spellName = "Final Verdict",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Paladin_templarverdict.blp",
		},
		[53385] = {
			spellName = "Divine Storm",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\ABILITY_Paladin_DIVINESTORM.blp",
		},

		--// Common //--
		[216527] = {
			spellName = "Lawbringer",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Holy_RighteousFury.blp",
		},		
		[158392] = {
			spellName = "Hummer of Wrath",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_ThunderClap.blp",
		},
		[42463] = {
			spellName = "Seal of Truth",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Holy_SealOFVengeance.blp",
		},
		[228288] = {
			spellName = "Judgment",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Holy_RighteousFury.blp",
		},

		--// Talent //--

		--// Pet //--

		--// Disabled/deleted //--

		--// Unhandled //--
	},


	SNT_Priest = {
		--// Priest //--

		--// Discipline //--
		[47666] = {
			spellName = "Penance",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Holy_Penance.blp",
		},
		[207946] = {
			spellName = "Light's Wrath",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\inv_staff_2h_artifacttome_d_01.blp",
		},

		--// Holy //--

		--// Shadow //--
		[34914] = {
			spellName = "Vampiric Touch",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Holy_Stoicism.blp",
		},
		[199911] = {
			spellName = "Shadow Word: Death",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shadow_DemonicFortitude.blp",
		},
		[32379] = {
			spellName = "Shadow Word: Death",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shadow_DemonicFortitude.blp",
		},
		[228361] = {
			spellName = "Void Eruption",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Priest_Void Blast.blp",
		},

		--// Common //--
		[589] = {
			spellName = "Shadow Word: Pain",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shadow_ShadowWOrdPain.blp",
		},		
		[237388] = {
			spellName = "Mind Flay",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shadow_MindShear.blp",
		},

		--// Talent //--

		--// Pet //--

		--// Disabled/deleted //--

		--// Unhandled //--
	},

	--// Rogue //--
	SNT_Rogue = {
		--// Combat //--
		[193315] = {
			spellName = "Saber Slah",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Rogue_SabreSlash.blp",
		},
		[57841] = {
			spellName = "Killing Spree",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Warrior_FocusedRage.blp",
		},
		[57842] = {
			spellName = "Killing Spree Off-Hand",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Whirlwind.blp",
		},
		[86392] = {
			spellName = "Main Gauche(Mastery)",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Weapon_ShortBlade_15.blp",
		},
		[1752] = {
			spellName = "Sinister Strike",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shadow_RitualOfSacrifice.blp",
		},
		[2098] = {
			spellName = "Eviscerate",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Rogue_Waylay.blp",
		},

		--// Subtlety //--
		[53] = {
			spellName = "Backstab",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_BackStab.blp",
		},
		[32645] = {
			spellName = "Envenom",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Rogue_Disembowel.blp",
		},
		[154953] = {
			spellName = "Internal Bleeding",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Rogue_BloodSplatter.blp",
		},

		--// Assassination //--

		--// Common //--
		[1943] = {
			spellName = "Rupture",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Rogue_Rupture.blp",
		},
		[8676] = {
			spellName = "Ambush",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Rogue_Ambush.blp",
		},
		[8680] = {
			spellName = "Wound Poison",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Misc_Herb_16.blp",
		},

		--// Talent //--

		--// Pet //--

		--// Disabled/deleted //--

		--// Unhandled //--
	},

	--// Shaman //--
	SNT_Shaman = {
		--// Restoration //--

		--// Elemental //--
		[210714] = {
			spellName = "Icefury",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Frost_IceShard.blp",
		},
		[188196] = {
			spellName = "Lighting Bolt",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Nature_Lightning.blp",
		},
		[45284] = {
			spellName = "Lighting Bolt Overload",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Nature_Lightning.blp",
		},
		[188389] = {
			spellName = "Flame Shock",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Fire_FlameShock.blp",
		},
		[188443] = {
			spellName = "Chain Lightning",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Nature_ChainLightning.blp",
		},
		[45297] = {
			spellName = "Chain Lightning Overload",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Nature_ChainLightning.blp",
		},
		[51505] = {
			spellName = "Lava Burst",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shaman_LavaBurst.blp",
		},
		[77451] = {
			spellName = "Lava Burst Overload",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shaman_LavaBurst.blp",
		},
		[117014] = {
			spellName = "Elemental Blast",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Shaman_talent_ElementalBlast.blp",
		},
		[120588] = {
			spellName = "Elemental Blast Overload",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Shaman_talent_ElementalBlast.blp",
		},

		[196840] = {
			spellName = "Frost Shock",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Frost_FrostShock.blp",
		},
		[8042] = {
			spellName = "Earth Shock",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Nature_EarthShock.blp",
		},
		[51490] = {
			spellName = "Thunderstorm",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shaman_ThunderStorm.blp",
		},

		--// Enhancement //--

		--// Common //--

		--// Talent //--
		[233433] = {
			spellName = "Counterstrike(Totem)",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Nature_Lightning.blp",
		},

		--// Pet //--

		--// Disabled/deleted //--

		--// Unhandled //--
	},

	--// Warlock //--
	SNT_Warlock = {
		--// Affliction //--
		[103103] = {
			spellName = "Drain Soul",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shadow_Haunting.blp",
		},
		[233490] = {
			spellName = "Unstable Affliction",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shadow_UnstableAffliction_3.blp",
		},
		[233496] = {
			spellName = "Unstable Affliction",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shadow_UnstableAffliction_3.blp",
		},
		[233497] = {
			spellName = "Unstable Affliction",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shadow_UnstableAffliction_3.blp",
		},
		[233498] = {
			spellName = "Unstable Affliction",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shadow_UnstableAffliction_3.blp",
		},

		--// Demonology //--

		--// Destruction //--
		[29722] = {
			spellName = "Incinerate",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Fire_Burnout.blp",
		},
		[116858] = {
			spellName = "Chaos Bolt",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Warlock_ChaosBolt.blp",
		},
		[17877] = {
			spellName = "Shadowburn",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shadow_ScourgeBuild.blp",
		},
		[234153] = {
			spellName = "Drain Life",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shadow_LifeDrain02.blp",
		},

		--// Common //--
		[218615] = {
			spellName = "Harvester of Souls",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Warlock_SpellDrain.blp",
		},
		[17962] = {
			spellName = "Conflagrate",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Fire_Fireball02.blp",
		},
		[85692] = {
			spellName = "Doom Bolt",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shadow_ShadowBolt.blp",
		},
		[157736] = {
			spellName = "Immolate",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Fire_Immolation.blp",
		},
		[157114] = {
			spellName = "EmpoweRed Immolate",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Fire_Immolation.blp",
		},
		[348] = {
			spellName = "Immolate",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Fire_Immolation.blp",
		},

		--// Talent //--

		--// Pet //--

		--// Disabled/deleted //--

		--// Unhandled //--
	},

	--// Warrior //--
	SNT_Warrior = {
		--// Arms //--
		[163201] = {
			spellName = "Execute",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Sword_48.blp",
		},
		[772] = {
			spellName = "Rend",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Gouge.blp",
		},

		--// Fury //--
		[205546] = {
			spellName = "Odyn's Fury",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\inv_sword_1h_artifactvigfus_d_01.blp",
		},
		[205547] = {
			spellName = "Odyn's Fury",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\inv_sword_1h_artifactvigfus_d_01.blp",
		},
		[201363] = {
			spellName = "Rampage",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_IronMaidens_BladeRush.blp",
		},
		[218617] = {
			spellName = "Rampage",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_IronMaidens_BladeRush.blp",
		},
		[184707] = {
			spellName = "Rampage",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_IronMaidens_BladeRush.blp",
		},
		[184709] = {
			spellName = "Rampage",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_IronMaidens_BladeRush.blp",
		},
		[100130] = {
			spellName = "Wild Strike",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Warrior_WeaponMastery.blp",
		},
		[23881] = {
			spellName = "Bloodthrist",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Nature_BloodLust.blp",
		},

		--// Protection //--
		[236282] = {
			spellName = "Devastator",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Sword_11.blp",
		},

		--// Common //--
		[217957] = {
			spellName = "Execute Off-Hand",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Sword_48.blp",
		},
		[163558] = {
			spellName = "Execute Off-Hand",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Sword_48.blp",
		},
		[217956] = {
			spellName = "Execute",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Sword_48.blp",
		},
		[217955] = {
			spellName = "Execute",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Sword_48.blp",
		},
		[5308] = {
			spellName = "Execute",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Sword_48.blp",
		},
		[50622] = {
			spellName = "Bladestorm",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Warrior_Bladestorm.blp",
		},
		[176318] = {
			spellName = "Siegebreaker Off-Hand",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Mace_2h_DraenorGuard_B_01_Horde.blp",
		},
		[96103] = {
			spellName = "Raging Blow",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Warrior_Wild_strike.blp",
		},
		[85384] = {
			spellName = "Raging Blow Off-Hand",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Warrior_Wild_strike.blp",
		},
		[1680] = {
			spellName = "Whirlwind",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Whirlwind.blp",
		},

		--// Talent //--
		[46968] = {
			spellName = "Shockwave",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Warrior_ShockWave.blp",
		},
		[113344] = {
			spellName = "Bloodbath",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_Warrior_BloodBath.blp",
		},
		[107570] = {
			spellName = "Storm Bolt",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Warrior_talent_icon_Stormbolt.blp",
		},

		--// Pet //--

		--// Disabled/deleted //--

		--// Unhandled //--
	},


	SNT_Race = {
		--// Undead //--
		[127802] = {
			spellName = "Touch of the Grave",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shadow_FingerOfDeath.blp",
		},
	},


	SNT_Mechanicals = {
		--// Battleground //--
		[96211] = {
			spellName = "Rocket blast(missile turret/missile launcher)",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Fire_Fire.blp",
		},
		[67452] = {
			spellName = "Rocket blast(missile turret/missile launcher)",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Fire_Fire.blp",
		},
		[67453] = {
			spellName = "Rocket blast(missile turret/missile launcher)",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Fire_Fire.blp",
		},

		[66541] = {
			spellName = "Incendiary Rocket(missile turret/missile launcher)",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Fire_Fire.blp",
		},
		[66542] = {
			spellName = "Incendiary Rocket(missile turret/missile launcher)",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Fire_Fire.blp",
		},

		[52339] = {
			spellName = "Hurl Boulder(battleground demolisher)",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shadow_Fumble.blp",
		},
		[52339] = {
			spellName = "Hurl Boulder(battleground demolisher)",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Spell_Shadow_Fumble.blp",
		},

		[67195] = {
			spellName = "Blade Salvo(glaive thrower)",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Weapon_ShortBlade_06.blp",
		},
		[67200] = {
			spellName = "Blade Salvo(glaive thrower)",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Weapon_ShortBlade_06.blp",
		},
		[67034] = {
			spellName = "Glaive Throw(glaive thrower)" ,
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\INV_Weapon_ShortBlade_06.blp",
		},
	},


	SNT_Common = {
		--// Common //--
		[6603] = {
			spellName = "Auto Attack(Melee)",
			spellIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\abilities\\Ability_SteelMelee.blp",
		},
	},
}

--////////////////////////////////////////////////////--




--////////////////////////////////////////////////////--


