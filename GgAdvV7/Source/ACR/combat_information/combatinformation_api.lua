local This = GGF.ModuleGetWrapper("ACR-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- Обновление статистики по временным килам(AKD + SKD)
function This:IncrementTmpKillStat(in_journalRecord)
	self:incrementTmpKillStat(in_journalRecord);
end


-- Обновление статистики по временному кол-ву нанесений урона(ADD + SDD)
function This:IncrementTmpDaMageStat(in_journalRecord)
	self:incrementTmpDaMageStat(in_journalRecord);
end


-- Обновление статистики по временному кол-ву "промахов"(AMD + SMD)
function This:IncrementTmpMissStat(in_journalRecord)
	self:incrementTmpMissStat(in_journalRecord);
end


-- Обнуление стат. значений(след. бой и т.п.)
function This:DropCurrentStat()
	self:dropCurrentStat();
end


-- TODO: реализовать в нормальном виде(GG-33)
-- Обработка перетаскивания мышкой панели приборов
--function GGM.ACR.CombatInformation_EXTERN_Ui_OnMouseDown(self)
	--if not IsControlKeyDown() or not IsAltKeyDown() then
		--print("Missclick protector security stop");
		--return;
	--end
	--self:StartMoving();
--end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------