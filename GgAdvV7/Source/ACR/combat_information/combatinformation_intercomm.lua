local This = GGF.ModuleGetWrapper("ACR-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.6 (Main Event Callbacks)
function This:MPRC_MEC_INITIALIZE()
   self.fr_hkscounter:RegisterEvent("PLAYER_PVP_KILLS_CHANGED");

   self.fr_hkscounter:SetScript("OnEvent", function(self, event, ...)
      GGM.GUI.SystemInstrumentation:SetHonorKillsCount(GetPVPLifetimeStats());
   end);
end

-- п.2.8 (After-load Settings)
function This:MPRC_ALS_INITIALIZE()
	UpdateAddOnMemoryUsage();
   GGM.GUI.SystemInstrumentation:SetHonorKillsCount(GetPVPLifetimeStats());
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-- п.3.0 (Addon System Timer)
function This:MPRC_AST_RUNTIME(in_elapsed)
	GGF.Timer(self.vs_statTimer, in_elapsed, function()
		if (GGM.ACR.RuntimeLogger:GetLoggingFlag() == true) then
			self:processStatisticUpDate(in_elapsed);
		end

		self:updateMaintenanceInformation();
	end);
end


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------