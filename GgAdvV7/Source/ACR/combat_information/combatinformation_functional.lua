local This = GGF.ModuleGetWrapper("ACR-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- Подготовка уев...по факту
function This:prepareMetaInformation()
	GGM.GUI.SystemDashboard:ChangeControlPanelBattleLogToken("N/A");
	local mapID = C_Map.GetBestMapForUnit("Player");
	WorldMapFrame:SetMapID(mapID);
	local ai_new = GetCurrentMapAreaID();
	local zt_new = self.md_zoneCluster[tostring(ai_new)] or "WPZ";
	GGM.GUI.SystemDashboard:ChangeControlPanelBattleTitle(zt_new);
end


-- Таймерный обработчик изменения статистики
function This:processStatisticUpDate(in_elapsed)
	local stat = self.vs_statistic;
	stat.output.AKD = stat.runtime.tmpKills/stat.runtime.tmpTime;
	stat.output.ADD = stat.runtime.tmpDaMages/stat.runtime.tmpTime;
	stat.output.AMD = stat.runtime.tmpMisses/stat.runtime.tmpTime;

	--print(stat.runtime.tmpKills, stat.runtime.tmpDaMages, stat.runtime.tmpMisses, timeTotal);

	stat.output.AEN = stat.output.AKD + stat.output.ADD + stat.output.AMD;

	stat.output.SKD = stat.output.SKD + stat.runtime.tmpKills;
	stat.output.SDD = stat.output.SDD + stat.runtime.tmpDaMages;
	stat.output.SMD = stat.output.SMD + stat.runtime.tmpMisses;

	stat.output.SEN = stat.output.SKD + stat.output.SDD + stat.output.SMD;

	stat.runtime.tmpKills = 0;
	stat.runtime.tmpDaMages = 0;
	stat.runtime.tmpMisses = 0;

	local timeDifference = time() - stat.runtime.absTime;

	--ACR_CL:LogStatistic(timeDifference, stat.output);
end


-- self
-- Обновление статистики по временным килам(AKD + SKD)
function This:incrementTmpKillStat(in_journalRecord)
	self.vs_statistic.runtime.tmpKills = self.vs_statistic.runtime.tmpKills + 1;
end


-- api
-- Обновление статистики по временному кол-ву нанесений урона(ADD + SDD)
function This:incrementTmpDaMageStat(in_journalRecord)
	self.vs_statistic.runtime.tmpDaMages = self.vs_statistic.runtime.tmpDaMages + 1;
end


-- api
-- Обновление статистики по временному кол-ву "промахов"(AMD + SMD)
function This:incrementTmpMissStat(in_journalRecord)
	self.vs_statistic.runtime.tmpMisses = self.vs_statistic.runtime.tmpMisses + 1;
end 


-- api
-- Обнуление стат. значений(след. бой и т.п.)
function This:dropCurrentStat()
	self.vs_statistic.output.AEN = 0;
	self.vs_statistic.output.SEN = 0;
	self.vs_statistic.output.AKD = 0;
	self.vs_statistic.output.SKD = 0;
	self.vs_statistic.output.ADD = 0;
	self.vs_statistic.output.SDD = 0;
	self.vs_statistic.output.AMD = 0;
	self.vs_statistic.output.SMD = 0;

	self.vs_statistic.runtime.tmpKills = 0;
	self.vs_statistic.runtime.tmpDaMages = 0;
	self.vs_statistic.runtime.tmpMisses = 0;
	self.vs_statistic.runtime.tmpTime = 0;
	self.vs_statistic.runtime.absTime = time();
end


-- Функция обновления информации о занимаемой аддоном памяти
function This:updateMaintenanceInformation()
	--UpDateAddOnMemoryUsage();
	
	--if (GGM.ACR.RuntimeLogger:GetLoggingFlag() ~= false) then
		--GGM.ACR.CombatInformation:SetAENVText(string.format("%.2f e.u./s", self.vs_statistic.output.AEN));
		--GGM.ACR.CombatInformation:SetSENVText(string.format("%d e.u.", self.vs_statistic.output.SEN));
		--GGM.ACR.CombatInformation:SetADDVText(string.format("%.2f/%.2f/%.2f [k/d/m]/s", self.vs_statistic.output.AKD, self.vs_statistic.output.ADD, self.vs_statistic.output.AMD));
		--GGM.ACR.CombatInformation:SetSDDVText(string.format("%d/%d/%d [k/d/m]", self.vs_statistic.output.SKD, self.vs_statistic.output.SDD, self.vs_statistic.output.SMD));
	--end
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------