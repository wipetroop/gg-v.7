----------------------------------------------------------------------------
-- "functional" block
----------------------------------------------------------------------------


----------------------------------------------------------------------------
-- "define" block
----------------------------------------------------------------------------

GGM.ACR = {};
GGD.ACR = {};
GGD.ACR.ClusterName = "Advanced Combat Resolver";
GGD.ACR.ClusterToken = "ACR";
GGD.ACR.ClusterVersion = GGD.BuildMeta.Version;
GGD.ACR.ClusterBuild = "AT-"..GGD.BuildMeta.Build;
GGD.ACR.ClusterMBI = "6102-2072";