local SM = LibStub:GetLibrary("LibShaRedMedia-3.0")
--local Astrolabe = DongleStub("Astrolabe-1.0")
local AceLocale = LibStub("AceLocale-3.0")
--local L = AceLocale:GetLocale("Spy")
local _
local TopWindow
local AddToScale = {}
local AllWindows = {}
local LevelDiff


GGAA = LibStub("AceAddon-3.0"):NewAddon("GGAA", "AceConsole-3.0", "AceEvent-3.0", "AceComm-3.0", "AceTimer-3.0")


GGAA.GGM.GUI.MainWindow = nil
GGAA.Version = "3.2.10"
GGAA.DatabaseVersion = "1.1"
GGAA.Signature = "[GGAA]"
GGAA.ButtonLimit = 10
GGAA.MaximumPlayerLevel = MAX_Player_LEVEL_TABLE[GetExpansionLevel()]
GGAA.MapNoteLimit = 20
GGAA.MapProximityThreshold = 0.02
GGAA.CurrentMapNote = 1
GGAA.ZoneID = {}
GGAA.KOSGUild = {}
GGAA.CurrentList = {}
GGAA.NearbyList = {}
GGAA.LastHourList = {}
GGAA.ActiveList = {}
GGAA.InactiveList = {}
GGAA.PlayerCommList = {}
GGAA.ListAmountDisplayed = 0
GGAA.ButtonName = {}
GGAA.EnabledInZone = false
GGAA.InInstance = false
GGAA.AlertType = nil
GGAA.UpgradeMessageSent = false
GGAA.ListTypes = {
	{"Nearby", GGAA.ManageNearbyList, GGAA.ManageNearbyListExpirations},
	{"LastHour", GGAA.ManageLastHourList, GGAA.ManageLastHourListExpirations},
	{"Ignore", GGAA.ManageIgnoreList},
	{"KillOnSight", GGAA.ManageKillOnSightList},
}

local Default_Profile = {
	profile = {
		Colors = {
			["Window"] = {
				["Title"] = { r = 1, g = 1, b = 1, a = 1 },
				["Background"]= { r = 24/255, g = 24/255, b = 24/255, a = 1 },
				["Title Text"] = { r = 1, g = 1, b = 1, a = 1 },
			},
			["Other Windows"] = {
				["Title"] = { r = 1, g = 0, b = 0, a = 1 },
				["Background"]= { r = 24/255, g = 24/255, b = 24/255, a = 1 },
				["Title Text"] = { r = 1, g = 1, b = 1, a = 1 },
			},
			["Bar"] = {
				["Bar Text"] = { r = 1, g = 1, b = 1 },
			},
			["Warning"] = {
				["Warning Text"] = { r = 1, g = 1, b = 1 },
			},
			["Tooltip"] = {
				["Title Text"] = { r = 0.8, g = 0.3, b = 0.22 },
				["Details Text"] = { r = 1, g = 1, b = 1 },
				["Location Text"] = { r = 1, g = 0.82, b = 0 },
				["Reason Text"] = { r = 1, g = 0, b = 0 },
			},
			["Alert"] = {
				["Background"]= { r = 0, g = 0, b = 0, a = 0.4 },
				["Icon"] = { r = 1, g = 1, b = 1, a = 0.5 },
				["KOS Border"] = { r = 1, g = 0, b = 0, a = 0.4 },
				["KOS Text"] = { r = 1, g = 0, b = 0 },
				["KOS GUild Border"] = { r = 1, g = 0.82, b = 0, a = 0.4 },
				["KOS GUild Text"] = { r = 1, g = 0.82, b = 0 },
				["Stealth Border"] = { r = 0.6, g = 0.2, b = 1, a = 0.4 },
				["Stealth Text"] = { r = 0.6, g = 0.2, b = 1 },
				["Away Border"] = { r = 0, g = 1, b = 0, a = 0.4 },
				["Away Text"] = { r = 0, g = 1, b = 0 },
				["Location Text"] = { r = 1, g = 0.82, b = 0 },
				["Name Text"] = { r = 1, g = 1, b = 1 },
			},
			["Class"] = {
				["Hunter"] = { r = 0.67, g = 0.83, b = 0.45, a = 0.6 },
				["Warlock"] = { r = 0.58, g = 0.51, b = 0.79, a = 0.6 },
				["Priest"] = { r = 1.0, g = 1.0, b = 1.0, a = 0.6 },
				["Paladin"] = { r = 0.96, g = 0.55, b = 0.73, a = 0.6 },
				["Mage"] = { r = 0.41, g = 0.8, b = 0.94, a = 0.6 },
				["Rogue"] = { r = 1.0, g = 0.96, b = 0.41, a = 0.6 },
				["DrUid"] = { r = 1.0, g = 0.49, b = 0.04, a = 0.6 },
				["Shaman"] = { r = 0.14, g = 0.35, b = 1.0, a = 0.6 },
				["Warrior"] = { r = 0.78, g = 0.61, b = 0.43, a = 0.6 },
				["DeathKnight"] = { r = 0.77, g = 0.12, b = 0.23, a = 0.6 },
                ["Monk"] = { r = 0.00, g = 1.00, b = 0.59, a = 0.6 },
				["Pet"] = { r = 0.09, g = 0.61, b = 0.55, a = 0.6 },
				["Mob"] = { r = 0.58, g = 0.24, b = 0.63, a = 0.6 },
				["UNKNOWN"] = { r = 0.1, g = 0.1, b = 0.1, a = 0.6 },
				["Hostile"] = { r = 0.7, g = 0.1, b = 0.1, a = 0.6 },
				["UNGROUPED"] = { r = 0.63, g = 0.58, b = 0.24, a = 0.6 },
				
			},
		},
		GGM.GUI.MainWindow={
			Buttons={
				ClearButton=true,
				LeftButton=true,
				RightButton=true,
			},
			RowHeight=20,
			RowSpacing=2,
			TextHeight=12,
			AutoHide=false,
			BarText={
				RankNum = true,
				PerSec = true,
				Percent = true,
				NumFormat = 1,
			},
			Position={
				x = 4,
				y = 740,
				w = 130,
				h = 44,
			}
		},
		AlertWindowNameSize=14,
		AlertWindowLocationSize=10,
		BarTexture="blend",
		GGM.GUI.MainWindowVis=true,
		CurrentList=1,
		Locked=false,
		Font="Friz Quadrata TT",
		Scaling=1,
		Enabled=true,
		EnabledInBattlegrounds=true,
		EnabledInArenas=true,
		EnabledInWintergrasp=true,
		DisableWhenPVPUnflagged=false,
		MinimapTracking=true,
		MinimapDetails=true,
		DisplayOnMap=true,
		MapDisplayLimit="None",
		DisplayWinLossStatistics=true,
		DisplayKOSReason=true,
		DisplayLastSeen=true,
		ShowOnDetection=true,
		HideSpy=false,
		InvertSpy=false,
--		SpyLocked=false,
		ResizeSpy=true,
		Announce="Self",
		OnlyAnnounceKoS=false,
		WarnOnStealth=true,
		WarnOnKOS=true,
		WarnOnKOSGUild=true,
		WarnOnRace=false,
		SelectWarnRace="None",		
		DisplayWarningsInErrorsFrame=false,
		EnableSound=true,
		RemoveUndetected="OneMinute",
		ShowNearbyList=true,
		PrioritiseKoS=true,
		PurgeData="NinetyDays",
		PurgeKoS=true,
		PurgeWinLossData=true,
		ShaRedata=true,
		UseData=true,
		ShareKOSBetweenCharacters=true,
	}
}

function GGAA:RefreshCurrentList(Player, source)
	local GGM.GUI.MainWindow = GGAA.GGM.GUI.MainWindow
	if not GGM.GUI.MainWindow then
		return
	end
	if not GGM.GUI.MainWindow:IsShown() then
		return
	end

	local mode = GGAA.db.profile.CurrentList
	local manageFunction = GGAA.ListTypes[mode][2]
	if manageFunction then manageFunction() end

	local button = 1
	for index, data in pairs(GGAA.CurrentList) do
		if button <= GGAA.ButtonLimit then
			local level = "??"
			local class = "UNKNOWN"
			local opacity = 1

			local PlayerData = SpyPerCharDB.PlayerData[data.Player]
			if PlayerData then
				if PlayerData.level then
					level = PlayerData.level
					if PlayerData.isGuess == true and tonumber(PlayerData.level) < GGAA.MaximumPlayerLevel then level = level.."+" end
				end
				if PlayerData.class then class = PlayerData.class end
			end

			local description = level.." "
			if L[class] and type(L[class]) == "string" then description = description..L[class] end

			if mode == 1 and GGAA.InactiveList[data.Player] then
				opacity = 0.5
			end
			if Player == data.Player then
				if not source or source ~= GGAA.CharacterName then
					GGAA:AlertPlayer(Player, source)
					if not source then GGAA:AnnouncePlayer(Player) end
				end
			end

			GGAA:SetBar(button, data.Player, description, 100, "Class", class, nil, opacity)
			GGAA.ButtonName[button] = data.Player
			button = button + 1
		end
	end
	GGAA.ListAmountDisplayed = button - 1

	if GGAA.db.profile.ResizeSpy then
		GGAA:AutomaticallyResize()
	end

	GGAA:ManageBarsDisplayed()
end


function GGAA:OnloadReadiness()
	GGAA.timeid = GGAA:ScheduleRepeatingTimer("ManageExpirations", 10, true)
	--GGAA:RegisterEvent("ZONE_CHANGED", "ZoneChangedEvent")
	--GGAA:RegisterEvent("ZONE_CHANGED_NEW_AREA", "ZoneChangedEvent")
	--GGAA:RegisterEvent("Player_ENTERING_WORLD", "ZoneChangedEvent")
	--GGAA:RegisterEvent("UNIT_FACTION", "ZoneChangedEvent")
	--GGAA:RegisterEvent("Player_TARGET_CHANGED", "PlayerTargetEvent")
	--GGAA:RegisterEvent("UPDate_MOUSEOVER_UNIT", "PlayerMouseoverEvent")
	--GGAA:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERed", "CombatLogEvent")
--	GGAA:RegisterEvent("WORLD_MAP_UPDate", "WorldMapUpDateEvent")
	--GGAA:RegisterEvent("Player_REGEN_ENABLED", "LeftCombatEvent")
	--GGAA:RegisterEvent("Player_DEAD", "PlayerDeadEvent")
	--GGAA:RegisterComm(GGAA.Signature, "CommReceived")
	GGAA.IsEnabled = true
	GGAA:RefreshCurrentList()
end


function GGAA:CheckDatabase()

	-- subService retractor
	local acedb = LibStub:GetLibrary("AceDB-3.0")

	GGAA.db = acedb:New("SpyDB", Default_Profile)
	-- eoln



	if not GGAAPerCharDB or not GGAAPerCharDB.PlayerData then
		GGAAPerCharDB = {}
	end
	GGAAPerCharDB.Version = GGAA.DatabaseVersion
	if not GGAAPerCharDB.PlayerData then
		GGAAPerCharDB.PlayerData = {}
	end
	if not GGAAPerCharDB.IgnoRedata then
		GGAAPerCharDB.IgnoRedata = {}
	end
	if not GGAAPerCharDB.KOSData then
		GGAAPerCharDB.KOSData = {}
	end
	--if GGAADB.kosData == nil then SpyDB.kosData = {} end
	--if GGAADB.kosData[GGAA.RealmName] == nil then SpyDB.kosData[GGAA.RealmName] = {} end
	--if GGAADB.kosData[GGAA.RealmName][GGAA.FactionName] == nil then SpyDB.kosData[GGAA.RealmName][GGAA.FactionName] = {} end
	--if GGAADB.kosData[GGAA.RealmName][GGAA.FactionName][GGAA.CharacterName] == nil then SpyDB.kosData[GGAA.RealmName][GGAA.FactionName][GGAA.CharacterName] = {} end
	--if GGAADB.removeKOSData == nil then SpyDB.removeKOSData = {} end
	--if GGAADB.removeKOSData[GGAA.RealmName] == nil then SpyDB.removeKOSData[GGAA.RealmName] = {} end
	--if GGAADB.removeKOSData[GGAA.RealmName][GGAA.FactionName] == nil then SpyDB.removeKOSData[GGAA.RealmName][GGAA.FactionName] = {} end
	if GGAA.db.profile == nil then GGAA.db.profile = Default_Profile.profile end
	if GGAA.db.profile.Colors == nil then GGAA.db.profile.Colors = Default_Profile.profile.Colors end
	if GGAA.db.profile.Colors["Window"] == nil then GGAA.db.profile.Colors["Window"] = Default_Profile.profile.Colors["Window"] end
	if GGAA.db.profile.Colors["Window"]["Title"] == nil then GGAA.db.profile.Colors["Window"]["Title"] = Default_Profile.profile.Colors["Window"]["Title"] end
	if GGAA.db.profile.Colors["Window"]["Background"] == nil then GGAA.db.profile.Colors["Window"]["Background"] = Default_Profile.profile.Colors["Window"]["Background"] end
	if GGAA.db.profile.Colors["Window"]["Title Text"] == nil then GGAA.db.profile.Colors["Window"]["Title Text"] = Default_Profile.profile.Colors["Window"]["Title Text"] end
	if GGAA.db.profile.Colors["Other Windows"] == nil then GGAA.db.profile.Colors["Other Windows"] = Default_Profile.profile.Colors["Other Windows"] end
	if GGAA.db.profile.Colors["Other Windows"]["Title"] == nil then GGAA.db.profile.Colors["Other Windows"]["Title"] = Default_Profile.profile.Colors["Other Windows"]["Title"] end
	if GGAA.db.profile.Colors["Other Windows"]["Background"] == nil then GGAA.db.profile.Colors["Other Windows"]["Background"] = Default_Profile.profile.Colors["Other Windows"]["Background"] end
	if GGAA.db.profile.Colors["Other Windows"]["Title Text"] == nil then GGAA.db.profile.Colors["Other Windows"]["Title Text"] = Default_Profile.profile.Colors["Other Windows"]["Title Text"] end
	if GGAA.db.profile.Colors["Bar"] == nil then GGAA.db.profile.Colors["Bar"] = Default_Profile.profile.Colors["Bar"] end
	if GGAA.db.profile.Colors["Bar"]["Bar Text"] == nil then GGAA.db.profile.Colors["Bar"]["Bar Text"] = Default_Profile.profile.Colors["Bar"]["Bar Text"] end
	if GGAA.db.profile.Colors["Warning"] == nil then GGAA.db.profile.Colors["Warning"] = Default_Profile.profile.Colors["Warning"] end
	if GGAA.db.profile.Colors["Warning"]["Warning Text"] == nil then GGAA.db.profile.Colors["Warning"]["Warning Text"] = Default_Profile.profile.Colors["Warning"]["Warning Text"] end
	if GGAA.db.profile.Colors["Tooltip"] == nil then GGAA.db.profile.Colors["Tooltip"] = Default_Profile.profile.Colors["Tooltip"] end
	if GGAA.db.profile.Colors["Tooltip"]["Title Text"] == nil then GGAA.db.profile.Colors["Tooltip"]["Title Text"] = Default_Profile.profile.Colors["Tooltip"]["Title Text"] end
	if GGAA.db.profile.Colors["Tooltip"]["Details Text"] == nil then GGAA.db.profile.Colors["Tooltip"]["Details Text"] = Default_Profile.profile.Colors["Tooltip"]["Details Text"] end
	if GGAA.db.profile.Colors["Tooltip"]["Location Text"] == nil then GGAA.db.profile.Colors["Tooltip"]["Location Text"] = Default_Profile.profile.Colors["Tooltip"]["Location Text"] end
	if GGAA.db.profile.Colors["Tooltip"]["Reason Text"] == nil then GGAA.db.profile.Colors["Tooltip"]["Reason Text"] = Default_Profile.profile.Colors["Tooltip"]["Reason Text"] end
	if GGAA.db.profile.Colors["Alert"] == nil then GGAA.db.profile.Colors["Alert"] = Default_Profile.profile.Colors["Alert"] end
	if GGAA.db.profile.Colors["Alert"]["Background"] == nil then GGAA.db.profile.Colors["Alert"]["Background"] = Default_Profile.profile.Colors["Alert"]["Background"] end
	if GGAA.db.profile.Colors["Alert"]["Icon"] == nil then GGAA.db.profile.Colors["Alert"]["Icon"] = Default_Profile.profile.Colors["Alert"]["Icon"] end
	if GGAA.db.profile.Colors["Alert"]["KOS Border"] == nil then GGAA.db.profile.Colors["Alert"]["KOS Border"] = Default_Profile.profile.Colors["Alert"]["KOS Border"] end
	if GGAA.db.profile.Colors["Alert"]["KOS Text"] == nil then GGAA.db.profile.Colors["Alert"]["KOS Text"] = Default_Profile.profile.Colors["Alert"]["KOS Text"] end
	if GGAA.db.profile.Colors["Alert"]["KOS GUild Border"] == nil then GGAA.db.profile.Colors["Alert"]["KOS GUild Border"] = Default_Profile.profile.Colors["Alert"]["KOS GUild Border"] end
	if GGAA.db.profile.Colors["Alert"]["KOS GUild Text"] == nil then GGAA.db.profile.Colors["Alert"]["KOS GUild Text"] = Default_Profile.profile.Colors["Alert"]["KOS GUild Text"] end
	if GGAA.db.profile.Colors["Alert"]["Stealth Border"] == nil then GGAA.db.profile.Colors["Alert"]["Stealth Border"] = Default_Profile.profile.Colors["Alert"]["Stealth Border"] end
	if GGAA.db.profile.Colors["Alert"]["Stealth Text"] == nil then GGAA.db.profile.Colors["Alert"]["Stealth Text"] = Default_Profile.profile.Colors["Alert"]["Stealth Text"] end
	if GGAA.db.profile.Colors["Alert"]["Away Border"] == nil then GGAA.db.profile.Colors["Alert"]["Away Border"] = Default_Profile.profile.Colors["Alert"]["Away Border"] end
	if GGAA.db.profile.Colors["Alert"]["Away Text"] == nil then GGAA.db.profile.Colors["Alert"]["Away Text"] = Default_Profile.profile.Colors["Alert"]["Away Text"] end
	if GGAA.db.profile.Colors["Alert"]["Location Text"] == nil then GGAA.db.profile.Colors["Alert"]["Location Text"] = Default_Profile.profile.Colors["Alert"]["Location Text"] end
	if GGAA.db.profile.Colors["Alert"]["Name Text"] == nil then GGAA.db.profile.Colors["Alert"]["Name Text"] = Default_Profile.profile.Colors["Alert"]["Name Text"] end
	if GGAA.db.profile.Colors["Class"] == nil then GGAA.db.profile.Colors["Class"] = Default_Profile.profile.Colors["Class"] end
	if GGAA.db.profile.Colors["Class"]["Hunter"] == nil then GGAA.db.profile.Colors["Class"]["Hunter"] = Default_Profile.profile.Colors["Class"]["Hunter"] end
	if GGAA.db.profile.Colors["Class"]["Warlock"] == nil then GGAA.db.profile.Colors["Class"]["Warlock"] = Default_Profile.profile.Colors["Class"]["Warlock"] end
	if GGAA.db.profile.Colors["Class"]["Priest"] == nil then GGAA.db.profile.Colors["Class"]["Priest"] = Default_Profile.profile.Colors["Class"]["Priest"] end
	if GGAA.db.profile.Colors["Class"]["Paladin"] == nil then GGAA.db.profile.Colors["Class"]["Paladin"] = Default_Profile.profile.Colors["Class"]["Paladin"] end
	if GGAA.db.profile.Colors["Class"]["Mage"] == nil then GGAA.db.profile.Colors["Class"]["Mage"] = Default_Profile.profile.Colors["Class"]["Mage"] end
	if GGAA.db.profile.Colors["Class"]["Rogue"] == nil then GGAA.db.profile.Colors["Class"]["Rogue"] = Default_Profile.profile.Colors["Class"]["Rogue"] end
	if GGAA.db.profile.Colors["Class"]["DrUid"] == nil then GGAA.db.profile.Colors["Class"]["DrUid"] = Default_Profile.profile.Colors["Class"]["DrUid"] end
	if GGAA.db.profile.Colors["Class"]["Shaman"] == nil then GGAA.db.profile.Colors["Class"]["Shaman"] = Default_Profile.profile.Colors["Class"]["Shaman"] end
	if GGAA.db.profile.Colors["Class"]["Warrior"] == nil then GGAA.db.profile.Colors["Class"]["Warrior"] = Default_Profile.profile.Colors["Class"]["Warrior"] end
	if GGAA.db.profile.Colors["Class"]["DeathKnight"] == nil then GGAA.db.profile.Colors["Class"]["DeathKnight"] = Default_Profile.profile.Colors["Class"]["DeathKnight"] end
	if GGAA.db.profile.Colors["Class"]["Monk"] == nil then GGAA.db.profile.Colors["Class"]["Monk"] = Default_Profile.profile.Colors["Class"]["Monk"] end
	if GGAA.db.profile.Colors["Class"]["Pet"] == nil then GGAA.db.profile.Colors["Class"]["Pet"] = Default_Profile.profile.Colors["Class"]["Pet"] end
	if GGAA.db.profile.Colors["Class"]["Mob"] == nil then GGAA.db.profile.Colors["Class"]["Mob"] = Default_Profile.profile.Colors["Class"]["Mob"] end
	if GGAA.db.profile.Colors["Class"]["UNKNOWN"] == nil then GGAA.db.profile.Colors["Class"]["UNKNOWN"] = Default_Profile.profile.Colors["Class"]["UNKNOWN"] end
	if GGAA.db.profile.Colors["Class"]["Hostile"] == nil then GGAA.db.profile.Colors["Class"]["Hostile"] = Default_Profile.profile.Colors["Class"]["Hostile"] end
	if GGAA.db.profile.Colors["Class"]["UNGROUPED"] == nil then GGAA.db.profile.Colors["Class"]["UNGROUPED"] = Default_Profile.profile.Colors["Class"]["UNGROUPED"] end
	if GGAA.db.profile.GGM.GUI.MainWindow == nil then GGAA.db.profile.GGM.GUI.MainWindow = Default_Profile.profile.GGM.GUI.MainWindow end
	if GGAA.db.profile.GGM.GUI.MainWindow.Buttons == nil then GGAA.db.profile.GGM.GUI.MainWindow.Buttons = Default_Profile.profile.GGM.GUI.MainWindow.Buttons end
	if GGAA.db.profile.GGM.GUI.MainWindow.Buttons.ClearButton == nil then GGAA.db.profile.GGM.GUI.MainWindow.Buttons.ClearButton = Default_Profile.profile.GGM.GUI.MainWindow.Buttons.ClearButton end
	if GGAA.db.profile.GGM.GUI.MainWindow.Buttons.LeftButton == nil then GGAA.db.profile.GGM.GUI.MainWindow.Buttons.LeftButton = Default_Profile.profile.GGM.GUI.MainWindow.Buttons.LeftButton end
	if GGAA.db.profile.GGM.GUI.MainWindow.Buttons.RightButton == nil then GGAA.db.profile.GGM.GUI.MainWindow.Buttons.RightButton = Default_Profile.profile.GGM.GUI.MainWindow.Buttons.RightButton end
	if GGAA.db.profile.GGM.GUI.MainWindow.RowHeight == nil then GGAA.db.profile.GGM.GUI.MainWindow.RowHeight = Default_Profile.profile.GGM.GUI.MainWindow.RowHeight end
	if GGAA.db.profile.GGM.GUI.MainWindow.RowSpacing == nil then GGAA.db.profile.GGM.GUI.MainWindow.RowSpacing = Default_Profile.profile.GGM.GUI.MainWindow.RowSpacing end
	if GGAA.db.profile.GGM.GUI.MainWindow.TextHeight == nil then GGAA.db.profile.GGM.GUI.MainWindow.TextHeight = Default_Profile.profile.GGM.GUI.MainWindow.TextHeight end
	if GGAA.db.profile.GGM.GUI.MainWindow.AutoHide == nil then GGAA.db.profile.GGM.GUI.MainWindow.AutoHide = Default_Profile.profile.GGM.GUI.MainWindow.AutoHide end
	if GGAA.db.profile.GGM.GUI.MainWindow.BarText == nil then GGAA.db.profile.GGM.GUI.MainWindow.BarText = Default_Profile.profile.GGM.GUI.MainWindow.BarText end
	if GGAA.db.profile.GGM.GUI.MainWindow.BarText.RankNum == nil then GGAA.db.profile.GGM.GUI.MainWindow.BarText.RankNum = Default_Profile.profile.GGM.GUI.MainWindow.BarText.RankNum end
	if GGAA.db.profile.GGM.GUI.MainWindow.BarText.PerSec == nil then GGAA.db.profile.GGM.GUI.MainWindow.BarText.PerSec = Default_Profile.profile.GGM.GUI.MainWindow.BarText.PerSec end
	if GGAA.db.profile.GGM.GUI.MainWindow.BarText.Percent == nil then GGAA.db.profile.GGM.GUI.MainWindow.BarText.Percent = Default_Profile.profile.GGM.GUI.MainWindow.BarText.Percent end
	if GGAA.db.profile.GGM.GUI.MainWindow.BarText.NumFormat == nil then GGAA.db.profile.GGM.GUI.MainWindow.BarText.NumFormat = Default_Profile.profile.GGM.GUI.MainWindow.BarText.NumFormat end
	if GGAA.db.profile.GGM.GUI.MainWindow.Position == nil then GGAA.db.profile.GGM.GUI.MainWindow.Position = Default_Profile.profile.GGM.GUI.MainWindow.Position end
	if GGAA.db.profile.GGM.GUI.MainWindow.Position.x == nil then GGAA.db.profile.GGM.GUI.MainWindow.Position.x = Default_Profile.profile.GGM.GUI.MainWindow.Position.x end
	if GGAA.db.profile.GGM.GUI.MainWindow.Position.y == nil then GGAA.db.profile.GGM.GUI.MainWindow.Position.y = Default_Profile.profile.GGM.GUI.MainWindow.Position.y end
	if GGAA.db.profile.GGM.GUI.MainWindow.Position.w == nil then GGAA.db.profile.GGM.GUI.MainWindow.Position.w = Default_Profile.profile.GGM.GUI.MainWindow.Position.w end
	if GGAA.db.profile.GGM.GUI.MainWindow.Position.h == nil then GGAA.db.profile.GGM.GUI.MainWindow.Position.h = Default_Profile.profile.GGM.GUI.MainWindow.Position.h end
	if GGAA.db.profile.AlertWindowNameSize == nil then GGAA.db.profile.AlertWindowNameSize = Default_Profile.profile.AlertWindowNameSize end
	if GGAA.db.profile.AlertWindowLocationSize == nil then GGAA.db.profile.AlertWindowLocationSize = Default_Profile.profile.AlertWindowLocationSize end
	if GGAA.db.profile.BarTexture == nil then GGAA.db.profile.BarTexture = Default_Profile.profile.BarTexture end
	if GGAA.db.profile.GGM.GUI.MainWindowVis == nil then GGAA.db.profile.GGM.GUI.MainWindowVis = Default_Profile.profile.GGM.GUI.MainWindowVis end
	if GGAA.db.profile.CurrentList == nil then GGAA.db.profile.CurrentList = Default_Profile.profile.CurrentList end
	if GGAA.db.profile.Locked == nil then GGAA.db.profile.Locked = Default_Profile.profile.Locked end
	if GGAA.db.profile.Font == nil then GGAA.db.profile.Font = Default_Profile.profile.Font end
	if GGAA.db.profile.Scaling == nil then GGAA.db.profile.Scaling = Default_Profile.profile.Scaling end
	if GGAA.db.profile.Enabled == nil then GGAA.db.profile.Enabled = Default_Profile.profile.Enabled end
	if GGAA.db.profile.EnabledInBattlegrounds == nil then GGAA.db.profile.EnabledInBattlegrounds = Default_Profile.profile.EnabledInBattlegrounds end
	if GGAA.db.profile.EnabledInArenas == nil then GGAA.db.profile.EnabledInArenas = Default_Profile.profile.EnabledInArenas end
	if GGAA.db.profile.EnabledInWintergrasp == nil then GGAA.db.profile.EnabledInWintergrasp = Default_Profile.profile.EnabledInWintergrasp end
	if GGAA.db.profile.DisableWhenPVPUnflagged == nil then GGAA.db.profile.DisableWhenPVPUnflagged = Default_Profile.profile.DisableWhenPVPUnflagged end
	if GGAA.db.profile.MinimapTracking == nil then GGAA.db.profile.MinimapTracking = Default_Profile.profile.MinimapTracking end
	if GGAA.db.profile.MinimapDetails == nil then GGAA.db.profile.MinimapDetails = Default_Profile.profile.MinimapDetails end
	if GGAA.db.profile.DisplayOnMap == nil then GGAA.db.profile.DisplayOnMap = Default_Profile.profile.DisplayOnMap end
	if GGAA.db.profile.MapDisplayLimit == nil then GGAA.db.profile.MapDisplayLimit = Default_Profile.profile.MapDisplayLimit end
	if GGAA.db.profile.DisplayWinLossStatistics == nil then GGAA.db.profile.DisplayWinLossStatistics = Default_Profile.profile.DisplayWinLossStatistics end
	if GGAA.db.profile.DisplayKOSReason == nil then GGAA.db.profile.DisplayKOSReason = Default_Profile.profile.DisplayKOSReason end
	if GGAA.db.profile.DisplayLastSeen == nil then GGAA.db.profile.DisplayLastSeen = Default_Profile.profile.DisplayLastSeen end
	if GGAA.db.profile.ShowOnDetection == nil then GGAA.db.profile.ShowOnDetection = Default_Profile.profile.ShowOnDetection end
	if GGAA.db.profile.HideSpy == nil then GGAA.db.profile.HideSpy = Default_Profile.profile.HideSpy end
	if GGAA.db.profile.InvertSpy == nil then GGAA.db.profile.InvertSpy = Default_Profile.profile.InvertSpy end
	if GGAA.db.profile.ResizeSpy == nil then GGAA.db.profile.ResizeSpy = Default_Profile.profile.ResizeSpy end
	if GGAA.db.profile.Announce == nil then GGAA.db.profile.Announce = Default_Profile.profile.Announce end
	if GGAA.db.profile.OnlyAnnounceKoS == nil then GGAA.db.profile.OnlyAnnounceKoS = Default_Profile.profile.OnlyAnnounceKoS end
	if GGAA.db.profile.WarnOnStealth == nil then GGAA.db.profile.WarnOnStealth = Default_Profile.profile.WarnOnStealth end
	if GGAA.db.profile.WarnOnKOS == nil then GGAA.db.profile.WarnOnKOS = Default_Profile.profile.WarnOnKOS end
	if GGAA.db.profile.WarnOnKOSGUild == nil then GGAA.db.profile.WarnOnKOSGUild = Default_Profile.profile.WarnOnKOSGUild end
	if GGAA.db.profile.WarnOnRace == nil then GGAA.db.profile.WarnOnRace = Default_Profile.profile.WarnOnRace end
	if GGAA.db.profile.SelectWarnRace == nil then GGAA.db.profile.SelectWarnRace = Default_Profile.profile.SelectWarnRace end
	if GGAA.db.profile.DisplayWarningsInErrorsFrame == nil then GGAA.db.profile.DisplayWarningsInErrorsFrame = Default_Profile.profile.DisplayWarningsInErrorsFrame end
	if GGAA.db.profile.EnableSound == nil then GGAA.db.profile.EnableSound = Default_Profile.profile.EnableSound end
	if GGAA.db.profile.RemoveUndetected == nil then GGAA.db.profile.RemoveUndetected = Default_Profile.profile.RemoveUndetected end
	if GGAA.db.profile.ShowNearbyList == nil then GGAA.db.profile.ShowNearbyList = Default_Profile.profile.ShowNearbyList end
	if GGAA.db.profile.PrioritiseKoS == nil then GGAA.db.profile.PrioritiseKoS = Default_Profile.profile.PrioritiseKoS end
	if GGAA.db.profile.PurgeData == nil then GGAA.db.profile.PurgeData = Default_Profile.profile.PurgeData end
	if GGAA.db.profile.PurgeKoS == nil then GGAA.db.profile.PurgeKoS = Default_Profile.profile.PurgeKoSData end	
	if GGAA.db.profile.PurgeWinLossData == nil then GGAA.db.profile.PurgeWinLossData = Default_Profile.profile.PurgeWinLossData end	
	if GGAA.db.profile.ShaRedata == nil then GGAA.db.profile.ShaRedata = Default_Profile.profile.ShaRedata end
	if GGAA.db.profile.UseData == nil then GGAA.db.profile.UseData = Default_Profile.profile.UseData end
	if GGAA.db.profile.ShareKOSBetweenCharacters == nil then GGAA.db.profile.ShareKOSBetweenCharacters = Default_Profile.profile.ShareKOSBetweenCharacters end
end





function GGAA:CreateRow(num)
	local rowmin = 1
	if num < rowmin or GGAA.GGM.GUI.MainWindow.Rows[num] then
		return
	end

	local row = CreateFrame("Button", "Spy_GGM.GUI.MainWindow_Bar"..num, GGAA.GGM.GUI.MainWindow, "SpySecureActionButtonTemplate")
	row:SetPoint("TOPLEFT", GGAA.GGM.GUI.MainWindow, "TOPLEFT", 2, -34 - (GGAA.db.profile.GGM.GUI.MainWindow.RowHeight + GGAA.db.profile.GGM.GUI.MainWindow.RowSpacing) * (num - 1))
	row:SetHeight(GGAA.db.profile.GGM.GUI.MainWindow.RowHeight)
	row:SetWidth(GGAA.GGM.GUI.MainWindow:GetWidth() - 4)

	GGAA:SetupBar(row)
	GGAA.GGM.GUI.MainWindow.Rows[num] = row
	GGAA.GGM.GUI.MainWindow.Rows[num]:Hide()
	row.id = num
end


function GGAA:SetupBar(row)
	row.StatusBar = CreateFrame("StatusBar", nil, row)
	row.StatusBar:SetAllPoints(row)

	local BarTexture
	if not BarTexture then
		BarTexture = GGAA.db.profile.BarTexture
	end

	if not BarTexture then
		BarTexture = SM:Fetch("statusbar", "blend")
	else
		BarTexture = SM:Fetch("statusbar", BarTexture)
	end
	row.StatusBar:SetStatusBarTexture(BarTexture)
	row.StatusBar:SetStatusBarColor(.5, .5, .5, 0.8)
	row.StatusBar:SetMinMaxValues(0, 100)
	row.StatusBar:SetValue(100)
	row.StatusBar:Show()

	row.LeftText = row.StatusBar:CreateGGC.FontString(nil, "OVERLAY", "GameFontHighlightSmall")
	row.LeftText:SetPoint("TOPLEFT", row.StatusBar, "TOPLEFT", 2, -4)
	row.LeftText:SetJustifyH("LEFT")
	row.LeftText:SetHeight(GGAA.db.profile.GGM.GUI.MainWindow.TextHeight)
	row.LeftText:SetTextColor(1, 1, 1, 1)
	GGAA:SetFontSize(row.LeftText, GGAA.db.profile.GGM.GUI.MainWindow.RowHeight - 8)
	GGAA:AddGGC.FontString(row.LeftText)

	row.RightText = row.StatusBar:CreateGGC.FontString(nil, "OVERLAY", "GameFontHighlightSmall")
	row.RightText:SetPoint("TOPRIGHT", row.StatusBar, "TOPRIGHT", -2, -5.5)
	row.RightText:SetJustifyH("RIGHT")
	row.RightText:SetTextColor(1, 1, 1, 1)
	GGAA:SetFontSize(row.RightText, GGAA.db.profile.GGM.GUI.MainWindow.RowHeight - 12)
	GGAA:AddGGC.FontString(row.RightText)

	GGAA.Colors:RegisterFont("Bar", "Bar Text", row.LeftText)
	GGAA.Colors:RegisterFont("Bar", "Bar Text", row.RightText)
end

function GGAA:SetFontSize(string, size)
	local Font, Height, Flags = string:GetFont()
	string:SetFont(Font, size, Flags)
end


function GGAA:SaveGGM.GUI.MainWindowPosition()
	GGAA.db.profile.GGM.GUI.MainWindow.Position.x = GGAA.GGM.GUI.MainWindow:GetLeft()
	GGAA.db.profile.GGM.GUI.MainWindow.Position.y = GGAA.GGM.GUI.MainWindow:GetTop()
	GGAA.db.profile.GGM.GUI.MainWindow.Position.w = GGAA.GGM.GUI.MainWindow:GetWidth()
	GGAA.db.profile.GGM.GUI.MainWindow.Position.h = GGAA.GGM.GUI.MainWindow:GetHeight()
end


function GGAA:RestoreGGM.GUI.MainWindowPosition(x, y, width, height)
	GGAA.GGM.GUI.MainWindow:ClearAllPoints()
	GGAA.GGM.GUI.MainWindow:SetPoint("TOPLEFT", UiParent, "BOTTOMLEFT", x, y)
	GGAA.GGM.GUI.MainWindow:SetWidth(width)
	for i = 1, GGAA.ButtonLimit do
		GGAA.GGM.GUI.MainWindow.Rows[i]:SetWidth(width - 4)
	end
	GGAA.GGM.GUI.MainWindow:SetHeight(height)
	GGAA:SaveGGM.GUI.MainWindowPosition()
end


function GGAA:SetupGGM.GUI.MainWindowButtons()
	for k, v in pairs(GGAA.db.profile.GGM.GUI.MainWindow.Buttons) do
		if v then
			GGAA.GGM.GUI.MainWindow[k]:Show()
			GGAA.GGM.GUI.MainWindow[k]:SetWidth(16)
		else
			GGAA.GGM.GUI.MainWindow[k]:SetWidth(1)
			GGAA.GGM.GUI.MainWindow[k]:Hide()
		end
	end
end

function GGAA:ResizeGGM.GUI.MainWindow()
	if GGAA.GGM.GUI.MainWindow.Rows[0] then GGAA.GGM.GUI.MainWindow.Rows[0]:Hide() end

	local CurWidth = GGAA.GGM.GUI.MainWindow:GetWidth() - 4
	GGAA.GGM.GUI.MainWindow.title:SetWidth(CurWidth - 75)
	for i = 1, GGAA.ButtonLimit do
		GGAA.GGM.GUI.MainWindow.Rows[i]:SetWidth(CurWidth)
	end

	GGAA:ManageBarsDisplayed()
end

function GGAA:ManageBarsDisplayed()
	local detected = GGAA.ListAmountDisplayed
	local bars = math.floor((GGAA.GGM.GUI.MainWindow:GetHeight() - 44) / (GGAA.db.profile.GGM.GUI.MainWindow.RowHeight + GGAA.db.profile.GGM.GUI.MainWindow.RowSpacing))
	if bars > detected then bars = detected end
	if bars > GGAA.ButtonLimit then bars = GGAA.ButtonLimit end
	GGAA.GGM.GUI.MainWindow.CurRows = bars

	if not InCombatLockdown() then
		for i = 1, GGAA.ButtonLimit do
			if i <= GGAA.GGM.GUI.MainWindow.CurRows then
				GGAA.GGM.GUI.MainWindow.Rows[i]:Show()
			else
				GGAA.GGM.GUI.MainWindow.Rows[i]:Hide()
			end
		end
	end
end

function GGAA:ManageExpirations()
	local mode = GGAA.db.profile.CurrentList
	local expirationFunction = GGAA.ListTypes[mode][3]
	if expirationFunction then expirationFunction() end
end

function GGAA:CreateFrame(Name, Title, Height, Width, ShowFunc, HideFunc)
	local theFrame = CreateFrame("Frame", Name, UiParent)

	theFrame:ClearAllPoints()
	theFrame:SetPoint("TOPLEFT", UiParent)
	theFrame:SetHeight(Height)
	theFrame:SetWidth(Width)

	theFrame:SetBackdrop({
		bgFile = "Interface\\Tooltips\\Ui-Tooltip-Background", tile = true, tileSize = 16,
		edgeFile = "Interface\\AddOns\\GgAdvAnnouncer\\Textures\\Title-industrial.tga", edgeSize = 32,
		insets = {left = 0, right = 0, top = 31, bottom = 0},
	})

	if Name == "ACR_GGM.GUI.MainWindow" then
		GGAA.Colors:RegisterBorder("Window", "Title", theFrame)
		GGAA.Colors:RegisterBackground("Window", "Background", theFrame)
	else
		GGAA.Colors:RegisterBorder("Other Windows", "Title", theFrame)
		GGAA.Colors:RegisterBackground("Other Windows", "Background", theFrame)
	end
--	if GGAA.db.profile.LockSpy == true then
--		print("Spy position locked")
--		theFrame:SetMovable(false)
--	else
	theFrame:EnableMouse(true)
	theFrame:SetMovable(true)

	theFrame:SetScript("OnMouseDown",
	function(self, event) 
		if (((not self.isLocked) or (self.isLocked == 0)) and (event == "LeftButton")) then
			GGAA:SetWindowTop(self)
			self:StartMoving();
			self.isMoving = true;
		end
	end)
	theFrame:SetScript("OnMouseUp",
	function(self) 
		if (self.isMoving) then
			self:StopMovingOrSizing();
			self.isMoving = false;
			GGAA:SaveGGM.GUI.MainWindowPosition()
		end
	end)
--	end
	theFrame.ShowFunc = ShowFunc
	theFrame:SetScript("OnShow",
	function(self)
		GGAA:SetWindowTop(self)
		if (self.ShowFunc) then
			self:ShowFunc()
		end
	end)
	theFrame.HideFunc = HideFunc
	theFrame:SetScript("OnHide",
	function(self) 
		if (self.isMoving) then
			self:StopMovingOrSizing();
			self.isMoving = false;
		end
		if (self.HideFunc) then
			self:HideFunc()
		end
	end)
	theFrame.title = theFrame:CreateGGC.FontString(nil, "OVERLAY", "GameFontNormal")
	theFrame.title:SetPoint("TOPLEFT", theFrame, "TOPLEFT", 8, -16)
	theFrame.title:SetJustifyH("LEFT")
	theFrame.title:SetTextColor(1.0, 1.0, 1.0, 1.0)
	theFrame.title:SetText(Title)
	theFrame.title:SetHeight(GGAA.db.profile.GGM.GUI.MainWindow.TextHeight)
	GGAA:AddGGC.FontString(theFrame.title)

	if Name == "ACR_GGM.GUI.MainWindow" then
		GGAA.Colors:UnregisterItem(theFrame.title)
		GGAA.Colors:RegisterFont("Window", "Title Text", theFrame.title)
	else
		GGAA.Colors:UnregisterItem(theFrame.title)
		GGAA.Colors:RegisterFont("Other Windows", "Title Text", theFrame.title)
	end

	theFrame.CloseButton = CreateFrame("Button", nil, theFrame)
	theFrame.CloseButton:SetNormalTexture("Interface\\Buttons\\Ui-Panel-MinimizeButton-Up.blp")
	theFrame.CloseButton:SetPushedTexture("Interface\\Buttons\\Ui-Panel-MinimizeButton-Down.blp")
	theFrame.CloseButton:SetHighlightTexture("Interface\\Buttons\\Ui-Panel-MinimizeButton-Highlight.blp")
	theFrame.CloseButton:SetWidth(20)
	theFrame.CloseButton:SetHeight(20)
	theFrame.CloseButton:SetPoint("TOPRIGHT", theFrame, "TOPRIGHT", -4, -12)
	theFrame.CloseButton:SetScript("OnEnter", function(self)
	GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
	GameTooltip:AddLine(L["Close"], 1, 0.82, 0, 1)
	GameTooltip:AddLine(L["CloseDescription"],0,0,0,1)
	GameTooltip:Show()
	end)
	theFrame.CloseButton:SetScript("OnLeave", function(self) GameTooltip:Hide() end)
	theFrame.CloseButton:SetScript("OnClick", function(self) self:GetParent():Hide() end)

	return theFrame
end

function GGAA:SetLevel(frame, level)
	LevelDiff = level - frame:GetFrameLevel()
	frame:SetFrameLevel(level)
end


function GGAA:AddWindow(window)
	window.Below = TopWindow
	TopWindow.Above = window
	window.Above = nil

	GGAA:SetLevel(window, TopWindow:GetFrameLevel() + 10)
	TopWindow = window

	AddToScale[#AddToScale + 1] = window
	AllWindows[#AllWindows + 1] = window

	window.isLocked = GGAA.db.profile.Locked
end


function GGAA:InitOrder()
	TopWindow = UiParent
	GGAA:AddWindow(GGAA.GGM.GUI.MainWindow)
end


function GGAA:SetCurrentList(mode)
	if not mode or mode > #GGAA.ListTypes then
		mode = 1
	end
	GGAA.db.profile.CurrentList = mode
	GGAA:ManageExpirations()

	local data = GGAA.ListTypes[mode]
	GGAA.GGM.GUI.MainWindow.title:SetText(data[1])

	GGAA:RefreshCurrentList()
end

function GGAA:CreateGGM.GUI.MainWindow()
	print("creating mw")
	if not GGAA.GGM.GUI.MainWindow then
		GGAA.GGM.GUI.MainWindow = GGAA:CreateFrame("ACR_GGM.GUI.MainWindow", "Nearby", 44, 200,
		function()
			GGAA.db.profile.GGM.GUI.MainWindowVis = true
		end,
		function()
			GGAA.db.profile.GGM.GUI.MainWindowVis = false
		end)

		local theFrame = GGAA.GGM.GUI.MainWindow
		theFrame:SetResizable(true)
		theFrame:SetMinResize(90, 44)
		theFrame:SetMaxResize(300, 264)
		theFrame:SetScript("OnSizeChanged",
		function(self)
			if (self.isResizing) then
				GGAA:ResizeGGM.GUI.MainWindow()
			end
		end)
--		if GGAA.db.profile.LockSpy == true then
--			theFrame:SetMovable(false)
--		else
		theFrame:SetMovable(true)
		theFrame.titleClick = CreateFrame("FRAME", nil, theFrame)
		theFrame.titleClick:SetAllPoints(theFrame.title)
		theFrame.titleClick:EnableMouse(true)
		theFrame.titleClick:SetScript("OnMouseDown",
		function(self, button) 
			local parent = self:GetParent()
			if (((not parent.isLocked) or (parent.isLocked == 0)) and (button == "LeftButton")) then
				GGAA:SetWindowTop(parent)
				parent:StartMoving();
				parent.isMoving = true;
			end
		end)
		theFrame.titleClick:SetScript("OnMouseUp",
		function(self) 
			local parent = self:GetParent()
			if (parent.isMoving) then
				parent:StopMovingOrSizing();
				parent.isMoving = false;
				GGAA:SaveGGM.GUI.MainWindowPosition()
			end
		end)
--		end
		theFrame.DragBottomRight = CreateFrame("Button", "SpyResizeGripRight", theFrame)
		theFrame.DragBottomRight:Show()
		theFrame.DragBottomRight:SetFrameLevel(theFrame:GetFrameLevel() + 10)
		theFrame.DragBottomRight:SetNormalTexture("Interface\\AddOns\\GgAdvAnnouncer\\Textures\\resize-right.tga")
		theFrame.DragBottomRight:SetHighlightTexture("Interface\\AddOns\\GgAdvAnnouncer\\Textures\\resize-right.tga")
		theFrame.DragBottomRight:SetWidth(16)
		theFrame.DragBottomRight:SetHeight(16)
		theFrame.DragBottomRight:SetPoint("BOTTOMRIGHT", theFrame, "BOTTOMRIGHT", 0, 0)
		theFrame.DragBottomRight:EnableMouse(true)
		theFrame.DragBottomRight:SetScript("OnMouseDown", function(self, button) if (((not self:GetParent().isLocked) or (self:GetParent().isLocked == 0)) and (button == "LeftButton")) then self:GetParent().isResizing = true; self:GetParent():StartSizing("BOTTOMRIGHT") end end)
		theFrame.DragBottomRight:SetScript("OnMouseUp", function(self, button) if self:GetParent().isResizing == true then self:GetParent():StopMovingOrSizing(); GGAA:SaveGGM.GUI.MainWindowPosition(); GGAA:RefreshCurrentList(); self:GetParent().isResizing = false; end end)

		theFrame.DragBottomLeft = CreateFrame("Button", "SpyResizeGripLeft", theFrame)
		theFrame.DragBottomLeft:Show()
		theFrame.DragBottomLeft:SetFrameLevel(theFrame:GetFrameLevel() + 10)
		theFrame.DragBottomLeft:SetNormalTexture("Interface\\AddOns\\GgAdvAnnouncer\\Textures\\resize-left.tga")
		theFrame.DragBottomLeft:SetHighlightTexture("Interface\\AddOns\\GgAdvAnnouncer\\Textures\\resize-left.tga")
		theFrame.DragBottomLeft:SetWidth(16)
		theFrame.DragBottomLeft:SetHeight(16)
		theFrame.DragBottomLeft:SetPoint("BOTTOMLEFT", theFrame, "BOTTOMLEFT", 0, 0)
		theFrame.DragBottomLeft:EnableMouse(true)
		theFrame.DragBottomLeft:SetScript("OnMouseDown", function(self, button) if (((not self:GetParent().isLocked) or (self:GetParent().isLocked == 0)) and (button == "LeftButton")) then self:GetParent().isResizing = true; self:GetParent():StartSizing("BOTTOMLEFT") end end)
		theFrame.DragBottomLeft:SetScript("OnMouseUp", function(self, button) if self:GetParent().isResizing == true then self:GetParent():StopMovingOrSizing(); GGAA:SaveGGM.GUI.MainWindowPosition(); GGAA:RefreshCurrentList(); self:GetParent().isResizing = false; end end)

		theFrame.RightButton = CreateFrame("Button", nil, theFrame)
		theFrame.RightButton:SetNormalTexture("Interface\\AddOns\\GgAdvAnnouncer\\Textures\\button-right.tga")
		theFrame.RightButton:SetPushedTexture("Interface\\AddOns\\GgAdvAnnouncer\\Textures\\button-right.tga")
		theFrame.RightButton:SetHighlightTexture("Interface\\AddOns\\GgAdvAnnouncer\\Textures\\button-highlight.tga")
		theFrame.RightButton:SetWidth(16)
		theFrame.RightButton:SetHeight(16)
		theFrame.RightButton:SetPoint("TOPRIGHT", theFrame, "TOPRIGHT", -23, -14.5)
		theFrame.RightButton:SetScript("OnEnter", function(self)
		GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
		GameTooltip:AddLine(L["Left/Right"], 1, 0.82, 0, 1)
		GameTooltip:AddLine(L["Left/RightDescription"],0,0,0,1)
		GameTooltip:Show()
		end)
		theFrame.RightButton:SetScript("OnLeave", function(self) GameTooltip:Hide() end)
		theFrame.RightButton:SetScript("OnClick", function() GGAA:GGM.GUI.MainWindowNextMode() end)
		theFrame.RightButton:SetFrameLevel(theFrame.RightButton:GetFrameLevel() + 1)

		theFrame.LeftButton = CreateFrame("Button", nil, theFrame)
		theFrame.LeftButton:SetNormalTexture("Interface\\AddOns\\GgAdvAnnouncer\\Textures\\button-left.tga")
		theFrame.LeftButton:SetPushedTexture("Interface\\AddOns\\GgAdvAnnouncer\\Textures\\button-left.tga")
		theFrame.LeftButton:SetHighlightTexture("Interface\\AddOns\\GgAdvAnnouncer\\Textures\\button-highlight.tga")
		theFrame.LeftButton:SetWidth(16)
		theFrame.LeftButton:SetHeight(16)
		theFrame.LeftButton:SetPoint("RIGHT", theFrame.RightButton, "LEFT", 0, 0)
		theFrame.LeftButton:SetScript("OnEnter", function(self)
		GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
		GameTooltip:AddLine(L["Left/Right"], 1, 0.82, 0, 1)
		GameTooltip:AddLine(L["Left/RightDescription"],0,0,0,1)
		GameTooltip:Show()
		end)
		theFrame.LeftButton:SetScript("OnLeave", function(self) GameTooltip:Hide() end)
		theFrame.LeftButton:SetScript("OnClick", function() GGAA:GGM.GUI.MainWindowPrevMode() end)
		theFrame.LeftButton:SetFrameLevel(theFrame.LeftButton:GetFrameLevel() + 1)

		theFrame.ClearButton = CreateFrame("Button", nil, theFrame)
		theFrame.ClearButton:SetNormalTexture("Interface\\AddOns\\GgAdvAnnouncer\\Textures\\button-clear.tga")
		theFrame.ClearButton:SetPushedTexture("Interface\\AddOns\\GgAdvAnnouncer\\Textures\\button-clear.tga")
		theFrame.ClearButton:SetHighlightTexture("Interface\\AddOns\\GgAdvAnnouncer\\Textures\\button-highlight.tga")
		theFrame.ClearButton:SetWidth(16)
		theFrame.ClearButton:SetHeight(16)
		theFrame.ClearButton:SetPoint("RIGHT", theFrame.LeftButton,"LEFT", 0, 0)
		theFrame.ClearButton:SetScript("OnEnter", function(self)
		GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
		GameTooltip:AddLine(L["Clear"], 1, 0.82, 0, 1)
		GameTooltip:AddLine(L["ClearDescription"],0,0,0,1)
		GameTooltip:Show()
		end)
		theFrame.ClearButton:SetScript("OnLeave", function(self) GameTooltip:Hide() end)
		theFrame.ClearButton:SetScript("OnClick", function() GGAA:ClearList() end)
		theFrame.ClearButton:SetFrameLevel(theFrame.ClearButton:GetFrameLevel() + 1)
		
		theFrame.CountButton = CreateFrame("Button", nil, theFrame)
		theFrame.CountButton:SetNormalTexture("Interface\\AddOns\\GgAdvAnnouncer\\Textures\\button-crosshairs.tga")
		theFrame.CountButton:SetPushedTexture("Interface\\AddOns\\GgAdvAnnouncer\\Textures\\button-crosshairs.tga")
		theFrame.CountButton:SetHighlightTexture("Interface\\AddOns\\GgAdvAnnouncer\\Textures\\button-highlight.tga")
		theFrame.CountButton:SetWidth(12)
		theFrame.CountButton:SetHeight(12)
		theFrame.CountButton:SetPoint("RIGHT", theFrame.ClearButton,"LEFT", -4, 0)
		theFrame.CountButton:SetScript("OnEnter", function(self)
		GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
		GameTooltip:AddLine("NearbyCount", 1, 0.82, 0, 1)
		GameTooltip:AddLine("NearbyCountDescription",0,0,0,1)
		GameTooltip:Show()
		end)
		theFrame.CountButton:SetScript("OnLeave", function(self) GameTooltip:Hide() end)
		theFrame.CountButton:SetScript("OnClick", function() Default_CHAT_FRAME:AddMessage("SpySignatureColoRed".."PlayersDetectedColoRed"..GGAA:GetNearbyListSize()) end)
		theFrame.CountButton:SetFrameLevel(theFrame.CountButton:GetFrameLevel() + 1)

		
		GGAA.GGM.GUI.MainWindow.Rows = {}
		GGAA.GGM.GUI.MainWindow.CurRows = 0

		for i = 1, GGAA.ButtonLimit do
			GGAA:CreateRow(i)
		end

		GGAA:RestoreGGM.GUI.MainWindowPosition(GGAA.db.profile.GGM.GUI.MainWindow.Position.x, GGAA.db.profile.GGM.GUI.MainWindow.Position.y, GGAA.db.profile.GGM.GUI.MainWindow.Position.w, 44)
		GGAA:SetupGGM.GUI.MainWindowButtons()
		GGAA:ResizeGGM.GUI.MainWindow()
		GGAA:ScheduleRepeatingTimer("ManageExpirations", 10, true)
		GGAA:InitOrder()
	end

	if not GGAA.AlertWindow then
		GGAA.AlertWindow = CreateFrame("Frame", "Spy_AlertWindow", UiParent)
		GGAA.AlertWindow:ClearAllPoints()
		GGAA.AlertWindow:SetPoint("TOP", UiParent, "TOP", 0, -90)
		GGAA.AlertWindow:SetPoint("CENTER", UiParent, "CENTER", 0, 0)
		GGAA.AlertWindow:SetHeight(42)
		GGAA.AlertWindow:SetBackdrop({
			bgFile = "Interface\\AddOns\\GgAdvAnnouncer\\Textures\\alert-background.tga", tile = true, tileSize = 8,
			edgeFile = "Interface\\AddOns\\GgAdvAnnouncer\\Textures\\alert-industrial.tga", edgeSize = 8,
			insets = { left = 8, right = 8, top = 8, bottom = 8 },
		})
		GGAA.Colors:RegisterBackground("Alert", "Background", GGAA.AlertWindow)

		GGAA.AlertWindow.Icon = CreateFrame("Frame", nil, GGAA.AlertWindow)
		GGAA.AlertWindow.Icon:ClearAllPoints()
		GGAA.AlertWindow.Icon:SetPoint("TOPLEFT", GGAA.AlertWindow, "TOPLEFT", 6, -5)
		GGAA.AlertWindow.Icon:SetWidth(32)
		GGAA.AlertWindow.Icon:SetHeight(32)

		GGAA.AlertWindow.title = GGAA.AlertWindow:CreateGGC.FontString(nil, "OVERLAY", "GameFontNormal")
		GGAA.AlertWindow.title:SetPoint("TOPLEFT", GGAA.AlertWindow, "TOPLEFT", 42, -3)
		GGAA.AlertWindow.title:SetJustifyH("LEFT")
		GGAA.AlertWindow.title:SetHeight(GGAA.db.profile.GGM.GUI.MainWindow.TextHeight)
		GGAA:AddGGC.FontString(GGAA.AlertWindow.title)

		GGAA.AlertWindow.Name = GGAA.AlertWindow:CreateGGC.FontString(nil, "OVERLAY", "GameFontNormal")
		GGAA.AlertWindow.Name:SetPoint("TOPLEFT", GGAA.AlertWindow, "TOPLEFT", 42, -15)
		GGAA.AlertWindow.Name:SetJustifyH("LEFT")
		GGAA.AlertWindow.Name:SetHeight(GGAA.db.profile.GGM.GUI.MainWindow.TextHeight)
		GGAA:AddGGC.FontString(GGAA.AlertWindow.Name)
		GGAA:SetFontSize(GGAA.AlertWindow.Name, GGAA.db.profile.AlertWindowNameSize)

		GGAA.AlertWindow.Location = GGAA.AlertWindow:CreateGGC.FontString(nil, "OVERLAY", "GameFontNormal")
		GGAA.AlertWindow.Location:SetPoint("TOPLEFT", GGAA.AlertWindow, "TOPLEFT", 42, -26)
		GGAA.AlertWindow.Location:SetJustifyH("LEFT")
		GGAA.AlertWindow.Location:SetHeight(GGAA.db.profile.GGM.GUI.MainWindow.TextHeight)
		GGAA:AddGGC.FontString(GGAA.AlertWindow.Location)
		GGAA:SetFontSize(GGAA.AlertWindow.Location, GGAA.db.profile.AlertWindowLocationSize)

		GGAA.AlertWindow:Hide()
	end

	--if not GGAA.MapNoteList then
		--GGAA.MapNoteList = {}
		--for i = 1, GGAA.MapNoteLimit do
			--GGAA:CreateMapNote(i)
		--end
	--end

	if not GGAA.MapTooltip then
		GGAA.MapTooltip = CreateFrame("GameTooltip", "ACR_GameTooltip", nil, "GameTooltipTemplate")
	end

	GGAA:SetCurrentList(1)
	if not GGAA.db.profile.Enabled or not GGAA.db.profile.GGM.GUI.MainWindowVis then
		GGAA.GGM.GUI.MainWindow:Hide()
	end
end










-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

-- template

--	properties = {
--		base = {
--			parentName =,
--			parent =,
--		},
--	}

local DFList = {
	CompositeContainerSuffix = "GGD.CompositeContainerSuffix",
	CompositeContainer = "GGC.CompositeContainer"
};

GGF.GRegister(DFList.CompositeContainerSuffix, "СС");

GGF.GRegister(DFList.CompositeContainer, {
	meta = {
		name = "Composite Container",
	},

	objPropList = {},
});

GGC.CompositeContainer.objects = {};

GGC.CompositeContainer.properties = {};

GGC.CompositeContainer.elements = {};

GGF.Mark(GGC.CompositeContainer);

GGC.CompositeContainer = GGF.TableSafeExtend(GGC.CompositeContainer, GGC.EntityContainer);


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Применение всех свойств к объекту класса
function GGC.CompositeContainer:Adjust()
	local wrapperOrder = {};
	local transceiverOrder = {};
	local elementOrder = {};
	local propertyOrder = {};

	for token, data in pairs(self.objectMap) do
		local ctype = data.container.ctype;
		local orderList = GGF.SwitchCase(ctype,
			{ GGE.ContainerType.Wrapper, wrapperOrder },
			{ GGE.ContainerType.Transceiver, transceiverOrder },
			{ GGE.ContainerType.Element, elementOrder },
			{ GGE.ContainerType.Property, propertyOrder }
		);
		if (not ctype) then
			-- TODO: нормально обработать ошибку
			print("error: unhandled object ctype - ", ctype, token);
		elseif (not orderList) then
			print("error: null order list - ", ctype, token);
			ATLASSERT(orderList);
		else
			orderList[#orderList + 1] = token;
		end
	end

	-- TODO: возможно убрать..
	ATLASSERT(#wrapperOrder > 0);
	--for token, data in pairs(ObjectMap) do
		--self['Adjust'..token](self);
	--end
	-- WARNING: не менять местами!!!
	-- NOTE: там аджаст свойств в объекты из листа
	for index, token in pairs(wrapperOrder) do
		--self['Adjust'..token](self);
	end
	for index, token in pairs(transceiverOrder) do
		-- NOTE: не требуется(только при смене значения)
		--self['Adjust'..token](self);
	end
	for index, token in pairs(propertyOrder) do
		-- NOTE: не требуется в принципе
	end
	for index, token in pairs(elementOrder) do
		self['Adjust'..token](self);
	end
end


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Взятие ссылки на враппер объекта
function GGC.CompositeContainer:getWrapperRef(in_refObjToken)
	return self["Get"..in_refObjToken]();
end


-- Смена активного параметра вычисления(функции/значения)
function GGC.CompositeContainer:DefaultDataModify(in_container, in_value)
	--if (in_container.ctype == GGE.ContainerType.Transceiver) then
		--local pToken = in_container.refparamtoken;
		--local wr = self:GetWrapperRef(pToken);
		--wr["Modify"..pToken](wr, in_value);
		--return;
	--end

	if (type(in_value) ~= GGE.ParameterType.Function) then
		in_container.ptype = GGE.ParameterType.Value;
		in_container.parameter.value = in_value;
	else
		in_container.ptype = GGE.ParameterType.Function;
		in_container.parameter.func = in_value;
	end
end


-- Получение значения поля
function GGC.CompositeContainer:DefaultDataValueGet(in_container)
	--if (in_container.ctype == GGE.ContainerType.CC_TRANSCEIVER) then
		--local pToken = in_container.refparamtoken;
		--local wr = self:GetWrapperRef(pToken);
		--return wr["Get"..pToken](wr);
	--end

	if (in_container.ptype == GGE.ParameterType.Value) then
		return in_container.parameter.value;
	else
		return in_container.parameter.func(in_container.interdata.argline);
	end
	ATLASSERT(false);
end


-- Получение вычислителя поля
function GGC.CompositeContainer:DefaultDataCounterGet(in_container)
	--if (in_container.ctype == GGE.ContainerType.Transceiver) then
		--local pToken = in_container.refparamtoken;
		--local wr = self:GetWrapperRef(pToken);
		--return wr["Get"..pToken.."Counter"](wr);
	--end

	if (in_container.ptype == GGE.ParameterType.Value) then
		return nil;
	else
		return in_container.parameter.func;
	end
	ATLASSERT(false);
end


-- Получение списка объектов для параметра(linked to objects)
function GGC.CompositeContainer:DefaultLinkedToListGet(in_container)
	return in_container.linkedto;
end


-- Применение параметра к объекту
function GGC.CompositeContainer:DefaultAdjust(in_container)
	--if (in_container.ctype == GGE.ContainerType.Transceiver) then
		--local pToken = in_container.refparamtoken;
		--local wr = self:GetWrapperRef(pToken);
		--wr["Adjust"..pToken](wr);
		--return;
	--end
	ATLASSERT(false);
end


-- Дополнение активного набора аргументов функционального вычислителя
function GGC.CompositeContainer:DefaultAddArgline(in_container, in_key, in_value)
	--if (in_container.ctype == GGE.ContainerType.Transceiver) then
		--local pToken = in_container.refparamtoken;
		--local wr = self:GetWrapperRef(pToken);
		--wr["ArglineAdd"..pToken](wr, in_key, in_value);
		--return;
	--end

	in_container.interdata.argline[in_key] = in_value;
end


-- Смена активного набора аргументов функционального вычислителя
function GGC.CompositeContainer:DefaultSetArgline(in_container, in_argline)
	--if (in_container.ctype == GGE.ContainerType.Transceiver) then
		--local pToken = in_container.refparamtoken;
		--local wr = self:GetWrapperRef(pToken);
		--wr["ArglineSet"..pToken](wr, in_argline);
		--return;
	--end

	in_container.interdata.argline = in_argline;
end


-- Специальная обертка присвоения свойств
function GGC.CompositeContainer:adjustWrapper(in_adjfunc, in_token, in_ctype, in_mark)
	if (type(in_adjfunc) ~= 'table') then
		ATLASSERT(false);
	end
	if (in_ctype == GGE.ContainerType.Object) then
		ATLASSERT(false);
	end
	if (in_ctype == GGE.ContainerType.Wrapper) then
		if (self:checkHighLevel(in_mark)) then
			in_adjfunc(self);
		end
		return;
	end
	if (in_ctype == GGE.ContainerType.Element) then
		in_adjfunc(self);
		return;
	end
	if (in_ctype == GGE.ContainerType.Transceiver) then
		local list = self['Get'..token..'RPTListOD'](self);
		for index, wrapperName in ipairs(list) do
			in_adjfunc[wrapperName](self, self:getWrapperRef(wrapperName));
		end
		return;
	end
	ATLASSERT(false);
end