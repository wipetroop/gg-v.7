local GGC.FontStrings={}
local FontFile
local SM = LibStub:GetLibrary("LibShaRedMedia-3.0")
local _

SM:Register("font", "ABF", [[Interface\AddOns\Spy\Fonts\ABF.ttf]])

function GGAA:AddGGC.FontString(string)
	local Font, Height, Flags

	GGC.FontStrings[#GGC.FontStrings+1] = string

	if not FontFile and Spy.db.profile.Font then
		FontFile = SM:Fetch("font", Spy.db.profile.Font)
	end

	if FontFile then
		Font, Height, Flags = string:GetFont()
		if Font ~= FontFile then
			string:SetFont(FontFile, Height, Flags)
		end
	end
end

function GGAA:SetFont(fontname)
	local Height, Flags

	Spy.db.profile.Font = fontname
	FontFile = SM:Fetch("font",fontname)

--	for _, v in pairs(GGC.FontStrings) do
	for k, v in pairs(GGC.FontStrings) do
		k, Height, Flags = v:GetFont()
		v:SetFont(FontFile, Height, Flags)
	end
end
