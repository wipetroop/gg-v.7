-- Инициализация на стадии загрузки аддона
function GGM.ACR:OnAddonLoad()
	GGM.COM:RegisterInterComm(GGD.ACR.ClusterToken, GGM.ACR.EvokeController);

	GGM.IOC.OutputController:AddonWelcomeMessage({
		cluster = GGD.ACR.ClusterToken,
		version = GGD.ACR.ClusterVersion,
		build = GGD.ACR.ClusterBuild,
		mbi = GGD.ACR.ClusterMBI,
	});
end