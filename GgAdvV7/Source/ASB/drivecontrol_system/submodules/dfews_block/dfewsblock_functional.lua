local This = GGF.ModuleGetWrapper("ASB-1-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-- Обработчик событий таймеров
function This:mirrorTimerHandler(in_table, in_event, in_type, in_curval, in_maxval, in_speed, ...)
	if (in_type == "EXHAUSTION") then
		if (in_event == "MIRROR_TIMER_STOP" or in_event == "MIRROR_TIMER_PAUSE") then
			self.vs_countdownTimer.stopped = true;
			GGM.GUI.DrivecontrolInstrumentation:SetFTAState("idle");
		elseif (in_event == "MIRROR_TIMER_START") then
			self.vs_countdownTimer.stopped = false;
			self.vs_fatigueWatchdog.current = in_curval/1000;
			self.vs_fatigueWatchdog.max = in_maxval/1000;
			self.vs_fatigueWatchdog.speed = in_speed;
			GGM.GUI.DrivecontrolInstrumentation:SetFTAState("nonCritical");
		end
	end
end


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------