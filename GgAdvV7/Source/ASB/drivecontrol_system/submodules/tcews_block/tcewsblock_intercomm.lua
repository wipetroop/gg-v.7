local This = GGF.ModuleGetWrapper("ASB-1-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-- п.3.0 (Addon System Timer)
function This:MPRC_AST_RUNTIME(in_elapsed)
	GGF.Timer(self.vs_telemetryTimer, in_elapsed, function()
		if (IsFalling()) then
			self.vs_fallWatchdog.time = self.vs_fallWatchdog.time + self.vs_telemetryTimer.interval;
			if (not self.vs_fallWatchdog.falling) then
				self.vs_fallWatchdog.falling = true;
				self.vs_fallWatchdog.startSpeed = GGM.FCS.StatisticStorage:GetCurrentSpeedData().fact;
			end
			self.vs_fallWatchdog.curSpeed = GGM.FCS.StatisticStorage:GetCurrentSpeedData().fact;
			-- NOTE: зазор по времени отбрасывает прыжки
			-- NOTE: при падении скорости более чем на опр. величину включаем алерт
			-- Warning: плохо работает со способностями, уменьшающими скорость падения
			if (self.vs_fallWatchdog.time > self.cn_alertTimeWindow) then
				if (self.vs_fallWatchdog.startSpeed - self.vs_fallWatchdog.curSpeed > self.cn_speedMaximumChange
					or self.vs_fallWatchdog.curSpeed < self.cn_speedMaximumChange) then
					GGM.GUI.DrivecontrolInstrumentation:SetFLAState("critical");
				else
					GGM.GUI.DrivecontrolInstrumentation:SetFLAState("nonCritical");
				end
			end
		else
			self.vs_fallWatchdog.time = 0;
			self.vs_fallWatchdog.falling = false;
			GGM.GUI.DrivecontrolInstrumentation:SetFLAState("idle");
		end
	end);
end


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------