local This = GGF.ModuleGetWrapper("ASB-1-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.6 (Main Event Callbacks)
function This:MPRC_MEC_INITIALIZE()
	self.vs_timerFrame:RegisterEvent("MIRROR_TIMER_START");
	self.vs_timerFrame:RegisterEvent("MIRROR_TIMER_STOP");
	self.vs_timerFrame:RegisterEvent("MIRROR_TIMER_PAUSE");

	self.vs_timerFrame:SetScript("OnEvent", function(...) self:mirrorTimerHandler(...) end);
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-- п.3.0 (Addon System Timer)
function This:MPRC_AST_RUNTIME(in_elapsed)
	GGF.Timer(self.vs_telemetryTimer, in_elapsed, function()
		-- NOTE: А это че за таймер??
	end);

	GGF.Timer(self.vs_countdownTimer, in_elapsed, function()
		self.vs_drowningWatchdog.current = self.vs_drowningWatchdog.current + self.vs_drowningWatchdog.speed;
		if (self.vs_drowningWatchdog.current <= self.vs_drowningWatchdog.max/2) then
			GGM.GUI.DrivecontrolInstrumentation:SetDAState("critical");
		end
	end);
end


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------