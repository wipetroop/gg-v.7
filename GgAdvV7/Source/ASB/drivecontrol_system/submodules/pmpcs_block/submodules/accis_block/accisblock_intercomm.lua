local This = GGF.ModuleGetWrapper("ASB-1-3-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.6 (Main Event Callbacks)
function This:MPRC_MEC_INITIALIZE()
	--Core Autorun toggle logic--
	hooksecurefunc("ToggleAutoRun",function()
		if self.cf_autorunToggled then
			self.cf_autorunToggled = false;
		else 
			self.cf_autorunToggled = true;
		end
		if (self.cf_enabled) then
			if (self.cf_autorunToggled) then
				GGM.GUI.DrivecontrolInstrumentation:SetCCSState("cruiseon");
			else
				GGM.GUI.DrivecontrolInstrumentation:SetCCSState("cruiseoff");
			end
		else
			GGM.GUI.DrivecontrolInstrumentation:SetCCSState("cruiseoff");
		end
	end)
	--End Core Autorun toggle logic--

	--AutoRun key press logic block--
	hooksecurefunc("MoveForwardStart", function()
		self.cf_forwardButtonDown = true;
		if (self.cf_forwardButtonDown) and (self.cf_autorunToggled) then
			self.cf_autorunToggled = false;
			GGM.GUI.DrivecontrolInstrumentation:SetCCSState("cruiseoff");
		end
	end)

	hooksecurefunc("MoveBackwardStart", function()
		self.cf_backwardButtonDown = true;
		if (self.cf_backwardButtonDown) and (self.cf_autorunToggled) then
			self.cf_autorunToggled = false;
			GGM.GUI.DrivecontrolInstrumentation:SetCCSState("cruiseoff");
		end
	end)

	hooksecurefunc("CameraOrSelectOrMoveStart", function()
		self.cf_leftButtonDown = true
		if (self.cf_rightButtonDown) and (self.cf_autorunToggled) then
			self.cf_autorunToggled = false;
			GGM.GUI.DrivecontrolInstrumentation:SetCCSState("cruiseoff");
		end
	end)

	hooksecurefunc("CameraOrSelectOrMoveStop", function()
		self.cf_leftButtonDown = false;
	end)

	hooksecurefunc("TurnOrActionStart", function()
		self.cf_rightButtonDown = true
		if (self.cf_leftButtonDown) and (self.cf_autorunToggled) then
			self.cf_autorunToggled = false;
			GGM.GUI.DrivecontrolInstrumentation:SetCCSState("cruiseoff");
		end
	end)

	hooksecurefunc("TurnOrActionStop", function()
		self.cf_rightButtonDown = false;
	end)
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------