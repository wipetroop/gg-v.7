local This = GGF.ModuleGetWrapper("ASB-1-3-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-- п.3.0 (Addon System Timer)
function This:MPRC_AST_RUNTIME(in_elapsed)
	-- NOTE: отключено в воздухе
	-- NOTE: плохо работает на быстрых стрейфах туда-сюда из-за малого суммарного перемещения
	-- NOTE: большая погрешность определения скорости из-за малого промежутка времени
	GGF.Timer(self.vs_telemetryTimer, in_elapsed, function()
		--print(GetUnitSpeed("Player"));
		--print(UnitPosition("Player"));
		if (IsFlying()) then
			GGM.GUI.DrivecontrolInstrumentation:SetOCAState("idle");
		else
			local spData = GGM.FCS.StatisticStorage:GetCurrentSpeedData();	-- тут фактическая и теоретическая(близовская)
			local pFact, pTheor = spData.fact, spData.theor;

			-- TODO: разобраться с иксом(откуда он взялся то?)
			if (X ~= 0 and pFact < pTheor/(1 + self.vs_permissibleVariation)) then
				GGM.GUI.DrivecontrolInstrumentation:SetOCAState("triggered");
			else
				GGM.GUI.DrivecontrolInstrumentation:SetOCAState("idle");
			end
		end
	end);
end


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------