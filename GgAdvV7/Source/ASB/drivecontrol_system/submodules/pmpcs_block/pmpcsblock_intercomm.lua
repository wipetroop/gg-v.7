local This = GGF.ModuleGetWrapper("ASB-1-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-- п.3.0 (Addon System Timer)
function This:MPRC_AST_RUNTIME(in_elapsed)
	GGF.Timer(self.vs_telemetryTimer, in_elapsed, function()
		if (IsSwimming()) then
			GGM.GUI.DrivecontrolInstrumentation:SetSWSState("swim");
		else
			GGM.GUI.DrivecontrolInstrumentation:SetSWSState("noswim");
		end

		if (IsFlying()) then
			GGM.GUI.DrivecontrolInstrumentation:SetFSState("fly");
		else
			GGM.GUI.DrivecontrolInstrumentation:SetFSState("nofly");
		end

		if (IsMounted()) then
			GGM.GUI.DrivecontrolInstrumentation:SetMTSState("mounted");
		else
			GGM.GUI.DrivecontrolInstrumentation:SetMTSState("nomounted");
		end

		if (IsStealthed()) then
			GGM.GUI.DrivecontrolInstrumentation:SetSTSState("stealth");
		else
			GGM.GUI.DrivecontrolInstrumentation:SetSTSState("nostealth");
		end

		if (UnitOnTaxi("Player")) then
			GGM.GUI.DrivecontrolInstrumentation:SetTSState("taxi");
		else
			GGM.GUI.DrivecontrolInstrumentation:SetTSState("notaxi");
		end

      local cur, run;
		GGF.VersionSplitterProc({
   		{
   			versionInterval = {GGD.TocVersionList.Vanilla, GGD.TocVersionList.WotLK},
   			action = function()
               cur = GetUnitSpeed("Player");
               run = 7;    -- #NOTE: фиксированное значение по умолчанию
            end,
         },
         {
            versionInterval = {GGD.TocVersionList.Cata, GGD.TocVersionList.Invalid},
            action = function()
         		cur, run, _, _ = GetUnitSpeed("Player");
            end,
         },
      });
      if (cur <= 0 + self.cn_speedCountError) then
         GGM.GUI.DrivecontrolInstrumentation:SetMVSState("stay");
      elseif (cur >= run - self.cn_speedCountError) then
         GGM.GUI.DrivecontrolInstrumentation:SetMVSState("run");
      else
         GGM.GUI.DrivecontrolInstrumentation:SetMVSState("walk");
      end
	end);
end


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------