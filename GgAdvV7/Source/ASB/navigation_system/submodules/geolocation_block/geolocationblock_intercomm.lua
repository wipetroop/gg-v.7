local This = GGF.ModuleGetWrapper("ASB-3-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.6 (Main Event Callbacks)
function This:MPRC_MEC_INITIALIZE()
	self.fr_zoneTracker:RegisterEvent("ZONE_CHANGED_NEW_AREA");

	self.fr_zoneTracker:SetScript("OnEvent", function(...) self:zoneTransmitter(...) end);
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------