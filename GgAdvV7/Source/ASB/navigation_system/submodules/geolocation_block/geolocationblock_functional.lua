local This = GGF.ModuleGetWrapper("ASB-3-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Взятие кластера названий зон карты
function This:getZoneCluster()
	return self.md_zoneCluster;
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-- TODO: прибраться тут...(GG-63)
-- Автомат смены зоны геолокации
function This:zoneTransmitter(in_event)
	local mapID = 0;
	GGF.VersionSplitterProc({
		{
			versionInterval = {GGD.TocVersionList.Vanilla, GGD.TocVersionList.Draenor},
			action = function()
				mapID = GetCurrentMapAreaID();
				SetMapToCurrentZone();
			end,
		},
		{
			versionInterval = {GGD.TocVersionList.Legion, GGD.TocVersionList.Invalid},
			action = function()
				mapID = C_Map.GetBestMapForUnit("player");
				if (mapID == nil) then
					mapID = 0;
				end
			end,
		},
	});
	-- NOTE: вроде бы её больше нет
	--SetMapToCurrentZone();
	local ai_new = mapID;--GetCurrentMapAreaID();
	local zt_new = self.md_zoneCluster[tostring(ai_new)] or "WPZ";
	--local zoneName = GGM.ACR.CombatInformation.zoneCluster[tostring(areaid)] or "Arena/OPZ";
	--message(zoneName);
	--print("Zone geolocation changed");
	if (self.vs_currentZone.ID ~= ai_new) then
		print(self.vs_currentZone.name.."("..self.vs_currentZone.ID..") -> "..zt_new.."("..ai_new..")");
		self.vs_currentZone.ID = ai_new;
		self.vs_currentZone.name = zt_new;
		GGM.GUI.SystemInstrumentation:SetCurrentZoneSignature(zt_new);

		GGM.ACR.RuntimeLogger:SetBattleLogZoneName(zt_new);

		--ACR_CL.battleLogCluster.metaInformation.logZoneName = zt_new;
		--print(ai_new, zt_new);
	else
		--print(ai_new, zt_new);
	end
	--if select(2, IsInInstance()) == "pvp" then
		--GGM.GUI.CombatDashboard.proceedBattleZoneEnter(ai_new, zt_new);
	--end
end


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------