-- Инициализация на стадии загрузки аддона
function GGM.ASB:OnAddonLoad()
	GGM.COM:RegisterInterComm(GGD.ASB.ClusterToken, GGM.ASB.EvokeController);

	GGM.IOC.OutputController:AddonWelcomeMessage({
		cluster = GGD.ASB.ClusterToken,
		version = GGD.ASB.ClusterVersion,
		build = GGD.ASB.ClusterBuild,
		mbi = GGD.ASB.ClusterMBI,
	});
end