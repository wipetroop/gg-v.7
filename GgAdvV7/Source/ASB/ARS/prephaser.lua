----------------------------------------------------------------------------
-- "functional" block
----------------------------------------------------------------------------


----------------------------------------------------------------------------
-- "define" block
----------------------------------------------------------------------------

GGM.ASB = {};
GGD.ASB = {};
GGD.ASB.ClusterName = "Auxiliary System Block";
GGD.ASB.ClusterToken = "ASB";
GGD.ASB.ClusterVersion = GGD.BuildMeta.Version;
GGD.ASB.ClusterBuild = "AT-"..GGD.BuildMeta.Build;
GGD.ASB.ClusterMBI = "8102-6082";