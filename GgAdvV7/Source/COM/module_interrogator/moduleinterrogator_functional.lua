local This = GGF.ModuleGetWrapper("COM-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-- api
-- Взятие карты модулей
function This:getModuleMaintenanceMap()
	--return self.md_moduleMaintenanceMap;
	return GGD.ModuleList;
end

-- api
-- Взятие дерева ошибок(на самом деле та же самая мапа)
function This:getErrorTree()
	return self:getModuleMaintenanceMap();
end

-- api
-- Взятие количества модулей и шародов
function This:getCountInfo()
	return self.vs_moduleMeta.count, self.vs_moduleMeta.shard;
end

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- Система привязки структур ошибок модулей к дереву
-- WARNING: не запускать линки повторно!!! там один раз привязка должна идти!!!
function This:linkErrorTree()
	--local moduleTable = self.md_moduleMaintenanceMap;
	local moduleTable = GGD.ModuleList;

	for name, cluster in pairs(moduleTable) do
	--for name, module in pairs(moduleTable.cluster) do
		self:linkTraverse(name, cluster.modules);
	end
end


-- Рекурсивный обход дерева модулей
function This:linkTraverse(in_path, in_moduleList)
	-- NOTE: если так, то это модуль(отсечка кластеров)
	if in_path:find("-") then
		if (GGF.ModuleGetWrapper(in_path) == nil) then
			GGF.MDL.ModuleStdError(self, 101, "path '" .. in_path .. "' not found in global");
		else
			-- TODO: как-нибудь скоммутировать по-нормальному
			--in_module.data = GGF.GetGlobalModuleData(in_path).vs_currentErrors;
		end
	end
	if (in_moduleList) then
		for name, module in pairs(in_moduleList) do
			-- NOTE: отсечка метаинформации по ошибкам(то что выше биндится)
			self:linkTraverse(in_path .. "-" .. name, module.moduleList);
		end
	end
end


-- NOTE: отключен пока что(не разработан перехватывающий хук)
-- Перехватчик сообщений об ошибках в различных аддонах
function This:traceCatch(in_errmsg)
	print("caught: ", in_errmsg);
end

-- api
-- Тайминговый опрашиватель модулей(для взятия состояния)
function This:maintenanceCheckDispatcher()
	--local moduleTable = self.md_moduleMaintenanceMap;
	local moduleTable = GGD.ModuleList;
	local gState = true;

	for name, cluster in pairs(moduleTable) do
		local rmc = self:recursiveMaintenanceCheck(name, cluster.modules);
		gState = gState and rmc;
	end
	-- NOTE: тут ничего не приколхожено к gState результату обхода
	if (not gState) then
		GGF.MDL.ModuleStdError(self, 3, "module summary init progress check failure");
	end
end


-- Рекурсивный обход дерева модулей
function This:recursiveMaintenanceCheck(in_path, in_moduleList)
	local state = true;	-- false - не до конца инициализировано
	-- NOTE: если так, то это модуль

	if in_path:find("-") then
		local md = GGF.ModuleGetWrapper(in_path);
		if (md == nil) then
			GGF.MDL.ModuleStdError(self, 101, "path '" .. in_path .. "' not found in global");
		else
			GGF.MDL.SetModuleAsInterrogated(md);
			state = GGF.MDL.CheckInitProgress(md);
			self.vs_moduleMeta.count = self.vs_moduleMeta.count + 1;
			self.vs_moduleMeta.shard = self.vs_moduleMeta.shard + GGF.MDL.GetModuleShardCount(md);
		end
	end

	if (in_moduleList) then
		for name, md in pairs(in_moduleList) do
			local rmc = self:recursiveMaintenanceCheck(in_path .. "-" .. name, md.modules);
			state = state and rmc;
		end
	end
	return state;
end


-- api
-- Проверка наличия модуля в дереве модулей
-- NOTE: совместно с опрашивателем создает покрытия множества модулей и выделение разности объявленных, определнных и инициализированных
function This:checkModuleExistence(in_moduleToken)
	--local expPath = self.md_moduleMaintenanceMap.cluster;
	for module in string.gmatch(in_moduleToken, '([^_]+)') do
		if (expPath ~= nil and expPath[module] ~= nil) then
			expPath = expPath[module].module;
		else
			return false;
		end
	end
	return true;
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------