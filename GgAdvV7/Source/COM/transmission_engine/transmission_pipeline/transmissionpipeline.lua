GGC.TransmissionPipeline = {
	states = {
		STAND_BY = 0,	-- ожидание
		ENGAGED = 1,  	-- в процессе
		DETACHED = 2,	-- завершено
	},

	-- Манифест отработки стадий(сюда регистрация идет)
	registerManifest = {
		start = {
			SLG = {},
			SVR = {},
			MVR = {},
			MCP = {},
			OBJ = {},
			ACP = {},
			AEC = {},
			MEC = {},
			ADJ = {},
			ALS = {},
			AST = {},
		},
		runtime = {
			AST = {},
		},
		finish = {
			ACP = {},
			CDM = {},
		},
	},

	-- Порядок стадий инициализации
	startOrder = {
		stage = {
			[1] = "SLG",
			[2] = "SVR",
			[3] = "MVR",
			[4] = "MCP",
			[5] = "OBJ",
			[6] = "ACP",
			[7] = "AEC",
			[8] = "MEC",
			[9] = "ADJ",
			[10] = "ALS",
			[11] = "AST"
		},
	},
	finishOrder = {
		stage = {
			[1] = "ACP",
			[2] = "CDM",
		},
	},

   stageAbbreviationInterpret = {
      start = {
         SLG = "Special Logic",
         SVR = "System Variables",
         MVR = "Miscellaneous Variables",
         MCP = "Main Config Parameters",
         OBJ = "Objects",
         ACP = "Auxiliary Config Parameters",
         AEC = "Auxiliary Event Callbacks",
         MEC = "Main Event Callbacks",
         ADJ = "Object Adjust",
         ALS = "After-load Settings",
         AST = "Addon System Timer",
      },
      runtime = {
         AST = "Addon System Timer",
      },
      finish = {
         ACP = "All Config Parameters",
         CDM = "Coup De Module",
      },
   },

	-- Порядок обращения к аддонам
	runtimeOrder = {
		start = {
			SLG = {"COM", "ECP", "IOC", "FCS", "SND", "ACR", "ASB", "GUI"},
			SVR = {"FCS", "COM", "ECP", "IOC", "SND", "ACR", "ASB", "GUI"},
			MVR = {"COM", "ECP", "IOC", "FCS", "SND", "ACR", "ASB", "GUI"},
			MCP = {"COM", "ECP", "IOC", "FCS", "SND", "ACR", "ASB", "GUI"},
			OBJ = {"COM", "ECP", "IOC", "FCS", "SND", "ACR", "ASB", "GUI"},
			ACP = {"COM", "ECP", "IOC", "FCS", "SND", "ACR", "ASB", "GUI"},
			AEC = {"COM", "ECP", "IOC", "FCS", "SND", "ACR", "ASB", "GUI"},
			MEC = {"COM", "ECP", "IOC", "FCS", "SND", "ACR", "ASB", "GUI"},
			ADJ = {"COM", "ECP", "IOC", "FCS", "SND", "ACR", "ASB", "GUI"},
			ALS = {"COM", "ECP", "IOC", "FCS", "SND", "ACR", "ASB", "GUI"},
			AST = {"COM", "ECP", "IOC", "FCS", "SND", "ACR", "ASB", "GUI"},
		},
		runtime = {
			AST = {"COM", "ECP", "IOC", "FCS", "SND", "ACR", "ASB", "GUI"},
		},
		finish = {
			ACP = {"COM", "ECP", "IOC", "FCS", "SND", "ACR", "ASB", "GUI"},
			CDM = {"COM", "ECP", "IOC", "FCS", "SND", "ACR", "ASB", "GUI"},
		},
	},

	enterer = CreateFrame("FRAME"),
	runtimer = CreateFrame("FRAME"),
	finisher = CreateFrame("FRAME"),
   announceFS = {},
   delta = {},

	-- WARNING: костыль
	initialized = false,
	-- Флаг входа в игру(реагирует на PLAYER_ENTERING_WORLD)
	enteRed = false,

	vs_updateTimer = {
		tick = 0,
		interval = 0.5,
		stopped = false,
	},

	vs_garbageTimer = {
		tick = 0,
		interval = 5,
		stopped = false,
	},
};

This = GGC.TransmissionPipeline;

-------------------------------------------------------------------------------
-- Object initializers(контрольный вызов из инициализатора модуля(применение свойств по умолчанию))
-------------------------------------------------------------------------------


--
function This:Create()
	local obj = {};

	obj = GGF.TableExtend(self);

	obj:initialize();

	return obj;
end


--
function This:initialize()
	local filler = function(in_obj, in_stage, in_token)
		in_obj.registerManifest[in_stage][in_token].data = {};
		for k,v in ipairs(in_obj.runtimeOrder[in_stage][in_token]) do
			in_obj.registerManifest[in_stage][in_token].data[k] = {
				token = v,
				func = nil,
				state = 0,
				object = nil, -- нужен для вызова функции с self параметром
			};
		end
	end

	-- TODO: проверить работоспособность
	filler(self, "start", "SLG");
	filler(self, "start", "SVR");
	filler(self, "start", "MVR");
	filler(self, "start", "MCP");
	filler(self, "start", "OBJ");
	filler(self, "start", "ACP");
	filler(self, "start", "AEC");
	filler(self, "start", "MEC");
	filler(self, "start", "ADJ");
	filler(self, "start", "ALS");
	filler(self, "start", "AST");
	filler(self, "runtime", "AST");
	filler(self, "finish", "ACP");
	filler(self, "finish", "CDM");
end


-------------------------------------------------------------------------------
-- Registers(регистраторы эвентов)
-------------------------------------------------------------------------------


--
function This:registerPlayerEnter()
	self.enterer:RegisterEvent("PLAYER_ENTERING_WORLD");
	--self.enterer:RegisterEvent("Player_ALIVE");
	-- Костыль в виде окантовки вызова функции, иначе идет кривой селф туда
	self.enterer:SetScript("OnEvent", function() self:enterTrigger() end);
end


--
function This:registerPlayerLogout()
	-- В теории эта конструкция защищена от бинда на эвент до объявления самого эвента
	self.finisher:RegisterEvent("PLAYER_LOGOUT");
end


-- TODO: разобраться с коллбеками введя таки отдельный аддон для тестов
-- пока что похоже, что селф оттуда идет кривой...при этом линк переменной на поле класса не варик скорее всего
--
function This:enterTrigger()
	self.enteRed = true;
	self:registerTableCheck();
end


-- Регистрация функции на запуск по блоку, стадии и аддону
function This:Register(in_block, in_stage, in_addon, in_object, in_func)
	local regCortege = self:find(in_addon, self.registerManifest[in_block][in_stage].data);
	regCortege.func = in_func;
	regCortege.object = in_object;
	self:registerTableCheck();
end


-- Смена значения параметра
function This:changeStageState(in_block, in_stage, in_addon, in_state)
	-- TODO: чек на пустые значения(GG-57)
	self:find(in_addon, self.registerManifest[in_block][in_stage].data).state = in_state;
end


-- TODO: причесать(GG-63)
-- Поиск
function This:find(f, l) -- find element v of l satisfying f(v)
	for _, v in ipairs(l) do
		if f == v.token then
			return v;
		end
	end
	return nil;
end


-- Проверка регистрации функций, если зарегано все, что нужно - запускается пайплайн
-- Только старт
-- При всем этом, если пусканулся старт, то финиш тоже разрешен
function This:registerTableCheck()
	if (not self.enteRed) then
		return;
	end

	for blkName, blk in pairs(self.registerManifest) do
		for stgName, stg in pairs(blk) do
			for addIdx, addMeta in ipairs(stg.data) do
				if addMeta.func == nil then
					return;
				end
			end
		end
	end

	if (not self:checkCOMConsistency()) then
		return;
	end

	-- TODO: вставить звук иммобилайзера(GG-19)
	GGC.TriggerTimer:Create({
		time = 7,
		callback = function() self:engagePipeline(); end
	});
end


-- Проверка наличия поля
function This:checkExistence(in_field)
	return (in_field ~= nil) and (in_field ~= "");
end


-- Проверка всех классов фреймворка на наличие полей
function This:checkCOMConsistency()
	for name, structure in pairs(GGC) do
		if (structure.meta) then
			if (structure.instanceTemplate and structure.instanceTemplate.cntchecker) then
				-- NOTE: выкидываем из рассмотрения параметрический контейнер
			elseif (not self:checkExistence(structure.meta.suffix)
				or not self:checkExistence(structure.meta.mark)
				or not self:checkExistence(structure.meta.name)) then
				print("meta content check failed: ", structure.meta.name);
				ATLASSERT(false);
				return false;
			end
		elseif (structure.instanceTemplate and structure.instanceTemplate.MPRC) then
			-- NOTE: модуль
			-- TODO: дописать проверку
		elseif (structure.runtimeOrder) then
			-- NOTE: этот класс
		else
			print("structure check failed: ", name);
			ATLASSERT(false);
			return false;
		end
	end
	return true;
end

-- Запуск пайплайна инициализации, бинд рантаймера и финишера
function This:engagePipeline()
	if (initialized) then
		return;
	end
	initialized = true;

	GGF.PrepareModuleList();
	self.announceFS = GGC.FontString:Create({}, {
      properties = {
         base = {
            parent = UIParent,
            wrapperName = "A",
         },
         miscellaneous = {
            layer = GGE.LayerType.Overlay,
            fontHeight = 32,
            hidden = true,
            font = "DD_FONT",
         },
         size = {
            width = 1000,
            height = 50,
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = UIParent,
            justifyH = GGE.JustifyHType.Center,
            relativePoint = GGE.PointType.Center,
            offsetY = 200,
         },
         color = GGD.Color.Red,
      },
   });
   GGF.OuterInvoke(self.announceFS, "Adjust");

   self.announceEB = GGC.TextEditFrame:Create({}, {
      properties = {
         base = {
            parent = UIParent,
            wrapperName = "A",
         },
         miscellaneous = {
            --fontHeight = 40,
            hidden = false,
         },
         size = {
            width = 500,
            height = 300,
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = UIParent,
            relativePoint = GGE.PointType.Center,
            --offsetX = 100,
            --offsetY = -100,
         },
         backdrop = GGD.Backdrop.Segregate,
      },
   });
   GGF.OuterInvoke(self.announceEB, "Adjust");

	self.delta = {};
   self:announceLoadStart();
   self:announceStage(1);
   GGC.TriggerTimer:Create({
      time = 1,
      callback = function() self:stageInit(1); end
   });
	--for odIdx, odToken in ipairs(self.startOrder.stage) do
		--local start = time()
      --GGF.OuterInvoke(announceFS, "SetText", "Loading... stage: " .. odToken);
		--for addIdx, addMeta in ipairs(self.registerManifest.start[odToken].data) do
			--if (not addMeta.func(addMeta.object)) then
				--return;
			--end
		--end
		--local dur = time() - start;
		--delta[odToken] = dur;
	--end
	--for odToken, dur in pairs(delta) do
		--print("stage", odToken, dur);
	--end

	--self.runtimer:SetScript("OnUpdate", function(in_self, in_elapsed)
		--self:updateTimerTick(self.vs_updateTimer, in_elapsed);
		--self:garbageTimerTick(self.vs_garbageTimer, in_elapsed);
	--end);

	--self.finisher:SetScript("OnEvent", function()
		--for odIdx, odToken in ipairs(self.finishOrder.stage) do
			--for addIdx, addMeta in pairs(self.registerManifest.finish[odToken].data) do
				--addMeta.func(addMeta.object);
			--end
		--end
	--end);

	--for addIdx, addMeta in pairs(self.registerManifest.runtime.AST.data) do
		--addMeta.func(addMeta.object, in_timerdata.interval);
	--end
end

-- 
function This:stageInit(in_odIdx)
   if (in_odIdx > #self.startOrder.stage) then
      self.finisher:SetScript("OnEvent", function()
         for odIdx, odToken in ipairs(self.finishOrder.stage) do
            for addIdx, addMeta in pairs(self.registerManifest.finish[odToken].data) do
               addMeta.func(addMeta.object);
            end
         end
      end);
      --for odToken, dur in pairs(self.delta) do
         --print("stage", odToken, dur);
      --end
      self:announceTime(in_odIdx - 1);
      self:announceLoadTimeResult();
      self:announceModuleCount();
      GGF.OuterInvoke(self.announceFS, "SetHidden", true);
      self.announceFS = nil;
      collectgarbage();
      GGC.TriggerTimer:Create({
         time = 7,
         callback = function()
            GGF.OuterInvoke(self.announceEB, "SetHidden", true);
            GGM.GUI.InformationPanel:SetTEFData(GGF.OuterInvoke(self.announceEB, "GetText"));
         end
      });
      return;
   end
   local start = GetTime();
   local odToken = self.startOrder.stage[in_odIdx];
   for addIdx, addMeta in ipairs(self.registerManifest.start[odToken].data) do
      if (not addMeta.func(addMeta.object)) then
         print("start sequence failed on stage", odToken);
         return;
      end
   end
   local dur = GetTime() - start;
   self.delta[odToken] = dur;
   self:announceStage(in_odIdx+1);
   GGC.TriggerTimer:Create({
      time = 1,
      callback = function() self:stageInit(in_odIdx+1); end
   });
end

-- 
function This:announceTime(in_odIdx)
   if (in_odIdx > 0) then
      local coloredTime = GGF.StructColorToStringColor(GGD.Color.Green) .. string.format("%.2f", self.delta[self.startOrder.stage[in_odIdx]]) .. GGF.FlushStringColor();
      GGF.OuterInvoke(self.announceEB, "AddText", "| " .. coloredTime .. " sec.");
      GGF.OuterInvoke(self.announceEB, "NewString");
   end
end

-- 
function This:announceModuleCount()
   GGM.COM.ModuleInterrogator:MaintenanceCheckDispatcher();
   local count, shard = GGM.COM.ModuleInterrogator:GetCountInfo();
   local coloredCount = GGF.StructColorToStringColor(GGD.Color.Green) .. string.format("%i", count) .. GGF.FlushStringColor();
   local coloredShard = GGF.StructColorToStringColor(GGD.Color.Green) .. string.format("%i", shard) .. GGF.FlushStringColor();
   GGF.OuterInvoke(self.announceEB, "AddText", "Load trace..");
   GGF.OuterInvoke(self.announceEB, "NewString");
   GGF.OuterInvoke(self.announceEB, "AddText", "Module count: ".. coloredCount);
   GGF.OuterInvoke(self.announceEB, "NewString");
   GGF.OuterInvoke(self.announceEB, "AddText", "Shard count: ".. coloredShard);
end

-- 
function This:announceStage(in_odIdx)
   if (in_odIdx > #self.startOrder.stage) then
      return;
   end
   local odToken = self.startOrder.stage[in_odIdx];
   local interpret = self.stageAbbreviationInterpret.start[odToken];
   -- NOTE: Время прошлого стейджа
   self:announceTime(in_odIdx - 1);
   local coloredInterpret = GGF.StructColorToStringColor(GGD.Color.Neutral) .. interpret .. GGF.FlushStringColor();
   local coloredToken = GGF.StructColorToStringColor(GGD.Color.Green) .. odToken .. GGF.FlushStringColor();
   GGF.OuterInvoke(self.announceFS, "SetText", "Loading... stage: " .. odToken .. " [ " .. interpret .. " ] ");
   GGF.OuterInvoke(self.announceEB, "AddText", "Stage: " .. coloredToken .. " [ " .. coloredInterpret .. " ] ");
end

-- 
function This:announceLoadTimeResult()
   local summary = 0;
   for odToken, dur in pairs(self.delta) do
      summary = summary + dur;
   end
   local coloredTime = GGF.StructColorToStringColor(GGD.Color.Green) .. string.format("%.2f", summary) .. GGF.FlushStringColor();
   GGF.OuterInvoke(self.announceEB, "AddText", "Addon loading finished..");
   GGF.OuterInvoke(self.announceEB, "NewString");
   GGF.OuterInvoke(self.announceEB, "AddText", "result time: " .. coloredTime .. " sec.");
   GGF.OuterInvoke(self.announceEB, "NewString");
end

--
function This:announceLoadStart()
   GGF.OuterInvoke(self.announceEB, "AddText", "Addon loading started..");
   GGF.OuterInvoke(self.announceEB, "NewString");
end

-- NOTE: временное решение - вызывать общий onUpdate раз в 0.5 сек, чтоб не плодить бесконечные циклы
-- Свертывание количества таймеров бесконечного цикла до 1
function This:updateTimerTick(in_timerdata, in_elapsed)
	if (in_timerdata.stopped == true) then
		return;
	end
	if (in_timerdata.tick >= in_timerdata.interval) then
		in_timerdata.tick = 0;

		for addIdx, addMeta in pairs(self.registerManifest.runtime.AST.data) do
			addMeta.func(addMeta.object, in_timerdata.interval);
		end
	else
		in_timerdata.tick = in_timerdata.tick + in_elapsed;
	end
end


-- Сборщик мусора
function This:garbageTimerTick(in_timerdata, in_elapsed)
	if (in_timerdata.stopped == true) then
		return;
	end
	if (in_timerdata.tick >= in_timerdata.interval) then
		in_timerdata.tick = 0;

		-- NOTE: плохой вариант - лагает
		--collectgarbage();
	else
		in_timerdata.tick = in_timerdata.tick + in_elapsed;
	end
end