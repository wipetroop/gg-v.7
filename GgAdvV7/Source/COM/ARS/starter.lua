GGM.COM.transmission = {};
GGM.COM.transmission = GGC.TransmissionPipeline:Create();

function GGM.COM:RegisterInterComm(in_clustertoken, in_evokeInstance)
	local stList = { "SLG", "SVR", "MVR", "MCP", "OBJ", "ACP", "AEC", "MEC", "ADJ", "ALS", "AST" };
	local rtList = { "AST" };
	local fnList = { "ACP", "CDM" };
	local reg = function(in_list, in_stage, in_suffix)
		for idx,regline in ipairs(in_list) do
			self.transmission:Register(
				in_stage,
				regline,
				in_clustertoken,
				in_evokeInstance,
				(in_evokeInstance["MPRC_"..regline..in_suffix]
					or GGF.MDL["MPRC_"..regline..in_suffix])
			);
		end
	end
	reg(stList, "start", "_INITCTRL");
	reg(rtList, "runtime", "_RNTMCTRL");
	reg(fnList, "finish", "_TERMCTRL");
end


-------------------------------------------------------------------------------
-- Local frames
-------------------------------------------------------------------------------


-- Инициализация на стадии загрузки аддона
function GGM.COM:OnAddonLoad()
	self.transmission:registerPlayerEnter();
	self.transmission:registerPlayerLogout();

	self:RegisterInterComm(GGD.COM.ClusterToken, GGM.COM.EvokeController);

	GGM.IOC.OutputController:AddonWelcomeMessage({
		cluster = GGD.COM.ClusterToken,
		version = GGD.COM.ClusterVersion,
		build = GGD.COM.ClusterBuild,
		mbi = GGD.COM.ClusterMBI,
	});
end