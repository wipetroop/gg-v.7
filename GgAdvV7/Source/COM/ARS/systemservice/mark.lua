----------------------------------------------------------------------------
-- "functional" block
----------------------------------------------------------------------------


local markList = {};
-- Генератор меток объектов
local function markGenerator()
	-- NOTE: XXX-XXX
	local mark = "";
	--print("markgen e:", mark);
	while (mark == "") do
		local array = {}
		for i = 1, 3 do
			array[i] = string.char(math.random(65, 90))
		end
		array[4] = "-"
		for i = 5, 7 do
			array[i] = string.char(math.random(65, 90))
		end
		mark = table.concat(array);
		if (markList[mark] == true) then
			mark = "";
		else
			markList[mark] = true;
		end
	end
	--print("markgen rd:", mark);
	return mark;
end


-- Рекурсивный обход с выставлением меток в контейнеры
local function markTraverse(in_tokenline, in_object, in_mark)
	if (type(in_object) ~= 'table') then
		return;
	end

	-- Если условие проходит, то генератор наткнулся на какой-то параметр объекта
	if (in_object.cntchecker) then
		--if (in_object.mark ~= nil and in_object.mark ~= "") then
			--print(in_object.mark, in_object.token);
		--end
		ATLASSERT(in_object.mark == nil or in_object.mark == "");
		in_object.mark = in_mark;
		return;
	end

	for token, value in pairs(in_object) do
		--print("trav:", in_tokenline, "-", token, in_mark);
		markTraverse(in_tokenline .. " - " .. token, value, in_mark);
	end

	return;
end


-- Функция маркировки объектов и всех его свойств
function GGF.Mark(in_object)
	local mark = markGenerator();
	ATLASSERT(in_object.meta.mark == nil or in_object.meta.mark == "");
	in_object.meta.mark = mark;
	markTraverse(in_object.meta.name, in_object, mark);
end