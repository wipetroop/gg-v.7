-- NOTE: namespace
GGF.INS = {};

-- Взятие шаблона экземпляра класса
function GGF.INS.GetClassInstanceTemplate()
	return {
		__object = {},
		__objectSuffix = {},
		__objectTemplate = {},
		__overrideParams = {},
		__unused = false,
	};
end


-- Взятие экземпляра близзовского объекта(если таковой имеется)
function GGF.INS.GetObjectRef(in_instance)
	return in_instance.__object;
end


-- Установка экземпляра близзовского объекта
function GGF.INS.SetObjectRef(in_instance, in_objectRef)
	in_instance.__object = in_objectRef;
end


-- Взятие суффикса близзовского объекта
function GGF.INS.GetObjectSuffix(in_instance)
	return in_instance.__objectSuffix;
end


-- Установка суффикса близзовского объекта
function GGF.INS.SetObjectSuffix(in_instance, in_objectSuffix)
	in_instance.__objectSuffix = in_objectSuffix;
end


-- Взятие шаблона с исходными параметрами
function GGF.INS.GetObjectTemplate(in_instance)
	return in_instance.__objectTemplate;
end


-- Установка шаблона с исходными параметрами
function GGF.INS.SetObjectTemplate(in_instance, in_objectTemplate)
	in_instance.__objectTemplate = GGF.TableCopy(in_objectTemplate);
end


-- Взятие перегрузочных параметров
function GGF.INS.GetOverrideParams(in_instance)
	return in_instance.__overrideParams;
end


-- Установка перегрузочных параметров
function GGF.INS.SetOverrideParams(in_instance, in_overrideParams)
	in_instance.__overrideParams = GGF.TableCopy(in_overrideParams);
end


-- Взятие шаблона экземпляра модуля
function GGF.INS.GetModuleInstanceTemplate()
	return {
	};
end


function GGF.INS.GetUnused(in_instance)
	return in_instance.__unused;
end

function GGF.INS.SetUnused(in_instance, in_unused)
	in_instance.__unused = in_unused;
end