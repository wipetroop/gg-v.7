----------------------------------------------------------------------------
-- "functional" block
----------------------------------------------------------------------------


GGE.RegValType = {
	Default = "DEFAULT",
	Suffix = "SUFFIX",
};

local suffixList = {};

-- Регистрация дефайнов(пока что только для классов)
function GGF.GRegister(in_variablePath, in_variableValue, in_type)
	ATLASSERT(type(in_variablePath) == 'string');
	--in_type = GGF.TernExpSingle(in_type == nil, GGE.RegValType.Default, in_type);
	local extobj = _G;
	local prevobj = _G;
	local lasttoken = "";
	for v in string.gmatch(in_variablePath, "%w+") do
		if (extobj == nil) then
			--print(in_variablePath);
			ATLASSERT(false);
			return;
		end
		prevobj = extobj;
		extobj = extobj[v];
		lasttoken = v;
	end

	ATLASSERT(prevobj[lasttoken] == nil);

	if (in_type == GGE.RegValType.Suffix) then
		ATLASSERT(type(in_variableValue) == 'string');
		ATLASSERT(suffixList[in_variableValue] == nil);
		suffixList[in_variableValue] = 1;
		-- NOTE: создаем внутри суффикса дефолты
		prevobj[lasttoken] = in_variableValue;
		prevobj[in_variableValue] = {
			--Default = {}, -- NOTE: Default создается вызовом регистрации значений, т.е. тут создавать не надо
		};
	else
		-- NOTE: сюда падают енамы, т.е. де факто таблицы
		--prevobj[lasttoken] = GGF.TableCopy(in_variableValue);
		prevobj[lasttoken] = in_variableValue;
	end

	if (prevobj[lasttoken].meta and prevobj[lasttoken].meta.suffix) then
		suffixList[prevobj[lasttoken].meta.suffix] = in_variableValue;-- prevobj[lasttoken];
	end
end


-- Взятие объекта по суффиксу
function GGF.GetObjectBySuffix(in_suffix)
	return suffixList[in_suffix];
end


-- Замена в суффиксном списке
function GGF.RewriteObjectBySuffix(in_suffix, in_newObject)
	suffixList[in_suffix] = in_newObject;
end