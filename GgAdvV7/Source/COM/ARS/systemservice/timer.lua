----------------------------------------------------------------------------
-- "functional" block
----------------------------------------------------------------------------


-- Service Default timer
function GGF.Timer(in_timerdata, in_elapsed, in_callback)
	if (in_timerdata.stopped == true) then
		return;
	end
	if (in_timerdata.tick >= in_timerdata.interval) then
		in_timerdata.tick = 0;
		in_callback();
	else
		in_timerdata.tick = in_timerdata.tick + in_elapsed;
	end
end


-- Service single shot timer
function GGF.SingleShotTimer(in_timerdata, in_elapsed, in_callback)
	if (in_timerdata.stopped == true) then
		return;
	end
	if (in_timerdata.tick >= in_timerdata.interval) then
		in_timerdata.tick = 0;
		in_callback();
		in_timerdata.stopped = true;
	else
		in_timerdata.tick = in_timerdata.tick + in_elapsed;
	end
end