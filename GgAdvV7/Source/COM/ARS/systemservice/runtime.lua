----------------------------------------------------------------------------
-- "functional" block
----------------------------------------------------------------------------

-- TODO: потом переделать чутка для вариантов до/после
-- Версионный разветвитель
local function getVersionMthd(in_versionVariant)
   -- NOTE: образец in_versionVariant
   -- [GGD.TocVersionList.Vanilla] = function() do staff end,  -- works only for Vanilla
   -- [{GGD.TocVersionList.Burning, GGD.TocVersionList.Panda}] = function() do staff end, -- works for all patches between Burning and Panda inclusive
   local versionOrder = {
      [GGD.TocVersionList.Vanilla] = {ub = GGD.TocVersionList.Burning},
      [GGD.TocVersionList.Burning] = {ub = GGD.TocVersionList.WotLK},
      [GGD.TocVersionList.WotLK] = {ub = GGD.TocVersionList.Cata},
      [GGD.TocVersionList.Cata] = {ub = GGD.TocVersionList.Panda},
      [GGD.TocVersionList.Panda] = {ub = GGD.TocVersionList.Draenor},
      [GGD.TocVersionList.Draenor] = {ub = GGD.TocVersionList.Legion},
      [GGD.TocVersionList.Legion] = {ub = GGD.TocVersionList.BFA},
      [GGD.TocVersionList.BFA] = {ub = GGD.TocVersionList.Shadowlands},
      [GGD.TocVersionList.Shadowlands] = {ub = GGD.TocVersionList.Invalid},
      [GGD.TocVersionList.Invalid] = {ub = GGD.TocVersionList.Invalid}, -- фиктивный элемент
   };
   local coverageList = {};
   local mthd = nil;
   for startVer,metaInf in pairs(versionOrder) do
      if (GGD.BuildMeta.TocVersion >= startVer and GGD.BuildMeta.TocVersion < metaInf.ub) then
         for idx,data in pairs(in_versionVariant) do
            local versionInterval = data.versionInterval;
            local func = data.action;
            if (type(versionInterval) == "table") then
               if (startVer >= versionInterval[1] and startVer <= versionInterval[2]) then
                  ATLASSERT(mthd == nil);
                  mthd = func;
               end
            else
               if (startVer == versionInterval) then
                  ATLASSERT(mthd == nil);
                  mthd = func;
               end
            end
         end
      end
   end
   return (mthd or in_versionVariant.Default or function() end);
end

function GGF.VersionSplitterProc(in_versionVariant)
   mthd = getVersionMthd(in_versionVariant);
   mthd();
end


function GGF.VersionSplitterFunc(in_versionVariant)
   mthd = getVersionMthd(in_versionVariant);
   return mthd();
end


-- Функция генерации токенизированных имен для объектов(в основном уёвых)(Object Tokenized Name)
function GGF.GenerateOTName(...)
	local name = "";
	for ind, shard in ipairs{...} do
		name = name .. shard .. "_";
	end
	-- NOTE: убираем последний символ
	name = name:sub(1, -2);
	return name;
end


-- Safe проверка наличия элемента по некоторому пути
function GGF.SplitCallCheck(in_object, in_path)
	local extobj = in_object;

	for v in string.gmatch(in_path, "%w+") do
		if (type(extobj) == 'function') then
			extobj = extobj()[v];
		else
			extobj = extobj[v];
		end
		if (extobj == nil) then
			return false;
		end
	end

	return true;
end


-- Safe взятие элемента сложного объекта/структуры
function GGF.SplitCallGet(in_object, in_path)
	local extobj = in_object;
	ATLASSERT(in_object);
	for v in string.gmatch(in_path, "%w+") do
		if (type(extobj) == 'function') then
			print("scg token:", v, in_path);
			extobj = extobj()[v];
		else
			extobj = extobj[v];
		end
		if (extobj == nil) then
			return nil;
		end
	end

	return extobj;
end


-- TODO: чекнуть работоспособность сета(GG-52, но не факт)
-- Safe установка значения элемента сложного объекта/структуры
function GGF.SplitCallSet(in_object, in_path, in_value)
	local extobj = in_object;
	-- NOTE: нужен, чтобы обращение к объекту при замене выглядело так: Object[elem] = newelem;
	local prevobj = in_object;
	local lasttoken = "";
	for v in string.gmatch(in_path, "%w+") do
		if (extobj == nil) then
			-- TODO: трассировка ошибки
			return;
		end
		prevobj = extobj;
		extobj = extobj[v];
		lasttoken = v;
	end

	prevobj[lasttoken] = in_value;
	-- TODO: отладить ctype, ptype
	--if (in_object.token:find("Template")) then
		--print("scs", in_path, in_value);
	--end
end


-- NOTE: Сброс лишних пробелов, т.е. в принципе всех
function GGF.TrimSpace(in_string)
	local nwst = string.gsub(in_string, " ", "");
	return nwst;
end


-- Внутренний сокращенный вызов метода через суффикс
function GGF.InnerInvoke(in_meta, in_method, ...)
	return GGF.GetObjectBySuffix(GGF.INS.GetObjectSuffix(in_meta.instance))[in_method](in_meta.class, {
		instance = in_meta.instance,
		class = in_meta.class,
		object = in_meta.object,
	}, ...);
end


-- Внешний сокращенный вызов метода через суффикс
function GGF.OuterInvoke(in_instance, in_method, ...)
	return GGF.InnerInvoke({
		instance = in_instance,
		class = GGF.GetObjectBySuffix(GGF.INS.GetObjectSuffix(in_instance)),
		object = GGF.INS.GetObjectRef(in_instance),
	}, in_method, ...);
end


-- Функция запуска метода с проверкой на нуллптр
function GGF.NotNullInvoke(in_meta, in_method, ...)
	if (GGF.GetObjectBySuffix(GGF.INS.GetObjectSuffix(in_meta.instance))[in_method]) then
		return GGF.InnerInvoke(in_meta, in_method, ...);
	end
	--print("no method:", in_meta.instance, in_meta.class, in_meta.object, in_method);
	--ATLASSERT(false);
	return nil;
end

--
function GGF.PrintMetaData(in_instance)
   class = GGF.GetObjectBySuffix(GGF.INS.GetObjectSuffix(in_instance));
   object = GGF.INS.GetObjectRef(in_instance);
   print("in_meta:", in_instance, class, object);
end