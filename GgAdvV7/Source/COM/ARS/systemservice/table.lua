----------------------------------------------------------------------------
-- "functional" block
----------------------------------------------------------------------------


local function isBlizzardObject(in_obj)
	return in_obj.GetName ~= nil;
end
-- TODO: разрулить путаницу в функциях....(GG-52)

-- Рекурсивное копирование таблицы(на всех уровнях)(возможно сломано...я не помню)
function GGF.TableCopy(in_orig)
	local orig_type = type(in_orig);
	local copy;
	if ((orig_type == 'table') and (not isBlizzardObject(in_orig))) then
		copy = {};
		--for orig_key, orig_value in next, in_orig, nil do
		for orig_key, orig_value in pairs(in_orig) do
			copy[GGF.TableCopy(orig_key)] = GGF.TableCopy(orig_value);
		end
		setmetatable(copy, getmetatable(in_orig));
	else -- number, string, boolean, etc
		copy = in_orig;
	end
	return copy;
end


-- TODO: как-нибудь чекнуть на возможные утчеки памяти после return(GG-52)
-- TODO: сделать рекурсивным(только первый уровень? слабовато...)
-- Стандартный гитовый форс мерж
function GGF.TableMerge(in_master, in_branch)
	local master_type = type(in_master);
	local copy;-- = GGF.TableCopy(in_master);
	if master_type == 'table' then
		copy = {};
		--for orig_key, orig_value in next, in_branch, nil do
		for orig_key, orig_value in pairs(in_branch) do
			copy[GGF.TableCopy(orig_key)] = GGF.TableCopy(orig_value);
		end

		-- TODO: а вот тут проверить(переопредеделение не сольет ли переменные класса..)(GG-52)
		setmetatable(copy, getmetatable(in_branch));
	else -- number, string, boolean, etc
		copy = in_branch;
	end

	-- т.к. при копировании переопределяется мета, мерж вынесен сюда
	-- уже существующие элементы не мержим
	for master_key, master_value in pairs(in_master) do
		if (copy[GGF.TableCopy(master_key)] == nil) then
			copy[GGF.TableCopy(master_key)] = GGF.TableCopy(master_value);
		end
	end
	return copy;
end


--
function GGF.TableSafeMerge(in_master, in_branch)
	local master_type = type(in_master)
	local branch_type = type(in_branch);
	local copy = GGF.TableCopy(in_master);
	if branch_type == 'table' and master_type == 'table' then
		for orig_key, orig_value in pairs(in_branch) do
			if (not copy[orig_key]) then
				if (type(orig_value) == 'table') then
					copy[orig_key] = {};
				else
					copy[orig_key] = nil;
				end
			end
			copy[orig_key] = GGF.TableSafeMerge(copy[orig_key], GGF.TableCopy(orig_value));
		end
	end

	if (copy == nil) then
		copy = GGF.TableCopy(in_branch);
	end

	return copy;
end

function GGF.ClassInheritanceMerge(in_master, in_branch)
   local master_type = type(in_master)
   local branch_type = type(in_branch);
   local copy = GGF.TableCopy(in_master);
   -- HACK: дропаемся, если замечен конечный элемент
   if (copy ~= nil) and (copy.cntchecker ~= nil) and (copy.cntchecker == true) then
      return copy;
   end
   if branch_type == 'table' and master_type == 'table' then
      for orig_key, orig_value in pairs(in_branch) do
         if (not copy[orig_key]) then
            if (type(orig_value) == 'table') then
               copy[orig_key] = {};
            else
               copy[orig_key] = nil;
            end
         end
         copy[orig_key] = GGF.ClassInheritanceMerge(copy[orig_key], GGF.TableCopy(orig_value));
      end
   end

   if (copy == nil) then
      copy = GGF.TableCopy(in_branch);
   end

   return copy;
end


-- Service driven extend
function GGF.TableExtend(in_base)
	return GGF.TableCopy(in_base);
	-- Здесь по умолчанию вызов не через глобал, т.к. иначе дедлок
	--COM_CS:comdextend(in_base);
end


-- Service driven safe extend
function GGF.TableSafeExtend(in_master, in_branch)
	return GGF.TableSafeMerge(in_master, in_branch);
end


function GGF.ClassInheritance(in_master, in_branch)
   in_master = GGF.ClassInheritanceMerge(in_master, in_branch);
   return in_master;
end


-- Дамп данных таблицы в принт
function GGF.TableDump(in_tbl, in_path)
	local path = in_path or "";
	local type = type(in_tbl);
	if ((type == 'table') and (not isBlizzardObject(in_tbl))) then
		--for orig_key, orig_value in next, in_orig, nil do
		local empty = true;
		for key, value in pairs(in_tbl) do
			empty = false;
			GGF.TableDump(value, path .. "." .. key);
		end
		if (empty) then
			print(in_path, "empty table");
		end
	else -- number, string, boolean, etc
		print(in_path, in_tbl);
	end
end