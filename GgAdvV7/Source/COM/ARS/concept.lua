-- COM грузится первый...всегда

-- Система MPRC(Module Powertrain Runtime Control)
-- Общая структура любого фукнционального модуля
-- rgcontrol(register control) block
-- Этот блок вынесен в общий класс(см. me_bm_GGC.BaseModule)
-- 1.0 MPRC_SLG_INITCTRL()		-- (Special Logic) Инициализация внутренней логики модуля
	-- 1.0.0 checkModuleShardExistence()	-- (Existence Check) Проверка наличия объявленных частей(файлов) модуля
		-- 1.0.0.0 MS_CHECK_API()			-- (API Check) Проверка наличия файла *_api.lua
		-- 1.0.0.1 MS_CHECK_FNC()			-- (FNC Check) Проверка наличия файла *_functional.lua
		-- 1.0.0.2 MS_CHECK_INT()			-- (INT Check) Проверка наличия файла *_intercomm.lua
		-- 1.0.0.3 MS_CHECK_MDT_[№]()		-- (MDT Check) Проверка наличия файла *_mdata0.lua
		-- 1.0.0.4 MS_CHECK_OBC()			-- (OBC Check) Проверка наличия файла *_objconfig.lua
-- 1.1 MPRC_SVR_INITCTRL()		-- (System Variables) Контроллер SVR
-- 1.2 MPRC_MVR_INITCTRL()		-- (Miscellaneous Variables) Контроллер MVR
-- 1.3 MPRC_MCP_INITCTRL()		-- (Main Config Parameters) Контроллер MCP
-- 1.4 MPRC_OBJ_INITCTRL()		-- (Objects) Контроллер OBJ
-- 1.5 MPRC_ACP_INITCTRL()		-- (Auxiliary Config Parameters) Контроллер ACP
-- 1.6 MPRC_AEC_INITCTRL()		-- (Auxiliary Event Callbacks) Контроллер AEC
-- 1.7 MPRC_MEC_INITCTRL()		-- (Main Event Callbacks) Контроллер MEC
-- 1.8 MPRC_ADJ_INITCTRL()		-- (Object Adjust) Контроллер ADJ
-- 1.9 MPRC_ALS_INITCTRL()		-- (After-load Settings) Контроллер ALS
-- 1.10 MPRC_AST_RNTMCTRL()		-- (Addon System Timer) Контроллер AST
-- 1.11 MPRC_ACP_TERMCTRL()		-- (All Config Parameters) Контроллер ACP
-- 1.12 MPRC_CDM_TERMCTRL()		-- (Coup De Module) Контроллер CDM


-- Блоки 2,3,4,5 вынесены в отедельные файлы(_intercomm)
-- initialize block
-- 2.0 MPRC_SVR_INITIALIZE()	-- (System Variables) Игровые(берутся из клиента ч-з близз апи)
-- 2.1 MPRC_MVR_INITIALIZE()	-- (Miscellaneous Variables) Общие переменные(эйс и т.п.)
-- 2.2 MPRC_MCP_INITIALIZE()	-- (Main Config Parameters) Основные сохраненные во вне параметры
-- 2.3 MPRC_OBJ_INITIALIZE()	-- (Objects) Объекты фреймворка для модуля
-- 2.4 MPRC_ACP_INITIALIZE()	-- (Auxiliary Config Parameters) Вспомогательные сохраненные во вне параметры(объектные)
-- 2.5 MPRC_AEC_INITIALIZE()	-- (Auxiliary Event Callbacks) Вспомогательные эвенты(объектные)
-- 2.6 MPRC_MEC_INITIALIZE()	-- (Main Event Callbacks) Основные эвенты
-- 2.7 MPRC_ADJ_INITIALIZE()	-- (Object Adjust) Применение свойств объектов
-- 2.8 MPRC_ALS_INITIALIZE()	-- (After-load Settings) Загрузка вдогонку

-- timer block
-- 3.0 MPRC_AST_RUNTIME()		-- (Addon System Timer)

-- finalize block
-- 4.0 MPRC_ACP_TERMINATE()		-- (All Config Parameters) Сохранение всех параметров из вне
-- 4.1 MPRC_CDM_TERMINATE()		-- (Coup De Module) Остаточные финишные операции

-- unit-test block
-- 5.0 MPRC_STP_UNITTEST()		-- (Scenario Teardown Prepare) Загрузка юнит-тестов в специальный контейнер

-- на стадии разработки(отключен/не реализован)
-- в теории должен быть реализован где-то в базовом классе
-- diagnostic block
-- 5.0 interrogatorCheck 	-- опрос модулей для проверки их наличия и работоспособности
-- 5.1 troubleCodeListTransfer 	-- передача листа накопившихся ошибок на сторону(т.е. в один из модулей GUi)(все чеки отсюда получаются)
-- 5.1 unitTestInvoke

-- Общая структура класса
-- rgcontrol block		-- внешний контроль модуля(завернут в базовый класс)
-- initialize block		-- инициализация(вынесена в intercomm)
-- timer block			-- таймер(вынесен в intercomm)
-- finalize block		-- финишер(вынесен в intercomm)
-- setter block			-- сеттеры для объектов/параметров класса 
-- getter block			-- геттеры для объектов/параметров класса
-- runtime block		-- функции времени выполнения
-- callback block		-- фукнции обратного вызова
-- Service block		-- внешние сервисы(завязаны на класс только по смыслу)


-- Общая структура любого ООП модуля
-- declaration block	-- объявления(в т.ч. дефолты)
-- initialize block		-- инициализация на любой стадии
-- modify block			-- изменение параметра без применения к объекту
-- setter block			-- стандартные сеттеры(без параметра работает как апдейтер)
-- getter block			-- стандартные геттеры
-- adjuster block		-- грубо говоря, применители параметров
-- Service block		-- вынесенные вспомогательные операции(вычисления и т.п.)
-- export block			-- экспорт на глобальный уровень для всех аддонов серии GG


-- Блок ошибок
-- Типы ошибок
-- Сбрасывыющиеся после исправления(загрузка модулей)
-- Сбрасывающиеся после интервала(некорреткный агрумент ком. строки)
-- Несбрасывающиеся некритические(корреткная обработка внезапных событий)
-- Несбрасывающиеся критические(ассерты в пределах модулей)

-- Немного про функции
-- Общий стиль коллбеков
-- on_[<имя объекта>]_[<эвент>]
-- on_mainFrame_click