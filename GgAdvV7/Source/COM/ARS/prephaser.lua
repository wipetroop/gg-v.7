----------------------------------------------------------------------------
-- "functional" block
----------------------------------------------------------------------------


-- Стандартный ассерт
function ATLASSERT(in_x)
	assert(in_x,tostring(in_x));
end


----------------------------------------------------------------------------
-- "define" block
----------------------------------------------------------------------------

GGM.COM = {};
GGD.COM = {};
GGD.COM.ClusterName = "Component Object Model";
GGD.COM.ClusterToken = "COM";
GGD.COM.ClusterVersion = GGD.BuildMeta.Version;
GGD.COM.ClusterBuild = "AT-"..GGD.BuildMeta.Build;
GGD.COM.ClusterMBI = "8022-1012";

GGD.COM.TexturePath = "Interface\\AddOns\\" .. GGD.AddonName .. "\\Textures\\COM\\";

GGD.EvokeCascadeEndList = "89e9fa96f55d7d61bd31c7af2ccb23d4";	-- NOTE: захешированное название константы