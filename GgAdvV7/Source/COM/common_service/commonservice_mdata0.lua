local This = GGF.ModuleGetWrapper("COM-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.3 (MDT Check)
function This:MS_CHECK_MDT_0()
	return 1;
end


-------------------------------------------------------------------------------
-- "multitudinous data" block
-------------------------------------------------------------------------------


-- NOTE: залинковано на глобальный Color
This.md_colorList = {
	-- стандартные
	Red =         { R = 1.00, G = 0.00, B = 0.00, A = 1.00 },
	Green =       { R = 0.00, G = 1.00, B = 0.00, A = 1.00 },
	Yellow =      { R = 1.00, G = 1.00, B = 0.00, A = 1.00 },
	Blue =        { R = 0.00, G = 0.00, B = 1.00, A = 1.00 },
	Black =       { R = 0.00, G = 0.00, B = 0.00, A = 1.00 },
	Grey =        { R = 0.50, G = 0.50, B = 0.50, A = 1.00 },

	-- кастомные
	Title =       { R = 0.46, G = 0.86, B = 0.90, A = 1.00 },
	Neutral =     { R = 1.00, G = 0.80, B = 1.00, A = 1.00 },
	Normal =      { R = 0.50, G = 1.00, B = 0.50, A = 1.00 },
	Warning =     { R = 1.00, G = 0.86, B = 0.20, A = 1.00 },
	Critical =    { R = 1.00, G = 0.12, B = 0.23, A = 1.00 },

	-- классовые(игровая семантика)
	Warrior =     { R = 0.78, G = 0.61, B = 0.43, A = 1.00 },
	Paladin =     { R = 0.96, G = 0.55, B = 0.73, A = 1.00 },
	Hunter =      { R = 0.50, G = 1.00, B = 0.50, A = 1.00 },
	Rogue =       { R = 1.00, G = 0.96, B = 0.41, A = 1.00 },
	Priest =      { R = 1.00, G = 1.00, B = 1.00, A = 1.00 },
	DeathKnight = { R = 0.77, G = 0.12, B = 0.23, A = 1.00 },
	Shaman =      { R = 0.00, G = 0.44, B = 0.87, A = 1.00 },
	Mage =        { R = 0.41, G = 0.80, B = 0.94, A = 1.00 },
	Warlock =     { R = 0.58, G = 0.51, B = 0.79, A = 1.00 },
	Monk =        { R = 1.00, G = 1.00, B = 0.59, A = 1.00 },
	DrUid =       { R = 1.00, G = 0.49, B = 0.04, A = 1.00 },
	DemonHunter = { R = 0.64, G = 0.19, B = 0.79, A = 1.00 },
	Unclassed =   { R = 0.70, G = 0.70, B = 0.70, A = 1.00 },-- бывший Default

	-- check button цвета
	CBEnabled =   { R = 1.00, G = 0.82, B = 0.00, A = 1.00 },
	CBDisabled =  { R = 0.50, G = 0.50, B = 0.50, A = 1.00 },
};

-- TODO: добавить цвет
-- NOTE: залинковано на глобальный Backdrop
This.md_backdrop = {
	Default = {
		bgFile = nil,
		edgeFile = nil,
		tile = nil,
		edgeSize = nil,
		tileSize = nil,
		insets = {
			left = nil,
			right = nil,
			top = nil,
			bottom = nil,
		},
		color = {
			R = nil,
			G = nil,
			B = nil,
			A = nil,
		},
		borderColor = {
			R = nil,
			G = nil,
			B = nil,
			A = nil,
		},
	},
	Borderless = {
		bgFile = nil,
		edgeFile = "",
		edgeSize = 16,
		tileSize = 16,
		insets = {
			left = 3,
			right = 3,
			top = 3,
			bottom = 3,
		},
		color = {
			R = nil,
			G = nil,
			B = nil,
			A = nil,
		},
		borderColor = {
			R = nil,
			G = nil,
			B = nil,
			A = nil,
		},
	},
	Nested = {
		bgFile = "",
		edgeFile = "Interface\\Tooltips\\Ui-Tooltip-Border",
		edgeSize = 16,
		tileSize = 16,
		insets = {
			left = 3,
			right = 3,
			top = 3,
			bottom = 3,
		},
		color = {
			R = nil,
			G = nil,
			B = nil,
			A = nil,
		},
		borderColor = {
			R = nil,
			G = nil,
			B = nil,
			A = nil,
		},
	},
	Segregate = {
		-- NOTE: тут то же самое, что и в конфиге, только фон по умолчанию ставится
		bgFile = nil,
		edgeFile = "Interface\\Tooltips\\Ui-Tooltip-Border",
		edgeSize = 16,
		tileSize = 16,
		insets = {
			left = 4,
			right = 4,
			top = 4,
			bottom = 4,
		},
		color = {
			R = nil,
			G = nil,
			B = nil,
			A = nil,
		},
		borderColor = {
			R = nil,
			G = nil,
			B = nil,
			A = nil,
		},
	},
	Empty = {
		bgFile = "",
		edgeFile = "",
		edgeSize = 16,
		tileSize = 16,
		insets = {
			left = 3,
			right = 3,
			top = 3,
			bottom = 3,
		},
		color = {
			R = nil,
			G = nil,
			B = nil,
			A = nil,
		},
		borderColor = {
			R = nil,
			G = nil,
			B = nil,
			A = nil,
		},
	},
	VerticalSlider = {
		bgFile = "Interface\\Buttons\\Ui-SliderBar-Background",
		edgeFile = "Interface\\Buttons\\Ui-SliderBar-Border",
		edgeSize = 8,
		tileSize = 8,
		insets = {
			left = 3,
			right = 3,
			top = 3,
			bottom = 3,
		},
		color = {
			R = nil,
			G = nil,
			B = nil,
			A = nil,
		},
		borderColor = {
			R = nil,
			G = nil,
			B = nil,
			A = nil,
		},
	},
	EvolutionSlider = {
		bgFile = "Interface\\Buttons\\Ui-SliderBar-Background",
		edgeFile = "Interface\\Buttons\\Ui-SliderBar-Border",
		tile = true,
		edgeSize = 8,
		tileSize = 8,
		insets = {
			left = 3,
			right = 3,
			top = 6,
			bottom = 6,
		},
		color = {
			R = nil,
			G = nil,
			B = nil,
			A = nil,
		},
		borderColor = {
			R = nil,
			G = nil,
			B = nil,
			A = nil,
		},
	},
	EditBox = {
		bgFile = "Interface\\ChatFrame\\ChatFrameBackground",
		edgeFile = "Interface\\ChatFrame\\ChatFrameBackground",
		tile = true,
		edgeSize = 1,
		tileSize = 5,
		color = {
			R = 0,
			G = 0,
			B = 0,
			A = 0.5,
		},
		borderColor = {
			R = 0.3,
			G = 0.3,
			B = 0.3,
			A = 0.8,
		},
	},
};