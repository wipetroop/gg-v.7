local This = GGF.ModuleGetWrapper("COM-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- api(GGF.TernExpComplex)
-- Реализация тернарного оператора одним вызовом(с результатом условия)
-- Ternary Expression Complex
function This:ternExpComplex(in_cond, in_tval, in_fval)
	if (in_cond) then
		return in_cond, in_tval;
	end
	return in_cond, in_fval;
end


-- api(GGF.TernExpSingle)
-- Реализация тернарного оператора одним вызовом(простая)
-- Ternary Expression Simple
function This:ternExpSingle(in_cond, in_tval, in_fval)
	-- WARNING: костыль
	return select(2, self:ternExpComplex(in_cond, in_tval, in_fval));
end


-- api
-- Слияние конфигов(git-like)
function This:mergeConfig(in_master, in_branch)
	if type(in_master) == 'table' and type(in_branch) == 'table' then
		for k,v in pairs(in_branch) do
			if ((type(v) == 'table') and (type(in_master[k] or false) == 'table')) then
				merge(in_master[k],v);
			else
				in_master[k] = v;
			end
		end
	end

	return in_master;
end


-- api(GGF.SetTableDefaultValue)
-- Установка дефолтного значения результата в случае обращения к несуществуещему элементу
function This:setTableDefaultValue(in_table, in_value)
	local mt = {__index = function () return in_value end}
	setmetatable(in_table, mt);
end


-- api(GGF.SetTooltip)
-- Установка тултипа на поле для игрового окна тултипа
function This:setTooltip(in_object, in_text)
	in_object.tooltip_text = in_text;
end


-- api(GGF.StructColorToStringColor)
-- Установка начала цветовой схемы текста
function This:structColorToStringColor(in_struct)
	local hexLimit = 255;
	return "|c"		-- NOTE: тут использован шаблон %02X, что значит представить число ввиде двукх знаков, недостающее забить нулями
		.. string.format("%02X", in_struct.A*hexLimit)
		.. string.format("%02X", in_struct.R*hexLimit)
		.. string.format("%02X", in_struct.G*hexLimit)
		.. string.format("%02X", in_struct.B*hexLimit);
end


-- api(GGF.FlushStringColor)
-- Установка конца цветовой схемы текста
function This:flushStringColor()
	return "|r";
end


-- api(GGF.SwitchCase)
-- Укороченный вызов стандартного свича
function This:switchCase(in_case, ...)
	local args = {...};

	for i=1,#args do
		if (in_case == args[i][1]) then
			return args[i][2];
		end
	end
	return nil;
end


-- api(GGF.GetTableSize)
-- Подсчет реального размера таблицы в элементах
function This:getTableSize(in_table)
	local count = 0;
	for _ in pairs(in_table) do
		count = count + 1;
	end
	return count;
end


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------