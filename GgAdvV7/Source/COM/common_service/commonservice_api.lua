local This = GGF.ModuleGetWrapper("COM-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Реализация условного оператора одним вызовом(с результатом условия)
function This:TernExpComplex(in_cond, in_tval, in_fval)
	return self:ternExpComplex(in_cond, in_tval, in_fval);
end


-- short call
function GGF.TernExpComplex(in_cond, in_tval, in_fval)
	return This:TernExpComplex(in_cond, in_tval, in_fval);
end


-- Реализация условного оператора одним вызовом(простая)
function This:TernExpSingle(in_cond, in_tval, in_fval)
	return self:ternExpSingle(in_cond, in_tval, in_fval);
end


-- short call
function GGF.TernExpSingle(in_cond, in_tval, in_fval)
	return This:TernExpSingle(in_cond, in_tval, in_fval);
end


-- Слияние конфигов(git-like)(резерв)
function This:MergeConfig(in_master, in_branch)
	return self:mergeConfig(in_master, in_branch);
end


-- Установка дефолтного значения результата в случае обращения к несуществуещему элементу
function This:SetTableDefaultValue(in_table, in_value)
	self:setTableDefaultValue(in_table, in_value);
end


-- short call
function GGF.SetTableDefaultValue(in_table, in_value)
	This:setTableDefaultValue(in_table, in_value);
end


-- Установка тултипа на поле для игрового окна тултипа
function This:SetTooltip(in_object, in_text)
	self:setTooltip(in_object, in_text);
end


-- short call
function GGF.SetTooltip(in_object, in_text)
	This:setTooltip(in_object, in_text);
end


-- Установка начала цветовой схемы текста
function This:StructColorToStringColor(in_struct)
	return self:structColorToStringColor(in_struct);
end


-- short call
function GGF.StructColorToStringColor(in_struct)
	return This:structColorToStringColor(in_struct);
end


-- Установка конца цветовой схемы текста
function This:FlushStringColor()
	return self:flushStringColor();
end


-- short call
function GGF.FlushStringColor()
	return This:flushStringColor();
end


-- Укороченный вызов стандартного свича
function This:SwitchCase(in_case, ...)
	return self:switchCase(in_case, ...);
end


-- short call
function GGF.SwitchCase(in_case, ...)
	return This:switchCase(in_case, ...);
end


-- Подсчет реального размера таблицы в элементах
function This:GetTableSize(in_table)
	return self:getTableSize(in_table);
end


-- short call
function GGF.GetTableSize(in_table)
	return This:getTableSize(in_table);
end


GGD.Color = This.md_colorList;

GGD.Backdrop = This.md_backdrop;


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------