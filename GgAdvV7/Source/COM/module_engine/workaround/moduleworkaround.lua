-- NOTE: namespace
GGF.MDL = {};


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Установка имени
local function setInstanceModuleNameStandard(in_instance, in_name)
	in_instance.MPRC.maintenance.modulename.standard = in_name;
end


-- Установка имени объекта(трим от полного имени экземпляра, исп. для вызова)
local function setInstanceModuleNameObject(in_instance, in_name)
	in_instance.MPRC.maintenance.modulename.object = in_name;
end


-- Установка каскада дочерних объектов
local function setInstanceCascadeObj(in_instance, in_cascadeLink)
	in_instance.MPRC.maintenance.cascade_obj = in_cascadeLink;
end


-- Установка токена модуля
local function setInstanceGToken(in_instance, in_gToken)
	in_instance.MPRC.maintenance.modulegtoken = in_gToken;
end


-- Установка роли
local function setInstanceModuleRole(in_instance, in_moduleRole)
	in_instance.MPRC.maintenance.modulerole = in_moduleRole;
end


-- Установка токена кластера
local function setInstanceClusterToken(in_instance, in_clusterToken)
	in_instance.MPRC.maintenance.clustertoken = in_clusterToken;
end


-- Установка флага востребованности таймера
local function setInstanceTimerFlag(in_instance, in_timer)
	in_instance.MPRC.maintenance.timer = in_timer;
end


-- Установка флага стадии инициализации
local function setInstanceSIPStageState(in_instance, in_stage, in_state)
	ATLASSERT(in_instance.MPRC.maintenance.stateInitProtocol[in_stage] ~= nil);
	in_instance.MPRC.maintenance.stateInitProtocol[in_stage] = in_state;
end


-- Установка флага стадии финализации
local function setInstanceSTPStageState(in_instance, in_stage, in_state)
	ATLASSERT(in_instance.MPRC.maintenance.stateTerminateProtocol[in_stage] ~= nil);
	in_instance.MPRC.maintenance.stateTerminateProtocol[in_stage] = in_state;
end


-- Установка внешнего конфига
local function setInstanceExternalConfig(in_instance, in_config)
	in_instance.MPRC.powertrain.externalconfig = in_config;
end


-- Установка внутреннего конфига
local function setInstanceConfig(in_instance, in_config)
	in_instance.MPRC.powertrain.internalconfig = in_config;
end


-- Установка таймера
local function setInstanceTimer(in_instance, in_timer)
	in_instance.MPRC.powertrain.timer = in_timer;
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие имени
local function getInstanceModuleNameStandard(in_instance)
	return in_instance.MPRC.maintenance.modulename.standard;
end


-- Взятие имени объекта(трим от полного имени экземпляра, исп. для вызова)
local function getInstanceModuleNameObject(in_instance)
	return in_instance.MPRC.maintenance.modulename.object;
end


-- Взятие каскада дочерних объектов из экземпляра
local function getInstanceCascadeObj(in_instance)
	return in_instance.MPRC.maintenance.cascade_obj;
end


-- Взятие токена экземпляра модуля
local function getInstanceGToken(in_instance)
	return in_instance.MPRC.maintenance.modulegtoken;
end


-- Взятие токена кластера
local function getInstanceClusterToken(in_instance)
	return in_instance.MPRC.maintenance.clustertoken;
end


-- Взятие флага востребованности таймера
local function getInstanceTimerFlag(in_instance)
	return in_instance.MPRC.maintenance.timer;
end


-- Взятие токена экземпляра модуля
local function getInstanceSIPStageState(in_instance, in_stage)
	ATLASSERT(in_instance.MPRC.maintenance.stateInitProtocol[in_stage] ~= nil);
	return in_instance.MPRC.maintenance.stateInitProtocol[in_stage];
end


-- Взятие токена экземпляра модуля
local function getInstanceSTPStageState(in_instance, in_stage)
	ATLASSERT(in_instance.MPRC.maintenance.stateTerminateProtocol[in_stage] ~= nil);
	return in_instance.MPRC.maintenance.stateTerminateProtocol[in_stage];
end


-- Взятие внешнего конфига
local function getInstanceExternalConfig(in_instance)
	return in_instance.MPRC.powertrain.externalconfig;
end


-- Взятие внутреннего конфига
function GGF.MDL.GetInstanceConfig(in_instance)
	return in_instance.MPRC.powertrain.internalconfig;
end


-- Взятие таймера
local function getInstanceTimer(in_instance)
	return in_instance.MPRC.powertrain.timer;
end


-- Взятие каскада дочерних объектов из глобального списка
local function getCascadeObj(in_instance)
	return GGF.GetGlobalModuleCascadeObj(getInstanceGToken(in_instance)) or {};
end


-- Взятие остова объекта для проверки экземпляра
local function getChassis(in_instance)
	return GGF.GetGlobalModuleChassis(getInstanceGToken(in_instance)) or {};
end


-- Safe взятие элемента сложного объекта/структуры
-- NOTE: дублировано из CS, т.к. иначе дедлок
local function splitCallGet(in_object, in_path)
	local extobj = in_object;

	for v in string.gmatch(in_path, "%w+") do
		extobj = extobj[v];
		if (not extobj) then
			return nil;
		end
	end

	return extobj;
end


-- Взятие параметра из аргументов загрузки
local function getArglineParam(in_argline, in_path)
	return splitCallGet(in_argline, in_path);
end


-- Инициализатор базового класса
function GGF.MDL.InitializeModule(in_instance, in_argline)
	setInstanceModuleNameStandard(in_instance, getArglineParam(in_argline, "modulename.standard"));
	setInstanceModuleNameObject(in_instance, getArglineParam(in_argline, "modulename.object"));
	setInstanceGToken(in_instance, getArglineParam(in_argline, "modulegtoken"));
	setInstanceModuleRole(in_instance, getArglineParam(in_argline, "modulerole") or ATLASSERT(false));
	setInstanceClusterToken(in_instance, getArglineParam(in_argline, "clustertoken"));
	setInstanceTimerFlag(in_instance, getArglineParam(in_argline, "timer"));
	-- TODO: приколхозить крит при неправильной(отсутствующей роли)(GG-29)
	-- NOTE: возможно, все чеки стоит вынести в функцию с вызовом из спец логики

	-- NOTE: не перегруженный список идет по умолчанию
	-- WARNING: костыль
	--self.MPRC.powertrain.chassis = GGF.TableMerge(self.MPRC.powertrain.chassis, self:getArglineParam(in_argline, "chassis") or self.MPRC.powertrain.chassis);
	setInstanceConfig(in_instance, getArglineParam(in_argline, "config") or {});

	-- TODO: захачить, чтоли...
	-- NOTE: временно отключен из-за жора памяти и цп
	--self.fr_runtimer = CreateFrame("FRAME");
	--self.fr_runtimer:SetScript("OnUpDate", function(in_self, in_elapsed) self:MPRC_ACT_RUNTIME(in_elapsed) end);
end


-- Инициализация отдельной стадии
local function initStage(in_instance, in_stage, ...)
	if (in_instance[in_stage]) then
		return not in_instance[in_stage](in_instance, ...);
	else
		return GGF.MDL[in_stage](in_instance, ...);
	end
end


-- Обертка контроллера запуска(триггер)
local function initControllerTrigger(in_instance, in_stage)
	initStage(in_instance, "MPRC_"..in_stage.."_INITIALIZE");
	local mdList = getInstanceCascadeObj(in_instance);
	for idx,md in ipairs(mdList) do
		if (not initStage(md, "MPRC_"..in_stage.."_INITCTRL")) then
			return false;
		end
	end
	ATLASSERT(getInstanceSIPStageState(in_instance, in_stage) == false);
	setInstanceSIPStageState(in_instance, in_stage, true);
	return true;
end


-- Обертка контроллера выключения(триггер)
local function terminateControllerTrigger(in_instance, in_stage)
	local mdList = getInstanceCascadeObj(in_instance);
	for idx,md in ipairs(mdList) do
		if (initStage(md, "MPRC_"..in_stage.."_TERMCTRL")) then
			return false;
		end
	end
	initStage(in_instance, "MPRC_"..in_stage.."_TERMINATE");
	ATLASSERT(getInstanceSTPStageState(in_instance, in_stage) == false);
	setInstanceSTPStageState(in_instance, in_stage, true);
	return true;
end


-- Проверка пути на наличие элементов с объектами(запрещенных к обходу)
local function checkPath(in_path)
	local checkList = {
		"MPRC",
		".objects.",
		".properties.",
		".elements.",
		".wrappers.",
	};
	for k,v in ipairs(checkList) do
		if (in_path:find(v)) then
			return false;
		end
	end

	return true;
end


-- Проверка пути объекта на наличие собственно объекта
local function checkObjectPath(in_trg, in_path, in_checkedObjectName)
	if (not in_trg) then
		print("no ", in_checkedObjectName, ":", in_path);
		ATLASSERT(false);
	end
end


-- Рекурсивное создание карты джампера
local function createJumperMap(in_instance, in_object, in_path, in_parentToken)
	if (type(in_object) ~= 'table') then
		return;
	end

	for objtoken, object in pairs(in_object) do
		if (type(object) == 'table') then
			local nwPath = in_path .. "." .. objtoken;
			-- Выбрасываем системный раздел из обхода
			-- Чек на наличие в пути objconfig
			if (checkPath(nwPath) and nwPath:find("oc_") ~= nil) then
				local token = in_parentToken;
				if (object.template and object.object) then
					-- NOTE: нужен для создания джампера
					checkObjectPath(object.suffix, nwPath, "suffix");
					-- NOTE: временно(или нет) переключено на жесткие токены, т.к. есть проблема с цифрами в адресе
					--local token = object.template.properties.base.jrtoken;
					-- TODO: протестить!!! вот полюбасу отхлебнет жеж...
					if (in_parentToken ~= "") then
						token = token .. "_";
					end
					--self:checkObjectPath(object.template, nwPath, "template");
					checkObjectPath(object.template.properties, nwPath, "properties");
					checkObjectPath(object.template.properties.base, nwPath, "base");
					checkObjectPath(object.template.properties.base.wrapperName, nwPath, "wrapper name");
					checkObjectPath(object.object, nwPath, "object");
					token = token .. object.template.properties.base.wrapperName .. "_" .. object.suffix;
					--local token = self:generateJumperToken(nwPath);
					in_instance["JR_" .. token .. "_OBJ"] = object.object;
					in_instance["JR_" .. token .. "_TMP"] = object.template;
					--if (string.find(token, "MW_SF") ~= nil) then
						--print("token:", token, in_parentToken);
					--end
					object.template.meta = {
						jrtoken = token,
					};
				end
				createJumperMap(in_instance, object, nwPath, token);
			end
		end
	end
end


-- Приоритетное взятие сигнатуры ошибки
local function getErrorSignature(in_class, in_code)
	print("class:", in_class);
	return in_class.cs_customErrors[in_code] or in_class.cs_standardErrors[in_code];
end


-- Взятие токена по коду ошибки
local function getErrorTypesToken(in_code)
	return GGE.ErrorCodeToken[in_code];
end


-- TODO: чутка переработать систему(чтобы ошибки стакались как-нибудь)(GG-29)
-- Аналог through(инвокер ошибок)
local function stdError(in_instance, in_code, in_postDescription, in_blck)
	local errsgn = getErrorSignature(in_instance.__class, in_code);
	local type = "UND";
	local text = "Undefined";
	local blck = false;	-- блокер вывода(убрать повторы вывода ошибок)
	if (errsgn) then
		if (in_instance.currentErrors[in_code] == nil) then
			in_instance.currentErrors[in_code] = { time = errsgn.time };
			type = errsgn.type;
			text = errsgn.identifier;
		else
			blck = true;
		end
	end
	if (not blck and not in_blck) then
		GGM.IOC.OutputController:CommonPrint(in_instance,
			GGF.StructColorToStringColor(GGD.Color.Critical) .. "ERROR " .. 
			GGF.StructColorToStringColor(GGD.Color.Title) .. getErrorTypesToken(type) .. "-" .. in_code .. 
			GGF.FlushStringColor() .. ": " .. text, in_postDescription);
	end
end


-- Проверка наличия объявленных в data частей модуля(чек манифеста грубо говоря)
-- п.1.0.0 (Existence Check)
local function checkModuleShardExistence(in_instance)
	local chs = getChassis(in_instance);
	-- NOTE: резерв
	--if (chs.DT) then
	--end

	local api = GGF.TernExpSingle(chs.API, chs.API, 0);
	if (api ~= 0 and in_instance:MS_CHECK_API() == 0) or
		(api == 0 and in_instance:MS_CHECK_API() == 1) then
		stdError(in_instance, 1, "'api' shard check failure: "..GGF.TernExpSingle(api > 0, "absence", "overmuch"));
		return false;
	end

	local fnc = GGF.TernExpSingle(chs.FNC, chs.FNC, 0);
	if (fnc ~= 0 and in_instance:MS_CHECK_FNC() == 0) or
		(fnc == 0 and in_instance:MS_CHECK_FNC() == 1) then
		stdError(in_instance, 1, "'functional' shard check failure: "..GGF.TernExpSingle(fnc > 0, "absence", "overmuch"));
		return false;
	end

	local int = GGF.TernExpSingle(chs.INT, chs.INT, 0);
	if (int ~= 0 and in_instance:MS_CHECK_INT() == 0) or
		(int == 0 and in_instance:MS_CHECK_INT() == 1) then
		stdError(in_instance, 1, "'intercomm' shard check failure: "..GGF.TernExpSingle(int > 0, "absence", "overmuch"));
		return false;
	end

	local ind = 0;
	local summ = 0;
	while (in_instance["MS_CHECK_MDT_"..ind] ~= nil) do
		summ = summ + in_instance["MS_CHECK_MDT_"..ind](in_instance);
		ind = ind + 1;
	end
	local mdt = GGF.TernExpSingle(chs.MDT, chs.MDT, 0);
	if (mdt ~= summ) then
		stdError(in_instance, 1, "'mdata' shard check failure: "..GGF.TernExpSingle(mdt > summ, "absence", "overmuch"));
		return false;
	end

	local obc = GGF.TernExpSingle(chs.OBC, chs.OBC, 0);
	if (obc ~= 0 and in_instance:MS_CHECK_OBC() == 0) or
		(obc == 0 and in_instance:MS_CHECK_OBC() == 1) then
		stdError(in_instance, 1, "'objconfig' shard check failure: "..GGF.TernExpSingle(obc > 0, "absence", "overmuch"));
		return false;
	end

	return true;
end


-- п.1.0 (Special Logic)
function GGF.MDL.MPRC_SLG_INITCTRL(in_instance)
	-- NOTE: Для всех молулей
	if (not checkModuleShardExistence(in_instance)) then
		return false;
	end
	-- WARNING: НЕ ТРОГАТЬ ПУСТУЮ СТРОКУ!!! ВСЕ СЛОМАЕТСЯ!!!
	--print("suff:", GGF.INS.GetObjectSuffix(in_instance));
	createJumperMap(in_instance, in_instance, "", "");
	setInstanceCascadeObj(in_instance, getCascadeObj(in_instance));
	local mdList = getInstanceCascadeObj(in_instance);
	--local mdList = self.MPRC.powertrain.cascade_obj(self.MPRC.maintenance.modulegtoken);
	--print("md:", mdList, #mdList);
	for idx,md in ipairs(mdList) do
		--print("module slg:", module.MPRC_SLG_INITCTRL);
		--local module = GGF.ModuleGetWrapper(mdname);
		if (not md) then
			stdError(in_instance, 1, "module initctrl failure: undeclared module №"..tostring(idx).." in cascade");
			return false;
		end
		if (not initStage(md, "MPRC_SLG_INITCTRL")) then
			return false;
		end
	end
	ATLASSERT(getInstanceSIPStageState(in_instance, "SLG") == false);
	setInstanceSIPStageState(in_instance, "SLG", true);
	return true;
end


-- п.1.1 (System Variables)
function GGF.MDL.MPRC_SVR_INITCTRL(in_instance)
	return initControllerTrigger(in_instance, "SVR");
end


-- п.1.2 (Miscellaneous Variables)
function GGF.MDL.MPRC_MVR_INITCTRL(in_instance)
	return initControllerTrigger(in_instance, "MVR");
end


-- п.1.3 (Main Config Parameters)
function GGF.MDL.MPRC_MCP_INITCTRL(in_instance)
	return initControllerTrigger(in_instance, "MCP");
end


-- п.1.4 (Objects)
function GGF.MDL.MPRC_OBJ_INITCTRL(in_instance)
	return initControllerTrigger(in_instance, "OBJ");
end


-- п.1.5 (Auxiliary Config Parameters)
function GGF.MDL.MPRC_ACP_INITCTRL(in_instance)
	return initControllerTrigger(in_instance, "ACP");
end


-- п.1.6 (Auxiliary Event Callbacks)
function GGF.MDL.MPRC_AEC_INITCTRL(in_instance)
	return initControllerTrigger(in_instance, "AEC");
end


-- п.1.7 (Main Event Callbacks)
function GGF.MDL.MPRC_MEC_INITCTRL(in_instance)
	return initControllerTrigger(in_instance, "MEC");
end


-- п.1.8 (Object Adjust)
function GGF.MDL.MPRC_ADJ_INITCTRL(in_instance)
	return initControllerTrigger(in_instance, "ADJ");
end


-- п.1.9 (After-load Settings)
function GGF.MDL.MPRC_ALS_INITCTRL(in_instance)
	return initControllerTrigger(in_instance, "ALS");
end


-- п.1.10 (Addon System Timer)
function GGF.MDL.MPRC_AST_INITCTRL(in_instance)
	return initControllerTrigger(in_instance, "AST");
end


-- п.1.11 (Addon System Timer)
function GGF.MDL.MPRC_AST_RNTMCTRL(in_instance, in_elapsed)
	--self:MPRC_AST_RUNTIME(in_elapsed);
	-- NOTE: перенесено в инициализатор(независимый таймер)(откат до старого состояния, причина - см. сверху)
	--self:MPRC_ACT_RUNTIME(in_elapsed);
	--local mdList = self:getCascadeObj();
	--for idx,module in ipairs(mdList) do
		--module:MPRC_AST_RNTMCTRL(in_elapsed);
	--end
end


-- п.1.12 (All Config Parameters)
function GGF.MDL.MPRC_ACP_TERMCTRL(in_instance)
	return terminateControllerTrigger(in_instance, "ACP");
end


-- п.1.13 (Coup De Module)
function GGF.MDL.MPRC_CDM_TERMCTRL(in_instance)
	return terminateControllerTrigger(in_instance, "CDM");
end


-- п.2.0 (System Variables)
function GGF.MDL.MPRC_SVR_INITIALIZE(in_instance)
end


-- п.2.1 (Miscellaneous Variables)
function GGF.MDL.MPRC_MVR_INITIALIZE(in_instance)
end


-- п.2.2 (Main Config Parameters)
function GGF.MDL.MPRC_MCP_INITIALIZE(in_instance)
end


-- п.2.3 (Objects)
function GGF.MDL.MPRC_OBJ_INITIALIZE(in_instance)
end


-- п.2.4 (Auxiliary Config Parameters)
function GGF.MDL.MPRC_ACP_INITIALIZE(in_instance)
end


-- п.2.5 (Auxiliary Event Callbacks)
function GGF.MDL.MPRC_AEC_INITIALIZE(in_instance)
end


-- п.2.6 (Main Event Callbacks)
function GGF.MDL.MPRC_MEC_INITIALIZE(in_instance)
end


-- п.2.7 (Object Adjust)
function GGF.MDL.MPRC_ADJ_INITIALIZE(in_instance)
end


-- п.2.8 (After-load Settings)
function GGF.MDL.MPRC_ALS_INITIALIZE(in_instance)
end


-- п.2.9 (Addon System Timer)
function GGF.MDL.MPRC_AST_INITIALIZE(in_instance)
	-- WARNING: не перегружать!!!
	if (false) then
	--if (getInstanceTimerFlag(in_instance)) then
		setInstanceTimer(in_instance, CreateFrame("Frame"));
		getInstanceTimer(in_instance):SetScript("OnUpdate", function(in_self, in_elapsed)
			initStage(in_instance, "MPRC_AST_RUNTIME", in_elapsed);
		end);
	end
end


-- п.3.0 (Addon System Timer)
function GGF.MDL.MPRC_AST_RUNTIME(in_instance, in_elapsed)
end


-- п.3.1 (Addon Check Timer)
function GGF.MDL.MPRC_ACT_RUNTIME(in_instance, in_elapsed)
	-- TODO: приколхозить отдельный от общего таймер с целью запуска чеков для ошибочно не каскаднутых модулей
	GGF.Timer(self.vs_autoCheckTimer, in_elapsed, function()
		for index, check in ipairs(self.fn_standardAutoCheckList) do
			if (check.enabled) then
				local chckFlag, errDesc = check.checker(self);
				if (not chckFlag) then
					stdError(in_instance, check.code, errDesc);
				else
					self.vs_currentErrors[check.code] = nil;
					if (check.type == GGE.ModuleCheckTypes.DisablingOnComplete) then
						check.enabled = false;
					end
				end
			end
		end
		for index, check in ipairs(self.fn_customAutoCheckList) do
			if (check.enabled) then
				local chckFlag, errDesc = check.checker(self);
				if (not chckFlag) then
					stdError(in_instance, check.code, errDesc);
				else
					self.vs_currentErrors[check.code] = nil;
					if (check.type ==  GGE.ModuleCheckTypes.DisablingOnComplete) then
						check.enabled = false;
					end
				end
			end
		end
	end);

	GGF.Timer(self.vs_timeCheckTimer, in_elapsed, function()
		for code, errsgn in pairs(self.vs_currentErrors) do
			if (errsgn.time ~= nil) then
				errsgn.time = errsgn.time - in_elapsed;
				if (errsgn.time <= 0) then
					-- сброс ошибки
					vs_currentErrors[code] = nil;
				end
			end
		end
	end);
end


-- п.4.0 (All Config Parameters)
function GGF.MDL.MPRC_ACP_TERMINATE(in_instance)
end


-- п.4.1 (Coup De Module)
function GGF.MDL.MPRC_CDM_TERMINATE(in_instance)
end


-- п.5.0 ()
function GGF.MDL.MPRC_STP_UNITTEST(in_instance)
end


-- Установка фактического прохода модулем опрашивателя(interrogator)
function GGF.MDL.SetModuleAsInterrogated(in_instance)
	setInstanceSIPStageState(in_instance, "interrogated", true);
end


-- Взятие данных для неиспользуемых полей
function GGF.MDL.GetUnusedFiledData()
	return "N/A";
end


-- Взятие метаданных экземпляра
function GGF.MDL.GetModuleShardCount(in_instance)
	local chs = getChassis(in_instance);
	return (chs.FNC or 0) + (chs.API or 0) + (chs.INT or 0) + (chs.MDT or 0) + (chs.OBC or 0);
end


-- TODO: переделать
-- Взятие с предподготовокй списка ошибок
function GGF.MDL.GetErrorList(in_instance)
	local errData = {
		-- на всякий случай место осталось
		errList = {},
	};
	--for code, errstruct in pairs(in_instance.vs_currentErrors) do
		--local errsgn = self:getErrorSignature(code);
		--errData.errList[#errData.errList + 1] = GGF.TableCopy(errsgn);
		--errData.errList[#errData.errList].codeToken = self.cs_errorCodeTokens[code];
	--end

	--return errData;
end


-- Взятие метаданных модуля
function GGF.MDL.GetModuleMeta(in_instance)
	return {
		name = getInstanceModuleNameStandard(in_instance),
		token = getInstanceModuleNameObject(in_instance),
		cluster = getInstanceClusterToken(in_instance),
	};
end


-- Проверка проведенной инициализации шагов модуля
function GGF.MDL.CheckInitProgress(in_instance)
	local slg = getInstanceSIPStageState(in_instance, "SLG");
	local svr = getInstanceSIPStageState(in_instance, "SVR");
	local mvr = getInstanceSIPStageState(in_instance, "MVR");
	local mcp = getInstanceSIPStageState(in_instance, "MCP");
	local obj = getInstanceSIPStageState(in_instance, "OBJ");
	local acp = getInstanceSIPStageState(in_instance, "ACP");
	local aec = getInstanceSIPStageState(in_instance, "AEC");
	local mec = getInstanceSIPStageState(in_instance, "MEC");
	local adj = getInstanceSIPStageState(in_instance, "ADJ");
	local als = getInstanceSIPStageState(in_instance, "ALS");

	-- NOTE: не разбирать на части, может еще пригодится...
	if (not(slg and svr and mvr and mcp and obj and acp and aec and mec and adj and als)) then
		-- по идее эта ошибка сама триггерится
		--self:stdError(in_instance, 3, "step completeness check failure ("
		--.. "SL: " .. tostring(sl)
		--.. " / SV: " .. tostring(sv)
		--.. " / MV: " .. tostring(mv)
		--.. " / AS: " .. tostring(as)
		--.. ")");
		-- TODO: Заменить на нормальный дроп ошибки
		local inst = GGF.MDL.GetModuleMeta(in_instance);
		print("cip failed:", inst.token, slg, svr, mvr, mcp, obj, acp, aec, mec, adj, als);
		return false;
	end
	return true;
end


-- Подготовка отчета о трехфазной загрузке
function GGF.MDL.PrepareInitializeReport(in_instance)
	local okText, failText = "OK", "FAILED";
	local wrp = function(in_shard)
		return GGF.StructColorToStringColor(
			GGF.TernExpSingle(in_shard, GGD.Color.Normal, GGD.Color.Critical)
		)
		 .. GGF.TernExpSingle(in_shard, okText, failText)
		 .. GGF.FlushStringColor();
	end
	return "( SLG : " .. wrp(getInstanceSIPStageState(in_instance, "SLG"))
		.. " / SVR : " .. wrp(getInstanceSIPStageState(in_instance, "SVR"))
		.. " / MVR : " .. wrp(getInstanceSIPStageState(in_instance, "MVR"))
		.. " / MCP : " .. wrp(getInstanceSIPStageState(in_instance, "MCP"))
		.. " / OBJ : " .. wrp(getInstanceSIPStageState(in_instance, "OBJ"))
		.. " / ACP : " .. wrp(getInstanceSIPStageState(in_instance, "ACP"))
		.. " / AEC : " .. wrp(getInstanceSIPStageState(in_instance, "AEC"))
		.. " / MEC : " .. wrp(getInstanceSIPStageState(in_instance, "MEC"))
		.. " / ADJ : " .. wrp(getInstanceSIPStageState(in_instance, "ADJ"))
		.. " / ALS : " .. wrp(getInstanceSIPStageState(in_instance, "ALS"))
		.. " )";
end


-- Создание идентифицирующей записи для глобального конфига
function GGF.MDL.AssignConfigParameter(in_instance, in_cfgdata)
	local cfgstorage = getInstanceExternalConfig(in_instance);
	-- NOTE: первое объявление параметра
	if (cfgstorage[in_cfgdata.token] ~= nil) then
		-- TODO: приколхозить обработчик ошибки(GG-29)
		ATLASSERT(false);
		return;
	end

	cfgstorage[in_cfgdata.token] = GGC.ConfigParameter:Create({
		moduletoken = getInstanceGToken(in_instance),
		parametertoken = in_cfgdata.token,
		vstr = in_cfgdata.vstr,
		vgtr = in_cfgdata.vgtr,
		vobj = in_cfgdata.vobj,
		cstr = in_cfgdata.cstr,
		cgtr = in_cfgdata.cgtr,
		cobj = in_cfgdata.cobj,
	});
end


-- Попытка подцепить параметр из глобального конфига
function GGF.MDL.AdjustConfigParameter(in_instance, in_token)
	local cfgstorage = getInstanceExternalConfig(in_instance);
	-- TODO: приколхозить обработчик ошибки(GG-29)
	ATLASSERT(cfgstorage[in_token]);

	cfgstorage[in_token]:Adjust();
end


-- Обновление записи в глобальном конфиге(обработчик хвоста коллбека скорее всего)
function GGF.MDL.UpdateConfigParameter(in_instance, in_token)
	local cfgstorage = getInstanceExternalConfig(in_instance);
	-- TODO: приколхозить обработчик ошибки(GG-29)
	ATLASSERT(cfgstorage[in_token]);

	cfgstorage[in_token]:Update();
end


-- 
function GGF.MDL.armUnitTest(in_name, in_description, in_function, in_time)
	-- TODO: дописать вместе с модулем тестирования
end


-- Обработчик ошибки для модулей
function GGF.MDL.ModuleStdError(in_instance, in_code, in_postDescription, in_blck)
	stdError(in_instance, in_code, in_postDescription, in_blck);
end