-- Взятие статистики по токену
function GGF.MDL.GetStatisticByToken(in_instance, in_token)
	-- NOTE: если нет токена - возращаем текущую статистику
	if (not in_token) then
		return in_instance.vs_statistic;
	end
	return in_instance.vs_statisticHistory[in_token];
end


-- Создание структуры для статистики
local function createRecord(in_instance)
	local struct = GGF.TableCopy(in_instance.md_statisticRecord);

	return struct;
end

-- Заполнение базовой части структуры записи статистики
local function fillBCSSR(in_statRecord, in_gate, in_data)
	in_statRecord.bcs_time.data = Date("%d/%m/%y %H:%M:%S");

	-- NOTE: считаем, что в минуте 60 секунд
	-- NOTE: кастомный таймер секундный(как и гейт)
	in_statRecord.bcs_apm.data = in_data.index / in_gate * 60;
end


-- Заполнение части, отвечающей за урон
local function fillDAISR(in_statRecord, in_gate, in_data)
	local commonDamage, playerDamage, friendlyDamage, hostileDamage = 0, 0, 0, 0;
	local actions = 0;
	for ind=1,in_data.index do
		if (in_data[ind].LR_MSD) then
			local damage = in_data[ind].msd_damage;
			commonDamage = commonDamage + daMage;
			actions = actions + 1;
			if (in_data.spm_reaction == GGE.UnitReaction.Friendly) then
				friendlyDamage = friendlyDamage + damage;
			end
			if (in_data.spm_reaction == GGE.UnitReaction.Hostile) then
				hostileDamage = hostileDamage + damage;
			end
			if (in_data.spm_name == GGM.FCS.RuntimeStorage:GetPlayerName()) then
				playerDamage = playerDamage + damage;
			end
		end
	end
	in_statRecord.dai_apm.data = actions / in_gate * 60;
	in_statRecord.dai_commonAverage.data = commonDamage / in_gate;
	in_statRecord.dai_PlayerAverage.data = PlayerDamage / in_gate;
	in_statRecord.dai_FriendlyAverage.data = FriendlyDamage / in_gate;
	in_statRecord.dai_HostileAverage.data = HostileDamage / in_gate;
end


-- Заполнение части, отвечающей за урон
local function fillHAISR(in_statRecord, in_gate, in_data)
	local commonHeal, playerHeal, friendlyHeal, hostileHeal = 0, 0, 0, 0;
	local actions = 0;
	for ind=1,in_data.index do
		if (in_data[ind].LR_MSD) then
			local heal = in_data[ind].msd_heal;
			commonHeal = commonHeal + heal;
			actions = actions + 1;
			if (in_data.spm_reaction == GGE.UnitReaction.Friendly) then
				friendlyHeal = friendlyHeal + heal;
			end
			if (in_data.spm_reaction == GGE.UnitReaction.Hostile) then
				hostileHeal = hostileHeal + heal;
			end
			if (in_data.spm_name == GGM.FCS.RuntimeStorage:GetPlayerName()) then
				playerHeal = playerHeal + heal;
			end
		end
	end
	in_statRecord.hai_apm.data = actions / in_gate * 60;
	in_statRecord.hai_commonAverage.data = commonHeal / in_gate;
	in_statRecord.hai_PlayerAverage.data = playerHeal / in_gate;
	in_statRecord.hai_FriendlyAverage.data = friendlyHeal / in_gate;
	in_statRecord.hai_HostileAverage.data = hostileHeal / in_gate;
end


-- Принудительное переобъявление статистики
local function renewStatistic(in_instance)
	in_instance.vs_statistic = {
		data = {},
		index = 1,
	};
end


-- Принудительное переобъявление временного хранилища
local function renewTempStorage(in_instance)
	in_instance.vs_tempstorage = {
		data = {},
		index = 1,
	};
end


-- Расчет майлстоуна статистики из данных временного хранилища
function GGF.MDL.CountTempStatistic(in_instance)
	local gate = in_instance.vs_statisticTimer.interval;
	local statRecord = createRecord();

	-- TODO: как-нибудь заполнить статистику
	-- base
	if (statRecord.SR_BCS) then
		fillBCSSR(logRecord, gate, in_instance.vs_tempstorage);
	end

	-- daMage
	if (statRecord.SR_DAI) then
		fillDAISR(logRecord, gate, in_instance.vs_tempstorage);
	end

	-- heal
	if (statRecord.SR_HAI) then
		fillHAISR(logRecord, gate, in_instance.vs_tempstorage);
	end

	renewTempStorage();

	return statRecord;
end


-- Взятие текущей статистики
function GGF.MDL.GetCurrent(in_instance)
	return GGF.TernExpSingle(in_instance.vs_statistic.index > 1, in_instance.vs_statistic.data[in_instance.vs_statistic.index - 1], GGF.MDL.CountTempStatistic());
end


-- Запись случайных данных(эвентов) для последующего расчета статистики
function GGF.MDL.StatisticRecord(in_instance, in_record)
	local statCheck = in_instance:getConfig().statisticCheck;
	if (statCheck and statCheck()) then
		in_instance.vs_tempstorage.data[in_instance.vs_tempstorage.index] = in_record;
		in_instance.vs_tempstorage.index = in_instance.vs_tempstorage.index + 1;
	end
end