-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


GGE.UnitReaction = {
	Friendly = 1,
	Hostile = 2,
};

GGE.UnitFaction = {
	Alliance = 1,
	Horde = 2,
	Default = 3,
};

GGE.UnitType = {
	Pet = 1,
	Player = 2,
	Mob = 3,
	Object = 4,
};

GGC.StatisticModule = {
	instanceTemplate = {
		vs_tempstorage = {
			data = {},
			index = 1,
		},
		vs_statistic = {
			data = {},
			index = 1,
		},
		vs_statisticHistory = {},

		-- Таймер расчета статистики
		vs_statisticTimer = {
			tick = 0,
			interval = 1,
			stopped = true,
		},

		-- NOTE: тут лежит прототип записи статистики
		md_statisticRecord = {},	-- определение отдельное в каждом модуле, раздел mdata

		-- п.3.0 (Addon System Timer)
		MPRC_AST_RUNTIME = function(in_instance, in_elapsed)
			GGF.Timer(in_instance.vs_statisticTimer, in_elapsed, function()
				in_instance.vs_statistic.data[in_instance.vs_statistic.index] = GGF.MDL.CountTempStatistic();
				in_instance.vs_statistic.index = in_instance.vs_statistic.index + 1;
			end);
		end
	},
};

GGC.StatisticModule = GGF.TableSafeExtend(GGC.StatisticModule, GGC.BaseModule);

local This = GGC.StatisticModule;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "rgcontrol" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------