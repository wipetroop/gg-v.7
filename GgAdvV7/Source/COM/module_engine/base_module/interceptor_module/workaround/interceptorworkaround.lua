-- TODO: убрать!!!
local multicastedSpells = {
	["45284"] = true,
	["45297"] = true,
	["120588"] = true,
	["77451"] = true,
};

-- Заполнение базовой части структуры записи лога
local function fillBCSLR(in_logRecord, in_data)
	in_logRecord.bcs_time.data = date("%d/%m/%y %H:%M:%S");

	in_logRecord.bcs_timestamp.data = in_data.bcs_timestamp;
end

-- Заполнение части, отвечающей за информацию о дополнительном заклинании эвента
local function fillESDLR(in_logRecord, in_data)
	local skillStat = GGM.ACR.CombatInterceptor:GetStatisticSkillNameBT();

	if (skillStat[in_data.esd_id] == nil) then
		in_logRecord.esd_defined.color = GGD.Color.Critical;
		in_logRecord.esd_defined.data = GGE.SpellDefiniteness.Undefined;
		in_logRecord.esd_name.data = GGF.MDL.GetUnusedFiledData();
	else
		in_logRecord.esd_defined.color = GGD.Color.Normal;
		in_logRecord.esd_defined.data = GGE.SpellDefiniteness.Defined;
		in_logRecord.esd_name.data = skillStat[in_data.esd_id].spellName;
	end
end

-- Заполнение части, отвечающей за информацию о типе/подтипе эвента
local function fillETMLR(in_logRecord, in_data)
	in_logRecord.etm_extraType.data = in_data.etm_extraType;

	in_logRecord.etm_type.data = in_data.etm_type;
end

-- Заполнение части, отвечающей за информацию об основном заклинании эвента
local function fillMSDLR(in_logRecord, in_data)
	local skillStat = GGM.ACR.CombatInterceptor:GetStatisticSkillNameBT();

	in_logRecord.msd_absorbed.data = in_data.msd_absorbed;
	in_logRecord.msd_amount.data = in_data.msd_amount;

	if (skillStat[in_data.msd_id] == nil) then
		in_logRecord.msd_defined.color = GGD.Color.Critical;
		in_logRecord.msd_defined.data = GGE.SpellDefiniteness.Undefined;
		in_logRecord.msd_name.data = GGF.MDL.GetUnusedFiledData();
	else
		in_logRecord.msd_defined.color = GGD.Color.Normal;
		in_logRecord.msd_defined.data = GGE.SpellDefiniteness.Defined;
		in_logRecord.msd_name.data = skillStat[in_data.msd_id].spellName;
	end

	in_logRecord.msd_extraAmount.data = in_data.msd_extraAmount;

	in_logRecord.msd_id.data = in_data.msd_id;

	in_logRecord.msd_isCritical.data = in_data.msd_isCritical;
	in_logRecord.msd_isCritical.color = GGF.TernExpSingle(in_logRecord.msd_isCritical.data, GGD.Color.Normal, GGD.Color.Critical);

	in_logRecord.msd_isMulticast.data = (multicastedSpells[tostring(in_data.msd_id)] ~= nil);
	in_logRecord.msd_isMulticast.color = GGF.TernExpSingle(in_logRecord.msd_isMulticast.data, GGD.Color.Normal, GGD.Color.Critical);

	in_logRecord.msd_isOffhand.data = in_data.msd_isOffhand;

	in_logRecord.msd_overheal.data = in_data.msd_overheal;
	in_logRecord.msd_overkill.data = in_data.msd_overkill;
end

-- Заполнение части, отвечающей за информацию о сурсе
local function fillSPMLR(in_logRecord, in_data)
	in_logRecord.spm_flag.data = in_data.spm_flag;

	in_logRecord.spm_id.data = in_data.spm_id;

	in_logRecord.spm_name.data = in_data.spm_name;

	in_logRecord.spm_ownerName.data = GGF.MDL.GetUnusedFiledData();
   in_logRecord.spm_ownerId.data = GGF.MDL.GetUnusedFiledData();
   in_logRecord.spm_ownerFlag.data = GGF.MDL.GetUnusedFiledData();
	-- Заполняет поля: spm_ownerName, spm_ownerId, spm_ownerFlag
	--GGM.GUI.PetResolver:DefineSourcePetOwner(in_logRecord);

	in_logRecord.spm_name.reaction = GGM.ACR.CommonService:GetUnitFactionByFlag(in_data.spm_flag);
end

-- Заполнение части, отвечающей за информацию о таргете
local function fillTPMLR(in_logRecord, in_data)
	in_logRecord.tpm_flag.data = in_data.tpm_flag;

	in_logRecord.tpm_id.data = in_data.tpm_id;

	in_logRecord.tpm_name.data = in_data.tpm_name;

   in_logRecord.tpm_ownerName.data = GGF.MDL.GetUnusedFiledData();
   in_logRecord.tpm_ownerId.data = GGF.MDL.GetUnusedFiledData();
   in_logRecord.tpm_ownerFlag.data = GGF.MDL.GetUnusedFiledData();
	-- Заполняет поля: tpm_ownerName, tpm_ownerId, tpm_ownerFlag
	--GGM.GUI.PetResolver:DefineTargetPetOwner(in_logRecord);

	in_logRecord.tpm_name.reaction = GGM.ACR.CommonService:GetUnitFactionByFlag(in_data.tpm_flag);
end

--
function GGF.MDL.InterceptEvent(in_instance, in_data)
	-- Создание структуры записи
	local logRecord = GGF.MDL.CreateRecord(in_instance:getConfig().logger());

	-- base
	if (logRecord.LR_BCS ~= nil) then
		fillBCSLR(logRecord, in_data);
	end

	-- extra spell
	if (logRecord.LR_ESD ~= nil) then
		fillESDLR(logRecord, in_data);
	end

	-- event type
	if (logRecord.LR_ETM ~= nil) then
		fillETMLR(logRecord, in_data);
	end

	-- main spell
	if (logRecord.LR_MSD ~= nil) then
		fillMSDLR(logRecord, in_data);
	end

	-- source
	if (logRecord.LR_SPM ~= nil) then
		fillSPMLR(logRecord, in_data);
	end

	-- target
	if (logRecord.LR_TPM ~= nil) then
		fillTPMLR(logRecord, in_data);
	end

	-- Дополнительная обработка(случай редиректа на перехватчик с особой логикой)
	local addCond = in_instance:getConfig().additionalConditions;
	if (addCond ~= nil) then
		addCond(logRecord);
	end

	-- Логгирование записи
	local logger = in_instance:getConfig().logger;
	if (logger ~= nil) then
		--print("logged");
		GGF.MDL.LogRecord(in_instance:getConfig():logger(), logRecord);
	end

	-- Запись статистики
	local statistic = in_instance:getConfig().statistic;
	if (statistic ~= nil) then
		--statistic():StatisticRecord(logRecord);
	end
end