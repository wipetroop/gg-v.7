local DFList = {
	ErrorType = GGF.DockDomName(GGD.EnumerationIdentifier, "ErrorType"),
	ErrorCodeToken = GGF.DockDomName(GGD.EnumerationIdentifier, "ErrorCodeToken"),
	ModuleCheckType = GGF.DockDomName(GGD.EnumerationIdentifier, "ModuleCheckType"),
};

-- Типы ошибок(даже скорее правила)
GGF.GRegister(DFList.ErrorType, {
	FixSelfThrowing = 1,		-- Самосбрасывающиеся после исправления(только авточеки)
	TimeSelfThrowing = 2,		-- Самосбрасывающиеся после интервала
	NonCriticalBlocker = 3,		-- Несбрасывающиеся некритические
	CriticalBlocker = 4,		-- Несбрасывающиеся критические
});

GGF.GRegister(DFList.ErrorCodeToken, {
	[1] = "ET_FX-SFTH",
	[2] = "ET_TM-SFTH",
	[3] = "ET_NC-BLCK",
	[4] = "ET_CC-BLCK",
});

GGF.GRegister(DFList.ModuleCheckType, {
	Standard = 0,				-- цилкические чеки, доп. действия не требуются
	DisablingOnComplete = 1,	-- сбрасывающиеся, если проверка прошла(после первой успешной)
});

GGC.BaseModule = {
	-- Переменные классов
	instanceTemplate = {
		-- NOTE: module powertrain runtime control(вроде бы)
		MPRC = {		-- системный раздел модуля
			maintenance = {
				modulename = {
					standard = "",
					object = "",
				},
				cascade_obj = {},	-- NOTE: link
				modulegtoken = "",
				--modulepath = "",
				modulerole = GGD.ModuleRole.Undefined,
				clustertoken = "",
				timer = false,

				stateInitProtocol = {
					interrogated = false,

					SLG = false,
					SVR = false,
					MVR = false,
					MCP = false,
					OBJ = false,
					ACP = false,
					AEC = false,
					MEC = false,
					ADJ = false,
					ALS = false,
					AST = false,
				},
				stateTerminateProtocol = {
					ACP = false,
					CDM = false,
				},
			},
			powertrain = {
				externalconfig = {},	-- врапперы для параметров внешнего конфига
				--chassis = {				-- список файлов модуля(типовых)(со значением по умолчанию)
					--DAT		-- data нет смысла чекать(т.к. прочекано при каскаде модулей)
					--FNC = 0,	-- *_functional.lua
					--API = 0,	-- *_api.lua
					--INT = 0,	-- *_intercomm.lua
					--MDT = 0,	-- *_mdata[№].lua
					--OBC = 0,	-- *_objconfig.lua
				--},
				--cascade = {
					--GGD.EvokeCascadeEndList,
				--},			-- манифест каскадной загрузки/отгрузки/выполнения зависимых модулей
				--cascade_obj = function() return {} end,
				internalconfig = {},	-- общий цифровой конфиг(преимущественно для сущностей objconfig файлов)(чаще всего линкуется на локальный MSC_CONFIG)
				timer = {},
			},
		},


		-- п.1.0.0.0 (API Check)
		MS_CHECK_API = function()
			return 0;
		end,


		-- п.1.0.0.1 (FNC Check)
		MS_CHECK_FNC = function()
			return 0;
		end,


		-- п.1.0.0.2 (INT Check)
		MS_CHECK_INT = function()
			return 0;
		end,


		-- п.1.0.0.3 (MDT Check)
		MS_CHECK_MDT_0 = function()
			return 0;
		end,


		-- п.1.0.0.4 (OBC Check)
		MS_CHECK_OBC = function()
			return 0;
		end,


		-- Взятие локального конфига модуля
		getConfig = function(self)
			return GGF.MDL.GetInstanceConfig(self);
		end,

		--mdata = {},			-- "большие" данные(md_)
		--objconfig = {},		-- место хранения псевдообъектов(и прототипов)(oc_)
		--jumper = {}, 			-- укороченный доступ к объектам(дублёр)(jr_)

		-- Текущие ошибки по модулю
		currentErrors = {
			[5] = {},	-- TODO: потом убрать(это отладка)
		},
	},

	-------------------------------------------------------------------------------
	-- Maintenance information -- системная информация(mi_)
	-------------------------------------------------------------------------------


	-------------------------------------------------------------------------------
	-- Constant numerics -- числовая константа(cn_)
	-------------------------------------------------------------------------------


	-------------------------------------------------------------------------------
	-- Constant flags -- булевская константа(cf_)
	-------------------------------------------------------------------------------


	-------------------------------------------------------------------------------
	-- Constant lines -- строковая константа(cl_)
	-------------------------------------------------------------------------------


	-------------------------------------------------------------------------------
	-- Constant structs -- константная структура(cs_)
	-------------------------------------------------------------------------------

	-- Список стандратных ошибок для авточеков(предел - 100, дальше - кастомные)
	-- NOTE: объявлено ниже
	cs_standardErrors = {
	},


	cs_customErrors = {
	},

	-------------------------------------------------------------------------------
	-- Functors -- по факту, это колхозный функтор..(fn_)
	-------------------------------------------------------------------------------

	-- Авточеки для тайминговой проверки модулей(true - ошибки нет)
	fn_standardAutoCheckList = {
	},

	-- Кастомные авточеки
	fn_customAutoCheckList = {
	},

	-------------------------------------------------------------------------------
	-- Variable numerics -- числовая переменная(vn_)
	-------------------------------------------------------------------------------


	-------------------------------------------------------------------------------
	-- Variable flags -- булевская переменная(vf_)
	-------------------------------------------------------------------------------


	-------------------------------------------------------------------------------
	-- Variable lines -- строковая переменная(vl_)
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	-- Variable structs -- переменная структура(vs_)
	-------------------------------------------------------------------------------

	-- Таймер авточеков
	vs_autoCheckTimer = {
		tick = 0,
		interval = 15,
		stopped = false,
	},

	-- Таймер временных ошибок
	vs_timeCheckTimer = {
		tick = 0,
		interval = 15,
		stopped = false,
	},

	-- список кастомно объявленных ошибок при инициализации(перегружаемое)
	vs_customErrors = {};

	-------------------------------------------------------------------------------
	-- Frames -- остов фрейма(fr_)
	-------------------------------------------------------------------------------

	-- Объявлен дальше
	--fr_runtimer = {};
};


-------------------------------------------------------------------------------
-- "pre-initialize" block(only for base classes)
-------------------------------------------------------------------------------


-- NOTE: инициализация некоторых вещей работает только после объявления объекта
-- Список стандратных ошибок для стандартных чеков(предел - 100, дальше - кастомные)
GGC.BaseModule.cs_standardErrors = {
	[1] = {
		type = GGE.ErrorType.CriticalBlocker,
		identifier = "DEFECTIVE_INITIALIZATION",
		description = "Initialization step failed\n",
	},
	[2] = {
		type = GGE.ErrorType.TimeSelfThrowing,
		identifier = "INCORRECT_CONSOLE_ARGUMENT",
		description = "Parameter mismatch\n",
		time = 10,
	},
	[3] = {
		type = GGE.ErrorType.FixSelfThrowing,
		identifier = "UNFINISHED_INITIALIZATION",
		description = "Initialization step not complete\n",
	},
	[4] = {
		type = GGE.ErrorType.FixSelfThrowing,
		identifier = "INCOMPLETE_INTERROGATION",
		description = "Interrogation not complete\n",
	},
	[5] = {
		type = GGE.ErrorType.FixSelfThrowing,
		identifier = "PREDEFINED_TEST_RECORD",
		description = "This is only a test\n",
	},
};


-- Авточеки для тайминговой проверки модулей(true - ошибки нет)
GGC.BaseModule.fn_standardAutoCheckList = {
	{
		-- NOTE: чекер не сработает, если модуль не в каскаде, т.е. ошибка так и будет висеть
		checker = function(in_instance)
			local protocol = in_instance.MPRC.maintenance.stateInitProtocol;
			local slg = protocol.SLG;
			local svr = protocol.SVR;
			local mvr = protocol.MVR;
			local mcp = protocol.MCP;
			local obj = protocol.OBJ;
			local acp = protocol.ACP;
			local aec = protocol.AEC;
			local mec = protocol.MEC;
			local adj = protocol.ADJ;
			local als = protocol.ALS;

			local errDesc = "step completeness check failure " .. GGF.MDL.PrepareInitializeReport(in_instance);
			return (slg and svr and mvr and mcp and obj and acp and aec and mec and adj and als), errDesc;
		end,
		code = 3,
		enabled = true,
		type = GGE.ModuleCheckType.DisablingOnComplete,
	},
	{
		-- NOTE: чекер не сработает, если модуль не пройден опрашивателем
		checker = function(in_instance)
			local protocol = in_instance.MPRC.maintenance.stateInitProtocol;
			local errDesc = "module interrogate failure";
			return protocol.interrogated, errDesc;
		end,
		code = 4,
		enabled = true,
		type = GGE.ModuleCheckType.DisablingOnComplete,
	},
};


local This = GGC.BaseModule;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- Конструктор базового класса
function This:Create(in_argline)
	local obj = GGF.TableCopy(GGF.INS.GetModuleInstanceTemplate());

	obj = GGF.TableSafeExtend(obj, self:getInstanceTemplate());
	obj.__class = This;
	--obj = GGF.TableCopy(self);

	GGF.MDL.InitializeModule(obj, in_argline);

	return obj;
end


-------------------------------------------------------------------------------
-- "rgcontrol" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие шаблона параметров объекта
function This:getInstanceTemplate()
	return self.instanceTemplate;
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------