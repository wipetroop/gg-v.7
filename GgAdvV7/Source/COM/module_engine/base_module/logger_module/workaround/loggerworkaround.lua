-- Принудительное переобъявление лога
local function renewLog(in_instance)
	in_instance.vs_log = {
		data = {},
		index = 1,
	};
end


-- Взятие лога по токену вместе с десериализатором записей
function GGF.MDL.GetLogByToken(in_instance)
	-- NOTE: если нет токена - возращаем текущий лог
	if (not in_token) then
		return in_instance.vs_log, in_instance:getConfig().deserializer;
	end
	return in_instance.vs_logHistory[in_token], in_instance:getConfig().deserializer;
end


-- Создание структуры для логгирования урона(до 2 объектов(source, target) за одну запись)
function GGF.MDL.CreateRecord(in_instance)
	local struct = GGF.TableCopy(in_instance.md_logRecord);

	return struct;
end


-- Взятие структуры логгирования для доп. расчетов
function GGF.MDL.GetLogRecordStructure(in_instance)
	local struct = GGF.TableCopy(in_instance.md_logRecord);
	-- NOTE: вообще говоря, копировать не обязательно, но так безопаснее
	return struct;
end


-- Логгирование событий
function GGF.MDL.LogRecord(in_instance, in_record)
	local loggingCheck = in_instance:getConfig().loggingCheck;
	if (loggingCheck and loggingCheck()) then
		print("logged", GGF.MDL.GetModuleMeta(in_instance).name);
		in_instance.vs_log.data[in_instance.vs_log.index] = in_instance:getConfig().serializer(in_record);
		in_instance.vs_log.index = in_instance.vs_log.index + 1;
	end
end


-- Стартер лога
function GGF.MDL.EngageLog(in_instance, in_data)
	renewLog(in_instance);
end


-- Финишер лога с записью в историю по токену
function GGF.MDL.DisengageLog(in_instance, in_data, in_token)
	in_instance.vs_logHistory[in_token] = in_instance.vs_log;
end