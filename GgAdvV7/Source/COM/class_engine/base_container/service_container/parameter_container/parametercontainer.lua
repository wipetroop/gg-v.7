-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

-- template

--	ptype = 
--	ctype = 
--	token = 
--	fixed = 
--	linkedto = 
--	parameter = {
--		value = 
--		func = 
--	}

local OBJSuffix = "PC";

local DFList = {
	ParameterContainerSuffix = GGF.DockDomName(GGD.DefineIdentifier, "ParameterContainerSuffix"),
	ParameterContainer = GGF.DockDomName(GGD.ClassIdentifier, "ParameterContainer"),
};

GGF.GRegister(DFList.ParameterContainerSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.ParameterContainer, {
	meta = {
		name = "Parameter Container",
		mark = "",	-- авт.
		suffix = GGD.ParameterContainerSuffix,
	},

	instanceTemplate = {
		-- WARNING: не пихать эти переменные во вложенные структуры!!!
		cntchecker = true;	-- NOTE: переменная проверки модуля(если есть, то это контейнер)(нужно для установки метки)
		--ptype = GGE.ParameterType.Value,	-- parameter type(function/value)
		ctype = GGE.ContainerType.Property,	-- container type(property/object)
		token = 'DefaultToken',		-- для генерируемых функций(Get[Token], Set[Token], etc.)
		fixed = false,		-- флаг не перегружаемого параметра
		mark = "",		-- метка объекта, которому принадлежит контейнер(автополе, не заполнять!!!)
		-- NOTE: токен служит еще и для применения свойств
		-- NOTE: убрано
		--linkedto = {},	-- список токенов объекта на которые распространяется действий свойства(только трансивер!!!)
		--refparamtoken = "",	-- токен параметра объекта(NOTE: зачем это???)
		priority = 0,	-- только для трансиверов(NOTE: не используется)

		parameter = {
			value = 0,
			func = function(in_argline) end,
		},
		default = {
			value = 0,
			objSuffix = "",
		},
		interdata = {
			argline = {},
		},

		[GGE.RuntimeMethod.Modifier] = nil,
		[GGE.RuntimeMethod.Setter] = nil,
		[GGE.RuntimeMethod.CounterGetter] = nil,
		[GGE.RuntimeMethod.TrLinkGetter] = nil,
		[GGE.RuntimeMethod.ValueGetter] = nil,
		[GGE.RuntimeMethod.Adjuster] = nil,
		[GGE.RuntimeMethod.Synchronizer] = nil,
		--[GGE.RuntimeMethod.ArglineAdder] = nil,
		--[GGE.RuntimeMethod.ArglineSetter] = nil,
	},
});

--GGF.Mark(GGC.ParameterContainer);

GGC.ParameterContainer = GGF.TableSafeExtend(GGC.ParameterContainer, GGC.ServiceContainer);

local This = GGC.ParameterContainer;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- Конструктор базового класса(перегружен)
function This:Create(in_overrideParams)
	local obj = GGF.TableCopy(self.instanceTemplate);
	ATLASSERT(in_overrideParams, "cte.Token");
	ATLASSERT(in_overrideParams, "cte.Default");
	ATLASSERT(in_overrideParams, "cte.ObjSuffix");
	-- NOTE: проверка, не перетерлись ли параметры нулями
	--self:safeAssignValue(in_overrideParams, "ptype");
	--ATLASSERT(self.ptype ~= nil);
	GGF.SafeAssignValue(obj, in_overrideParams, "ctype");
	ATLASSERT(obj.ctype ~= nil);
	GGF.SafeAssignValue(obj, in_overrideParams, "cte.Token", "token");
	ATLASSERT(obj.token ~= nil);
	GGF.SafeAssignValue(obj, in_overrideParams, "fixed");
	ATLASSERT(obj.fixed ~= nil);

	GGF.SafeAssignValue(obj, in_overrideParams, "cte.Default", "default.token");
	ATLASSERT(obj.default.value ~= nil);
	GGF.SafeAssignValue(obj, in_overrideParams, "cte.ObjSuffix", "default.objSuffix");
	ATLASSERT(obj.default.objSuffix ~= nil);

	-- TODO: убрать, если не нужно
	ATLASSERT(GGF.SplitCallGet(in_overrideParams, "parameter.default") == nil);
	ATLASSERT(GGF.SplitCallGet(in_overrideParams, "parameter.value") == nil);
	ATLASSERT(GGF.SplitCallGet(in_overrideParams, "parameter.func") == nil);
	--self:safeAssignValue(in_overrideParams, "parameter.value");
	--ATLASSERT(self.parameter.value ~= nil);
	--self:safeAssignValue(in_overrideParams, "parameter.func");
	--ATLASSERT(self.parameter.func ~= nil);
	-- NOTE: отключен
	--GGF.SafeAssignValue(obj, in_overrideParams, "interdata.argline");
	--ATLASSERT(obj.interdata.argline ~= nil);

	GGF.SafeAssignValue(obj, in_overrideParams, GGE.RuntimeMethod.Modifier);
	GGF.SafeAssignValue(obj, in_overrideParams, GGE.RuntimeMethod.Setter);
	GGF.SafeAssignValue(obj, in_overrideParams, GGE.RuntimeMethod.CounterGetter);
	GGF.SafeAssignValue(obj, in_overrideParams, GGE.RuntimeMethod.ValueGetter);
	-- NOTE: не требуется(инвариант)
	--self:safeAssignValue(in_overrideParams, GGE.RuntimeMethod.RPTListGetter);
	GGF.SafeAssignValue(obj, in_overrideParams, GGE.RuntimeMethod.Adjuster);
	GGF.SafeAssignValue(obj, in_overrideParams, GGE.RuntimeMethod.Synchronizer);
	--GGF.SafeAssignValue(in_overrideParams, GGE.RuntimeMethod.ArglineAdder);
	--GGF.SafeAssignValue(in_overrideParams, GGE.RuntimeMethod.ArglineSetter);
	return obj;
end


-- Применение всех свойств к объекту класса(перегружен на пустой)
function This:Adjust()
end


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Безопасная установка дефолтного значения
--function This:safeAssignDefaultValue(in_params, in_path, in_diffPath)
	-- NOTE: дифф путь нужен, если значение в другом месте
	-- TODO: придумать чек на недопустимые поля(возможно не требуется)(GG-57)
	--print("sadv:", GGF.SplitCallGet(in_params, in_path), in_path);
	--local defval = GGF.GetDefaultValue(GGF.SplitCallGet(in_params, in_path).ObjSuffix, GGF.SplitCallGet(in_params, in_path).Default);
	--if (GGF.SplitCallCheck(in_params, in_path)) then
		--GGF.SplitCallSet(self, in_diffPath or in_path, defval);
	--end
--end