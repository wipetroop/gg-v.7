-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


GGC.ConfigParameter = {
	meta = {
		name = "Config Parameter",
	},

	-- токен идентификатор пути
	-- ex.: GUI_DS_[name]
	modulepath = "",
	parametertoken = "",

	-- флаг использования частного параметра
	vgtr = function() end,
	vstr = function() end,
	vobj = "",
	cgtr = function() end,
	cstr = function() end,
	cobj = "",
};

--
-- На внешке должно быть как-то так:
-- common = {
-- 		value,	значение параметра
--		flag,	использование частного(только в общем конфиге)
-- }
-- percharacter = {
-- 		value,	значение параметра
-- }

GGC.ConfigParameter = GGF.TableSafeExtend(GGC.ConfigParameter, GGC.ServiceContainer);

local This = GGC.ConfigParameter;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- TODO: доделать
-- NOTE: тут вроде и так все работает же, не?

-- Конструктор базового класса(перегружен)
function This:Create(in_data)
	local obj = {};
	-- in_overrideParams = {
	-- modulepath = "CombatInformation.CommonService"
	-- parametertoken = "TSW_WIDTH",	-- обязательно заглавные(общепринятое соглашение по конфигам)
	-- vgtr = function() end,			-- для обновления параметра на внешке
	-- vstr = function() end,			-- для установки параметра класса
	-- cgtr = function() end,			-- для обновления принадлежности персонажу
	-- cstr = function() end,			-- для установки принадлежности персонажу
	--};
	self.modulepath = self:getDataElem(in_data, "modulepath");
	self.parametertoken = self:getDataElem(in_data, "parametertoken");
	self.vgtr = self:getDataElem(in_data, "vgtr");
	self.vstr = self:getDataElem(in_data, "vstr");
	self.vobj = self:getDataElem(in_data, "vobj");
	self.cgtr = self:getDataElem(in_data, "cgtr");
	self.cstr = self:getDataElem(in_data, "cstr");
	self.cobj = self:getDataElem(in_data, "cobj");

	if (not GGM.FCS.ConfigController:CheckParameter(self.modulepath, self.parametertoken)) then
		GGM.FCS.ConfigController:CreateParameter(self.modulepath, self.parametertoken, self.vgtr(self.vobj), self.cgtr(self.cobj));
	end

	return obj;
end


-- Применение всех свойств к объекту класса(перегружен)
function This:Adjust()
	self.cstr(self.cobj, GGM.FCS.ConfigController:GetCharspec(self.modulepath, self.parametertoken));
	self.vstr(self.vobj, GGM.FCS.ConfigController:GetValue(self.modulepath, self.parametertoken));
end


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Необходимость обновить параметр(неявный сеттер)
function This:Update()
	GGM.FCS.ConfigController:SetCharspec(self.modulepath, self.parametertoken, self.cgtr(self.cobj));
	GGM.FCS.ConfigController:SetValue(self.modulepath, self.parametertoken, self.vgtr(self.vobj));
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие параметра из аргументов загрузки
function This:getDataElem(in_data, in_path)
	return GGF.SplitCallGet(in_data, in_path);
end


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------