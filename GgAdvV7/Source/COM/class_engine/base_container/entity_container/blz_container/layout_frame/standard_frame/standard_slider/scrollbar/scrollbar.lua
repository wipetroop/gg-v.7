-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "SCB";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	ScrollBarSuffix = GGF.DockDomName(GGD.DefineIdentifier, "ScrollBarSuffix"),
	ScrollBar = GGF.DockDomName(GGD.ClassIdentifier, "ScrollBar"),
};

local CTList = {
   ScrollBar = GGF.CreateCT(OBJSuffix, "ScrollBar", "Frame"),
};

GGF.GRegister(DFList.ScrollBarSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.ScrollBar, {
	meta = {
		name = "Scroll Bar",
		mark = "",	-- авт.
		suffix = GGD.ScrollBarSuffix,
	},
});

GGC.ScrollBar.objects = {
	slider = {
		standard = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.ScrollBar,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.ScrollBar.Default)) then
					local slider = CreateFrame(
						"Slider",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyStandardSliderOD", slider);
					slider:SetScript("OnValueChanged", function() end);
					--in_wrapper:SetBackdrop(GGD.Backdrop.StandardSlider);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.ScrollBar.wrappers = {};

GGC.ScrollBar.properties = {};

GGC.ScrollBar.elements = {};

GGF.Mark(GGC.ScrollBar);

GGC.ScrollBar = GGF.TableSafeExtend(GGC.ScrollBar, GGC.StandardSlider);

GGF.CreateClassWorkaround(GGC.ScrollBar);

local This = GGC.ScrollBar;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------