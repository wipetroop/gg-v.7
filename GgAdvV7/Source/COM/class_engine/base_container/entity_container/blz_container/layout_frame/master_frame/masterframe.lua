-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "MF";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	MasterFrameSuffix = GGF.DockDomName(GGD.DefineIdentifier, "MasterFrameSuffix"),
	MasterFrame = GGF.DockDomName(GGD.ClassIdentifier, "MasterFrame"),
};

local CTList = {
	MasterFrame = GGF.CreateCT(OBJSuffix, "MasterFrame", "Frame"),
};

GGF.GRegister(DFList.MasterFrameSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.MasterFrame, {
	meta = {
		name = "Master Frame",
		mark = "",	-- авт.
		suffix = GGD.MasterFrameSuffix,
	},
});

-- TODO: убрать нахер!!!
local mfCount = 0;

-- NOTE: разница по факту только в принудительном названии без отсылки к объекту родителя
GGC.MasterFrame.objects = {
	frame = {
		master = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.MasterFrame,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				ATLASSERT(mfCount < 1);	-- NOTE: считаем, что мастер фрейм = рут, т.е. он только один может быть
				--print("mf create:", in_object, GGF.GetDefaultValue(OBJSuffix, CTList.MasterFrame.Default));
				-- TODO: отладить!!!
				--print(GGF.InnerInvoke(in_meta, "GetWrapperName"), in_class:getClassSuffix());
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.MasterFrame.Default)) then
					local masterFrame = CreateFrame(
						"Frame",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent")
					);
					GGF.InnerInvoke(in_meta, "ModifyMasterFrameOD", masterFrame);
					mfCount = mfCount + 1;
					--print("mf set", masterFrame, GGF.InnerInvoke(in_meta, "GetMasterFrame"), masterFrame.GetName);
					--ATLASSERT(false);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.MasterFrame.wrappers = {};

GGC.MasterFrame.properties = {};

GGC.MasterFrame.elements = {};

GGF.Mark(GGC.MasterFrame);

GGC.MasterFrame = GGF.TableSafeExtend(GGC.MasterFrame, GGC.LayoutFrame);

GGF.CreateClassWorkaround(GGC.MasterFrame);

local This = GGC.MasterFrame;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------
