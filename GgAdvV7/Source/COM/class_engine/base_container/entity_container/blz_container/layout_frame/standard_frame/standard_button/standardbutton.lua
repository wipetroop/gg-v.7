-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "SB";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	StandardButtonSuffix = GGF.DockDomName(GGD.DefineIdentifier, "StandardButtonSuffix"),
	StandardButton = GGF.DockDomName(GGD.ClassIdentifier, "StandardButton"),
};

local CTList = {
	StandardButton = GGF.CreateCT(OBJSuffix, "StandardButton", "Frame"),
	Template = GGF.CreateCT(OBJSuffix, "Template", "Template"),
	Enabled = GGF.CreateCT(OBJSuffix, "Enabled", "Enabled"),
	Text = GGF.CreateCT(OBJSuffix, "Text", "Text"),
	Highlighted = GGF.CreateCT(OBJSuffix, "Highlighted", "Highlighted"),
	OnClick = GGF.CreateCT(OBJSuffix, "OnClick", "EmptyCallback"),
};

GGF.GRegister(DFList.StandardButtonSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	Template = "UIPanelButtonTemplate",
	Enabled = true,
	Text = "Default",
	Highlighted = false,
	EmptyCallback = function(in_meta)
		print("std btn empty callback");
	end,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.StandardButton, {
	meta = {
		name = "Standard Button",
		mark = "",	-- авт.
		suffix = GGD.StandardButtonSuffix,
	},
});

GGC.StandardButton.objects = {
	button = {
		standard = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.StandardButton,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.StandardButton.Default)) then
					local standardButton = CreateFrame(
						"Button",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyStandardButtonOD", standardButton);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.StandardButton.wrappers = {};

GGC.StandardButton.properties = {
	base = {
		template = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Template,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	miscellaneous = {
		enabled = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Enabled,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local enabled = GGF.InnerInvoke(in_meta, "GetEnabled");
				if (enabled) then
					in_meta.object:Enable();
				else
					in_meta.object:Disable();
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyEnabled", in_meta.object:IsEnabled());
			end,
		}),
		text = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Text,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetText(GGF.InnerInvoke(in_meta, "GetText"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyText", in_meta.object:GetText());
			end,
		}),
		highlighted = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Highlighted,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local highlighted = GGF.InnerInvoke(in_meta, "GetHighlighted");
				if (highlighted) then
					in_meta.object:LockHighlight();
				else
					in_meta.object:UnlockHighlight();
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				ATLASSERT(false);
			end,
		}),
	},
	callback = {
		onClick = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnClick,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local callback = GGF.InnerInvoke(in_meta, "CounterGetOnClick");
				in_meta.object:SetScript("OnClick", callback);
				--print("adjust onclick", in_meta);
            --print("onclck cb:", GGF.InnerInvoke(in_meta, "GetWrapperName"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.StandardButton.elements = {};

GGF.Mark(GGC.StandardButton);

GGC.StandardButton = GGF.TableSafeExtend(GGC.StandardButton, GGC.StandardFrame);

GGF.CreateClassWorkaround(GGC.StandardButton);

local This = GGC.StandardButton;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------