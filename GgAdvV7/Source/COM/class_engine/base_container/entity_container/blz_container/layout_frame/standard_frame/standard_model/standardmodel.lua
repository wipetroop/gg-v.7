-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "SM";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	StandardModelSuffix = GGF.DockDomName(GGD.DefineIdentifier, "StandardModelSuffix"),
	StandardModel = GGF.DockDomName(GGD.ClassIdentifier, "StandardModel"),
};

GGF.GRegister(DFList.StandardModelSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.StandardModel, {
	meta = {
		name = "Standard Model",
		mark = "",	-- авт.
		suffix = GGD.StandardModelSuffix,
	},
});

GGC.StandardModel.objects = {};

GGC.StandardModel.properties = {};

GGC.StandardModel.elements = {};

GGF.Mark(GGC.StandardModel);

GGC.StandardModel = GGF.TableSafeExtend(GGC.StandardModel, GGC.StandardFrame);

GGF.CreateClassWorkaround(GGC.StandardModel);

local This = GGC.StandardModel;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------