-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "PM";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	PlayerModelSuffix = GGF.DockDomName(GGD.DefineIdentifier, "PlayerModelSuffix"),
	PlayerModel = GGF.DockDomName(GGD.ClassIdentifier,"PlayerModel"),
};

local CTList = {
	PlayerModel = GGF.CreateCT(OBJSuffix, "PlayerModel", "Frame"),
	CamDistanceScale = GGF.CreateCT(OBJSuffix, "CamDistanceScale", "CamDistanceScale"),
	PortraitZoom = GGF.CreateCT(OBJSuffix, "PortraitZoom", "PortraitZoom"),
	PositionX = GGF.CreateCT(OBJSuffix, "PositionX", "PositionX"),
	PositionY = GGF.CreateCT(OBJSuffix, "PositionY", "PositionY"),
	PositionZ = GGF.CreateCT(OBJSuffix, "PositionZ", "PositionZ"),
};

GGF.GRegister(DFList.PlayerModelSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	CamDistanceScale = 1.0,
	PortraitZoom = 0.5,
	PositionX = 0.0,
	PositionY = 0.0,
	PositionZ = 0.0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.PlayerModel, {
	meta = {
		name = "Player Model",
		mark = "",	-- авт.
		suffix = GGD.PlayerModelSuffix,
	},
});

GGC.PlayerModel.objects = {
	model = {
		player = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.PlayerModel,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.PlayerModel.Default)) then
					local playerModel = CreateFrame(
						"PlayerModel",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyPlayerModelOD", playerModel);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.PlayerModel.wrappers = {};

GGC.PlayerModel.properties = {
	-- TODO: это что?
	base = {
	},
	miscellaneous = {
		camDistanceScale = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.CamDistanceScale,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
            -- TODO: Уточнить патч, когда появилась анимированная иконка персонажа
				GGF.VersionSplitterProc({
					{
						versionInterval = {GGD.TocVersionList.Vanilla, GGD.TocVersionList.WotLK},
						action = function()
                  end,
               },
               {
                  versionInterval = {GGD.TocVersionList.Cata, GGD.TocVersionList.Invalid},
                  action = function()
				         in_meta.object:SetCamDistanceScale(GGF.InnerInvoke(in_meta, "GetCamDistanceScale"));
                  end,
               }
            });
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyCamDistanceScale", in_meta.object:GetCamDistanceScale());
			end,
		}),
		portraitZoom = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.PortraitZoom,
			fixed = false,


			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
         -- TODO: Уточнить патч, когда появилась анимированная иконка персонажа
            GGF.VersionSplitterProc({
               {
                  versionInterval = {GGD.TocVersionList.Vanilla, GGD.TocVersionList.WotLK},
                  action = function()
                  end,
               },
               {
                  versionInterval = {GGD.TocVersionList.Cata, GGD.TocVersionList.Invalid},
                  action = function()
				         in_meta.object:SetPortraitZoom(GGF.InnerInvoke(in_meta, "GetPortraitZoom"));
                  end,
               }
            });
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyPortraitZoom", in_meta.object:GetPortraitZoom());
			end,
		}),
		position = {
			X = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.PositionX,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustPosition");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizePosition");
				end,
			}),
			Y = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.PositionY,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustPosition");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizePosition");
				end,
			}),
			Z = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.PositionZ,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustPosition");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizePosition");
				end,
			}),
		},
	},
};

GGC.PlayerModel.elements = {};

GGF.Mark(GGC.PlayerModel);

GGC.PlayerModel = GGF.TableSafeExtend(GGC.PlayerModel, GGC.StandardModel);

GGF.CreateClassWorkaround(GGC.PlayerModel);

local This = GGC.PlayerModel;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-- Установка юнита для отображения
function This:SetUnit(in_meta, in_unitName)
	in_meta.object:SetUnit(in_unitName);
end

-- Установка юнита для отображения
function This:SetCamera(in_meta, in_index)
	in_meta.object:SetCamera(in_index);
end

-- Установка относительной позиции
function This:SetPosition(in_meta, in_x, in_y, in_z)
	GGF.InnerInvoke(in_meta, "ModifyPositionX", in_x);
	GGF.InnerInvoke(in_meta, "ModifyPositionY", in_y);
	GGF.InnerInvoke(in_meta, "ModifyPositionZ", in_z);
	GGF.InnerInvoke(in_meta, "AdjustPosition");
end

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-- Установка относительной позиции
function This:AdjustPosition(in_meta)
	local x, y, z = GGF.InnerInvoke(in_meta, "GetPositionX"), GGF.InnerInvoke(in_meta, "GetPositionY"), GGF.InnerInvoke(in_meta, "GetPositionZ");
	in_meta.object:SetPosition(x, y, z);
end

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-- Синхронизация относительной позиции
function This:SynchronizePosition(in_meta)
	local x, y, z = in_meta.object:GetPosition();
	GGF.InnerInvoke(in_meta, "ModifyPositionX", x);
	GGF.InnerInvoke(in_meta, "ModifyPositionY", y);
	GGF.InnerInvoke(in_meta, "ModifyPositionZ", z);
end

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

-- Сброс модели
function This:ClearModel(in_meta)
	in_meta.object:ClearModel();
end

--
function This:RotateCamera(in_meta, in_dyaw, in_dpitch)
	local distance, yaw, pitch = GGF.InnerInvoke(in_meta, "getCameraDistance");
	GGF.InnerInvoke(in_meta, "setCameraDistance", distance, yaw + in_dyaw, pitch + in_dpitch);
end

--
function This:getCameraDistance(in_meta)
	local x, y, z = in_meta.object:GetCameraPosition();
	local distance = math.sqrt(x * x + y * y + z * z);
	local yaw = - math.atan(y / x);
	local pitch = - math.atan(z / x);
	return distance, yaw, pitch;
end

--
function This:setCameraDistance(in_meta, in_distance, in_yaw, in_pitch)
	in_meta.object:SetCustomCamera(1);
	local x = in_distance * math.cos(in_yaw) * math.cos(in_pitch);
	local y = in_distance * math.sin(- in_yaw) * math.cos(in_pitch);
	local z = (in_distance * math.sin(- in_pitch));
	in_meta.object:SetCameraPosition(x, y, z);
end