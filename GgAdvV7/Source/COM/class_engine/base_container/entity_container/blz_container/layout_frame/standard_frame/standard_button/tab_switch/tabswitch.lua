-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "TS";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	TabSwitchSuffix = GGF.DockDomName(GGD.DefineIdentifier, "TabSwitchSuffix"),
	TabSwitch = GGF.DockDomName(GGD.ClassIdentifier, "TabSwitch"),
};

local CTList = {
	TabSwitch = GGF.CreateCT(OBJSuffix, "TabSwitch", "Frame"),
	GxName = GGF.CreateCT(OBJSuffix, "GxName", "GxName"),
};

GGF.GRegister(DFList.TabSwitchSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	GxName = "Tab",	-- TODO: уточнить!!!
}, GGE.RegValType.Default);


GGF.GRegister(DFList.TabSwitch, {
	meta = {
		name = "Tab Switch",
		mark = "",	-- авт.
		suffix = GGD.TabSwitchSuffix,
	},
});

GGC.TabSwitch.objects = {
	button = {
		tabSwitch = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.TabSwitch,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.TabSwitch.Default)) then
					--for tkn,obj in pairs(in_wrapper) do
						--if (string.find(tkn, "Get") ~= nil) then
							--print("tkn", tkn);
						--end
					--end
					local tabSwitch = CreateFrame(
						"Button",
						GGF.InnerInvoke(in_meta, "GetParentName") .. GGF.InnerInvoke(in_meta, "GetGxName"),
						GGF.InnerInvoke(in_meta, "GetParent"),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
         						return GGF.TernExpSingle(
         							GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
         							nil
         						)
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyTabSwitchOD", tabSwitch);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.TabSwitch.wrappers = {};

GGC.TabSwitch.properties = {
	miscellaneous = {
		gxname = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.GxName,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.TabSwitch.elements = {};

GGF.Mark(GGC.TabSwitch);

GGC.TabSwitch = GGF.TableSafeExtend(GGC.TabSwitch, GGC.StandardButton);

GGF.CreateClassWorkaround(GGC.TabSwitch);

local This = GGC.TabSwitch;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------