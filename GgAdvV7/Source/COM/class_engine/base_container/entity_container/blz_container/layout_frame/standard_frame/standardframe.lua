-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "SF";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	FrameStrataType = GGF.DockDomName(GGD.EnumerationIdentifier, "FrameStrataType"),
	StandardFrameSuffix = GGF.DockDomName(GGD.DefineIdentifier, "StandardFrameSuffix"),
	StandardFrame = GGF.DockDomName(GGD.ClassIdentifier, "StandardFrame"),
};

local CTList = {
	StandardFrame = GGF.CreateCT(OBJSuffix, "StandardFrame", "StandardFrame"),
	Alpha = GGF.CreateCT(OBJSuffix, "Alpha", "Alpha"),
	TopLevel = GGF.CreateCT(OBJSuffix, "TopLevel", "TopLevel"),
	Movable = GGF.CreateCT(OBJSuffix, "Movable", "Movable"),
	FrameStrata = GGF.CreateCT(OBJSuffix, "FrameStrata", "FrameStrata"),
	FrameLevelOffset = GGF.CreateCT(OBJSuffix, "FrameLevelOffset", "FrameLevelOffset"),
	ID = GGF.CreateCT(OBJSuffix, "ID", "ID"),
	TooltipText = GGF.CreateCT(OBJSuffix, "TooltipText", "TooltipText"),
	EnableMouse = GGF.CreateCT(OBJSuffix, "EnableMouse", "EnableMouse"),
	EnableKeyboard = GGF.CreateCT(OBJSuffix, "EnableKeyboard", "EnableKeyboard"),
	ClampedToScreen = GGF.CreateCT(OBJSuffix, "ClampedToScreen", "ClampedToScreen"),
	BGFile = GGF.CreateCT(OBJSuffix, "BGFile", "BGFile"),
	EdgeFile = GGF.CreateCT(OBJSuffix, "EdgeFile", "EdgeFile"),
	Tile = GGF.CreateCT(OBJSuffix, "Tile", "Tile"),
	EdgeSize = GGF.CreateCT(OBJSuffix, "EdgeSize", "EdgeSize"),
	TileSize = GGF.CreateCT(OBJSuffix, "TileSize", "TileSize"),
	BackdropInsetLeft = GGF.CreateCT(OBJSuffix, "BackdropInsetLeft", "BackdropInsetLeft"),
	BackdropInsetRight = GGF.CreateCT(OBJSuffix, "BackdropInsetRight", "BackdropInsetRight"),
	BackdropInsetTop = GGF.CreateCT(OBJSuffix, "BackdropInsetTop", "BackdropInsetTop"),
	BackdropInsetBottom = GGF.CreateCT(OBJSuffix, "BackdropInsetBottom", "BackdropInsetBottom"),
	BackdropColorR = GGF.CreateCT(OBJSuffix, "BackdropColorR", "BackdropColorR"),
	BackdropColorG = GGF.CreateCT(OBJSuffix, "BackdropColorG", "BackdropColorG"),
	BackdropColorB = GGF.CreateCT(OBJSuffix, "BackdropColorB", "BackdropColorB"),
	BackdropColorA = GGF.CreateCT(OBJSuffix, "BackdropColorA", "BackdropColorA"),
	BackdropBorderColorR = GGF.CreateCT(OBJSuffix, "BackdropBorderColorR", "BackdropBorderColorR"),
	BackdropBorderColorG = GGF.CreateCT(OBJSuffix, "BackdropBorderColorG", "BackdropBorderColorG"),
	BackdropBorderColorB = GGF.CreateCT(OBJSuffix, "BackdropBorderColorB", "BackdropBorderColorB"),
	BackdropBorderColorA = GGF.CreateCT(OBJSuffix, "BackdropBorderColorA", "BackdropBorderColorA"),
	HitRectInsetLeft = GGF.CreateCT(OBJSuffix, "HitRectInsetLeft", "HitRectInsetLeft"),
	HitRectInsetRight = GGF.CreateCT(OBJSuffix, "HitRectInsetRight", "HitRectInsetRight"),
	HitRectInsetTop = GGF.CreateCT(OBJSuffix, "HitRectInsetTop", "HitRectInsetTop"),
	HitRectInsetBottom = GGF.CreateCT(OBJSuffix, "HitRectInsetBottom", "HitRectInsetBottom"),
	OnEnter = GGF.CreateCT(OBJSuffix, "OnEnter", "OnMouseFocusFrame"),
	OnLeave = GGF.CreateCT(OBJSuffix, "OnLeave", "OnMouseDefocusFrame"),
	OnMouseDown = GGF.CreateCT(OBJSuffix, "OnMouseDown", "EmptyCallback"),
	OnMouseUp = GGF.CreateCT(OBJSuffix, "OnMouseUp", "EmptyCallback"),
	OnDragStop = GGF.CreateCT(OBJSuffix, "OnDragStop", "EmptyCallback"),
	OnFrameMouseDown = GGF.CreateCT(OBJSuffix, "OnFrameMouseDown", "OnFrameMouseDown"),
	OnFrameMouseUp = GGF.CreateCT(OBJSuffix, "OnFrameMouseUp", "OnFrameMouseUp"),
	OnFrameDragStop = GGF.CreateCT(OBJSuffix, "OnFrameDragStop", "OnFrameDragStop"),
	HackOffsetX = GGF.CreateCT(OBJSuffix, "HackOffsetX", "HackOffsetX"),
	HackOffsetY = GGF.CreateCT(OBJSuffix, "HackOffsetY", "HackOffsetY"),
	PointOffsetTable = GGF.CreateCT(OBJSuffix, "PointOffsetTable", "PointOffsetTable"),
};

GGF.GRegister(DFList.StandardFrameSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.FrameStrataType, {
	-- HACK: WORLD юзать нельзя(у близзов заблочено), т.ч. при этом варианте будет сброс через иф
	Default = "WORLD",
	Background = "BACKGROUND",
	Low = "LOW",
	Medium = "MEDIUM",
	High = "HIGH",
	Dialog = "DIALOG",
	Fullscreen = "FULLSCREEN",
	FullscreenDialog = "FULLSCREEN_DIALOG",
	Tooltip = "TOOLTIP",
});

GGF.GRegister(DFList.Default, {
	StandardFrame = 0,
	Alpha = 1.0,
	TopLevel = false,
	Movable = false,
	FrameStrata = GGE.FrameStrataType.Default,
	FrameLevelOffset = 0,	-- HACK: разность в 0 не должна быть, т.ч. будет сброс через иф
	ID = 0,
	EnableMouse = true,
	EnableKeyboard = true,
	ClampedToScreen = false,
	BGFile = "Interface\\DialogFrame\\Ui-DialogBox-Background",
	EdgeFile = "Interface\\DialogFrame\\Ui-DialogBox-Border",
	Tile = true,
	EdgeSize = 32,	-- TODO: подкорректировать с учетом среднего размера дефолтного окна
	TileSize = 32,
	BackdropInsetLeft = 11,
	BackdropInsetRight = 12,
	BackdropInsetTop = 12,
	BackdropInsetBottom = 11,
	BackdropColorR = 0.5,
	BackdropColorG = 0.5,
	BackdropColorB = 0.5,
	BackdropColorA = 1.0,
	BackdropBorderColorR = 0.5,
	BackdropBorderColorG = 0.5,
	BackdropBorderColorB = 0.5,
	BackdropBorderColorA = 1.0,
	HitRectInsetLeft = 0,
	HitRectInsetRight = 0,
	HitRectInsetTop = 0,
	HitRectInsetBottom = 0,
	TooltipText = function(in_meta)
		return in_meta.object:GetName();
	end,
	OnMouseFocusFrame = function(...) GGM.GUI.CommonService:OnMouseFocusFrame(...) end,
	OnMouseDefocusFrame = function(...) GGM.GUI.CommonService:OnMouseDefocusFrame(...) end,
	EmptyCallback = function(in_meta) end,
	OnFrameMouseDown = function(in_meta, in_self, ...)
		-- NOTE: проверка на true нужна, ибо иначе могёт упасть
		if (GGF.InnerInvoke(in_meta, "GetMovable") == true) then
			in_self:StartMoving();
			local pt, rt, rp, ox, oy = in_self:GetPoint();
			GGF.InnerInvoke(in_meta, "SetHackOffsetX", ox);
			GGF.InnerInvoke(in_meta, "SetHackOffsetY", oy);
			--print("ofmd:", ox, oy, pt, rt, rp);
			GGF.InnerInvoke(in_meta, "CounterGetOnMouseDown")(in_meta, in_self, ...);
		end
	end,
	OnFrameMouseUp = function(in_meta, in_self, ...)
		if (GGF.InnerInvoke(in_meta, "GetMovable") == true) then
			--GGF.InnerInvoke(in_meta, "SynchronizeOffsetX");
			--GGF.InnerInvoke(in_meta, "SynchronizeOffsetY");
			--print("onfmu:", GGF.InnerInvoke(in_meta, "GetOffsetX"), GGF.InnerInvoke(in_meta, "GetOffsetY"));
			in_self:StopMovingOrSizing();
			local pt, rt, rp, ox, oy = in_self:GetPoint();
			local correctorTable = GGF.InnerInvoke(in_meta, "GetPointOffsetTable")[pt];
			local hox = GGF.InnerInvoke(in_meta, "GetHackOffsetX");
			local hoy = GGF.InnerInvoke(in_meta, "GetHackOffsetY");
			--print("ofmu:", ox, oy, pt, rt, rp, correctorTable.x(in_meta), correctorTable.y(in_meta));
			--print("move:", ox - hox + correctorTable.x(in_meta), oy - hoy + correctorTable.y(in_meta));
			local deltaX = ox - hox + correctorTable.x(in_meta);
			local deltaY = oy - hoy + correctorTable.y(in_meta);
			GGF.InnerInvoke(in_meta, "ModifyOffsetX", GGF.InnerInvoke(in_meta, "GetOffsetX") + deltaX);
			GGF.InnerInvoke(in_meta, "ModifyOffsetY", GGF.InnerInvoke(in_meta, "GetOffsetY") + deltaY);
			GGF.InnerInvoke(in_meta, "CounterGetOnMouseUp")(in_meta, in_self, ...);
		end
	end,
	OnFrameDragStop = function(in_meta, in_self, ...)
		--GGF.InnerInvoke(in_meta, "SynchronizeOffsetX");
		--GGF.InnerInvoke(in_meta, "SynchronizeOffsetY");
		--print("onfds:", GGF.InnerInvoke(in_meta, "GetOffsetX"), GGF.InnerInvoke(in_meta, "GetOffsetY"));
		--GGF.InnerInvoke(in_meta, "CounterGetOnDragStop")(in_meta, ...);
	end,
	HackOffsetX = 0,
	HackOffsetY = 0,
	-- NOTE: Точка возврата крепления окна(которая приходит после мува)
	PointOffsetTable = {
		["TOP"] = {
			x = function(in_meta) return (GGM.FCS.RuntimeStorage:GetScreenWidth() - GGF.InnerInvoke(in_meta, "GetWidth"))/2.0 end,
			y = function(in_meta) return 0 end,
		},
		["BOTTOM"] = {
			x = function(in_meta) return (GGM.FCS.RuntimeStorage:GetScreenWidth() - GGF.InnerInvoke(in_meta, "GetWidth"))/2.0 end,
			y = function(in_meta) return - GGM.FCS.RuntimeStorage:GetScreenHeight() + GGF.InnerInvoke(in_meta, "GetHeight") end,
		},
		["LEFT"] = {
			x = function(in_meta) return 0 end,
			y = function(in_meta) return (- GGM.FCS.RuntimeStorage:GetScreenHeight() + GGF.InnerInvoke(in_meta, "GetHeight"))/2.0 end,
		},
		["RIGHT"] = {
			x = function(in_meta) return GGM.FCS.RuntimeStorage:GetScreenWidth() - GGF.InnerInvoke(in_meta, "GetWidth") end,
			y = function(in_meta) return (- GGM.FCS.RuntimeStorage:GetScreenHeight() + GGF.InnerInvoke(in_meta, "GetHeight"))/2.0 end,
		},
		["TOPLEFT"] = {
			x = function(in_meta) return 0 end,
			y = function(in_meta) return 0 end,
		},
		["TOPRIGHT"] = {
			x = function(in_meta) return GGM.FCS.RuntimeStorage:GetScreenWidth() - GGF.InnerInvoke(in_meta, "GetWidth") end,
			y = function(in_meta) return 0 end,
		},
		["BOTTOMLEFT"] = {
			x = function(in_meta) return 0 end,
			y = function(in_meta) return (- GGM.FCS.RuntimeStorage:GetScreenHeight() + GGF.InnerInvoke(in_meta, "GetHeight")) end,
		},
		["BOTTOMRIGHT"] = {
			x = function(in_meta) return GGM.FCS.RuntimeStorage:GetScreenWidth() - GGF.InnerInvoke(in_meta, "GetWidth") end,
			y = function(in_meta) return (- GGM.FCS.RuntimeStorage:GetScreenHeight() + GGF.InnerInvoke(in_meta, "GetHeight")) end,
		},
		["CENTER"] = {
			x = function(in_meta) return (GGM.FCS.RuntimeStorage:GetScreenWidth() - GGF.InnerInvoke(in_meta, "GetWidth"))/2.0 end,
			y = function(in_meta) return (- GGM.FCS.RuntimeStorage:GetScreenHeight() + GGF.InnerInvoke(in_meta, "GetHeight"))/2.0 end,
		},
	},
}, GGE.RegValType.Default);

GGF.GRegister(DFList.StandardFrame, {
	meta = {
		name = "Standard Frame",
		mark = "",	-- авт.
		suffix = GGD.StandardFrameSuffix,
	},
});

GGC.StandardFrame.objects = {
	frame = {
		standard = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.StandardFrame,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.StandardFrame.Default)) then
					local standardFrame = CreateFrame(
						"Frame",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
                  GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              --BackdropTemplateMixin and "BackdropTemplate" or nil
                              "BackdropTemplate" or nil
                           )
                        end,
                     },
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyStandardFrameOD", standardFrame);
					--print("standardframe cr:", standardFrame);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
		}),
	},
};

GGC.StandardFrame.wrappers = {};

GGC.StandardFrame.properties = {
	miscellaneous = {
		alpha = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Alpha,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetAlpha(GGF.InnerInvoke(in_meta, "GetAlpha"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyAlpha", in_meta.object:GetAlpha());
			end,
		}),
		topLevel = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.TopLevel,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetToplevel(GGF.InnerInvoke(in_meta, "GetTopLevel"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyTopLevel", in_meta.object:GetToplevel());
			end,
		}),
		movable = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Movable,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetMovable(GGF.InnerInvoke(in_meta, "GetMovable"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyMovable", in_meta.object:GetMovable());
			end,
		}),
		--resizable = false,
		frameStrata = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.FrameStrata,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local fs = GGF.InnerInvoke(in_meta, "GetFrameStrata");
				if (fs ~= GGF.GetDefaultValue(OBJSuffix, CTList.FrameStrata.Default)) then
					in_meta.object:SetFrameStrata(GGF.InnerInvoke(in_meta, "GetFrameStrata"));
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyFrameStrata", in_meta.object:GetFrameStrata());
			end,
		}),
		-- WARNING: этот параметр отвечает за разность с парентом, а не абсолютное значение!!!
		frameLevelOffset = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.FrameLevelOffset,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local fl = GGF.InnerInvoke(in_meta, "GetFrameLevelOffset");
				if (fl ~= GGF.GetDefaultValue(OBJSuffix, CTList.FrameLevelOffset.Default)) then
					in_meta.object:SetFrameLevel(fl + GGF.InnerInvoke(in_meta, "GetParent"):GetFrameLevel());
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				-- NOTE: нужен каскад, если уж так
				ATLASSERT(false);
			end,
		}),
		id = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ID,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local id = GGF.InnerInvoke(in_meta, "GetID");
				if (id ~= GGF.GetDefaultValue(OBJSuffix, CTList.ID.Default)) then
					in_meta.object:SetID(id);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyID", in_meta.object:GetID());
			end,
		}),
		tooltipText = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.TooltipText,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object.tooltip_text = GGF.InnerInvoke(in_meta, "GetTooltipText");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyTooltipText", in_meta.object.tooltip_text);
			end,
		}),
		enableMouse = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.EnableMouse,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local enableMouse = GGF.InnerInvoke(in_meta, "GetEnableMouse");
				in_meta.object:EnableMouse(enableMouse);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				ATLASSERT(false);
			end,
		}),
		enableKeyboard = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.EnableKeyboard,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local enableKeyboard = GGF.InnerInvoke(in_meta, "GetEnableKeyboard");
				in_meta.object:EnableKeyboard(enableKeyboard);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				ATLASSERT(false);
			end,
		}),
		clampedToScreen = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ClampedToScreen,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local clampedToScreen = GGF.InnerInvoke(in_meta, "GetClampedToScreen");
				in_meta.object:SetClampedToScreen(clampedToScreen);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyClampedToScreen", in_meta.object:GetClampedToScreen());
			end,
		}),
	},
	--protected = true;	-- только для близзовского кода..
	--jumpNavigateEnabled = false,	-- не работает
	--jumpNavigateStart = false,	-- не работает
	backdrop = {
		bgFile = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.BGFile,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
					GGF.InnerInvoke(in_meta, "AdjustBackdrop");
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
					GGF.InnerInvoke(in_meta, "SynchronizeBackdrop");
				end
			end,
		}),
		edgeFile = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.EdgeFile,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
					GGF.InnerInvoke(in_meta, "AdjustBackdrop");
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
					GGF.InnerInvoke(in_meta, "SynchronizeBackdrop");
				end
			end,
		}),
		tile = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Tile,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
					GGF.InnerInvoke(in_meta, "AdjustBackdrop");
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
					GGF.InnerInvoke(in_meta, "SynchronizeBackdrop");
				end
			end,
		}),
		edgeSize = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.EdgeSize,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
					GGF.InnerInvoke(in_meta, "AdjustBackdrop");
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
					GGF.InnerInvoke(in_meta, "SynchronizeBackdrop");
				end
			end,
		}),
		tileSize = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.TileSize,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
					GGF.InnerInvoke(in_meta, "AdjustBackdrop");
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
					GGF.InnerInvoke(in_meta, "SynchronizeBackdrop");
				end
			end,
		}),
		--alphaMode = 'BLEND',
		insets = {
			left = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.BackdropInsetLeft,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "AdjustBackdrop");
					end
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "SynchronizeBackdrop");
					end
				end,
			}),
			right = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.BackdropInsetRight,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "AdjustBackdrop");
					end
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "SynchronizeBackdrop");
					end
				end,
			}),
			top = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.BackdropInsetTop,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "AdjustBackdrop");
					end
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "SynchronizeBackdrop");
					end
				end,
			}),
			bottom = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.BackdropInsetBottom,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "AdjustBackdrop");
					end
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "SynchronizeBackdrop");
					end
				end,
			}),
			-- TODO: выяснить, что это такое
			--absolute = {
				--left = 0,
				--right = 0,
				--top = 0,
				--bottom = 0,
			--},
			--relative = {
				--left = 0,
				--right = 0,
				--top = 0,
				--bottom = 0,
			--},
		},
		color = {
			R = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.BackdropColorR,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "AdjustBackdrop");
					end
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "SynchronizeBackdrop");
					end
				end,
			}),
			G = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.BackdropColorG,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "AdjustBackdrop");
					end
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "SynchronizeBackdrop");
					end
				end,
			}),
			B = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.BackdropColorB,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "AdjustBackdrop");
					end
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "SynchronizeBackdrop");
					end
				end,
			}),
			A = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.BackdropColorA,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "AdjustBackdrop");
					end
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "SynchronizeBackdrop");
					end
				end,
			}),
		},
		borderColor = {
			R = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.BackdropBorderColorR,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "AdjustBackdrop");
					end
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "SynchronizeBackdrop");
					end
				end,
			}),
			G = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.BackdropBorderColorG,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "AdjustBackdrop");
					end
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "SynchronizeBackdrop");
					end
				end,
			}),
			B = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.BackdropBorderColorB,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "AdjustBackdrop");
					end
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "SynchronizeBackdrop");
					end
				end,
			}),
			A = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.BackdropBorderColorA,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "AdjustBackdrop");
					end
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsTemplated")) then
						GGF.InnerInvoke(in_meta, "SynchronizeBackdrop");
					end
				end,
			}),
		},
	},
	hitRectInsets = {
		-- WARNING: отломано
		left = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.HitRectInsetLeft,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local hitRectInsets = GGF.InnerInvoke(in_meta, "GetHitRectInsets");
				--in_object:GetObject():SetHitRectInsets(hitRectInsets.left, hitRectInsets.right, hitRectInsets.top, hitRectInsets.bottom);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		right = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.HitRectInsetRight,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local hitRectInsets = GGF.InnerInvoke(in_meta, "GetHitRectInsets");
				--in_object:GetObject():SetHitRectInsets(hitRectInsets.left, hitRectInsets.right, hitRectInsets.top, hitRectInsets.bottom);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		top = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.HitRectInsetTop,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local hitRectInsets = GGF.InnerInvoke(in_meta, "GetHitRectInsets");
				--in_object:GetObject():SetHitRectInsets(hitRectInsets.left, hitRectInsets.right, hitRectInsets.top, hitRectInsets.bottom);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		bottom = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.HitRectInsetBottom,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local hitRectInsets = GGF.InnerInvoke(in_meta, "GetHitRectInsets");
				--in_object:GetObject():SetHitRectInsets(hitRectInsets.left, hitRectInsets.right, hitRectInsets.top, hitRectInsets.bottom);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	callback = {
		onEnter = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnEnter,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local callback = GGF.InnerInvoke(in_meta, "CounterGetOnEnter");
				in_meta.object:SetScript("OnEnter", callback);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onLeave = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnLeave,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local callback = GGF.InnerInvoke(in_meta, "CounterGetOnLeave");
				in_meta.object:SetScript("OnLeave", callback);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onFrameMouseDown = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnFrameMouseDown,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetScript("OnMouseDown", function(...)
					GGF.InnerInvoke(in_meta, "CounterGetOnFrameMouseDown")(in_meta, ...);
				end);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onMouseDown = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnMouseDown,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onFrameMouseUp = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnFrameMouseUp,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetScript("OnMouseUp", function(...)
					GGF.InnerInvoke(in_meta, "CounterGetOnFrameMouseUp")(in_meta, ...);
				end);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onMouseUp = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnMouseUp,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onFrameDragStop = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnFrameDragStop,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetScript("OnDragStop", function(...)
					GGF.InnerInvoke(in_meta, "CounterGetOnFrameDragStop")(in_meta, ...);
				end);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onDragStop = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnDragStop,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.StandardFrame.elements = {
	movementHack = {
		offsetX = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.HackOffsetX,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		offsetY = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.HackOffsetY,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		pointOffsetTable = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.PointOffsetTable,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGF.Mark(GGC.StandardFrame);

GGC.StandardFrame = GGF.TableSafeExtend(GGC.StandardFrame, GGC.LayoutFrame);

GGF.CreateClassWorkaround(GGC.StandardFrame);

local This = GGC.StandardFrame;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-- Смена фона объекта
function This:ModifyBackdrop(in_meta, in_backdrop)
	if (in_backdrop ~= nil) then
		-- WARNING: опасно, может отвалиться
		GGF.InnerInvoke(in_meta, "ModifyBackdropBase", in_backdrop);
		GGF.InnerInvoke(in_meta, "ModifyBackdropInsets", in_backdrop.insets);
		GGF.InnerInvoke(in_meta, "ModifyBackdropColor", in_backdrop.color);
		GGF.InnerInvoke(in_meta, "ModifyBackdropBorderColor", in_backdrop.borderColor);
	end
end


-- Смена базы фона объекта
function This:ModifyBackdropBase(in_meta, in_backdropBase)
	if (in_backdropBase ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyBGFile", in_backdropBase.bgFile);
		GGF.InnerInvoke(in_meta, "ModifyEdgeFile", in_backdropBase.edgeFile);
		GGF.InnerInvoke(in_meta, "ModifyTile", in_backdropBase.tile);
		-- NOTE: пока что отломано
		--self:ModifyAlphaMode(in_backdropBase.alphaMode);
	end
end


-- Смена инсетов фона объекта
function This:ModifyBackdropInsets(in_meta, in_backdropInsets)
	if (in_backdropInsets ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyBackdropInsetLeft", in_backdropInsets.left);
		GGF.InnerInvoke(in_meta, "ModifyBackdropInsetRight", in_backdropInsets.right);
		GGF.InnerInvoke(in_meta, "ModifyBackdropInsetTop", in_backdropInsets.top);
		GGF.InnerInvoke(in_meta, "ModifyBackdropInsetBottom", in_backdropInsets.bottom);
	end
end


-- Смена цвета фона объекта
function This:ModifyBackdropColor(in_meta, in_backdropColor)
	if (in_backdropColor ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyBackdropColorR", in_backdropColor.R);
		GGF.InnerInvoke(in_meta, "ModifyBackdropColorG", in_backdropColor.G);
		GGF.InnerInvoke(in_meta, "ModifyBackdropColorB", in_backdropColor.B);
		GGF.InnerInvoke(in_meta, "ModifyBackdropColorA", in_backdropColor.A);
	end
end


-- Смена цвета границы фона объекта
function This:ModifyBackdropBorderColor(in_meta, in_backdropBorderColor)
	if (in_backdropBorderColor ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyBackdropBorderColorR", in_backdropBorderColor.R);
		GGF.InnerInvoke(in_meta, "ModifyBackdropBorderColorG", in_backdropBorderColor.G);
		GGF.InnerInvoke(in_meta, "ModifyBackdropBorderColorB", in_backdropBorderColor.B);
		GGF.InnerInvoke(in_meta, "ModifyBackdropBorderColorA", in_backdropBorderColor.A);
	end
end



-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Смена фона объекта
function This:SetBackdrop(in_meta, in_backdrop)
	GGF.InnerInvoke(in_meta, "ModifyBackdrop", in_backdrop);
	GGF.InnerInvoke(in_meta, "AdjustBackdrop");
end


-- Смена базы фона объекта
function This:SetBackdropBase(in_meta, in_backdropBase)
	GGF.InnerInvoke(in_meta, "ModifyBackdropBase", in_backdrop);
	GGF.InnerInvoke(in_meta, "AdjustBackdropBase");
end


-- Смена инсетов фона объекта
function This:SetBackdropInsets(in_meta, in_backdropInsets)
	GGF.InnerInvoke(in_meta, "ModifyBackdropInsets", in_backdropInsets);
	GGF.InnerInvoke(in_meta, "AdjustBackdropInsets");
end


-- Смена цвета фона объекта
function This:SetBackdropColor(in_meta, in_backdropColor)
	GGF.InnerInvoke(in_meta, "ModifyBackdropColor", in_backdropColor);
	GGF.InnerInvoke(in_meta, "AdjustBackdropColor");
end


-- Смена цвета границы фона объекта
function This:SetBackdropBorderColor(in_meta, in_backdropBorderColor)
	GGF.InnerInvoke(in_meta, "ModifyBackdropBorderColor", in_backdropBorderColor);
	GGF.InnerInvoke(in_meta, "AdjustBackdropBorderColor");
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие фона объекта
function This:GetBackdrop(in_meta)
	local backdrop = {
		insets = GGF.TableCopy(GGF.InnerInvoke(in_meta, "GetBackdropInsets")),
	};
	-- WARNING: костыль
	backdrop = GGF.TableSafeExtend(backdrop, GGF.InnerInvoke(in_meta, "GetBackdropBase"));
	--print("standard frame:", self:GetTemplate(), backdrop.bgFile, backdrop.edgeFile, backdrop.tile, backdrop.tileSize, backdrop.edgeSize);
	return backdrop;
end


-- Взятие базы фона объекта
function This:GetBackdropBase(in_meta)
	local backdropBase = {
		bgFile = GGF.InnerInvoke(in_meta, "GetBGFile"),
		edgeFile = GGF.InnerInvoke(in_meta, "GetEdgeFile"),
		tile = GGF.InnerInvoke(in_meta, "GetTile"),
		tileSize = GGF.InnerInvoke(in_meta, "GetTileSize"),
		edgeSize = GGF.InnerInvoke(in_meta, "GetEdgeSize"),
		-- TODO: выяснить, откуда взялось
		--alpha = self:GetAlphaMode(),
	};
	return backdropBase;
end


-- Взятие инсетов фона объекта
function This:GetBackdropInsets(in_meta)
	local backdropInsets = {
		left = GGF.InnerInvoke(in_meta, "GetBackdropInsetLeft"),
		right = GGF.InnerInvoke(in_meta, "GetBackdropInsetRight"),
		top = GGF.InnerInvoke(in_meta, "GetBackdropInsetTop"),
		bottom = GGF.InnerInvoke(in_meta, "GetBackdropInsetBottom"),
	};
	return backdropInsets;
end


-- Взятие цвета фона объекта
function This:GetBackdropColor(in_meta)
	local backdropColor = {
		R = GGF.InnerInvoke(in_meta, "GetBackdropColorR"),
		G = GGF.InnerInvoke(in_meta, "GetBackdropColorG"),
		B = GGF.InnerInvoke(in_meta, "GetBackdropColorB"),
		A = GGF.InnerInvoke(in_meta, "GetBackdropColorA"),
	};
	return backdropColor;
end


-- Взятие цвета границы фона объекта
function This:GetBackdropBorderColor(in_meta)
	local backdropBorderColor = {
		R = GGF.InnerInvoke(in_meta, "GetBackdropBorderColorR"),
		G = GGF.InnerInvoke(in_meta, "GetBackdropBorderColorG"),
		B = GGF.InnerInvoke(in_meta, "GetBackdropBorderColorB"),
		A = GGF.InnerInvoke(in_meta, "GetBackdropBorderColorA"),
	};
	return backdropBorderColor;
end


-- Взятие отступов по хитбоксам
function This:GetHitRectInsets(in_meta)
	local hitRectInsets = {
		left = GGF.InnerInvoke(in_meta, "GetHitRectInsetLeft"),
		right = GGF.InnerInvoke(in_meta, "GetHitRectInsetRight"),
		top = GGF.InnerInvoke(in_meta, "GetHitRectInsetTop"),
		bottom = GGF.InnerInvoke(in_meta, "GetHitRectInsetBottom"),
	};
	return hitRectInsets;
end


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Установка фона объекта(суммарная)
function This:AdjustBackdrop(in_meta)
   in_meta.object:SetBackdrop(GGF.InnerInvoke(in_meta, "GetBackdrop"));
   GGF.InnerInvoke(in_meta, "AdjustBackdropColor");
   GGF.InnerInvoke(in_meta, "AdjustBackdropBorderColor");
end

--
function This:AdjustBackdropColor(in_meta)
   local color = GGF.InnerInvoke(in_meta, "GetBackdropColor");
   in_meta.object:SetBackdropColor(color.R, color.G, color.B, color.A);
end

--
function This:AdjustBackdropBorderColor(in_meta)
   local borderColor = GGF.InnerInvoke(in_meta, "GetBackdropBorderColor");
   in_meta.object:SetBackdropBorderColor(borderColor.R, borderColor.G, borderColor.B, borderColor.A);
end


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-- Синхронизация фона объекта(суммарная)
function This:SynchronizeBackdrop(in_meta)
	ATLASSERT(false);
end


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------