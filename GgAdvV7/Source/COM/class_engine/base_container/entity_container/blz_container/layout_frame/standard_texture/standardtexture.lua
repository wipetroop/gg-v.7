-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


-- TODO: надо бы понять, что делать с альфа модом...(GG-56)
-- TODO: доввести стандартную текстуру(GG-56)

local OBJSuffix = "STX";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	StandardTextureSuffix = GGF.DockDomName(GGD.DefineIdentifier, "StandardTextureSuffix"),
	StandardTexture = GGF.DockDomName(GGD.ClassIdentifier, "StandardTexture"),
};

local CTList = {
	StandardTexture = GGF.CreateCT(OBJSuffix, "StandardTexture", "Texture"),
	File = GGF.CreateCT(OBJSuffix, "File", "File"),
	Layer = GGF.CreateCT(OBJSuffix, "Layer", "Layer"),
	ColorR = GGF.CreateCT(OBJSuffix, "ColorR", "ColorR"),
	ColorG = GGF.CreateCT(OBJSuffix, "ColorG", "ColorG"),
	ColorB = GGF.CreateCT(OBJSuffix, "ColorB", "ColorB"),
	ColorA = GGF.CreateCT(OBJSuffix, "ColorA", "ColorA"),
};

GGF.GRegister(DFList.StandardTextureSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Texture = 0,
	-- WARNING: не менять 0 на ""
	File = 0,		-- NOTE: также является аргументом сброса текстуры
	Layer = GGE.LayerType.Artwork,
	ColorR = 0.5,
	ColorG = 0.5,
	ColorB = 0.5,
	ColorA = 1.0,
}, GGE.RegValType.Default);

-- TODO: придумать текстуру по умолчанию(на самом деле хороший чек)(GG-56)
GGF.GRegister(DFList.StandardTexture, {
	meta = {
		name = "Standard Texture",
		mark = "",	-- авт.
		suffix = GGD.StandardTextureSuffix,
	},
});

GGC.StandardTexture.objects = {
	frame = {
		texture = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.StandardTexture,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.StandardTexture.Default)) then
					local texture = GGF.InnerInvoke(in_meta, "GetParent"):CreateTexture(
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetLayer"),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyStandardTextureOD", texture);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.StandardTexture.wrappers = {};

GGC.StandardTexture.properties = {
	miscellaneous = {
		file = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.File,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local file = GGF.InnerInvoke(in_meta, "GetFile");
				if (file ~= GGF.GetDefaultValue(OBJSuffix, CTList.File.Default)) then
					in_meta.object:SetTexture(file);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeColor");
			end,
		}),
		layer = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Layer,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		color = {
			R = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ColorR,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeColor");
				end,
			}),
			G = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ColorG,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeColor");
				end,
			}),
			B = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ColorB,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeColor");
				end,
			}),
			A = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ColorA,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeColor");
				end,
			}),
		},
	},
};

GGC.StandardTexture.elements = {};

GGF.Mark(GGC.StandardTexture);

GGC.StandardTexture = GGF.TableSafeExtend(GGC.StandardTexture, GGC.LayoutFrame);

GGF.CreateClassWorkaround(GGC.StandardTexture);

local This = GGC.StandardTexture;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-- Изменение цвета текстуры
function This:ModifyColor(in_meta, in_color)
	if (in_color) then
		GGF.InnerInvoke(in_meta, "ModifyColorR", in_color.R);
		GGF.InnerInvoke(in_meta, "ModifyColorG", in_color.G);
		GGF.InnerInvoke(in_meta, "ModifyColorB", in_color.B);
		GGF.InnerInvoke(in_meta, "ModifyColorA", in_color.A);
	end
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Установка цвета текстуры
function This:SetColor(in_meta, in_color)
	if (in_color) then
		GGF.InnerInvoke(in_meta, "ModifyColor", in_color);
		GGF.InnerInvoke(in_meta, "AdjustColor");
	end
end

-- Установка координат графического объекта внутри фрейма
function This:SetTexCoord(in_meta, in_c1, in_c2, in_c3, in_c4)
   in_meta.object:SetTexCoord(in_c1, in_c2, in_c3, in_c4);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие цвета текстуры
function This:GetColor(in_meta)
	local color = {
		R = GGF.InnerInvoke(in_meta, "GetColorR"),
		G = GGF.InnerInvoke(in_meta, "GetColorG"),
		B = GGF.InnerInvoke(in_meta, "GetColorB"),
		A = GGF.InnerInvoke(in_meta, "GetColorA"),
	};
	return color;
end


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Применение цвета текстуры
function This:AdjustColor(in_meta)
	local texclr = GGF.InnerInvoke(in_meta, "GetColor");
	if (GGF.InnerInvoke(in_meta, "GetFile") ~= GGF.GetDefaultValue(OBJSuffix, CTList.File.Default)) then
		return;
	end
	-- TODO: выяснить, при каких обстоятельствах тут nil может быть
	if (texclr ~= 0) then
		GGF.VersionSplitterProc({
			{
				versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.Draenor },
				action = function()
					-- NOTE: до 7.0.3
					in_meta.object:SetTexture(texclr.R, texclr.G, texclr.B, texclr.A);
				end,
			},
			{
				versionInterval = { GGD.TocVersionList.Legion, GGD.TocVersionList.Invalid },
				action = function()
					in_meta.object:SetColorTexture(texclr.R, texclr.G, texclr.B, texclr.A);
				end,
			},
		});
	end
end


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-- Синхронизация цвета текстуры
function This:SynchronizeColor(in_meta)
	-- NOTE: походу, невозможно вытащить цвет просто так....
	ATLASSERT(false);
end


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Сброс текущей текстуры
function This:DropTexture(in_meta)
	in_meta.object:SetTexture(GGF.GetDefaultValue(GGF.InnerInvoke(in_meta, "getObjSuffix"), CTList.File.Default));
end