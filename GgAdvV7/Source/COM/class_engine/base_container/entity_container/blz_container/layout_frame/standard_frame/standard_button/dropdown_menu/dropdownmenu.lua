-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "DDM";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	DropdownMenuSuffix = GGF.DockDomName(GGD.DefineIdentifier, "DropdownMenuSuffix"),
	DropdownMenu = GGF.DockDomName(GGD.ClassIdentifier, "DropdownMenu"),
};

local CTList = {
	DropdownMenuButton = GGF.CreateCT(OBJSuffix, "DropdownMenuButton", "Frame"),
	DropdownMenuFrameWrapper = GGF.CreateCT(OBJSuffix, "DropdownMenuFrameWrapper", "Frame"),
	DataSet = GGF.CreateCT(OBJSuffix, "DataSet", "DataSet"),
   OnSelect = GGF.CreateCT(OBJSuffix, "OnSelect", "EmptyCallback"),
};

GGF.GRegister(DFList.DropdownMenuSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	DataSet = {},	-- NOTE: пустой
   EmptyCallback = function(in_meta)
      print("ddm empty callback");
   end,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.DropdownMenu, {
	meta = {
		name = "Dropdown Menu",
		mark = "",	-- авт.
		suffix = GGD.DropdownMenuSuffix,
	},
});

GGC.DropdownMenu.objects = {
	frame = {
		dropdownMenu = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.DropdownMenuButton,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.DropdownMenuButton.Default)) then
					local standardButton = CreateFrame(
						"Button",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
						"UIDropDownMenuTemplate"
                  --"UIPanelButtonTemplate"
					);
					GGF.InnerInvoke(in_meta, "ModifyDropdownMenuButtonOD", standardButton);
					-- TODO: check http://wowwiki.wikia.com/wiki/using_UIDropDownMenu
					--in_wrapper:ModifyDropdownMenuFrame();

					--GGF.InnerInvoke(in_meta, "SetOnClick", function()
               UIDropDownMenu_SetWidth(standardButton, 310);
					--GGF.InnerInvoke(in_meta, "ModifyOnClick", function()
						--print("mod onclick", in_meta);
						--GGF.InnerInvoke(in_meta, "dropDownToggle");
					--end);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.DropdownMenu.wrappers = {
	frame = {
		dropDown = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Element,
			cte = CTList.DropdownMenuFrameWrapper,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.DropdownMenu.properties = {
	hardware = {
		dataSet = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.DataSet,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	callback = {
		-- NOTE: не выставлять вне объекта!!!
		--onClick = GGC.ParameterContainer:Create({
			--ptype = GGE.ParameterType.Function,
			--ctype = GGE.ContainerType.Property,
			--token = "OnClick",
			--fixed = false,
			--parameter = {
				--func = function() end,
			--},

			--[GGE.RuntimeMethod.Adjuster] = function(in_wrapper, in_object)
				--in_object:SetScript("OnClick", in_wrapper:GetOnClickCounter());
			--end,
		--}),
      onSelect = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.OnSelect,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            --local callback = GGF.InnerInvoke(in_meta, "CounterGetOnClick");
            --in_meta.object:SetScript("OnClick", callback);
            --print("adjust onclick", in_meta);
            --print("onclck cb:", GGF.InnerInvoke(in_meta, "GetWrapperName"));
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
	},
};

GGC.DropdownMenu.elements = {};

GGF.Mark(GGC.DropdownMenu);

GGC.DropdownMenu = GGF.TableSafeExtend(GGC.DropdownMenu, GGC.StandardButton);
GGC.DropdownMenu.properties.callback.onClick = nil;

GGF.CreateClassWorkaround(GGC.DropdownMenu);

local This = GGC.DropdownMenu;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

storePrev = 1;

-- Установка основных объектов дропдауна
function This:adjustServiceObjects(in_meta)
	--GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetDropdownMenuFrameWrapper"), "Adjust");
   --GGF.InnerInvoke(in_meta, "AdjustOnClick");
	-- WARNING: может ебануть...но не должно...
	GGF.InnerInvoke(in_meta, "refresh");
end


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Создание основных объектов дропдауна
function This:createServiceObjectsB(in_meta)
	GGF.InnerInvoke(in_meta, "SetDropdownMenuFrameWrapperOD", GGC.StandardFrame:Create({}, {
		properties = {
			base = {
				parent = GGF.InnerInvoke(in_meta, "GetParent"),
				template = "UIDropdownMenuTemplate",
				wrapperName = "DDM",
			},
			miscellaneous = {
				hidden = false,
				movable = true,
				frameStrata = GGE.FrameStrataType.Default,
				frameLevelOffset = 0,
				id = 0,
				enableMouse = true,
				enableKeyboard = false,
				clampedToScreen = true,

				-- TODO: узнать, что это за свойства
				--highlight = false,
			},
			--Object = CreateFrame("Frame", self:GetObjectName() .. "_DDM_F", self:GetObject(), "UiGGC.DropdownMenuTemplate"),
			backdrop = emptyFrameBackdrop,
			size = {
				width = 122,
				height = 29,
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetDropdownMenuButton")),
				relativePoint = GGE.PointType.BottomLeft,
				offsetX = 0,
				offsetY = 0,
			},
		},
	}));
end

function This:init(in_meta)
   UIDropDownMenu_Initialize(
      GGF.InnerInvoke(in_meta, "GetDropdownMenuButton"),
      function(in_frame, in_level, in_menuList)
         --GGF.InnerInvoke(in_meta, "dropDownLoad", in_instance, in_level, in_menuList)
         GGF.InnerInvoke(in_meta, "dropDown", in_instance, in_level, in_menuList)
      end--,
      --"MENU"
   );
end

function This:refresh(in_meta, in_onselect)
   print("refresh");
   if not in_onselect then
      GGF.InnerInvoke(in_meta, "init");
   end
   UIDropDownMenu_SetSelectedID(
      GGF.InnerInvoke(in_meta, "GetDropdownMenuButton"),
      storePrev
   );
end

function This:onSelect(in_meta, in_obj)
   storePrev = in_obj:GetID();
   GGF.InnerInvoke(in_meta, "refresh", true);
end

function This:dropDown(in_meta, in_level, in_menuList)
   local dataSet = GGF.InnerInvoke(in_meta, "GetDataSet");
   print("dropDown", getn(dataSet));
   for index, data in ipairs(dataSet) do
      info = {
         text = data.description,   --"This is an option in the menu.";
         value = data.value,        --"OptionVariable";
         func = function(obj)
            --print(data.description, data.value)
            GGF.InnerInvoke(in_meta, "onSelect", obj);
         end,  --FunctionCalledWhenOptionIsClicked 
      };
      -- can also be done as function() FunctionCalledWhenOptionIsClicked() end;
      -- Add the above information to the options menu as a button.
      UIDropDownMenu_AddButton(info);
   end
end


-- Коллбек загрузки(стадия инициализации)
function This:dropDownLoad(in_meta, in_level, in_menuList)
	local dataSet = GGF.InnerInvoke(in_meta, "GetDataSet");
   local onSelect = GGF.InnerInvoke(in_meta, "CounterGetOnSelect");

	for index, data in pairs(dataSet) do
		info = {
			text = data.description,	--"This is an option in the menu.";
			value = data.value,			--"OptionVariable";
			func = function() 
            print(data.description, data.value);
            onSelect(data.value);
         end,	--FunctionCalledWhenOptionIsClicked 
		};
		-- can also be done as function() FunctionCalledWhenOptionIsClicked() end;
		-- Add the above information to the options menu as a button.
		UIDropDownMenu_AddButton(info);
	end
end


-- Коллбек expand/collapse
function This:dropDownToggle(in_meta)
	print("ddm toggle");
	ToggleDropDownMenu(1, nil, GGF.InnerInvoke(in_meta, "GetDropdownMenuFrameWrapper"):GetStandardFrame(), GGF.InnerInvoke(in_meta, "GetDropdownMenuButton"), 0, 0);
end