-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "LF";

local DFList = {
	AnchorType = GGF.DockDomName(GGD.EnumerationIdentifier, "AnchorType"),
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	LayoutFrameSuffix = GGF.DockDomName(GGD.DefineIdentifier, "LayoutFrameSuffix"),
	LayoutFrame = GGF.DockDomName(GGD.ClassIdentifier, "LayoutFrame"),
};

local CTList = {
	Frame = GGF.CreateCT(OBJSuffix, "LayoutFrame", "Frame"),
	Template = GGF.CreateCT(OBJSuffix, "Template", "Template"),
	--Suffix = GGF.CreateCT(OBJSuffix, "Suffix", "Suffix"),
	Virtual = GGF.CreateCT(OBJSuffix, "Virtual", "Virtual"),
	Hidden = GGF.CreateCT(OBJSuffix, "Hidden", "Hidden"),
	AnchorType = GGF.CreateCT(OBJSuffix, "AnchorType", "AnchorType"),
	Width = GGF.CreateCT(OBJSuffix, "Width", "Width"),
	Height = GGF.CreateCT(OBJSuffix, "Height", "Height"),
	Point = GGF.CreateCT(OBJSuffix, "Point", "Point"),
	RelativeTo = GGF.CreateCT(OBJSuffix, "RelativeTo", "RelativeTo"),
	RelativePoint = GGF.CreateCT(OBJSuffix, "RelativePoint", "RelativePoint"),
	OffsetX = GGF.CreateCT(OBJSuffix, "OffsetX", "OffsetX"),
	OffsetY = GGF.CreateCT(OBJSuffix, "OffsetY", "OffsetY"),
	SecondPoint = GGF.CreateCT(OBJSuffix, "SecondPoint", "Point"),
	SecondRelativeTo = GGF.CreateCT(OBJSuffix, "SecondRelativeTo", "RelativeTo"),
	SecondRelativePoint = GGF.CreateCT(OBJSuffix, "SecondRelativePoint", "RelativePoint"),
	SecondOffsetX = GGF.CreateCT(OBJSuffix, "SecondOffsetX", "OffsetX"),
	SecondOffsetY = GGF.CreateCT(OBJSuffix, "SecondOffsetY", "OffsetY"),
	ThirdPoint = GGF.CreateCT(OBJSuffix, "ThirdPoint", "Point"),
	ThirdRelativeTo = GGF.CreateCT(OBJSuffix, "ThirdRelativeTo", "RelativeTo"),
	ThirdRelativePoint = GGF.CreateCT(OBJSuffix, "ThirdRelativePoint", "RelativePoint"),
	ThirdOffsetX = GGF.CreateCT(OBJSuffix, "ThirdOffsetX", "OffsetX"),
	ThirdOffsetY = GGF.CreateCT(OBJSuffix, "ThirdOffsetY", "OffsetY"),
};

GGF.GRegister(DFList.LayoutFrameSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.AnchorType, {
	Single = 'single',
	Double = 'double',
	Triple = 'triple',
});

GGF.GRegister(DFList.Default, {
	Frame = 0,
	Template = 0,
	--Suffix = "OBJ",
	Virtual = false,
	Hidden = false,
	AnchorType = GGE.AnchorType.Single,
	Width = 800,
	Height = 200,
	Point = GGE.PointType.TopLeft,
	RelativeTo = function(in_meta)
		return UIParent;
	end,
	RelativePoint = GGE.PointType.TopLeft,
	OffsetX = 0,
	OffsetY = 0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.LayoutFrame, {
	meta = {
		name = "Layout Frame",
		mark = "",	-- авт.
		suffix = GGD.LayoutFrameSuffix,
	},
});

GGC.LayoutFrame.objects = {
	frame = {
		layout = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.Frame,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.Frame.Default)) then
					local layoutFrame = CreateFrame(
						"Frame",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyLayoutFrameOD", layoutFrame);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.LayoutFrame.wrappers = {};

GGC.LayoutFrame.properties = {
	base = {
		template = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Template,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		--suffix = GGC.ParameterContainer:Create({
			--ptype = GGE.ParameterType.Value,
			--ctype = GGE.ContainerType.Property,
			--token = CTList.Suffix.Token,
			--fixed = false,
			--parameter = {
				---- NOTE: нужен для генерации имени близзовского объекта
				--value = GGF.GetDefaultValue(OBJSuffix, CTList.Suffix.Default),
			--},

			--[GGE.RuntimeMethod.Adjuster] = function(in_wrapper, in_object)
				--ATLASSERT(in_wrapper:getClassSuffix() == in_wrapper:getClassSuffix());
			--end,
		--}),
	},

	miscellaneous = {
		virtual = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Virtual,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		hidden = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Hidden,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (GGF.InnerInvoke(in_meta, "GetHidden")) then
					--for tkn,obj in pairs(in_object) do
						--print("tkn:", tkn);
					--end
					--print("layoutframe hd:", in_wrapper, in_object);
					in_meta.object:Hide();
				else
					in_meta.object:Show();
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyHidden", in_meta.object:IsHidden());
			end,
		}),
		anchorType = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.AnchorType,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},

	size = {
		width = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Width,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustSize");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeSize");
			end,
		}),
		height = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Height,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustSize");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeSize");
			end,
		}),
	},

	anchor = {
		point = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Point,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeAnchor");
			end,
		}),
		relativeTo = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.RelativeTo,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeAnchor");
			end,
		}),
		relativePoint = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.RelativePoint,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeAnchor");
			end,
		}),
		offsetX = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OffsetX,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeAnchor");
			end,
		}),
		offsetY = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OffsetY,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeAnchor");
			end,
		}),
	},
	secondAnchor = {
		point = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.SecondPoint,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeAnchor");
			end,
		}),
		relativeTo = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.SecondRelativeTo,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeAnchor");
			end,
		}),
		relativePoint = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.SecondRelativePoint,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeAnchor");
			end,
		}),
		offsetX = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.SecondOffsetX,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeAnchor");
			end,
		}),
		offsetY = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.SecondOffsetY,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeAnchor");
			end,
		}),
	},
	thirdAnchor = {
		point = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ThirdPoint,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeAnchor");
			end,
		}),
		relativeTo = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ThirdRelativeTo,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeAnchor");
			end,
		}),
		relativePoint = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ThirdRelativePoint,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeAnchor");
			end,
		}),
		offsetX = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ThirdOffsetX,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeAnchor");
			end,
		}),
		offsetY = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ThirdOffsetY,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeAnchor");
			end,
		}),
	},
};

GGC.LayoutFrame.elements = {};

GGF.Mark(GGC.LayoutFrame);

GGC.LayoutFrame = GGF.TableSafeExtend(GGC.LayoutFrame, GGC.BlzContainer);

GGF.CreateClassWorkaround(GGC.LayoutFrame);

local This = GGC.LayoutFrame;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-- Смена габаритов объекта
function This:ModifySize(in_meta, in_sz)
	if (in_sz ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyWidth", in_sz.width);
		GGF.InnerInvoke(in_meta, "ModifyHeight", in_sz.height);
	end
end


-- Смена геометрической точки привязки(якоря) объектов
function This:ModifyAnchor(in_meta, in_an)
	if (in_an ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyPoint", in_an.point);
		GGF.InnerInvoke(in_meta, "ModifyRelativeTo", in_an.relativeTo);
		GGF.InnerInvoke(in_meta, "ModifyRelativePoint", in_an.relativePoint);
		GGF.InnerInvoke(in_meta, "ModifyOffsetX", in_an.offsetX);
		GGF.InnerInvoke(in_meta, "ModifyOffsetY", in_an.offsetY);
		if (GGF.InnerInvoke(in_meta, "GetAnchorType") == GGE.AnchorType.Double
			or GGF.InnerInvoke(in_meta, "GetAnchorType") == GGE.AnchorType.Triple) then
			GGF.InnerInvoke(in_meta, "ModifySecondPoint", in_an.secondPoint);
			GGF.InnerInvoke(in_meta, "ModifySecondRelativeTo", in_an.secondRelativeTo);
			GGF.InnerInvoke(in_meta, "ModifySecondRelativePoint", in_an.secondRelativePoint);
			GGF.InnerInvoke(in_meta, "ModifySecondOffsetX", in_an.secondOffsetX);
			GGF.InnerInvoke(in_meta, "ModifySecondOffsetY", in_an.secondOffsetY);
		end
		if (GGF.InnerInvoke(in_meta, "GetAnchorType") == GGE.AnchorType.Triple) then
			GGF.InnerInvoke(in_meta, "ModifyThirdPoint", in_an.thirdPoint);
			GGF.InnerInvoke(in_meta, "ModifyThirdRelativeTo", in_an.thirdRelativeTo);
			GGF.InnerInvoke(in_meta, "ModifyThirdRelativePoint", in_an.thirdRelativePoint);
			GGF.InnerInvoke(in_meta, "ModifyThirdOffsetX", in_an.thirdOffsetX);
			GGF.InnerInvoke(in_meta, "ModifyThirdOffsetY", in_an.thirdOffsetY);
		end
	end
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Смена габаритов объекта
function This:SetSize(in_meta, in_sz)
	GGF.InnerInvoke(in_meta, "ModifySize", in_sz);
	GGF.InnerInvoke(in_meta, "AdjustSize");
end


-- Смена геометрической точки привязки(якоря) объектов
function This:SetAnchor(in_meta, in_an)
	GGF.InnerInvoke(in_meta, "ModifyAnchor", in_an);
	GGF.InnerInvoke(in_meta, "AdjustAnchor");
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие габаритов объекта
function This:GetSize(in_meta)
	local size = {
		width = GGF.InnerInvoke(in_meta, "GetWidth"),
		height = GGF.InnerInvoke(in_meta, "GetHeight"),
	};
	return size;
end


-- Взятие точки привязки(якоря) объекта
function This:GetAnchor(in_meta)
	local anchor = {
		point = GGF.InnerInvoke(in_meta, "GetPoint"),
		relativeTo = GGF.InnerInvoke(in_meta, "GetRelativeTo"),
		relativePoint = GGF.InnerInvoke(in_meta, "GetRelativePoint"),
		offsetX = GGF.InnerInvoke(in_meta, "GetOffsetX"),
		offsetY = GGF.InnerInvoke(in_meta, "GetOffsetY"),

		secondPoint = GGF.InnerInvoke(in_meta, "GetSecondPoint"),
		secondRelativeTo = GGF.InnerInvoke(in_meta, "GetSecondRelativeTo"),
		secondRelativePoint = GGF.InnerInvoke(in_meta, "GetSecondRelativePoint"),
		secondOffsetX = GGF.InnerInvoke(in_meta, "GetSecondOffsetX"),
		secondOffsetY = GGF.InnerInvoke(in_meta, "GetSecondOffsetY"),

		thirdPoint = GGF.InnerInvoke(in_meta, "GetThirdPoint"),
		thirdRelativeTo = GGF.InnerInvoke(in_meta, "GetThirdRelativeTo"),
		thirdRelativePoint = GGF.InnerInvoke(in_meta, "GetThirdRelativePoint"),
		thirdOffsetX = GGF.InnerInvoke(in_meta, "GetThirdOffsetX"),
		thirdOffsetY = GGF.InnerInvoke(in_meta, "GetThirdOffsetY"),
	};
	return anchor;
end


-- Взятие факта шаблонности фрейма
function This:IsTemplated(in_meta)
	return GGF.InnerInvoke(in_meta, "GetTemplate") ~= GGF.GetDefaultValue(OBJSuffix, CTList.Template.Default);
end


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Установка габаритов объекта
function This:AdjustSize(in_meta)
	local size = GGF.InnerInvoke(in_meta, "GetSize");
	in_meta.object:SetSize(size.width, size.height);
end


-- Установка точки привязки исходного объекта
function This:AdjustAnchor(in_meta)
	--print("layout anch adjust");
	local anchor = GGF.InnerInvoke(in_meta, "GetAnchor");
	in_meta.object:ClearAllPoints();
	in_meta.object:SetPoint(anchor.point, anchor.relativeTo, anchor.relativePoint, anchor.offsetX, anchor.offsetY);
	if (GGF.InnerInvoke(in_meta, "GetAnchorType") == GGE.AnchorType.Double
		or GGF.InnerInvoke(in_meta, "GetAnchorType") == GGE.AnchorType.Triple) then
		in_meta.object:SetPoint(anchor.secondPoint, anchor.secondRelativeTo, anchor.secondRelativePoint, anchor.secondOffsetX, anchor.secondOffsetY);
	end
	if (GGF.InnerInvoke(in_meta, "GetAnchorType") == GGE.AnchorType.Triple) then
		in_meta.object:SetPoint(anchor.thirdPoint, anchor.thirdRelativeTo, anchor.thirdRelativePoint, anchor.thirdOffsetX, anchor.thirdOffsetY);
	end
end


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-- Синхронизация габаритов объекта
function This:SynchronizeSize(in_meta)
	local width, height = in_meta.object:GetSize();
	GGF.InnerInvoke(in_meta, "ModifySize", {
		width = width,
		height = height,
	});
end


-- Синхронизация точки привязки исходного объекта
function This:SynchronizeAnchor(in_meta)
	local pt1, rt1, rp1, ox1, oy1;
	--if (GGF.InnerInvoke(in_meta, "GetAnchorType") == GGE.AnchorType.Single) then
		--pt1, rt1, rp1, ox1, oy1 = in_meta.object:GetPoint();
	--else
		pt1, rt1, rp1, ox1, oy1 = in_meta.object:GetPoint(1);
	--end
	print("anch sync obj:", self, pt1, rt1, rp1, ox1, oy1);
	print("anch sync wrp:", self, GGF.InnerInvoke(in_meta, "GetPoint"), GGF.InnerInvoke(in_meta, "GetRelativeTo"), GGF.InnerInvoke(in_meta, "GetRelativePoint"), GGF.InnerInvoke(in_meta, "GetOffsetX"), GGF.InnerInvoke(in_meta, "GetOffsetY"));
	local point = {
		point = pt1,
		relativeTo = rt1,
		relativePoint = rp1,
		offsetX = ox1,
		offsetY = oy1,
	};
	if (GGF.InnerInvoke(in_meta, "GetAnchorType") == GGE.AnchorType.Double
		or GGF.InnerInvoke(in_meta, "GetAnchorType") == GGE.AnchorType.Triple) then
		local pt2, rt2, rp2, ox2, oy2 = in_meta.object:GetPoint(2);
		point.secondPoint = pt2;
		point.secondRelativeTo = rt2;
		point.secondRelativePoint = rp2;
		point.secondOffsetX = ox2;
		point.secondOffsetY = oy2;
	end
	if (GGF.InnerInvoke(in_meta, "GetAnchorType") == GGE.AnchorType.Triple) then
		local pt3, rt3, rp3, ox3, oy3 = in_meta.object:GetPoint(3);
		point.thirdPoint = pt3;
		point.thirdRelativeTo = rt3;
		point.thirdRelativePoint = rp3;
		point.thirdOffsetX = ox3;
		point.thirdOffsetY = oy3;
	end
	GGF.InnerInvoke(in_meta, "ModifyAnchor", point);
end


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------