-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "SS";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	SliderOrientation = GGF.DockDomName(GGD.EnumerationIdentifier, "SliderOrientation"),
	StandardSliderSuffix = GGF.DockDomName(GGD.DefineIdentifier, "StandardSliderSuffix"),
	StandardSlider = GGF.DockDomName(GGD.ClassIdentifier, "StandardSlider"),
};

local CTList = {
	StandardSlider = GGF.CreateCT(OBJSuffix, "StandardSlider", "Frame"),
	Layer = GGF.CreateCT(OBJSuffix, "Layer", "Layer"),
	Enabled = GGF.CreateCT(OBJSuffix, "Enabled", "Enabled"),
	Value = GGF.CreateCT(OBJSuffix, "Value", "Value"),
	Orientation = GGF.CreateCT(OBJSuffix, "Orientation", "Orientation"),
	ThumbTexture = GGF.CreateCT(OBJSuffix, "ThumbTexture", "ThumbTexture"),
	ValuesMin = GGF.CreateCT(OBJSuffix, "ValuesMin", "ValuesMin"),
	ValuesMax = GGF.CreateCT(OBJSuffix, "ValuesMax", "ValuesMax"),
	ValuesStep = GGF.CreateCT(OBJSuffix, "ValuesStep", "ValuesStep"),
	ValuesStepsPerPage = GGF.CreateCT(OBJSuffix, "ValuesStepsPerPage", "ValuesStepsPerPage"),
	OnValueChanged = GGF.CreateCT(OBJSuffix, "OnValueChanged", "EmptyCallback"),
	OnSliderValueChanged = GGF.CreateCT(OBJSuffix, "OnSliderValueChanged", "OnSliderValueChanged"),
	OnMouseUp = GGF.CreateCT(OBJSuffix, "OnMouseUp", "EmptyCallback"),
	OnMouseWheel = GGF.CreateCT(OBJSuffix, "OnMouseWheel", "EmptyCallback"),
};

GGF.GRegister(DFList.StandardSliderSuffix, OBJSuffix, GGE.RegValType.Suffix);

-- NOTE: вроде бы какая-то из них не работает
GGF.GRegister(DFList.SliderOrientation, {
	Vertical = "VERTICAL",
	Horizontal = "HORIZONTAL",
});

GGF.GRegister(DFList.Default, {
	Frame = 0,
	Layer = GGE.LayerType.Artwork,
	Enabled = true,
	Value = 0.0,
	Orientation = GGE.SliderOrientation.Horizontal,
	ThumbTexture = "Interface\\Buttons\\UI-SliderBar-Button-Horizontal",
	ValuesMin = 0.0,
	ValuesMax = 10.0,
	ValuesStep = 1.0,
	-- NOTE: 0 - значит отключен
	ValuesStepsPerPage = 0.0,
	OnSliderValueChanged = function(in_meta, in_slider, in_val)
		-- NOTE: тут все не так просто
		ATLASSERT(in_meta.object == in_slider);
		print("sl val chng", in_val);
		--GGF.InnerInvoke(in_meta, "SetValue", value);
		if (in_val ~= GGF.InnerInvoke(in_meta, "GetValue")) then
			GGF.InnerInvoke(in_meta, "ModifyValue", in_val);	-- TODO: проверить, тут возможно нужен OD метод вместо обычного...
			GGF.InnerInvoke(in_meta, "CounterGetOnValueChanged")(in_val);	-- запуск коллбека во внешку
		end
	end,
	EmptyCallback = function(in_meta) end,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.StandardSlider, {
	meta = {
		name = "Standard Slider",
		mark = "",	-- авт.
		suffix = GGD.StandardSliderSuffix,
	},
});

GGC.StandardSlider.objects = {
	slider = {
		standard = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.StandardSlider,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.StandardSlider.Default)) then
					local slider = CreateFrame(
						"Slider",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyStandardSliderOD", slider);
					slider:SetScript("OnValueChanged", function() end);
					--in_wrapper:SetBackdrop(GGD.Backdrop.StandardSlider);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.StandardSlider.wrappers = {};

GGC.StandardSlider.properties = {
	miscellaneous = {
		layer = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Layer,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		enabled = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Enabled,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local enabled = GGF.InnerInvoke(in_meta, "GetEnabled");
				if (enabled) then
					in_meta.object:Enable();
				else
					in_meta.object:Disable();
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyEnabled", in_meta.object:IsEnabled());
			end,
		}),
		value = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Value,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetValue(GGF.InnerInvoke(in_meta, "GetValue"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyValue", in_meta.object:GetValue());
			end,
		}),
		orientation = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Orientation,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetOrientation(GGF.InnerInvoke(in_meta, "GetOrientation"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyOrientation", in_meta.object:GetOrientation());
			end,
		}),
		thumbTexture = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ThumbTexture,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (GGF.InnerInvoke(in_meta, "IsTemplated")) then
					in_meta.object:SetThumbTexture(GGF.InnerInvoke(in_meta, "GetThumbTexture"));
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	values = {
		min = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ValuesMin,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustValues");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeValues");
			end,
		}),
		max = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ValuesMax,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustValues");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeValues");
			end,
		}),
		step = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ValuesStep,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetValueStep(GGF.InnerInvoke(in_meta, "GetValuesStep"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyValuesStep", in_meta.object:GetValueStep());
			end,
		}),
		stepsPerPage = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ValuesStepsPerPage,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
   			GGF.VersionSplitterProc({
   			{
   				versionInterval = {GGD.TocVersionList.Vanilla, GGD.TocVersionList.Cata},
   				action = function()
   	        	end,
   	    	},
   	    	{
	            versionInterval = {GGD.TocVersionList.Panda, GGD.TocVersionList.Invalid},
	         	action = function()
                  -- WARNING: формально функцию добавили только в 5.4.0, могут быть траблы с 5.0.Х и т.п.
				      in_meta.object:SetStepsPerPage(GGF.InnerInvoke(in_meta, "GetValuesStepsPerPage"));
               end,
            }
         });
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyValuesStepsPerPage", in_meta.object:GetStepsPerPage());
			end,
		}),
	},

	callback = {
		onValueChanged = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnValueChanged,
			fixed = false,

			--NOTE: ничего не надо делать - вызывается из другого коллбека(который ниже)
			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		-- TODO: разобраться, почему тут фиксед стоял
		onSliderValueChanged = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnSliderValueChanged,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetScript("OnValueChanged", function(...)
					--print("on val chng", GGF.InnerInvoke(in_meta, "CounterGetOnSliderValueChanged"));
					GGF.InnerInvoke(in_meta, "CounterGetOnSliderValueChanged")(in_meta, ...);
				end);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onMouseUp = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnMouseUp,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetScript("OnMouseUp", GGF.InnerInvoke(in_meta, "CounterGetOnMouseUp"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onMouseWheel = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnMouseWheel,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetScript("OnMouseWheel", GGF.InnerInvoke(in_meta, "CounterGetOnMouseWheel"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.StandardSlider.elements = {};

GGF.Mark(GGC.StandardSlider);

GGC.StandardSlider = GGF.TableSafeExtend(GGC.StandardSlider, GGC.StandardFrame);

GGF.CreateClassWorkaround(GGC.StandardSlider);

local This = GGC.StandardSlider;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-- Смена предельных значений слайдера
function This:ModifyValues(in_meta, in_values)
	GGF.InnerInvoke(in_meta, "ModifyValuesMin", in_values.min);
	GGF.InnerInvoke(in_meta, "ModifyValuesMax", in_values.max);
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Смена предельных значений слайдера
function This:SetValues(in_meta, in_values)
	GGF.InnerInvoke(in_meta, "SetValuesMin", in_values.min);
	GGF.InnerInvoke(in_meta, "SetValuesMax", in_values.max);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие предельных значений слайдера
function This:GetValues(in_meta)
	local values = {
		min = GGF.InnerInvoke(in_meta, "GetValuesMin"),
		max = GGF.InnerInvoke(in_meta, "GetValuesMax"),
	};
	return values;
end


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Установка предельных значений слайдера
function This:AdjustValues(in_meta)
	local values = GGF.InnerInvoke(in_meta, "GetValues");
	in_meta.object:SetMinMaxValues(values.min, values.max);
	in_meta.object:SetValue(GGF.InnerInvoke(in_meta, "GetValue"));
end


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-- Синхронизация предельных значений слайдера
function This:SynchronizeValues(in_meta)
	local min, max = in_meta.object:GetMinMaxValues();
	local value = in_meta.object:GetValue();
	GGF.InnerInvoke(in_meta, "ModifyValues", {
		min = min,
		max = max,
	});
	GGF.InnerInvoke(in_meta, "ModifyValue", value);
end


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------