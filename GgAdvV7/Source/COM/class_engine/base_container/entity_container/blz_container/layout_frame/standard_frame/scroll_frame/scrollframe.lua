-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

local OBJSuffix = "SCF";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	ScrollFrameSuffix = GGF.DockDomName(GGD.DefineIdentifier, "ScrollFrameSuffix"),
	ScrollFrame = GGF.DockDomName(GGD.ClassIdentifier, "ScrollFrame"),
};

local CTList = {
   ScrollFrame = GGF.CreateCT(OBJSuffix, "ScrollFrame", "Frame"),
   ScrollChildFrame = GGF.CreateCT(OBJSuffix, "ScrollChildFrame", "Frame"),
};

GGF.GRegister(DFList.ScrollFrameSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
   Frame = 0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.ScrollFrame, {
   meta = {
      name = "Scroll Frame",
      mark = "",  -- авт.
      suffix = GGD.ScrollFrameSuffix,
   },
});

GGC.ScrollFrame.objects = {
   frame = {
      scroll = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Object,
         cte = CTList.ScrollFrame,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.ScrollFrame.Default)) then
               local slider = CreateFrame(
                  "ScrollFrame",
                  GGF.GenerateOTName(
                     GGF.InnerInvoke(in_meta, "GetParentName"),
                     GGF.InnerInvoke(in_meta, "GetWrapperName"),
                     GGF.InnerInvoke(in_meta, "getClassSuffix")
                  ),
                  GGF.InnerInvoke(in_meta, "GetParent"),
                  GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
               );
               GGF.InnerInvoke(in_meta, "ModifyScrollFrameOD", slider);
               --slider:SetScript("OnValueChanged", function() end);
               --in_wrapper:SetBackdrop(GGD.Backdrop.ScrollFrame);
            end
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.ScrollFrame.wrappers = {};

GGC.ScrollFrame.properties = {
   frame = {
      scrollChild = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.ScrollChildFrame,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            local child = GGF.InnerInvoke(in_meta, "GetScrollChildFrame");
            if (child) then
               in_meta.object:SetScrollChild(child);
            end
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.ScrollFrame.elements = {};

GGF.Mark(GGC.ScrollFrame);

GGC.ScrollFrame = GGF.TableSafeExtend(GGC.ScrollFrame, GGC.StandardFrame);

GGF.CreateClassWorkaround(GGC.ScrollFrame);

local This = GGC.ScrollFrame;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------