-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


-- NOTE: по сути это обыкновенная залипайка
local OBJSuffix = "STB";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	StuckingButtonSuffix = GGF.DockDomName(GGD.DefineIdentifier, "StuckingButtonSuffix"),
	StuckingButton = GGF.DockDomName(GGD.ClassIdentifier, "StuckingButton"),
};

local CTList = {
	StuckingButton = GGF.CreateCT(OBJSuffix, "StuckingButton", "Frame"),
	OnStateChanged = GGF.CreateCT(OBJSuffix, "OnStateChanged", "EmptyCallback"),
	OnClick = GGF.CreateCT(OBJSuffix, "OnClick", "OnClick"),
	State = GGF.CreateCT(OBJSuffix, "State", "State"),
};

GGF.GRegister(DFList.StuckingButtonSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	OnClick = function(in_meta, ...)
		GGF.InnerInvoke(in_meta, "changeState");
		GGF.InnerInvoke(in_meta, "CounterGetOnStateChanged")(in_meta, GGF.InnerInvoke(in_meta, "GetState"));
	end,
	State = false,
	EmptyCallback = function(in_meta) end,
}, GGE.RegValType.Default);

-- TODO: пошаманить с цветом
GGF.GRegister(DFList.StuckingButton, {
	meta = {
		name = "Stucking Button",
		mark = "",	-- авт.
		suffix = GGD.StuckingButtonSuffix,
	},
	--objPropList = {},
});

GGC.StuckingButton.objects = {
	button = {
		stucking = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.StuckingButton,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.StuckingButton.Default)) then
					local standardButton = CreateFrame(
						"Button",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
         						return GGF.TernExpSingle(
         							GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
         							nil
         						)
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyStuckingButtonOD", standardButton);
               --print("stck btn:", GGF.InnerInvoke(in_meta, "GetWrapperName"), in_meta.instance);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.StuckingButton.wrappers = {};

GGC.StuckingButton.properties = {
	callback = {
      onClick = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.OnClick,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            local callback = GGF.InnerInvoke(in_meta, "CounterGetOnClick");
            --print("cb adjuster", in_meta, in_meta.instance, in_meta.class, in_meta.object);
            in_meta.object:SetScript("OnClick", function(...) callback(in_meta, ...); end);
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
		onStateChanged = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnStateChanged,
			fixed = false,

			-- NOTE: пустой(автокаст с другого коллбека)
			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.StuckingButton.elements = {
	state = GGC.ParameterContainer:Create({
		ctype = GGE.ContainerType.Element,
		cte = CTList.State,
		fixed = false,

		[GGE.RuntimeMethod.Adjuster] = function(in_meta)
		end,
		[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
		end,
	}),
};

GGF.Mark(GGC.StuckingButton);

GGC.StuckingButton = GGF.ClassInheritance(GGC.StuckingButton, GGC.StandardButton);
print("df stck", GGC.StuckingButton.properties.callback.onClick.default.objSuffix);
GGF.CreateClassWorkaround(GGC.StuckingButton);

local This = GGC.StuckingButton;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

-- Смена состояния тумблерной кнопки
function This:changeState(in_meta)
	local nwState = not GGF.InnerInvoke(in_meta, "GetState");
	GGF.InnerInvoke(in_meta, "SetState", nwState);
	GGF.InnerInvoke(in_meta, "SetHighlighted", nwState);
end