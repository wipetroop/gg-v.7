-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "EB";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	EditBoxSuffix = GGF.DockDomName(GGD.DefineIdentifier, "EditBoxSuffix"),
	EditBox = GGF.DockDomName(GGD.ClassIdentifier, "EditBox"),
};

local CTList = {
	EditBox = GGF.CreateCT(OBJSuffix, "EditBox", "Frame"),
	IsNumeric = GGF.CreateCT(OBJSuffix, "IsNumeric", "IsNumeric"),
	Number = GGF.CreateCT(OBJSuffix, "Number", "Number"),
	Text = GGF.CreateCT(OBJSuffix, "Text", "Text"),
	FontObject = GGF.CreateCT(OBJSuffix, "FontObject", "FontObject"),
	JustifyH = GGF.CreateCT(OBJSuffix, "JustifyH", "JustifyH"),
	AutoFocus = GGF.CreateCT(OBJSuffix, "AutoFocus", "AutoFocus"),
   MultiLine = GGF.CreateCT(OBJSuffix, "MultiLine", "MultiLine"),
	OnEnterPressed = GGF.CreateCT(OBJSuffix, "OnEnterPressed", "EmptyCallback"),
	OnTextChanged = GGF.CreateCT(OBJSuffix, "OnTextChanged", "EmptyCallback"),
};

GGF.GRegister(DFList.EditBoxSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	IsNumeric = false,
	Number = 0,
	Text = "Default",
	JustifyH = GGE.JustifyHType.Center,
	FontObject = GameFontHighlightSmall,
	AutoFocus = false,
   MultiLine = false,
	EmptyCallback = function() end,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.EditBox, {
	meta = {
		name = "Edit Box",
		mark = "",	-- авт.
		suffix = GGD.EditBoxSuffix,
	},
});

GGC.EditBox.objects = {
	editBox = {
		standard = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.EditBox,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.EditBox.Default)) then
					local editBox = CreateFrame(
						"EditBox",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
						GGF.VersionSplitterFunc({
	                  {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
         						return GGF.TernExpSingle(
   									GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
   									nil
   								)
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyEditBoxOD", editBox);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.EditBox.wrappers = {};

GGC.EditBox.properties = {
	miscellaneous = {
		isNumeric = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.IsNumeric,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetNumeric(GGF.InnerInvoke(in_meta, "GetIsNumeric"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyIsNumeric", in_meta.object:GetNumeric());
			end,
		}),
		number = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Number,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (GGF.InnerInvoke(in_meta, "GetIsNumeric")) then
					in_meta.object:SetNumber(GGF.InnerInvoke(in_meta, "GetNumber"));
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				if (GGF.InnerInvoke(in_meta, "GetIsNumeric")) then
					GGF.InnerInvoke(in_meta, "ModifyNumber", in_meta.object:GetNumber());
				end
			end,
		}),
		text = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Text,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (not GGF.InnerInvoke(in_meta, "GetIsNumeric")) then
					in_meta.object:SetText(GGF.InnerInvoke(in_meta, "GetText"));
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				if (not GGF.InnerInvoke(in_meta, "GetIsNumeric")) then
					GGF.InnerInvoke(in_meta, "ModifyText", in_meta.object:GetText());
				end
			end,
		}),
		fontObject = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.FontObject,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetFontObject(GGF.InnerInvoke(in_meta, "GetFontObject"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyFontObject", in_meta.object:GetFontObject());
			end,
		}),
		autofocus = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.AutoFocus,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetAutoFocus(GGF.InnerInvoke(in_meta, "GetAutoFocus"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyAutoFocus", in_meta.object:GetAutoFocus());
			end,
		}),
      multiline = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.MultiLine,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            in_meta.object:SetMultiLine(GGF.InnerInvoke(in_meta, "GetMultiLine"));
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            GGF.InnerInvoke(in_meta, "ModifyMultiLine", in_meta.object:GetMultiLine());
         end,
      }),
	},
	anchor = {
		justifyH = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.JustifyH,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetJustifyH(GGF.InnerInvoke(in_meta, "GetJustifyH"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyJustifyH", in_meta.object:GetJustifyH());
			end,
		}),
	},
	callback = {
		onEnterPressed = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnEnterPressed,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local callback = GGF.InnerInvoke(in_meta, "CounterGetOnEnterPressed");
				in_meta.object:SetScript("OnEnterPressed", function()
					local value;
					if (GGF.InnerInvoke(in_meta, "GetIsNumeric")) then
						value = in_meta.object:GetNumber();
					else
						value = in_meta.object:GetText();
					end
					GGF.InnerInvoke(in_meta, "SetUniversalValue", value);
					callback(in_object);
				end);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onTextChanged = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnTextChanged,
			fixed = false,

			-- TODO: надо бы ликвидировать
			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				--local callback = in_wrapper:GetOnTextChangedCounter();
				--in_object:SetScript("OnTextChanged", function()
					--callback(in_wrapper:GetUniversalValue());
				--end);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.EditBox.elements = {};

GGF.Mark(GGC.EditBox);

GGC.EditBox = GGF.TableSafeExtend(GGC.EditBox, GGC.StandardFrame);

GGF.CreateClassWorkaround(GGC.EditBox);

local This = GGC.EditBox;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Универсальная установка значения
function This:SetUniversalValue(in_meta, in_val)
	local isNumeric = GGF.InnerInvoke(in_meta, "GetIsNumeric");
	--print("editbox val set", in_val, isNumeric);
	if (isNumeric) then
		GGF.InnerInvoke(in_meta, "SetNumber", in_val);
	else
		GGF.InnerInvoke(in_meta, "SetText", in_val);
	end
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Универсальная взятие значения
function This:GetUniversalValue(in_meta)
	local isNumeric = GGF.InnerInvoke(in_meta, "GetIsNumeric");
	local result = 0; -- NOTE: отладочная фишка
	if (isNumeric) then
		result = GGF.InnerInvoke(in_meta, "GetNumber");
	else
		result = GGF.InnerInvoke(in_meta, "GetText");
	end
	--print("editbox val get", result, isNumeric);
	return result;
end


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

-- Добавочное значение
function This:AddText(in_meta, in_string)
   ATLASSERT(in_string);
   local curText = GGF.InnerInvoke(in_meta, "GetText");
   GGF.InnerInvoke(in_meta, "SetText", curText .. in_string);
end

-- Добавочное значение
function This:NewString(in_meta)
   local curText = GGF.InnerInvoke(in_meta, "GetText");
   GGF.InnerInvoke(in_meta, "SetText", curText .. "\n");
end