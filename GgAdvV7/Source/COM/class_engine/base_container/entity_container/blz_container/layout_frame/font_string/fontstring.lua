-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "FS";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	FontStringSuffix = GGF.DockDomName(GGD.DefineIdentifier, "FontStringSuffix"),
	FontString = GGF.DockDomName(GGD.ClassIdentifier, "FontString"),
};

local CTList = {
	FontString = GGF.CreateCT(OBJSuffix, "FontString", "Frame"),
	Layer = GGF.CreateCT(OBJSuffix, "Layer", "Layer"),
	Font = GGF.CreateCT(OBJSuffix, "Font", "Font"),
	Text = GGF.CreateCT(OBJSuffix, "Text", "Text"),
	TooltipText = GGF.CreateCT(OBJSuffix, "TooltipText", "TooltipText"),
	FontHeight = GGF.CreateCT(OBJSuffix, "FontHeight", "FontHeight"),
	Alpha = GGF.CreateCT(OBJSuffix, "Alpha", "Alpha"),
	JustifyH = GGF.CreateCT(OBJSuffix, "JustifyH", "JustifyH"),
	JustifyV = GGF.CreateCT(OBJSuffix, "JustifyV", "JustifyV"),
	ColorR = GGF.CreateCT(OBJSuffix, "ColorR", "ColorR"),
	ColorG = GGF.CreateCT(OBJSuffix, "ColorG", "ColorG"),
	ColorB = GGF.CreateCT(OBJSuffix, "ColorB", "ColorB"),
	ColorA = GGF.CreateCT(OBJSuffix, "ColorA", "ColorA"),
	ShadowColorR = GGF.CreateCT(OBJSuffix, "ShadowColorR", "ShadowColorR"),
	ShadowColorG = GGF.CreateCT(OBJSuffix, "ShadowColorG", "ShadowColorG"),
	ShadowColorB = GGF.CreateCT(OBJSuffix, "ShadowColorB", "ShadowColorB"),
	ShadowColorA = GGF.CreateCT(OBJSuffix, "ShadowColorA", "ShadowColorA"),
	ShadowOffsetX = GGF.CreateCT(OBJSuffix, "ShadowOffsetX", "ShadowOffsetX"),
	ShadowOffsetY = GGF.CreateCT(OBJSuffix, "ShadowOffsetY", "ShadowOffsetY"),
};

GGF.GRegister(DFList.FontStringSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	Layer = GGE.LayerType.Artwork,
	Font = "GameFontNormal",
	Text = "Default text",
	TooltipText = "Default tooltip text",
	FontHeight = 12,
	Alpha = 1.0,
	JustifyH = GGE.JustifyHType.Center,
	JustifyV = GGE.JustifyVType.Middle,
	ColorR = 1.0,
	ColorG = 1.0,
	ColorB = 1.0,
	ColorA = 1.0,
	ShadowColorR = 0.0,
	ShadowColorG = 0.0,
	ShadowColorB = 0.0,
	ShadowColorA = 1.0,
	ShadowOffsetX = 0.0,
	ShadowOffsetY = 0.0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.FontString, {
	meta = {
		name = "Font String",
		mark = "",	-- авт.
		suffix = GGD.FontStringSuffix,
	},
});

GGC.FontString.objects = {
	layer = {
		fontString  = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.FontString,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.FontString.Default)) then
					local fontString = GGF.InnerInvoke(in_meta, "GetParent"):CreateFontString(
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetLayer"),
						GGF.InnerInvoke(in_meta, "GetFont")
					);
					--fontString:SetFont(in_wrapper:GetFont(), in_wrapper:GetFontHeight());
					GGF.InnerInvoke(in_meta, "ModifyFontStringOD", fontString);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.FontString.wrappers = {};

GGC.FontString.properties = {
	miscellaneous = {
		layer = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Layer,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		font = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Font,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetFont(GGF.InnerInvoke(in_meta, "GetFont"), GGF.InnerInvoke(in_meta, "GetFontHeight"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		text = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Text,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetText(GGF.InnerInvoke(in_meta, "GetText"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		tooltipText = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.TooltipText,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				-- TODO: починить!!!
				--in_object.tooltip_text = GGF.InnerInvoke(in_meta, "GetTooltipText");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		fontHeight = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.FontHeight,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetTextHeight(GGF.InnerInvoke(in_meta, "GetFontHeight"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		alpha = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Alpha,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				--print("fs alpha", GGF.InnerInvoke(in_meta, "GetAlpha"));
				in_meta.object:SetAlpha(GGF.InnerInvoke(in_meta, "GetAlpha"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	anchor = {
		justifyH = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.JustifyH,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetJustifyH(GGF.InnerInvoke(in_meta, "GetJustifyH"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		justifyV = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.JustifyV,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetJustifyV(GGF.InnerInvoke(in_meta, "GetJustifyV"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	color = {
		R = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ColorR,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustColor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeColor");
			end,
		}),
		G = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ColorG,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustColor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeColor");
			end,
		}),
		B = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ColorB,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustColor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeColor");
			end,
		}),
		A = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ColorA,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustColor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeColor");
			end,
		}),
	},
	shadow = {
		color = {
			R = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ShadowColorR,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustShadowColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeShadowColor");
				end,
			}),
			G = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ShadowColorG,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustShadowColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeShadowColor");
				end,
			}),
			B = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ShadowColorB,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustShadowColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeShadowColor");
				end,
			}),
			A = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ShadowColorA,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustShadowColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeShadowColor");
				end,
			}),
		},
		offset = {
			X = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ShadowOffsetX,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustShadowOffset");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeShadowOffset");
				end,
			}),
			Y = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ShadowOffsetY,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustShadowOffset");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeShadowOffset");
				end,
			}),
		},
	},
};

GGC.FontString.elements = {};

GGF.Mark(GGC.FontString);

GGC.FontString = GGF.TableSafeExtend(GGC.FontString, GGC.LayoutFrame);

GGF.CreateClassWorkaround(GGC.FontString);

local This = GGC.FontString;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-- Модификация цвета фона
function This:ModifyColor(in_meta, in_clr)
	if (in_clr ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyColorR", in_clr.R);
		GGF.InnerInvoke(in_meta, "ModifyColorG", in_clr.G);
		GGF.InnerInvoke(in_meta, "ModifyColorB", in_clr.B);
		GGF.InnerInvoke(in_meta, "ModifyColorA", in_clr.A);
	end
end


-- Модификация цвета фона
function This:ModifyShadowColor(in_meta, in_sclr)
	if (in_sclr ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyShadowColorR", in_sclr.R);
		GGF.InnerInvoke(in_meta, "ModifyShadowColorG", in_sclr.G);
		GGF.InnerInvoke(in_meta, "ModifyShadowColorB", in_sclr.B);
		GGF.InnerInvoke(in_meta, "ModifyShadowColorA", in_sclr.A);
	end
end


-- Модификация цвета фона
function This:ModifyShadowOffset(in_meta, in_off)
	if (in_off ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyShadowOffsetX", in_off.X);
		GGF.InnerInvoke(in_meta, "ModifyShadowOffsetY", in_off.Y);
	end
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------



-- Смена цвета основного текста
function This:SetColor(in_meta, in_clr)
	GGF.InnerInvoke(in_meta, "ModifyColor", in_clr);
	GGF.InnerInvoke(in_meta, "AdjustColor");
end


-- Смена цвета тени основного текста
function This:SetShadowColor(in_meta, in_sclr)
	GGF.InnerInvoke(in_meta, "ModifyShadowColor", in_sclr);
	GGF.InnerInvoke(in_meta, "AdjustShadowColor");
end


-- Смена смещения тени относительно основного текста
function This:SetShadowOffset(in_meta, in_off)
	GGF.InnerInvoke(in_meta, "ModifyShadowOffset", in_off);
	GGF.InnerInvoke(in_meta, "AdjustShadowOffset");
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие цвета основного текста
function This:GetColor(in_meta)
	local color = {
		R = GGF.InnerInvoke(in_meta, "GetColorR"),
		G = GGF.InnerInvoke(in_meta, "GetColorG"),
		B = GGF.InnerInvoke(in_meta, "GetColorB"),
		A = GGF.InnerInvoke(in_meta, "GetColorA"),
	};
	return color;
end


-- Взятие цвета тени основного текста
function This:GetShadowColor(in_meta)
	local color = {
		R = GGF.InnerInvoke(in_meta, "GetShadowColorR"),
		G = GGF.InnerInvoke(in_meta, "GetShadowColorG"),
		B = GGF.InnerInvoke(in_meta, "GetShadowColorB"),
		A = GGF.InnerInvoke(in_meta, "GetShadowColorA"),
	};
	return color;
end


-- Взятие смещения тени относительно основного текста
function This:GetShadowOffset(in_meta)
	local offset = {
		X = GGF.InnerInvoke(in_meta, "GetShadowOffsetX"),
		Y = GGF.InnerInvoke(in_meta, "GetShadowOffsetY"),
	};
	return offset;
end


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Установка цвета основного текста
function This:AdjustColor(in_meta)
	local color = GGF.InnerInvoke(in_meta, "GetColor");
	in_meta.object:SetTextColor(color.R, color.G, color.B, color.A);
end


-- Установка цвета тени основного текста
function This:AdjustShadowColor(in_meta)
	local color = GGF.InnerInvoke(in_meta, "GetShadowColor");
	in_meta.object:SetShadowColor(color.R, color.G, color.B, color.A);
end


-- Установка свещения тени относительно основного текста
function This:AdjustShadowOffset(in_meta)
	local offset = GGF.InnerInvoke(in_meta, "GetShadowOffset");
	in_meta.object:SetShadowOffset(offset.X, offset.Y);
end


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-- Синхронизация цвета основного текста
function This:SynchronizeColor(in_meta)
	local r, g, b, a = in_meta.object:GetTextColor();
	GGF.InnerInvoke(in_meta, "ModifyColor", {
		R = r,
		G = g,
		B = b,
		A = a,
	});
end


-- Синхронизация цвета тени основного текста
function This:SynchronizeShadowColor(in_meta)
	local r, g, b, a = in_meta.object:GetShadowColor();
	GGF.InnerInvoke(in_meta, "ModifyShadowColor", {
		R = r,
		G = g,
		B = b,
		A = a,
	});
end


-- Синхронизация свещения тени относительно основного текста
function This:SynchronizeShadowOffset(in_meta)
	local x, y = in_meta.object:GetShadowOffset();
	GGF.InnerInvoke(in_meta, "ModifyShadowOffset", {
		X = x,
		Y = y,
	});
end


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------