-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "CB";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	CheckButtonSuffix = GGF.DockDomName(GGD.DefineIdentifier, "CheckButtonSuffix"),
	CheckButton = GGF.DockDomName(GGD.ClassIdentifier, "CheckButton"),
};

local CTList = {
	CheckButton = GGF.CreateCT(OBJSuffix, "CheckButton", "Frame"),
	Template = GGF.CreateCT(OBJSuffix, "Template", "Template"),
	SubObject = GGF.CreateCT(OBJSuffix, "SubObject", "SubObject"),
	SubObjectName = GGF.CreateCT(OBJSuffix, "SubObjectName", "SubObjectName"),
	Enabled = GGF.CreateCT(OBJSuffix, "Enabled", "Enabled"),
	Text = GGF.CreateCT(OBJSuffix, "Text", "Text"),
	Checked = GGF.CreateCT(OBJSuffix, "Checked", "Checked"),
	OnClick = GGF.CreateCT(OBJSuffix, "OnClick", "EmptyCallback"),
	OnStateChanged = GGF.CreateCT(OBJSuffix, "OnStateChanged", "EmptyCallback"),
};

GGF.GRegister(DFList.CheckButtonSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	Template = "UICheckButtonTemplate",
	SubObject = function(in_meta)
		--return getglobal(in_argline.object:GetName() .. 'Text');
		return getglobal(in_meta.object:GetName() .. 'Text');
	end,
	SubObjectName = function(in_meta)
		--return in_argline.object:GetName() .. 'Text';
		return in_meta.object:GetName() .. 'Text';
	end,
	Enabled = true,
	Text = "",
	Checked = false,
	EmptyCallback = function(in_meta) end,
}, GGE.RegValType.Default);

-- TODO: проверить работоспособность!!!особенно сабнейм...(GG-55)
GGF.GRegister(DFList.CheckButton, {
	meta = {
		name = "Check Button",
		mark = "",	-- авт.
		suffix = GGD.CheckButtonSuffix,
	},
});

GGC.CheckButton.objects = {
	button = {
		standard = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.CheckButton,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.CheckButton.Default)) then
					local checkButton = CreateFrame(
						"CheckButton",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
         						return GGF.TernExpSingle(
         							GGF.InnerInvoke(in_meta, "IsTemplated"),
         							GGF.InnerInvoke(in_meta, "GetTemplate"),
         							nil
         						)
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					--local checkButton = CreateFrame("Button", GGF.GenerateOTName(in_wrapper:GetParentName(), in_wrapper:GetWrapperName(), in_wrapper:getClassSuffix()), in_wrapper:GetParent(), "UICheckButtonTemplate");
					GGF.InnerInvoke(in_meta, "ModifyCheckButtonOD", checkButton);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.CheckButton.properties = {
	base = {
		template = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Template,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		subObject = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.SubObject,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				-- NOTE: вроде бы нельзя менять
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		subObjectName = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.SubObjectName,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				-- NOTE: вроде бы нельзя менять
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},

	miscellaneous = {
		enabled = GGC.ParameterContainer:Create({	-- параметр перегружен
			ctype = GGE.ContainerType.Property,
			cte = CTList.Enabled,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local enabled = GGF.InnerInvoke(in_meta, "GetEnabled");
				local color = GGF.TernExpSingle(enabled, GGD.Color.CBEnabled, GGD.Color.CBDisabled);
				if (enabled) then
					in_meta.object:Enable();
					GGF.InnerInvoke(in_meta, "GetSubObject"):SetTextColor(color.R, color.G, color.B, color.A);
				else
					in_meta.object:Disable();
					GGF.InnerInvoke(in_meta, "GetSubObject"):SetTextColor(color.R, color.G, color.B, color.A);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				ATLASSERT(false);
			end,
		}),
		text = GGC.ParameterContainer:Create({	-- параметр перегружен
			ctype = GGE.ContainerType.Property,
			cte = CTList.Text,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local text = GGF.InnerInvoke(in_meta, "GetText");
				GGF.InnerInvoke(in_meta, "GetSubObject"):SetText(text);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				local text = GGF.InnerInvoke(in_meta, "GetSubObject"):GetText();
				GGF.InnerInvoke(in_meta, "ModifyText", text);
			end,
		}),
		checked = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Checked,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				-- HACK: геттер сует состояние чекбокса в переменную, а аджастер - наоборот
				in_meta.object:SetChecked(in_meta.instance.properties.miscellaneous.checked);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
			[GGE.RuntimeMethod.ValueGetter] = function(in_meta)
				local obj = in_meta.object;
				-- NOTE: если туда попадает nil - стирается нахер поле и след. вызов критует
				local chck = GGF.TernExpSingle(obj:GetChecked() == nil, false, true);
				--print("obj dump", in_class, in_instance, in_object, chck);
				GGF.InnerInvoke(in_meta, "ModifyCheckedOD", chck);
				return chck;
			end,
		}),
	},

	callback = {
		onClick = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnClick,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local callback = GGF.InnerInvoke(in_meta, "CounterGetOnClick");
				local stateback = GGF.InnerInvoke(in_meta, "CounterGetOnStateChanged");
				in_meta.object:SetScript("OnClick", function()
					callback();
					stateback(GGF.InnerInvoke(in_meta, "GetChecked"));
				end);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onStateChanged = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnStateChanged,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				--local callback = in_wrapper:GetOnStateChangedCounter();
				--in_object:SetScript("OnClick", callback);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.CheckButton.elements = {};

GGF.Mark(GGC.CheckButton);

GGC.CheckButton = GGF.TableSafeExtend(GGC.CheckButton, GGC.StandardButton);

GGF.CreateClassWorkaround(GGC.CheckButton);

local This = GGC.CheckButton;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------