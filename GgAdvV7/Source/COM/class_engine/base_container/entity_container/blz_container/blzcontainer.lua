-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "BLC";

local DFList = {
	BlzContainerSuffix = GGF.DockDomName(GGD.DefineIdentifier, "BlzContainerSuffix"),
	BlzContainer = GGF.DockDomName(GGD.ClassIdentifier, "BlzContainer"),
};

GGF.GRegister(DFList.BlzContainerSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.BlzContainer, {
	meta = {
		name = "Blz Container",
		mark = "",	-- авт.
		suffix = GGD.BlzContainerSuffix,
	},
});

GGC.BlzContainer.objects = {};

GGC.BlzContainer.wrappers = {};

GGC.BlzContainer.properties = {};

GGC.BlzContainer.elements = {};

GGF.Mark(GGC.BlzContainer);

GGC.BlzContainer = GGF.TableSafeExtend(GGC.BlzContainer, GGC.EntityContainer);

GGF.CreateClassWorkaround(GGC.BlzContainer);

local This = GGC.BlzContainer;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Применение всех свойств к объекту класса
function This:Adjust(in_meta)
	GGF.BlzAdjust(in_meta);
end


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- NOTE: на данном этапе не работает
-- Взятие названия объекта
function This:getObjectName()
	--local i = debug.getinfo(3, 'Sl');
	--return "'"..(io.open(i.source:match'@(.*)'):read'*a'
		--:gsub('.-\n','',i.currentline-1)
		--:match('(.-)[:.]%s*'..debug.getinfo(2,'n').name..'%s*%(')
		--:match'([%w_]+)%s*$' or '<unnamed>').."' at line #"..i.currentline;
end