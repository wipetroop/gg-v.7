-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "EC";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	EntityContainerSuffix = GGF.DockDomName(GGD.DefineIdentifier, "EntityContainerSuffix"),
	EntityContainer = GGF.DockDomName(GGD.ClassIdentifier, "EntityContainer"),
};

local CTList = {
	ParentName = GGF.CreateCT(OBJSuffix, "ParentName", "ParentName"),
	Parent = GGF.CreateCT(OBJSuffix, "Parent", "Parent"),
	WrapperName = GGF.CreateCT(OBJSuffix, "WrapperName", "WrapperName"),
};

GGF.GRegister(DFList.EntityContainerSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	ParentName = "UIParent",
	Parent = function(in_meta)
		return UIParent;
	end,
	WrapperName = "DWN",
}, GGE.RegValType.Default);

GGF.GRegister(DFList.EntityContainer, {
	meta = {
		name = "Entity Container",
		mark = "",	-- авт.
		suffix = GGD.EntityContainerSuffix,
	},
});

GGC.EntityContainer.objects = {};

GGC.EntityContainer.wrappers = {};

GGC.EntityContainer.properties = {
	base = {
		parentName = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ParentName,
			fixed = false,

			-- TODO: вот тут подумать надо...
			[GGE.RuntimeMethod.Setter] = function(in_meta, in_val)
				--ATLASSERT(in_val and type(in_val) == 'string');
				--in_wrapper:DefaultDataModify(in_wrapper.properties.base.parentName, in_val);
				--in_wrapper:DefaultDataModify(in_wrapper.properties.base.parent, _G[in_val]);
				--in_wrapper:AdjustParentName();
			end,
			[GGE.RuntimeMethod.Modifier] = function(in_meta, in_val)
				--ATLASSERT(in_val and type(in_val) == 'string');
				--in_wrapper:DefaultDataModify(in_wrapper.properties.base.parentName, in_val);
				--in_wrapper:DefaultDataModify(in_wrapper.properties.base.parent, _G[in_val]);
			end,
			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				-- NOTE: не аджастится
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				-- NOTE: не синхронится
			end,
		}),
		parent = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Parent,
			fixed = false,

			[GGE.RuntimeMethod.Setter] = function(in_meta, in_val)
				ATLASSERT(in_val and (type(in_val) == 'table' or type(in_val) == 'function'));
				local objectFun = GGF.TernExpSingle(type(in_val) == 'table', function() return in_val end, function() return in_val() end);
				local object = objectFun();
				in_meta.instance.properties.base.parent = object;
				in_meta.instance.properties.base.parentName = object:GetName();
				GGF.InnerInvoke(in_meta, "AdjustParent");
			end,
			[GGE.RuntimeMethod.Modifier] = function(in_meta, in_val, ...)
				ATLASSERT(in_val and (type(in_val) == 'table' or type(in_val) == 'function'));
				local objectFun = GGF.TernExpSingle(type(in_val) == 'table', function() return in_val end, function() return in_val() end);
				local object = objectFun();
				--print("prnt mod:", in_val, object, ...);
				in_meta.instance.properties.base.parent = object;
				in_meta.instance.properties.base.parentName = object:GetName();
			end,
			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				-- NOTE: не аджастится
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				-- NOTE: не синхронится
			end,
		}),
		wrapperName = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.WrapperName,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.EntityContainer.elements = {};

GGF.Mark(GGC.EntityContainer);

GGC.EntityContainer = GGF.TableSafeExtend(GGC.EntityContainer, GGC.BaseContainer);

GGF.CreateClassWorkaround(GGC.EntityContainer);

local This = GGC.EntityContainer;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------