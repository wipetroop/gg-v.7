-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

local OBJSuffix = "DD";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	DigitalDisplaySuffix = GGF.DockDomName(GGD.DefineIdentifier, "DigitalDisplaySuffix"),
	DigitalDisplay = GGF.DockDomName(GGD.ClassIdentifier, "DigitalDisplay"),
};

local CTList = {
	DigitalDisplay = GGF.CreateCT(OBJSuffix, "DigitalDisplay", "Frame"),
	Font = GGF.CreateCT(OBJSuffix, "Font", "Font"),
	FontHeight = GGF.CreateCT(OBJSuffix, "FontHeight", "FontHeight"),
};

GGF.GRegister(DFList.DigitalDisplaySuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	--Font = "Interface\\AddOns\\" .. GGD.AddonName .. "\\Fonts\\digifaw.ttf",
	Font = "DD_FONT",
	FontHeight = 18,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.DigitalDisplay, {
	meta = {
		name = "Digital Display",
		mark = "",	-- авт.
		suffix = GGD.DigitalDisplaySuffix,
	},
});

GGC.DigitalDisplay.objects = {
	layer = {
		fontString  = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.DigitalDisplay,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.DigitalDisplay.Default)) then
					local fontString = GGF.InnerInvoke(in_meta, "GetParent"):CreateFontString(
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetLayer"),
						GGF.InnerInvoke(in_meta, "GetFont")
					);
					--fontString:SetFont(in_wrapper:GetFont(), in_wrapper:GetFontHeight());
					GGF.InnerInvoke(in_meta, "ModifyDigitalDisplayOD", fontString);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.DigitalDisplay.wrappers = {};

GGC.DigitalDisplay.properties = {
	miscellaneous = {
		font = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Font,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustFontInner");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeFontInner");
			end,
		}),
		-- TODO: починить! не работает!
		fontHeight = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.FontHeight,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustFontInner");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SynchronizeFontInner");
			end,
		}),
	},
};

GGC.DigitalDisplay.elements = {};

GGF.Mark(GGC.DigitalDisplay);

GGC.DigitalDisplay = GGF.TableSafeExtend(GGC.DigitalDisplay, GGC.FontString);

GGF.CreateClassWorkaround(GGC.DigitalDisplay);

local This = GGC.DigitalDisplay;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-- Установка шрифта
function This:AdjustFontInner(in_meta)
	print("adjust font");
	in_meta.object:SetFont(
		GGF.InnerInvoke(in_meta, "GetFont"),
		GGF.InnerInvoke(in_meta, "GetFontHeight")
	);
end

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-- Синхронизация шрифта
function This:SynchronizeFontInner(in_meta)
	local font, size, flags = in_meta.object:GetFont();
	GGF.InnerInvoke(in_meta, "ModifyFont", font);
	GGF.InnerInvoke(in_meta, "ModifyFontHeight", size);
end

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------