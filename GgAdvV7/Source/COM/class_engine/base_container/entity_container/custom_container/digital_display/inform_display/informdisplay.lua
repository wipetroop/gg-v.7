-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "ID";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	InformDisplaySuffix = GGF.DockDomName(GGD.DefineIdentifier, "InformDisplaySuffix"),
	InformDisplay = GGF.DockDomName(GGD.ClassIdentifier, "InformDisplay"),
};

local CTList = {
	InformDisplay = GGF.CreateCT(OBJSuffix, "InformDisplay", "Frame"),
	Text = GGF.CreateCT(OBJSuffix, "Text", "Text"),
	Interval = GGF.CreateCT(OBJSuffix, "Interval", "Interval"),
	Status = GGF.CreateCT(OBJSuffix, "Status", "Status"),
	ErrList = GGF.CreateCT(OBJSuffix, "ErrList", "ErrList"),
	Current = GGF.CreateCT(OBJSuffix, "Current", "Current"),
	Running = GGF.CreateCT(OBJSuffix, "Running", "Running"),
};

GGF.GRegister(DFList.InformDisplaySuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	Text = "",
	Interval = 5,
	Status = "HP",
	ErrList = { 43, "C6", 56, "FF" },
	Current = "status",
	Running = false,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.InformDisplay, {
	meta = {
		name = "Digital Display",
		mark = "",	-- авт.
		suffix = GGD.InformDisplaySuffix,
	},
});

GGC.InformDisplay.objects = {
	layer = {
		fontString  = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.InformDisplay,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.InformDisplay.Default)) then
					local fontString = GGF.InnerInvoke(in_meta, "GetParent"):CreateFontString(
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetLayer"),
						GGF.InnerInvoke(in_meta, "GetFont")
					);
					--fontString:SetFont(in_wrapper:GetFont(), in_wrapper:GetFontHeight());
					GGF.InnerInvoke(in_meta, "ModifyInformDisplayOD", fontString);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.InformDisplay.wrappers = {};

GGC.InformDisplay.properties = {
	miscellaneous = {
		text = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Text,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetText(GGF.InnerInvoke(in_meta, "GetText"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyText", in_meta.object:GetText());
			end,
		}),
		interval = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Interval,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		status = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Status,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		errList = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ErrList,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.InformDisplay.elements = {
	runtime = {
		current = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Current,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		running = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Running,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGF.Mark(GGC.InformDisplay);

GGC.InformDisplay = GGF.TableSafeExtend(GGC.InformDisplay, GGC.DigitalDisplay);

GGF.CreateClassWorkaround(GGC.InformDisplay);

local This = GGC.InformDisplay;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Запуск
function This:Launch(in_meta)
	GGF.InnerInvoke(in_meta, "SetRunning", true);
	GGF.InnerInvoke(in_meta, "setNextValue");
end


-- Стоп
function This:Stop(in_meta)
	GGF.InnerInvoke(in_meta, "SetRunning", false);
end


-- Шаг
function This:setNextValue(in_meta)
	local current = GGF.InnerInvoke(in_meta, "GetCurrent");
	local list = GGF.InnerInvoke(in_meta, "GetErrList");
	if (current == "status") then
		if (#list ~= 0) then
			GGF.InnerInvoke(in_meta, "SetText", list[1]);
			GGF.InnerInvoke(in_meta, "SetCurrent", 1);
		end
	else
		if (current < #list) then
			GGF.InnerInvoke(in_meta, "SetText", list[current + 1])
			GGF.InnerInvoke(in_meta, "SetCurrent", current + 1);
		else
			GGF.InnerInvoke(in_meta, "SetText", GGF.InnerInvoke(in_meta, "GetStatus"));
			GGF.InnerInvoke(in_meta, "SetCurrent", "status");
		end
	end
	if (GGF.InnerInvoke(in_meta, "GetRunning") == true) then
		local timer = GGC.TriggerTimer:Create({
			time = 5,--self:GetInterval(),
			callback = function() GGF.InnerInvoke(in_meta, "setNextValue"); end
		});
	end
end