-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "ED";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   ErrorDisplaySuffix = GGF.DockDomName(GGD.DefineIdentifier, "ErrorDisplaySuffix"),
   ErrorDisplay = GGF.DockDomName(GGD.ClassIdentifier, "ErrorDisplay"),
};

local CTList = {
   ControlFrame = GGF.CreateCT(OBJSuffix, "ControlFrame", "Frame"),
   CodeDigitalDisplayWrapper = GGF.CreateCT(OBJSuffix, "CodeDigitalDisplayWrapper", "Frame"),
   SubcodeDigitalDisplayWrapper = GGF.CreateCT(OBJSuffix, "SubcodeDigitalDisplayWrapper", "Frame"),
   NoValueText = GGF.CreateCT(OBJSuffix, "NoValueText", "NoValueText"),
   FontHeight = GGF.CreateCT(OBJSuffix, "FontHeight", "FontHeight"),
   Interval = GGF.CreateCT(OBJSuffix, "Interval", "Interval"),
   Status = GGF.CreateCT(OBJSuffix, "Status", "Status"),
   ErrList = GGF.CreateCT(OBJSuffix, "ErrList", "ErrList"),
   ColorR = GGF.CreateCT(OBJSuffix, "ColorR", "ColorR"),
   ColorG = GGF.CreateCT(OBJSuffix, "ColorG", "ColorG"),
   ColorB = GGF.CreateCT(OBJSuffix, "ColorB", "ColorB"),
   ColorA = GGF.CreateCT(OBJSuffix, "ColorA", "ColorA"),
   Current = GGF.CreateCT(OBJSuffix, "Current", "Current"),
   Running = GGF.CreateCT(OBJSuffix, "Running", "Running"),
};

GGF.GRegister(DFList.ErrorDisplaySuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
   Frame = 0,
   Text = "",
   Interval = 5,
   Status = "HP",
   NoValueText = "--",
   FontHeight = 12,
   ColorR = 1.0,
   ColorG = 1.0,
   ColorB = 1.0,
   ColorA = 1.0,
   ErrList = {
      {
         code = "43",
         subcode = "C6"
      },
      {
         code = "56",
         subcode = "FF"
      }
   },
   Current = "status",
   Running = false,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.ErrorDisplay, {
   meta = {
      name = "Error Display",
      mark = "",  -- авт.
      suffix = GGD.ErrorDisplaySuffix,
   },
});

GGC.ErrorDisplay.objects = {
   frame = {
      controlFrame  = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Object,
         cte = CTList.ControlFrame,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.ControlFrame.Default)) then
               local controlFrame = CreateFrame(
                  "Frame",
                  GGF.GenerateOTName(
                     GGF.InnerInvoke(in_meta, "GetParentName"),
                     GGF.InnerInvoke(in_meta, "GetWrapperName"),
                     GGF.InnerInvoke(in_meta, "getClassSuffix")
                  ),
                  GGF.InnerInvoke(in_meta, "GetParent"),
                  GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     },
                  })
               );
               GGF.InnerInvoke(in_meta, "ModifyControlFrameOD", controlFrame);
            end
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.ErrorDisplay.wrappers = {
   digitalDisplay = {
      code = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.CodeDigitalDisplayWrapper,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      subcode = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.SubcodeDigitalDisplayWrapper,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.ErrorDisplay.properties = {
   miscellaneous = {
      noValueText = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.NoValueText,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      fontHeight = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.FontHeight,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            --in_meta.object:SetTextHeight(GGF.InnerInvoke(in_meta, "GetFontHeight"));
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      interval = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.Interval,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      status = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.Status,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      errList = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.ErrList,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
   color = {
      R = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.ColorR,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            GGF.InnerInvoke(in_meta, "AdjustColor");
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            GGF.InnerInvoke(in_meta, "SynchronizeColor");
         end,
      }),
      G = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.ColorG,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            GGF.InnerInvoke(in_meta, "AdjustColor");
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            GGF.InnerInvoke(in_meta, "SynchronizeColor");
         end,
      }),
      B = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.ColorB,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            GGF.InnerInvoke(in_meta, "AdjustColor");
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            GGF.InnerInvoke(in_meta, "SynchronizeColor");
         end,
      }),
      A = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.ColorA,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            GGF.InnerInvoke(in_meta, "AdjustColor");
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            GGF.InnerInvoke(in_meta, "SynchronizeColor");
         end,
      }),
   },
};

GGC.ErrorDisplay.elements = {
   runtime = {
      current = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.Current,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      running = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.Running,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGF.Mark(GGC.ErrorDisplay);

--GGC.NamedValueFontString = GGF.TableSafeExtend(GGC.NamedValueFontString, GGC.CustomContainer);
GGC.ErrorDisplay = GGF.TableSafeExtend(GGC.ErrorDisplay, GGC.StandardFrame);

GGF.CreateClassWorkaround(GGC.ErrorDisplay);

local This = GGC.ErrorDisplay;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-- Взятие цвета основного текста
function This:GetColor(in_meta)
   local color = {
      R = GGF.InnerInvoke(in_meta, "GetColorR"),
      G = GGF.InnerInvoke(in_meta, "GetColorG"),
      B = GGF.InnerInvoke(in_meta, "GetColorB"),
      A = GGF.InnerInvoke(in_meta, "GetColorA"),
   };
   return color;
end

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-- Установка основных объектов
function This:adjustServiceObjects(in_meta)
   GGF.OuterInvoke(
      GGF.InnerInvoke(in_meta, "GetCodeDigitalDisplayWrapper"),
      "Adjust"
   );
   GGF.OuterInvoke(
      GGF.InnerInvoke(in_meta, "GetSubcodeDigitalDisplayWrapper"),
      "Adjust"
   );
end

-- Установка цвета основного текста
function This:AdjustColor(in_meta)
   local color = GGF.InnerInvoke(in_meta, "GetColor");
   --in_meta.object:SetTextColor(color.R, color.G, color.B, color.A);
end

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-- Синхронизация цвета основного текста
function This:SynchronizeColor(in_meta)
   --local r, g, b, a = in_meta.object:GetTextColor();
   --GGF.InnerInvoke(in_meta, "ModifyColor", {
      --R = r,
      --G = g,
      --B = b,
      --A = a,
   --});
end

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

-- Создание основных объектов
function This:createServiceObjects(in_meta)
   local parent = in_meta.object;
   local color = GGF.InnerInvoke(in_meta, "GetColor");
   GGF.InnerInvoke(in_meta, "SetCodeDigitalDisplayWrapperOD", GGC.DigitalDisplay:Create({}, {
      properties = {
         base = {
            parent = function() return parent end,
            wrapperName = "C",
         },
         miscellaneous = {
            hidden = false,
            fontHeight = function() return GGF.InnerInvoke(in_meta, "GetFontHeight"); end
         },
         size = {
            width = function() return GGF.InnerInvoke(in_meta, "GetWidth")/2; end,
            height = function() return GGF.InnerInvoke(in_meta, "GetHeight"); end,
         },
         anchor = {
            point = GGE.PointType.Right,
            relativeTo = function() return parent end,
            relativePoint = GGE.PointType.Center,
            offsetX = 0,
            offsetY = 0,
         },
         color = color,
      },
   }));
   GGF.InnerInvoke(in_meta, "SetSubcodeDigitalDisplayWrapperOD", GGC.DigitalDisplay:Create({}, {
      properties = {
         base = {
            parent = function() return parent end,
            wrapperName = "S",
         },
         miscellaneous = {
            hidden = false,
            fontHeight = function() return GGF.InnerInvoke(in_meta, "GetFontHeight"); end
         },
         size = {
            width = function() return GGF.InnerInvoke(in_meta, "GetWidth")/2; end,
            height = function() return GGF.InnerInvoke(in_meta, "GetHeight"); end,
         },
         anchor = {
            point = GGE.PointType.Left,
            relativeTo = function() return parent end,
            relativePoint = GGE.PointType.Center,
            offsetX = 0,
            offsetY = 0,
         },
         color = color,
      },
   }));
end

-- Запуск
function This:Launch(in_meta)
   GGF.InnerInvoke(in_meta, "SetRunning", true);
   GGF.InnerInvoke(in_meta, "setNextValue");
end

-- Стоп
function This:Stop(in_meta)
   GGF.InnerInvoke(in_meta, "SetRunning", false);
end

-- Шаг
function This:setNextValue(in_meta)
   local current = GGF.InnerInvoke(in_meta, "GetCurrent");
   local list = GGF.InnerInvoke(in_meta, "GetErrList");
   local codeDD = GGF.InnerInvoke(in_meta, "GetCodeDigitalDisplayWrapper");
   local subcodeDD = GGF.InnerInvoke(in_meta, "GetSubcodeDigitalDisplayWrapper");
   local codeText = "";
   local subcodeText = "";
   if (current == "status") then
      if (#list ~= 0) then
         local data = list[1];
         codeText = data.code;
         subcodeText = data.subcode;
         GGF.InnerInvoke(in_meta, "SetCurrent", 1);
      end
   else
      if (current < #list) then
         local data = list[current + 1]
         codeText = data.code;
         subcodeText = data.subcode;
         GGF.InnerInvoke(in_meta, "SetCurrent", current + 1);
      else
         codeText = GGF.InnerInvoke(in_meta, "GetStatus");
         subcodeText = GGF.InnerInvoke(in_meta, "GetNoValueText");
         GGF.InnerInvoke(in_meta, "SetCurrent", "status");
      end
   end
   GGF.OuterInvoke(codeDD, "SetText", codeText);
   GGF.OuterInvoke(subcodeDD, "SetText", subcodeText);
   if (GGF.InnerInvoke(in_meta, "GetRunning") == true) then
      local timer = GGC.TriggerTimer:Create({
         time = 5,--self:GetInterval(),
         callback = function() GGF.InnerInvoke(in_meta, "setNextValue"); end
      });
   end
end