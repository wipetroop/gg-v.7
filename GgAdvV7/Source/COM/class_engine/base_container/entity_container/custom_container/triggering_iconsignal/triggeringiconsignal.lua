-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "TIS";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	TriggeringIconSignalSuffix = GGF.DockDomName(GGD.DefineIdentifier, "TriggeringIconSignalSuffix"),
	TriggeringIconSignal = GGF.DockDomName(GGD.ClassIdentifier, "TriggeringIconSignal"),
};

local CTList = {
	TextureFrame = GGF.CreateCT(OBJSuffix, "TextureFrame", "Frame"),
	StateList = GGF.CreateCT(OBJSuffix, "StateList", "StateList"),
	InitialState = GGF.CreateCT(OBJSuffix, "InitialState", "InitialState"),
   Interval = GGF.CreateCT(OBJSuffix, "Interval", "Interval"),
   ForwardStartDelay = GGF.CreateCT(OBJSuffix, "ForwardStartDelay", "ForwardStartDelay"),
   ForwardDuration = GGF.CreateCT(OBJSuffix, "ForwardDuration", "ForwardDuration"),
   ForwardEndDelay = GGF.CreateCT(OBJSuffix, "ForwardEndDelay", "ForwardEndDelay"),
   BackwardStartDelay = GGF.CreateCT(OBJSuffix, "BackwardStartDelay", "BackwardStartDelay"),
   BackwardDuration = GGF.CreateCT(OBJSuffix, "BackwardDuration", "BackwardDuration"),
   BackwardEndDelay = GGF.CreateCT(OBJSuffix, "BackwardEndDelay", "BackwardEndDelay"),
   StepCount = GGF.CreateCT(OBJSuffix, "StepCount", "StepCount"),
   Sound = GGF.CreateCT(OBJSuffix, "Sound", "Sound"),
	Animation = GGF.CreateCT(OBJSuffix, "Animation", "Animation"),
   CurrentCount = GGF.CreateCT(OBJSuffix, "CurrentCount", "CurrentCount"),
   AnimationState = GGF.CreateCT(OBJSuffix, "AnimationState", "AnimationState"),
   AnimationType = GGF.CreateCT(OBJSuffix, "AnimationType", "AnimationType"),
	CurrentState = GGF.CreateCT(OBJSuffix, "CurrentState", "CurrentState"),
   AnimationGroupForward = GGF.CreateCT(OBJSuffix, "AnimationGroupForward", "Frame"),
   AnimationGroupBackward = GGF.CreateCT(OBJSuffix, "AnimationGroupBackward", "Frame"),
   AnimationInstanceForward = GGF.CreateCT(OBJSuffix, "AnimationInstanceForward", "Frame"),
   AnimationInstanceBackward = GGF.CreateCT(OBJSuffix, "AnimationInstanceBackward", "Frame"),
};

GGF.GRegister(DFList.TriggeringIconSignalSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	StateList = {},
	InitialState = "",
   Interval = 0,
   StepCount = 0,
   ForwardStartDelay = function(in_meta) return GGF.InnerInvoke(in_meta, "GetInterval")/10 end,
   ForwardDuration = function(in_meta) return GGF.InnerInvoke(in_meta, "GetInterval")*3/10 end,
   ForwardEndDelay = function(in_meta) return 0 end,
   BackwardStartDelay = function(in_meta) return GGF.InnerInvoke(in_meta, "GetInterval")*2/10 end,
   BackwardDuration = function(in_meta) return GGF.InnerInvoke(in_meta, "GetInterval")*2/10 end,
   BackwardEndDelay = function(in_meta) return 0 end,
   Sound = "",
	Animation = "",
   CurrentCount = 0,
   AnimationState = GGE.BlinkAnimationState.Stopped,
   AnimationType = GGE.BlinkAnimationType.Cycled,
	CurrentState = "",
}, GGE.RegValType.Default);

--              FEDelay   BSDelay
--              ________|________            
--             /                 \           
--        FDur/                   \BDur      
-- __________/                     \_________
-- FSDelay                           BEDelay

GGF.GRegister(DFList.TriggeringIconSignal, {
	meta = {
		name = "Triggering Icon Signal",
		mark = "",	-- авт.
		suffix = GGD.TriggeringIconSignalSuffix,
	},
});

GGC.TriggeringIconSignal.objects = {
	frame = {
		texture = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.TextureFrame,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.TextureFrame.Default)) then
					--local texture = CreateFrame("Frame", GGF.GenerateOTName(in_wrapper:GetParentName() ,in_wrapper:GetWrapperName(), in_wrapper:getClassSuffix()), in_wrapper:GetParent(), GGF.TernExpSingle(in_wrapper:IsTemplated(), nil, in_wrapper:GetTemplate()));
					local texture = GGF.InnerInvoke(in_meta, "GetParent"):CreateTexture(
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyTextureFrameOD", texture);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.TriggeringIconSignal.properties = {
	miscellaneous = {
		stateList = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.StateList,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		initialState = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.InitialState,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "SetState", GGF.InnerInvoke(in_meta, "GetInitialState"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	anitronic = {
      animationType = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.AnimationType,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      time = {
         interval = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.Interval,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         forwardStartDelay = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.ForwardStartDelay,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         forwardDuration = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.ForwardDuration,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         forwardEndDelay = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.ForwardEndDelay,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         backwardStartDelay = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.BackwardStartDelay,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         backwardDuration = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.BackwardDuration,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         backwardEndDelay = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.BackwardEndDelay,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
      },
      stepCount = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.StepCount,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      sound = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.Sound,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
	},
};

GGC.TriggeringIconSignal.elements = {
	runtime = {
		animation = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Element,
			cte = CTList.Animation,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		currentState = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Element,
			cte = CTList.CurrentState,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
      anitronic = {
         currentCount = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.CurrentCount,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         animationState = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.AnimationState,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         animationGroupForward = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.AnimationGroupForward,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         animationGroupBackward = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.AnimationGroupBackward,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         animationInstanceForward = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.AnimationInstanceForward,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         animationInstanceBackward = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.AnimationInstanceBackward,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
      },
	},
};

GGF.Mark(GGC.TriggeringIconSignal);

--GGC.TriggeringIconSignal = GGF.TableSafeExtend(GGC.TriggeringIconSignal, GGC.CustomContainer);
GGC.TriggeringIconSignal = GGF.TableSafeExtend(GGC.TriggeringIconSignal, GGC.StandardTexture);

GGF.CreateClassWorkaround(GGC.TriggeringIconSignal);

local This = GGC.TriggeringIconSignal;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Переход между состояниями
function This:SetState(in_meta, in_stateToken)
	local stateList = GGF.InnerInvoke(in_meta, "GetStateList");
	local currentState = GGF.InnerInvoke(in_meta, "GetCurrentState");
	--print("set state:", currentState, "->", in_stateToken);
	if (stateList[in_stateToken]) then
		if (currentState ~= in_stateToken) then
			if (currentState ~= GGF.GetDefaultValue(OBJSuffix, CTList.CurrentState.Default)) then
				stateList[currentState].onDeactivate(in_meta);
			end
			--stateList[currentState].animation:Stop();
			GGF.InnerInvoke(in_meta, "SetCurrentStateOD", in_stateToken);
			if (stateList[in_stateToken]) then
				stateList[in_stateToken].onActivate(in_meta);
            GGF.InnerInvoke(in_meta, "SetAlpha", stateList[in_stateToken].alpha);
				--stateList[in_stateToken].animation:Start();
				--print("set file:", stateList[in_stateToken].texture, in_stateToken);
				GGF.InnerInvoke(in_meta, "SetFile", stateList[in_stateToken].texture);
			end
		end
	else
		-- TODO: присобачить исключение(GG-57)
	end
end


-- Переход между состояниями
function This:SetAlpha(in_meta, in_alpha)
	in_meta.object:SetAlpha(in_alpha);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Переход между состояниями
function This:IsBlinking(in_meta)
	local stateList = GGF.InnerInvoke(in_meta, "GetStateList");
	ATLASSERT(stateList);
	return stateList[GGF.InnerInvoke(in_meta, "GetCurrentState")].blinking;
end


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

--
function This:commitAlpha(in_meta)
   local stateList = GGF.InnerInvoke(in_meta, "GetStateList");
   ATLASSERT(stateList);
   GGF.InnerInvoke(in_meta, "SetAlpha", stateList[GGF.InnerInvoke(in_meta, "GetCurrentState")].alpha);
end

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

function This:prepareStageForward(in_meta)
   local frame = in_meta.object;
   local interval = GGF.InnerInvoke(in_meta, "GetInterval");
   local sound = GGF.InnerInvoke(in_meta, "GetSound");
   local animationGroupForward = GGF.InnerInvoke(in_meta, "GetAnimationGroupForward");
   local animationInstanceForward = GGF.InnerInvoke(in_meta, "GetAnimationInstanceForward");

   animationInstanceForward:SetOrder(1);
   --animationGroupForward:SetLooping("BOUNCE");
   GGF.VersionSplitterProc({
      {
         versionInterval = {GGD.TocVersionList.Vanilla, GGD.TocVersionList.Draenor},
         action = function()
            animationInstanceForward:SetChange(1);
         end,
      },
      {
         versionInterval = {GGD.TocVersionList.Legion, GGD.TocVersionList.Invalid},
         action = function()
            animationInstanceForward:SetFromAlpha(0);
            animationInstanceForward:SetToAlpha(1);
         end,
      },
   });
   --a4:SetDuration(interval/7);
   animationInstanceForward:SetDuration(GGF.InnerInvoke(in_meta, "GetForwardDuration"));
   --a4:SetStartDelay(interval/7);
   animationInstanceForward:SetStartDelay(GGF.InnerInvoke(in_meta, "GetForwardStartDelay"));

   animationGroupForward:SetScript("OnFinished", function()
      --animationGroupForward:Restart();
      frame:SetAlpha(1);
      GGF.InnerInvoke(in_meta, "animationStageBackward");
   end)
end

function This:prepareStageBackward(in_meta)
   local frame = in_meta.object;
   local interval = GGF.InnerInvoke(in_meta, "GetInterval");
   local sound = GGF.InnerInvoke(in_meta, "GetSound");
   local animationGroupBackward = GGF.InnerInvoke(in_meta, "GetAnimationGroupBackward");
   local animationInstanceBackward = GGF.InnerInvoke(in_meta, "GetAnimationInstanceBackward");

   animationInstanceBackward:SetOrder(-1);
   GGF.VersionSplitterProc({
      {
         versionInterval = {GGD.TocVersionList.Vanilla, GGD.TocVersionList.Draenor},
         action = function()
            animationInstanceBackward:SetChange(-1);
         end,
      },
      {
         versionInterval = {GGD.TocVersionList.Legion, GGD.TocVersionList.Invalid},
         action = function()
            animationInstanceBackward:SetFromAlpha(1);
            animationInstanceBackward:SetToAlpha(0);
         end,
      },
   });
   --a4:SetDuration(interval/7);
   animationInstanceBackward:SetDuration(GGF.InnerInvoke(in_meta, "GetBackwardDuration"));
   --a4:SetStartDelay(interval/7);
   animationInstanceBackward:SetStartDelay(GGF.InnerInvoke(in_meta, "GetBackwardStartDelay"));

   animationGroupBackward:SetScript("OnFinished", function()
      --animationGroupBackward:Restart();
      frame:SetAlpha(0);
      GGF.InnerInvoke(in_meta, "animationStageForward");
   end)
end

-- Стык для зацикленной анимации
function This:animationStageForward(in_meta)
   if (GGF.InnerInvoke(in_meta, "GetAnimationState") == GGE.BlinkAnimationState.Stopped) then
      GGF.InnerInvoke(in_meta, "commitAlpha");
      return;
   end

   if (GGF.InnerInvoke(in_meta, "GetAnimationType") == GGE.BlinkAnimationType.Limited) then
      local cc = GGF.InnerInvoke(in_meta, "GetCurrentCount");
      if (cc <= 0) then
         GGF.InnerInvoke(in_meta, "SetAnimationState", GGE.BlinkAnimationState.Stopped);
         GGF.InnerInvoke(in_meta, "commitAlpha");
         return;
      end
      GGF.InnerInvoke(in_meta, "SetCurrentCount", cc - 1);
   end

   local interval = GGF.InnerInvoke(in_meta, "GetInterval");
   local sound = GGF.InnerInvoke(in_meta, "GetSound");
   local animationGroupForward = GGF.InnerInvoke(in_meta, "GetAnimationGroupForward");

   -- TODO: как-то бы заменить это дело, а то много объектов плодится...
   local timer = GGC.TriggerTimer:Create({
      time = interval/14,
      callback = function() PlaySoundFile(sound) end
   });
   --C_Timer.NewTimer(interval/14, function() PlaySoundFile(sound) end);

   animationGroupForward:Play();
end

function This:animationStageBackward(in_meta)
   local animationGroupBackward = GGF.InnerInvoke(in_meta, "GetAnimationGroupBackward");
   --PlaySoundFile(sound);

   animationGroupBackward:Play();
end

-- Запуск анимации
function This:Launch(in_meta)
   if (GGF.InnerInvoke(in_meta, "GetAnimationState") == GGE.BlinkAnimationState.Running) then
      return;
   end
   local frame = in_meta.object;
   local animationGroupForward = frame:CreateAnimationGroup();
   local animationGroupBackward = frame:CreateAnimationGroup();
   GGF.InnerInvoke(in_meta, "SetAnimationGroupForward", animationGroupForward);
   GGF.InnerInvoke(in_meta, "SetAnimationInstanceForward", animationGroupForward:CreateAnimation("Alpha"));
   GGF.InnerInvoke(in_meta, "SetAnimationGroupBackward", animationGroupBackward);
   GGF.InnerInvoke(in_meta, "SetAnimationInstanceBackward", animationGroupBackward:CreateAnimation("Alpha"));
   GGF.InnerInvoke(in_meta, "prepareStageForward");
   GGF.InnerInvoke(in_meta, "prepareStageBackward");

   GGF.InnerInvoke(in_meta, "SetAnimationState", GGE.BlinkAnimationState.Running);
   if (GGF.InnerInvoke(in_meta, "GetAnimationType") == GGE.BlinkAnimationType.Limited) then
      GGF.InnerInvoke(in_meta, "SetCurrentCount", GGF.InnerInvoke(in_meta, "GetStepCount"));
   end

   GGF.InnerInvoke(in_meta, "animationStageForward");
end

-- Функция остановки анимации
function This:Stop(in_meta)
   GGF.InnerInvoke(in_meta, "SetAnimationState", GGE.BlinkAnimationState.Stopped);
end