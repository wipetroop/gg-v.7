-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "CS";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	CompositeSliderSuffix = GGF.DockDomName(GGD.DefineIdentifier, "CompositeSliderSuffix"),
	CompositeSlider = GGF.DockDomName(GGD.ClassIdentifier, "CompositeSlider"),
};

local CTList = {
	ControlFrameWrapper = GGF.CreateCT(OBJSuffix, "ControlFrameWrapper", "Frame"),
	CompositeSlider = GGF.CreateCT(OBJSuffix, "CompositeSlider", "Frame"),
	LabelFontStringDirectLink = GGF.CreateCT(OBJSuffix, "LabelFontStringDirectLink", "Frame"),
	LowtextFontStringDirectLink = GGF.CreateCT(OBJSuffix, "LowtextFontStringDirectLink", "Frame"),
	HightextFontStringDirectLink = GGF.CreateCT(OBJSuffix, "HightextFontStringDirectLink", "Frame"),
	ManualEditBoxWrapper = GGF.CreateCT(OBJSuffix, "ManualEditBoxWrapper", "Frame"),
	Template = GGF.CreateCT(OBJSuffix, "Template", "Template"),
	Value = GGF.CreateCT(OBJSuffix, "Value", "Value"),
	Title = GGF.CreateCT(OBJSuffix, "Title", "Title"),
	SliderWD = GGF.CreateCT(OBJSuffix, "SliderWidthDifference", "SliderWidthDifference"),
	SliderHeight = GGF.CreateCT(OBJSuffix, "SliderHeight", "SliderHeight"),
	TextHeight = GGF.CreateCT(OBJSuffix, "TextHeight", "TextHeight"),
	TextWidth = GGF.CreateCT(OBJSuffix, "TextWidth", "TextWidth"),
	EditBoxText = GGF.CreateCT(OBJSuffix, "EditBoxText", "EditBoxText"),
	OnSliderValueChanged = GGF.CreateCT(OBJSuffix, "OnSliderValueChanged", "OnSliderValueChanged"),
};

GGF.GRegister(DFList.CompositeSliderSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	Title = "",
	OnSliderValueChanged = function(in_meta, in_slider, in_val)
		--local value = math.floor(in_val*100 + 0.5)/100;
		local value = math.floor(in_val);
		ATLASSERT(in_meta.object == in_slider);

		in_meta.object:SetValue(value);
		--print("val changed:", value);
		GGF.InnerInvoke(in_meta, "UpdateText", value);
		GGF.InnerInvoke(in_meta, "ModifyValue", value);
		GGF.InnerInvoke(in_meta, "CounterGetOnValueChanged")(value);	-- запуск коллбека во внешку
	end,
	SliderWidthDifference = 20,
	SliderHeight = 15,
	TextHeight = 15,
	TextWidth = 150,
	EditBoxText = 0,
	Template = "OptionsSliderTemplate",
}, GGE.RegValType.Default);

GGF.GRegister(DFList.CompositeSlider, {
	meta = {
		name = "Composite Slider",
		mark = "",	-- авт.
		suffix = GGD.CompositeSliderSuffix,
	},
});

GGC.CompositeSlider.objects = {
	slider = {
		standard = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.CompositeSlider,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.CompositeSlider.Default)) then
					local slider = CreateFrame(
						"Slider",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyCompositeSliderOD", slider);
					--CreateFrame("Slider", in_wrapper:GetParentName() .. "_" .. in_wrapper:GetWrapperName() .. "_S", in_wrapper:GetParent(), in_wrapper:GetTemplate());
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.CompositeSlider.wrappers = {
	frame = {
		control = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.ControlFrameWrapper,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	fontString = {
		label = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.LabelFontStringDirectLink,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		lowtext = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.LowtextFontStringDirectLink,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		hightext = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.HightextFontStringDirectLink,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	editBox = {
		standard = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.ManualEditBoxWrapper,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				--in_object:GetManualEditBox():SetScript("OnEnterPressed", function(...) in_object:GetOnEditBoxEnterPressedCounter()(in_object, ...) end);
				--in_object:GetManualEditBox():SetScript("OnEscapePressed", function(...) in_object:GetOnEditBoxEscapePressedCounter()(in_object, ...) end);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.CompositeSlider.properties = {
	base = {
		template = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Template,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	miscellaneous = {
		title = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Title,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},

	callback = {
		-- NOTE: не менять, не удалять!!!
		onSliderValueChanged = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnSliderValueChanged,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetScript("OnValueChanged", function(...) GGF.InnerInvoke(in_meta, "CounterGetOnSliderValueChanged")(in_meta, ...) end);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				-- NOTE: вот ту хз...
			end,
		}),
	},
};

GGC.CompositeSlider.elements = {
	runtime = {
		sliderWidthDifference = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Element,
			cte = CTList.SliderWD,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "adjustSliderSize");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		sliderHeight = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Element,
			cte = CTList.SliderHeight,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "adjustSliderSize");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		textHeight = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Element,
			cte = CTList.TextHeight,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "adjustLabelSize");
				GGF.InnerInvoke(in_meta, "adjustLowtextSize");
				GGF.InnerInvoke(in_meta, "adjustHightextSize");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		textWidth = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Element,
			cte = CTList.TextWidth,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "adjustLabelSize");
				GGF.InnerInvoke(in_meta, "adjustLowtextSize");
				GGF.InnerInvoke(in_meta, "adjustHightextSize");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		editBoxText = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Element,
			cte = CTList.EditBoxText,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetManualEditBoxWrapper"), "SetText", GGF.InnerInvoke(in_meta, "GetEditBoxText"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGF.Mark(GGC.CompositeSlider);

GGC.CompositeSlider = GGF.TableSafeExtend(GGC.CompositeSlider, GGC.StandardSlider);

GGF.CreateClassWorkaround(GGC.CompositeSlider);

local This = GGC.CompositeSlider;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие габаритов текста
function This:GetTextSize(in_meta)
	local size = {
		width = GGF.InnerInvoke(in_meta, "GetTextWidth"),
		height = GGF.InnerInvoke(in_meta, "GetTextHeight"),
	};
	return size;
end


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- NOTE: расширено, относительно функционала базового AdjustValues
-- Установка предельных значений слайдера
function This:AdjustValues(in_meta)
	local values = GGF.InnerInvoke(in_meta, "GetValues");
	in_meta.object:SetMinMaxValues(values.min, values.max);
	in_meta.object:SetValue(GGF.InnerInvoke(in_meta, "GetValue"));
	GGF.InnerInvoke(in_meta, "UpdateLabels");
end


-- Установка габаритов контрольного окна
function This:adjustControlSize(in_meta)
	local size = GGF.InnerInvoke(in_meta, "GetSize");
	in_meta.object:SetSize(size.width, size.height);
end


-- Установка габаритов слайдера
function This:adjustSliderSize(in_meta)
	local size = GGF.InnerInvoke(in_meta, "GetSize");
	in_meta.object:SetSize(size.width - GGF.InnerInvoke(in_meta, "GetSliderWidthDifference"), GGF.InnerInvoke(in_meta, "GetSliderHeight"));	-- TODO: вот тут надо уточнить
end


-- Установка габаритов строки названия
function This:adjustLabelSize(in_meta)
	local size = GGF.InnerInvoke(in_meta, "GetTextSize");
	in_meta.object:SetSize(size.width, size.height);
end


-- Установка габаритов строки нижнего значения
function This:adjustLowtextSize(in_meta)
	local size = GGF.InnerInvoke(in_meta, "GetTextSize");
	in_meta.object:SetSize(size.width, size.height);
end


-- Установка габаритов строки верхнего значения
function This:adjustHightextSize(in_meta)
	local size = GGF.InnerInvoke(in_meta, "GetTextSize");
	in_meta.object:SetSize(size.width, size.height);
end



-- Установка основных объектов эвослайдера
function This:adjustServiceObjects(in_meta)
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetControlFrameWrapper"), "Adjust");
	--self:GetLabelFontStringWrapper():Adjust();
	--self:GetLowtextFontStringWrapper():Adjust();
	--self:GetHightextFontStringWrapper():Adjust();
	--local inst = GGF.InnerInvoke(in_meta, "GetManualEditBoxWrapper");
	--print(GGF.GetObjectSuffix(inst), GGF.GetObjectBySuffix(GGF.GetObjectSuffix(inst)).Invoke);
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetManualEditBoxWrapper"), "Adjust");
end


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Резерв
function This:rezerv(in_meta)
	GGF.InnerInvoke(in_meta, "SetLabelFontStringWrapperOD", GGC.FontString:Create({}, {
		properties = {
			base = {
				parent = in_object,
				wrapperName = "LBL",
			},
			miscellaneous = {
				hidden = false,
				text = GGF.InnerInvoke(in_meta, "GetTitle"),
			},
			size = {
				width = GGF.InnerInvoke(in_meta, "GetTextWidth"),
				height = GGF.InnerInvoke(in_meta, "GetTextHeight"),
			},
			anchor = {
				point = GGE.PointType.Center,
				relativeTo = in_object,
				relativePoint = GGE.PointType.Center,
				offsetX = 0,
				offsetY = 0,
			},
		},
	}));

	GGF.InnerInvoke(in_meta, "SetLowtextFontStringWrapperOD", GGC.FontString:Create({}, {
		properties = {
			base = {
				parent = in_object,
				wrapperName = "LT",
			},
			miscellaneous = {
				hidden = false,
				text = tostring(GGF.InnerInvoke(in_meta, "GetValuesMin")),
			},
			size = {
				width = GGF.InnerInvoke(in_meta, "GetTextWidth"),
				height = GGF.InnerInvoke(in_meta, "GetTextHeight"),
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = in_object,
				relativePoint = GGE.PointType.Bottom,
				offsetX = 2,
				offsetY = 3,
			},
		},
	}));

	GGF.InnerInvoke(in_meta, "SetHightextFontStringWrapperOD", GGC.FontString:Create({}, {
		properties = {
			base = {
				parent = in_object,
				wrapperName = "HT",
			},
			miscellaneous = {
				hidden = false,
				text = tostring(GGF.InnerInvoke(in_meta, "GetValuesMax")),
			},
			size = {
				width = GGF.InnerInvoke(in_meta, "GetTextWidth"),
				height = GGF.InnerInvoke(in_meta, "GetTextHeight"),
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = in_object,
				relativePoint = GGE.PointType.Bottom,
				offsetX = 2,
				offsetY = 3,
			},
		},
	}));
end

-- Создание основных объектов эвослайдера
function This:createServiceObjects(in_meta)
	local parentObjName = GGF.GenerateOTName(
		GGF.InnerInvoke(in_meta, "GetParentName"),
		GGF.InnerInvoke(in_meta, "GetWrapperName"),
		GGF.InnerInvoke(in_meta, "getClassSuffix")
	);
	GGF.InnerInvoke(in_meta, "SetControlFrameWrapperOD", GGC.LayoutFrame:Create({}, {
		properties = {
			base = {
				parent = GGF.InnerInvoke(in_meta, "GetParent"),
				wrapperName = "C",
			},
			miscellaneous = {
				hidden = GGF.InnerInvoke(in_meta, "GetHidden"),
			},
			size = {
				width = GGF.InnerInvoke(in_meta, "GetSize").width,
				height = GGF.InnerInvoke(in_meta, "GetSize").height,
			},
			anchor = {
				point = GGF.InnerInvoke(in_meta, "GetAnchor").point,
				relativeTo = GGF.InnerInvoke(in_meta, "GetAnchor").relativeTo,
				relativePoint = GGF.InnerInvoke(in_meta, "GetAnchor").relativePoint,
				offsetX = GGF.InnerInvoke(in_meta, "GetAnchor").offsetX,
				offsetY = GGF.InnerInvoke(in_meta, "GetAnchor").offsetY,
			},
		},
	}));

	GGF.InnerInvoke(in_meta, "SetLabelFontStringDirectLinkOD", _G[parentObjName.."Text"]);

	GGF.InnerInvoke(in_meta, "SetLowtextFontStringDirectLinkOD", _G[parentObjName.."Low"]);

	GGF.InnerInvoke(in_meta, "SetHightextFontStringDirectLinkOD", _G[parentObjName.."High"]);
	-- TODO: разобраться в необходимости
	GGF.InnerInvoke(in_meta, "SetManualEditBoxWrapperOD", GGC.EditBox:Create({}, {
		properties = {
			base = {
				parent = in_object,
				wrapperName = "M",
				--template = "InputBoxTemplate",
			},
			miscellaneous = {
				hidden = false,
				isNumeric = false,	-- HACK: для отрицательных значений
				number = function() return tonumber(GGF.InnerInvoke(in_meta, "GetValue")) end,
				text = function() return tostring(GGF.InnerInvoke(in_meta, "GetValue")) end,
			},
			size = {
				width = 70,
				height = 14,
			},
			anchor = {
				point = GGE.PointType.Top,
				relativeTo = in_object,
				relativePoint = GGE.PointType.Bottom,
				offsetX = 0,
				offsetY = 0,
			},
			backdrop = GGD.Backdrop.EditBox,
			callback = {
				onEnterPressed = function(in_meta, in_editBox)
					--local self = frame.obj;
					local value = GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetManualEditBoxWrapper"), "GetUniversalValue");
					--if self.ispercent then
						--value = value:gsub('%%', '');
						--value = tonumber(value) / 100;
					--else
						value = tonumber(value);
					--end
					if (value) then
						PlaySound(856) -- SOUNDKIT.IG_MAINMENU_OPTION_CHECKBOX_ON
						GGF.InnerInvoke(in_meta, "SetValue", value);
					end
				end,
				--onEscapePressed = function(in_object, in_EditBox)
					--in_editBox:ClearFocus();
				--end
			},
		},
	}));
end


-- TODO: сделать управляемое округление
function This:UpdateText(in_meta, in_value)
	--local value = self.value or 0
	--if self.ispercent then
		--self:GetGGC.EditBox():SetText(("%s%%"):format(floor(value * 1000 + 0.5) / 10))
	--else
		--self:GetManualEditBoxWrapper():SetUniversalValue(math.floor(in_value * 100 + 0.5) / 100)
		GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetManualEditBoxWrapper"), "SetUniversalValue", math.floor(in_value));
	--end
end


function This:UpdateLabels(in_meta)
	local min, max = (GGF.InnerInvoke(in_meta, "GetValuesMin") or 0), (GGF.InnerInvoke(in_meta, "GetValuesMax") or 100);
	--if self.ispercent then
		--self.lowtext:SetFormattedText("%s%%", (min * 100))
		--self.hightext:SetFormattedText("%s%%", (max * 100))
	--else
	GGF.InnerInvoke(in_meta, "GetLowtextFontStringDirectLink"):SetText(min);
	GGF.InnerInvoke(in_meta, "GetHightextFontStringDirectLink"):SetText(max);
	--end
end
