-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


-- NOTE: пока что не трогаем
-- NOTE: очень похоже, что PCG был отключен, и на данном этапе не используется
GGC.PanelControlGroup = {
	meta = {
		name = "Panel Control Group",
	},
};

GGC.PanelControlGroup.Object = {
	inlineGroup = {
		standard = {
			assembly = GGC.ParameterContainer:Create({
				ptype = GGE.ParameterType.Value,
				ctype = GGE.ContainerType.Object,
				cte = "AssemblyInlineGroup",
				fixed = false,
				parameter = {
					value = 0,
				},

				[GGE.RuntimeMethod.Adjuster] = function(in_object)
					in_object:GetAssemblyInlineGroup():PauseLayout();
					in_object:GetAssemblyInlineGroup():SetParent(in_object:GetParent());
					in_object:GetAssemblyInlineGroup():SetRelativeTo(in_object:GetParent().content);
				end,
			}),
		},
	},
	button = {
		standard = {
			reset = GGC.ParameterContainer:Create({
				ptype = GGE.ParameterType.Value,
				ctype = GGE.ContainerType.Object,
				cte = "ResetButton",
				fixed = false,
				parameter = {
					value = 0,
				},

				[GGE.RuntimeMethod.Adjuster] = function(in_object)
					in_object:GetResetButton():SetParent(in_object:GetAssemblyInlineGroup():GetObject());
					in_object:GetResetButton():SetRelativeTo(in_object:GetAssemblyInlineGroup():GetObjectFrame());
					--in_object:GetResetButton():SetAllPoints(in_object:GetAssemblyInlineGroup():GetObjectFrame());
					--in_object:GetAssemblyInlineGroup():AddChild(in_object:GetResetButton():GetObject());
				end,
			}),
		},
	},
	checkbox = {
		standard = {
			visibility = GGC.ParameterContainer:Create({
				ptype = GGE.ParameterType.Value,
				ctype = GGE.ContainerType.Object,
				cte = "VisibilityCheckbox",
				fixed = false,
				parameter = {
					value = 0,
				},

				[GGE.RuntimeMethod.Adjuster] = function(in_object)
					--in_object:GetVisibilityCheckbox():SetAllPoints(in_object:GetAssemblyInlineGroup():GetObjectFrame());
					in_object:GetVisibilityCheckbox():SetParent(in_object:GetAssemblyInlineGroup():GetObject());
					in_object:GetVisibilityCheckbox():SetRelativeTo(in_object:GetAssemblyInlineGroup():GetObjectFrame());
					--in_object:GetAssemblyInlineGroup():AddChild(in_object:GetVisibilityCheckbox():GetObject());
				end,
			}),
			charspec = GGC.ParameterContainer:Create({
				ptype = GGE.ParameterType.Value,
				ctype = GGE.ContainerType.Object,
				cte = "CharspecCheckbox",
				fixed = false,
				parameter = {
					value = 0,
				},

				[GGE.RuntimeMethod.Adjuster] = function(in_object)
					--in_object:GetCharspecCheckbox():SetAllPoints(in_object:GetAssemblyInlineGroup():GetObjectFrame());
					in_object:GetCharspecCheckbox():SetParent(in_object:GetAssemblyInlineGroup():GetObject());
					in_object:GetCharspecCheckbox():SetRelativeTo(in_object:GetAssemblyInlineGroup():GetObjectFrame());
					--in_object:GetAssemblyInlineGroup():AddChild(in_object:GetCharspecCheckbox():GetObject());
				end,
			}),
		},
	},
	slider = {
		standard = {
			offsetX = GGC.ParameterContainer:Create({
				ptype = GGE.ParameterType.Value,
				ctype = GGE.ContainerType.Object,
				cte = "OffsetXSlider",
				fixed = false,
				parameter = {
					value = 0,
				},

				[GGE.RuntimeMethod.Adjuster] = function(in_object)
					--in_object:GetOffsetXSlider():SetAllPoints(in_object:GetAssemblyInlineGroup():GetObjectFrame());
					in_object:GetOffsetXSlider():SetParent(in_object:GetAssemblyInlineGroup():GetObject());
					in_object:GetOffsetXSlider():SetRelativeTo(in_object:GetAssemblyInlineGroup():GetObjectFrame());
					--in_object:GetAssemblyInlineGroup():AddChild(in_object:GetOffsetXSlider():GetObject());
				end,
			}),
			offsetY = GGC.ParameterContainer:Create({
				ptype = GGE.ParameterType.Value,
				ctype = GGE.ContainerType.Object,
				cte = "OffsetYSlider",
				fixed = false,
				parameter = {
					value = 0,
				},

				[GGE.RuntimeMethod.Adjuster] = function(in_object)
					--in_object:GetOffsetYSlider():SetAllPoints(in_object:GetAssemblyInlineGroup():GetObjectFrame());
					in_object:GetOffsetYSlider():SetParent(in_object:GetAssemblyInlineGroup():GetObject());
					in_object:GetOffsetYSlider():SetRelativeTo(in_object:GetAssemblyInlineGroup():GetObjectFrame());
					--in_object:GetAssemblyInlineGroup():AddChild(in_object:GetOffsetYSlider():GetObject());
				end,
			}),
		},
	},
};

GGC.PanelControlGroup.properties = {
};

GGC.PanelControlGroup.elements = {
};

GGF.Mark(GGC.PanelControlGroup);

GGC.PanelControlGroup = GGF.TableSafeExtend(GGC.PanelControlGroup, GGC.WrapperAggregator);

local This = GGC.PanelControlGroup;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------