-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local Config = {
	ACLButtonOffsetX = 10,
	ACLButtonOffsetY = -30,

	AFButtonOffsetX = -10,
	AFButtonOffsetY = -30,

	ButtonWidth = 122,
	ButtonHeight = 29,

	MainFieldTopOffsetY = -60,

	FieldOffset = 5,
	SliderWidth = 20,
	PhantomHeight = 50,	-- NOTE: вообще пофигу на самом деле
};

local OBJSuffix = "AGF";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	FilterType = GGF.DockDomName(GGD.EnumerationIdentifier, "FilterType"),
	AggregatorFrameSuffix = GGF.DockDomName(GGD.DefineIdentifier, "AggregatorFrameSuffix"),
	AggregatorFrame = GGF.DockDomName(GGD.ClassIdentifier, "AggregatorFrame"),
};

local CTList = {
	AggregatorFrame = GGF.CreateCT(OBJSuffix, "AggregatorFrame", "Frame"),
	InnerStandardFrameWrapper = GGF.CreateCT(OBJSuffix, "InnerStandardFrameWrapper", "Frame"),
	PhantomStandardFrameWrapper = GGF.CreateCT(OBJSuffix, "PhantomStandardFrameWrapper", "Frame"),
	VerticalStandardSliderWrapper = GGF.CreateCT(OBJSuffix, "VerticalStandardSliderWrapper", "Frame"),
	TitleFontStringWrapper = GGF.CreateCT(OBJSuffix, "TitleFontStringWrapper", "Frame"),
	AddCombatLogStuckingButtonWrapper = GGF.CreateCT(OBJSuffix, "AddCombatLogStuckingButtonWrapper", "Frame"),
	AddFilterStuckingButtonWrapper = GGF.CreateCT(OBJSuffix, "AddFilterStuckingButtonWrapper", "Frame"),
	SliderValue = GGF.CreateCT(OBJSuffix, "SliderValue", "SliderValue"),
	FilterList = GGF.CreateCT(OBJSuffix, "FilterList", "FilterList"),
	GeneratedLogTokens = GGF.CreateCT(OBJSuffix, "GeneratedLogTokens", "GeneratedLogTokens"),
	Width = GGF.CreateCT(OBJSuffix, "Width", "Width"),
	Height = GGF.CreateCT(OBJSuffix, "Height", "Height"),
	Point = GGF.CreateCT(OBJSuffix, "Point", "Point"),
	RelativeTo = GGF.CreateCT(OBJSuffix, "RelativeTo", "RelativeTo"),
	RelativePoint = GGF.CreateCT(OBJSuffix, "RelativePoint", "RelativePoint"),
	OffsetX = GGF.CreateCT(OBJSuffix, "OffsetX", "OffsetX"),
	OffsetY = GGF.CreateCT(OBJSuffix, "OffsetY", "OffsetY"),
	OnACLClick = GGF.CreateCT(OBJSuffix, "OnACLClick", "EmptyCallback"),
	OnAFClick = GGF.CreateCT(OBJSuffix, "OnAFClick", "EmptyCallback"),
	OnACLStateChanged = GGF.CreateCT(OBJSuffix, "OnACLStateChanged", "EmptyCallback"),
	OnAFStateChanged = GGF.CreateCT(OBJSuffix, "OnAFStateChanged", "EmptyCallback"),
	OnSliderValueChanged = GGF.CreateCT(OBJSuffix, "OnSliderValueChanged", "EmptyCallback"),
};

GGF.GRegister(DFList.AggregatorFrameSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	SliderValue = 0,
	Width = 0,
	Height = 0,
	Point = GGE.PointType.TopLeft,
	RelativeTo = function(in_meta)
		return UIParent;
	end,
	RelativePoint = GGE.PointType.TopLeft,
	OffsetX = 0,
	OffsetY = 0,
	EmptyCallback = function(in_meta) end,
	FilterList = {
		logTitle = {},
		log = {},
		dataTitle = {},
		data = {},
	},
	GeneratedLogTokens = {},
}, GGE.RegValType.Default);

-- TODO: проверить, используется ли еще
GGF.GRegister(DFList.FilterType, {
	Log = 1,
	Data = 2,
});

GGF.GRegister(DFList.AggregatorFrame, {
	meta = {
		name = "Aggregator Frame",
		mark = "",	-- авт.
		suffix = GGD.AggregatorFrameSuffix,
	},

	vs_generatedLogTokens = {},
});

GGC.AggregatorFrame.objects = {
	frame = {
		aggregator = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.AggregatorFrame,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.AggregatorFrame.Default)) then
					local aggregatorFrame = CreateFrame(
						"Frame",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyAggregatorFrameOD", aggregatorFrame);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.AggregatorFrame.wrappers = {
	frame = {
		inner = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.InnerStandardFrameWrapper,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		phantom = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.PhantomStandardFrameWrapper,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	slider = {
		vertical = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.VerticalStandardSliderWrapper,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_metae)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	fontString = {
		title = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.TitleFontStringWrapper,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	button = {
		addCombatLog = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.AddCombatLogStuckingButtonWrapper,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		addFilter = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.AddFilterStuckingButtonWrapper,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.AggregatorFrame.properties = {
	miscellaneous = {
		value = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.SliderValue,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},

	callback = {
		onACLClick = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnACLClick,
			fixed = true,

			-- NOTE: тестовый, анлочить для отладки
			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				--local callback = in_wrapper:GetOnACLClickCounter();
				--in_wrapper:GetAddCombatLogStuckingButtonWrapper():SetOnClickOD(callback);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onAFClick = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnAFClick,
			fixed = true,

			-- NOTE: тестовый, анлочить для отладки
			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				--local callback = in_wrapper:GetOnAFClickCounter();
				--in_wrapper:GetAddFilterStuckingButtonWrapper():SetOnClickOD(callback);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onACLStateChanged = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnACLStateChanged,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				-- NOTE: пока так
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onAFStateChanged = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnAFStateChanged,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				-- NOTE: пока так
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.AggregatorFrame.elements = {
	runtime = {
		filterList = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Element,
			cte = CTList.FilterList,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		generatedLogTokens = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Element,
			cte = CTList.GeneratedLogTokens,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGF.Mark(GGC.AggregatorFrame);

GGC.AggregatorFrame = GGF.TableSafeExtend(GGC.AggregatorFrame, GGC.StandardFrame);

GGF.CreateClassWorkaround(GGC.AggregatorFrame);

local This = GGC.AggregatorFrame;


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Установка смещения фантома
function This:setPhantomOffset(in_meta, in_offset)
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetPhantomStandardFrameWrapper")
		"SetOffsetY",
		in_offset
	);
	GGF.InnerInvoke(in_meta, "geometryRefresh");
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие активных фильтров нужного типа
function This:GetActiveFilterList(in_meta, in_type)
	local isLog = (in_type == GGE.FilterType.Log);
	local fListToken = GGF.TernExpSingle(isLog, "log", "data");
	local filterList = GGF.InnerInvoke(in_meta, "GetFilterList");
	local fList = filterList[fListToken];

	local filterList = {};
	for ind,filter in ipairs(fList) do
		if (GGF.OuterInvoke(filter, "GetFilterEnabled")) then
			local fData = GGF.OuterInvoke(filter, "GetFilterData");
			filterList[#filterList + 1] = GGF.TableCopy(fData);
		end
	end

	return filterList;
end


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Создание основных объектов фильтрфрейма
function This:adjustServiceObjects(in_meta)
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetInnerStandardFrameWrapper"),
		"Adjust"
	);
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetPhantomStandardFrameWrapper"),
		"Adjust"
	);
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetVerticalStandardSliderWrapper"),
		"Adjust"
	);
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetTitleFontStringWrapper"),
		"Adjust"
	);
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetAddCombatLogStuckingButtonWrapper"),
		"Adjust"
	);
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetAddFilterStuckingButtonWrapper"),
		"Adjust"
	);
	local filterList = GGF.InnerInvoke(in_meta, "GetFilterList");
	GGF.OuterInvoke(filterList.logTitle, "Adjust");
	GGF.OuterInvoke(filterList.dataTitle, "Adjust");
	GGF.InnerInvoke(in_meta, "geometryRefresh");
end


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Создание основных объектов фильтрфрейма
function This:createServiceObjects(in_meta)
	GGF.InnerInvoke(in_meta, "SetInnerStandardFrameWrapper", GGC.StandardFrame:Create({}, {
		properties = {
			base = {
				parent = in_meta.object,
				wrapperName = "I",
			},
			miscellaneous = {
				anchorType = GGE.AnchorType.Double,
			},
			size = {
				width = GGF.InnerInvoke(in_meta, "TrLinkGetWidth"),
				height = GGF.InnerInvoke(in_meta, "TrLinkGetHeight"),
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = in_meta.object,
				relativePoint = GGE.PointType.TopLeft,
				offsetX = Config.FieldOffset,
				offsetY = Config.MainFieldTopOffsetY,
			},
			secondAnchor = {
				point = GGE.PointType.BottomRight,
				relativeTo = in_meta.object,
				relativePoint = GGE.PointType.BottomRight,
				offsetX = - Config.FieldOffset - Config.SliderWidth,
				offsetY = Config.FieldOffset,
			},
			--backdrop = GGD.Backdrop.Empty,
			backdrop = GGD.Backdrop.Segregate,
		},
	}));
	local isf = GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetInnerStandardFrameWrapper"));
	GGF.InnerInvoke(in_meta, "SetPhantomStandardFrameWrapper", GGC.StandardFrame:Create({}, {
		properties = {
			base = {
				parent = isf,
				wrapperName = "P",
			},
			miscellaneous = {
				anchorType = GGE.AnchorType.Double,
			},
			size = {
				--width = self:GetWidthTrLink() - 2*Config.FieldOffset - Config.SliderWidth,
				height = Config.PhantomHeight,
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = isf,
				relativePoint = GGE.PointType.TopLeft,
				offsetX = 0,
				offsetY = 0,
			},
			secondAnchor = {
				point = GGE.PointType.TopRight,
				relativeTo = isf,
				relativePoint = GGE.PointType.TopRight,
				offsetX = 0,
				offsetY = 0,
			},
			--backdrop = GGD.Backdrop.Empty,
			backdrop = GGD.Backdrop.Segregate,
		},
	}));

	GGF.InnerInvoke(in_meta, "SetVerticalStandardSliderWrapper", GGC.StandardSlider:Create({}, {
		properties = {
			base = {
				parent = in_meta.object,
				wrapperName = "V",
				--template = "OptionsSliderTemplate",
				template = "UIPanelScrollBarTemplate",
			},
			miscellaneous = {
				enabled = true,
				orientation = GGE.SliderOrientation.Vertical,
				--thumbTexture = "Interface\\Buttons\\UI-SliderBar-Button-Vertical",
				anchorType = GGE.AnchorType.Double,
			},
			values = {
				min = 0.0,
				max = 100.0,
				step = 1.0,
			},
			size = {
				width = Config.SliderWidth,
				--height = 15,
			},
			-- WARNING: костыли связаны с некорректностью геометрии шаблонного слайдера
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = isf,
				relativePoint = GGE.PointType.TopRight,
				offsetX = 0,
				offsetY = - Config.SliderWidth,
			},
			secondAnchor = {
				point = GGE.PointType.BottomRight,
				relativeTo = isf,
				relativePoint = GGE.PointType.BottomRight,
				offsetX = Config.SliderWidth,
				offsetY = Config.SliderWidth,
			},
			backdrop = GGD.Backdrop.VerticalSlider,
			callback = {
				onValueChanged = function(in_value) print(value); GGF.InnerInvoke(in_meta, "setPhantomOffset", GGF.InnerInvoke(in_meta, "GetSliderValue")) end,
				onMouseUp = function(in_frame) end,
				onMouseWheel = function(in_frame) end,
			},
		},
	}));
	--self:GetVerticalStandardSliderWrapper():GetStandardSlider():SetHitRectInsets(0, 0, -10, 0);
	--self:GetVerticalStandardSliderWrapper():GetStandardSlider():SetFrameStrata("FULLSCREEN_DIALOG");

	GGF.InnerInvoke(in_meta, "SetTitleFontStringWrapper", GGC.FontString:Create({}, {
		properties = {
			base = {
				parent = in_meta.object,
				wrapperName = "T",
			},
			miscellaneous = {
				hidden = false,
				text = "Filter Settings",
			},
			size = {
				-- TODO: поставить нормальные параметры
				width = 180,
				height = 12,
			},
			anchor = {
				point = GGE.PointType.Top,
				relativeTo = in_meta.object,
				relativePoint = GGE.PointType.Top,
				offsetX = 0,
				offsetY = - Config.FieldOffset,
			},
		},
	}));

	GGF.InnerInvoke(in_meta, "SetAddCombatLogStuckingButtonWrapper", GGC.StuckingButton:Create({}, {
		properties = {
			base = {
				parent = in_meta.object,
				wrapperName = "ACL",
			},
			miscellaneous = {
				text = "Add Combat Log",
				--tooltipText = "GUi_MW_F_DS_F_DS_F_ACL_TS",
			},
			size = {
				width = Config.ButtonWidth,
				height = Config.ButtonHeight,
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = in_meta.object,
				relativePoint = GGE.PointType.TopLeft,
				offsetX = Config.ACLButtonOffsetX,
				offsetY = Config.ACLButtonOffsetY,
			},
			callback = {
				onStateChanged = GGF.InnerInvoke(in_meta, "TrLinkGetOnACLStateChanged"),
				--onEnter = function(...) MSC_CONFIG.on_frame_entered(...) end,
				--onLeave = function(...) MSC_CONFIG.on_frame_leaved(...) end,
			},
		},
	}));

	GGF.InnerInvoke(in_meta, "SetAddFilterStuckingButtonWrapper", GGC.StuckingButton:Create({}, {
		properties = {
			base = {
				parent = in_meta.object,
				wrapperName = "AF",
			},
			miscellaneous = {
				text = "Add Filter",
				--tooltipText = "GUi_MW_F_DS_F_DS_F_ACL_TS",
			},
			size = {
				width = Config.ButtonWidth,
				height = Config.ButtonHeight,
			},
			anchor = {
				point = GGE.PointType.TopRight,
				relativeTo = in_meta.object,
				relativePoint = GGE.PointType.TopRight,
				offsetX = Config.AFButtonOffsetX,
				offsetY = Config.AFButtonOffsetY,
			},
			callback = {
				onStateChanged = GGF.InnerInvoke(in_meta, "TrLinkGetOnAFStateChanged"),
				--onEnter = function(...) MSC_CONFIG.on_frame_entered(...) end,
				--onLeave = function(...) MSC_CONFIG.on_frame_leaved(...) end,
			},
		},
	}));

	local parent = GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetPhantomStandardFrameWrapper"));
	--local ltObject = CreateFrame("Button", parentName .. "_LLEC_B", parent, "UiPanelButtonTemplate");	-- Log list expand/collapse
	--local dtObject = CreateFrame("Button", parentName .. "_DLEC_B", parent, "UiPanelButtonTemplate");	-- Data list expand/collapse

	local offsetX = 10;

	local filterList = GGF.InnerInvoke(in_meta, "GetFilterList");

	filterList.logTitle = GGC.StuckingButton:Create({}, {
		properties = {
			base = {
				parent = parent,
				--template = "UIPanelButtonTemplate",
				wrapperName = "FLLT",
			},
			miscellaneous = {
				hidden = false,
				enabled = true,
				highlighted = false,
				text = "",
				--tooltipText = "Expand/Collapse log filter list",
				anchorType = GGE.AnchorType.Double,
			},
			-- NOTE: специально оставлено пустым(чек рефреша геометрии)
			--backdrop = GGD.Backdrop.Empty,
			size = {
				--width = function() return self:GetAggregatorFrame():GetWidth() - 2*offsetX; end,
				height = 20,
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = parent,
				relativePoint = GGE.PointType.TopLeft,
				offsetX = Config.FieldOffset,
				offsetY = - Config.FieldOffset,
			},
			secondAnchor = {
				point = GGE.PointType.TopRight,
				relativeTo = parent,
				relativePoint = GGE.PointType.TopRight,
				offsetX = - Config.FieldOffset,
				offsetY = - Config.FieldOffset,
			},
			callback = {
				onStateChanged = function() GGF.InnerInvoke(in_meta, "on_FLLT_STB_stateChanged") end,
				--onEnter = function(...) GGM.GUI.CommonService:OnMouseFocusFrame(...) end,
				--onLeave = function(...) GGM.GUI.CommonService:OnMouseDefocusFrame(...) end,
			},
		},
	});

	filterList.dataTitle = GGC.StuckingButton:Create({}, {
		properties = {
			base = {
				parent = parent,
				wrapperName = "FLDT",
			},
			miscellaneous = {
				hidden = false,
				enabled = true,
				highlighted = false,

				-- NOTE: специально оставлено пустым(чек рефреша геометрии)
				text = "",
				--tooltipText = "Expand/Collapse data filter list",
				anchorType = GGE.AnchorType.Double,
			},
			--backdrop = GGD.Backdrop.Empty,
			size = {
				--width = function() return self:GetAggregatorFrame():GetWidth() - 2*offsetX end,
				height = 20,
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = parent,
				relativePoint = GGE.PointType.TopLeft,
				offsetX = Config.FieldOffset,
				offsetY = 0,
			},
			secondAnchor = {
				point = GGE.PointType.TopRight,
				relativeTo = parent,
				relativePoint = GGE.PointType.TopRight,
				offsetX = - Config.FieldOffset,
				offsetY = 0,
			},
			callback = {
				onStateChanged = function() GGF.InnerInvoke(in_meta, "on_FLDT_STB_stateChanged") end,
				--onEnter = function(...) GGM.GUI.CommonService:OnMouseFocusFrame(...) end,
				--onLeave = function(...) GGM.GUI.CommonService:OnMouseDefocusFrame(...) end,
			},
		},
	});
end


-- Подсчет фильтров по шаблону(фильтры данных/фильтры логов)
function This:CountFiltersByPattern(in_meta, in_type, ...)
	local pattern = {...};
	local isLog = (in_type == GGE.FilterType.Log);
	local fListToken = GGF.TernExpSingle(isLog, "log", "data");
	local filterList = GGF.InnerInvoke(in_meta, "GetFilterList");
	local fList = filterList[fListToken];

	-- NOTE: карта быстрого доступа к полям данных фильтра
	local fdTable = {
		["log"] = {
			[1] = "battlefieldSignature",
			[2] = "startTime",
			[3] = "finishTime",
		},
		["data"] = {
			[1] = "class",
			[2] = "type",
			[3] = "subtype",
		},
	};
	local ln1, ln2, ln3 = pattern[1], pattern[2], pattern[3];
	local count = 0;
	for ind,filter in ipairs(fList) do
		local fData = GGF.OuterInvoke(filter, "GetFilterData");
		if (ln1 == fData[fdTable[fListToken][1]]) then
			if ((not ln2) or (ln2 == fData[fdTable[fListToken][2]])) then
				if ((not ln3) or (ln3 == fData[fdTable[fListToken][3]])) then
					count = count + 1;
				end
			end
		end
	end

	return count;
end


-- Добавление нового фильтра
function This:AddNewFilter(in_meta, in_type, in_ftSignature)
	local isLog = (in_type == GGE.FilterType.Log);
	local offsetX = 10;
	local filterList = GGF.InnerInvoke(in_meta, "GetFilterList");
	local fList = filterList[GGF.TernExpSingle(isLog, "log", "data")];
	local phwrp = GGF.InnerInvoke(in_meta, "GetPhantomFrameWrapper");
	--local fObject = CreateFrame("Frame", parentName .. "_FO_F_" .. tostring(#fList + 1), parent);
	fList[#fList + 1] = GGC.FilterFrame:Create({}, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(phwrp),
				wrapperName = "FO_" .. tostring(#fList + 1),
			},
			miscellaneous = {
				hidden = false,
				type = in_type,
				token = "F-" .. GGF.TernExpSingle(isLog, "LG", "DT") .. "-TP-" .. GGF.InnerInvoke(in_meta, "generateLogToken"),
			},
			filterData = GGF.TableCopy(in_ftSignature),	-- NOTE: обязательно копировать!! иначе туда лезут текущие значения селектора
			backdrop = GGD.Backdrop.Nested,
			size = {
				width = GGF.OuterInvoke(phwrp, "GetWidth") - 2*offsetX,
				height = 120,
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = GGF.INS.GetObjectRef(phwrp),
				relativePoint = GGE.PointType.TopLeft,
				offsetX = offsetX,
				offsetY = -10,
			},
			callback = {
				onRemove = function(in_objType, in_token)
					GGF.InnerInvoke(in_meta, "removeFilter", in_objType, in_token);
				end
			},
		},
	});

	GGF.OuterInvoke(fList[#fList], "Adjust");

	GGF.InnerInvoke(in_meta, "geometryRefresh");
end


-- Удаление активного фильтра
function This:removeFilter(in_meta, in_type, in_token)
	local isLog = (in_type == GGE.FilterType.Log);
	local fListToken = GGF.TernExpSingle(isLog, "log", "data");
	local filterList = GGF.InnerInvoke(in_meta, "GetFilterList");
	local fList = filterList[fListToken];

	local delIdx = 0;
	--local filterList = {};
	for ind,filter in ipairs(fList) do
		local token = filter:GetToken();
		if (token == in_token) then
			delIdx = ind;
		end
	end
	ATLASSERT(delIdx ~= 0);

	local sz = #fList;
	fList[delIdx]:SetHidden(true);
	fList[delIdx] = nil;
	for idx=delIdx,sz do
		fList[idx] = fList[idx+1];
	end

	GGF.InnerInvoke(in_meta, "geometryRefresh");
end


-- Удаление активного фильтра
function This:geometryRefresh(in_meta)
	local vPos = 0;
	local vGap = 10;
	-- NOTE: резерв, так-то у этого объекта поинт не меняется
	--self.filterList.logTitle:SetOffsetY();
	local filterList = GGF.InnerInvoke(in_meta, "GetFilterList");
	vPos = vPos - GGF.OuterInvoke(filterList.logTitle, "GetHeight") - vGap;
	local lList = filterList.log;
	local lTitle = filterList.logTitle;
	GGF.OuterInvoke(
		lTitle,
		"SetHidden",
		GGF.InnerInvoke(
			in_meta,
			"checkListElementCollision",
			0,
			GGF.OuterInvoke(lTitle, "GetHeight")
		)
	);

	if (GGF.OuterInvoke(lTitle, "GetState")) then
		GGF.OuterInvoke(lTitle, "SetText", "\\\\ Log Filters (" .. tostring(#lList) .. ") //");
		for i=1,#lList do
			GGF.OuterInvoke(lList[i], "SetOffsetY", vPos);
			GGF.OuterInvoke(
				lList[i],
				"SetHidden",
				GGF.InnerInvoke(
					in_meta,
					"checkListElementCollision",
					vPos,
					GGF.OuterInvoke(lList[i], "GetHeight")
				)
			);
			vPos = vPos - GGF.OuterInvoke(lList[i], "GetHeight") - vGap;
		end
	else
		GGF.OuterInvoke(lTitle, "SetText", "> Log Filters (" .. tostring(#lList) .. ") >");
		for i=1,#lList do
			GGF.OuterInvoke(lList[i], "SetHidden", true);
		end
	end

	local dTitle = filterList.dataTitle;
	GGF.OuterInvoke(dTitle, "SetOffsetY", vPos);
	GGF.OuterInvoke(dTitle, "SetSecondOffsetY", vPos);
	GGF.OuterInvoke(
		dTitle,
		"SetHidden",
		GGF.InnerInvoke(
			in_meta,
			"checkListElementCollision",
			vPos,
			GGF.OuterInvoke(dTitle, "GetHeight")
		)
	);
	vPos = vPos - GGF.OuterInvoke(filterList.dataTitle, "GetHeight") - vGap;

	local dList = filterList.data;
	if (GGF.OuterInvoke(dTitle, "GetState")) then
		GGF.OuterInvoke(dTitle, "SetText", "\\\\ Data Filters (" .. tostring(#dList) .. ") //");
		for i=1,#dList do
			GGF.OuterInvoke(dList[i], "SetOffsetY", vPos);
			GGF.OuterInvoke(
				dList[i],
				"SetHidden",
				GGF.InnerInvoke(
					in_meta,
					"checkListElementCollision",
					vPos,
					GGF.OuterInvoke(dList[i], "GetHeight")
				)
			);
			vPos = vPos - dList[i]:GetHeight() - vGap;
		end
	else
		GGF.OuterInvoke(dTitle, "SetText", "> Data Filters (" .. tostring(#dList) .. ") >");
		for i=1,#dList do
			GGF.OuterInvoke(dList[i], "SetHidden", true);
		end
	end
	-- TODO: узнать, зачем это..
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetPhantomStandardFrameWrapper"), "SetHeight", - vPos);

	local winHeight = GGF.InnerInvoke(in_meta, "GetHeight");

	--NOTE: min всегда 0, объекты располагаются вниз
	local sliderMax = GGF.TernExpSingle(winHeight < - vPos , - vPos - winHeight, 0);	-- NOTE: разность высот фрейма и фантома

	--self:GetVerticalStandardSliderWrapper():SetValues({
		--min = 0,
		--max = sliderMax,
		--step = 1,
		--stepsPerPage = 1,
	--});
end


-- Проверка коллизии элемента с видимым фреймом
function This:checkListElementCollision(in_meta, in_offset, in_height)
	local phantomOffset = GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetPhantomStandardFrameWrapper"), "GetOffsetY");
	local maxHeight = GGF.InnerInvoke(in_meta, "GetHeight");
	if (- phantomOffset - in_offset < 0 or - phantomOffset - in_offset + in_height > maxHeight) then
		return true;
	end
	return false;
end


-- Обработчик нажатия на экспандер списка фильтров логов
function This:on_FLLT_STB_stateChanged(in_meta)
	--self.filterList.logTitle:SetHighlighted(not self.filterList.logTitle:GetHighlighted());
	GGF.InnerInvoke(in_meta, "geometryRefresh");
end


-- Обработчик нажатия на экспандер списка фильтров данных
function This:on_FLDT_STB_stateChanged(in_meta)
	--self.filterList.dataTitle:SetHighlighted(not self.filterList.dataTitle:GetHighlighted());
	GGF.InnerInvoke(in_meta, "geometryRefresh");
end


-- Генерация токена текущего лога(идентификатор)
function This:generateLogToken(in_meta)
	local token = ""
	local generatedTokens = GGF.InnerInvoke(in_meta, "GetGeneratedLogTokens");
	while (token == "") do
		local array = {}
		array[1] = string.char(math.random(48, 57))		-- 0..9
		for i = 2, 4 do
			array[i] = string.char(math.random(65, 90))	-- A..Z
		end
		for i = 5, 6 do
			array[i] = string.char(math.random(48, 57))	-- 0..9
		end
		token = table.concat(array);
		if generatedTokens[token] then
			token = "";
		else
			generatedTokens[token] = true;
		end
	end
	return token;
end