-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

local OBJSuffix = "GF";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	GraphicFrameSuffix = GGF.DockDomName(GGD.DefineIdentifier, "GraphicFrameSuffix"),
	GraphicFrame = GGF.DockDomName(GGD.ClassIdentifier, "GraphicFrame"),
};

local CTList = {
	GraphicFrame = GGF.CreateCT(OBJSuffix, "GraphicFrame", "Frame"),
	GridColorR = GGF.CreateCT(OBJSuffix, "GridColorR", "GridColorR"),
	GridColorG = GGF.CreateCT(OBJSuffix, "GridColorG", "GridColorG"),
	GridColorB = GGF.CreateCT(OBJSuffix, "GridColorB", "GridColorB"),
	GridColorA = GGF.CreateCT(OBJSuffix, "GridColorA", "GridColorA"),
	AxisDrawingX = GGF.CreateCT(OBJSuffix, "AxisDrawingX", "AxisDrawingX"),
	AxisDrawingY = GGF.CreateCT(OBJSuffix, "AxisDrawingY", "AxisDrawingY"),
	AxisColorR = GGF.CreateCT(OBJSuffix, "AxisColorR", "AxisColorR"),
	AxisColorG = GGF.CreateCT(OBJSuffix, "AxisColorG", "AxisColorG"),
	AxisColorB = GGF.CreateCT(OBJSuffix, "AxisColorB", "AxisColorB"),
	AxisColorA = GGF.CreateCT(OBJSuffix, "AxisColorA", "AxisColorA"),
	YLabelsLeft = GGF.CreateCT(OBJSuffix, "YLabelsLeft", "YLabelsLeft"),
	YLabelsRight = GGF.CreateCT(OBJSuffix, "YLabelsRight", "YLabelsRight"),
	GridSpacingX = GGF.CreateCT(OBJSuffix, "GridSpacingX", "GridSpacingX"),
	GridSpacingY = GGF.CreateCT(OBJSuffix, "GridSpacingY", "GridSpacingY"),
	XAIStart = GGF.CreateCT(OBJSuffix, "XAxisIntervalStart", "XAxisIntervalStart"),
	XAIFinish = GGF.CreateCT(OBJSuffix, "XAxisIntervalFinish", "XAxisIntervalFinish"),
	YAIStart = GGF.CreateCT(OBJSuffix, "YAxisIntervalStart", "YAxisIntervalStart"),
	YAIFinish = GGF.CreateCT(OBJSuffix, "YAxisIntervalFinish", "YAxisIntervalFinish"),
	Width = GGF.CreateCT(OBJSuffix, "Width", "Width"),
	Height = GGF.CreateCT(OBJSuffix, "Height", "Height"),
	Point = GGF.CreateCT(OBJSuffix, "Point", "Point"),
	RelativeTo = GGF.CreateCT(OBJSuffix, "RelativeTo", "RelativeTo"),
	RelativePoint = GGF.CreateCT(OBJSuffix, "RelativePoint", "RelativePoint"),
	OffsetX = GGF.CreateCT(OBJSuffix, "OffsetX", "OffsetX"),
	OffsetY = GGF.CreateCT(OBJSuffix, "OffsetY", "OffsetY"),
};

GGF.GRegister(DFList.GraphicFrameSuffix, OBJSuffix, GGE.RegValType.Suffix);	-- NOTE: LF уже есть

GGF.GRegister(DFList.Default, {
	Frame = 0,
	GridColorR = 0.5,
	GridColorG = 0.5,
	GridColorB = 0.5,
	GridColorA = 0.5,
	AxisDrawingX = true,
	AxisDrawingY = true,
	AxisColorR = 1.0,
	AxisColorG = 1.0,
	AxisColorB = 1.0,
	AxisColorA = 1.0,
	YLabelsLeft = true,
	YLabelsRight = true,
	GridSpacingX = 0.25,
	GridSpacingY = 0.25,
	XAxisIntervalStart = 0,
	XAxisIntervalFinish = 1,
	YAxisIntervalStart = 0,
	YAxisIntervalFinish = 1,
	Width = 800,
	Height = 200,
	Point = GGE.PointType.TopLeft,
	RelativeTo = function(in_meta) return UIParent end,
	RelativePoint = GGE.PointType.TopLeft,
	OffsetX = 0,
	OffsetY = 0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.GraphicFrame, {
	meta = {
		name = "Graphic Frame",
		mark = "",	-- авт.
		suffix = GGD.GraphicFrameSuffix,
	},
});

GGC.GraphicFrame.objects = {
	frame = {
		graphic = GGC.ParameterContainer:Create({
			ptype = GGE.ParameterType.Value,
			ctype = GGE.ContainerType.Property,
			cte = CTList.GraphicFrame,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_object == GGF.GetDefaultValue(OBJSuffix, CTList.GraphicFrame.Default)) then
					local graphicFrame = CreateFrame(
						"Frame",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyGraphicFrameOD", graphicFrame);
					--print("standardframe cr:", standardFrame);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.GraphicFrame.wrappers = {};

GGC.GraphicFrame.properties = {
	geometry = {
		gridColor = {
			R = GGC.ParameterContainer:Create({
				ptype = GGE.ParameterType.Value,
				ctype = GGE.ContainerType.Property,
				cte = CTList.GridColorR,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustGridColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			G = GGC.ParameterContainer:Create({
				ptype = GGE.ParameterType.Value,
				ctype = GGE.ContainerType.Property,
				cte = CTList.GridColorG,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustGridColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			B = GGC.ParameterContainer:Create({
				ptype = GGE.ParameterType.Value,
				ctype = GGE.ContainerType.Property,
				cte = CTList.GridColorB,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustGridColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			A = GGC.ParameterContainer:Create({
				ptype = GGE.ParameterType.Value,
				ctype = GGE.ContainerType.Property,
				cte = CTList.GridColorA,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustGridColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
		axisDrawing = {
			X = GGC.ParameterContainer:Create({
				ptype = GGE.ParameterType.Value,
				ctype = GGE.ContainerType.Property,
				cte = CTList.AxisDrawingX,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustAxisDrawing");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			Y = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.AxisDrawingY,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustAxisDrawing");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
		axisColor = {
			R = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.AxisColorR,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustAxisColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			G = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.AxisColorG,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustAxisColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			B = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.AxisColorB,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustAxisColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			A = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.AxisColorA,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustAxisColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
		yLabels = {
			left = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.YLabelsLeft,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustYLabels");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			right = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.YLabelsRight,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustYLabels");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
		gridSpacing = {
			X = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.GridSpacingX,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustGridSpacing");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			Y = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.GridSpacingY,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustGridSpacing");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
		xAxisInterval = {
			start = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.XAIStart,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustXAxisInterval");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			finish = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.XAIFinish,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustXAxisInterval");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
		yAxisInterval = {
			start = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.YAIStart,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustYAxisInterval");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			finish = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.YAIFinish,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustYAxisInterval");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
	},
};

GGF.Mark(GGC.GraphicFrame);

GGC.GraphicFrame = GGF.TableSafeExtend(GGC.GraphicFrame, GGC.StandardFrame);

GGF.CreateClassWorkaround(GGC.GraphicFrame);

local This = GGC.GraphicFrame;


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-- Смена цвета сетки
function This:ModifyGridColor(in_meta, in_color)
	if (in_color ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyGridColorR", in_color.R);
		GGF.InnerInvoke(in_meta, "ModifyGridColorG", in_color.G);
		GGF.InnerInvoke(in_meta, "ModifyGridColorB", in_color.B);
		GGF.InnerInvoke(in_meta, "ModifyGridColorA", in_color.A);
	end
end


-- Смена цвета сетки
function This:ModifyAxisDrawing(in_meta, in_axis)
	if (in_axis ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyAxisDrawingX", in_axis.X);
		GGF.InnerInvoke(in_meta, "ModifyAxisDrawingY", in_axis.Y);
	end
end


-- Смена цвета осей координат
function This:ModifyAxisColor(in_meta, in_color)
	if (in_color ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyAxisColorR", in_color.R);
		GGF.InnerInvoke(in_meta, "ModifyAxisColorG", in_color.G);
		GGF.InnerInvoke(in_meta, "ModifyAxisColorB", in_color.B);
		GGF.InnerInvoke(in_meta, "ModifyAxisColorA", in_color.A);
	end
end


-- Смена отрисовки левой и правой(дублирующей) осей OY
function This:ModifyYLabels(in_meta, in_labels)
	if (in_labels ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyYLabelsLeft", in_labels.left);
		GGF.InnerInvoke(in_meta, "ModifyYLabelsRight", in_labels.right);
	end
end


-- Смена спейсинга по всем осям(подгон масштаба)
function This:ModifyGridSpacing(in_meta, in_gs)
	if (in_gs ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyGridSpacingX", in_gs.X);
		GGF.InnerInvoke(in_meta, "ModifyGridSpacingY", in_gs.Y);
	end
end


-- Смена интервала оси OX
function This:ModifyXAxisInterval(in_meta, in_int)
	if (in_int ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyXAxisIntervalStart", in_int.start);
		GGF.InnerInvoke(in_meta, "ModifyXAxisIntervalFinish", in_int.finish);
	end
end


-- Смена интервала оси OY
function This:ModifyYAxisInterval(in_meta, in_int)
	if (in_int ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyYAxisIntervalStart", in_int.start);
		GGF.InnerInvoke(in_meta, "ModifyYAxisIntervalFinish", in_int.finish);
	end
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Смена цвета сетки
function This:SetGridColor(in_meta, in_color)
	GGF.InnerInvoke(in_meta, "ModifyGridColor", in_color);
	GGF.InnerInvoke(in_meta, "AdjustGridColor");
end


-- Смена цвета сетки
function This:SetAxisDrawing(in_meta, in_axis)
	GGF.InnerInvoke(in_meta, "ModifyAxisDrawing", in_axis);
	GGF.InnerInvoke(in_meta, "AdjustAxisDrawing");
end


-- Смена цвета осей координат
function This:SetAxisColor(in_meta, in_color)
	GGF.InnerInvoke(in_meta, "ModifyAxisColor", in_color)
	GGF.InnerInvoke(in_meta, "AdjustAxisColor");
end


-- Смена отрисовки левой и правой(дублирующей) осей OY
function This:SetYLabels(in_meta, in_labels)
	GGF.InnerInvoke(in_meta, "ModifyYLabels", in_labels);
	GGF.InnerInvoke(in_meta, "AdjustYLabels");
end


-- Смена спейсинга по всем осям(подгон масштаба)
function This:SetGridSpacing(in_meta, in_gs)
	GGF.InnerInvoke(in_meta, "ModifyGridSpacing", in_gs);
	GGF.InnerInvoke(in_meta, "AdjustGridSpacing");
end


-- Смена интервала оси OX
function This:SetXAxisInterval(in_meta, in_int)
	GGF.InnerInvoke(in_meta, "ModifyXAxisInterval", in_int);
	GGF.InnerInvoke(in_meta, "AdjustXAxisInterval");
end


-- Смена интервала оси OY
function This:SetYAxisInterval(in_meta, in_int)
	GGF.InnerInvoke(in_meta, "ModifyYAxisInterval", in_int);
	GGF.InnerInvoke(in_meta, "AdjustYAxisInterval");
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие цвета сетки
function This:GetGridColor(in_meta)
	local color = {
		R = GGF.InnerInvoke(in_meta, "GetGridColorR"),
		G = GGF.InnerInvoke(in_meta, "GetGridColorG"),
		B = GGF.InnerInvoke(in_meta, "GetGridColorB"),
		A = GGF.InnerInvoke(in_meta, "GetGridColorA"),
	};
	return color;
end


-- Взятие отрисовки осей координат
function This:GetAxisDrawing(in_meta)
	local axisDrawing = {
		X = GGF.InnerInvoke(in_meta, "GetAxisDrawingX"),
		Y = GGF.InnerInvoke(in_meta, "GetAxisDrawingY"),
	};
	return axisDrawing;
end


-- Взятие цвета осей координат
function This:GetAxisColor(in_meta)
	local axisColor = {
		R = GGF.InnerInvoke(in_meta, "GetAxisColorR"),
		G = GGF.InnerInvoke(in_meta, "GetAxisColorG"),
		B = GGF.InnerInvoke(in_meta, "GetAxisColorB"),
		A = GGF.InnerInvoke(in_meta, "GetAxisColorA"),
	};
	return axisColor;
end


-- Взятие отрисовки левой и правой(дублирующей) осей OY
function This:GetYLabels(in_meta)
	local yLabels = {
		left = GGF.InnerInvoke(in_meta, "GetYLabelsLeft"),
		right = GGF.InnerInvoke(in_meta, "GetYLabelsRight"),
	};
	return yLabels;
end


-- Взятие спейсинга по всем осям(подгон масштаба)
function This:GetGridSpacing(in_meta)
	local gridSpacing = {
		X = GGF.InnerInvoke(in_meta, "GetGridSpacingX"),
		Y = GGF.InnerInvoke(in_meta, "GetGridSpacingY"),
	};
	return gridSpacing;
end


-- Взятие интервала оси OX
function This:GetXAxisInterval(in_meta)
	local xAxisInterval = {
		start = GGF.InnerInvoke(in_meta, "GetXAxisIntervalStart"),
		finish = GGF.InnerInvoke(in_meta, "GetXAxisIntervalFinish"),
	};
	return xAxisInterval;
end


-- Взятие интервала оси OY
function This:GetYAxisInterval(in_meta)
	local yAxisInterval = {
		start = GGF.InnerInvoke(in_meta, "GetYAxisIntervalStart"),
		finish = GGF.InnerInvoke(in_meta, "GetYAxisIntervalFinish"),
	};
	return yAxisInterval;
end


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Установка цвета сетки
function This:AdjustGridColor(in_meta)
	local color = GGF.InnerInvoke(in_meta, "GetGridColor");
	in_meta.object:SetGridColor({color.R, color.G, color.B, color.A});
end


-- Установка отрисовки осей координат
function This:AdjustAxisDrawing(in_meta)
	local axisDrawing = GGF.InnerInvoke(in_meta, "GetAxisDrawing");
	in_meta.object:SetAxisDrawing(axisDrawing.X, axisDrawing.Y);
end


-- Установка цвета осей координат
function This:AdjustAxisColor(in_meta)
	local axisColor = GGF.InnerInvoke(in_meta, "GetAxisColor");
	in_meta.object:SetAxisColor({axisColor.R, axisColor.G, axisColor.B, axisColor.A});
end


-- Установка отрисовки левой и правой(дублирующей) осей OY
function This:AdjustYLabels(in_meta)
	local yLabels = GGF.InnerInvoke(in_meta, "GetYLabels");
	in_meta.object:SetYLabels(yLabels.left, yLabels.right);
end


-- Установка спейсинга по всем осям(подгон масштаба)
function This:AdjustGridSpacing(in_meta)
	local gridSpacing = GGF.InnerInvoke(in_meta, "GetGridSpacing");
	in_meta.object:SetGridSpacing(gridSpacing.X, gridSpacing.Y);
end


-- Установка интервала оси OX
function This:AdjustXAxisInterval(in_meta)
	local xAxisInterval = GGF.InnerInvoke(in_meta, "GetXAxisInterval");
	in_meta.object:SetXAxis(xAxisInterval.start, xAxisInterval.finish);
end


-- Установка интервала оси OY
function This:AdjustYAxisInterval(in_meta)
	local yAxisInterval = GGF.InnerInvoke(in_meta, "GetYAxisInterval");
	in_meta.object:SetYAxis(yAxisInterval.start, yAxisInterval.finish);
end


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- TODO: перереализовать!!!
-- Принудительный ресет(чтоб не клинило при замене данных)
function This:ResetData(in_meta)
	--self:GetObject():ResetData();
end


-- Добавление подготовленных данных на график
function This:AddDataSeries(in_meta, in_data, in_color, in_flag)
	--self:GetObject():AddDataSeries(in_data, in_color, in_flag);
end