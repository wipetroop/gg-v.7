-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

-- template

--	properties = {
--		base = {
--			parentName =,
--			parent =,
--		},
--	}

local Config = {
	TitleWidth = 180,	-- как слайдер по размеру
	TitleHeight = 20,

	TitleOffsetX = 0,
	TitleOffsetY = 20,

	ResetButtonWidth = 122,
	ResetButtonHeight = 29,

	ResetButtonOffsetX = 0,
	ResetButtonOffsetY = -185,

	UpperCheckButtonWidth = 20,
	UpperCheckButtonHeight = 20,

	UpperCheckButtonOffsetX = 10,
	UpperCheckButtonOffsetY = -10,

	LowerCheckButtonWidth = 20,
	LowerCheckButtonHeight = 20,

	LowerCheckButtonOffsetX = 10,
	LowerCheckButtonOffsetY = -160,

	-- NOTE: это который с иксовой координатой(скорее всего)
	UpperSliderWidth = 180,
	UpperSliderHeight = 20,

	UpperSliderOffsetX = 0,
	UpperSliderOffsetY = -35,

	LowerSliderWidth = 180,
	LowerSliderHeight = 20,

	LowerSliderOffsetX = 0,
	LowerSliderOffsetY = -105,

	-- NOTE: float т.к. там деление с неявным преобразованием типа
	SliderStepCount = 800.0,
};

local OBJSuffix = "PGC";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	PanelGeometryControllerSuffix = GGF.DockDomName(GGD.DefineIdentifier, "PanelGeometryControllerSuffix"),
	PanelGeometryController = GGF.DockDomName(GGD.ClassIdentifier, "PanelGeometryController"),
};

local CTList = {
	WrapperName = GGF.CreateCT(OBJSuffix, "WrapperName", "WrapperName"),
	ControlFrame = GGF.CreateCT(OBJSuffix, "ControlFrame", "Frame"),
	OffsetXCompositeSliderWrapper = GGF.CreateCT(OBJSuffix, "OffsetXCompositeSliderWrapper", "Frame"),
	OffsetYCompositeSliderWrapper  = GGF.CreateCT(OBJSuffix, "OffsetYCompositeSliderWrapper", "Frame"),
	ResetButtonWrapper = GGF.CreateCT(OBJSuffix, "ResetButtonWrapper", "Frame"),
	VisibilityCheckButtonWrapper = GGF.CreateCT(OBJSuffix, "VisibilityCheckButtonWrapper", "Frame"),
	CharspecCheckButtonWrapper = GGF.CreateCT(OBJSuffix, "CharspecCheckButtonWrapper", "Frame"),
	TitleFontStringWrapper = GGF.CreateCT(OBJSuffix, "TitleFontStringWrapper", "Frame"),
	Title = GGF.CreateCT(OBJSuffix, "Title", "Title"),
	SliderXValueCharspec = GGF.CreateCT(OBJSuffix, "SliderXValueCharspec", "SliderValue"),
	SliderXValueCommon = GGF.CreateCT(OBJSuffix, "SliderXValueCommon", "SliderValue"),
	SliderYValueCharspec = GGF.CreateCT(OBJSuffix, "SliderYValueCharspec", "SliderValue"),
	SliderYValueCommon = GGF.CreateCT(OBJSuffix, "SliderYValueCommon", "SliderValue"),
	SliderXValueMetaMin = GGF.CreateCT(OBJSuffix, "SliderXValueMetaMin", "SliderMin"),
	SliderXValueMetaMax = GGF.CreateCT(OBJSuffix, "SliderXValueMetaMax", "SliderMax"),
	SliderXValueMetaStep = GGF.CreateCT(OBJSuffix, "SliderXValueMetaStep", "SliderStep"),
	SliderYValueMetaMin = GGF.CreateCT(OBJSuffix, "SliderYValueMetaMin", "SliderMin"),
	SliderYValueMetaMax = GGF.CreateCT(OBJSuffix, "SliderYValueMetaMax", "SliderMax"),
	SliderYValueMetaStep = GGF.CreateCT(OBJSuffix, "SliderYValueMetaStep", "SliderStep"),
	OnResetButtonClick = GGF.CreateCT(OBJSuffix, "OnResetButtonClick", "EmptyCallback"),
	OnVisibilityCheckButtonClick = GGF.CreateCT(OBJSuffix, "OnVisibilityCheckButtonClick", "EmptyCallback"),
	OnCharspecCheckButtonClick = GGF.CreateCT(OBJSuffix, "OnCharspecCheckButtonClick", "EmptyCallback"),
	OnVisibilityCheckButtonStateChanged = GGF.CreateCT(OBJSuffix, "OnVisibilityCheckButtonStateChanged", "EmptyCallback"),
	OnCharspecCheckButtonStateChanged = GGF.CreateCT(OBJSuffix, "OnCharspecCheckButtonStateChanged", "EmptyCallback"),
	OnXSliderValueChanged = GGF.CreateCT(OBJSuffix, "OnXSliderValueChanged", "EmptyCallback"),
	OnYSliderValueChanged = GGF.CreateCT(OBJSuffix, "OnYSliderValueChanged", "EmptyCallback"),
	OnEnter = GGF.CreateCT(OBJSuffix, "OnEnter", "EmptyCallback"),
	OnLeave = GGF.CreateCT(OBJSuffix, "OnLeave", "EmptyCallback"),
};

GGF.GRegister(DFList.PanelGeometryControllerSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	WrapperName = "DWN",
	Hidden = false,
	Title = "",
	Width = 0,
	Height = 0,
	Point = GGE.PointType.TopLeft,
	RelativeTo = function(in_meta)
		return UIParent;
	end,
	RelativePoint = GGE.PointType.TopLeft,
	OffsetX = 0,
	OffsetY = 0,
	SliderValue = 0,
	SliderMin = 0,
	SliderMax = 1,
	SliderStep = 1,
	EmptyCallback = function(in_meta) end,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.PanelGeometryController, {
	meta = {
		name = "Panel Geometry Controller",
		mark = "",	-- авт.
		suffix = GGD.PanelGeometryControllerSuffix,
	},
});

GGC.PanelGeometryController.objects = {
	frame = {
		control = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.ControlFrame,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.ControlFrame.Default)) then
					local standardFrame = CreateFrame(
						"Frame",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyControlFrameOD", standardFrame);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.PanelGeometryController.wrappers = {
	compositeSlider = {
		offsetX = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.OffsetXCompositeSliderWrapper,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		offsetY = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.OffsetYCompositeSliderWrapper,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	button = {
		reset = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.ResetButtonWrapper,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	checkButton = {
		visibility = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.VisibilityCheckButtonWrapper,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		charspec = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.CharspecCheckButtonWrapper,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	fontString = {
		title = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.TitleFontStringWrapper,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.PanelGeometryController.properties = {
	miscellaneous = {
		title = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Title,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.OuterInvoke(
					GGF.InnerInvoke(in_meta, "GetTitleFontStringWrapper"),
					"SetText",
					GGF.InnerInvoke(in_meta, "GetTitle")
				);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				local tfsw = GGF.InnerInvoke(in_meta, "GetTitleFontStringWrapper");
				GGF.OuterInvoke(tfsw, "SynchronizeText");
				GGF.InnerInvoke(in_meta, "ModifyTitle", GGF.OuterInvoke(tfsw, "GetText"));
			end,
		}),
	},
	values = {
		X = {
			charspec = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.SliderXValueCharspec,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					if (GGF.InnerInvoke(in_meta, "IsCharspec")) then
						GGF.OuterInvoke(
							GGF.InnerInvoke(in_meta, "GetOffsetXCompositeSliderWrapper"),
							"SetValue",
							GGF.InnerInvoke(in_meta, "GetSliderXValueCharspec")
						);
					end
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					if (GGF.InnerInvoke(in_meta, "IsCharspec")) then
						local oxcsw = GGF.InnerInvoke(in_meta, "GetOffsetXCompositeSliderWrapper");
						GGF.OuterInvoke(oxcsw, "SynchronizeValue");
						GGF.InnerInvoke(in_meta, "ModifySliderXValueCharspec", GGF.OuterInvoke(oxcsw, "GetValue"));
					end
				end,
			}),
			common = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.SliderXValueCommon,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsCharspec")) then
						GGF.OuterInvoke(
							GGF.InnerInvoke(in_meta, "GetOffsetXCompositeSliderWrapper"),
							"SetValue",
							GGF.InnerInvoke(in_meta, "GetSliderXValueCommon")
						);
					end
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsCharspec")) then
						local oxcsw = GGF.InnerInvoke(in_meta, "GetOffsetXCompositeSliderWrapper");
						GGF.OuterInvoke(oxcsw, "SynchronizeValue");
						GGF.InnerInvoke(in_meta, "ModifySliderXValueCommon", GGF.OuterInvoke(oxcsw, "GetValue"));
					end
				end,
			}),
		},
		Y = {
			charspec = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.SliderYValueCharspec,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					if (GGF.InnerInvoke(in_meta, "IsCharspec")) then
						GGF.OuterInvoke(
							GGF.InnerInvoke(in_meta, "GetOffsetYCompositeSliderWrapper"),
							"SetValue",
							GGF.InnerInvoke(in_meta, "GetSliderYValueCharspec")
						);
					end
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					if (GGF.InnerInvoke(in_meta, "IsCharspec")) then
						local oycsw = GGF.InnerInvoke(in_meta, "GetOffsetYCompositeSliderWrapper");
						GGF.OuterInvoke(oycsw, "SynchronizeValue");
						GGF.InnerInvoke(in_meta, "ModifySliderYValueCharspec", GGF.OuterInvoke(oycsw, "GetValue"));
					end
				end,
			}),
			common = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.SliderYValueCommon,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsCharspec")) then
						GGF.OuterInvoke(
							GGF.InnerInvoke(in_meta, "GetOffsetYCompositeSliderWrapper"),
							"SetValue",
							GGF.InnerInvoke(in_meta, "GetSliderYValueCommon")
						);
					end
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					if (not GGF.InnerInvoke(in_meta, "IsCharspec")) then
						local oycsw = GGF.InnerInvoke(in_meta, "GetOffsetYCompositeSliderWrapper");
						GGF.OuterInvoke(oycsw, "SynchronizeValue");
						GGF.InnerInvoke(in_meta, "ModifySliderYValueCommon", GGF.OuterInvoke(oycsw, "GetValue"));
					end
				end,
			}),
		},
	},
	valueMeta = {
		X = {
			min = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.SliderXValueMetaMin,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "adjustValueMetaX");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "synchronizeValueMetaX");
				end,
			}),
			max = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.SliderXValueMetaMax,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "adjustValueMetaX");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "synchronizeValueMetaX");
				end,
			}),
			step = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.SliderXValueMetaStep,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "adjustValueMetaX");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "synchronizeValueMetaX");
				end,
			}),
		},
		Y = {
			min = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.SliderYValueMetaMin,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "adjustValueMetaY");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "synchronizeValueMetaY");
				end,
			}),
			max = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.SliderYValueMetaMax,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "adjustValueMetaY");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "synchronizeValueMetaY");
				end,
			}),
			step = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.SliderYValueMetaStep,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "adjustValueMetaY");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "synchronizeValueMetaY");
				end,
			}),
		},
	},

	callback = {
		onResetButtonClick = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnResetButtonClick,
			fixed = false,

			-- NOTE: автовызов
			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onVisibilityCheckButtonClick = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnVisibilityCheckButtonClick,
			fixed = false,

			-- NOTE: автовызов
			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onCharspecCheckButtonClick = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnCharspecCheckButtonClick,
			fixed = false,

			-- NOTE: автовызов
			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onVisibilityCheckButtonStateChanged = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnVisibilityCheckButtonStateChanged,
			fixed = false,

			-- NOTE: автовызов
			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onCharspecCheckButtonStateChanged = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnCharspecCheckButtonStateChanged,
			fixed = false,

			-- NOTE: автовызов
			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onXSliderValueChanged = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnXSliderValueChanged,
			fixed = false,

			-- NOTE: автовызов
			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onYSliderValueChanged = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnYSliderValueChanged,
			fixed = false,

			-- NOTE: автовызов
			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onEnter = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnEnter,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local callback = GGF.InnerInvoke(in_meta, "CounterGetOnEnter");
				--in_object:SetScript("OnEnter", callback);
				--in_object:SetScript("OnEnter", callback);
				--in_object:SetScript("OnEnter", callback);
				--in_object:SetScript("OnEnter", callback);
				--in_object:SetScript("OnEnter", callback);
				--in_object:SetScript("OnEnter", callback);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onLeave = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnLeave,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local callback = GGF.InnerInvoke(in_meta, "CounterGetOnLeave");
				-- NOTE: там такого нет
				--in_wrapper:GetOffsetXEvolutionSlider():SetOnLeave("OnLeave", callback);
				--in_wrapper:GetOffsetYEvolutionSlider():SetOnLeave("OnLeave", callback);
				-- NOTE: заготовка
				--in_object:SetScript("OnLeave", callback);
				--in_object:SetScript("OnLeave", callback);
				--in_object:SetScript("OnLeave", callback);
				--in_object:SetScript("OnLeave", callback);
				--in_object:SetScript("OnLeave", callback);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.PanelGeometryController.elements = {};

GGF.Mark(GGC.PanelGeometryController);

--GGC.PanelGeometryController = GGF.TableSafeExtend(GGC.PanelGeometryController, GGC.CompositeContainer);
GGC.PanelGeometryController = GGF.TableSafeExtend(GGC.PanelGeometryController, GGC.StandardFrame);

GGF.CreateClassWorkaround(GGC.PanelGeometryController);

local This = GGC.PanelGeometryController;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Установка значения слайдера по оси X
function This:SetSliderXValue(in_meta, in_val)
	if (GGF.InnerInvoke(in_meta, "IsCharspec")) then
		GGF.InnerInvoke(in_meta, "SetSliderXValueCharspec", in_val);
	else
		GGF.InnerInvoke(in_meta, "SetSliderXValueCommon", in_val);
	end
end


-- Установка значения слайдера по оси Y
function This:SetSliderYValue(in_meta, in_val)
	if (GGF.InnerInvoke(in_meta, "IsCharspec")) then
		GGF.InnerInvoke(in_meta, "SetSliderYValueCharspec", in_val);
	else
		GGF.InnerInvoke(in_meta, "SetSliderYValueCommon", in_val);
	end
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие значения слайдера по оси X
function This:GetSliderXValue(in_meta, in_val)
	if (GGF.InnerInvoke(in_meta, "IsCharspec")) then
		return GGF.InnerInvoke(in_meta, "GetSliderXValueCharspec");
	else
		return GGF.InnerInvoke(in_meta, "GetSliderXValueCommon");
	end
end


-- Взятие значения слайдера по оси Y
function This:GetSliderYValue(in_meta, in_val)
	if (GGF.InnerInvoke(in_meta, "IsCharspec")) then
		return GGF.InnerInvoke(in_meta, "GetSliderYValueCharspec");
	else
		return GGF.InnerInvoke(in_meta, "GetSliderYValueCommon");
	end
end


-- Взятие значений X слайдера
function This:GetSliderXValueMeta(in_meta)
	local values = {
		min = GGF.InnerInvoke(in_meta, "GetSliderXValueMetaMin"),
		max = GGF.InnerInvoke(in_meta, "GetSliderXValueMetaMax"),
		step = GGF.InnerInvoke(in_meta, "GetSliderXValueMetaStep"),
	};
	return values;
end


-- Взятие значений Y слайдера
function This:GetSliderYValueMeta(in_meta)
	local values = {
		min = GGF.InnerInvoke(in_meta, "GetSliderYValueMetaMin"),
		max = GGF.InnerInvoke(in_meta, "GetSliderYValueMetaMax"),
		step = GGF.InnerInvoke(in_meta, "GetSliderYValueMetaStep"),
	};
	return values;
end


-- Взятие значений принадлежности персонажу
function This:IsCharspec(in_meta)
	return GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetCharspecCheckButtonWrapper"), "GetChecked");
end


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Применение значения слайдера по оси X
function This:AdjustSliderXValue(in_meta, in_val)
	if (GGF.InnerInvoke(in_meta, "IsCharspec")) then
		GGF.InnerInvoke(in_meta, "AdjustSliderXValueCharspec");
	else
		GGF.InnerInvoke(in_meta, "AdjustSliderXValueCommon");
	end
end


-- Применение значения слайдера по оси Y
function This:AdjustSliderYValue(in_meta, in_val)
	if (GGF.InnerInvoke(in_meta, "IsCharspec")) then
		GGF.InnerInvoke(in_meta, "AdjustSliderYValueCharspec");
	else
		GGF.InnerInvoke(in_meta, "AdjustSliderYValueCommon");
	end
end


-- Установка габаритов контрольного окна
function This:adjustControlSize(in_meta)
	local size = GGF.InnerInvoke(in_meta, "GetSize");
	in_meta.object:SetSize(size.width, size.height);
end


-- Установка габаритов слайдера
function This:adjustSliderSize(in_meta)
	local size = GGF.InnerInvoke(in_meta, "GetSize");
	in_meta.object:SetSize(size.width - GGF.InnerInvoke(in_meta, "GetSliderWidthDifference"), GGF.InnerInvoke(in_meta, "GetSliderHeight"));	-- TODO: вот тут надо уточнить
end


-- Установка метаданных слайдера по оси X
function This:adjustValueMetaX(in_meta)
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetOffsetXCompositeSliderWrapper"),
		"SetValues",
		GGF.InnerInvoke(in_meta, "GetSliderXValueMeta")
	);
end


-- Установка метаданных слайдера по оси Y
function This:adjustValueMetaY(in_meta)
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetOffsetYCompositeSliderWrapper"),
		"SetValues",
		GGF.InnerInvoke(in_meta, "GetSliderYValueMeta")
	);
end


-- Установка основных объектов контроллера
function This:adjustServiceObjects(in_meta)
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetOffsetXCompositeSliderWrapper"), "Adjust");
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetOffsetYCompositeSliderWrapper"), "Adjust");
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetResetButtonWrapper"), "Adjust");
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetVisibilityCheckButtonWrapper"), "Adjust");
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetCharspecCheckButtonWrapper"), "Adjust");
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetTitleFontStringWrapper"), "Adjust");
end


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-- Синхронизация метаданных слайдера по оси Y
function This:synchronizeValueMetaX(in_meta)
	local oxcsw = GGF.InnerInvoke(in_meta, "GetOffsetXCompositeSliderWrapper");
	GGF.OuterInvoke(oxcsw, "SynchronizeValues");
	GGF.InnerInvoke(in_meta, "ModifySliderXValueMeta", GGF.OuterInvoke(oxcsw, "GetValues"));
end


-- Синхронизация метаданных слайдера по оси X
function This:synchronizeValueMetaY(in_meta)
	local oycsw = GGF.InnerInvoke(in_meta, "GetOffsetYCompositeSliderWrapper");
	GGF.OuterInvoke(oycsw, "SynchronizeValues");
	GGF.InnerInvoke(in_meta, "ModifySliderYValueMeta", GGF.OuterInvoke(oycsw, "GetValues"));
end


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Создание основных объектов контроллера
function This:createServiceObjects(in_meta)
	GGF.InnerInvoke(in_meta, "SetOffsetXCompositeSliderWrapperOD", GGC.CompositeSlider:Create({}, {
		properties = {
			base = {
				parent = in_meta.object,
				wrapperName = "OX",
			},
			miscellaneous = {
				title = "Offset X",
				value = function() return GGF.InnerInvoke(in_meta, "GetSliderXValue"); end,
			},
			size = {
				width = Config.UpperSliderWidth,
				height = Config.UpperSliderHeight,
			},
			anchor = {
				point = GGE.PointType.Top,
				relativeTo = in_meta.object,
				relativePoint = GGE.PointType.Top,
				offsetX = Config.UpperSliderOffsetX,
				offsetY = Config.UpperSliderOffsetY,
			},
			values = {
				min = GGF.InnerInvoke(in_meta, "TrLinkGetSliderXValueMetaMin"),
				max = GGF.InnerInvoke(in_meta, "TrLinkGetSliderXValueMetaMax"),
				step = function()
					return (GGF.InnerInvoke(in_meta, "GetSliderXValueMetaMax") - GGF.InnerInvoke(in_meta, "GetSliderXValueMetaMin"))/Config.SliderStepCount;
				end,
			},
			callback = {
				onValueChanged = function(...)
					--print("on val changed", ...);
					GGF.InnerInvoke(in_meta, "SetSliderXValue", ...);
					GGF.InnerInvoke(in_meta, "CounterGetOnXSliderValueChanged")(...);
				end,
			},
		},
	}));
	--evoSlider:ArglineSetValuesMax({
		--width = function() return GGM.GUI.SystemDashboard:GetDashboardPanelWidth() end,
	--});
	--evoSlider:ArglineSetValuesStep({
		--valmax = function() return self.JR_HG_F_SD_F_OXSWR_F_OBJ:GetValuesMax() end,
		--valmin = function() return self.JR_HG_F_SD_F_OXSWR_F_OBJ:GetValuesMin() end,
		--stepCount = Config.SliderStepCount,
	--});
	--self:ModifyOffsetXSliderOD(evoSlider);

	GGF.InnerInvoke(in_meta, "SetOffsetYCompositeSliderWrapperOD", GGC.CompositeSlider:Create({}, {
		properties = {
			base = {
				parent = in_meta.object,
				wrapperName = "OY",
			},
			miscellaneous = {
				title = "Offset Y",
				value = function() return GGF.InnerInvoke(in_meta, "GetSliderYValue") end,
			},
			size = {
				width = Config.UpperSliderWidth,
				height = Config.UpperSliderHeight,
			},
			anchor = {
				point = GGE.PointType.Top,
				relativeTo = in_meta.object,
				relativePoint = GGE.PointType.Top,
				offsetX = Config.LowerSliderOffsetX,
				offsetY = Config.LowerSliderOffsetY,
			},
			values = {
				min = GGF.InnerInvoke(in_meta, "TrLinkGetSliderYValueMetaMin"),
				max = GGF.InnerInvoke(in_meta, "TrLinkGetSliderYValueMetaMax"),
				--step = self:GetSliderYValuesStep(),
				step = function() return (GGF.InnerInvoke(in_meta, "GetSliderYValueMetaMax") - GGF.InnerInvoke(in_meta, "GetSliderYValueMetaMin"))/Config.SliderStepCount end,
			},
			callback = {
				onValueChanged = function(...)
					GGF.InnerInvoke(in_meta, "SetSliderYValue", ...);
					GGF.InnerInvoke(in_meta, "CounterGetOnYSliderValueChanged")(...);
				end,
			},
		},
	}));

	GGF.InnerInvoke(in_meta, "SetResetButtonWrapperOD", GGC.StandardButton:Create({}, {
		properties = {
			base = {
				parent = in_meta.object,
				wrapperName = "R",
			},
			miscellaneous = {
				enabled = true,
				text = "Reset",
			},
			size = {
				width = Config.ResetButtonWidth,
				height = Config.ResetButtonHeight,
			},
			anchor = {
				point = GGE.PointType.Top,
				relativeTo = in_meta.object,
				relativePoint = GGE.PointType.Top,
				offsetX = Config.ResetButtonOffsetX,
				offsetY = Config.ResetButtonOffsetY,
			},
			callback = {
				onClick = function(...) GGF.InnerInvoke(in_meta, "MasterReset"); GGF.InnerInvoke(in_meta, "GetOnResetButtonClickCounter")(...) end,
				--onEnter = function(...) self:GetOnEnterCounter()(self:GetResetButton():GetButton(), ...) end,
				--onLeave = function(...) self:GetOnLeaveCounter()(self:GetResetButton():GetButton(), ...) end,
			},
		},
	}));

	GGF.InnerInvoke(in_meta, "SetVisibilityCheckButtonWrapperOD", GGC.CheckButton:Create({}, {
		properties = {
			base = {
				parent = in_meta.object,
				wrapperName = "R",
			},
			miscellaneous = {
				text = "Visible",
			},
			size = {
				width = Config.UpperCheckButtonWidth,
				height = Config.UpperCheckButtonHeight,
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = in_meta.object,
				relativePoint = GGE.PointType.TopLeft,
				offsetX = Config.UpperCheckButtonOffsetX,
				offsetY = Config.UpperCheckButtonOffsetY,
			},
			callback = {
				onClick = function(...) GGF.InnerInvoke(in_meta, "CounterGetOnVisibilityCheckButtonClick")(...) end,
				onStateChanged = function(...) GGF.InnerInvoke(in_meta, "CounterGetOnVisibilityCheckButtonStateChanged")(...) end,
				--onEnter = function(...) self:GetOnEnterCounter()(self:GetVisibilityCheckButton():GetButton(), ...) end,
				--onLeave = function(...) self:GetOnLeaveCounter()(self:GetVisibilityCheckButton():GetButton(), ...) end,
			},
		},
	}));

	GGF.InnerInvoke(in_meta, "SetCharspecCheckButtonWrapperOD", GGC.CheckButton:Create({}, {
		properties = {
			base = {
				parent = in_meta.object,
				wrapperName = "C",
			},
			miscellaneous = {
				text = "Charspec",
			},
			size = {
				width = Config.LowerCheckButtonWidth,
				height = Config.LowerCheckButtonHeight,
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = in_meta.object,
				relativePoint = GGE.PointType.TopLeft,
				offsetX = Config.LowerCheckButtonOffsetX,
				offsetY = Config.LowerCheckButtonOffsetY,
			},
			callback = {
				onClick = function(...) GGF.InnerInvoke(in_meta, "CounterGetOnCharspecCheckButtonClick")(...) end,
				onStateChanged = function(in_value, ...)
					GGF.InnerInvoke(in_meta, "CounterGetOnCharspecCheckButtonStateChanged")(in_value, ...);
					GGF.InnerInvoke(in_meta, "AdjustSliderXValue");
					GGF.InnerInvoke(in_meta, "AdjustSliderYValue");
				end,
				--onEnter = function(...) self:GetOnEnterCounter()(self:GetCharspecCheckButton():GetButton(), ...) end,
				--onLeave = function(...) self:GetOnLeaveCounter()(self:GetCharspecCheckButton():GetButton(), ...) end,
			},
		},
	}));
	--print("cb inst cr:", GGF.InnerInvoke(in_meta, "GetCharspecCheckButtonWrapper"));

	GGF.InnerInvoke(in_meta, "SetTitleFontStringWrapperOD", GGC.FontString:Create({}, {
		properties = {
			base = {
				parent = in_meta.object,
				wrapperName = "T",
			},
			miscellaneous = {
				hidden = false,
				text = GGF.InnerInvoke(in_meta, "TrLinkGetTitle"),
			},
			size = {
				width = Config.TitleWidth,
				height = Config.TitleHeight,
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = in_meta.object,
				relativePoint = GGE.PointType.TopLeft,
				offsetX = Config.TitleOffsetX,
				offsetY = Config.TitleOffsetY,
			},
		},
	}));
end