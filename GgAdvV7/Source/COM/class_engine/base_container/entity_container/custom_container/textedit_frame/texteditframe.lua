-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

local OBJSuffix = "TEF";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   TextEditFrameSuffix = GGF.DockDomName(GGD.DefineIdentifier, "TextEditFrameSuffix"),
   TextEditFrame = GGF.DockDomName(GGD.ClassIdentifier, "TextEditFrame"),
};

local CTList = {
   ControlFrame = GGF.CreateCT(OBJSuffix, "ControlFrame", "Frame"),
   EditBoxHolder = GGF.CreateCT(OBJSuffix, "EditBoxHolder", "Frame"),
   DataEditBoxWrapper = GGF.CreateCT(OBJSuffix, "DataEditBoxWrapper", "Frame"),
   ScrollWrapper = GGF.CreateCT(OBJSuffix, "ScrollWrapper", "Frame"),
};

GGF.GRegister(DFList.TextEditFrameSuffix, OBJSuffix, GGE.RegValType.Suffix);


GGF.GRegister(DFList.Default, {
   Frame = 0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.TextEditFrame, {
   meta = {
      name = "Text Edit",
      mark = "",  -- авт.
      suffix = GGD.TextEditFrameSuffix,
   },
});

GGC.TextEditFrame.objects = {
   frame = {
      controlFrame = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Object,
         cte = CTList.ControlFrame,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.ControlFrame.Default)) then
               local controlFrame = CreateFrame(
                  "Frame",
                  GGF.GenerateOTName(
                     GGF.InnerInvoke(in_meta, "GetParentName"),
                     GGF.InnerInvoke(in_meta, "GetWrapperName"),
                     GGF.InnerInvoke(in_meta, "getClassSuffix")
                  ),
                  GGF.InnerInvoke(in_meta, "GetParent"),
                  GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     },
                  })
               );
               GGF.InnerInvoke(in_meta, "ModifyControlFrameOD", controlFrame);
            end
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.TextEditFrame.wrappers = {
   frame = {
      -- HACK: холдер нужен т.к. на editBox не работает установка позиции
      editBoxHolder = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.EditBoxHolder,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      editBox = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.DataEditBoxWrapper,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      scroll = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.ScrollWrapper,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.TextEditFrame.properties = {
};

GGC.TextEditFrame.elements = {
};

GGF.Mark(GGC.TextEditFrame);

GGC.TextEditFrame = GGF.TableSafeExtend(GGC.TextEditFrame, GGC.StandardFrame);

GGF.CreateClassWorkaround(GGC.TextEditFrame);

local This = GGC.TextEditFrame;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-- Установка значения
function This:SetText(in_meta, in_string)
   ATLASSERT(in_string);
   GGF.OuterInvoke(
      GGF.InnerInvoke(in_meta, "GetDataEditBoxWrapper"),
      "SetText",
      in_string
   );
end

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-- Взятие значения
function This:GetText(in_meta)
   return GGF.OuterInvoke(
      GGF.InnerInvoke(in_meta, "GetDataEditBoxWrapper"),
      "GetText"
   );
end

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-- Установка основных объектов
function This:adjustServiceObjects(in_meta)
   GGF.OuterInvoke(
      GGF.InnerInvoke(in_meta, "GetEditBoxHolder"),
      "Adjust"
   );
   GGF.OuterInvoke(
      GGF.InnerInvoke(in_meta, "GetDataEditBoxWrapper"),
      "Adjust"
   );
   GGF.OuterInvoke(
      GGF.InnerInvoke(in_meta, "GetScrollWrapper"),
      "Adjust"
   );
end

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

-- Создание основных объектов
function This:createServiceObjects(in_meta)
   local parent = in_meta.object;
   --local color = GGF.InnerInvoke(in_meta, "GetColor");
   local gap = 10;
   local scrollWidth = 32; -- NOTE: с 2 зазорами между объектами
   GGF.InnerInvoke(in_meta, "SetEditBoxHolderOD", GGC.StandardFrame:Create({}, {
      properties = {
         base = {
            parent = function() return parent end,
            wrapperName = "EBH",
         },
         miscellaneous = {
            hidden = false,
         },
         size = {
            width = function() return GGF.InnerInvoke(in_meta, "GetWidth") - gap - scrollWidth; end,
            height = function() return GGF.InnerInvoke(in_meta, "GetHeight") - 2*gap; end,
         },
         anchor = {
            point = GGE.PointType.TopLeft,
            relativeTo = function() return parent end,
            relativePoint = GGE.PointType.TopLeft,
            offsetX = gap,
            offsetY = -gap,
         },
         --backdrop = GGD.Backdrop.Segregate,
         backdrop = GGD.Backdrop.Empty,
      },
   }));
   local holder = GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetEditBoxHolder"));
   GGF.InnerInvoke(in_meta, "SetDataEditBoxWrapperOD", GGC.EditBox:Create({}, {
      properties = {
         base = {
            parent = function() return holder end,
            wrapperName = "D",
         },
         miscellaneous = {
            hidden = false,
            --fontHeight = function() return GGF.InnerInvoke(in_meta, "GetFontHeight"); end
            autofocus = false,
            multiline = true,
            fontObject = "DD_FONT_SMALL",
            text = "",
         },
         size = {
            --width = function() return GGF.InnerInvoke(in_meta, "GetWidth") - 2*gap; end,
            --height = function() return GGF.InnerInvoke(in_meta, "GetHeight") - 2*gap; end,
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = function() return holder end,
            relativePoint = GGE.PointType.Center,
            justifyH = GGE.JustifyHType.Left,
            offsetX = 0,
            offsetY = 0,
         },
         --backdrop = GGD.Backdrop.Segregate,
         backdrop = GGD.Backdrop.Empty,
      },
   }));
   GGF.InnerInvoke(in_meta, "SetScrollWrapperOD", GGC.ScrollFrame:Create({}, {
      properties = {
         frame = {
            scrollChild = GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetDataEditBoxWrapper")),
         },
         base = {
            parent = function() return holder end,
            wrapperName = "S",
            template = "UIPanelScrollFrameTemplate",
         },
         miscellaneous = {
            hidden = false,
         },
         size = {
            width = function() return GGF.InnerInvoke(in_meta, "GetWidth") - gap - scrollWidth; end,
            height = function() return GGF.InnerInvoke(in_meta, "GetHeight") - 2*gap; end,
         },
         anchor = {
            point = GGE.PointType.Right,
            relativeTo = function() return holder end,
            relativePoint = GGE.PointType.Right,
            offsetX = 0,
            offsetY = 0,
         },
      },
   }));
end

-- Добавочное значение
function This:AddText(in_meta, in_string)
   ATLASSERT(in_string);
   GGF.OuterInvoke(
      GGF.InnerInvoke(in_meta, "GetDataEditBoxWrapper"),
      "AddText",
      in_string
   );
end

-- Добавочное значение
function This:NewString(in_meta)
   GGF.OuterInvoke(
      GGF.InnerInvoke(in_meta, "GetDataEditBoxWrapper"),
      "NewString",
      in_string
   );
end