-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "PS";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	PageScrollSliderSuffix = GGF.DockDomName(GGD.DefineIdentifier, "PageScrollSliderSuffix"),
	PageScrollSlider = GGF.DockDomName(GGD.ClassIdentifier, "PageScrollSlider"),
};

local CTList = {
	Slider = GGF.CreateCT(OBJSuffix, "Slider", "Frame"),
	UpButton = GGF.CreateCT(OBJSuffix, "UpButton", "Frame"),
	DownButton = GGF.CreateCT(OBJSuffix, "DownButton", "Frame"),
	SliderDragger = GGF.CreateCT(OBJSuffix, "SliderDragger", "Frame"),
	Value = GGF.CreateCT(OBJSuffix, "Value", "Value"),
	Orientation = GGF.CreateCT(OBJSuffix, "Orientation", "Orientation"),
	ValuesMin = GGF.CreateCT(OBJSuffix, "ValuesMin", "ValuesMin"),
	ValuesMax = GGF.CreateCT(OBJSuffix, "ValuesMax", "ValuesMax"),
	ValuesStep = GGF.CreateCT(OBJSuffix, "ValuesStep", "ValuesStep"),
	ValuesStepsPerPage = GGF.CreateCT(OBJSuffix, "ValuesStepsPerPage", "ValuesStepsPerPage"),
	OnValueChanged = GGF.CreateCT(OBJSuffix, "OnValueChanged", "EmptyCallback"),
	OffsetMin = GGF.CreateCT(OBJSuffix, "OffsetMin", "OffsetMin"),
	OffsetMax = GGF.CreateCT(OBJSuffix, "OffsetMax", "OffsetMax"),
	CurrentOffset = GGF.CreateCT(OBJSuffix, "CurrentOffset", "CurrentOffset"),
	DraggerSize = GGF.CreateCT(OBJSuffix, "DraggerSize", "DraggerSize"),
	--OnUpButtonClick = GGF.CreateCT(OBJSuffix, "OnUpButtonClick", "EmptyCallback"),
	--OnDownButtonClick = GGF.CreateCT(OBJSuffix, "OnDownButtonClick", "EmptyCallback"),
};

GGF.GRegister(DFList.PageScrollSliderSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	Value = 0,
	Orientation = GGE.SliderOrientation.Horizontal,
	ValuesMin = 0,
	ValuesMax = 0,
	ValuesStep = 1,
	ValuesStepsPerPage = 1,
	EmptyCallback = function(in_meta) end,
	OffsetMin = 0,
	OffsetMax = 0,
	CurrentOffset = 0,
	DraggerSize = 0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.PageScrollSlider, {
	meta = {
		name = "Pagescroll Slider",
		mark = "",	-- авт.
		suffix = GGD.PageScrollSliderSuffix,
	},
});

GGC.PageScrollSlider.objects = {
	slider = {
		standard = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.Slider,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.Slider.Default)) then
					local slider = CreateFrame(
						"Slider",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
                  GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
         						return GGF.TernExpSingle(
         							GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
         							nil
         						)
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate") and "BackdropTemplate",
                              "BackdropTemplate"
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifySliderOD", slider);
					--in_wrapper:ModifySliderOD(slider);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.PageScrollSlider.wrappers = {
	frame = {
		sliderDragger = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.SliderDragger,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	standardButtons = {
		upButton = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.UpButton,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		downButton = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.DownButton,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.PageScrollSlider.properties = {
	miscellaneous = {
		value = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Value,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		orientation = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Orientation,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		values = {
			min = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ValuesMin,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustValues");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			max = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ValuesMax,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustValues");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			step = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ValuesStep,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustValues");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			stepsPerPage = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ValuesStepsPerPage,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
	},
	callback = {
		onValueChanged = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnValueChanged,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.PageScrollSlider.elements = {
	runtime = {
		sliderRange = {
			-- NOTE: считаем от центра
			offsetMin = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.OffsetMin,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			offsetMax = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.OffsetMax,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			currentOffset = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.CurrentOffset,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			draggerSize = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.DraggerSize,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		}
	},
};

GGF.Mark(GGC.PageScrollSlider);

GGC.PageScrollSlider = GGF.TableSafeExtend(GGC.PageScrollSlider, GGC.StandardFrame);

GGF.CreateClassWorkaround(GGC.PageScrollSlider);

local This = GGC.PageScrollSlider;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-- Смена предельных значений слайдера
function This:ModifyValues(in_meta, in_values)
	GGF.InnerInvoke(in_meta, "ModifyValuesMin", in_values.min);
	GGF.InnerInvoke(in_meta, "ModifyValuesMax", in_values.max);
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Смена предельных значений слайдера
function This:SetValues(in_meta, in_values)
	GGF.InnerInvoke(in_meta, "SetValuesMin", in_values.min);
	GGF.InnerInvoke(in_meta, "SetValuesMax", in_values.max);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие предельных значений слайдера
function This:GetValues(in_meta)
	local values = {
		min = GGF.InnerInvoke(in_meta, "GetValuesMin"),
		max = GGF.InnerInvoke(in_meta, "GetValuesMax"),
	};
	return values;
end


-- Взятие форм-фактора слайдера
function This:getFormFactor(in_meta)
	return GGF.TernExpSingle(
		GGF.InnerInvoke(in_meta, "isVertical"),
		GGF.InnerInvoke(in_meta, "GetWidth"),
		GGF.InnerInvoke(in_meta, "GetHeight")
	);
end


-- Проверка вертикальности слайдера
function This:isVertical(in_meta)
	return GGF.InnerInvoke(in_meta, "GetOrientation") == GGE.SliderOrientation.Vertical;
end


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Установка предельных значений слайдера
function This:AdjustValues(in_meta)
	local values = GGF.InnerInvoke(in_meta, "GetValues");
	local step = GGF.InnerInvoke(in_meta, "GetValuesStep");
	local isVertical = GGF.InnerInvoke(in_meta, "isVertical");
	-- NOTE: требуемое количество пикселей
	local expectedRange = (values.max - values.min)/step;
	local availableRange;
   local formFactor = GGF.InnerInvoke(in_meta, "getFormFactor");
   GGF.InnerInvoke(in_meta, "SetOffsetMin", formFactor);
   GGF.InnerInvoke(in_meta, "SetCurrentOffset", GGF.InnerInvoke(in_meta, "GetOffsetMin"));
	if (isVertical) then
		availableRange = GGF.InnerInvoke(in_meta, "GetHeight") - 3*GGF.InnerInvoke(in_meta, "getFormFactor");
		if (availableRange < expectedRange) then
			--print("oversize height", availableRange, expectedRange);
		else
			local size = availableRange - expectedRange + formFactor;
			GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSliderDragger"), "SetHeight", size);
			GGF.InnerInvoke(in_meta, "SetDraggerSize", size);
			GGF.InnerInvoke(in_meta, "SetOffsetMax", expectedRange + formFactor);
			--print("normal height", availableRange, expectedRange);
		end
	else
		availableRange = GGF.InnerInvoke(in_meta, "GetWidth") - 3*GGF.InnerInvoke(in_meta, "getFormFactor");
		if (availableRange < expectedRange) then
			--print("oversize width", availableRange, expectedRange);
		else
			local size = availableRange - expectedRange + formFactor;
			GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSliderDragger"), "SetWidth", size);
			GGF.InnerInvoke(in_meta, "SetDraggerSize", size);
			GGF.InnerInvoke(in_meta, "SetOffsetMax", expectedRange + formFactor);
			--print("normal width", availableRange, expectedRange);
		end
	end
	GGF.InnerInvoke(in_meta, "updateSliderPosition");
end


-- Установка основных объектов эвослайдера
function This:adjustServiceObjects(in_meta)
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetUpButton"), "Adjust");
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetDownButton"), "Adjust");
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSliderDragger"), "Adjust");
	--local slider = GGF.InnerInvoke(in_meta, "GetSliderDragger");
	--print("pss aso 1:", GGF.OuterInvoke(slider, "GetRelativeTo"), GGF.OuterInvoke(slider, "GetOffsetX"), GGF.OuterInvoke(slider, "GetOffsetY"));
	GGF.InnerInvoke(in_meta, "updateSliderPosition");
   GGF.InnerInvoke(in_meta, "AdjustValues");
	--print("pss aso 2:", GGF.OuterInvoke(slider, "GetRelativeTo"), GGF.OuterInvoke(slider, "GetOffsetX"), GGF.OuterInvoke(slider, "GetOffsetY"));
end

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

-- Создание основных объектов эвослайдера
function This:createServiceObjects(in_meta)
	local parentFrame = in_meta.object;

	GGF.InnerInvoke(in_meta, "SetBackdrop", GGD.Backdrop.Empty);

	GGF.InnerInvoke(in_meta, "SetUpButtonOD", GGC.StandardButton:Create({}, {
		properties = {
			--Object = CreateFrame("Button", self:GetObjectName() .. "_UT_B", self:GetObject(), "UiPanelButtonTemplate"),
			base = {
				parent = parentFrame,
				wrapperName = "UP",
				--template = "UIPanelButtonGrayTemplate",
				template = "UIPanelScrollUPButtonTemplate",
			},
			miscellaneous = {
				hidden = false,
				enabled = true,
				highlighted = false,
				text = GGF.TernExpSingle(GGF.InnerInvoke(in_meta, "isVertical"), "U", "L"),
				--tooltipText = self:GetObjectName() .. "_UT_B",
			},
			--backdrop = GGD.Backdrop.Empty,
			size = {
				width = GGF.InnerInvoke(in_meta, "getFormFactor"),
				height = GGF.InnerInvoke(in_meta, "getFormFactor"),
			},
			anchor = {
				point = GGF.TernExpSingle(GGF.InnerInvoke(in_meta, "isVertical"), GGE.PointType.Top, GGE.PointType.Left),
				relativeTo = parentFrame,
				relativePoint = GGF.TernExpSingle(GGF.InnerInvoke(in_meta, "isVertical"), GGE.PointType.Top, GGE.PointType.Left),
			},
			callback = {
				onClick = function(...) GGF.InnerInvoke(in_meta, "on_upButton_clicked", ...) end,
				--onEnter = function(...) GGF.InnerInvoke(in_meta, "GetOnButtonEnterCounter")(GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetUpdateButton")), ...) end,
				--onLeave = function(...) GGF.InnerInvoke(in_meta, "GetOnButtonLeaveCounter")(GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetUpdateButton")), ...) end,
			},
		},
	}));

	GGF.InnerInvoke(in_meta, "SetDownButtonOD", GGC.StandardButton:Create({}, {
		properties = {
			--Object = CreateFrame("Button", self:GetObjectName() .. "_UT_B", self:GetObject(), "UiPanelButtonTemplate"),
			base = {
				parent = parentFrame,
				wrapperName = "DN",
				--template = "UIPanelButtonGrayTemplate",
				template = "UIPanelScrollDownButtonTemplate",
			},
			miscellaneous = {
				hidden = false,
				enabled = true,
				highlighted = false,
				text = GGF.TernExpSingle(GGF.InnerInvoke(in_meta, "isVertical"), "D", "R"),
				--tooltipText = self:GetObjectName() .. "_UT_B",
			},
			--backdrop = GGD.Backdrop.Empty,
			size = {
				width = GGF.InnerInvoke(in_meta, "getFormFactor"),
				height = GGF.InnerInvoke(in_meta, "getFormFactor"),
			},
			anchor = {
				point = GGF.TernExpSingle(GGF.InnerInvoke(in_meta, "isVertical"), GGE.PointType.Bottom, GGE.PointType.Right), --GGE.PointType.BottomRight,
				relativeTo = parentFrame,
				relativePoint = GGF.TernExpSingle(GGF.InnerInvoke(in_meta, "isVertical"), GGE.PointType.Bottom, GGE.PointType.Right),
			},
			callback = {
				onClick = function(...) GGF.InnerInvoke(in_meta, "on_downButton_clicked", ...) end,
				--onEnter = function(...) GGF.InnerInvoke(in_meta, "GetOnButtonEnterCounter")(GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetUpdateButton")), ...) end,
				--onLeave = function(...) GGF.InnerInvoke(in_meta, "GetOnButtonLeaveCounter")(GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetUpdateButton")), ...) end,
			},
		},
	}));

	GGF.InnerInvoke(in_meta, "SetSliderDraggerOD", GGC.ScrollBar:Create({}, {
		properties = {
			base = {
				parent = parentFrame,
				wrapperName = "SD",
			},
			miscellaneous = {
            enabled = true,
				--movable = true,
				orientation = GGF.InnerInvoke(in_meta, "GetOrientation")
			},
			size = {
				width = GGF.InnerInvoke(in_meta, "getFormFactor"),
				height = GGF.InnerInvoke(in_meta, "getFormFactor"),
			},
			anchor = {
				point = GGF.TernExpSingle(GGF.InnerInvoke(in_meta, "isVertical"), GGE.PointType.Top, GGE.PointType.Left),
				relativeTo = parentFrame,
				relativePoint = GGF.TernExpSingle(GGF.InnerInvoke(in_meta, "isVertical"), GGE.PointType.Top, GGE.PointType.Left),
				offsetX = GGF.TernExpSingle(GGF.InnerInvoke(in_meta, "isVertical"), 0, GGF.InnerInvoke(in_meta, "getFormFactor")),
            offsetY = GGF.TernExpSingle(GGF.InnerInvoke(in_meta, "isVertical"), GGF.InnerInvoke(in_meta, "getFormFactor"), 0),
			},
			secondAnchor = {
				point = GGF.TernExpSingle(GGF.InnerInvoke(in_meta, "isVertical"), GGE.PointType.Bottom, GGE.PointType.Right),
				relativeTo = parentFrame,
				relativePoint = GGF.TernExpSingle(GGF.InnerInvoke(in_meta, "isVertical"), GGE.PointType.Bottom, GGE.PointType.Right),
				offsetX = GGF.TernExpSingle(GGF.InnerInvoke(in_meta, "isVertical"), 0, - GGF.InnerInvoke(in_meta, "getFormFactor")),
            offsetY = GGF.TernExpSingle(GGF.InnerInvoke(in_meta, "isVertical"), - GGF.InnerInvoke(in_meta, "getFormFactor"), 0),
			},
			--backdrop = GGD.Backdrop.Empty,
			backdrop = GGD.Backdrop.Nested,
			callback = {
				--onMouseDown = function(in_ameta, in_self, ...)
				--end,
				--onMouseUp = function(in_ameta, in_self, ...)
					--GGF.InnerInvoke(in_meta, "correctDraggerPosition");
					--GGF.InnerInvoke(in_meta, "updateSliderPosition");
				--end,
				--onDragStop = function(in_ameta, in_self, ...)
					--GGF.InnerInvoke(in_meta, "correctDraggerPosition");
					--GGF.InnerInvoke(in_meta, "updateSliderPosition");
				--end,
			},
		},
	}));

	--local slider = GGF.InnerInvoke(in_meta, "GetSliderDragger");
	--print("pss cso:", GGF.OuterInvoke(slider, "GetRelativeTo"), GGF.OuterInvoke(slider, "GetOffsetX"), GGF.OuterInvoke(slider, "GetOffsetY"));
end

-- Обработчик смещения вверх/влево
function This:on_upButton_clicked(in_meta)
	GGF.InnerInvoke(in_meta, "proceedValueChange", true);
end

-- Обработчик смещения вниз/вправо
function This:on_downButton_clicked(in_meta)
	GGF.InnerInvoke(in_meta, "proceedValueChange", false);
end

-- Корректирующий подсрачник по драггеру после драг-н-дропа
function This:correctDraggerPosition(in_meta)
	local slider = GGF.InnerInvoke(in_meta, "GetSliderDragger");
	local isVertical = GGF.InnerInvoke(in_meta, "isVertical");
	local offsetMin = GGF.InnerInvoke(in_meta, "GetOffsetMin");
	local offsetMax = GGF.InnerInvoke(in_meta, "GetOffsetMax");
	local drgSize = GGF.InnerInvoke(in_meta, "GetDraggerSize");
	local zone = GGF.InnerInvoke(in_meta, "getZone");
	local offset = GGF.InnerInvoke(in_meta, "getRelativeOffset");
	if (isVertical) then
		GGF.OuterInvoke(slider, "SetOffsetX", 0);
	else
		GGF.OuterInvoke(slider, "SetOffsetY", 0);
	end
	--print("corr:", offsetMin, offsetMax, offset);
	--print("corr:", GGF.OuterInvoke(slider, "GetOffsetX"), GGF.OuterInvoke(slider, "GetOffsetY"));
	if (offset < offsetMin) then
		GGF.InnerInvoke(in_meta, "SetCurrentOffset", offsetMin);
	elseif (offset > offsetMax) then
		GGF.InnerInvoke(in_meta, "SetCurrentOffset", offsetMax);
	else
		GGF.InnerInvoke(in_meta, "SetCurrentOffset", offset);
	end
end

-- Взятие зоны слайдера
function This:getZone(in_meta)
	local isVertical = GGF.InnerInvoke(in_meta, "isVertical");
	if (isVertical) then
		return GGF.InnerInvoke(in_meta, "GetHeight") - 2*GGF.InnerInvoke(in_meta, "getFormFactor");
	else
		return GGF.InnerInvoke(in_meta, "GetWidth") - 2*GGF.InnerInvoke(in_meta, "getFormFactor");
	end
end

-- Взятие реального оффсета объекта по относительному
function This:getRealOffset(in_meta)
	local slider = GGF.InnerInvoke(in_meta, "GetSliderDragger");
	local offset = GGF.InnerInvoke(in_meta, "GetCurrentOffset");
	local isVertical = GGF.InnerInvoke(in_meta, "isVertical");
	local drgSize = GGF.InnerInvoke(in_meta, "GetDraggerSize");
	local zone = GGF.InnerInvoke(in_meta, "getZone");
   local formFactor = GGF.InnerInvoke(in_meta, "getFormFactor");
   
	--print("to offset:", zone, drgSize, GGF.OuterInvoke(slider, "GetOffsetX"), GGF.OuterInvoke(slider, "GetOffsetY"));
	if (isVertical) then
		--return zone/2 - drgSize/2 - offset;
      print("offset", - offset + formFactor);
      return - offset + formFactor;
	else
		--return - zone/2 + drgSize/2 + offset;
      print("offset", offset - formFactor);
      return offset - formFactor;
	end
end

-- Взятие относительного оффсета объекта по реальному
function This:getRelativeOffset(in_meta)
	local slider = GGF.InnerInvoke(in_meta, "GetSliderDragger");
	local isVertical = GGF.InnerInvoke(in_meta, "isVertical");
	local drgSize = GGF.InnerInvoke(in_meta, "GetDraggerSize");
	local zone = GGF.InnerInvoke(in_meta, "getZone");
	--print("to relative:", zone, drgSize, GGF.OuterInvoke(slider, "GetOffsetX"), GGF.OuterInvoke(slider, "GetOffsetY"), slider, GGF.OuterInvoke(slider, "GetParent"));
	-- NOTE: расчет на основе того, что слайдер исходно поцентру
	if (isVertical) then
		return - GGF.OuterInvoke(slider, "GetOffsetY") + zone/2 - drgSize/2;
	else
		return GGF.OuterInvoke(slider, "GetOffsetX") + zone/2 - drgSize/2;
	end
end

-- 
function This:proceedValueChange(in_meta, in_up)
	GGF.InnerInvoke(in_meta, "changeOffset", GGF.InnerInvoke(in_meta, "GetCurrentOffset") + GGF.TernExpSingle(in_up, - 1, 1));
end

--
function This:changeOffset(in_meta, in_offset)
	if (in_offset < GGF.InnerInvoke(in_meta, "GetOffsetMin")) then
		in_offset = GGF.InnerInvoke(in_meta, "GetOffsetMin");
	end
	if (in_offset > GGF.InnerInvoke(in_meta, "GetOffsetMax")) then
		in_offset = GGF.InnerInvoke(in_meta, "GetOffsetMax");
	end

	GGF.InnerInvoke(in_meta, "SetCurrentOffset", in_offset);
	GGF.InnerInvoke(in_meta, "updateSliderPosition");
end

--
function This:updateSliderPosition(in_meta)
	local drg = GGF.InnerInvoke(in_meta, "GetSliderDraggerOD");
	local isVertical = GGF.InnerInvoke(in_meta, "isVertical");
	local realOffset = GGF.InnerInvoke(in_meta, "getRealOffset");
	local currentOffset = GGF.InnerInvoke(in_meta, "GetCurrentOffset");
	local values = GGF.InnerInvoke(in_meta, "GetValues");
	local step = GGF.InnerInvoke(in_meta, "GetValuesStep");
   local formFactor = GGF.InnerInvoke(in_meta, "getFormFactor");
   local curval = 0;
   if (isVertical) then
      GGF.OuterInvoke(drg, "SetOffsetY", realOffset - formFactor);
      curval = (currentOffset - formFactor)*step - values.min;
   else
      GGF.OuterInvoke(drg, "SetOffsetX", realOffset + formFactor);
      curval = (currentOffset - formFactor)*step - values.min;
   end
   print("upd", curval, currentOffset);
	GGF.InnerInvoke(in_meta, "ModifyValue", curval);
	GGF.InnerInvoke(in_meta, "onValueChange");
end

-- Проекция изменения оффсета слайдера на значение переменной
function This:onValueChange(in_meta)
   local values = GGF.InnerInvoke(in_meta, "GetValues");
   local value = GGF.InnerInvoke(in_meta, "GetValue");
   local step = GGF.InnerInvoke(in_meta, "GetValuesStep");
   print("val ch", value, values.min, values.max, step);
   GGF.InnerInvoke(in_meta, "CounterGetOnValueChanged")(GGF.InnerInvoke(in_meta, "GetValue"));
end