-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

-- NOTE: не более чем бутафория, т.к. почти не наследуется

-- template

--	properties = {
--		base = {
--			parentName =,
--			parent =,
--		},
--	}

local OBJSuffix = "CC";

local DFList = {
	CustomContainerSuffix = GGF.DockDomName(GGD.DefineIdentifier, "CustomContainerSuffix"),
	CustomContainer = GGF.DockDomName(GGD.ClassIdentifier, "CustomContainer"),
};

GGF.GRegister(DFList.CustomContainerSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.CustomContainer, {
	meta = {
		name = "Custom Container",
		mark = "",	-- авт.
		suffix = GGD.CustomContainerSuffix,
	},
});

GGC.CustomContainer.objects = {};

GGC.CustomContainer.properties = {};

GGC.CustomContainer.elements = {};

GGF.Mark(GGC.CustomContainer);

GGC.CustomContainer = GGF.TableSafeExtend(GGC.CustomContainer, GGC.EntityContainer);

GGF.CreateClassWorkaround(GGC.CustomContainer);

local This = GGC.CustomContainer;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------