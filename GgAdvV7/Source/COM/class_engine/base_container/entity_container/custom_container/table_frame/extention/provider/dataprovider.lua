-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

local OBJSuffix = "DP";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   DataProviderSuffix = GGF.DockDomName(GGD.DefineIdentifier, "DataProviderSuffix"),
   DataProvider = GGF.DockDomName(GGD.ClassIdentifier, "DataProvider"),
};

local CTList = {
   DataSet = GGF.CreateCT(OBJSuffix, "DataSet", "DataSet"),
};

GGF.GRegister(DFList.DataProviderSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
   -- WARNING: это тестовый!!!
   DataSet = {
      vHeader = {
         textColor = GGD.Color.Green,
         tooltipColor = GGD.Color.Red,
      },
      fields = {
         {{
            text = "v11",
            textColor = GGD.Color.Green,
            tooltip = "v11t",
            tooltipColor = GGD.Color.Red
         }, {
            text = "v12",
            textColor = GGD.Color.Blue,
            tooltip = "v12t",
            tooltipColor = GGD.Color.Yellow
         }, {
            text = "v13",
            textColor = GGD.Color.Blue,
            tooltip = "v13t",
            tooltipColor = GGD.Color.Yellow
         }},
         {{
            text = "v21",
            textColor = GGD.Color.Green,
            tooltip = "v21t",
            tooltipColor = GGD.Color.Red
         }, {
            text = "v22",
            textColor = GGD.Color.Blue,
            tooltip = "v22t",
            tooltipColor = GGD.Color.Yellow
         }, {
            text = "v23",
            textColor = GGD.Color.Blue,
            tooltip = "v23t",
            tooltipColor = GGD.Color.Yellow
         }}
      },
   },
}, GGE.RegValType.Default);

GGF.GRegister(DFList.DataProvider, {
   meta = {
      name = "Data Provider",
      mark = "",  -- авт.
      suffix = GGD.DataProviderSuffix,
   },
});

GGC.DataProvider.objects = {};

GGC.DataProvider.wrappers = {};

GGC.DataProvider.properties = {
   data = {
      dataSet = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.DataSet,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            GGF.InnerInvoke(in_meta, "AdjustDataSetSpec");
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.DataProvider.elements = {};

GGF.Mark(GGC.DataProvider);

GGC.DataProvider = GGF.TableSafeExtend(GGC.DataProvider, GGC.TableProviderContainer);

GGF.CreateClassWorkaround(GGC.DataProvider);

local This = GGC.DataProvider;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-- Установка данных
function This:AdjustPrototypeSpec(in_meta)
   --[<column_number>] = {
   -- type = <type_of_data>(GGE.DataType)
   -- width
   -- align
   -- Title
   -- text
   -- tooltip
   --}
   -- NOTE: сразу пересчитываем слайдер по горизонтали(если таковой имеется)
   -- TODO: чек, не?
   local prototype = GGF.InnerInvoke(in_meta, "GetPrototype");
   local config = GGF.InnerInvoke(in_meta, "GetTypeConfig");
   GGF.InnerInvoke(in_meta, "AdjustDataConflomerationObjects");
end

-- Установка данных
function This:AdjustDataSetSpec(in_meta)
   if (not GGF.InnerInvoke(in_meta, "GetLaunched")) then
      return;
   end

   GGF.OuterInvoke(
      GGF.OuterInvoke(
         GGF.OuterInvoke(
            GGF.InnerInvoke(in_meta, "GetParentInstance"),
         "GetObjectProvider"),
      "GetDataConglomerationSubModule"),
   "ReassignFieldDataIndex");
   if (true) then
      return;
   end

   local tableFrame = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");

   local config = GGF.OuterInvoke(visionLogicProvider, "GetTypeConfig");
   local dataSet = GGF.InnerInvoke(in_meta, "GetDataSet");
   local prototype = GGF.OuterInvoke(tableFrame, "GetPrototype");

   local dataMap = {};
   for i=1,#dataSet.fields do
      dataMap[i] = i;
   end
   GGF.OuterInvoke(visionLogicProvider, "SetIndexMapOD", dataMap);

   local rowCount = #dataSet.fields;

   --in_data[idx].cols
   GGF.OuterInvoke(visionLogicProvider, "SetDataRowCount", rowCount);
   GGF.OuterInvoke(visionLogicProvider, "SetDataColumnCount", #GGF.OuterInvoke(tableFrame, "GetPrototype").columns);

   if (config.paging) then
      --self:SetHidden(false);
      -- NOTE: количество страниц должно быть с избытком
      GGF.InnerInvoke(in_meta, "SetPageCount", 1 + math.floor(GGF.InnerInvoke(in_meta, "GetDataRowCount") / GGF.InnerInvoke(in_meta, "GetPageCapacity")));
      GGF.InnerInvoke(in_meta, "SetPageSelectorLimit", GGF.InnerInvoke(in_meta, "GetPageCount"));
   else
      -- NOTE: ничего не делаем
   end
   -- WARNING: тут не хватает условий и смены состояния таблицы/фрейма

   -- NOTE: тут надо сбросить локи строк и столбцов т.к. меняется количество и семантика
   GGF.OuterInvoke(visionLogicProvider, "SetLockedRows", {});
   GGF.OuterInvoke(visionLogicProvider, "SetLockedColumns", {});
   -- NOTE: эффект как от пустых изменений слайдера

   
end

-- Запуск
function This:launch(in_meta)
end

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------