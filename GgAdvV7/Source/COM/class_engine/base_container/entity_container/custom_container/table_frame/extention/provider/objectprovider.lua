-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "OP";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   ObjectProviderSuffix = GGF.DockDomName(GGD.DefineIdentifier, "ObjectProviderSuffix"),
   ObjectProvider = GGF.DockDomName(GGD.ClassIdentifier, "ObjectProvider"),
};

local CTList = {
   AdditionalControlsSubModule = GGF.CreateCT(OBJSuffix, "AdditionalControlsSubModule", "Frame"),
   DataConglomerationSubModule = GGF.CreateCT(OBJSuffix, "DataConglomerationSubModule", "Frame"),
   PhantomSubModule = GGF.CreateCT(OBJSuffix, "PhantomSubModule", "Frame"),
   SideCurtainMarkupSubModule = GGF.CreateCT(OBJSuffix, "SideCurtainMarkupSubModule", "Frame"),
};

GGF.GRegister(DFList.ObjectProviderSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
   Frame = 0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.ObjectProvider, {
   meta = {
      name = "Object Provider",
      mark = "",  -- авт.
      suffix = GGD.ObjectProviderSuffix,
   },
});

GGC.ObjectProvider.objects = {};

GGC.ObjectProvider.wrappers = {
   provider = {
      additionalControlsSubModule = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.AdditionalControlsSubModule,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      dataConglomerationSubModule = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.DataConglomerationSubModule,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      phantomSubModule = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.PhantomSubModule,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      sideCurtainMarkupSubModule = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.SideCurtainMarkupSubModule,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.ObjectProvider.properties = {};

GGC.ObjectProvider.elements = {};

GGF.Mark(GGC.ObjectProvider);

GGC.ObjectProvider = GGF.TableSafeExtend(GGC.ObjectProvider, GGC.TableProviderContainer);

GGF.CreateClassWorkaround(GGC.ObjectProvider);

local This = GGC.ObjectProvider;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-- Установка основных объектов табличного фрейма
function This:adjustServiceObjects(in_meta)
end

-- Запуск
function This:launch(in_meta)
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetAdditionalControlsSubModule"), "Launch");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetPhantomSubModule"), "Launch");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetDataConglomerationSubModule"), "Launch");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSideCurtainMarkupSubModule"), "Launch");
end

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-- Синхронизатор геометрии слайдеров
function This:SynchronizeSliderGeometry(in_meta)
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSideCurtainMarkupSubModule"), "SynchronizeSliderGeometry")
end

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

-- Создание основных объектов табличного фрейма
function This:createServiceObjects(in_meta)
   GGF.InnerInvoke(in_meta, "SetAdditionalControlsSubModuleOD", GGC.AdditionalControlsSubModule:Create({}, {
      properties = {
         structure = {
            parentInstance = function() return in_meta.instance end
         }
      }
   }));
   GGF.InnerInvoke(in_meta, "SetPhantomSubModuleOD", GGC.PhantomSubModule:Create({}, {
      properties = {
         structure = {
            parentInstance = function() return in_meta.instance end
         }
      }
   }));
   GGF.InnerInvoke(in_meta, "SetSideCurtainMarkupSubModuleOD", GGC.SideCurtainMarkupSubModule:Create({}, {
      properties = {
         structure = {
            parentInstance = function() return in_meta.instance end
         }
      }
   }));
   GGF.InnerInvoke(in_meta, "SetDataConglomerationSubModuleOD", GGC.DataConglomerationSubModule:Create({}, {
      properties = {
         structure = {
            parentInstance = function() return in_meta.instance end
         }
      }
   }));
end