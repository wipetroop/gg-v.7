-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "LST";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	ListSelectorTableSuffix = GGF.DockDomName(GGD.DefineIdentifier, "ListSelectorTableSuffix"),
	ListSelectorTable = GGF.DockDomName(GGD.ClassIdentifier, "ListSelectorTable"),
};

local CTList = {
	TableFrame = GGF.CreateCT(OBJSuffix, "ListSelectorTableFrame", "Frame"),
	TypeConfig = GGF.CreateCT(OBJSuffix, "TypeConfig", "TypeConfig"),
};

GGF.GRegister(DFList.ListSelectorTableSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	TypeConfig = {
		paging = false,
		locking = false,
		hsliding = false,
		settings = false,
	},
}, GGE.RegValType.Default);

GGF.GRegister(DFList.ListSelectorTable, {
	meta = {
		name = "List Selector Table",
		mark = "",	-- авт.
		suffix = GGD.ListSelectorTableSuffix,
	},
});

GGC.ListSelectorTable.object = {
	frame = {
		table = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.TableFrame,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.TableFrame.Default)) then
					local tableFrame = CreateFrame(
						"Frame",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
                  GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyListSelectorTableFrameOD", tableFrame);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.ListSelectorTable.properties = {};

GGC.ListSelectorTable.elements = {
	runtime = {
		properties = {
			typeConfig = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.TypeConfig,
				fixed = true,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
	},
};

GGF.Mark(GGC.ListSelectorTable);

GGC.ListSelectorTable = GGF.TableSafeExtend(GGC.ListSelectorTable, GGC.TableFrame);

GGF.CreateClassWorkaround(GGC.ListSelectorTable);

local This = GGC.ListSelectorTable;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------