-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

local OBJSuffix = "DCSM";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   DataConglomerationSubModuleSuffix = GGF.DockDomName(GGD.DefineIdentifier, "DataConglomerationSubModuleSuffix"),
   DataConglomerationSubModule = GGF.DockDomName(GGD.ClassIdentifier, "DataConglomerationSubModule"),
};

local CTList = {
   HorizontalHeaderDataConglomeration = GGF.CreateCT(OBJSuffix, "HorizontalHeaderDataConglomeration", "Frame"),
   VerticalHeaderDataConglomeration = GGF.CreateCT(OBJSuffix, "VerticalHeaderDataConglomeration", "Frame"),
   CentralFieldDataConglomeration = GGF.CreateCT(OBJSuffix, "CentralFieldDataConglomeration", "Frame"),
};

GGF.GRegister(DFList.DataConglomerationSubModuleSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
}, GGE.RegValType.Default);

GGF.GRegister(DFList.DataConglomerationSubModule, {
   meta = {
      name = "Data Conglomeration Sub Module",
      mark = "",  -- авт.
      suffix = GGD.DataConglomerationSubModuleSuffix,
   },
});

GGC.DataConglomerationSubModule.objects = {};

GGC.DataConglomerationSubModule.wrappers = {
   dataConglomeration = {
      horizontalHeader = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.HorizontalHeaderDataConglomeration,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      verticalHeader = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.VerticalHeaderDataConglomeration,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      centralField = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.CentralFieldDataConglomeration,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.DataConglomerationSubModule.properties = {};

GGC.DataConglomerationSubModule.elements = {};

GGF.Mark(GGC.DataConglomerationSubModule);

GGC.DataConglomerationSubModule = GGF.TableSafeExtend(GGC.DataConglomerationSubModule, GGC.TableExtentionContainer);

GGF.CreateClassWorkaround(GGC.DataConglomerationSubModule);

local This = GGC.DataConglomerationSubModule;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-- Установка выделенной линии/клетки по индексу
function This:SetSelection(in_meta, in_elem, in_rowIdx, in_colIdx)
   ATLASSERT(in_rowIdx == 0 or in_rowIdx == nil);
   local rowIdx, colIdx = 0, 0;
   local selectionType = GGF.InnerInvoke(in_meta, "GetSelectionType");
   local selectionColor = GGF.InnerInvoke(in_meta, "GetSelectionColor");
   --local field = GGF.InnerInvoke(in_meta, "GetCentralFieldDataConglomeration");

   local selectedRow = GGF.InnerInvoke(in_meta, "GetSelectedRowIdx");
   GGF.InnerInvoke(in_meta, "SetSelectedRowIdx", 0);

   ATLASSERT(selectionType == GGE.SelectionType.Row or selectionType == GGE.SelectionType.None)
   if (selectionType == GGE.SelectionType.Row) then
      rowIdx = in_rowIdx;
      -- NOTE: сброс текущего выделения(энивей)
      -- TODO: переделать, когда другие варианты выделения появятся
      if (selectedRow ~= 0) then
         for colidx=1,#field[selectedRow] do
            --GGF.InnerInvoke(in_meta, "cellHighlightCheckSwitch", in_elem, selectedRow, colidx, false);
         end
      end
      for colidx=1,#field[rowIdx] do
         --GGF.InnerInvoke(in_meta, "cellHighlightCheckSwitch", in_elem, rowIdx, colidx, true);
      end
   elseif (selectionType == GGE.SelectionType.Column) then
      colIdx = in_colIdx;
   elseif (selectionType == GGE.SelectionType.Element) then
      rowIdx = in_rowIdx;
      colIdx = in_colIdx;
      GGF.InnerInvoke(in_meta, "cellHighlightCheckSwitch", in_elem, rowIdx, colIdx, true);
   elseif (selectionType == GGE.SelectionType.None) then
      -- NOTE: ничего не делаем
      -- NOTE: возможно понадобится сброс текущего выделения
   end
   GGF.InnerInvoke(in_meta, "SetSelectedRowIdx", rowIdx);
   if (colIdx) then
      GGF.InnerInvoke(in_meta, "SetSelectedColumnIdx", colIdx);
   end
end

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-- Взятие индекса выделенной линии
function This:GetSelectionMeta(in_meta)
   local rowQuantitativeIndent, columnQuantitativeIndent = GGF.InnerInvoke(in_meta, "countQuantitativeIndent");
   local fieldRowIdx, fieldColIdx = GGF.InnerInvoke(in_meta, "GetSelectedRowIdx"), GGF.InnerInvoke(in_meta, "GetSelectedColumnIdx");
   local dataRowIdx, dataColIdx = fieldRowIdx - rowQuantitativeIndent, fieldColIdx - columnQuantitativeIndent;
   -- NOTE: сначала относительные, потом абсолютные(грубо говоря idx и realidx)
   return fieldRowIdx, fieldColIdx, dataRowIdx, dataColIdx;
end

-- Взятие данных выделенной области
function This:GetSelectionData(in_meta)
   local fieldRowIdx, fieldColIdx = GGF.InnerInvoke(in_meta, "GetSelectedRowIdx"), GGF.InnerInvoke(in_meta, "GetSelectedColumnIdx");
   if (fieldRowIdx == 0) then
      -- NOTE: выбран столбец
      return nil;
   elseif (fieldColIdx == 0) then
      -- NOTE: выбрана строка
      return GGF.TableCopy({});
   elseif (fieldRowIdx + fieldColIdx == 0) then
      -- NOTE: селекта нет
      return nil;
   end
end

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-- Установка основных объектов табличного фрейма
function This:adjustServiceObjects(in_meta)
   -- NOTE: там сеты, т.е. авто-аджаст тоже есть
   --GGF.InnerInvoke(in_meta, "AdjustPrototypeRelativeObjects");
end

-- Создание объектов, зависящих от прототипа
function This:AdjustPrototypeRelativeObjects(in_meta)
   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");
   local preallocatedFieldRowCount = GGF.OuterInvoke(visionLogicProvider, "GetPreallocatedFieldRowCount");
   local preallocatedFieldColumnCount = GGF.OuterInvoke(visionLogicProvider, "GetPreallocatedFieldColumnCount");
   local vHeaderElem = GGF.InnerInvoke(in_meta, "GetVerticalHeaderDataConglomeration");
   for rowidx=1,preallocatedFieldRowCount do
      GGF.OuterInvoke(vHeaderElem.button, "Adjust");
      GGF.OuterInvoke(vHeaderElem.fontString, "Adjust");
      GGF.OuterInvoke(vHeaderElem.texture, "Adjust");
      vHeaderElem = vHeaderElem.next;
   end

   local hHeaderElem = GGF.InnerInvoke(in_meta, "GetHorizontalHeaderDataConglomeration");
   for colidx=1,preallocatedFieldColumnCount do
      GGF.OuterInvoke(hHeaderElem.button, "Adjust");
      GGF.OuterInvoke(hHeaderElem.fontString, "Adjust");
      GGF.OuterInvoke(hHeaderElem.texture, "Adjust");
      hHeaderElem = hHeaderElem.next;
   end

   local fieldElem = GGF.InnerInvoke(in_meta, "GetCentralFieldDataConglomeration");
   for rowidx=1,preallocatedFieldRowCount do
      local fieldInnerElem = fieldElem.first;
      for colidx=1,preallocatedFieldColumnCount do
         GGF.OuterInvoke(fieldInnerElem.button, "Adjust");
         GGF.OuterInvoke(fieldInnerElem.fontString, "Adjust");
         GGF.OuterInvoke(fieldInnerElem.texture, "Adjust");
         fieldInnerElem = fieldInnerElem.next;
      end
      fieldElem = fieldElem.next;
   end
end

-- Запуск
function This:launch(in_meta)
end

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

-- Сброс выделения
function This:ClearSelection(in_meta)
   local field = GGF.InnerInvoke(in_meta, "GetCentralFieldDataConglomeration");
   for rowldx=1,#field do
      for coildx=1,#field[in_rowidx] do
         GGF.OuterInvoke(field[in_rowidx][in_colidx].texture, "SetHidden", true);
      end
   end
   GGF.InnerInvoke(in_meta, "SetSelectedRowIdx", 0);
   GGF.InnerInvoke(in_meta, "SetSelectedColumnIdx", 0);
end

-- Создание объектов слоеного типа(кнопка + строка + тектстура)
function This:createServiceObjectsB(in_meta)
   --local state = GGF.InnerInvoke(in_meta, "getCurrentState");
   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local fontHeight = GGF.OuterInvoke(tableFrame, "GetFontHeight");
   local fontStringGap = GGF.OuterInvoke(tableFrame, "GetFontStringGap");
   local borderWidth = GGF.OuterInvoke(tableFrame, "GetBorderWidth");
   local sliderWidth = GGF.OuterInvoke(tableFrame, "GetSliderWidth");
   local minCellWidth = GGF.OuterInvoke(tableFrame, "GetMinCellWidth");
   local cellHeight = GGF.OuterInvoke(tableFrame, "GetCellHeight");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");
   local config = GGF.OuterInvoke(visionLogicProvider, "GetTypeConfig");
   local frameWidth = GGF.OuterInvoke(tableFrame, "GetWidth");
   local frameHeight = GGF.OuterInvoke(tableFrame, "GetHeight");
   local hHeaderOffsetX = GGF.OuterInvoke(visionLogicProvider, "CountHHeaderOffsetX");
   local hHeaderOffsetY = GGF.OuterInvoke(visionLogicProvider, "CountHHeaderOffsetY");
   local vHeaderOffsetX = GGF.OuterInvoke(visionLogicProvider, "CountVHeaderOffsetX");
   local vHeaderOffsetY = GGF.OuterInvoke(visionLogicProvider, "CountVHeaderOffsetY");
   local dataFieldOffsetX = GGF.OuterInvoke(visionLogicProvider, "CountDataFieldOffsetX");
   local dataFieldOffsetY = GGF.OuterInvoke(visionLogicProvider, "CountDataFieldOffsetY");

   local phantomSubModule = GGF.OuterInvoke(objectProvider, "GetPhantomSubModule");
   local dynamicVerticalPhantomFrame = GGF.OuterInvoke(phantomSubModule, "GetDynamicVerticalPhantomFrameOD");
   local dynamicHorizontalPhantomFrame = GGF.OuterInvoke(phantomSubModule, "GetDynamicHorizontalPhantomFrameOD");
   local dynamicCentralPhantomFrame = GGF.OuterInvoke(phantomSubModule, "GetDynamicCentralPhantomFrameOD");

   -- NOTE: два угловых зазора(между краем таблицы и хедерами)(относительно объекта)
   local sliderArrayWidth = sliderWidth*GGF.TernExpSingle(config.hsliding, 2, 1);
   local colCount = 0;
   -- NOTE: два неучтенных отступа - перед хедером и перед полем
   local summWd = 3*borderWidth + 2*fontStringGap + sliderArrayWidth;
   while (summWd < frameWidth) do
      summWd = summWd + minCellWidth + fontStringGap;
      colCount = colCount + 1;
   end

   -- NOTE: доп. элементы, на всякий случай
   local countGap = 2;

   local fieldWidth = summWd;
   -- NOTE: гарантировано избыточное количество столбцов
   local redundantColumnCount = colCount + countGap;
   -- NOTE: гарантировано избыточное количество строк
   local redundantRowCount = math.floor(frameHeight / (cellHeight + fontStringGap)) + countGap;
   print("redundantRowCount", frameHeight, cellHeight, fontStringGap, redundantRowCount);

   GGF.OuterInvoke(visionLogicProvider, "SetVisibleObjectRegionRow", redundantRowCount - countGap);
   GGF.OuterInvoke(visionLogicProvider, "SetPreallocatedFieldRowCount", redundantRowCount);
   GGF.OuterInvoke(visionLogicProvider, "SetPreallocatedFieldColumnCount", redundantColumnCount);

   -- NOTE: ширина поля куда вплотную влезет целое число столбцов

   -- NOTE: отступы поля с данными

   local vHeader = {};
   local hHeader = {};
   local field = {};

   local parentFrame = GGF.INS.GetObjectRef(tableFrame);

   -- NOTE: располагаем объекты по умолчанию
   -- NOTE: используется двусвязный список двусвязных списков
   local prev = 0;
   local first = 0;
   for rowidx=1,redundantRowCount do
      local elem = {
         index = rowidx,
         geo_index = rowidx,
         data_index = 0
      };
      if (rowidx == 1) then
         first = elem;
      else
         prev.next = elem;
         elem.prev = prev;
      end
      prev = elem;
      GGF.InnerInvoke(
         in_meta, "createVerticalHeaderElement", elem,
         parentFrame, rowidx, 0,
         dynamicVerticalPhantomFrame,
         nil, minCellWidth, cellHeight, fontHeight,
         vHeaderOffsetX, vHeaderOffsetY - (rowidx - 1)*(cellHeight + fontStringGap),
         function() GGF.InnerInvoke(in_meta, "SetRowLocked", rowidx) end,
         function() end,
         function() end
      );
   end
   -- NOTE: закольцовка списка
   first.prev = prev;
   prev.next = first;
   GGF.InnerInvoke(in_meta, "SetVerticalHeaderDataConglomerationOD", first);

   local offsetX = hHeaderOffsetX;
   for colidx=1,redundantColumnCount do
      local elem = {
         index = colidx,
         geo_index = colidx,
         data_index = 0
      };
      if (colidx == 1) then
         first = elem;
      else
         prev.next = elem;
         elem.prev = prev;
      end
      prev = elem;
      GGF.InnerInvoke(
         in_meta, "createHorizontalHeaderElement", elem,
         parentFrame, 0, colidx,
         dynamicHorizontalPhantomFrame,
         nil, minCellWidth, cellHeight, fontHeight,
         offsetX, hHeaderOffsetY,
         function() GGF.InnerInvoke(in_meta, "SetColumnLocked", colidx) end,
         function() end,
         function() end
      );
      offsetX = offsetX + minCellWidth + fontStringGap;
   end
   -- NOTE: закольцовка списка
   first.prev = prev;
   prev.next = first;
   GGF.InnerInvoke(in_meta, "SetHorizontalHeaderDataConglomerationOD", first);

   for rowidx=1,redundantRowCount do
      local elem = {
         index = rowidx,
         geo_index = rowidx,
         data_index = 0
      };
      if (rowidx == 1) then
         first = elem;
      else
         prev.next = elem;
         elem.prev = prev;
      end
      prev = elem;
      local offsetX = dataFieldOffsetX;
      local innerFirst = 0;
      local innerPrev = 0;
      for colidx=1,redundantColumnCount do
         local innerElem = {
            index = colidx,
            geo_index = colidx,
            data_index = 0
         };
         if (colidx == 1) then
            innerFirst = innerElem;
         else
            innerPrev.next = innerElem;
            innerElem.prev = innerPrev;
         end
         innerPrev = innerElem;
         GGF.InnerInvoke(
            in_meta, "createCentralFieldElement", innerElem,
            parentFrame, rowidx, colidx,
            dynamicCentralPhantomFrame,
            nil, minCellWidth, cellHeight, fontHeight,
            offsetX, vHeaderOffsetY - (rowidx - 1)*(cellHeight + fontStringGap),
            function() GGF.InnerInvoke(in_meta, "SetSelection", innerElem, rowidx, colidx) end,
            function() GGF.InnerInvoke(in_meta, "cellHighlightCheckSwitch", innerElem, rowidx, colidx, true) end,
            function() GGF.InnerInvoke(in_meta, "cellHighlightCheckSwitch", innerElem, rowidx, colidx, false) end
         );
         offsetX = offsetX + minCellWidth + fontStringGap;
      end
      -- NOTE: закольцовка списка
      innerFirst.prev = innerPrev;
      innerPrev.next = innerFirst;
      elem.first = innerFirst;
   end
   -- NOTE: закольцовка списка
   first.prev = prev;
   prev.next = first;
   GGF.InnerInvoke(in_meta, "SetCentralFieldDataConglomerationOD", first);
end

-- Корректор коллизий поля данных с границами
function This:CollisionCorrector(in_meta)
   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local phantomSubModule = GGF.OuterInvoke(objectProvider, "GetPhantomSubModule");
   local dynamicCentralPhantomFrame = GGF.OuterInvoke(phantomSubModule, "GetDynamicCentralPhantomFrameOD");
   local phantomHeight, phantomWidth = GGF.OuterInvoke(dynamicCentralPhantomFrame, "GetHeight"), GGF.OuterInvoke(dynamicCentralPhantomFrame, "GetWidth");

   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local dataProvider = GGF.OuterInvoke(tableFrame, "GetDataProvider");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");
   local hHeader = GGF.InnerInvoke(in_meta, "GetHorizontalHeaderDataConglomeration");
   local vHeader = GGF.InnerInvoke(in_meta, "GetVerticalHeaderDataConglomeration");
   local centralField = GGF.InnerInvoke(in_meta, "GetCentralFieldDataConglomeration");
   local dataSet = GGF.OuterInvoke(dataProvider, "GetDataSet");
   local dataFieldOffsetX = GGF.OuterInvoke(visionLogicProvider, "CountDataFieldOffsetX");
   local dataFieldOffsetY = GGF.OuterInvoke(visionLogicProvider, "CountDataFieldOffsetY");

   elem = vHeader;
   while (elem.next ~= vHeader.next) do
      local cur_button = elem.button;
      local offsetX = GGF.OuterInvoke(cur_button, "GetOffsetY");
      local height = GGF.OuterInvoke(cur_button, "GetHeight");
      local cur_string = elem.fontString;
      GGF.OuterInvoke(cur_string, "SetColor", GGF.TernExpSingle(offsetY + dataFieldOffsetY - height < phantomHeight, GGD.Color.Yellow, GGD.Color.Green));
      elem = elem.next;
   end

   elem = hHeader;
   while (elem.next ~= hHeader.next) do
      local cur_button = elem.button;
      local offsetX = GGF.OuterInvoke(cur_button, "GetOffsetX");
      local width = GGF.OuterInvoke(cur_button, "GetWidth");
      local cur_string = elem.fontString;
      GGF.OuterInvoke(cur_string, "SetColor", GGF.TernExpSingle(offsetX + dataFieldOffsetX + width > phantomWidth, GGD.Color.Yellow, GGD.Color.Green));
      elem = elem.next;
   end
end

-- Корректор коллизий поля данных с границами
function This:CollisionCorrectorB(in_meta)
   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");
   local lowBorderOffsetX = GGF.OuterInvoke(visionLogicProvider, "GetVisibleMovingFieldOffsetX");
   local highBorderOffsetX = lowBorderOffsetX + GGF.OuterInvoke(visionLogicProvider, "GetVisibleMovingFieldWidth");
   local lowBorderOffsetY = GGF.OuterInvoke(visionLogicProvider, "GetVisibleMovingFieldOffsetY");
   local highBorderOffsetY = lowBorderOffsetY + GGF.OuterInvoke(visionLogicProvider, "GetVisibleMovingFieldHeight");
   local vHeader = GGF.InnerInvoke(in_meta, "GetVerticalHeaderDataConglomerationOD");
   local hHeader = GGF.InnerInvoke(in_meta, "GetHorizontalHeaderDataConglomerationOD");
   local centralField = GGF.InnerInvoke(in_meta, "GetCentralFieldDataConglomerationOD");
   local preallocatedFieldRowCount = GGF.OuterInvoke(visionLogicProvider, "GetPreallocatedFieldRowCount");
   local preallocatedFieldColumnCount = GGF.OuterInvoke(visionLogicProvider, "GetPreallocatedFieldColumnCount");

   local topCollisionDetected, bottomCollisionDetected, leftCollisionDetected, rightCollisionDetected = false, false, false, false;

   local phantomSubModule = GGF.OuterInvoke(objectProvider, "GetPhantomSubModule");
   local dynamicCentralPhantomFrame = GGF.OuterInvoke(phantomSubModule, "GetDynamicCentralPhantomFrameOD");
   local dynamicOffsetX = GGF.OuterInvoke(dynamicCentralPhantomFrame, "GetOffsetX");
   local dynamicOffsetY = GGF.OuterInvoke(dynamicCentralPhantomFrame, "GetOffsetY");
   local dataFieldHeight = GGF.OuterInvoke(visionLogicProvider, "CountDataFieldHeight");
   local dataFieldWidth = GGF.OuterInvoke(visionLogicProvider, "CountDataFieldWidth");
   local vHeaderWidth = GGF.OuterInvoke(visionLogicProvider, "CountVHeaderWidth");
   local hHeaderHeight = GGF.OuterInvoke(visionLogicProvider, "CountHHeaderHeight");

   local dataProvider = GGF.OuterInvoke(tableFrame, "GetDataProvider");
   local dataSet = GGF.OuterInvoke(dataProvider, "GetDataSet");
   local rowCount, colCount = #dataSet.fields, #dataSet.fields[1];

   local elem = vHeader;
   local extraTopRows, extraBottomRows = 0, 0;
   local extraLeftColumns, extraRightColumns = 0, 0;
   local visibleRowCount, visibleColCount = 0, 0;
   for rowidx=1,preallocatedFieldRowCount do
      local cur_button = elem.button;
      local button_offset = GGF.OuterInvoke(cur_button, "GetOffsetY");
      local dOffsetY = button_offset + dynamicOffsetY;
      if (dOffsetY > - hHeaderHeight) then
         local str = elem.fontString;
         GGF.OuterInvoke(str, "SetColor", GGD.Color.Yellow);
         elem.data_index = 0;
         extraTopRows = extraTopRows + 1;
      elseif (dOffsetY < - dataFieldHeight) then
         local str = elem.fontString;
         GGF.OuterInvoke(str, "SetColor", GGD.Color.Yellow);
         elem.data_index = 0;
         extraBottomRows = extraBottomRows + 1;
      else
         local str = elem.fontString;
         -- NOTE: выставляем значение, только если его там не было
         if (elem.data_index == 0) then
            if (elem.next.data_index ~= 0) then
               elem.data_index = elem.next.data_index - 1;
            elseif (elem.prev.data_index ~= 0 and elem.prev.data_index < rowCount) then
               elem.data_index = elem.prev.data_index + 1;
            else
            end
         end
         GGF.OuterInvoke(str, "SetColor", GGD.Color.Green);
         visibleRowCount = visibleRowCount + 1;
      end
      elem = elem.next;
   end
   GGF.OuterInvoke(visionLogicProvider, "SetVisibleMovingFieldRowCount", visibleRowCount);

   local verticalIndexMap = {};
   elem = hHeader;
   for colidx=1,preallocatedFieldColumnCount do
      local cur_button = elem.button;
      local button_offset = GGF.OuterInvoke(cur_button, "GetOffsetX");
      local dOffsetX = button_offset + dynamicOffsetX;
      if (dOffsetX < vHeaderWidth) then
         local str = elem.fontString;
         GGF.OuterInvoke(str, "SetColor", GGD.Color.Yellow);
         elem.data_index = 0;
         extraLeftColumns = extraLeftColumns + 1;
      elseif (dOffsetX > dataFieldWidth) then
         local str = elem.fontString;
         GGF.OuterInvoke(str, "SetColor", GGD.Color.Yellow);
         --print("right offsetX", dOffsetX, dataFieldWidth);
         elem.data_index = 0;
         extraRightColumns = extraRightColumns + 1;
      else
         local str = elem.fontString;
         -- NOTE: выставляем значение, только если его там не было
         if (elem.data_index == 0) then
            if (elem.next.data_index ~= 0) then
               elem.data_index = elem.next.data_index - 1;
            elseif (elem.prev.data_index ~= 0 and elem.prev.data_index < colCount) then
               elem.data_index = elem.prev.data_index + 1;
            else
            end
         end
         GGF.OuterInvoke(str, "SetColor", GGD.Color.Green);
         visibleColCount = visibleColCount + 1;
      end
      verticalIndexMap[colidx] = elem.data_index;
      elem = elem.next;
   end
   GGF.OuterInvoke(visionLogicProvider, "SetVisibleMovingFieldColumnCount", visibleColCount);

   elem = centralField;
   for rowidx=1,preallocatedFieldRowCount do
      local innerElem = elem.first;
      for colidx=1,preallocatedFieldColumnCount do
         local cur_button = innerElem.button;
         local button_offset_X = GGF.OuterInvoke(cur_button, "GetOffsetX");
         local button_offset_Y = GGF.OuterInvoke(cur_button, "GetOffsetY");
         local dOffsetX = button_offset_X + dynamicOffsetX;
         local dOffsetY = button_offset_Y + dynamicOffsetY;
         if (dOffsetX < vHeaderWidth or dOffsetX > dataFieldWidth) then
            local str = innerElem.fontString;
            innerElem.data_index = 0;
            GGF.OuterInvoke(str, "SetColor", GGD.Color.Yellow);
         elseif (dOffsetY > - hHeaderHeight or dOffsetY < - dataFieldHeight) then
            local str = innerElem.fontString;
            innerElem.data_index = 0;
            elem.data_index = 0;
            GGF.OuterInvoke(str, "SetColor", GGD.Color.Yellow);
         else
            local str = innerElem.fontString;
            -- NOTE: выставляем значение, только если его там не было
            if (innerElem.data_index == 0) then
               if (innerElem.next.data_index ~= 0) then
                  innerElem.data_index = innerElem.next.data_index - 1;
               elseif (innerElem.prev.data_index ~= 0 and innerElem.prev.data_index < colCount) then
                  innerElem.data_index = innerElem.prev.data_index + 1;
               else
                  innerElem.data_index = verticalIndexMap[colidx];
               end
            end
            -- WARNING: неустойчивая конструкция
            if (elem.data_index == 0) then
               if (elem.next.data_index ~= 0) then
                  elem.data_index = elem.next.data_index - 1;
               elseif (elem.prev.data_index ~= 0 and elem.prev.data_index < rowCount) then
                  elem.data_index = elem.prev.data_index + 1;
               else
               end
            end
            GGF.OuterInvoke(str, "SetColor", GGD.Color.Green);
         end
         innerElem = innerElem.next;
      end
      elem = elem.next;
   end

   -- NOTE: перекидываем лишние
   local recountRequested = false;
   local extraLRGap = math.floor((extraLeftColumns - extraRightColumns)/2);
   local extraRLGap = math.floor((extraRightColumns - extraLeftColumns)/2);
   local extraTBGap = math.floor((extraTopRows - extraBottomRows)/2);
   local extraBTGap = math.floor((extraBottomRows - extraTopRows)/2);

   local elem = hHeader;
   local joint_prev_point, joint_next_point = 0, 0;
   local mark_index_prev, mark_index_next = 0, 0;
   for rowidx=1,preallocatedFieldColumnCount do
      if GGF.OuterInvoke(elem.button, "GetOffsetX") > GGF.OuterInvoke(elem.next.button, "GetOffsetX") then
         recountRequested = true;
         joint_next_point = elem.next;
         joint_prev_point = elem;
         mark_index_next = joint_next_point.geo_index;
         mark_index_prev = joint_prev_point.geo_index;
      end
      --print("elem geo ind", elem.geo_index, elem.data_index, GGF.OuterInvoke(elem.button, "GetOffsetX"));
      elem = elem.next
   end

   -- NOTE: vertical header - LEFT -> RIGHT
   point = joint_next_point;
   for stepX=1,extraLRGap do
      prev_point = point.prev;
      local oldindex = point.geo_index;
      point.geo_index = prev_point.geo_index + 1;
      --print("change index left hdr", oldindex, "->", point.geo_index, prev_point.geo_index);
      point = point.next;
   end

   -- NOTE: vertical header - RIGHT -> LEFT
   point = joint_prev_point;
   for stepX=1,extraRLGap do
      next_point = point.next;
      local oldindex = point.geo_index;
      point.geo_index = next_point.geo_index - 1;
      --print("change index right hdr", oldindex, "->", point.geo_index, next_point.geo_index);
      point = point.prev;
   end

   -- NOTE: central field - LEFT -> RIGHT
   elem = centralField;
   for rowidx=1,preallocatedFieldRowCount do
      local innerElem = elem.first;
      local j_prev_point, j_next_point = 0, 0;
      --local m_index_prev, m_index_next = 0, 0;
      for colidx=1,preallocatedFieldColumnCount do
         if (innerElem.geo_index == mark_index_prev) then
            j_prev_point = innerElem;
            j_next_point = innerElem.next;
         end
         innerElem = innerElem.next;
      end
      point = j_next_point;
      for stepX=1,extraLRGap do
         prev_point = point.prev;
         local oldindex = point.geo_index;
         point.geo_index = prev_point.geo_index + 1;
         --print("change index right cf", oldindex, "->", cfpoint.geo_index, prev_point.geo_index);
         point = point.next;
      end
      elem = elem.next;
   end

   -- NOTE: central field - RIGHT -> LEFT
   elem = centralField;
   for rowidx=1,preallocatedFieldRowCount do
      local innerElem = elem.first;
      local j_prev_point, j_next_point = 0, 0;
      for colidx=1,preallocatedFieldColumnCount do
         if (innerElem.geo_index == mark_index_prev) then
            j_prev_point = innerElem;
            j_next_point = innerElem.next;
         end
         innerElem = innerElem.next;
      end
      point = j_prev_point;
      for stepX=1,extraRLGap do
         next_point = point.next;
         local oldindex = point.geo_index;
         point.geo_index = next_point.geo_index - 1;
         --print("change index right cf", oldindex, "->", cfpoint.geo_index, next_point.geo_index);
         point = point.prev;
      end
      elem = elem.next;
   end

   print("corrector gap l-r:", extraLRGap, extraRLGap, vHeaderWidth, dataFieldWidth);

   local elem = vHeader;
   local joint_prev_point, joint_next_point = 0, 0;
   local mark_index_prev, mark_index_next = 0, 0;
   for rowidx=1,preallocatedFieldRowCount do
      if GGF.OuterInvoke(elem.button, "GetOffsetY") < GGF.OuterInvoke(elem.next.button, "GetOffsetY") then
         recountRequested = true;
         joint_next_point = elem.next;
         joint_prev_point = elem;
         mark_index_next = joint_next_point.geo_index;
         mark_index_prev = joint_prev_point.geo_index;
      end
      --print("elem geo ind", elem.geo_index, elem.data_index, GGF.OuterInvoke(elem.button, "GetOffsetX"));
      elem = elem.next
   end

   -- NOTE: vertical header - TOP -> BOTTOM
   point = joint_next_point;
   for stepY=1,extraTBGap do
      prev_point = point.prev;
      local oldindex = point.geo_index;
      point.geo_index = prev_point.geo_index + 1;
      --print("change index top hdr", oldindex, "->", point.geo_index, prev_point.geo_index);
      point = point.next;
   end

   -- NOTE: vertical header - BOTTOM -> TOP
   point = joint_prev_point;
   for stepX=1,extraBTGap do
      next_point = point.next;
      local oldindex = point.geo_index;
      point.geo_index = next_point.geo_index - 1;
      --print("change index bottom hdr", oldindex, "->", point.geo_index, next_point.geo_index);
      point = point.prev;
   end

   elem = centralField;
   local j_prev_point, j_next_point = 0, 0;
   for rowidx=1,preallocatedFieldRowCount do
      if (elem.geo_index == mark_index_prev) then
         j_prev_point = elem;
         j_next_point = elem.next;
      end
      elem = elem.next;
   end

   -- NOTE: central field - TOP -> BOTTOM
   point = j_next_point;
   for stepY=1,extraTBGap do
      prev_point = point.prev;
      local oldindex = point.geo_index;
      point.geo_index = prev_point.geo_index + 1;
      --print("change index top hdr", oldindex, "->", point.geo_index, prev_point.geo_index);
      point = point.next;
   end

   -- NOTE: central field - BOTTOM -> TOP
   point = joint_prev_point;
   for stepX=1,extraBTGap do
      next_point = point.next;
      local oldindex = point.geo_index;
      point.geo_index = next_point.geo_index - 1;
      --print("change index bottom hdr", oldindex, "->", point.geo_index, next_point.geo_index);
      point = point.prev;
   end

   print("corrector gap t-p:", extraTBGap, extraBTGap, vHeaderWidth, dataFieldWidth);

   if (recountRequested) then
      GGF.InnerInvoke(in_meta, "RecountViewFieldGeometry");
   end
end

function This:dropGeoIndexChanges(in_meta)
   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");

   local hHeader = GGF.InnerInvoke(in_meta, "GetHorizontalHeaderDataConglomeration");
   local vHeader = GGF.InnerInvoke(in_meta, "GetVerticalHeaderDataConglomeration");
   local centralField = GGF.InnerInvoke(in_meta, "GetCentralFieldDataConglomeration");

   local preallocatedFieldRowCount = GGF.OuterInvoke(visionLogicProvider, "GetPreallocatedFieldRowCount");
   local preallocatedFieldColumnCount = GGF.OuterInvoke(visionLogicProvider, "GetPreallocatedFieldColumnCount");
   local elem = vHeader;
   for rowidx=1,preallocatedFieldRowCount do
      elem.geo_index = rowidx;
      elem = elem.next;
   end
   elem = hHeader;
   for colidx=1,preallocatedFieldColumnCount do
      elem.geo_index = colidx;
      elem = elem.next;
   end
   elem = centralField;
   for rowidx=1,preallocatedFieldRowCount do
      elem.geo_index = rowidx;
      local innerElem = elem.first;
      for colidx=1,preallocatedFieldColumnCount do
         innerElem.geo_index = colidx;
         innerElem = innerElem.next;
      end
      elem = elem.next;
   end
end

-- Переназначение индексов данных в кольцевых списках поля
function This:ReassignFieldDataIndex(in_meta)
   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");

   local prototype = GGF.OuterInvoke(tableFrame, "GetPrototype");
   local hHeader = GGF.InnerInvoke(in_meta, "GetHorizontalHeaderDataConglomeration");
   local vHeader = GGF.InnerInvoke(in_meta, "GetVerticalHeaderDataConglomeration");
   local centralField = GGF.InnerInvoke(in_meta, "GetCentralFieldDataConglomeration");
   local dataProvider = GGF.OuterInvoke(tableFrame, "GetDataProvider")
   local dataSet = GGF.OuterInvoke(dataProvider, "GetDataSet");

   local preallocatedFieldRowCount = GGF.OuterInvoke(visionLogicProvider, "GetPreallocatedFieldRowCount");
   local preallocatedFieldColumnCount = GGF.OuterInvoke(visionLogicProvider, "GetPreallocatedFieldColumnCount");

   local rowCount, colCount = #dataSet.fields, #dataSet.fields[1];
   local vLimit = GGF.TernExpSingle(rowCount < preallocatedFieldRowCount, rowCount, preallocatedFieldRowCount);
   local hLimit = GGF.TernExpSingle(colCount < preallocatedFieldColumnCount, colCount, preallocatedFieldColumnCount);
   print("reass", hHeader, vHeader, centralField);
   local elem = vHeader;
   for rowidx=1,vLimit do
      elem.data_index = rowidx;
      elem = elem.next;
   end

   elem = hHeader;
   local offsetX = dataFieldOffsetX;
   for colidx=1,hLimit do
      elem.data_index = colidx;
      elem = elem.next;
   end

   elem = centralField;
   for rowidx=1,vLimit do
      elem.data_index = rowidx
      local innerElem = elem.first;
      if (elem.data_index ~= 0) then
         for colidx=1,hLimit do
            innerElem.data_index = colidx;
            innerElem = innerElem.next;
         end
      end
      elem = elem.next;
   end

   GGF.InnerInvoke(in_meta, "dropGeoIndexChanges");
end

function This:countCellOffsetX(in_meta, in_elem, in_dataDriven)
   local geoIndex = in_elem.geo_index;
   local dataIndex = in_elem.data_index;
   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local prototype = GGF.OuterInvoke(tableFrame, "GetPrototype");
   local minCellWidth = GGF.OuterInvoke(tableFrame, "GetMinCellWidth");
   local fontStringGap = GGF.OuterInvoke(tableFrame, "GetFontStringGap");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");
   local dataFieldOffsetX = GGF.OuterInvoke(visionLogicProvider, "CountDataFieldOffsetX");
   local dataFieldWidth = GGF.OuterInvoke(visionLogicProvider, "CountDataFieldWidth");
   --print("cell offset X", in_dataIndex, #prototype.columns);
   -- NOTE: вариант для нижних строк вне поля
   if (not in_dataDriven) then
      return dataFieldOffsetX + minCellWidth*geoIndex;
   end
   if (dataIndex == 0) then
      if (geoIndex > 0) then
         return dataFieldWidth + (minCellWidth + fontStringGap)*(geoIndex - GGF.OuterInvoke(visionLogicProvider, "GetVisibleMovingFieldColumnCount"));
      else
         return (minCellWidth + fontStringGap)*geoIndex;
      end
   else
      ATLASSERT(dataIndex <= #prototype.columns);
      local offsetX = dataFieldOffsetX;
      for colidx=1,dataIndex do
         local curColMeta = prototype.columns[colidx];
         local width = GGF.TernExpSingle(curColMeta.width > minCellWidth, curColMeta.width, minCellWidth);
         offsetX = offsetX + width + fontStringGap;
      end
      return offsetX;
   end
end

function This:AssignDataSet(in_meta)
   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local dataProvider = GGF.OuterInvoke(tableFrame, "GetDataProvider");
   local dataSet = GGF.OuterInvoke(dataProvider, "GetDataSet");
   local prototype = GGF.OuterInvoke(tableFrame, "GetPrototype");
   local cellHeight = GGF.OuterInvoke(tableFrame, "GetCellHeight");
   local fontStringGap = GGF.OuterInvoke(tableFrame, "GetFontStringGap");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");
   local minCellWidth = GGF.OuterInvoke(tableFrame, "GetMinCellWidth");
   local hHeaderOffsetX = GGF.OuterInvoke(visionLogicProvider, "CountHHeaderOffsetX");
   local hHeaderOffsetY = GGF.OuterInvoke(visionLogicProvider, "CountHHeaderOffsetY");
   local vHeaderOffsetX = GGF.OuterInvoke(visionLogicProvider, "CountVHeaderOffsetX");
   local vHeaderOffsetY = GGF.OuterInvoke(visionLogicProvider, "CountVHeaderOffsetY");
   local dataFieldOffsetX = GGF.OuterInvoke(visionLogicProvider, "CountDataFieldOffsetX");

   local rowCount, colCount = #dataSet.fields, #dataSet.fields[1];

   local vHeader = {};
   local hHeader = {};
   local field = {};

   local parentFrame = GGF.INS.GetObjectRef(tableFrame);

   -- NOTE: располагаем объекты по умолчанию
   -- NOTE: используется двусвязный список двусвязных списков
   local prev = 0;
   local first = 0;
   for rowidx=1,rowCount do
      local elem = {
         index = rowidx,
         geo_index = rowidx,
         data_index = 0
      };
      if (rowidx == 1) then
         first = elem;
      else
         prev.next = elem;
         elem.prev = prev;
      end
      prev = elem;
      GGF.InnerInvoke(
         in_meta, "createVerticalHeaderElement", elem,
         parentFrame, rowidx, 0,
         dynamicVerticalPhantomFrame,
         --minCellWidth, cellHeight, fontHeight,
         nil, prototype.vHeader.width, cellHeight, fontHeight,
         vHeaderOffsetX, vHeaderOffsetY - (rowidx - 1)*(cellHeight + fontStringGap),
         function() GGF.InnerInvoke(in_meta, "SetRowLocked", rowidx) end,
         function() end,
         function() end
      );
   end
   -- NOTE: закольцовка списка
   first.prev = prev;
   prev.next = first;
   GGF.InnerInvoke(in_meta, "SetVerticalHeaderDataConglomerationOD", first);

   local offsetX = hHeaderOffsetX;
   for colidx=1,colCount do
      local elem = {
         index = colidx,
         geo_index = colidx,
         data_index = 0
      };
      if (colidx == 1) then
         first = elem;
      else
         prev.next = elem;
         elem.prev = prev;
      end
      prev = elem;
      local curColMeta = prototype.columns[colidx];
      local width = GGF.TernExpSingle(curColMeta.width > minCellWidth, curColMeta.width, minCellWidth);
      GGF.InnerInvoke(
         in_meta, "createHorizontalHeaderElement", elem,
         parentFrame, 0, colidx,
         dynamicHorizontalPhantomFrame,
         curColMeta.title, width, cellHeight, fontHeight,
         offsetX, hHeaderOffsetY,
         function() GGF.InnerInvoke(in_meta, "SetColumnLocked", colidx) end,
         function() end,
         function() end
      );
      offsetX = offsetX + width + fontStringGap;
      --GGF.InnerInvoke(in_meta, "countCellOffsetX", elem, true)
   end
   -- NOTE: закольцовка списка
   first.prev = prev;
   prev.next = first;
   GGF.InnerInvoke(in_meta, "SetHorizontalHeaderDataConglomerationOD", first);

   for rowidx=1,rowCount do
      local elem = {
         index = rowidx,
         geo_index = rowidx,
         data_index = 0
      };
      if (rowidx == 1) then
         first = elem;
      else
         prev.next = elem;
         elem.prev = prev;
      end
      prev = elem;
      local offsetX = dataFieldOffsetX;
      local innerFirst = 0;
      local innerPrev = 0;
      for colidx=1,colCount do
         local innerElem = {
            index = colidx,
            geo_index = colidx,
            data_index = 0
         };
         if (colidx == 1) then
            innerFirst = innerElem;
         else
            innerPrev.next = innerElem;
            innerElem.prev = innerPrev;
         end
         innerPrev = innerElem;
         local curColMeta = prototype.columns[colidx];
         local width = GGF.TernExpSingle(curColMeta.width > minCellWidth, curColMeta.width, minCellWidth);
         local strDat = dataSet.fields[rowidx][colidx];
         GGF.InnerInvoke(
            in_meta, "createCentralFieldElement", innerElem,
            parentFrame, rowidx, colidx,
            dynamicCentralPhantomFrame,
            strDat.text, width, cellHeight, fontHeight,
            offsetX, vHeaderOffsetY - (rowidx - 1)*(cellHeight + fontStringGap),
            function() GGF.InnerInvoke(in_meta, "SetSelection", innerElem, rowidx, colidx) end,
            function() GGF.InnerInvoke(in_meta, "cellHighlightCheckSwitch", innerElem, rowidx, colidx, true) end,
            function() GGF.InnerInvoke(in_meta, "cellHighlightCheckSwitch", innerElem, rowidx, colidx, false) end
         );
         offsetX = offsetX + width + fontStringGap;
      end
      -- NOTE: закольцовка списка
      innerFirst.prev = innerPrev;
      innerPrev.next = innerFirst;
      elem.first = innerFirst;
   end
   -- NOTE: закольцовка списка
   first.prev = prev;
   prev.next = first;
   GGF.InnerInvoke(in_meta, "SetCentralFieldDataConglomerationOD", first);
end

-- Пересчет сложной геометрии таблицы
function This:RecountViewFieldGeometry(in_meta)
   -- NOTE: нужна ориентировка, т.к. прототип управляет шириной
   -- NOTE: ширина столбца с номером записи
   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");
   local dataProvider = GGF.OuterInvoke(tableFrame, "GetDataProvider")

   local preallocatedFieldRowCount = GGF.OuterInvoke(visionLogicProvider, "GetPreallocatedFieldRowCount");
   local preallocatedFieldColumnCount = GGF.OuterInvoke(visionLogicProvider, "GetPreallocatedFieldColumnCount");
   local prototype = GGF.OuterInvoke(tableFrame, "GetPrototype");
   local dataFieldOffsetY = GGF.OuterInvoke(visionLogicProvider, "CountDataFieldOffsetY");
   local cellHeight = GGF.OuterInvoke(tableFrame, "GetCellHeight");
   local fontStringGap = GGF.OuterInvoke(tableFrame, "GetFontStringGap");
   --local rowIndent = GGF.OuterInvoke(visionLogicProvider, "GetRowIndent");
   --local columnIndent = GGF.OuterInvoke(visionLogicProvider, "GetColumnIndent");
   --local visibleObjectRegionRow = GGF.OuterInvoke(visionLogicProvider, "GetVisibleObjectRegionRow");
   --local visibleObjectRegionColumn = GGF.OuterInvoke(visionLogicProvider, "GetVisibleObjectRegionColumn");
   --local indexMap = GGF.OuterInvoke(visionLogicProvider, "GetIndexMap");
   local minCellWidth = GGF.OuterInvoke(tableFrame, "GetMinCellWidth");
   local hHeader = GGF.InnerInvoke(in_meta, "GetHorizontalHeaderDataConglomeration");
   local vHeader = GGF.InnerInvoke(in_meta, "GetVerticalHeaderDataConglomeration");
   local centralField = GGF.InnerInvoke(in_meta, "GetCentralFieldDataConglomeration");
   local dataSet = GGF.OuterInvoke(dataProvider, "GetDataSet");
   local rowCount, colCount = #dataSet.fields, #dataSet.fields[1];

   print("vorr, vorc", visibleObjectRegionRow, visibleObjectRegionColumn);
   print("pfrc, pfcc", preallocatedFieldRowCount, preallocatedFieldColumnCount);
   --print("ri, ci", rowIndent, columnIndent);
   print("rc, cc", rowCount, colCount);

   local vLimit = preallocatedFieldRowCount;--GGF.TernExpSingle(rowCount < preallocatedFieldRowCount, rowCount, preallocatedFieldRowCount);
   local hLimit = preallocatedFieldColumnCount;--GGF.TernExpSingle(colCount < preallocatedFieldColumnCount, colCount, preallocatedFieldColumnCount);

   local elem = vHeader;
   for rowidx=1,vLimit do
      ATLASSERT(rowidx == elem.index);
      local cur_button = elem.button;
      GGF.OuterInvoke(cur_button, "SetWidth", prototype.vHeader.width);
      

      local cur_string = elem.fontString;
      --GGF.OuterInvoke(cur_string, "SetColor", GGD.Color.Green);
      GGF.OuterInvoke(cur_string, "SetWidth", prototype.vHeader.width);
      --GGF.OuterInvoke(cur_string, "SetText", rowidx + rowIndent);

      local cur_texture = elem.texture;
      GGF.OuterInvoke(cur_texture, "SetWidth", prototype.vHeader.width);
      elem = elem.next;
      if (elem.data_index == 0) then
         GGF.OuterInvoke(cur_string, "SetText", rowidx);
         local str = elem.fontString;
         --GGF.OuterInvoke(str, "SetColor", GGD.Color.Yellow);
      else
         GGF.OuterInvoke(cur_string, "SetText", tostring(rowidx) .. "-INV");
         GGF.OuterInvoke(cur_button, "SetOffsetY", dataFieldOffsetY - (elem.geo_index - 1)*(cellHeight + fontStringGap));
      end
   end

   elem = hHeader;
   for colidx=1,hLimit do
      ATLASSERT(colidx == elem.index);
      local button = elem.button;
      local str = elem.fontString;
      local texture = elem.texture;
      if (elem.data_index ~= 0) then
         local curColMeta = prototype.columns[elem.data_index];
         local width = GGF.TernExpSingle(curColMeta.width > minCellWidth, curColMeta.width, minCellWidth);
         GGF.OuterInvoke(button, "SetWidth", width);
         GGF.OuterInvoke(button, "SetOffsetX", GGF.InnerInvoke(in_meta, "countCellOffsetX", elem, true));
         --hHeader[colidx].button:SetOffsetX(fieldOffsetX + (colidx - 1)*(minWidth + fontStringGap));
         --GGF.OuterInvoke(button, "SetOffsetX", offsetX);

         --GGF.OuterInvoke(button, "SetHidden", false);

         --GGF.OuterInvoke(str, "SetColor", GGD.Color.Green);

         GGF.OuterInvoke(str, "SetWidth", width);
         GGF.OuterInvoke(str, "SetText", curColMeta.title);

         GGF.OuterInvoke(texture, "SetWidth", width);
      else
         --GGF.OuterInvoke(str, "SetColor", GGD.Color.Yellow);

         GGF.OuterInvoke(str, "SetText", tostring(colidx) .. "-INV");
         GGF.OuterInvoke(button, "SetOffsetX", GGF.InnerInvoke(in_meta, "countCellOffsetX", elem, true));
      end
      --print("hdr offsetX", elem.geo_index, elem.data_index, GGF.OuterInvoke(button, "GetOffsetX"));
      elem = elem.next;
   end

   elem = centralField;
   for rowidx=1,vLimit do
      local dataRowIndex = elem.data_index;
      local innerElem = elem.first;
      if (elem.data_index ~= 0) then
         for colidx=1,hLimit do
            local dataColIndex = innerElem.data_index;
            local dataColGeoIndex = innerElem.geo_index;
            local button = innerElem.button;
            local str = innerElem.fontString;

            if (innerElem.data_index ~= 0) then
               local curColMeta = prototype.columns[dataColIndex];
               local width = GGF.TernExpSingle(curColMeta.width > minCellWidth, curColMeta.width, minCellWidth);
               local strDat = dataSet.fields[dataRowIndex][dataColIndex];
               GGF.OuterInvoke(button, "SetWidth", width);
               GGF.OuterInvoke(button, "SetOffsetY", dataFieldOffsetY - (elem.data_index - 1)*(cellHeight + fontStringGap));
               --GGF.OuterInvoke(button, "SetOffsetX", offsetX);
               --GGF.OuterInvoke(button, "SetHidden", false);
               --GGF.OuterInvoke(str, "SetColor", GGD.Color.Green);
               GGF.OuterInvoke(str, "SetWidth", width);
               --GGF.OuterInvoke(str, "SetText", GGF.InnerInvoke(in_meta, "countCellText", rowidx, colidx));
               GGF.OuterInvoke(str, "SetText", strDat.text);
               --GGF.OuterInvoke(str, "SetColor", strDat.textColor);
               GGF.OuterInvoke(button, "SetTooltipText", strDat.tooltip);
               --GGF.OuterInvoke(str, "SetTooltipColor", strDat.tooltipColor);
               GGF.OuterInvoke(innerElem.texture, "SetWidth", width);
               GGF.OuterInvoke(button, "SetOffsetX", GGF.InnerInvoke(in_meta, "countCellOffsetX", innerElem, true));
            else
               GGF.OuterInvoke(str, "SetText", tostring(rowidx) .. "-" .. tostring(colidx) .. "-D-INV");
               GGF.OuterInvoke(str, "SetColor", GGD.Color.Yellow);
               GGF.OuterInvoke(button, "SetOffsetX", GGF.InnerInvoke(in_meta, "countCellOffsetX", innerElem, true));
            end
            innerElem = innerElem.next;
         end
      else
         for colidx=1,hLimit do
            local str = innerElem.fontString;
            local button = innerElem.button;
            GGF.OuterInvoke(str, "SetText", tostring(rowidx) .. "-" .. tostring(colidx) .. "-INV");
            GGF.OuterInvoke(str, "SetColor", GGD.Color.Yellow);
            GGF.OuterInvoke(button, "SetOffsetX", GGF.InnerInvoke(in_meta, "countCellOffsetX", innerElem, false));
            innerElem = innerElem.next;
         end
      end
      elem = elem.next;
   end
end

--
function This:createCentralFieldElement(in_meta, in_elem, in_parentFrame, in_rowidx, in_colidx, in_dynamicCentralPhantomFrame, in_text, in_cellWidth, in_cellHeight, in_fontHeight, in_offsetX, in_offsetY, in_onClick, in_onEnter, in_onLeave)
   GGF.InnerInvoke(
      in_meta, "createUniversalTableElement", in_elem,
      in_parentFrame, "FLF", in_rowidx, in_colidx,
      function() return GGF.INS.GetObjectRef(in_dynamicCentralPhantomFrame) end,
      in_text, in_cellWidth, in_cellHeight, in_fontHeight,
      in_offsetX, in_offsetY, in_onClick, in_onEnter, in_onLeave
   );
end

--
function This:createHorizontalHeaderElement(in_meta, in_elem, in_parentFrame, in_rowidx, in_colidx, in_dynamicHorizontalPhantomFrame, in_text, in_cellWidth, in_cellHeight, in_fontHeight, in_offsetX, in_offsetY, in_onClick, in_onEnter, in_onLeave)
   GGF.InnerInvoke(
      in_meta, "createUniversalTableElement", in_elem,
      in_parentFrame, "FLHH", in_rowidx, in_colidx,
      function() return GGF.INS.GetObjectRef(in_dynamicHorizontalPhantomFrame) end,
      in_text, in_cellWidth, in_cellHeight, in_fontHeight,
      in_offsetX, in_offsetY, in_onClick, in_onEnter, in_onLeave
   );
end

--
function This:createVerticalHeaderElement(in_meta, in_elem, in_parentFrame, in_rowidx, in_colidx, in_dynamicVerticalPhantomFrame, in_text, in_cellWidth, in_cellHeight, in_fontHeight, in_offsetX, in_offsetY, in_onClick, in_onEnter, in_onLeave)
   GGF.InnerInvoke(
      in_meta, "createUniversalTableElement", in_elem,
      in_parentFrame, "FLVH", in_rowidx, in_colidx,
      function() return GGF.INS.GetObjectRef(in_dynamicVerticalPhantomFrame) end,
      in_text, in_cellWidth, in_cellHeight, in_fontHeight,
      in_offsetX, in_offsetY, in_onClick, in_onEnter, in_onLeave
   );
end

function This:createUniversalTableElement(in_meta, in_elem, in_parentFrame, in_wrapperNamePrefix, in_rowidx, in_colidx, in_relativeTo, in_text, in_cellWidth, in_cellHeight, in_fontHeight, in_offsetX, in_offsetY, in_onClick, in_onEnter, in_onLeave)
   in_elem.button = GGC.StandardButton:Create({}, {
      properties = {
         -- NOTE: Button(Frame)
         base = {
            parent = function() return in_parentFrame end,
            wrapperName = in_wrapperNamePrefix .. tostring(in_rowidx) .. tostring(in_colidx),
            template = 0,
         },
         miscellaneous = {
            hidden = false,
         },
         backdrop = GGD.Backdrop.Empty,
         size = {
            width = in_cellWidth,
            height = in_cellHeight,
         },
         anchor = {
            point = GGE.PointType.TopLeft,
            relativeTo = in_relativeTo,
            relativePoint = GGE.PointType.TopLeft,
            offsetX = in_offsetX,
            offsetY = in_offsetY,
         },
         callback = {
            onClick = in_onClick,
            onEnter = in_onEnter,
            onLeave = in_onLeave,
         },
      },
   });
   --GGF.OuterInvoke(in_elem.button, "Adjust");
   local prButton = GGF.INS.GetObjectRef(in_elem.button);
   in_elem.fontString = GGC.FontString:Create({}, {
      properties = {
         -- NOTE: Font String
         base = {
            parent = function() return prButton end,
            wrapperName = in_wrapperNamePrefix .. tostring(in_rowidx) .. tostring(in_colidx),
         },
         miscellaneous = {
            hidden = false,
            text = GGF.TernExpSingle(in_text == nil, "UE-" .. tostring(in_rowidx) .. "-" .. tostring(in_colidx), in_text),
         },
         size = {
            width = in_cellWidth,
            height = in_fontHeight,
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = function() return prButton end,
            relativePoint = GGE.PointType.Center,
         },
         color = GGD.Color.Title,
      },
   });
   --GGF.OuterInvoke(in_elem.fontString, "Adjust");
   in_elem.texture = GGC.StandardTexture:Create({}, {
      properties = {
         -- NOTE: Highlight Texture
         base = {
            parent = function() return prButton end,
            wrapperName = in_wrapperNamePrefix .. tostring(in_rowidx) .. tostring(in_colidx)
         },
         miscellaneous = {
            color = GGD.Color.Green,
            hidden = true,
         },
         size = {
            width = in_cellWidth,
            height = in_cellHeight,
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = function() return prButton end,
            relativePoint = GGE.PointType.Center,
         },
      },
   });
   --GGF.OuterInvoke(in_elem.texture, "Adjust");
end

-- Расчет смещений данных относительно поля
function This:countQuantitativeIndent(in_meta)
   local currentPage = GGF.InnerInvoke(in_meta, "GetCurrentPage");
   local pageLineIndent = (currentPage - 1)*GGF.InnerInvoke(in_meta, "GetPageCapacity");
   --print(GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetVerticalSlider"), "GetValue"), GGF.OuterInvoke(GGF.OuterInvoke(in_instance, "GetHorizontalSlider"), "GetValue"));
   return GGF.OuterInvoke(
      GGF.InnerInvoke(in_meta, "GetVerticalSlider"),
      "GetValue"
   ) + pageLineIndent,
   GGF.OuterInvoke(
      GGF.InnerInvoke(in_meta, "GetHorizontalSlider"),
      "GetValue"
   ),
   pageLineIndent;
end

-- Установка подсветки с чеком на наличие в выбранных полях(selected row/col)
function This:cellHighlightCheckSwitch(in_meta, in_elem, in_rowIdx, in_colIdx, in_state)
   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");
   local selectedRowIdx, selectedColIdx = GGF.OuterInvoke(visionLogicProvider, "GetSelectedRowIdx"), GGF.OuterInvoke(visionLogicProvider, "GetSelectedColumnIdx");
   local selectionType = GGF.OuterInvoke(tableFrame, "GetSelectionType");
   local field = GGF.InnerInvoke(in_meta, "GetCentralFieldDataConglomeration");

   if (selectionType == GGE.SelectionType.Row) then
      GGF.OuterInvoke(in_elem.texture, "SetHidden", GGF.TernExpSingle(in_rowIdx == selectedRowIdx, false, not in_state));
   elseif (selectionType == GGE.SelectionType.Column) then
      GGF.OuterInvoke(in_elem.texture, "SetHidden", GGF.TernExpSingle(in_columnIdx == selectedColIdx, false, not in_state));
   elseif (selectionType == GGE.SelectionType.Element) then
      GGF.OuterInvoke(in_elem.texture, "SetHidden", GGF.TernExpSingle((in_rowIdx == selectedRowIdx and in_columnIdx == selectedColIdx), false, not in_state));
   elseif (selectionType == GGE.SelectionType.None) then
      print("cell highlight:", GGF.INS.GetObjectSuffix(GGF.INS.GetObjectRef(in_elem.texture)));
      GGF.OuterInvoke(in_elem.texture, "SetHidden", not in_state);
   end
end