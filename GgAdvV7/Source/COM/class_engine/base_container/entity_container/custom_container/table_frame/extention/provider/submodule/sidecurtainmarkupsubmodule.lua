-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

local OBJSuffix = "SCMSM";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   SideCurtainMarkupSubModuleSuffix = GGF.DockDomName(GGD.DefineIdentifier, "SideCurtainMarkupSubModuleSuffix"),
   SideCurtainMarkupSubModule = GGF.DockDomName(GGD.ClassIdentifier, "SideCurtainMarkupSubModule"),
};

local CTList = {
   HorizontalHighBorderDelimiter = GGF.CreateCT(OBJSuffix, "HorizontalHighBorderDelimiter", "Frame"),
   HorizontalLowBorderDelimiter = GGF.CreateCT(OBJSuffix, "HorizontalLowBorderDelimiter", "Frame"),
   HorizontalHeaderDelimiter = GGF.CreateCT(OBJSuffix, "HorizontalHeaderDelimiter", "Frame"),
   HorizontalSliderDelimiter = GGF.CreateCT(OBJSuffix, "HorizontalSliderDelimiter", "Frame"),

   VerticalLeftBorderDelimiter = GGF.CreateCT(OBJSuffix, "VerticalLeftBorderDelimiter", "Frame"),
   VerticalRightBorderDelimiter = GGF.CreateCT(OBJSuffix, "VerticalRightBorderDelimiter", "Frame"),
   VerticalHeaderDelimiter = GGF.CreateCT(OBJSuffix, "VerticalHeaderDelimiter", "Frame"),
   VerticalSliderDelimiter = GGF.CreateCT(OBJSuffix, "VerticalSliderDelimiter", "Frame"),

   VerticalSlider = GGF.CreateCT(OBJSuffix, "VerticalSlider", "Frame"),
   HorizontalSlider = GGF.CreateCT(OBJSuffix, "HorizontalSlider", "Frame"),
};

GGF.GRegister(DFList.SideCurtainMarkupSubModuleSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
}, GGE.RegValType.Default);

GGF.GRegister(DFList.SideCurtainMarkupSubModule, {
   meta = {
      name = "Side Curtain Markup Sub Module",
      mark = "",  -- авт.
      suffix = GGD.SideCurtainMarkupSubModuleSuffix,
   },
});

GGC.SideCurtainMarkupSubModule.objects = {};

GGC.SideCurtainMarkupSubModule.wrappers = {
   delimiter = {
      horizontalHighBorder = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.HorizontalHighBorderDelimiter,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      horizontalLowBorder = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.HorizontalLowBorderDelimiter,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      horizontalHeader = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.HorizontalHeaderDelimiter,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      horizontalSlider = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.HorizontalSliderDelimiter,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      verticalLeftBorder = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.VerticalLeftBorderDelimiter,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      verticalRightBorder = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.VerticalRightBorderDelimiter,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      verticalHeader = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.VerticalHeaderDelimiter,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      verticalSlider = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.VerticalSliderDelimiter,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
   standardSlider = {
      vertical = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.VerticalSlider,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      horizontal = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.HorizontalSlider,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.SideCurtainMarkupSubModule.properties = {};

GGC.SideCurtainMarkupSubModule.elements = {};

GGF.Mark(GGC.SideCurtainMarkupSubModule);

GGC.SideCurtainMarkupSubModule = GGF.TableSafeExtend(GGC.SideCurtainMarkupSubModule, GGC.TableExtentionContainer);

GGF.CreateClassWorkaround(GGC.SideCurtainMarkupSubModule);

local This = GGC.SideCurtainMarkupSubModule;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-- Установка основных объектов табличного фрейма
function This:adjustServiceObjects(in_meta)
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetHorizontalHighBorderDelimiter"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetHorizontalLowBorderDelimiter"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetVerticalLeftBorderDelimiter"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetVerticalRightBorderDelimiter"), "Adjust");

   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetHorizontalHeaderDelimiter"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetVerticalHeaderDelimiter"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetHorizontalSliderDelimiter"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetVerticalSliderDelimiter"), "Adjust");

   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetVerticalSlider"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetHorizontalSlider"), "Adjust");
end

-- Запуск
function This:launch(in_meta)
   GGF.InnerInvoke(in_meta, "adjustServiceObjects");
end

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

-- Создание основных объектов табличного фрейма
function This:createServiceObjects(in_meta)
   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local phantomSubModule = GGF.OuterInvoke(objectProvider, "GetPhantomSubModule");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");

   local sliderFrameBackdrop = GGD.Backdrop.VerticalSlider;
   local fontStringGap = GGF.OuterInvoke(tableFrame, "GetFontStringGap");
   local parentFrameObject = GGF.INS.GetObjectRef(tableFrame);
   local borderWidth = GGF.OuterInvoke(tableFrame, "GetBorderWidth");
   local staticPhantomFrame = GGF.OuterInvoke(phantomSubModule, "GetStaticPhantomFrame");
   local staticPhantomObject = GGF.INS.GetObjectRef(staticPhantomFrame);
   local config = GGF.OuterInvoke(visionLogicProvider, "GetTypeConfig");
   local verticalSliderArrayWidth = GGF.OuterInvoke(tableFrame, "GetSliderWidth");
   local horizontalSliderArrayHeight = GGF.OuterInvoke(tableFrame, "GetSliderWidth")*GGF.TernExpSingle(config.hsliding, 1, 0);

   print("phantom:", staticPhantomObject, parentFrameObject);
   GGF.InnerInvoke(in_meta, "SetHorizontalHighBorderDelimiterOD", GGC.LineFrame:Create({}, {
      properties = {
         base = {
            parent = function() return parentFrameObject end,
            wrapperName = "HHBD",
         },
         miscellaneous = {
            color = GGF.TableCopy(GGD.Color.Grey),
            hidden = false,
         },
         size = {
            width = borderWidth,
         },
         anchor = {
            relativeTo = function() return staticPhantomObject end,
            relativePoint = GGE.PointType.TopLeft,
         },
         secondAnchor = {
            offsetX = function() return GGF.OuterInvoke(staticPhantomFrame, "GetWidth") end,
         },
      },
   }));
   GGF.InnerInvoke(in_meta, "SetHorizontalLowBorderDelimiterOD", GGC.LineFrame:Create({}, {
      properties = {
         base = {
            parent = function() return parentFrameObject end,
            wrapperName = "HLBD",
         },
         miscellaneous = {
            color = GGF.TableCopy(GGD.Color.Grey),
            hidden = false,
         },
         size = {
            width = borderWidth,
         },
         anchor = {
            relativeTo = function() return staticPhantomObject end,
            relativePoint = GGE.PointType.BottomLeft,
         },
         secondAnchor = {
            offsetX = function() return GGF.OuterInvoke(staticPhantomFrame, "GetWidth") end,
         },
      },
   }));
   GGF.InnerInvoke(in_meta, "SetVerticalLeftBorderDelimiterOD", GGC.LineFrame:Create({}, {
      properties = {
         base = {
            parent = function() return parentFrameObject end,
            wrapperName = "VLBD",
         },
         miscellaneous = {
            color = GGF.TableCopy(GGD.Color.Grey),
            hidden = false,
         },
         size = {
            width = borderWidth,
         },
         anchor = {
            relativeTo = function() return staticPhantomObject end,
            relativePoint = GGE.PointType.TopLeft,
         },
         secondAnchor = {
            offsetY = function() return - GGF.OuterInvoke(staticPhantomFrame, "GetHeight") end,
         },
      },
   }));
   GGF.InnerInvoke(in_meta, "SetVerticalRightBorderDelimiterOD", GGC.LineFrame:Create({}, {
      properties = {
         base = {
            parent = function() return parentFrameObject end,
            wrapperName = "VRBD",
         },
         miscellaneous = {
            color = GGF.TableCopy(GGD.Color.Grey),
            hidden = false,
         },
         size = {
            width = borderWidth,
         },
         anchor = {
            relativeTo = function() return staticPhantomObject end,
            relativePoint = GGE.PointType.TopRight,
         },
         secondAnchor = {
            offsetY = function() return - GGF.OuterInvoke(staticPhantomFrame, "GetHeight") end,
         },
      },
   }));
   GGF.InnerInvoke(in_meta, "SetVerticalHeaderDelimiterOD", GGC.LineFrame:Create({}, {
      properties = {
         base = {
            parent = function() return parentFrameObject end,
            wrapperName = "VHD",
         },
         miscellaneous = {
            color = GGF.TableCopy(GGD.Color.Grey),
            hidden = false,
         },
         size = {
            width = borderWidth,
         },
         anchor = {
            relativeTo = function() return staticPhantomObject end,
            relativePoint = GGE.PointType.TopLeft,
            offsetX = function() return GGF.OuterInvoke(visionLogicProvider, "CountDataFieldOffsetX") end,
         },
         secondAnchor = {
            offsetX = function() return GGF.OuterInvoke(visionLogicProvider, "CountDataFieldOffsetX") end,
            offsetY = function() return - GGF.OuterInvoke(staticPhantomFrame, "GetHeight") end,
         },
      },
   }));
   GGF.InnerInvoke(in_meta, "SetHorizontalHeaderDelimiterOD", GGC.LineFrame:Create({}, {
      properties = {
         base = {
            parent = function() return parentFrameObject end,
            wrapperName = "HHD",
         },
         miscellaneous = {
            color = GGD.Color.Grey,
            hidden = false,
         },
         size = {
            width = borderWidth,
         },
         anchor = {
            relativeTo = function() return staticPhantomObject end,
            relativePoint = GGE.PointType.TopLeft,
            offsetY = function() return GGF.OuterInvoke(visionLogicProvider, "CountDataFieldOffsetY") end,
         },
         secondAnchor = {
            offsetX = function() return GGF.OuterInvoke(staticPhantomFrame, "GetWidth") end,
            offsetY = function() return GGF.OuterInvoke(visionLogicProvider, "CountDataFieldOffsetY") end,
         },
      },
   }));
   GGF.InnerInvoke(in_meta, "SetVerticalSliderDelimiterOD", GGC.LineFrame:Create({}, {
      properties = {
         base = {
            parent = function() return parentFrameObject end,
            wrapperName = "VSD",
         },
         miscellaneous = {
            color = GGF.TableCopy(GGD.Color.Grey),
            hidden = false,
         },
         size = {
            width = borderWidth,
         },
         anchor = {
            relativeTo = function() return staticPhantomObject end,
            relativePoint = GGE.PointType.TopRight,
            offsetX = function() return - verticalSliderArrayWidth - 2*fontStringGap end,
         },
         secondAnchor = {
            offsetX = function() return - verticalSliderArrayWidth - 2*fontStringGap end,
            offsetY = function() return - GGF.OuterInvoke(staticPhantomFrame, "GetHeight") end,
         },
      },
   }));
   GGF.InnerInvoke(in_meta, "SetHorizontalSliderDelimiterOD", GGC.LineFrame:Create({}, {
      properties = {
         base = {
            parent = function() return parentFrameObject end,
            wrapperName = "HSD",
         },
         miscellaneous = {
            color = GGD.Color.Grey,
            hidden = false,
         },
         size = {
            width = borderWidth,
         },
         anchor = {
            relativeTo = function() return staticPhantomObject end,
            relativePoint = GGE.PointType.BottomLeft,
            offsetY = function() return horizontalSliderArrayHeight + 2*fontStringGap end,
         },
         secondAnchor = {
            offsetX = function() return GGF.OuterInvoke(staticPhantomFrame, "GetWidth") end,
            offsetY = function() return horizontalSliderArrayHeight + 2*fontStringGap end,
         },
      },
   }));
   GGF.InnerInvoke(in_meta, "SetVerticalSliderOD", GGC.PageScrollSlider:Create({}, {
   --GGF.InnerInvoke(in_meta, "SetVerticalSliderOD", GGC.StandardSlider:Create({}, {
      properties = {
         base = {
            parent = function() return parentFrameObject end,
            --template = "UIPanelScrollBarTemplate",
            wrapperName = "V",
         },
         miscellaneous = {
            --enabled = true,
            hidden = false,
            orientation = GGE.SliderOrientation.Vertical,
            --thumbTexture = "Interface\\Buttons\\UI-SliderBar-Button-Vertical",
            --thumbTexture = function(in_meta)
               --local thTex = in_meta.object:CreateTexture();
               --thTex:SetColorTexture(0.44,0.45,0.50,0.7)
               --thTex:SetSize(16,8)
               --return thTex;
            --end,
         },
         backdrop = sliderFrameBackdrop,
         size = {
            width = verticalSliderArrayWidth,
         },
         anchor = {
            point = GGE.PointType.BottomRight,
            relativeTo = function() return staticPhantomObject end,
            relativePoint = GGE.PointType.BottomRight,
            offsetX = - fontStringGap,
            offsetY = verticalSliderArrayWidth + 3*fontStringGap,
         },
         callback = {
            onValueChanged = function(...) GGF.InnerInvoke(in_meta, "vSliderValChanged", ...); end,
         },
      },
   }));
   --GGF.InnerInvoke(in_meta, "SetHorizontalSliderOD", GGC.PageScrollSlider:Create({}, {
   GGF.InnerInvoke(in_meta, "SetHorizontalSliderOD", GGC.PageScrollSlider:Create({}, {
   --GGF.InnerInvoke(in_meta, "SetHorizontalSliderOD", GGC.StandardSlider:Create({}, {
      properties = {
         base = {
            parent = function() return parentFrameObject end,
            --template = "OptionsSliderTemplate",
            wrapperName = "H",
         },
         miscellaneous = {
            --enabled = true,
            hidden = false,
            orientation = GGE.SliderOrientation.Horizontal,
            --thumbTexture = ,
         },
         backdrop = sliderFrameBackdrop,
         size = {
            -- NOTE: это не баг(фича)
            height = verticalSliderArrayWidth,
         },
         anchor = {
            point = GGE.PointType.BottomRight,
            relativeTo = function() return staticPhantomObject end,
            relativePoint = GGE.PointType.BottomRight,
            offsetX = - verticalSliderArrayWidth - 3*fontStringGap,
            offsetY = fontStringGap,
         },
         callback = {
            onValueChanged = function(...) GGF.InnerInvoke(in_meta, "hSliderValChanged", ...) end,
         },
      },
   }));
end

-- 
function This:recountServiceGeometry(in_meta)
   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local fontStringGap = GGF.OuterInvoke(tableFrame, "GetFontStringGap");
   local phantomSubModule = GGF.OuterInvoke(objectProvider, "GetPhantomSubModule");
   local staticPhantomObject = GGF.OuterInvoke(phantomSubModule, "GetStaticPhantomFrame");
   local staticPhantomWidth = GGF.OuterInvoke(staticPhantomObject, "GetWidth");
   local staticPhantomHeight = GGF.OuterInvoke(staticPhantomObject, "GetHeight");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");
   local config = GGF.OuterInvoke(visionLogicProvider, "GetTypeConfig");
   local vSlider = GGF.InnerInvoke(in_meta, "GetVerticalSlider");
   local vSliderWidth = GGF.OuterInvoke(vSlider, "GetWidth");
   local hSlider = GGF.InnerInvoke(in_meta, "GetHorizontalSlider");
   local hSliderHeight = GGF.OuterInvoke(hSlider, "GetHeight");
   local borderWidth = GGF.OuterInvoke(tableFrame, "GetBorderWidth");
   local prototype = GGF.OuterInvoke(tableFrame, "GetPrototype");
   local vHeaderWidth = 0;
   if prototype.vHeader ~= nil then
      vHeaderWidth = prototype.vHeader.width;
   end
   local hHeaderHeight = 0;
   if prototype.hHeader ~= nil then
       hHeaderHeight = prototype.hHeader.height;
   end
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");
   local offsetX = GGF.OuterInvoke(visionLogicProvider, "CountDataFieldOffsetX");
   local offsetY = GGF.OuterInvoke(visionLogicProvider, "CountDataFieldOffsetY");
   local verticalSliderArrayWidth = GGF.OuterInvoke(tableFrame, "GetSliderWidth");
   local horizontalSliderArrayHeight = GGF.OuterInvoke(tableFrame, "GetSliderWidth");

   GGF.OuterInvoke(hSlider, "SetWidth", staticPhantomWidth - offsetX - verticalSliderArrayWidth - 3*fontStringGap);
   GGF.OuterInvoke(hSlider, "SetHidden", not config.hsliding);

   GGF.OuterInvoke(vSlider, "SetHeight", staticPhantomHeight + offsetY - horizontalSliderArrayHeight - 3*fontStringGap);
   --GGF.OuterInvoke(state.vSlider, "SetOffsetX", - state.contentOffsetX);
   --GGF.OuterInvoke(state.vSlider, "SetOffsetY", spikedSliderOffsetY(state.vSlider, state.phantomOffsetY));


   local horizontalHighBorderDelimiter = GGF.InnerInvoke(in_meta, "GetHorizontalHighBorderDelimiter");
   GGF.OuterInvoke(horizontalHighBorderDelimiter, "AdjustAnchor");

   local horizontalLowBorderDelimiter = GGF.InnerInvoke(in_meta, "GetHorizontalLowBorderDelimiter");
   GGF.OuterInvoke(horizontalLowBorderDelimiter, "AdjustAnchor");

   local verticalLeftBorderDelimiter = GGF.InnerInvoke(in_meta, "GetVerticalLeftBorderDelimiter");
   GGF.OuterInvoke(verticalLeftBorderDelimiter, "AdjustAnchor");

   local verticalRightBorderDelimiter = GGF.InnerInvoke(in_meta, "GetVerticalRightBorderDelimiter");
   GGF.OuterInvoke(verticalRightBorderDelimiter, "AdjustAnchor");

   local verticalHeaderDelimiter = GGF.InnerInvoke(in_meta, "GetVerticalHeaderDelimiter");
   GGF.OuterInvoke(verticalHeaderDelimiter, "AdjustAnchor");

   local verticalSliderDelimiter = GGF.InnerInvoke(in_meta, "GetVerticalSliderDelimiter");
   GGF.OuterInvoke(verticalSliderDelimiter, "AdjustAnchor");

   local horizontalHeaderDelimiter = GGF.InnerInvoke(in_meta, "GetHorizontalHeaderDelimiter");
   GGF.OuterInvoke(horizontalHeaderDelimiter, "AdjustAnchor");

   local horizontalSliderDelimiter = GGF.InnerInvoke(in_meta, "GetHorizontalSliderDelimiter");
   GGF.OuterInvoke(horizontalSliderDelimiter, "AdjustAnchor");
end

-- Пересчет геометрии слайдеров
function This:RecountSliderGeometry(in_meta)
   GGF.InnerInvoke(in_meta, "recountServiceGeometry");
end

-- Пересчет слайдеров
function This:RecountSliderValues(in_meta)
   GGF.InnerInvoke(in_meta, "recountSliderValues");
end

-- Пересчет слайдеров
function This:recountSliderValues(in_meta)
   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");
   local dataProvider = GGF.OuterInvoke(tableFrame, "GetDataProvider");
   local minCellWidth = GGF.OuterInvoke(tableFrame, "GetMinCellWidth");
   local dataSet = GGF.OuterInvoke(dataProvider, "GetDataSet");
   local fontStringGap = GGF.OuterInvoke(tableFrame, "GetFontStringGap");
   local cellHeight = GGF.OuterInvoke(tableFrame, "GetCellHeight");
   local config = GGF.OuterInvoke(visionLogicProvider, "GetTypeConfig");
   local pageCapacity = GGF.OuterInvoke(tableFrame, "GetPageCapacity");
   local prototype = GGF.OuterInvoke(tableFrame, "GetPrototype");
   local vSlider = GGF.InnerInvoke(in_meta, "GetVerticalSlider");
   local hSlider = GGF.InnerInvoke(in_meta, "GetHorizontalSlider");
   -- TODO: потом заменить на visibile data field параметры
   local visibleMovingFieldHeight = GGF.OuterInvoke(visionLogicProvider, "CountDataFieldHeight");
   local visibleMovingFieldWidth = GGF.OuterInvoke(visionLogicProvider, "CountDataFieldWidth");

   local rowCount = #dataSet.fields;
   local requestedHeight = fontStringGap + (cellHeight + fontStringGap)*GGF.TernExpSingle(config.paging, pageCapacity, rowCount);

   -- NOTE: Пересчитываем ширину поля с данными(за вычетом фиксированных столбцов)
   local requestedViewFieldWidth = fontStringGap;
   for colIdx,colMeta in ipairs(prototype.columns) do
      local cellWidth = GGF.TernExpSingle(colMeta.width - minCellWidth > 0, colMeta.width, minCellWidth);
      requestedViewFieldWidth = requestedViewFieldWidth + cellWidth + fontStringGap;
   end

   GGF.OuterInvoke(vSlider, "SetValues", {
      min = GGF.OuterInvoke(vSlider, "GetValuesMin"),
      max = GGF.TernExpSingle(requestedHeight - visibleMovingFieldHeight > 0, requestedHeight - visibleMovingFieldHeight, GGF.OuterInvoke(vSlider, "GetValuesMin")),
   });
   -- TODO: потом убрать(когда страницы заработают)
   GGF.OuterInvoke(vSlider, "SetValuesStep", 10);--GGF.OuterInvoke(state.vSlider, "GetValuesStep"));
   --GGF.OuterInvoke(state.vSlider, "SetStepsPerPage", fieldRowCount);--GGF.TernExpSingle(state.onPageData > fieldRowCount, onPageData - fieldRowCount, 0));
   print("hslider values", requestedViewFieldWidth, visibleMovingFieldWidth);
   if (config.hsliding ~= false) then
      GGF.OuterInvoke(hSlider, "SetValues", {
         min = GGF.OuterInvoke(hSlider, "GetValuesMin"),
         max = GGF.TernExpSingle(requestedViewFieldWidth - visibleMovingFieldWidth > 0, requestedViewFieldWidth - visibleMovingFieldWidth, GGF.OuterInvoke(hSlider, "GetValuesMin")),
      });
      GGF.OuterInvoke(hSlider, "SetValuesStep", 10);
      --GGF.OuterInvoke(state.hSlider, "SetValuesStep", 1);--GGF.OuterInvoke(state.hSlider, "GetValuesStep"));
      --GGF.OuterInvoke(state.hSlider, "SetStepsPerPage", GGF.OuterInvoke(state.hSlider, "GetValuesStepPerPage"));
   end
   --print("slider val", GGF.OuterInvoke(state.vSlider, "GetValuesMin"), GGF.OuterInvoke(state.vSlider, "GetValuesMax"), GGF.OuterInvoke(state.vSlider, "GetValuesStep"));
end

-- Коллбек для вертикального слайдера
function This:vSliderValChanged(in_meta, in_val)
   print("vslider:", in_val)
   GGF.InnerInvoke(in_meta, "proceedSliderChanges", { vertical = in_val });
end

-- Коллбек для горизонтального слайдера
function This:hSliderValChanged(in_meta, in_val)
   print("hslider:", in_val)
   GGF.InnerInvoke(in_meta, "proceedSliderChanges", { horizontal = in_val });
end

-- Пересчет сложной геометрии таблицы
function This:ProceedSliderChanges(in_meta, in_sliderChanges)
   GGF.InnerInvoke(in_meta, "proceedSliderChanges", in_sliderChanges);
end

-- Пересчет сложной геометрии таблицы
function This:proceedSliderChanges(in_meta, in_sliderChanges)
   if (not GGF.InnerInvoke(in_meta, "GetLaunched")) then
      return;
   end

   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");
   local phantomSubModule = GGF.OuterInvoke(objectProvider, "GetPhantomSubModule");
   local dataConglomerationSubModule = GGF.OuterInvoke(objectProvider, "GetDataConglomerationSubModule");

   -- HACK: там мешает неинициализированные конгломерации
   if (not GGF.OuterInvoke(dataConglomerationSubModule, "GetLaunched")) then
      return;
   end

   local prototype = GGF.OuterInvoke(tableFrame, "GetPrototype");
   local lockedRows = GGF.OuterInvoke(visionLogicProvider, "GetLockedRows");
   local dynamicVerticalPhantomFrame = GGF.OuterInvoke(phantomSubModule, "GetDynamicVerticalPhantomFrame");
   local dynamicHorizontalPhantomFrame = GGF.OuterInvoke(phantomSubModule, "GetDynamicHorizontalPhantomFrame");
   local dynamicCentralPhantomFrame = GGF.OuterInvoke(phantomSubModule, "GetDynamicCentralPhantomFrame");

   local vSliderVal = GGF.OuterInvoke(visionLogicProvider, "GetVerticalSliderValue");
   local hSliderVal = GGF.OuterInvoke(visionLogicProvider, "GetHorizontalSliderValue");
   local vSliderDiff = 0;
   local hSliderDiff = 0;
   if (in_sliderChanges.vertical ~= nil) then
      vSliderDiff = in_sliderChanges.vertical - vSliderVal;
      GGF.OuterInvoke(visionLogicProvider, "SetVerticalSliderValue", in_sliderChanges.vertical);
   end
   if (in_sliderChanges.horizontal ~= nil) then
      hSliderDiff = in_sliderChanges.horizontal - hSliderVal;
      GGF.OuterInvoke(visionLogicProvider, "SetHorizontalSliderValue", in_sliderChanges.horizontal);
   end
   --print("reco geom", vSliderDiff, hSliderDiff, in_sliderChanges.vertical, in_sliderChanges.horizontal);

   -- NOTE: ищем соответствующий первый элемент по локерам и прототипу
   --local dataSet = GGF.InnerInvoke(in_meta, "GetDataSet");
   local summaryWidth = 0;
   local startRequestIdx = 0;
   for colIdx,colData in ipairs(prototype.columns) do
      if (lockedRows[colIdx] == nil) then
         summaryWidth = summaryWidth + prototype.columns[colIdx].width;
         if (summaryWidth >= hSliderVal) then
            startRequestIdx = colIdx;
            break;
         end
      end
   end

   local dcpfX = GGF.OuterInvoke(dynamicCentralPhantomFrame, "GetOffsetX");
   local dcpfY = GGF.OuterInvoke(dynamicCentralPhantomFrame, "GetOffsetY");
   local dhpfX = GGF.OuterInvoke(dynamicHorizontalPhantomFrame, "GetOffsetX");
   local dvpfY = GGF.OuterInvoke(dynamicVerticalPhantomFrame, "GetOffsetY");

   --local width = GGF.TernExpSingle(prototype.vHeader.width > minWidth, prototype.vHeader.width, minWidth);
   if (vSliderDiff ~= 0) then
      GGF.OuterInvoke(dynamicCentralPhantomFrame, "SetOffsetY", GGF.OuterInvoke(dynamicCentralPhantomFrame, "GetOffsetY") + vSliderDiff);
      GGF.OuterInvoke(dynamicVerticalPhantomFrame, "SetOffsetY", GGF.OuterInvoke(dynamicVerticalPhantomFrame, "GetOffsetY") + vSliderDiff);
   end

   if (hSliderDiff ~= 0) then
      GGF.OuterInvoke(dynamicCentralPhantomFrame, "SetOffsetX", GGF.OuterInvoke(dynamicCentralPhantomFrame, "GetOffsetX") - hSliderDiff);
      GGF.OuterInvoke(dynamicHorizontalPhantomFrame, "SetOffsetX", GGF.OuterInvoke(dynamicHorizontalPhantomFrame, "GetOffsetX") - hSliderDiff);
   end

   --print("phantoms moved:",
      --"dcpf X:", dcpfX, "->", GGF.OuterInvoke(state.dynamicCentralPhantomFrame, "GetOffsetX"),
      --"dcpf Y:", dcpfY, "->", GGF.OuterInvoke(state.dynamicCentralPhantomFrame, "GetOffsetY"),
      --"dhpf X:", dhpfX, "->", GGF.OuterInvoke(state.dynamicHorizontalPhantomFrame, "GetOffsetX"),
      --"dvpf Y:", dvpfY, "->", GGF.OuterInvoke(state.dynamicVerticalPhantomFrame, "GetOffsetY"));
   --if (GGF.InnerInvoke(in_meta, "collisionCheck")) then
      --GGF.InnerInvoke(in_meta, "recountViewField");
   --end
   -- NOTE: CollisionCorrector временно отключен
   GGF.OuterInvoke(dataConglomerationSubModule, "CollisionCorrector");
   -- NOTE: А нахуа тут вызов пересчета по шаблону???
   --GGF.OuterInvoke(dataConglomerationSubModule, "RecountViewFieldGeometry");
end

-- 
function This:countCellText(in_meta, in_rowidx, in_colidx)
   if (#state.idxMap > in_rowidx + state.rowIndent) then
      if (#state.dataSet.fields[state.idxMap[in_rowidx + state.rowIndent]] > in_colidx + state.columnIndent) then
         return state.dataSet.fields[state.idxMap[in_rowidx + state.rowIndent]][in_colidx + state.columnIndent].text;
      end
   end
   return "";
end