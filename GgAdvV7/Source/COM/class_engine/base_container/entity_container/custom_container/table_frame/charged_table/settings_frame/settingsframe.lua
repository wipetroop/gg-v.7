	-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "STF";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	SettingsFrameSuffix = GGF.DockDomName(GGD.DefineIdentifier, "SettingsFrameSuffix"),
	SettingsFrame = GGF.DockDomName(GGD.ClassIdentifier, "SettingsFrame"),
};

local CTList = {
	SettingsFrame = GGF.CreateCT(OBJSuffix, "SettingsFrame", "Frame"),
	OnButtonEnter = GGF.CreateCT(OBJSuffix, "OnButtonEnter", "EmptyCallback"),
	OnButtonLeave = GGF.CreateCT(OBJSuffix, "OnButtonLeave", "EmptyCallback"),
	SelectionTypeSelector = GGF.CreateCT(OBJSuffix, "SelectionTypeSelector", "Frame"),
	LeftArrowButton = GGF.CreateCT(OBJSuffix, "LeftArrowButton", "Frame"),
	RightArrowButton = GGF.CreateCT(OBJSuffix, "RightArrowButton", "Frame"),
	VisibleFieldTable = GGF.CreateCT(OBJSuffix, "VisibleFieldTable", "Frame"),
	InvisibleFieldTable = GGF.CreateCT(OBJSuffix, "InvisibleFieldTable", "Frame"),
};

GGF.GRegister(DFList.SettingsFrameSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	EmptyCallback = function(in_meta) end,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.SettingsFrame, {
	meta = {
		name = "Settings Frame",
		mark = "",	-- авт.
		suffix = GGD.SettingsFrameSuffix,
	},
});

GGC.SettingsFrame.objects = {
	frame = {
		standard = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.SettingsFrame,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.SettingsFrame.Default)) then
					local settingsFrame = CreateFrame(
						"Frame",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifySettingsFrameOD", settingsFrame);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	}
};

GGC.SettingsFrame.properties = {
	callback = {
		onButtonEnter = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnButtonEnter,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onButtonLeave = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnButtonLeave,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.SettingsFrame.elements = {
	widget = {
		dropdownMenu = {
			selectionTypeSelector = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.SelectionTypeSelector,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
		button = {
			leftArrow = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.LeftArrowButton,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			rightArrow = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.RightArrowButton,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
		table = {
			visibleField = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.VisibleFieldTable,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			invisibleField = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.InvisibleFieldTable,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
	},
};

GGF.Mark(GGC.SettingsFrame);

GGC.SettingsFrame = GGF.TableSafeExtend(GGC.SettingsFrame, GGC.StandardFrame);

GGF.CreateClassWorkaround(GGC.SettingsFrame);

local This = GGC.SettingsFrame;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Установка основных объектов фильтрфрейма
function This:adjustServiceObjects(in_meta, in_filterData)
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetLeftArrowButton"), "Adjust");
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetRightArrowButton"), "Adjust");
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetVisibleFieldTable"), "Adjust");
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetInvisibleFieldTable"), "Adjust");
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSelectionTypeSelector"), "Adjust");
end


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Создание основных объектов фильтрфрейма
function This:createServiceObjects(in_meta, in_filterData)
	if true then
		return;
	end
	GGF.InnerInvoke(in_meta, "SetLeftArrowButton", GGC.StandardButton:Create({}, {
		properties = {
			base = {
				parent = in_object,
				wrapperName = "LA",
			},
			--Object = CreateFrame("Button", self:GetObjectName() .. "_LA_B", self:GetObject(), "UiPanelButtonTemplate"),
			miscellaneous = {
				hidden = false,
				enabled = true,
				highlighted = false,
				text = "<-",
			},
			backdrop = GGD.Backdrop.Empty,
			size = {
				width = 122,
				height = 29,
			},
			anchor = {
				point = GGE.PointType.Center,
				relativeTo = in_object,
				relativePoint = GGE.PointType.Center,
				offsetX = 0,
				offsetY = -10,
			},
			callback = {
				onClick = function(...) GGF.InnerInvoke(in_meta, "changeColumnVisibilityState", true) end,
				onEnter = function(...) GGF.InnerInvoke(in_meta, "GetOnButtonEnterCounter", GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetLeftArrowButton")), ...) end,
				onLeave = function(...) GGF.InnerInvoke(in_meta, "GetOnButtonLeaveCounter", GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetLeftArrowButton")), ...) end,
			},
		},
	}));

	GGF.InnerInvoke(in_meta, "SetRightArrowButton", GGC.StandardButton:Create({}, {
		properties = {
			--Object = CreateFrame("Button", self:GetObjectName() .. "_RA_B", self:GetObject(), "UiPanelButtonTemplate"),
			base = {
				parent = in_object,
				wrapperName = "RA",
			},
			miscellaneous = {
				hidden = false,
				enabled = true,
				highlighted = false,
				text = "->",
			},
			backdrop = GGD.Backdrop.Empty,
			size = {
				width = 122,
				height = 29,
			},
			anchor = {
				point = GGE.PointType.Center,
				relativeTo = in_object,
				relativePoint = GGE.PointType.Center,
				offsetX = 0,
				offsetY = 10,
			},
			callback = {
				onClick = function(...) GGF.InnerInvoke(in_meta, "changeColumnVisibilityState", false) end,
				onEnter = function(...) GGF.InnerInvoke(in_meta, "GetOnButtonEnterCounter", GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetRightArrowButton")), ...) end,
				onLeave = function(...) GGF.InnerInvoke(in_meta, "GetOnButtonLeaveCounter", GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetRightArrowButton")), ...) end,
			},
		},
	}));

	-- NOTE: временно используются ace-вские таблицы
	local dataPrototype = {
		{
			name = "field",
			width = 50,
			align = GGE.PointType.Center,
		},
	};
	local rowCount = 20;
	local fontHeight = 14;
	local highlight = false;
	GGF.InnerInvoke(in_meta, "SetVisibleFieldTable", GGC.ViewTable:Create({}, {
		properties = {
			--Object = self.hInstance:CreateST(
				--dataPrototype,
				--rowCount,
				--fontHeight,
				--highlight,
				--self:GetObject()
			--),
			base = {
				parent = in_object,
				-- NOTE: не требуется
				wrapperName = "VF",
			},
			miscellaneous = {
				hidden = false,
			},
			hardware = {
				miscellaneous = {
					selection = GGE.SelectionType.Row,
				},
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = in_object,
				relativePoint = GGE.PointType.TopLeft,
				offsetX = 10,
				offsetY = -10,
			},
			callback = {
				onCellClick = function(...) GGF.InnerInvoke(in_meta, "customOnVisibleTableClicked", ...) end,
				onCellEnter = function(...) GGF.InnerInvoke(in_meta, "customOnTableEnter", ...) end,
				onCellLeave = function(...) GGF.InnerInvoke(in_meta, "customOnTableLeave", ...) end,
			},
		},
	}));

	GGF.InnerInvoke(in_meta, "SetInvisibleFieldTable", GGC.ViewTable:Create({}, {
		properties = {
			--Object = self.hInstance:CreateST(
				--dataPrototype,
				--rowCount,
				--fontHeight,
				--highlight,
				--self:GetObject()
			--),
			base = {
				parent = in_object,
				wrapperName = "IF",
			},
			miscellaneous = {
				hidden = false,
			},
			hardware = {
				miscellaneous = {
					selection = GGE.SelectionType.Row,
				},
			},
			anchor = {
				point = GGE.PointType.TopRight,
				relativeTo = in_object,
				relativePoint = GGE.PointType.TopRight,
				offsetX = -10,
				offsetY = -10,
			},
			callback = {
				onCellClick = function(...) GGF.InnerInvoke(in_meta, "customOnInvisibleTableClicked", ...) end,
				onCellEnter = function(...) GGF.InnerInvoke(in_meta, "customOnTableEnter", ...) end,
				onCellLeave = function(...) GGF.InnerInvoke(in_meta, "customOnTableLeave", ...) end,
			},
		},
	}));

	local selectionTypes = {
		{
			description = "None",
			value = GGE.SelectionType.None,
		},
		{
			description = "Element",
			value = GGE.SelectionType.Element,
		},
		{
			description = "Row",
			value = GGE.SelectionType.Row,
		},
		{
			description = "Column",
			value = GGE.SelectionType.Column,
		},
	};

	GGF.InnerInvoke(in_meta, "SetSelectionTypeSelector", GGC.DropdownMenu:Create({}, {
		properties = {
			--Object = CreateFrame("Button", self:GetObjectName() .. "_STS_B", self:GetObject());--, "UiPanelButtonTemplate"),
			base = {
				parent = in_object,
				wrapperName = "ST",
			},
			miscellaneous = {
				hidden = false,
				enabled = true,
				highlighted = false,

				text = "Selection Type",
				--tooltipText = self:GetObjectName() .. "_TS_B",
			},
			backdrop = GGD.Backdrop.Empty,
			hardware = {
				dataSet = selectionTypes,
			},
			size = {
				width = 122,
				height = 29,
			},
			anchor = {
				point = GGE.PointType.BottomLeft,
				relativeTo = in_object,
				relativePoint = GGE.PointType.BottomLeft,
				offsetX = 20,
				offsetY = 20,
			},
			callback = {
				onEnter = function(...) GGF.InnerInvoke(in_meta, "GetOnButtonEnter", ...) end,
				onLeave = function(...) GGF.InnerInvoke(in_meta, "GetOnButtonLeave", ...) end,
			},
		},
	}));
	--self:GetSelectionTypeSelector():ModifyDataSet(selectionTypes);
end


-- TODO: потом заменить вместе с самим объектом
-- Кастомный коллбек на вход в ячейку таблицы
function This:customOnTableEnter(in_meta, in_rowFrame, in_cellFrame, in_data, in_cols, in_row, in_realrow, in_column, in_scrollingTable, ...)
	if in_data[in_realrow] == nil or in_data[in_realrow].cols == nil then
		return;
	end
	GameTooltip:SetOwner(in_cellFrame, "ANCHOR_RIGHT");
	GameTooltip:SetText(in_data[in_realrow].cols[in_column], nil, nil, nil, nil, true);
	GameTooltip:Show();
end



-- Кастомный коллбек на выход из ячейки таблицы
function This:customOnTableLeave(in_meta, in_rowFrame, in_cellFrame, in_data, in_cols, in_row, in_realrow, in_column, in_scrollingTable, ...)
	GameTooltip:Hide();
end


-- Кастомный обработчик селекта
function This:customOnVisibleTableClicked(in_meta, in_rowFrame, in_cellFrame, in_data, in_cols, in_row, in_realrow, in_column, in_table, in_button, ...)
end