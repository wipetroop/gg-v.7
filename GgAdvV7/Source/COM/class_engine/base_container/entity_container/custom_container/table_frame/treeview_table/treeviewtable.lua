-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "TVT";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	TreeViewTableSuffix = GGF.DockDomName(GGD.DefineIdentifier, "TreeViewTableSuffix"),
	TreeViewTable = GGF.DockDomName(GGD.ClassIdentifier, "TreeViewTable"),
};

local CTList = {
	Table = GGF.CreateCT(OBJSuffix, "TreeViewTable", "Frame"),
	TypeConfig = GGF.CreateCT(OBJSuffix, "TypeConfig", "TypeConfig"),
};

GGF.GRegister(DFList.TreeViewTableSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	TypeConfig = {
		paging = false,
		locking = false,
		hsliding = false,
		settings = false,
	},
}, GGE.RegValType.Default);

GGF.GRegister(DFList.TreeViewTable, {
	meta = {
		name = "Tree View Table",
		mark = "",	-- авт.
		suffix = GGD.TreeViewTableSuffix,
	},
});

GGC.TreeViewTable.object = {
	frame = {
		table = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.Table,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.Table.Default)) then
					local tableFrame = CreateFrame(
						"Frame",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							in_meta.class:getClassSuffix()
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
                  GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyTreeViewTableOD", tableFrame);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.TreeViewTable.properties = {};

GGC.TreeViewTable.elements = {
	runtime = {
		properties = {
			typeConfig = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.TypeConfig,
				fixed = true,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
	},
};

GGF.Mark(GGC.TreeViewTable);

GGC.TreeViewTable = GGF.TableSafeExtend(GGC.TreeViewTable, GGC.TableFrame);

GGF.CreateClassWorkaround(GGC.TreeViewTable);

local This = GGC.TreeViewTable;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------