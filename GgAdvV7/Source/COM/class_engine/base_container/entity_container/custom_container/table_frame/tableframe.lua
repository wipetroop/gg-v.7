-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "TF";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	TableType = GGF.DockDomName(GGD.EnumerationIdentifier, "TableType"),
	DataType = GGF.DockDomName(GGD.EnumerationIdentifier, "DataType"),
	SelectionType = GGF.DockDomName(GGD.EnumerationIdentifier, "SelectionType"),
	TableFrameSuffix = GGF.DockDomName(GGD.DefineIdentifier, "TableFrameSuffix"),
	TableFrame = GGF.DockDomName(GGD.ClassIdentifier, "TableFrame"),
};

local CTList = {
	-- objects
	TableFrame = GGF.CreateCT(OBJSuffix, "TableFrame", "Frame"),
   -- wrappers
   DataProvider = GGF.CreateCT(OBJSuffix, "DataProvider", "Frame"),
   ObjectProvider = GGF.CreateCT(OBJSuffix, "ObjectProvider", "Frame"),
   VisionLogicProvider = GGF.CreateCT(OBJSuffix, "VisionLogicProvider", "Frame"),
	-- properties
   -- miscellaneous
	Highlight = GGF.CreateCT(OBJSuffix, "Highlight", "Highlight"),
	TableType = GGF.CreateCT(OBJSuffix, "TableType", "TableType"),
	SelectionType = GGF.CreateCT(OBJSuffix, "SelectionType", "SelectionType"),
	SelectionColor = GGF.CreateCT(OBJSuffix, "SelectionColor", "SelectionColor"),
   -- geometry
	FontHeight = GGF.CreateCT(OBJSuffix, "FontHeight", "FontHeight"),
	FontStringGap = GGF.CreateCT(OBJSuffix, "FontStringGap", "FontStringGap"),
	SliderWidth = GGF.CreateCT(OBJSuffix, "SliderWidth", "SliderWidth"),
	MinCellWidth = GGF.CreateCT(OBJSuffix, "MinCellWidth", "MinCellWidth"),
	CellHeight = GGF.CreateCT(OBJSuffix, "CellHeight", "CellHeight"),
	BorderWidth = GGF.CreateCT(OBJSuffix, "BorderWidth", "BorderWidth"),
   -- paging
	RowsPerPage = GGF.CreateCT(OBJSuffix, "RowsPerPage", "RowsPerPage"),
	PageCapacity = GGF.CreateCT(OBJSuffix, "PageCapacity", "PageCapacity"),
   -- data
   Prototype = GGF.CreateCT(OBJSuffix, "Prototype", "Prototype"),
   -- callback
	OnCellEnter = GGF.CreateCT(OBJSuffix, "OnCellEnter", "EmptyCallback"),
	OnCellLeave = GGF.CreateCT(OBJSuffix, "OnCellLeave", "EmptyCallback"),
	OnCellClick = GGF.CreateCT(OBJSuffix, "OnCellClick", "EmptyCallback"),
	OnButtonClick = GGF.CreateCT(OBJSuffix, "OnUpdateButtonClick", "EmptyCallback"),
	OnButtonEnter = GGF.CreateCT(OBJSuffix, "OnUpdateButtonEnter", "EmptyCallback"),
	OnButtonLeave = GGF.CreateCT(OBJSuffix, "OnUpdateButtonLeave", "EmptyCallback"),
	-- elements
};

GGF.GRegister(DFList.TableFrameSuffix, OBJSuffix, GGE.RegValType.Suffix);

-- NOTE: заготовка. виды таблицы по степени урезанности функционала
GGF.GRegister(DFList.TableType, {
	None = 0,
	View = 1,			-- обзорная
	ListSelector = 2,	-- выбор из списка
	TreeView = 3,		-- дерево
	Charged = 4,		-- заряженная максималка(без обвеса/турбо/прямотока/и т.п.)
});

GGF.GRegister(DFList.DataType, {
	String = 1,
	Button = 2,		-- NOTE: на стадии разработки
	CheckBox = 3,	-- NOTE: на стадии разработки
	Texture = 4,	-- NOTE: на стадии разработки
});

GGF.GRegister(DFList.SelectionType, {
	None = 1,
	Element = 2,
	Row = 3,
	Column = 4,
});

GGF.GRegister(DFList.Default, {
	Frame = 0,
	Highlight = 0,
	TableType = GGE.TableType.None,
	SelectionType = GGE.SelectionType.None,
	SelectionColor = GGD.Color.Title,
	FontHeight = 12,
	FontStringGap = 2,
	SliderWidth = 20,
	MinCellWidth = 70,
	CellHeight = 14,
	BorderWidth = 1,
	RowsPerPage = 40,
	PageCapacity = 100,
   Prototype = {
      vHeader = {
         width = 40,
         justifyH = GGE.JustifyHType.Center,
      },
      columns = {
         [1] = {
            type = GGE.DataType.String,
            width = 60,
            justifyH = GGE.JustifyHType.Center,
            title = "index",
            --text = "value",
            tooltip = "index tooltip",
         },
         [2] = {
            type = GGE.DataType.String,
            width = 100,
            justifyH = GGE.JustifyHType.Center,
            title = "test",
            --text = "test value",
            tooltip = "test tooltip",
         },
         [3] = {
            type = GGE.DataType.String,
            width = 55,
            justifyH = GGE.JustifyHType.Center,
            title = "test 2",
            --text = "test value",
            tooltip = "test 2 tooltip",
         },
      },
   },
	EmptyCallback = function(in_meta)
	end,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.TableFrame, {
	meta = {
		name = "Table Frame",
		mark = "",	-- авт.
		suffix = GGD.TableFrameSuffix,
	},
});

GGC.TableFrame.objects = {
	frame = {
		table = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.TableFrame,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				-- NOTE: базовая таблица сделана чтобы отделить функционал по типу таблицы и вынести общие параметры разных типов сюды
				-- NOTE: своего рода пародия на protected конструктор, только тут все падает, а в плюсах просто не собирается...
				ATLASSERT(false);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.TableFrame.wrappers = {
   providers = {
      dataProvider = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.DataProvider,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      objectProvider = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.ObjectProvider,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      visionLogicProvider = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.VisionLogicProvider,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.TableFrame.properties = {
	-- TODO: узнать, нужно ли это вообще..
	hardware = {
		miscellaneous = {
			highlight = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.Highlight,
				fixed = false,

				-- TODO: отыскать эту функцию
				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			tableType = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.TableType,
				fixed = false,

				-- TODO: отыскать эту функцию
				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			-- NOTE: тип стратегии выбора элементов
			selection = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.SelectionType,
				fixed = false,

				-- Нет такой функции
				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			-- NOTE: цвет выделения
			selectionColor = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.SelectionColor,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
		geometry = {
			-- NOTE: высота шрифта внутри строк(и заголовок тоже)
			fontHeight = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.FontHeight,
				fixed = false,

				-- Нет такой функции
				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			-- NOTE: зазоры между краями клетки и текстом(вертикальные, привязка к высоте текста)
			fontStringGap = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.FontStringGap,
				fixed = false,

				-- Нет такой функции
				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			sliderWidth = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.SliderWidth,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			minCellWidth = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.MinCellWidth,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			cellHeight = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.CellHeight,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			borderWidth = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.BorderWidth,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
		paging = {
			-- TODO: почему 2 параметра отвечают за одно и то же???
			-- NOTE: количество строк на странице(разбиение на страницы по умолчанию включено всегда)
			rowsPerPage = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.RowsPerPage,
				fixed = false,

				-- Нет такой функции
				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			-- NOTE: количество строк на странице(если не влезают на экран, появляется слайдер)
			pageCapacity = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.PageCapacity,
				fixed = false,

				-- Нет такой функции
				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
               local objectProvider = GGF.InnerInvoke(in_meta, "GetObjectProvider");
               local sideCurtainMarkupSubModule = GGF.TernExpSingle(objectProvider == nil, function() return nil end, function() return GGF.OuterInvoke(objectProvider, "GetSideCurtainMarkupSubModule") end)();
               if not objectProvider or not sideCurtainMarkupSubModule then
                  return
               end
               print("scmsm", sideCurtainMarkupSubModule);
               GGF.OuterInvoke(sideCurtainMarkupSubModule, "ProceedSliderChanges", {});
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
	},
   data = {
      prototype = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.Prototype,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            GGF.InnerInvoke(in_meta, "AdjustPrototypeSpec");
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
	callback = {
		onCellEnter = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnCellEnter,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onCellLeave = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnCellLeave,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onCellClick = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnCellClick,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onUpdateButtonClick = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnButtonClick,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				--in_object:GetUpDateButton():GetObject():SetScript("OnClick", in_object:GetOnUpDateButtonEnterCounter());
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onUpdateButtonEnter = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnButtonEnter,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				--in_object:GetUpDateButton():GetObject():SetScript("OnEnter", function(...) in_object:GetOnUpDateButtonEnterCounter()(self:GetUpDateButton():GetObject(), ...) end;
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onUpdateButtonLeave = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnButtonLeave,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				--in_object:GetUpDateButton():GetObject():SetScript("OnLeave", function(...) in_object:GetOnUpDateButtonLeaveCounter()(self:GetUpDateButton():GetObject(), ...) end);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.TableFrame.elements = {};

GGF.Mark(GGC.TableFrame);

GGC.TableFrame = GGF.TableSafeExtend(GGC.TableFrame, GGC.StandardFrame);

GGF.CreateClassWorkaround(GGC.TableFrame);

local This = GGC.TableFrame;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Установка значения ифнормационной строки селектора страницы
function This:SetPageSelectorLimit(in_meta, in_limit)
	GGF.InnerInvoke(in_meta, "GetPageSelectorLimitFontString"):SetText("page from (1-" .. tostring(in_limit) .. ")");
end


-- Установка лока на строку таблицы
function This:SetRowLocked(in_meta, in_rowIdx)
	local lockedRows = GGF.InnerInvoke(in_meta, "GetLockedRows");
	lockedRows[in_rowIdx] = GGF.TernExpSingle(lockedRows[in_rowIdx], nil, "");
	--self:updateFieldOfView();
end


-- Установка лока на столбец таблицы
function This:SetColumnLocked(in_meta, in_colIdx)
	local lockedColumns = GGF.InnerInvoke(in_meta, "GetLockedColumns");
	lockedColumns[in_colIdx] = GGF.TernExpSingle(lockedColumns[in_colIdx], nil, "");
	--self:updateFieldOfView();
end


-- Установка значений в элемент таблицы
function This:SetTableElementData(in_meta, in_datArray, in_index, in_val, in_colorData)
   ATLASSERT(false);
   if (in_datArray[in_index] == nil) then
      in_datArray[in_index] = {};
   end
   local elem = in_datArray[in_index];
   elem.value = in_val;
   elem.color = {
      ["r"] = in_colorData.R,
      ["g"] = in_colorData.G,
      ["b"] = in_colorData.B,
      ["a"] = in_colorData.A,
   };
end

-- Установка лока на столбец таблицы
function This:SetDataSet(in_meta, in_data)
   local dataProvider = GGF.InnerInvoke(in_meta, "GetDataProvider");
   GGF.OuterInvoke(dataProvider, "SetDataSet", in_data);
   GGF.OuterInvoke(dataProvider, "AdjustDataSetSpec");
   local objectProvider = GGF.InnerInvoke(in_meta, "GetObjectProvider");
   local dataConglomerationSubModule = GGF.OuterInvoke(objectProvider, "GetDataConglomerationSubModule")
   GGF.OuterInvoke(dataConglomerationSubModule, "AssignDataSet");
   GGF.OuterInvoke(dataConglomerationSubModule, "CollisionCorrector");
end

-- TODO: доделать!!!
function This:ClearSelection(in_meta)
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие состояния объекта
function This:GetCurrentState(in_meta)
	return {
      --frame_width = GGF.InnerInvoke(in_meta, "GetWidth"),
      --frame_height = GGF.InnerInvoke(in_meta, "GetHeight"),

      -- wrappers
      --data_provider_state = GGF.OuterInvoke(GGF.InnerInvoke(), "GetCurrentState"));
      --object_provider_state = GGF.OuterInvoke(GGF.InnerInvoke(), "GetCurrentState"));
      --vision_logic_provider_state = GGF.OuterInvoke(GGF.InnerInvoke(), "GetCurrentState"));

      -- properties
      --font_height = GGF.InnerInvoke(in_meta, "GetFontHeight"),
      --font_string_gap = GGF.InnerInvoke(in_meta, "GetFontStringGap"),
      --slider_width = GGF.InnerInvoke(in_meta, "GetSliderWidth"),
      --border_width = GGF.InnerInvoke(in_meta, "GetBorderWidth"),
      --min_cell_width = GGF.InnerInvoke(in_meta, "GetMinCellWidth"),
      --cell_height = GGF.InnerInvoke(in_meta, "GetCellHeight"),

      --rows_per_page_capacity = GGF.InnerInvoke(in_meta, "GetPageCapacity"),
		--page_capacity = GGF.InnerInvoke(in_meta, "GetPageCapacity"),
	};
end


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Установка основных объектов табличного фрейма
--function This:adjustServiceObjects(in_meta)
   -- TODO: убрать, если не нужен!!!
--end


-- Установка данных
function This:AdjustPrototypeSpec(in_meta)
   local objectProvider = GGF.InnerInvoke(in_meta, "GetObjectProvider");
   local dataConglomerationSubModule = GGF.OuterInvoke(objectProvider, "GetDataConglomerationSubModule");
   GGF.OuterInvoke(dataConglomerationSubModule, "AdjustPrototypeRelativeObjects");
end

-- Запуск
function This:Launch(in_meta)
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetDataProvider"), "Launch");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetVisionLogicProvider"), "Launch");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetObjectProvider"), "Launch");
   -- NOTE: тут было закоменчено зачем-то
   local objectProvider = GGF.InnerInvoke(in_meta, "GetObjectProvider");
	GGF.OuterInvoke(GGF.OuterInvoke(objectProvider, "GetPhantomSubModule"), "recountPrototypeRelativeGeometry");
	GGF.OuterInvoke(GGF.OuterInvoke(objectProvider, "GetDataConglomerationSubModule"), "RecountViewFieldGeometry");
	local sideCurtainMarkupSubModule = GGF.OuterInvoke(objectProvider, "GetSideCurtainMarkupSubModule");
   GGF.OuterInvoke(sideCurtainMarkupSubModule, "RecountSliderValues");
	GGF.OuterInvoke(sideCurtainMarkupSubModule, "ProceedSliderChanges", {});
   local dataProvider = GGF.InnerInvoke(in_meta, "GetDataProvider");
   GGF.OuterInvoke(dataProvider, "AdjustDataSetSpec");
   local dataConglomerationSubModule = GGF.OuterInvoke(objectProvider, "GetDataConglomerationSubModule")
   GGF.OuterInvoke(dataConglomerationSubModule, "RecountViewFieldGeometry");
end

-- Установка габаритов объекта
function This:AdjustSize(in_meta)
	local size = GGF.InnerInvoke(in_meta, "GetSize");
	in_meta.object:SetSize(size.width, size.height);
	GGF.OuterInvoke(GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetObjectProvider"), "GetSideCurtainMarkupSubModule"), "RecountSliderGeometry");
end

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

-- Создание основных объектов табличного фрейма
function This:createServiceObjects(in_meta)
   GGF.InnerInvoke(in_meta, "SetDataProviderOD", GGC.DataProvider:Create({}, {
      properties = {
         structure = {
            parentInstance = function() return in_meta.instance end
         }
      }
   }));
   GGF.InnerInvoke(in_meta, "SetVisionLogicProviderOD", GGC.VisionLogicProvider:Create({}, {
      properties = {
         structure = {
            parentInstance = function() return in_meta.instance end
         }
      }, 
      elements = {
         runtime = {
            properties = {
               typeConfig = function() return GGF.TableCopy(GGF.InnerInvoke(in_meta, "GetTypeConfigOD")) end
            }
         }
      }
   }));
   GGF.InnerInvoke(in_meta, "SetObjectProviderOD", GGC.ObjectProvider:Create({}, {
      properties = {
         structure = {
            parentInstance = function() return in_meta.instance end
         }
      }
   }));
end

-- Смена состояния видимости окна настроек
function This:switchSettingsFrameState(in_meta)
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetSettingsFrame"),
		"SetHidden",
		not GGF.OuterInvoke(
			GGF.InnerInvoke(in_meta, "GetSettingsFrame"),
			"GetHidden"
		)
	);
end