-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

local OBJSuffix = "ACSM";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   AdditionalControlsSubModuleSuffix = GGF.DockDomName(GGD.DefineIdentifier, "AdditionalControlsSubModuleSuffix"),
   AdditionalControlsSubModule = GGF.DockDomName(GGD.ClassIdentifier, "AdditionalControlsSubModule"),
};

local CTList = {
   SettingsFrame = GGF.CreateCT(OBJSuffix, "SettingsFrame", "Frame"),
   SettingsButton = GGF.CreateCT(OBJSuffix, "SettingsButton", "Frame"),
   UpdateButton = GGF.CreateCT(OBJSuffix, "UpdateButton", "Frame"),

   PageSelectorLimitFontString = GGF.CreateCT(OBJSuffix, "PageSelectorLimitFontString", "Frame"),
   PageSelectorEditBox = GGF.CreateCT(OBJSuffix, "PageSelectorEditBox", "Frame"),
};

GGF.GRegister(DFList.AdditionalControlsSubModuleSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
}, GGE.RegValType.Default);

GGF.GRegister(DFList.AdditionalControlsSubModule, {
   meta = {
      name = "Additional Control Sub Module",
      mark = "",  -- авт.
      suffix = GGD.AdditionalControlsSubModuleSuffix,
   },
});

GGC.AdditionalControlsSubModule.objects = {};

GGC.AdditionalControlsSubModule.wrappers = {
   frame = {
      settings = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.SettingsFrame,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
   button = {
      settings = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.SettingsButton,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      update = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.UpdateButton,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
   fontString = {
      pageSelectorLimit = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.PageSelectorLimitFontString,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
   editBox = {
      pageSelector = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.PageSelectorEditBox,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.AdditionalControlsSubModule.properties = {};

GGC.AdditionalControlsSubModule.elements = {};

GGF.Mark(GGC.AdditionalControlsSubModule);

GGC.AdditionalControlsSubModule = GGF.TableSafeExtend(GGC.AdditionalControlsSubModule, GGC.TableExtentionContainer);

GGF.CreateClassWorkaround(GGC.AdditionalControlsSubModule);

local This = GGC.AdditionalControlsSubModule;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-- Установка основных объектов табличного фрейма
function This:adjustServiceObjects(in_meta)
   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");
   local config = GGF.OuterInvoke(visionLogicProvider, "GetTypeConfig");
   if (config.settings) then
      GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetUpdateButton"), "Adjust");
      GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSettingsButton"), "Adjust");
      GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSettingsFrame"), "Adjust");
   end
   if (config.paging) then
      GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetPageSelectorEditBox"), "Adjust");
      GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetPageSelectorLimitFontString"), "Adjust");
   end
end

-- Запуск
function This:launch(in_meta)
   GGF.InnerInvoke(in_meta, "adjustServiceObjects");
end

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

-- Создание основных объектов табличного фрейма
function This:createServiceObjects(in_meta)
   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");
   local config = GGF.OuterInvoke(visionLogicProvider, "GetTypeConfig");
   if (config.paging) then
      GGF.InnerInvoke(in_meta, "createPageSelectionTool");
   end
   if (config.settings) then
      GGF.InnerInvoke(in_meta, "createUpdateButton");
      GGF.InnerInvoke(in_meta, "createSettingsTool");
   end
end

-- Создание кнопки апдейта данных в таблице
function This:createUpdateButton(in_meta)
   -- TODO: переделать под новый стандарт
   -- TODO: вдруг можно параметры кнопки прокидывать из внешнего объекта
   GGF.InnerInvoke(in_meta, "SetUpdateButton", GGC.StandardButton:Create({}, {
      properties = {
         --Object = CreateFrame("Button", self:GetObjectName() .. "_UT_B", self:GetObject(), "UiPanelButtonTemplate"),
         base = {
            parent = function() return GGF.INS.GetObjectRef(in_instance) end,
            wrapperName = "UT",
         },
         miscellaneous = {
            hidden = false,
            enabled = true,
            highlighted = false,
            text = "Update Table",
            --tooltipText = self:GetObjectName() .. "_UT_B",
         },
         backdrop = GGD.Backdrop.Empty,
         size = {
            width = 122,
            height = 29,
         },
         anchor = {
            point = GGE.PointType.TopLeft,
            relativeTo = function() return GGF.INS.GetObjectRef(in_instance) end,
            relativePoint = GGE.PointType.TopLeft,
            offsetX = 10,
            offsetY = - 10,
         },
         callback = {
            onClick = function(...) GGF.InnerInvoke(in_meta, "GetOnUpDateButtonClickCounter")(...) end,
            onEnter = function(...) GGF.InnerInvoke(in_meta, "GetOnButtonEnterCounter")(GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetUpdateButton")), ...) end,
            onLeave = function(...) GGF.InnerInvoke(in_meta, "GetOnButtonLeaveCounter")(GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetUpdateButton")), ...) end,
         },
      },
   }));
end

-- Создание кнопки апдейта данных в таблице
function This:createSettingsTool(in_meta)
   local parentFrame = GGF.INS.GetObjectRef(in_instance);
   -- WARNING: не менять местами с кнопкой!!!(слетит привязка позиции)
   GGF.InnerInvoke(in_meta, "SetSettingsFrame", GGC.SettingsFrame:Create({}, {
      properties = {
         --Object = CreateFrame("Frame", self:GetObjectName() .. "_S_F", self:GetObject()),
         base = {
            parent = function() return parentFrame end,
            wrapperName = "S",
         },
         miscellaneous = {
            hidden = true,
            topLevel = true,
            frameLevelOffset = 2,
            movable = true,
         },
         backdrop = segregateFrameBackdrop,
         size = {
            width = 220,
            height = 350,
         },
         anchor = {
            point = GGE.PointType.TopLeft,
            relativeTo = function() return GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetSettingsButton")) end,
            relativePoint = GGE.PointType.TopRight,
            offsetX = 0,
            offsetY = 0,
         },
         callback = {
            --onButtonEnter = function(...) self:GetOnButtonEnter(...) end,
            --onButtonLeave = function(...) self:GetOnButtonLeave(...) end,
         },
      },
   }));

   GGF.InnerInvoke(in_meta, "SetSettingsButton", GGC.StandardButton:Create({}, {
      properties = {
         --Object = CreateFrame("Button", self:GetObjectName() .. "_TS_B", self:GetObject(), "UiPanelButtonTemplate"),
         base = {
            parent = function() return parentFrame end,
            template = "UIPanelButtonTemplate",
            wrapperName = "TS",
         },
         miscellaneous = {
            hidden = false,
            enabled = true,
            highlighted = false,
            text = "Table Settings",
         },
         backdrop = emptyFrameBackdrop,
         size = {
            width = 122,
            height = 29,
         },
         anchor = {
            point = GGE.PointType.Left,
            relativeTo = function() return GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetUpdateButton")) end,
            relativePoint = GGE.PointType.Right,
            offsetX = 10,
            offsetY = 0,
         },
         callback = {
            onClick = function(...) GGF.InnerInvoke(in_meta, "switchSettingsFrameState") end,
            --onEnter = function(...) self:GetOnButtonEnterCounter()(self:GetSettingsButton():GetStandardButton(), ...) end,
            --onLeave = function(...) self:GetOnButtonLeaveCounter()(self:GetSettingsButton():GetStandardButton(), ...) end,
         },
      },
   }));
end

-- Создание кнопки апдейта данных в таблице
function This:createPageSelectionTool(in_meta)
   local parentFrame = GGF.INS.GetObjectRef(in_instance);
   GGF.InnerInvoke(in_meta, "SetPageSelectorEditBox", GGC.EditBox:Create({}, {
      properties = {
         --object = CreateFrame("EditBox", self:GetObjectName() .. "_PS_EB", self:GetObject(), "InputBoxTemplate"),
         base = {
            parent = function() return parentFrame end,
            template = "InputBoxTemplate",
            wrapperName = "PS",
         },
         miscellaneous = {
            hidden = false,
            isNumeric = true,
         },
         backdrop = emptyFrameBackdrop,
         size = {
            width = 120,
            height = 20,
         },
         anchor = {
            point = GGE.PointType.TopRight,
            relativeTo = function() return parentFrame end,
            relativePoint = GGE.PointType.TopRight,
            offsetX = - 220,
            offsetY = - 10,
         },
         callback = {
            onEnterPressed = function(in_object)
               -- NOTE: пока что чек на значения будет тут
               local pageNumber = in_object:GetNumber();
               if (pageNumber > 1) or (pageNumber < GGF.InnerInvoke(in_meta, "GetPageCount")) then
                  GGF.InnerInvoke(in_meta, "SetCurrentPage", pageNumber);
               end
            end,
         },
      },
   }));

   GGF.InnerInvoke(in_meta, "SetPageSelectorLimitFontString", GGC.FontString:Create({}, {
      properties = {
         -- NOTE: Page Selector Limit(Frame)
         --Object = self:GetObject():CreateFontString(self:GetObjectName() .. "_PSL_FS", "OVERLAY", "GameFontNormal"),
         base = {
            parent = function() return parentFrame end,
            wrapperName = "PSL",
         },
         miscellaneous = {
            hidden = false,
            text = "Test psl",
         },
         size = {
            width = minWidth,
            --height = self:GetFontHeight(),
         },
         anchor = {
            point = GGE.PointType.Left,
            relativeTo = function() return GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetPageSelectorEditBox")) end,
            relativePoint = GGE.PointType.Right,
            offsetX = 0,
            offsetY = 0,
         },
         color = GGD.Color.Title,
      },
   }));
end