-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "TEC";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   TableExtentionContainerSuffix = GGF.DockDomName(GGD.DefineIdentifier, "TableExtentionContainerSuffix"),
   TableExtentionContainer = GGF.DockDomName(GGD.ClassIdentifier, "TableExtentionContainer"),
};

local CTList = {
   ParentInstance = GGF.CreateCT(OBJSuffix, "ParentInstance", "ParentInstance"),
   Launched = GGF.CreateCT(OBJSuffix, "Launched", "Launched"),
};

GGF.GRegister(DFList.TableExtentionContainerSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
   ParentInstance = function() return 0 end,
   Launched = false,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.TableExtentionContainer, {
   meta = {
      name = "Table Extention Container",
      mark = "",  -- авт.
      suffix = GGD.TableExtentionContainerSuffix,
   },
});

GGC.TableExtentionContainer.objects = {};

GGC.TableExtentionContainer.wrappers = {};

GGC.TableExtentionContainer.properties = {
   structure = {
      parentInstance = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.ParentInstance,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   }
};

GGC.TableExtentionContainer.elements = {
   runtime = {
      launched = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.Launched,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGF.Mark(GGC.TableExtentionContainer);

GGC.TableExtentionContainer = GGF.TableSafeExtend(GGC.TableExtentionContainer, GGC.AbstractContainer);

GGF.CreateClassWorkaround(GGC.TableExtentionContainer);

local This = GGC.TableExtentionContainer;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-- Запуск
function This:Launch(in_meta)
   GGF.InnerInvoke(in_meta, "SetLaunchedOD", true);
   -- NOTE: малый лаунч, если надо что-то загрузить
   --GGF.InnerInvoke(in_meta, "launch");
   GGF.NotNullInvoke(in_meta, "launch");
end

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------