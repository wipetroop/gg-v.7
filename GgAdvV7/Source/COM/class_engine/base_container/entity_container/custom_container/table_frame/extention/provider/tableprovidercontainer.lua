-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "TPC";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   TableProviderContainerSuffix = GGF.DockDomName(GGD.DefineIdentifier, "TableProviderContainerSuffix"),
   TableProviderContainer = GGF.DockDomName(GGD.ClassIdentifier, "TableProviderContainer"),
};

local CTList = {};

GGF.GRegister(DFList.TableProviderContainerSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
}, GGE.RegValType.Default);

GGF.GRegister(DFList.TableProviderContainer, {
   meta = {
      name = "Table Provider Container",
      mark = "",  -- авт.
      suffix = GGD.TableProviderContainerSuffix,
   },
});

GGC.TableProviderContainer.objects = {};

GGC.TableProviderContainer.wrappers = {};

GGC.TableProviderContainer.properties = {};

GGC.TableProviderContainer.elements = {};

GGF.Mark(GGC.TableProviderContainer);

GGC.TableProviderContainer = GGF.TableSafeExtend(GGC.TableProviderContainer, GGC.TableExtentionContainer);

GGF.CreateClassWorkaround(GGC.TableProviderContainer);

local This = GGC.TableProviderContainer;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------