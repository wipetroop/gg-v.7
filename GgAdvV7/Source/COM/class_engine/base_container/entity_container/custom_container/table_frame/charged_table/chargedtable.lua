-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "CT";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	ChargedTableSuffix = GGF.DockDomName(GGD.DefineIdentifier, "ChargedTableSuffix"),
	ChargedTable = GGF.DockDomName(GGD.ClassIdentifier, "ChargedTable"),
};

local CTList = {
	ChargedTable = GGF.CreateCT(OBJSuffix, "ChargedTable", "Frame"),
	TypeConfig = GGF.CreateCT(OBJSuffix, "TypeConfig", "TypeConfig"),
};

GGF.GRegister(DFList.ChargedTableSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	TypeConfig = {
		paging = false,
		locking = false,
		hsliding = true,
		settings = false,
	},
}, GGE.RegValType.Default);

GGF.GRegister(DFList.ChargedTable, {
	meta = {
		name = "Charged Table",
		mark = "",	-- авт.
		suffix = GGD.ChargedTableSuffix,
	},
});

GGC.ChargedTable.objects = {
	frame = {
		table = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.ChargedTable,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.ChargedTable.Default)) then
					local tableFrame = CreateFrame(
						"Frame",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							in_meta.class:getClassSuffix()
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyChargedTableOD", tableFrame);
               print("charged table:", tableFrame);
               --GGF.PrintMetaData(in_meta.instance);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.ChargedTable.properties = {};

GGC.ChargedTable.elements = {
	runtime = {
		properties = {
			typeConfig = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.TypeConfig,
				fixed = true,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
	},
};

GGF.Mark(GGC.ChargedTable);

-- NOTE: вот тут много вопросов
-- NOTE: на данном этапе эта таблица просто добавляет меню настроек, поиск и страницы
GGC.ChargedTable = GGF.TableSafeExtend(GGC.ChargedTable, GGC.ViewTable);

GGF.CreateClassWorkaround(GGC.ChargedTable);

local This = GGC.ChargedTable;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------