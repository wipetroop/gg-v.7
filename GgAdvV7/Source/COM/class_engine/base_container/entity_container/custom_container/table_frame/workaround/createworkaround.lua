local This = GGC.TableFrame;


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Создание объектов слоеного типа(кнорка + строка + тектстура)
function This:createContentFieldObjects(in_meta)
	local state = GGF.InnerInvoke(in_meta, "getCurrentState");
	-- NOTE: два угловых зазора(между краем таблицы и хедерами)(относительно объекта)
	local sliderArrayWidth = state.sliderWidth*GGF.TernExpSingle(state.config.hsliding, 2, 1);
	local colCount = 0;
	-- NOTE: два неучтенных отступа - перед хедером и перед полем
	local summWd = 3*state.borderWidth + 2*state.fontStringGap + sliderArrayWidth;
	while (summWd < state.frameWidth) do
		summWd = summWd + state.minCellWidth + state.fontStringGap;
		colCount = colCount + 1;
	end

	-- NOTE: доп. элементы, на всякий случай
	local countGap = 2;

	local fieldWidth = summWd;
	local pfColumnCount = colCount + countGap;
	local pfRowCount = math.floor(state.dataFieldHeight / (state.cellHeight + state.fontStringGap)) + countGap;

	GGF.InnerInvoke(in_meta, "SetVisibleObjectRegionRow", pfRowCount - countGap);
	GGF.InnerInvoke(in_meta, "SetPreallocatedFieldRowCount", pfRowCount);
	GGF.InnerInvoke(in_meta, "SetPreallocatedFieldColumnCount", pfColumnCount);

	-- NOTE: ширина поля куда вплотную влезет целое число столбцов

	-- NOTE: отступы поля с данными

	local vHeader = {};
	local hHeader = {};
	local field = {};

	local parentFrame = GGF.INS.GetObjectRef(in_meta.instance);

	-- NOTE: располагаем объекты по умолчанию
	for rowidx=1,pfRowCount do
		vHeader[rowidx] = {};
		vHeader[rowidx].button = GGC.StandardButton:Create({}, {
			properties = {
				-- NOTE: Frame List Vertical Header Button(Frame)
				base = {
					parent = parentFrame,
					wrapperName = "FLVH",
					template = 0,
				},
				miscellaneous = {
					hidden = false,
				},
				backdrop = GGD.Backdrop.Empty,
				size = {
					width = state.minCellWidth,
					height = state.cellHeight,
				},
				anchor = {
					point = GGE.PointType.TopLeft,
					relativeTo = GGF.INS.GetObjectRef(state.dynamicVerticalPhantomFrame),
					relativePoint = GGE.PointType.TopLeft,
					offsetX = state.vHeaderOffsetX,
					offsetY = state.vHeaderOffsetY - (rowidx - 1)*(state.cellHeight + state.fontStringGap),
				},
				callback = {
					onClick = function() GGF.InnerInvoke(in_meta, "SetRowLocked", rowidx) end,
					onEnter = function() end,
					onLeave = function() end,
				},
			},
		});
		local prButton = GGF.INS.GetObjectRef(vHeader[rowidx].button);
		--print("tabf ccfo:", prButton, prButton.GetName, parentFrame, parentFrame.GetName);
		vHeader[rowidx].fontString = GGC.FontString:Create({}, {
			properties = {
				-- NOTE: Frame List Vertical Header Font String(Frame)
				base = {
					parent = prButton,
					wrapperName = "FLVH",
				},
				miscellaneous = {
					hidden = false,
					text = "Row: " .. tostring(rowidx),
				},
				size = {
					width = state.minCellWidth,
					height = state.fontHeight,
				},
				anchor = {
					point = GGE.PointType.Center,
					relativeTo = prButton,
					relativePoint = GGE.PointType.Center,
				},
				color = GGF.TableCopy(GGD.Color.Title),
			},
		});
		vHeader[rowidx].texture = GGC.StandardTexture:Create({}, {
			properties = {
				-- NOTE: Frame List Vertical Header Highlight Texture
				base = {
					parent = prButton,
					wrapperName = "FLVHH",
				},
				miscellaneous = {
					color = GGF.TableCopy(GGD.Color.Green),
					hidden = true,
				},
				size = {
					width = state.minCellWidth,
					height = state.cellHeight,
				},
				anchor = {
					point = GGE.PointType.Center,
					relativeTo = prButton,
					relativePoint = GGE.PointType.Center,
				},
			},
		});
	end
	GGF.InnerInvoke(in_meta, "SetVerticalHeaderDataConglomerationOD", vHeader);

	local offsetX = state.hHeaderOffsetX;
	for colidx=1,pfColumnCount do
		hHeader[colidx] = {};
		hHeader[colidx].button = GGC.StandardButton:Create({}, {
			properties = {
				-- NOTE: Frame List Horizontal Header Button(Frame)
				base = {
					parent = parentFrame,
					wrapperName = "FLHH",
					template = 0,
				},
				miscellaneous = {
					hidden = false,
				},
				backdrop = GGD.Backdrop.Empty,
				size = {
					width = state.minCellWidth,
					height = state.cellHeight,
				},
				anchor = {
					point = GGE.PointType.TopLeft,
					relativeTo = GGF.INS.GetObjectRef(state.dynamicHorizontalPhantomFrame),
					relativePoint = GGE.PointType.TopLeft,
					offsetX = offsetX,
					offsetY = state.hHeaderOffsetY,
				},
				callback = {
					onClick = function() GGF.InnerInvoke(in_meta, "SetColumnLocked", colidx) end,
					onEnter = function()
						--self:cellHighlightCheckSwitch(rowidx, colidx, true);
						--self.framelist.field[rowidx][colidx].texture:SetColor(GGF.TableCopy(selectionColor));
						--self.framelist.field[rowidx][colidx].texture:SetHidden(false);
					end,
					onLeave = function()
						--self:cellHighlightCheckSwitch(rowidx, colidx, false);
						--self.framelist.field[rowidx][colidx].texture:SetHidden(true);
					end
				},
			},
		});
		local prButton = GGF.INS.GetObjectRef(hHeader[colidx].button);
		hHeader[colidx].fontString = GGC.FontString:Create({}, {
			properties = {
				-- NOTE: Frame List Horizontal Header Font String
				base = {
					parent = prButton,
					wrapperName = "FLHH",
				},
				miscellaneous = {
					hidden = false,
					text = "Col: " .. tostring(colidx),
				},
				size = {
					width = state.minCellWidth,
				},
				anchor = {
					point = GGE.PointType.Center,
					relativeTo = prButton,
					relativePoint = GGE.PointType.Center,
				},
				color = GGD.Color.Title,
			},
		});
		hHeader[colidx].texture = GGC.StandardTexture:Create({}, {
			properties = {
				-- NOTE: Frame List Horizontal Header Highlight Texture
				base = {
					parent = prButton,
					wrapperName = "FLHHH"
				},
				miscellaneous = {
					color = GGD.Color.Green,
					hidden = true,
				},
				size = {
					width = state.minCellWidth,
				},
				anchor = {
					point = GGE.PointType.Center,
					relativeTo = prButton,
					relativePoint = GGE.PointType.Center,
				},
			},
		});
		offsetX = offsetX + state.minCellWidth + state.fontStringGap;
	end
	GGF.InnerInvoke(in_meta, "SetHorizontalHeaderDataConglomerationOD", hHeader);

	for rowidx=1,pfRowCount do
		field[rowidx] = {};
		local offsetX = state.dataFieldOffsetX;
		for colidx=1,pfColumnCount do
			field[rowidx][colidx] = {};
			field[rowidx][colidx].button = GGC.StandardButton:Create({}, {
				properties = {
					-- NOTE: Frame List Field Button(Frame)
					base = {
						parent = parentFrame,
						wrapperName = "FLF",
						template = 0,
					},
					miscellaneous = {
						hidden = false,
						enabled = true,

						text = "",
					},
					backdrop = GGD.Backdrop.Empty,
					size = {
						width = state.minCellWidth,
						height = state.cellHeight,
					},
					anchor = {
						point = GGE.PointType.TopLeft,
						relativeTo = GGF.INS.GetObjectRef(state.dynamicCentralPhantomFrame),
						relativePoint = GGE.PointType.TopLeft,
						offsetX = offsetX,
						offsetY = state.dataFieldOffsetY - (rowidx - 1)*(state.cellHeight + state.fontStringGap),
					},
					callback = {
						onClick = function()
							GGF.InnerInvoke(in_meta, "SetSelection", rowidx, colidx);
						end,
						-- TODO: дописать реакцию на модификаторы(ctrl, alt)
						onEnter = function()
							GGF.InnerInvoke(in_meta, "cellHighlightCheckSwitch", rowidx, colidx, true);
						end,
						onLeave = function()
							GGF.InnerInvoke(in_meta, "cellHighlightCheckSwitch", rowidx, colidx, false);
						end
					},
				},
			});
			local prButton = GGF.INS.GetObjectRef(field[rowidx][colidx].button);
			field[rowidx][colidx].fontString = GGC.FontString:Create({}, {
				properties = {
					-- NOTE: Frame List Field Font String
					base = {
						parent = prButton,
						wrapperName = "FLF",
					},
					miscellaneous = {
						hidden = false,
						text = "Test: " .. tostring(rowidx) .. "_".. tostring(colidx),
					},
					anchor = {
						point = GGE.PointType.Center,
						relativeTo = prButton,
						relativePoint = GGE.PointType.Center,
					},
					color = GGD.Color.Title,
				},
			});
			field[rowidx][colidx].texture = GGC.StandardTexture:Create({}, {
				properties = {
					-- FrameList Field Highlight Texture
					base = {
						parent = prButton,
						wrapperName = "FLFH",
					},
					miscellaneous = {
						color = GGD.Color.Green,
						hidden = true,
					},
					anchor = {
						point = GGE.PointType.Center,
						relativeTo = prButton,
						relativePoint = GGE.PointType.Center,
					},
				},
			});
			offsetX = offsetX + state.minCellWidth + state.fontStringGap;
		end
	end
	GGF.InnerInvoke(in_meta, "SetCentralFieldDataConglomerationOD", field);
end


-- Создание основных объектов табличного фрейма
function This:createServiceObjects(in_meta)
	-- TODO: починить!!!
	-- TODO: разгрести захламление
	local emptyFrameBackdrop = GGD.Backdrop.Empty;
	-- NOTE: отличие в наличии локера должно быть
	local tableHeaderBackdrop = {
		bgFile = "",
		edgeFile = "",
		edgeSize = 16,
		tileSize = 16,
		insets = {
			left = 0,
			right = 0,
			top = 0,
			bottom = 0,
		},
	};
	local sliderFrameBackdrop = GGD.Backdrop.VerticalSlider;
	local segregateFrameBackdrop = GGD.Backdrop.Segregate;
	-- WARNING: костыль
	GGF.InnerInvoke(in_meta, "SetSelectionColor", GGD.Color.Green);
	local selectionColor = GGF.InnerInvoke(in_meta, "GetSelectionColor");
	local parentFrame = GGF.INS.GetObjectRef(in_meta.instance);
	local config = GGF.InnerInvoke(in_meta, "GetTypeConfig");
	local verticalSliderArrayWidth = GGF.InnerInvoke(in_meta, "GetSliderWidth");
	local horizontalSliderArrayHeight = GGF.InnerInvoke(in_meta, "GetSliderWidth")*GGF.TernExpSingle(config.hsliding, 1, 0);

	local gap = GGF.InnerInvoke(in_meta, "GetFontStringGap");
	-- NOTE: кнопка(29) + два отступа(10х2),
	local spikeHeight = 49;
	local borderWidth = GGF.InnerInvoke(in_meta, "GetBorderWidth");
	local sliderWidth = GGF.InnerInvoke(in_meta, "GetSliderWidth");

	GGF.InnerInvoke(in_meta, "SetStaticPhantomFrameOD", GGC.StandardFrame:Create({}, {
		properties = {
			base = {
				parent = parentFrame,
				wrapperName = "SP",
			},
			size = {
				width = GGF.InnerInvoke(in_meta, "GetWidth"),
				height = GGF.InnerInvoke(in_meta, "GetHeight") - spikeHeight,
			},
			anchor = {
				point = GGE.PointType.Center,
				relativeTo = parentFrame,
				relativePoint = GGE.PointType.Center,
				-- WARNING: тут вроде бы костыль, но хз...
				offsetY = - spikeHeight/2 + 10,
			},
			backdrop = GGD.Backdrop.Empty,
			--backdrop = GGD.Backdrop.Nested,
		},
	}));
	local staticPhantomInstance = GGF.InnerInvoke(in_meta, "GetStaticPhantomFrame");
	local staticPhantom = GGF.INS.GetObjectRef(staticPhantomInstance);
	GGF.InnerInvoke(in_meta, "SetDynamicCentralPhantomFrameOD", GGC.StandardFrame:Create({}, {
		properties = {
			base = {
				parent = parentFrame,
				wrapperName = "DCP",
			},
			size = {
				width = GGF.OuterInvoke(staticPhantomInstance, "GetWidth"),
				height = GGF.OuterInvoke(staticPhantomInstance, "GetHeight"),
			},
			anchor = {
				point = GGE.PointType.Center,
				relativeTo = staticPhantom,
				relativePoint = GGE.PointType.Center,
			},
			backdrop = GGD.Backdrop.Empty,
			--backdrop = GGD.Backdrop.Nested,
		},
	}));
	GGF.InnerInvoke(in_meta, "SetDynamicVerticalPhantomFrameOD", GGC.StandardFrame:Create({}, {
		properties = {
			base = {
				parent = parentFrame,
				wrapperName = "DVP",
			},
			size = {
				width = GGF.OuterInvoke(staticPhantomInstance, "GetWidth"),
				height = GGF.OuterInvoke(staticPhantomInstance, "GetHeight"),
			},
			anchor = {
				point = GGE.PointType.Center,
				relativeTo = staticPhantom,
				relativePoint = GGE.PointType.Center,
			},
			backdrop = GGD.Backdrop.Empty,
			--backdrop = GGD.Backdrop.Nested,
		},
	}));
	GGF.InnerInvoke(in_meta, "SetDynamicHorizontalPhantomFrameOD", GGC.StandardFrame:Create({}, {
		properties = {
			base = {
				parent = parentFrame,
				wrapperName = "DHP",
			},
			size = {
				width = GGF.OuterInvoke(staticPhantomInstance, "GetWidth"),
				height = GGF.OuterInvoke(staticPhantomInstance, "GetHeight"),
			},
			anchor = {
				point = GGE.PointType.Center,
				relativeTo = staticPhantom,
				relativePoint = GGE.PointType.Center,
			},
			backdrop = GGD.Backdrop.Empty,
			--backdrop = GGD.Backdrop.Nested,
		},
	}));

	GGF.InnerInvoke(in_meta, "SetHorizontalHighBorderDelimiterOD", GGC.LineFrame:Create({}, {
		properties = {
			base = {
				parent = parentFrame,
				wrapperName = "HHBD",
			},
			miscellaneous = {
				color = GGF.TableCopy(GGD.Color.Grey),
				hidden = false,
			},
			size = {
				width = borderWidth,
			},
			anchor = {
				relativeTo = staticPhantom,
				relativePoint = GGE.PointType.TopLeft,
			},
			secondAnchor = {
				offsetX = function() return GGF.OuterInvoke(staticPhantomInstance, "GetWidth") end,
			},
		},
	}));
	GGF.InnerInvoke(in_meta, "SetHorizontalLowBorderDelimiterOD", GGC.LineFrame:Create({}, {
		properties = {
			base = {
				parent = parentFrame,
				wrapperName = "HLBD",
			},
			miscellaneous = {
				color = GGF.TableCopy(GGD.Color.Grey),
				hidden = false,
			},
			size = {
				width = borderWidth,
			},
			anchor = {
				relativeTo = staticPhantom,
				relativePoint = GGE.PointType.BottomLeft,
			},
			secondAnchor = {
				offsetX = function() return GGF.OuterInvoke(staticPhantomInstance, "GetWidth") end,
			},
		},
	}));
	GGF.InnerInvoke(in_meta, "SetVerticalLeftBorderDelimiterOD", GGC.LineFrame:Create({}, {
		properties = {
			base = {
				parent = parentFrame,
				wrapperName = "VLBD",
			},
			miscellaneous = {
				color = GGF.TableCopy(GGD.Color.Grey),
				hidden = false,
			},
			size = {
				width = borderWidth,
			},
			anchor = {
				relativeTo = staticPhantom,
				relativePoint = GGE.PointType.TopLeft,
			},
			secondAnchor = {
				offsetY = function() return - GGF.OuterInvoke(staticPhantomInstance, "GetHeight") end,
			},
		},
	}));
	GGF.InnerInvoke(in_meta, "SetVerticalRightBorderDelimiterOD", GGC.LineFrame:Create({}, {
		properties = {
			base = {
				parent = parentFrame,
				wrapperName = "VRBD",
			},
			miscellaneous = {
				color = GGF.TableCopy(GGD.Color.Grey),
				hidden = false,
			},
			size = {
				width = borderWidth,
			},
			anchor = {
				relativeTo = staticPhantom,
				relativePoint = GGE.PointType.TopRight,
			},
			secondAnchor = {
				offsetY = function() return - GGF.OuterInvoke(staticPhantomInstance, "GetHeight") end,
			},
		},
	}));
	GGF.InnerInvoke(in_meta, "SetVerticalHeaderDelimiterOD", GGC.LineFrame:Create({}, {
		properties = {
			base = {
				parent = parentFrame,
				wrapperName = "VHD",
			},
			miscellaneous = {
				color = GGF.TableCopy(GGD.Color.Grey),
				hidden = false,
			},
			size = {
				width = borderWidth,
			},
			anchor = {
				relativeTo = staticPhantom,
				relativePoint = GGE.PointType.TopLeft,
				offsetX = function() return GGF.InnerInvoke(in_meta, "GetDataFieldOffsetX") end,
			},
			secondAnchor = {
				offsetX = function() return GGF.InnerInvoke(in_meta, "GetDataFieldOffsetX") end,
				offsetY = function() return - GGF.OuterInvoke(staticPhantomInstance, "GetHeight") end,
			},
		},
	}));
	GGF.InnerInvoke(in_meta, "SetHorizontalHeaderDelimiterOD", GGC.LineFrame:Create({}, {
		properties = {
			base = {
				parent = parentFrame,
				wrapperName = "HHD",
			},
			miscellaneous = {
				color = GGD.Color.Grey,
				hidden = false,
			},
			size = {
				width = borderWidth,
			},
			anchor = {
				relativeTo = staticPhantom,
				relativePoint = GGE.PointType.TopLeft,
				offsetY = function() return GGF.InnerInvoke(in_meta, "GetDataFieldOffsetY") end,
			},
			secondAnchor = {
				offsetX = function() return GGF.OuterInvoke(staticPhantomInstance, "GetWidth") end,
				offsetY = function() return GGF.InnerInvoke(in_meta, "GetDataFieldOffsetY") end,
			},
		},
	}));
	GGF.InnerInvoke(in_meta, "SetVerticalSliderDelimiterOD", GGC.LineFrame:Create({}, {
		properties = {
			base = {
				parent = parentFrame,
				wrapperName = "VSD",
			},
			miscellaneous = {
				color = GGF.TableCopy(GGD.Color.Grey),
				hidden = false,
			},
			size = {
				width = borderWidth,
			},
			anchor = {
				relativeTo = staticPhantom,
				relativePoint = GGE.PointType.TopRight,
				offsetX = function() return - verticalSliderArrayWidth - 2*gap end,
			},
			secondAnchor = {
				offsetX = function() return - verticalSliderArrayWidth - 2*gap end,
				offsetY = function() return - GGF.OuterInvoke(staticPhantomInstance, "GetHeight") end,
			},
		},
	}));
	GGF.InnerInvoke(in_meta, "SetHorizontalSliderDelimiterOD", GGC.LineFrame:Create({}, {
		properties = {
			base = {
				parent = parentFrame,
				wrapperName = "HSD",
			},
			miscellaneous = {
				color = GGD.Color.Grey,
				hidden = false,
			},
			size = {
				width = borderWidth,
			},
			anchor = {
				relativeTo = staticPhantom,
				relativePoint = GGE.PointType.BottomLeft,
				offsetY = function() return horizontalSliderArrayHeight + 2*gap end,
			},
			secondAnchor = {
				offsetX = function() return GGF.OuterInvoke(staticPhantomInstance, "GetWidth") end,
				offsetY = function() return horizontalSliderArrayHeight + 2*gap end,
			},
		},
	}));

	GGF.InnerInvoke(in_meta, "SetVerticalSliderOD", GGC.PageScrollSlider:Create({}, {
		properties = {
			base = {
				parent = parentFrame,
				--template = "UIPanelScrollBarTemplate",
				wrapperName = "V",
			},
			miscellaneous = {
				--enabled = true,
				hidden = false,
				orientation = GGE.SliderOrientation.Vertical,
				--thumbTexture = "Interface\\Buttons\\UI-SliderBar-Button-Vertical",
			},
			backdrop = sliderFrameBackdrop,
			size = {
				width = sliderWidth,
			},
			anchor = {
				point = GGE.PointType.BottomRight,
				relativeTo = staticPhantom,
				relativePoint = GGE.PointType.BottomRight,
				offsetX = - gap,
				offsetY = sliderWidth + 3*gap,
			},
			callback = {
				onValueChanged = function(...) GGF.InnerInvoke(in_meta, "vSliderValChanged", ...); end,
			},
		},
	}));
	GGF.InnerInvoke(in_meta, "SetHorizontalSliderOD", GGC.PageScrollSlider:Create({}, {
		properties = {
			base = {
				parent = parentFrame,
				--template = "UIPanelScrollBarTemplate",
				wrapperName = "H",
			},
			miscellaneous = {
				--enabled = true,
				hidden = false,
				orientation = GGE.SliderOrientation.Horizontal,
				--thumbTexture = "Interface\\Buttons\\UI-SliderBar-Button-Vertical",
			},
			backdrop = sliderFrameBackdrop,
			size = {
				-- NOTE: это не баг(фича)
				height = sliderWidth,
			},
			anchor = {
				point = GGE.PointType.BottomRight,
				relativeTo = staticPhantom,
				relativePoint = GGE.PointType.BottomRight,
				offsetX = - sliderWidth - 3*gap,
				offsetY = gap,
			},
			callback = {
				onValueChanged = function(...) GGF.InnerInvoke(in_meta, "hSliderValChanged", ...) end,
			},
		},
	}));

	GGF.InnerInvoke(in_meta, "createContentFieldObjects");

	if (config.paging) then
		GGF.InnerInvoke(in_meta, "createPageSelectionTool");
	end
	if (config.settings) then
		GGF.InnerInvoke(in_meta, "createUpdateButton");
		GGF.InnerInvoke(in_meta, "createSettingsTool");
	end
	--GGF.NotNullInvoke(self, self.createPageSelectionTool);	-- CT
	--GGF.NotNullInvoke(self, self.createUpdateButton);	-- CT
	--GGF.NotNullInvoke(self, self.createSettingsTool);	-- CT
end


-- Создание кнопки апдейта данных в таблице
function This:createUpdateButton(in_meta)
	-- TODO: переделать под новый стандарт
	-- TODO: вдруг можно параметры кнопки прокидывать из внешнего объекта
	GGF.InnerInvoke(in_meta, "SetUpdateButton", GGC.StandardButton:Create({}, {
		properties = {
			--Object = CreateFrame("Button", self:GetObjectName() .. "_UT_B", self:GetObject(), "UiPanelButtonTemplate"),
			base = {
				parent = GGF.INS.GetObjectRef(in_instance),
				wrapperName = "UT",
			},
			miscellaneous = {
				hidden = false,
				enabled = true,
				highlighted = false,
				text = "Update Table",
				--tooltipText = self:GetObjectName() .. "_UT_B",
			},
			backdrop = GGD.Backdrop.Empty,
			size = {
				width = 122,
				height = 29,
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = GGF.INS.GetObjectRef(in_instance),
				relativePoint = GGE.PointType.TopLeft,
				offsetX = 10,
				offsetY = - 10,
			},
			callback = {
				onClick = function(...) GGF.InnerInvoke(in_meta, "GetOnUpDateButtonClickCounter")(...) end,
				onEnter = function(...) GGF.InnerInvoke(in_meta, "GetOnButtonEnterCounter")(GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetUpdateButton")), ...) end,
				onLeave = function(...) GGF.InnerInvoke(in_meta, "GetOnButtonLeaveCounter")(GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetUpdateButton")), ...) end,
			},
		},
	}));
end


-- Создание кнопки апдейта данных в таблице
function This:createSettingsTool(in_meta)
	local parentFrame = GGF.INS.GetObjectRef(in_instance);
	GGF.InnerInvoke(in_meta, "SetSettingsButton", GGC.StandardButton:Create({}, {
		properties = {
			--Object = CreateFrame("Button", self:GetObjectName() .. "_TS_B", self:GetObject(), "UiPanelButtonTemplate"),
			base = {
				parent = parentFrame,
				template = "UIPanelButtonTemplate",
				wrapperName = "TS",
			},
			miscellaneous = {
				hidden = false,
				enabled = true,
				highlighted = false,
				text = "Table Settings",
			},
			backdrop = emptyFrameBackdrop,
			size = {
				width = 122,
				height = 29,
			},
			anchor = {
				point = GGE.PointType.Left,
				relativeTo = GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetUpdateButton")),
				relativePoint = GGE.PointType.Right,
				offsetX = 10,
				offsetY = 0,
			},
			callback = {
				onClick = function(...) GGF.InnerInvoke(in_meta, "switchSettingsFrameState") end,
				--onEnter = function(...) self:GetOnButtonEnterCounter()(self:GetSettingsButton():GetStandardButton(), ...) end,
				--onLeave = function(...) self:GetOnButtonLeaveCounter()(self:GetSettingsButton():GetStandardButton(), ...) end,
			},
		},
	}));
	-- WARNING: не менять местами с кнопкой!!!(слетит привязка позиции)
	GGF.InnerInvoke(in_meta, "SetSettingsFrame", GGC.SettingsFrame:Create({}, {
		properties = {
			--Object = CreateFrame("Frame", self:GetObjectName() .. "_S_F", self:GetObject()),
			base = {
				parent = parentFrame,
				wrapperName = "S",
			},
			miscellaneous = {
				hidden = true,
				topLevel = true,
				frameLevelOffset = 2,
				movable = true,
			},
			backdrop = segregateFrameBackdrop,
			size = {
				width = 220,
				height = 350,
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetSettingsButton")),
				relativePoint = GGE.PointType.TopRight,
				offsetX = 0,
				offsetY = 0,
			},
			callback = {
				--onButtonEnter = function(...) self:GetOnButtonEnter(...) end,
				--onButtonLeave = function(...) self:GetOnButtonLeave(...) end,
			},
		},
	}));
end


-- Создание кнопки апдейта данных в таблице
function This:createPageSelectionTool(in_meta)
	local parentFrame = GGF.INS.GetObjectRef(in_instance);
	GGF.InnerInvoke(in_meta, "SetPageSelectorEditBox", GGC.EditBox:Create({}, {
		properties = {
			--object = CreateFrame("EditBox", self:GetObjectName() .. "_PS_EB", self:GetObject(), "InputBoxTemplate"),
			base = {
				parent = parentFrame,
				template = "InputBoxTemplate",
				wrapperName = "PS",
			},
			miscellaneous = {
				hidden = false,
				isNumeric = true,
			},
			backdrop = emptyFrameBackdrop,
			size = {
				width = 120,
				height = 20,
			},
			anchor = {
				point = GGE.PointType.TopRight,
				relativeTo = parentFrame,
				relativePoint = GGE.PointType.TopRight,
				offsetX = - 220,
				offsetY = - 10,
			},
			callback = {
				onEnterPressed = function(in_object)
					-- NOTE: пока что чек на значения будет тут
					local pageNumber = in_object:GetNumber();
					if (pageNumber > 1) or (pageNumber < GGF.InnerInvoke(in_meta, "GetPageCount")) then
						GGF.InnerInvoke(in_meta, "SetCurrentPage", pageNumber);
					end
				end,
			},
		},
	}));

	GGF.InnerInvoke(in_meta, "SetPageSelectorLimitFontString", GGC.FontString:Create({}, {
		properties = {
			-- NOTE: Page Selector Limit(Frame)
			--Object = self:GetObject():CreateFontString(self:GetObjectName() .. "_PSL_FS", "OVERLAY", "GameFontNormal"),
			base = {
				parent = parentFrame,
				wrapperName = "PSL",
			},
			miscellaneous = {
				hidden = false,
				text = "Test psl",
			},
			size = {
				width = minWidth,
				--height = self:GetFontHeight(),
			},
			anchor = {
				point = GGE.PointType.Left,
				relativeTo = GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetPageSelectorEditBox")),
				relativePoint = GGE.PointType.Right,
				offsetX = 0,
				offsetY = 0,
			},
			color = GGD.Color.Title,
		},
	}));
end