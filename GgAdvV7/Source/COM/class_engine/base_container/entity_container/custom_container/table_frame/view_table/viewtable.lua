-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "VT";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	ViewTableSuffix = GGF.DockDomName(GGD.DefineIdentifier, "ViewTableSuffix"),
	ViewTable = GGF.DockDomName(GGD.ClassIdentifier, "ViewTable"),
};

local CTList = {
	ViewTable = GGF.CreateCT(OBJSuffix, "ViewTable", "Frame"),
	TypeConfig = GGF.CreateCT(OBJSuffix, "TypeConfig", "TypeConfig"),
};

GGF.GRegister(DFList.ViewTableSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	TypeConfig = {
		paging = false,
		locking = false,
		hsliding = true,
		settings = false,
	},
}, GGE.RegValType.Default);

GGF.GRegister(DFList.ViewTable, {
	meta = {
		name = "View Table Frame",
		mark = "",	-- авт.
		suffix = GGD.ViewTableSuffix,
	},
});

GGC.ViewTable.objects = {
	frame = {
		table = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.ViewTable,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.ViewTable.Default)) then
					local tableFrame = CreateFrame(
						"Frame",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							in_meta.class:getClassSuffix()
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
                  GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyViewTableOD", tableFrame);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.ViewTable.wrappers = {};

GGC.ViewTable.properties = {};

GGC.ViewTable.elements = {
	runtime = {
		properties = {
			typeConfig = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.TypeConfig,
				fixed = true,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
	},
};

GGF.Mark(GGC.ViewTable);

GGC.ViewTable = GGF.TableSafeExtend(GGC.ViewTable, GGC.TableFrame);

GGF.CreateClassWorkaround(GGC.ViewTable);

local This = GGC.ViewTable;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------
