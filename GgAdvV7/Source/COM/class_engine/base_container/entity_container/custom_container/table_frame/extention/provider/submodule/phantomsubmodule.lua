-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

local OBJSuffix = "PSM";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   PhantomSubModuleSuffix = GGF.DockDomName(GGD.DefineIdentifier, "PhantomSubModuleSuffix"),
   PhantomSubModule = GGF.DockDomName(GGD.ClassIdentifier, "PhantomSubModule"),
};

local CTList = {
   DynamicCentralPhantomFrame = GGF.CreateCT(OBJSuffix, "DynamicCentralPhantomFrame", "Frame"),
   DynamicVerticalPhantomFrame = GGF.CreateCT(OBJSuffix, "DynamicVerticalPhantomFrame", "Frame"),
   DynamicHorizontalPhantomFrame = GGF.CreateCT(OBJSuffix, "DynamicHorizontalPhantomFrame", "Frame"),

   StaticPhantomFrame = GGF.CreateCT(OBJSuffix, "StaticPhantomFrame", "Frame"),
};

GGF.GRegister(DFList.PhantomSubModuleSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
   Frame = 0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.PhantomSubModule, {
   meta = {
      name = "Phantom Sub Module",
      mark = "",  -- авт.
      suffix = GGD.PhantomSubModuleSuffix,
   },
});

GGC.PhantomSubModule.objects = {};

GGC.PhantomSubModule.wrappers = {
   frame = {
      dynamicCentralPhantom = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.DynamicCentralPhantomFrame,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      dynamicVerticalPhantom = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.DynamicVerticalPhantomFrame,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      dynamicHorizontalPhantom = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.DynamicHorizontalPhantomFrame,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      staticPhantom = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.StaticPhantomFrame,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.PhantomSubModule.properties = {};

GGC.PhantomSubModule.elements = {};

GGF.Mark(GGC.PhantomSubModule);

GGC.PhantomSubModule = GGF.TableSafeExtend(GGC.PhantomSubModule, GGC.TableExtentionContainer);

GGF.CreateClassWorkaround(GGC.PhantomSubModule);

local This = GGC.PhantomSubModule;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-- Установка основных объектов табличного фрейма
function This:adjustServiceObjects(in_meta)
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetDynamicCentralPhantomFrame"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetDynamicVerticalPhantomFrame"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetDynamicHorizontalPhantomFrame"), "Adjust");

   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetStaticPhantomFrame"), "Adjust");
end

-- Запуск
function This:launch(in_meta)
   GGF.InnerInvoke(in_meta, "adjustServiceObjects");
end

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

-- Создание основных объектов табличного фрейма
function This:createServiceObjects(in_meta)
   -- TODO: починить!!!
   -- TODO: разгрести захламление
   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");
   local sideCurtainMarkupSubModule = GGF.OuterInvoke(objectProvider, "GetSideCurtainMarkupSubModule");
   local fontStringGap = GGF.OuterInvoke(tableFrame, "GetFontStringGap");

   local config = GGF.OuterInvoke(visionLogicProvider, "GetTypeConfig");
   --local fontStringGap = GGF.OuterInvoke(tableFrame, "GetFontStringGap");

   local emptyFrameBackdrop = GGD.Backdrop.Empty;
   -- NOTE: отличие в наличии локера должно быть
   local tableHeaderBackdrop = {
      bgFile = "",
      edgeFile = "",
      edgeSize = 16,
      tileSize = 16,
      insets = {
         left = 0,
         right = 0,
         top = 0,
         bottom = 0,
      },
   };
   local sliderFrameBackdrop = GGD.Backdrop.VerticalSlider;
   local segregateFrameBackdrop = GGD.Backdrop.Segregate;
   -- WARNING: костыль
   GGF.OuterInvoke(tableFrame, "SetSelectionColor", GGD.Color.Green);
   local selectionColor = GGF.OuterInvoke(tableFrame, "GetSelectionColor");
   local parentFrame = GGF.INS.GetObjectRef(tableFrame);
   local verticalSliderArrayWidth = GGF.OuterInvoke(tableFrame, "GetSliderWidth");
   local horizontalSliderArrayHeight = GGF.OuterInvoke(tableFrame, "GetSliderWidth")*GGF.TernExpSingle(config.hsliding, 1, 0);

   -- NOTE: кнопка(29) + два отступа(10х2),
   local spikeHeight = 49;
   -- NOTE: зазор между фантомом и рамкой, чтобы поместились линии
   local spikeGap = 10;
   local borderWidth = GGF.OuterInvoke(tableFrame, "GetBorderWidth");
   local sliderWidth = GGF.OuterInvoke(tableFrame, "GetSliderWidth");

   GGF.InnerInvoke(in_meta, "SetStaticPhantomFrameOD", GGC.StandardFrame:Create({}, {
      properties = {
         base = {
            parent = function() return parentFrame end,
            wrapperName = "SP",
         },
         size = {
            width = GGF.OuterInvoke(tableFrame, "GetWidth") - 2*spikeGap,
            height = GGF.OuterInvoke(tableFrame, "GetHeight") - spikeHeight,
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = function() return parentFrame end,
            relativePoint = GGE.PointType.Center,
            -- WARNING: тут вроде бы костыль, но хз...
            offsetY = - spikeHeight/2 + 10,
         },
         backdrop = GGD.Backdrop.Empty,
         --backdrop = GGD.Backdrop.Nested,
      },
   }));
   local staticPhantomInstance = GGF.InnerInvoke(in_meta, "GetStaticPhantomFrame");
   local staticPhantomObject = GGF.INS.GetObjectRef(staticPhantomInstance);
   print("phantom object:", staticPhantomObject, parentFrame);
   GGF.InnerInvoke(in_meta, "SetDynamicCentralPhantomFrameOD", GGC.StandardFrame:Create({}, {
      properties = {
         base = {
            parent = function() return parentFrame end,
            wrapperName = "DCP",
         },
         size = {
            width = GGF.OuterInvoke(staticPhantomInstance, "GetWidth"),
            height = GGF.OuterInvoke(staticPhantomInstance, "GetHeight"),
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = function() return staticPhantomObject end,
            relativePoint = GGE.PointType.Center,
         },
         backdrop = GGD.Backdrop.Empty,
         --backdrop = GGD.Backdrop.Nested,
      },
   }));
   GGF.InnerInvoke(in_meta, "SetDynamicVerticalPhantomFrameOD", GGC.StandardFrame:Create({}, {
      properties = {
         base = {
            parent = function() return parentFrame end,
            wrapperName = "DVP",
         },
         size = {
            width = GGF.OuterInvoke(staticPhantomInstance, "GetWidth"),
            height = GGF.OuterInvoke(staticPhantomInstance, "GetHeight"),
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = function() return staticPhantomObject end,
            relativePoint = GGE.PointType.Center,
         },
         backdrop = GGD.Backdrop.Empty,
         --backdrop = GGD.Backdrop.Nested,
      },
   }));
   GGF.InnerInvoke(in_meta, "SetDynamicHorizontalPhantomFrameOD", GGC.StandardFrame:Create({}, {
      properties = {
         base = {
            parent = function() return parentFrame end,
            wrapperName = "DHP",
         },
         size = {
            width = GGF.OuterInvoke(staticPhantomInstance, "GetWidth"),
            height = GGF.OuterInvoke(staticPhantomInstance, "GetHeight"),
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = function() return staticPhantomObject end,
            relativePoint = GGE.PointType.Center,
         },
         backdrop = GGD.Backdrop.Empty,
         --backdrop = GGD.Backdrop.Nested,
      },
   }));
end

-- Пересчет геометрии, связанной с прототипом(границы. слайдер), но не связанной с полем видимости
function This:recountPrototypeRelativeGeometry(in_meta)
   --local state = GGF.InnerInvoke(in_meta, "getCurrentState");
   local objectProvider = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local tableFrame = GGF.OuterInvoke(objectProvider, "GetParentInstance");
   local fontStringGap = GGF.OuterInvoke(tableFrame, "GetFontStringGap");
   local minCellWidth = GGF.OuterInvoke(tableFrame, "GetMinCellWidth");
   local sliderWidth = GGF.OuterInvoke(tableFrame, "GetSliderWidth");
   local frameWidth = GGF.OuterInvoke(tableFrame, "GetWidth");
   local borderWidth = GGF.OuterInvoke(tableFrame, "GetBorderWidth");
   local prototype = GGF.OuterInvoke(tableFrame, "GetPrototype");

   local visionLogicProvider = GGF.OuterInvoke(tableFrame, "GetVisionLogicProvider");
   local config = GGF.OuterInvoke(visionLogicProvider, "GetTypeConfig");

   local sliderArrayWidth = sliderWidth;
   local summWd = 3*borderWidth + 2*fontStringGap + sliderArrayWidth;   -- NOTE: два неучтенных отступа - перед хедером и перед полем
   local colCount = 0;
   for index,field in pairs(prototype.columns) do
      local cellWidth = GGF.TernExpSingle(field.width > minCellWidth, field.width, minCellWidth);
      if (summWd + cellWidth + fontStringGap < frameWidth) then
         summWd = summWd + cellWidth + fontStringGap;
         colCount = colCount + 1;
      else
         if (not config.hsliding) then
            -- TODO: нормально пробросить ошибку в модуль
            --ATLASSERT(false);
         end
      end
   end

   local fieldWidth = GGF.TernExpSingle(summWd > frameWidth, frameWidth, summWd);
   GGF.OuterInvoke(visionLogicProvider, "SetVisibleObjectRegionColumn", colCount);
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetStaticPhantomFrame"), "SetWidth", fieldWidth);
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetDynamicCentralPhantomFrame"), "SetWidth", fieldWidth);
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetDynamicVerticalPhantomFrame"), "SetWidth", fieldWidth);
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetDynamicHorizontalPhantomFrame"), "SetWidth", fieldWidth);

   --GGF.InnerInvoke(in_meta, "recountServiceGeometry");
   GGF.OuterInvoke(GGF.OuterInvoke(objectProvider, "GetSideCurtainMarkupSubModule"), "RecountSliderGeometry");
end