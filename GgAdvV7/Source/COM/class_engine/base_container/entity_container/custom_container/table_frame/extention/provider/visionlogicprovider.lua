-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "VLP";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   VisionLogicProviderSuffix = GGF.DockDomName(GGD.DefineIdentifier, "VisionLogicProviderSuffix"),
   VisionLogicProvider = GGF.DockDomName(GGD.ClassIdentifier, "VisionLogicProvider"),
};

local CTList = {
   -- elements
   TypeConfig = GGF.CreateCT(OBJSuffix, "TypeConfig", "TypeConfig"),
   IndexMap = GGF.CreateCT(OBJSuffix, "IndexMap", "IndexMap"),

   LockedRows = GGF.CreateCT(OBJSuffix, "LockedRows", "LockedRows"),
   LockedColumns = GGF.CreateCT(OBJSuffix, "LockedColumns", "LockedColumns"),

   VisibilityBorderRow = GGF.CreateCT(OBJSuffix, "VisibilityBorderRow", "VisibilityBorderRow"),
   VisibilityBorderColumn = GGF.CreateCT(OBJSuffix, "VisibilityBorderColumn", "VisibilityBorderColumn"),

   SelectedRowIdx = GGF.CreateCT(OBJSuffix, "SelectedRowIdx", "SelectedRowIdx"),
   SelectedColumnIdx = GGF.CreateCT(OBJSuffix, "SelectedColumnIdx", "SelectedColumnIdx"),

   DataFieldElementOffsetX = GGF.CreateCT(OBJSuffix, "DataFieldElementOffsetX", "DataFieldElementOffsetX"),
   DataFieldElementOffsetY = GGF.CreateCT(OBJSuffix, "DataFieldElementOffsetY", "DataFieldElementOffsetY"),

   VisibleMovingFieldRowCount = GGF.CreateCT(OBJSuffix, "VisibleMovingFieldRowCount", "VisibleMovingFieldRowCount"),
   VisibleMovingFieldColumnCount = GGF.CreateCT(OBJSuffix, "VisibleMovingFieldColumnCount", "VisibleMovingFieldColumnCount"),
   VisibleMovingFieldWidth = GGF.CreateCT(OBJSuffix, "VisibleMovingFieldWidth", "VisibleMovingFieldWidth"),
   VisibleMovingFieldHeight = GGF.CreateCT(OBJSuffix, "VisibleMovingFieldHeight", "VisibleMovingFieldHeight"),
   VisibleMovingFieldOffsetX = GGF.CreateCT(OBJSuffix, "VisibleMovingFieldOffsetX", "VisibleMovingFieldOffsetX"),
   VisibleMovingFieldOffsetY = GGF.CreateCT(OBJSuffix, "VisibleMovingFieldOffsetY", "VisibleMovingFieldOffsetY"),

   VisibleObjectRegionRow = GGF.CreateCT(OBJSuffix, "VisibleObjectRegionRow", "VisibleObjectRegionRow"),
   VisibleObjectRegionColumn = GGF.CreateCT(OBJSuffix, "VisibleObjectRegionColumn", "VisibleObjectRegionColumn"),

   PreallocatedFieldRowCount = GGF.CreateCT(OBJSuffix, "PreallocatedFieldRowCount", "PreallocatedFieldRowCount"),
   PreallocatedFieldColumnCount = GGF.CreateCT(OBJSuffix, "PreallocatedFieldColumnCount", "PreallocatedFieldColumnCount"),

   RowIndent = GGF.CreateCT(OBJSuffix, "RowIndent", "RowIndent"),
   ColumnIndent = GGF.CreateCT(OBJSuffix, "ColumnIndent", "ColumnIndent"),

   DataRowCount = GGF.CreateCT(OBJSuffix, "DataRowCount", "DataRowCount"),
   DataColumnCount = GGF.CreateCT(OBJSuffix, "DataColumnCount", "DataColumnCount"),

   VerticalSliderValue = GGF.CreateCT(OBJSuffix, "VerticalSliderValue", "VerticalSliderValue");
   HorizontalSliderValue = GGF.CreateCT(OBJSuffix, "HorizontalSliderValue", "HorizontalSliderValue");

   CurrentPage = GGF.CreateCT(OBJSuffix, "CurrentPage", "CurrentPage"),
   PageCount = GGF.CreateCT(OBJSuffix, "PageCount", "PageCount"),
};

GGF.GRegister(DFList.VisionLogicProviderSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
   TypeConfig = {
      paging = false,
      locking = false,
      hsliding = false,
      settings = false,
   },
   IndexMap = {},
   LockedRows = {},
   LockedColumns = {},
   VisibilityBorderRow = 0,
   VisibilityBorderColumn = 0,
   SelectedRowIdx = 0,
   SelectedColumnIdx = 0,
   DataFieldElementOffsetX = 0,
   DataFieldElementOffsetY = 0,
   VisibleMovingFieldRowCount = 0,
   VisibleMovingFieldColumnCount = 0,
   VisibleMovingFieldWidth = 0,
   VisibleMovingFieldHeight = 0,
   VisibleMovingFieldOffsetX = 0,
   VisibleMovingFieldOffsetY = 0,
   VisibleObjectRegionRow = 0,
   VisibleObjectRegionColumn = 0,
   PreallocatedFieldRowCount = 0,
   PreallocatedFieldColumnCount = 0,
   RowIndent = 0,
   ColumnIndent = 0,
   DataRowCount = 0,
   DataColumnCount = 0,
   VerticalSliderValue = 0,
   HorizontalSliderValue = 0,
   CurrentPage = 1,
   PageCount = 1,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.VisionLogicProvider, {
   meta = {
      name = "Data Provider",
      mark = "",  -- авт.
      suffix = GGD.VisionLogicProviderSuffix,
   },
});

GGC.VisionLogicProvider.objects = {};

GGC.VisionLogicProvider.wrappers = {};

GGC.VisionLogicProvider.properties = {};

GGC.VisionLogicProvider.elements = {
   runtime = {
      properties = {
         typeConfig = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.TypeConfig,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         -- NOTE: порядок вывода(выхлоп сортировки)
         indexMap = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.IndexMap,
            fixed = true,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
      },
      locked = {
         rows = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.LockedRows,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         columns = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.LockedColumns,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
      },
      visibilityBorder = {
         rows = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.VisibilityBorderRow,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         columns = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.VisibilityBorderColumn,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
      },
      selected = {
         -- NOTE: тут лежат индексы ячеек поля(centralField)
         rowIdx = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.SelectedRowIdx,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         columnIdx = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.SelectedColumnIdx,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
      },
      geometry = {
         dataField = {
            -- NOTE: смещения элементов относительно окна
            elementOffsetX = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.DataFieldElementOffsetX,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            elementOffsetY = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.DataFieldElementOffsetY,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
         },
         -- NOTE: поле не учитывает фикседы. т.е. только свободка
         visibleMovingFieldSize = {
            rowCount = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.VisibleMovingFieldRowCount,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            colCount = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.VisibleMovingFieldColumnCount,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            fieldWidth = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.VisibleMovingFieldWidth,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            fieldHeight = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.VisibleMovingFieldHeight,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            offsetX = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.VisibleMovingFieldOffsetX,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            offsetY = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.VisibleMovingFieldOffsetY,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
         },
         -- NOTE: 4 индекса для пресозданных объектов
         visibleObjectRegion = {
            row = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.VisibleObjectRegionRow,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            column = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.VisibleObjectRegionColumn,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
         },
         -- NOTE: метаданные конгломераций объектов
         preallocated = {
            rows = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.PreallocatedFieldRowCount,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            columns = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.PreallocatedFieldColumnCount,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
         },
         data = {
            rowIndent = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.RowIndent,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            colIndent = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.ColumnIndent,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
         }
      },
      dataCount = {
         rows = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.DataRowCount,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         columns = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.DataColumnCount,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
      },
      sliderValues = {
         vertical = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.VerticalSliderValue,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         horizontal = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.HorizontalSliderValue,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
      },
      paging = {
         -- WARNING: не ставить больше максимальной пока чека нет
         -- TODO: приделать чек на оверсайз
         currentPage = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.CurrentPage,
            fixed = false,

            -- NOTE: Нет такой функции
            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               --GGF.InnerInvoke(in_meta, "updateFieldOfView");
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         -- WARNING: автоматическое заполнение
         pageCount = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.PageCount,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               -- TODO: починить
               --in_wrapper:GetPageSelectorLimitFontString():SetText(in_wrapper:GetPageCount());
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
      },
   },
};

GGF.Mark(GGC.VisionLogicProvider);

GGC.VisionLogicProvider = GGF.TableSafeExtend(GGC.VisionLogicProvider, GGC.TableProviderContainer);

GGF.CreateClassWorkaround(GGC.VisionLogicProvider);

local This = GGC.VisionLogicProvider;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-- Запуск
function This:launch(in_meta)
end

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

function This:CountHHeaderOffsetX(in_meta)
   return GGF.InnerInvoke(in_meta, "CountDataFieldOffsetX");
end

function This:CountHHeaderOffsetY(in_meta)
   local tableFrame = GGF.InnerInvoke(in_meta, "GetParentInstance");
   return - GGF.OuterInvoke(tableFrame, "GetFontStringGap")
         - GGF.OuterInvoke(tableFrame, "GetBorderWidth");
end

function This:CountHHeaderHeight(in_meta)
   local tableFrame = GGF.InnerInvoke(in_meta, "GetParentInstance");
   return GGF.OuterInvoke(tableFrame, "GetCellHeight");
end

function This:CountVHeaderOffsetX(in_meta)
   local tableFrame = GGF.InnerInvoke(in_meta, "GetParentInstance");
   return GGF.OuterInvoke(tableFrame, "GetFontStringGap")
         + GGF.OuterInvoke(tableFrame, "GetBorderWidth");
end

function This:CountVHeaderOffsetY(in_meta)
   return GGF.InnerInvoke(in_meta, "CountDataFieldOffsetY");
end

function This:CountVHeaderWidth(in_meta)
   local tableFrame = GGF.InnerInvoke(in_meta, "GetParentInstance");
   return GGF.TernExpSingle(
      GGF.OuterInvoke(tableFrame, "GetPrototype").vHeader.width > GGF.OuterInvoke(tableFrame, "GetMinCellWidth"),
      GGF.OuterInvoke(tableFrame, "GetPrototype").vHeader.width,
      GGF.OuterInvoke(tableFrame, "GetMinCellWidth")
   );
end

function This:CountVHeaderHeight(in_meta)
   local tableFrame = GGF.InnerInvoke(in_meta, "GetParentInstance");
   return GGF.OuterInvoke(tableFrame, "GetCellHeight");
end

function This:CountDataFieldOffsetX(in_meta)
   local tableFrame = GGF.InnerInvoke(in_meta, "GetParentInstance");
   return GGF.InnerInvoke(in_meta, "CountVHeaderOffsetX")
      + 2*GGF.OuterInvoke(tableFrame, "GetFontStringGap")
      + GGF.OuterInvoke(tableFrame, "GetBorderWidth")
      + GGF.InnerInvoke(in_meta, "CountVHeaderWidth");
end

function This:CountDataFieldOffsetY(in_meta)
   local tableFrame = GGF.InnerInvoke(in_meta, "GetParentInstance");
   return GGF.InnerInvoke(in_meta, "CountHHeaderOffsetY")
      - 2*GGF.OuterInvoke(tableFrame, "GetFontStringGap")
      - GGF.OuterInvoke(tableFrame, "GetBorderWidth")
      - GGF.OuterInvoke(tableFrame, "GetCellHeight");
end

function This:CountDataFieldWidth(in_meta)
   local tableFrame = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local objectProvider = GGF.OuterInvoke(tableFrame, "GetObjectProvider");
   if (GGF.INS.GetObjectRef(objectProvider) == nil) then
      return 0;
   end
   local phantomSubModule = GGF.OuterInvoke(objectProvider, "GetPhantomSubModule");
   local staticPhantomFrame = GGF.OuterInvoke(phantomSubModule, "GetStaticPhantomFrame")
   return GGF.OuterInvoke(staticPhantomFrame, "GetWidth")
      - 6*GGF.OuterInvoke(tableFrame, "GetFontStringGap")
      - 4*GGF.OuterInvoke(tableFrame, "GetBorderWidth")
      - GGF.InnerInvoke(in_meta, "CountVHeaderWidth");
end

function This:CountDataFieldHeight(in_meta)
   local tableFrame = GGF.InnerInvoke(in_meta, "GetParentInstance");
   local objectProvider = GGF.OuterInvoke(tableFrame, "GetObjectProvider");
   --print("obj prov:", objectProvider, tableFrame, GGF.OuterInvoke(tableFrame, "GetParentName"), This);
   if (GGF.INS.GetObjectRef(objectProvider) == nil) then
      return 0;
   end
   --GGF.PrintMetaData(objectProvider);
   local phantomSubModule = GGF.OuterInvoke(objectProvider, "GetPhantomSubModule");
   local staticPhantomFrame = GGF.OuterInvoke(phantomSubModule, "GetStaticPhantomFrame")
   return GGF.OuterInvoke(staticPhantomFrame, "GetHeight")
      - 6*GGF.OuterInvoke(tableFrame, "GetFontStringGap")
      - 4*GGF.OuterInvoke(tableFrame, "GetBorderWidth")
      - GGF.OuterInvoke(tableFrame, "GetCellHeight");
end

function This:CountVisibleDataFieldWidth(in_meta)
   ATLASSERT(false);
   return 10;
end

function This:CountVisibleDataFieldHeight(in_meta)
   ATLASSERT(false);
   return 10;
end