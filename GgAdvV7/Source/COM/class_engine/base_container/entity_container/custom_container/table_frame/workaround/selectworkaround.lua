local This = GGC.TableFrame;

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Установка выделенной линии/клетки по индексу
function This:SetSelection(in_meta, in_rowIdx, in_colIdx)
	ATLASSERT(in_rowIdx == 0 or in_rowIdx == nil);
	local rowIdx, colIdx = 0, 0;
	local selectionType = GGF.InnerInvoke(in_meta, "GetSelectionType");
	local selectionColor = GGF.InnerInvoke(in_meta, "GetSelectionColor");
	local field = GGF.InnerInvoke(in_meta, "GetCentralFieldDataConglomeration");

	local selectedRow = GGF.InnerInvoke(in_meta, "GetSelectedRowIdx");
	GGF.InnerInvoke(in_meta, "SetSelectedRowIdx", 0);

	ATLASSERT(selectionType == GGE.SelectionType.Row or selectionType == GGE.SelectionType.None)
	if (selectionType == GGE.SelectionType.Row) then
		rowIdx = in_rowIdx;
		-- NOTE: сброс текущего выделения(энивей)
		-- TODO: переделать, когда другие варианты выделения появятся
		if (selectedRow ~= 0) then
			for colidx=1,#field[selectedRow] do
				GGF.InnerInvoke(in_meta, "cellHighlightCheckSwitch", selectedRow, colidx, false);
			end
		end
		for colidx=1,#field[rowIdx] do
			GGF.InnerInvoke(in_meta, "cellHighlightCheckSwitch", rowIdx, colidx, true);
		end
	elseif (selectionType == GGE.SelectionType.Column) then
		colIdx = in_colIdx;
	elseif (selectionType == GGE.SelectionType.Element) then
		rowIdx = in_rowIdx;
		colIdx = in_colIdx;
		GGF.InnerInvoke(in_meta, "cellHighlightCheckSwitch", rowIdx, colIdx, true);
	elseif (selectionType == GGE.SelectionType.None) then
		-- NOTE: ничего не делаем
		-- NOTE: возможно понадобится сброс текущего выделения
	end
	GGF.InnerInvoke(in_meta, "SetSelectedRowIdx", rowIdx);
	if (colIdx) then
		GGF.InnerInvoke(in_meta, "SetSelectedColumnIdx", colIdx);
	end
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-- Взятие индекса выделенной линии
function This:GetSelectionMeta(in_meta)
	local rowQuantitativeIndent, columnQuantitativeIndent = GGF.InnerInvoke(in_meta, "countQuantitativeIndent");
	local fieldRowIdx, fieldColIdx = GGF.InnerInvoke(in_meta, "GetSelectedRowIdx"), GGF.InnerInvoke(in_meta, "GetSelectedColumnIdx");
	local dataRowIdx, dataColIdx = fieldRowIdx - rowQuantitativeIndent, fieldColIdx - columnQuantitativeIndent;
	-- NOTE: сначала относительные, потом абсолютные(грубо говоря idx и realidx)
	return fieldRowIdx, fieldColIdx, dataRowIdx, dataColIdx;
end


-- Взятие данных выделенной области
function This:GetSelectionData(in_meta)
	local fieldRowIdx, fieldColIdx = GGF.InnerInvoke(in_meta, "GetSelectedRowIdx"), GGF.InnerInvoke(in_meta, "GetSelectedColumnIdx");
	if (fieldRowIdx == 0) then
		-- NOTE: выбран столбец
		return nil;
	elseif (fieldColIdx == 0) then
		-- NOTE: выбрана строка
		return GGF.TableCopy({});
	elseif (fieldRowIdx + fieldColIdx == 0) then
		-- NOTE: селекта нет
		return nil;
	end
end

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Сброс выделения
function This:ClearSelection(in_meta)
	local field = GGF.InnerInvoke(in_meta, "GetCentralFieldDataConglomeration");
	for rowldx=1,#field do
		for coildx=1,#field[in_rowidx] do
			GGF.OuterInvoke(field[in_rowidx][in_colidx].texture, "SetHidden", true);
		end
	end
	GGF.InnerInvoke(in_meta, "SetSelectedRowIdx", 0);
	GGF.InnerInvoke(in_meta, "SetSelectedColumnIdx", 0);
end


-- Расчет смещений данных относительно поля
function This:countQuantitativeIndent(in_meta)
	local currentPage = GGF.InnerInvoke(in_meta, "GetCurrentPage");
	local pageLineIndent = (currentPage - 1)*GGF.InnerInvoke(in_meta, "GetPageCapacity");
	--print(GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetVerticalSlider"), "GetValue"), GGF.OuterInvoke(GGF.OuterInvoke(in_instance, "GetHorizontalSlider"), "GetValue"));
	return GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetVerticalSlider"),
		"GetValue"
	) + pageLineIndent,
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetHorizontalSlider"),
		"GetValue"
	),
	pageLineIndent;
end


-- Установка подсветки с чеком на наличие в выбранных полях(selected row/col)
function This:cellHighlightCheckSwitch(in_meta, in_rowIdx, in_colIdx, in_state)
   local selectedRowIdx, selectedColIdx = GGF.InnerInvoke(in_meta, "GetSelectedRowIdx"), GGF.InnerInvoke(in_meta, "GetSelectedColumnIdx");
   local selectionType = GGF.InnerInvoke(in_meta, "GetSelectionType");
   local field = GGF.InnerInvoke(in_meta, "GetCentralFieldDataConglomeration");

   if (selectionType == GGE.SelectionType.Row) then
      GGF.OuterInvoke(field[in_rowIdx][in_colIdx].texture, "SetHidden", GGF.TernExpSingle(in_rowIdx == selectedRowIdx, false, not in_state));
   elseif (selectionType == GGE.SelectionType.Column) then
      GGF.OuterInvoke(field[in_rowIdx][in_colIdx].texture, "SetHidden", GGF.TernExpSingle(in_columnIdx == selectedColIdx, false, not in_state));
   elseif (selectionType == GGE.SelectionType.Element) then
      GGF.OuterInvoke(field[in_rowIdx][in_colIdx].texture, "SetHidden", GGF.TernExpSingle((in_rowIdx == selectedRowIdx and in_columnIdx == selectedColIdx), false, not in_state));
   elseif (selectionType == GGE.SelectionType.None) then
      print("cell highlight:", GGF.INS.GetObjectSuffix(GGF.INS.GetObjectRef(field[in_rowIdx][in_colIdx].texture)));
      GGF.OuterInvoke(field[in_rowIdx][in_colIdx].texture, "SetHidden", not in_state);
   end
end