local This = GGC.TableFrame;


-- Корректор коллизий поля данных с границами
function This:collisionCorrector(in_meta)
	local state = GGF.InnerInvoke(in_meta, "getCurrentState");

	local lowBorderOffsetX = state.visibleMovingFieldOffsetX;
	local highBorderOffsetX = state.visibleMovingFieldOffsetX + state.visibleMovingFieldWidth;
	local lowBorderOffsetY = state.visibleMovingFieldOffsetY;
	local highBorderOffsetY = state.visibleMovingFieldOffsetY + state.visibleMovingFieldHeight;
	local vHeader = GGF.InnerInvoke(in_meta, "GetVerticalHeaderDataConglomerationOD");

	if (GGF.OuterInvoke(vHeader[1].button, "GetOffsetY") < lowBorderOffsetY) then
		for rowidx=2,state.preallocatedFieldRowCount do
			GGF.OuterInvoke(state.vHeader[rowidx - 1].button, "SetOffsetY", GGF.OuterInvoke(state.vHeader[rowidx].button, "GetOffsetY"));
		end
		for colidx=1,state.preallocatedFieldColumnCount do
			for rowidx=2,state.preallocatedFieldRowCount do
				GGF.OuterInvoke(state.centralField[rowidx - 1][colidx].button, "SetOffsetY", GGF.OuterInvoke(state.centralField[rowidx][colidx].button, "GetOffsetY"))
			end
		end
		GGF.InnerInvoke(in_meta, "SetRowIndent", GGF.InnerInvoke(in_meta, "GetRowIndent") + 1);
	end

	if (GGF.OuterInvoke(vHeader[1].button, "GetOffsetX") < lowBorderOffsetX) then
		for colidx=2,state.preallocatedFieldColumnCount do
			GGF.OuterInvoke(state.hHeader[colidx - 1].button, "SetOffsetX", GGF.OuterInvoke(state.hHeader[colidx].button, "GetOffsetX"));
		end
		for rowidx=1,state.preallocatedFieldRowCount do
			for colidx=2,state.preallocatedFieldColumnCount do
				GGF.OuterInvoke(state.centralField[rowidx][colidx - 1].button, "SetOffsetY", GGF.OtuerInvoke(state.centralField[rowidx][colidx].button, "GetOffsetY"));
			end
		end
		GGF.InnerInvoke(in_meta, "SetColIndent", GGF.InnerInvoke(in_meta, "GetColIndent") + 1);
	end
end


-- 
function This:recountServiceGeometry(in_meta)
	local state = GGF.InnerInvoke(in_meta, "getCurrentState");

	GGF.OuterInvoke(state.hSlider, "SetWidth", state.staticPhantomWidth - state.sliderWidth - state.vHeaderWidth - 6*state.fontStringGap - 4*state.borderWidth);
	GGF.OuterInvoke(state.hSlider, "SetHidden", not state.config.hsliding);

	GGF.OuterInvoke(state.vSlider, "SetHeight", state.staticPhantomHeight - state.sliderWidth - state.hHeaderHeight - 6*state.fontStringGap - 4*state.borderWidth);
	--GGF.OuterInvoke(state.vSlider, "SetOffsetX", - state.contentOffsetX);
	--GGF.OuterInvoke(state.vSlider, "SetOffsetY", spikedSliderOffsetY(state.vSlider, state.phantomOffsetY));


	local horizontalHighBorderDelimiter = GGF.InnerInvoke(in_meta, "GetHorizontalHighBorderDelimiter");
	GGF.OuterInvoke(horizontalHighBorderDelimiter, "AdjustAnchor");

	local horizontalLowBorderDelimiter = GGF.InnerInvoke(in_meta, "GetHorizontalLowBorderDelimiter");
	GGF.OuterInvoke(horizontalLowBorderDelimiter, "AdjustAnchor");

	local verticalLeftBorderDelimiter = GGF.InnerInvoke(in_meta, "GetVerticalLeftBorderDelimiter");
	GGF.OuterInvoke(verticalLeftBorderDelimiter, "AdjustAnchor");

	local verticalRightBorderDelimiter = GGF.InnerInvoke(in_meta, "GetVerticalRightBorderDelimiter");
	GGF.OuterInvoke(verticalRightBorderDelimiter, "AdjustAnchor");

	local verticalHeaderDelimiter = GGF.InnerInvoke(in_meta, "GetVerticalHeaderDelimiter");
	GGF.OuterInvoke(verticalHeaderDelimiter, "AdjustAnchor");

	local verticalSliderDelimiter = GGF.InnerInvoke(in_meta, "GetVerticalSliderDelimiter");
	GGF.OuterInvoke(verticalSliderDelimiter, "AdjustAnchor");

	local horizontalHeaderDelimiter = GGF.InnerInvoke(in_meta, "GetHorizontalHeaderDelimiter");
	GGF.OuterInvoke(horizontalHeaderDelimiter, "AdjustAnchor");

	local horizontalSliderDelimiter = GGF.InnerInvoke(in_meta, "GetHorizontalSliderDelimiter");
	GGF.OuterInvoke(horizontalSliderDelimiter, "AdjustAnchor");
end


-- Пересчет геометрии, связанной с прототипом(границы. слайдер), но не связанной с полем видимости
function This:recountPrototypeRelativeGeometry(in_meta)
	local state = GGF.InnerInvoke(in_meta, "getCurrentState");

	local sliderArrayWidth = state.sliderWidth;
	local summWd = 3*state.borderWidth + 2*state.fontStringGap + sliderArrayWidth;	-- NOTE: два неучтенных отступа - перед хедером и перед полем
	local colCount = 0;
	for index,field in pairs(state.prototype.columns) do
		local cellWidth = GGF.TernExpSingle(field.width > state.minCellWidth, field.width, state.minCellWidth);
		if (summWd + cellWidth + state.fontStringGap < state.frameWidth) then
			summWd = summWd + cellWidth + state.fontStringGap;
			colCount = colCount + 1;
		else
			if (not state.config.hsliding) then
				-- TODO: нормально пробросить ошибку в модуль
				--ATLASSERT(false);
			end
		end
	end

	local fieldWidth = GGF.TernExpSingle(summWd > state.frameWidth, state.frameWidth, summWd);
	GGF.InnerInvoke(in_meta, "SetVisibleObjectRegionColumn", colCount);
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetStaticPhantomFrame"), "SetWidth", fieldWidth);
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetDynamicCentralPhantomFrame"), "SetWidth", fieldWidth);
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetDynamicVerticalPhantomFrame"), "SetWidth", fieldWidth);
	GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetDynamicHorizontalPhantomFrame"), "SetWidth", fieldWidth);

	GGF.InnerInvoke(in_meta, "recountServiceGeometry");
end


-- 
function This:countCellText(in_meta, in_rowidx, in_colidx)
	local state = GGF.InnerInvoke(in_meta, "getCurrentState");
	if (#state.idxMap > in_rowidx + state.rowIndent) then
		if (#state.dataSet.fields[state.idxMap[in_rowidx + state.rowIndent]] > in_colidx + state.columnIndent) then
			return state.dataSet.fields[state.idxMap[in_rowidx + state.rowIndent]][in_colidx + state.columnIndent].text;
		end
	end
	return "";
end


-- Пересчет слайдеров
function This:recountSliderValues(in_meta)
	local state = GGF.InnerInvoke(in_meta, "getCurrentState");

	local rowCount = #state.dataSet.fields;
	local requestedHeight = state.fontStringGap + (state.cellHeight + state.fontStringGap)*GGF.TernExpSingle(state.config.paging, state.pageCapacity, rowCount);

	-- NOTE: Пересчитываем ширину поля с данными(за вычетом фиксированных столбцов)
	local requestedViewFieldWidth = state.fontStringGap;
	for colIdx,colMeta in ipairs(state.prototype.columns) do
		requestedViewFieldWidth = requestedViewFieldWidth + colMeta.width + state.fontStringGap;
	end

	GGF.OuterInvoke(state.vSlider, "SetValues", {
		min = GGF.OuterInvoke(state.vSlider, "GetValuesMin"),
		max = GGF.TernExpSingle(requestedHeight - state.visibleMovingFieldHeight > 0, requestedHeight - state.visibleMovingFieldHeight, 0),
	});
	-- TODO: потом убрать(когда страницы заработают)
	GGF.OuterInvoke(state.vSlider, "SetValuesStep", 10);--GGF.OuterInvoke(state.vSlider, "GetValuesStep"));
	--GGF.OuterInvoke(state.vSlider, "SetStepsPerPage", fieldRowCount);--GGF.TernExpSingle(state.onPageData > fieldRowCount, onPageData - fieldRowCount, 0));
	if (state.config.hsliding ~= false) then
		GGF.OuterInvoke(state.hSlider, "SetValues", {
			min = GGF.OuterInvoke(state.hSlider, "GetValuesMin"),
			max = GGF.TernExpSingle(requestedViewFieldWidth - state.visibleMovingFieldWidth > 0, requestedViewFieldWidth - state.visibleMovingFieldWidth, 0),
		});
		--GGF.OuterInvoke(state.hSlider, "SetValuesStep", 1);--GGF.OuterInvoke(state.hSlider, "GetValuesStep"));
		--GGF.OuterInvoke(state.hSlider, "SetStepsPerPage", GGF.OuterInvoke(state.hSlider, "GetValuesStepPerPage"));
	end
	--print("slider val", GGF.OuterInvoke(state.vSlider, "GetValuesMin"), GGF.OuterInvoke(state.vSlider, "GetValuesMax"), GGF.OuterInvoke(state.vSlider, "GetValuesStep"));
end


-- Пересчет сложной геометрии таблицы
function This:recountViewFieldGeometry(in_meta)
	local state = GGF.InnerInvoke(in_meta, "getCurrentState");
	-- NOTE: нужна ориентировка, т.к. прототип управляет шириной
	-- NOTE: ширина столбца с номером записи

	for rowidx=1,state.preallocatedFieldRowCount do
		GGF.OuterInvoke(state.vHeader[rowidx].button, "SetWidth", state.prototype.vHeader.width);
		GGF.OuterInvoke(state.vHeader[rowidx].button, "SetOffsetY", state.dataFieldOffsetY - (rowidx - 1)*(state.cellHeight + state.fontStringGap));

		GGF.OuterInvoke(state.vHeader[rowidx].fontString, "SetWidth", state.prototype.vHeader.width);
		GGF.OuterInvoke(state.vHeader[rowidx].fontString, "SetText", rowidx + state.rowIndent);

		GGF.OuterInvoke(state.vHeader[rowidx].texture, "SetWidth", state.prototype.vHeader.width);
	end

	local offsetX = state.dataFieldOffsetX;
	for colidx=1,state.visibleObjectRegionColumn do
		local curColMeta = state.prototype.columns[state.idxMap[colidx + state.columnIndent]];
		local width = GGF.TernExpSingle(curColMeta.width > state.minCellWidth, curColMeta.width, state.minCellWidth);
		local button = state.hHeader[colidx].button;
		GGF.OuterInvoke(button, "SetWidth", width);
		--hHeader[colidx].button:SetOffsetX(fieldOffsetX + (colidx - 1)*(minWidth + fontStringGap));
		--GGF.OuterInvoke(button, "SetOffsetX", offsetX);

      --GGF.OuterInvoke(button, "SetHidden", false);
      local str = state.hHeader[colidx].fontString;
      GGF.OuterInvoke(str, "SetColor", GGD.Color.Green);

		GGF.OuterInvoke(state.hHeader[colidx].fontString, "SetWidth", width);
		GGF.OuterInvoke(state.hHeader[colidx].fontString, "SetText", curColMeta.title);

		GGF.OuterInvoke(state.hHeader[colidx].texture, "SetWidth", width);

		offsetX = offsetX + width + state.fontStringGap;
	end
	for colidx=state.visibleObjectRegionColumn,state.preallocatedFieldColumnCount do
		--local button = state.hHeader[colidx].button;
		--GGF.OuterInvoke(button, "SetHidden", true);
      local str = state.hHeader[colidx].fontString;
      GGF.OuterInvoke(str, "SetColor", GGD.Color.Yellow);
	end

	for rowidx=1,state.visibleObjectRegionRow do
		--local button = state.vHeader[rowidx].button;
		--GGF.OuterInvoke(button, "SetHidden", false);
      local str = state.vHeader[rowidx].fontString;
      GGF.OuterInvoke(str, "SetColor", GGD.Color.Green);
	end
	for rowidx=state.visibleObjectRegionRow,state.preallocatedFieldRowCount do
		--local button = state.vHeader[rowidx].button;
		--GGF.OuterInvoke(button, "SetHidden", true);
      local str = state.vHeader[rowidx].fontString;
      GGF.OuterInvoke(str, "SetColor", GGD.Color.Yellow);
	end

	for rowidx=1,state.visibleObjectRegionRow do
		local offsetX = state.dataFieldOffsetX;
		for colidx=1,state.visibleObjectRegionColumn do
			local curColMeta = state.prototype.columns[state.idxMap[colidx + state.columnIndent]];
			local width = GGF.TernExpSingle(curColMeta.width > state.minCellWidth, curColMeta.width, state.minCellWidth);
			local button = state.centralField[rowidx][colidx].button;
			GGF.OuterInvoke(button, "SetWidth", width);
			--GGF.OuterInvoke(button, "SetOffsetX", offsetX);
			--GGF.OuterInvoke(button, "SetOffsetY", state.dataFieldOffsetY - (rowidx - 1)*(state.cellHeight + state.fontStringGap));
			--GGF.OuterInvoke(button, "SetHidden", false);
         local str = state.centralField[rowidx][colidx].fontString;
         GGF.OuterInvoke(str, "SetColor", GGD.Color.Green);


			local fontString = state.centralField[rowidx][colidx].fontString;
			GGF.OuterInvoke(fontString, "SetWidth", width);
			GGF.OuterInvoke(fontString, "SetText", GGF.InnerInvoke(in_meta, "countCellText", rowidx, colidx));

			GGF.OuterInvoke(state.centralField[rowidx][colidx].texture, "SetWidth", width);

			offsetX = offsetX + width + state.fontStringGap;
		end
		for colidx=state.visibleObjectRegionColumn,state.preallocatedFieldColumnCount do
			--local button = state.centralField[rowidx][colidx].button;
			--GGF.OuterInvoke(button, "SetHidden", true);
         local str = state.centralField[rowidx][colidx].fontString;
         GGF.OuterInvoke(str, "SetColor", GGD.Color.Yellow);
		end
	end
	for rowidx=state.visibleObjectRegionRow,state.preallocatedFieldRowCount do
		for colidx=1,state.preallocatedFieldColumnCount do
			--local button = state.centralField[rowidx][colidx].button;
			--GGF.OuterInvoke(button, "SetHidden", true);
         local str = state.centralField[rowidx][colidx].fontString;
         GGF.OuterInvoke(str, "SetColor", GGD.Color.Yellow);
		end
	end
end


-- Пересчет сложной геометрии таблицы
function This:proceedSliderChanges(in_meta, in_sliderChanges)
	local state = GGF.InnerInvoke(in_meta, "getCurrentState");

	if (not GGF.InnerInvoke(in_meta, "GetLaunched")) then
		return;
	end

	local vSliderVal = GGF.InnerInvoke(in_meta, "GetVerticalSliderValue");
	local hSliderVal = GGF.InnerInvoke(in_meta, "GetHorizontalSliderValue");
	local vSliderDiff = 0;
	local hSliderDiff = 0;
	if (in_sliderChanges.vertical ~= nil) then
		vSliderDiff = in_sliderChanges.vertical - vSliderVal;
		GGF.InnerInvoke(in_meta, "SetVerticalSliderValue", in_sliderChanges.vertical);
	end
	if (in_sliderChanges.horizontal ~= nil) then
		hSliderDiff = in_sliderChanges.horizontal - hSliderVal;
		GGF.InnerInvoke(in_meta, "SetHorizontalSliderValue", in_sliderChanges.horizontal);
	end
	--print("reco geom", vSliderDiff, hSliderDiff, in_sliderChanges.vertical, in_sliderChanges.horizontal);

	-- NOTE: ищем соответствующий первый элемент по локерам и прототипу
	--local dataSet = GGF.InnerInvoke(in_meta, "GetDataSet");
	local summaryWidth = 0;
	local startRequestIdx = 0;
	for colIdx,colData in ipairs(state.prototype.columns) do
		if (state.lockedRows[colIdx] == nil) then
			summaryWidth = summaryWidth + state.prototype.columns[colIdx].width;
			if (summaryWidth >= hSliderVal) then
				startRequestIdx = colIdx;
				break;
			end
		end
	end

   local dcpfX, dcpfY, dhpfX, dvpfY = GGF.OuterInvoke(state.dynamicCentralPhantomFrame, "GetOffsetX"), GGF.OuterInvoke(state.dynamicCentralPhantomFrame, "GetOffsetY"), GGF.OuterInvoke(state.dynamicHorizontalPhantomFrame, "GetOffsetX"), GGF.OuterInvoke(state.dynamicVerticalPhantomFrame, "GetOffsetY");

	--local width = GGF.TernExpSingle(prototype.vHeader.width > minWidth, prototype.vHeader.width, minWidth);
	if (vSliderDiff ~= 0) then
		GGF.OuterInvoke(state.dynamicCentralPhantomFrame, "SetOffsetY", GGF.OuterInvoke(state.dynamicCentralPhantomFrame, "GetOffsetY") + vSliderDiff);
		GGF.OuterInvoke(state.dynamicVerticalPhantomFrame, "SetOffsetY", GGF.OuterInvoke(state.dynamicVerticalPhantomFrame, "GetOffsetY") + vSliderDiff);
	end

	if (hSliderDiff ~= 0) then
		GGF.OuterInvoke(state.dynamicCentralPhantomFrame, "SetOffsetX", GGF.OuterInvoke(state.dynamicCentralPhantomFrame, "GetOffsetX") + hSliderDiff);
		GGF.OuterInvoke(state.dynamicHorizontalPhantomFrame, "SetOffsetX", GGF.OuterInvoke(state.dynamicHorizontalPhantomFrame, "GetOffsetX") + hSliderDiff);
	end

   --print("phantoms moved:",
      --"dcpf X:", dcpfX, "->", GGF.OuterInvoke(state.dynamicCentralPhantomFrame, "GetOffsetX"),
      --"dcpf Y:", dcpfY, "->", GGF.OuterInvoke(state.dynamicCentralPhantomFrame, "GetOffsetY"),
      --"dhpf X:", dhpfX, "->", GGF.OuterInvoke(state.dynamicHorizontalPhantomFrame, "GetOffsetX"),
      --"dvpf Y:", dvpfY, "->", GGF.OuterInvoke(state.dynamicVerticalPhantomFrame, "GetOffsetY"));
	--if (GGF.InnerInvoke(in_meta, "collisionCheck")) then
		--GGF.InnerInvoke(in_meta, "recountViewField");
	--end
	GGF.InnerInvoke(in_meta, "collisionCorrector");
	GGF.InnerInvoke(in_meta, "recountViewFieldGeometry");
end