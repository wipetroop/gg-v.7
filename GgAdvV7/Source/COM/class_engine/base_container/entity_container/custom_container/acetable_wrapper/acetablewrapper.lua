-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

-- NOTE: пока что не трогаем
local hInstance = {
	--gui = LibStub("AceGUI-3.0"),
	-- NOTE: временно отключено
	--tbl = LibStub("ScrollingTable"),
};

local OBJSuffix = "ATW";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	AceTableWrapperSuffix = GGF.DockDomName(GGD.DefineIdentifier, "AceTableWrapperSuffix"),
	AceTableWrapper = GGF.DockDomName(GGD.ClassIdentifier, "AceTableWrapper"),
};

local CTList = {
	AceTable = GGF.CreateCT(OBJSuffix, "AceTable", "AceTable"),
	SubObject = GGF.CreateCT(OBJSuffix, "SubObject", "NullFrame"),
	Highlight = GGF.CreateCT(OBJSuffix, "Highlight", "Highlight"),
	PrototypeMap = GGF.CreateCT(OBJSuffix, "PrototypeMap", "PrototypeMap"),
	FontHeight = GGF.CreateCT(OBJSuffix, "FontHeight", "FontHeight"),
	RowCount = GGF.CreateCT(OBJSuffix, "RowCount", "RowCount"),
	Hidden = GGF.CreateCT(OBJSuffix, "Hidden", "Hidden"),
	Selection = GGF.CreateCT(OBJSuffix, "Selection", "Selection"),
	Data = GGF.CreateCT(OBJSuffix, "Data", "Data"),
	Width = GGF.CreateCT(OBJSuffix, "Width", "Width"),
	Height = GGF.CreateCT(OBJSuffix, "Height", "Height"),
	Point = GGF.CreateCT(OBJSuffix, "Point", "Point"),
	RelativeTo = GGF.CreateCT(OBJSuffix, "RelativeTo", "RelativeTo"),
	RelativePoint = GGF.CreateCT(OBJSuffix, "RelativePoint", "RelativePoint"),
	OffsetX = GGF.CreateCT(OBJSuffix, "OffsetX", "OffsetX"),
	OffsetY = GGF.CreateCT(OBJSuffix, "OffsetY", "OffsetY"),
	OnCellEnter = GGF.CreateCT(OBJSuffix, "OnCellEnter", "EmptyCallback"),
	OnCellLeave = GGF.CreateCT(OBJSuffix, "OnCellLeave", "EmptyCallback"),
	OnCellClick = GGF.CreateCT(OBJSuffix, "OnCellClick", "EmptyCallback"),
};

GGF.GRegister(DFList.AceTableWrapperSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	AceTable = 0,
	NullFrame = 0,
	Highlight = 0,
	PrototypeMap = {},
	FontHeight = 12,
	RowCount = 40,
	Hidden = false,
	Selection = false,
	Data = {},
	Width = 800,
	Height = 200,
	Point = GGE.PointType.TopLeft,
	RelativeTo = function(in_meta) return UIParent end,
	RelativePoint = GGE.PointType.TopLeft,
	OffsetX = 0,
	OffsetY = 0,
	EmptyCallback = function() end,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.AceTableWrapper, {
	meta = {
		name = "Ace Table Wrapper",
		mark = "",	-- авт.
		suffix = GGD.AceTableWrapperSuffix,
	},
});

GGC.AceTableWrapper.objects = {
	table = {
		ace = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.AceTable,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.AceTable.Default)) then
					local aceTable = hInstance.tbl:CreateST(
						GGF.InnerInvoke(in_meta, "GetPrototypeMap"),
						GGF.InnerInvoke(in_meta, "GetRowCount"),
						GGF.InnerInvoke(in_meta, "GetFontHeight"),
						GGF.InnerInvoke(in_meta, "GetHighlighted"),
						GGF.InnerInvoke(in_meta, "GetParent")
					);
					GGF.InnerInvoke(in_meta, "ModifyAceTableOD", aceTable);
					-- HACK: принудительно убираем фон у таблицы
					local scrollPaneBackdrop  = {
						bgFile = "",
						edgeFile = "Interface\\Tooltips\\Ui-Tooltip-Border",
						tile = true, tileSize = 16, edgeSize = 16,
						insets = { left = 3, right = 3, top = 5, bottom = 3 }
					};
					GGF.InnerInvoke(in_meta, "SetSubObject", GGF.InnerInvoke(in_meta, "GetAceTable").frame);
					GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSubObject"), "SetBackdrop", scrollPaneBackdrop);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.AceTableWrapper.wrappers = {};

GGC.AceTableWrapper.properties = {
	base = {
		subObject = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.SubObject,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	miscellaneous = {
		highlight = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Highlight,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		prototypeMap = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.PrototypeMap,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		fontHeight = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.FontHeight,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		rowCount = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.RowCount,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		hidden = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Hidden,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (GGF.InnerInvoke(in_meta, "GetHidden")) then
					GGF.InnerInvoke(in_meta, "GetSubObject"):Hide();
				else
					GGF.InnerInvoke(in_meta, "GetSubObject"):Show();
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		selection = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Selection,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:EnableSelection(GGF.InnerInvoke(in_meta, "GetSelection"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		data = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Data,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetData(GGF.InnerInvoke(in_meta, "GetData"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	size = {
		width = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Width,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "adjustSize");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		height = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Height,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "adjustSize");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	anchor = {
		point = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Point,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		relativeTo = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.RelativeTo,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		relativePoint = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.RelativePoint,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		offsetX = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OffsetX,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		offsetY = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OffsetY,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustAnchor");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	callback = {
		onCellEnter = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnCellEnter,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustCallbackSet");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onCellLeave = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnCellLeave,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustCallbackSet");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		onCellClick = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnCellClick,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "AdjustCallbackSet");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.AceTableWrapper.elements = {};

GGF.Mark(GGC.AceTableWrapper);

GGC.AceTableWrapper = GGF.TableSafeExtend(GGC.AceTableWrapper, GGC.EntityContainer);

local This = GGC.AceTableWrapper;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-- Смена габаритов объекта
function This:ModifySize(in_meta, in_sz)
	if (in_sz ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyWidth", in_sz.width);
		GGF.InnerInvoke(in_meta, "ModifyHeight", in_sz.height);
	end
end


-- Смена геометрической точки привязки(якоря) объектов
function This:ModifyAnchor(in_meta, in_an)
	if (in_an ~= nil) then
		GGF.InnerInvoke(in_meta, "ModifyPoint", in_an.point);
		GGF.InnerInvoke(in_meta, "ModifyRelativeTo", in_an.relativeTo);
		GGF.InnerInvoke(in_meta, "ModifyRelativePoint", in_an.relativePoint);
		GGF.InnerInvoke(in_meta, "ModifyOffsetX", in_an.offsetX);
		GGF.InnerInvoke(in_meta, "ModifyOffsetY", in_an.offsetY);
	end
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Смена габаритов объекта
function This:SetSize(in_meta, in_sz)
	GGF.InnerInvoke(in_meta, "ModifySize", in_sz);
	GGF.InnerInvoke(in_meta, "AdjustSize");
end


-- Смена геометрической точки привязки(якоря) объектов
function This:SetAnchor(in_meta, in_an)
	GGF.InnerInvoke(in_meta, "ModifyAnchor", in_an);
	GGF.InnerInvoke(in_meta, "AdjustAnchor");
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие габаритов объекта
function This:GetSize(in_meta)
	local size = {
		width = GGF.InnerInvoke(in_meta, "GetWidth"),
		height = GGF.InnerInvoke(in_meta, "GetHeight"),
	};
	return size;
end


-- Взятие точки привязки(якоря) объекта
function This:GetAnchor(in_meta)
	local anchor = {
		point = GGF.InnerInvoke(in_meta, "GetPoint"),
		relativeTo = GGF.InnerInvoke(in_meta, "GetRelativeTo"),
		relativePoint = GGF.InnerInvoke(in_meta, "GetRelativePoint"),
		offsetX = GGF.InnerInvoke(in_meta, "GetOffsetX"),
		offsetY = GGF.InnerInvoke(in_meta, "GetOffsetY"),
	};
	return anchor;
end


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Установка габаритов объекта
function This:AdjustSize(in_meta)
	local size = GGF.InnerInvoke(in_meta, "GetSize");
	GGF.InnerInvoke(in_meta, "GetSubObject"):SetSize(size.width, size.height);
end


-- Установка точки привязки исходного объекта
function This:AdjustAnchor(in_meta)
	local anchor = GGF.InnerInvoke(in_meta, "GetAnchor");
	GGF.InnerInvoke(in_meta, "GetSubObject"):ClearAllPoints();
	GGF.InnerInvoke(in_meta, "GetSubObject"):SetPoint(anchor.point, anchor.relativeTo, anchor.relativePoint, anchor.offsetX, anchor.offsetY);
end


-- Установка коллбеков
function This:AdjustCallbackSet(in_meta)
	in_meta.object:RegisterEvents({
		["OnEnter"] = GGF.InnerInvoke(in_meta, "GetOnCellEnterCounter"),
		["OnLeave"] = GGF.InnerInvoke(in_meta, "GetOnCellLeaveCounter"),
		["OnClick"] = GGF.InnerInvoke(in_meta, "GetOnCellClickCounter")
	});
end


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Взятие индекса выделенной линии
function This:GetSelection(in_meta)
	return in_meta.object:GetSelection();
end


-- Сброс выделения
function This:ClearSelection(in_meta)
	return in_meta.object:ClearSelection();
end


-- Установка выделенной линии по индексу
function This:SetSelection(in_meta, in_realrow)
	return in_meta.object:SetSelection(in_realrow);
end