-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "FF";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	FilterFrameSuffix = GGF.DockDomName(GGD.DefineIdentifier, "FilterFrameSuffix"),
	FilterFrame = GGF.DockDomName(GGD.ClassIdentifier, "FilterFrame"),
};

local CTList = {
	Frame = GGF.CreateCT(OBJSuffix, "FilterFrame", "Frame"),
	Type = GGF.CreateCT(OBJSuffix, "Type", "Type"),
	Token = GGF.CreateCT(OBJSuffix, "Token", "Token"),
	OnRemove = GGF.CreateCT(OBJSuffix, "OnRemove", "EmptyCallback"),
	SubObjectList = GGF.CreateCT(OBJSuffix, "SubObjectList", "SubObjectList"),
	FilterData = GGF.CreateCT(OBJSuffix, "FilterData", "FilterData"),
};

GGF.GRegister(DFList.FilterFrameSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	Type = GGE.FilterType.Log,
	Token = "EI-TEST",
	EmptyCallback = function(in_meta) end,
	SubObjectList = {},
	FilterData = {},
}, GGE.RegValType.Default);

GGF.GRegister(DFList.FilterFrame, {
	meta = {
		name = "Filter Frame",
		mark = "",	-- авт.
		suffix = GGD.FilterFrameSuffix,
	},
});

GGC.FilterFrame.objects = {
	frame = {
		filter = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Frame,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.FilterFrame.wrappers = {};

GGC.FilterFrame.properties = {
	miscellaneous = {
		type = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Type,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		token = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Token,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	callback = {
		onRemove = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnRemove,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.FilterFrame.elements = {
	runtime = {
		-- WARNING: не создавать из вне всё, что ниже до элементов!!!
		subojbectlist = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Element,
			cte = CTList.SubObjectList,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		filterData = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Element,
			cte = CTList.FilterData,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "createSubObjectList", GGF.InnerInvoke(in_meta, "GetFilterData"));
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGF.Mark(GGC.FilterFrame);

GGC.FilterFrame = GGF.TableSafeExtend(GGC.FilterFrame, GGC.StandardFrame);

GGF.CreateClassWorkaround(GGC.FilterFrame);

local This = GGC.FilterFrame;

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Создание основных объектов фильтрфрейма
function This:createSubObjectList(in_meta, in_filterData)
	local subobjlist = {
		Title = "",
		token = "",
		filter = {		-- Log/Data
			ln1 = "",	-- BF Name(Token)/Class
			ln2 = "",	-- Start time/Type
			ln3 = "",	-- Finish Time/Subtype
		},
		deleter = "",
		switcher = "",
	};
	local isLog = (GGF.InnerInvoke(in_meta, "GetType") == GGE.FilterType.Log);
	local prefix = GGF.InnerInvoke(in_meta, "GetObjectName");
	local fData = {
		battlefieldSignature = "",
		startTime = "",
		finishTime = "",
		class = {
			name = "",
			signature = "",
		},
		type = {
			name = "",
			signature = "",
		},
		subtype = {
			name = "",
			signature = "",
		},
	};
	fData = GGF.TableMerge(fData, in_filterData);
	local filterFrame = in_meta.object;

	subobjlist.title = GGC.FontString({}, {
		properties = {
			base = {
				parent = filterFrame,
				wrapperName = "TL",
			},
			miscellaneous = {
				text = GGF.StructColorToStringColor(GGF.TernExpSingle(isLog, GGD.Color.Green, GGD.Color.Blue)) .. GGF.TernExpSingle(isLog, "Log", "Data") .. GGF.FlushStringColor(),
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = filterFrame,
				relativePoint = GGE.PointType.TopLeft,
				offsetX = 10,
				offsetY = -10,
			},
		},
	});
	subobjlist.title:Adjust();
	--(prefix .. "_TL_FS_", "ARTWORK", "GameFontNormal")

	subobjlist.token = GGC.FontString({}, {
		properties = {
			base = {
				parent = filterFrame,
				wrapperName = "TK",
			},
			miscellaneous = {
				text = GGF.StructColorToStringColor(GGD.Color.Title) .. GGF.InnerInvoke(in_meta, "GetToken") .. GGF.FlushStringColor(),
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = filterFrame,
				relativePoint = GGE.PointType.TopLeft,
				offsetX = 10,
				offsetY = -30,
			},
		},
	});
	subobjlist.token:Adjust();
	--(prefix .. "_TK_FS_", "ARTWORK", "GameFontNormal")

	subobjlist.filter.ln1 = GGC.FontString({}, {
		properties = {
			base = {
				parent = filterFrame,
				wrapperName = "FL_1",
			},
			miscellaneous = {
				text = GGF.StructColorToStringColor(GGD.Color.Title) .. GGF.TernExpSingle(isLog, fData.battlefieldSignature, fData.class.name) .. GGF.FlushStringColor(),
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = filterFrame,
				relativePoint = GGE.PointType.TopLeft,
				offsetX = 10,
				offsetY = -50,
			},
		},
	});
	subobjlist.filter.ln1:Adjust();
	--(prefix .. "_FL_FS_1_", "ARTWORK", "GameFontNormal");

	subobjlist.filter.ln2 = GGC.FontString({}, {
		properties = {
			base = {
				parent = filterFrame,
				wrapperName = "FL_1",
			},
			miscellaneous = {
				text = GGF.StructColorToStringColor(GGD.Color.Title) .. GGF.TernExpSingle(isLog, fData.startTime, fData.type.name) .. GGF.FlushStringColor(),
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = filterFrame,
				relativePoint = GGE.PointType.TopLeft,
				offsetX = 10,
				offsetY = -70,
			},
		},
	});
	subobjlist.filter.ln2:Adjust();
	--(prefix .. "_FL_FS_2_", "ARTWORK", "GameFontNormal");

	subobjlist.filter.ln3 = GGC.FontString({}, {
		properties = {
			base = {
				parent = filterFrame,
				wrapperName = "FL_1",
			},
			miscellaneous = {
				text = GGF.StructColorToStringColor(GGD.Color.Title) .. GGF.TernExpSingle(isLog, fData.finishTime, fData.subtype.name) .. GGF.FlushStringColor(),
			},
			anchor = {
				point = GGE.PointType.TopLeft,
				relativeTo = filterFrame,
				relativePoint = GGE.PointType.TopLeft,
				offsetX = 10,
				offsetY = -90,
			},
		},
	});
	--(prefix .. "_FL_FS_3_", "ARTWORK", "GameFontNormal");
	subobjlist.filter.ln3:Adjust();

	--local elementBackdrop = GGD.Backdrop.Empty;
	local buttonSize = 32;
	local switcherVSize = 40;
	local switcherVCompensator = 8;
	local gap = 5;

	subobjlist.deleter = GGC.StandardButton:Create({}, {
		properties = {
			base = {
				--Object = CreateFrame("Button", self:GetObjectName() .. "_DLTR_B", self:GetObject(), "UiPanelCloseButton"),
				parent = filterFrame,
				template = "UIPanelCloseButton",
				wrapperName = "DLTR",
			},
			miscellaneous = {
				enabled = true,
				text = "Delete",
				tooltipText = "Removing filter from list",
			},
			backdrop = GGD.Backdrop.Empty,
			size = {
				width = buttonSize,
				height = buttonSize,
			},
			anchor = {
				point = GGE.PointType.TopRight,
				relativeTo = filterFrame,
				relativePoint = GGE.PointType.TopRight,
				offsetX = - gap,
				offsetY = - gap,
			},
			callback = {
				onClick = function() GGF.InnerInvoke(in_meta, "GetOnRemoveCounter")(GGF.InnerInvoke(in_meta, "GetType"), GGF.InnerInvoke(in_meta, "GetToken")) end,
			},
		},
	});
	subobjlist.deleter:Adjust();

	subobjlist.switcher = GGC.TumblerSwitcher:Create({}, {
		properties = {
			base = {
				--Object = CreateFrame("Button", self:GetObjectName() .. "_SWTC_SW", self:GetObject()),
				parent = filterFrame,
			},
			miscellaneous = {
				enabled = true,
				state = true,
				text = "Enabled",
				tooltipText = "Switch filter usage",
			},
			backdrop = GGD.Backdrop.Empty,
			size = {
				width = 2*switcherVSize,
				height = switcherVSize,
			},
			anchor = {
				point = "TopRight",
				relativeTo = filterFrame,
				relativePoint = "TopRight",
				offsetX = - gap,
				offsetY = - 2*gap - buttonSize + switcherVCompensator,
			},
		},
	});
	subobjlist.switcher:Adjust();

	GGF.InnerInvoke(in_meta, "SetSubObjectList", subobjlist);
end


-- Взятие сигнатуры фильтра
function This:GetFilterSignature()
	return GGF.InnerInvoke(in_meta, "GetFilterData");
end


-- Проверка подключенности фильтра
function This:GetFilterEnabled()
	return GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSubObjectList").switcher, "GetState");
end
