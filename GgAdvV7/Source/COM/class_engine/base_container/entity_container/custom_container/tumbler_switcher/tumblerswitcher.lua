-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


-- NOTE: по сути это обыкновенная залипайка
local OBJSuffix = "TMS";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	TumblerSwitcherSuffix = GGF.DockDomName(GGD.DefineIdentifier, "TumblerSwitcherSuffix"),
	TumblerSwitcher = GGF.DockDomName(GGD.ClassIdentifier, "TumblerSwitcher"),
};

local CTList = {
	TumblerSwitcher = GGF.CreateCT(OBJSuffix, "TumblerSwitcher", "Frame"),
	SCEnabled = GGF.CreateCT(OBJSuffix, "EnabledStateColor", "EnabledTexture"),
	SCDisabled = GGF.CreateCT(OBJSuffix, "DisabledStateColor", "DisabledTexture"),
	OnClick = GGF.CreateCT(OBJSuffix, "OnClick", "OnClick"),
	State = GGF.CreateCT(OBJSuffix, "State", "State"),
};

GGF.GRegister(DFList.TumblerSwitcherSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	EnabledTexture = GGD.COM.TexturePath .. "on_texture.tga",
	DisabledTexture = GGD.COM.TexturePath .. "off_texture.tga",
	OnClick = function(in_meta)
		GGF.InnerInvoke(in_meta, "changeState");
	end,
	State = false,
}, GGE.RegValType.Default);

-- TODO: пошаманить с цветом
GGF.GRegister(DFList.TumblerSwitcher, {
	meta = {
		name = "Tumbler Switcher",
		mark = "",	-- авт.
		suffix = GGD.TumblerSwitcherSuffix,
	},
});

GGC.TumblerSwitcher.objects = {
	tumbler = {
		switcher = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.TumblerSwitcher,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (GGF.InnerInvoke(in_meta, "GetTumblerSwitcher") == GGF.GetDefaultValue(OBJSuffix, CTList.TumblerSwitcher.Default)) then
					local standardButton = CreateFrame(
						"Button",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyTumblerSwitcherOD", standardButton);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.TumblerSwitcher.wrappers = {};

GGC.TumblerSwitcher.properties = {
	stateColors = {
		enabled = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.SCEnabled,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "adjustStateTextures");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "synchronizeStateTextures");
			end,
		}),
		disabled = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.SCDisabled,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "adjustStateTextures");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "synchronizeStateTextures");
			end,
		}),
	},
	callback = {
		onClick = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.OnClick,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				in_meta.object:SetScript("OnClick", function()
					GGF.InnerInvoke(in_meta, "GetOnClickCounter")(in_meta)
				end);
				-- TODO: разобраться, зачем тут лишний вызов
				GGF.InnerInvoke(in_meta, "changeState");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.TumblerSwitcher.elements = {
	state = GGC.ParameterContainer:Create({
		ctype = GGE.ContainerType.Element,
		cte = CTList.State,
		fixed = false,

		[GGE.RuntimeMethod.Adjuster] = function(in_meta)
		end,
		[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
		end,
	}),
};

GGF.Mark(GGC.TumblerSwitcher);

GGC.TumblerSwitcher = GGF.TableSafeExtend(GGC.TumblerSwitcher, GGC.StuckingButton);

GGF.CreateClassWorkaround(GGC.TumblerSwitcher);

local This = GGC.TumblerSwitcher;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Применение цветосхемы переключателя
function This:adjustStateTextures(in_meta)
	local object = in_meta.object;
	local texture = GGF.TernExpSingle(
		GGF.InnerInvoke(in_meta, "GetState"),
		GGF.InnerInvoke(in_meta, "GetEnabledStateColor"),
		GGF.InnerInvoke(in_meta, "GetDisabledStateColor")
	);
	object:SetNormalTexture(texture);
	object:SetHighlightTexture(texture);
end


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-- Синхронизация цветосхемы переключателя
function This:synchronizeStateTextures(in_meta)
	local object = in_meta.object;
	local state = GGF.InnerInvoke(in_meta, "GetState");
	local normalTex = object:GetNormalTexture();
	local highlightTex = object:GetHighlightTexture();
	ATLASSERT(normalTex == highlightTex);
	if (state) then
		GGF.InnerInvoke(in_meta, "ModifyEnabledStateColor", normalTex);
	else
		GGF.InnerInvoke(in_meta, "ModifyDisabledStateColor", normalTex);
	end
end


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Смена состояния тумблерной кнопки
function This:changeState(in_meta)
	GGF.InnerInvoke(in_meta, "SetState", not GGF.InnerInvoke(in_meta, "GetState"));
	GGF.InnerInvoke(in_meta, "adjustStateTextures");
end