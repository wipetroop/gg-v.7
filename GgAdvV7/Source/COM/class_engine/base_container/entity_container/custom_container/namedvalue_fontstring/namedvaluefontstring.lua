-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "NVFS";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	NamedValueSuffix = GGF.DockDomName(GGD.DefineIdentifier, "NamedValueSuffix"),
	NamedValueFontString = GGF.DockDomName(GGD.ClassIdentifier, "NamedValueFontString"),
};

local CTList = {
	ControlFrame = GGF.CreateCT(OBJSuffix, "ControlFrame", "Frame"),
	NameFontStringWrapper = GGF.CreateCT(OBJSuffix, "NameFontStringWrapper", "Frame"),
	ValueFontStringWrapper = GGF.CreateCT(OBJSuffix, "ValueFontStringWrapper", "Frame"),
	Template = GGF.CreateCT(OBJSuffix, "Template", "Template"),
	WrapperName = GGF.CreateCT(OBJSuffix, "WrapperName", "WrapperName"),
	Hidden = GGF.CreateCT(OBJSuffix, "Hidden", "Hidden"),
	Layer = GGF.CreateCT(OBJSuffix, "Layer", "Layer"),
	Name = GGF.CreateCT(OBJSuffix, "Name", "Name"),
	Value = GGF.CreateCT(OBJSuffix, "Value", "Value"),
	NameTooltip = GGF.CreateCT(OBJSuffix, "NameTooltip", "NameTooltip"),
	ValueTooltip = GGF.CreateCT(OBJSuffix, "ValueTooltip", "ValueTooltip"),
	FontHeight = GGF.CreateCT(OBJSuffix, "FontHeight", "FontHeight"),
	Alpha = GGF.CreateCT(OBJSuffix, "Alpha", "Alpha"),
	LRPercentage = GGF.CreateCT(OBJSuffix, "LRPercentage", "LRPercentage"),
	JustifyV = GGF.CreateCT(OBJSuffix, "JustifyV", "JustifyV"),
	NameColorR = GGF.CreateCT(OBJSuffix, "NameColorR", "ColorR"),
	NameColorG = GGF.CreateCT(OBJSuffix, "NameColorG", "ColorG"),
	NameColorB = GGF.CreateCT(OBJSuffix, "NameColorB", "ColorB"),
	NameColorA = GGF.CreateCT(OBJSuffix, "NameColorA", "ColorA"),
	ValueColorR = GGF.CreateCT(OBJSuffix, "ValueColorR", "ColorR"),
	ValueColorG = GGF.CreateCT(OBJSuffix, "ValueColorG", "ColorG"),
	ValueColorB = GGF.CreateCT(OBJSuffix, "ValueColorB", "ColorB"),
	ValueColorA = GGF.CreateCT(OBJSuffix, "ValueColorA", "ColorA"),
	NameShadowColorR = GGF.CreateCT(OBJSuffix, "NameShadowColorR", "ShadowColorR"),
	NameShadowColorG = GGF.CreateCT(OBJSuffix, "NameShadowColorG", "ShadowColorG"),
	NameShadowColorB = GGF.CreateCT(OBJSuffix, "NameShadowColorB", "ShadowColorB"),
	NameShadowColorA = GGF.CreateCT(OBJSuffix, "NameShadowColorA", "ShadowColorA"),
	ValueShadowColorR = GGF.CreateCT(OBJSuffix, "ValueShadowColorR", "ShadowColorR"),
	ValueShadowColorG = GGF.CreateCT(OBJSuffix, "ValueShadowColorG", "ShadowColorG"),
	ValueShadowColorB = GGF.CreateCT(OBJSuffix, "ValueShadowColorB", "ShadowColorB"),
	ValueShadowColorA = GGF.CreateCT(OBJSuffix, "ValueShadowColorA", "ShadowColorA"),
	NameShadowOffsetX = GGF.CreateCT(OBJSuffix, "NameShadowOffsetX", "ShadowOffsetX"),
	NameShadowOffsetY = GGF.CreateCT(OBJSuffix, "NameShadowOffsetY", "ShadowOffsetY"),
	ValueShadowOffsetX = GGF.CreateCT(OBJSuffix, "ValueShadowOffsetX", "ShadowOffsetX"),
	ValueShadowOffsetY = GGF.CreateCT(OBJSuffix, "ValueShadowOffsetY", "ShadowOffsetY"),
};

GGF.GRegister(DFList.NamedValueSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
	Template = 0,
	WrapperName = "NVFS",
	Hidden = false,
	Layer = GGE.LayerType.Artwork,
	Name = "Default name",
	Value = "Default value",
	NameTooltip = "Default tooltip text",
	ValueTooltip = "Default tooltip text",
	FontHeight = 12,
	Alpha = 1.0,
	LRPercentage = 0.5,
	NameWidth = 800,
	ValueWidth = 800,
	Height = 200,
	JustifyV = GGE.JustifyVType.Middle,
	RelativeTo = function(in_meta)
		return UIParent;
	end,
	RelativePoint = function(in_meta)
		return UIParent;
	end,
	OffsetX = 0,
	OffsetY = 0,
	ColorR = 1.0,
	ColorG = 0.8,
	ColorB = 1.0,
	ColorA = 1.0,
	ShadowColorR = 0.0,
	ShadowColorG = 0.0,
	ShadowColorB = 0.0,
	ShadowColorA = 1.0,
	ShadowOffsetX = 0.0,
	ShadowOffsetY = 0.0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.NamedValueFontString, {
	meta = {
		name = "Named Value Font String",
		mark = "",	-- авт.
		suffix = GGD.NamedValueSuffix,
	},
});

GGC.NamedValueFontString.objects = {
	frame = {
		control = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.ControlFrame,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.ControlFrame.Default)) then
					local controlFrame = CreateFrame(
						"Frame",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
                  GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
         						return GGF.TernExpSingle(
         							GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
         							nil
         						)
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     },
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyControlFrameOD", controlFrame);
					--controlFrame:SetBackdrop(GGD.Backdrop.Segregate);
					--print("standardframe cr:", standardFrame);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.NamedValueFontString.wrappers = {
	fontString = {
		name = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.NameFontStringWrapper,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		value = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Wrapper,
			cte = CTList.ValueFontStringWrapper,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.NamedValueFontString.properties = {
	miscellaneous = {
		layer = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Layer,
			fixed = false,

			-- NOTE: учтен в конструкторе
			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		name = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Name,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.OuterInvoke(
					GGF.InnerInvoke(in_meta, "GetNameFontStringWrapper"),
					"SetText",
					GGF.InnerInvoke(in_meta, "GetName")
				);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				local nfsw = GGF.InnerInvoke(in_meta, "GetNameFontStringWrapper");
				GGF.OuterInvoke(nfsw, "SynchronizeText");
				GGF.InnerInvoke(in_meta, "SetName", GGF.OuterInvoke(nfsw, "GetText"));
			end,
		}),
		value = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Value,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.OuterInvoke(
					GGF.InnerInvoke(in_meta, "GetValueFontStringWrapper"),
					"SetText",
					GGF.InnerInvoke(in_meta, "GetValue")
				);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				local nfsw = GGF.InnerInvoke(in_meta, "GetValueFontStringWrapper");
				GGF.OuterInvoke(nfsw, "SynchronizeText");
				GGF.InnerInvoke(in_meta, "SetValue", GGF.OuterInvoke(nfsw, "GetText"));
			end,
		}),
		nameTooltip = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.NameTooltip,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.OuterInvoke(
					GGF.InnerInvoke(in_meta, "GetNameFontStringWrapper"),
					"SetTooltipText",
					GGF.InnerInvoke(in_meta, "GetNameTooltip")
				);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				local nfsw = GGF.InnerInvoke(in_meta, "GetNameFontStringWrapper");
				GGF.OuterInvoke(nfsw, "SynchronizeTooltipText");
				GGF.InnerInvoke(in_meta, "SetNameTooltip", GGF.OuterInvoke(nfsw, "GetTooltipText"));
			end,
		}),
		valueTooltip = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ValueTooltip,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.OuterInvoke(
					GGF.InnerInvoke(in_meta, "GetValueFontStringWrapper"),
					"SetTooltipText",
					GGF.InnerInvoke(in_meta, "GetValueTooltip")
				);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				local vfsw = GGF.InnerInvoke(in_meta, "GetValueFontStringWrapper");
				GGF.OuterInvoke(vfsw, "SynchronizeTooltipText");
				GGF.InnerInvoke(in_meta, "SetValueTooltip", GGF.OuterInvoke(vfsw, "GetTooltipText"));
			end,
		}),
		fontHeight = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.FontHeight,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.OuterInvoke(
					GGF.InnerInvoke(in_meta, "GetNameFontStringWrapper"),
					"SetFontHeight",
					GGF.InnerInvoke(in_meta, "GetFontHeight")
				);
				GGF.OuterInvoke(
					GGF.InnerInvoke(in_meta, "GetValueFontStringWrapper"),
					"SetFontHeight",
					GGF.InnerInvoke(in_meta, "GetFontHeight")
				);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				local nfsw = GGF.InnerInvoke(in_meta, "GetNameFontStringWrapper");
				GGF.OuterInvoke(nfsw, "SynchronizeFontHeight");
				GGF.InnerInvoke(in_meta, "SetFontHeight", GGF.OuterInvoke(nfsw, "GetFontHeight"));
			end,
		}),
		alpha = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Alpha,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				--print("nvfs alpha", GGF.InnerInvoke(in_meta, "GetAlpha"));
				GGF.OuterInvoke(
					GGF.InnerInvoke(in_meta, "GetNameFontStringWrapper"),
					"SetAlpha",
					GGF.InnerInvoke(in_meta, "GetAlpha")
				);
				GGF.OuterInvoke(
					GGF.InnerInvoke(in_meta, "GetValueFontStringWrapper"),
					"SetAlpha",
					GGF.InnerInvoke(in_meta, "GetAlpha")
				);
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				local nfsw = GGF.InnerInvoke(in_meta, "GetNameFontStringWrapper");
				GGF.OuterInvoke(nfsw, "SynchronizeAlpha");
				GGF.InnerInvoke(in_meta, "SetAlpha", GGF.OuterInvoke(nfsw, "GetAlpha"));
			end,
		}),
	},
	size = {
		lrPercentage = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.LRPercentage,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				local prcnt = GGF.InnerInvoke(in_meta, "GetLRPercentage");
				local width = GGF.InnerInvoke(in_meta, "GetWidth");
				GGF.OuterInvoke(
					GGF.InnerInvoke(in_meta, "GetNameFontStringWrapper"),
					"SetWidth",
					prcnt*width
				);
				GGF.OuterInvoke(
					GGF.InnerInvoke(in_meta, "GetValueFontStringWrapper"),
					"SetWidth",
					(1 - prcnt)*width
				);
			end,
			-- NOTE: возможно, это и понадобится...
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	anchor = {
		justifyV = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.JustifyV,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	color = {
		name = {
			R = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.NameColorR,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustNameColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeNameColor");
				end,
			}),
			G = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.NameColorG,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustNameColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeNameColor");
				end,
			}),
			B = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.NameColorB,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustNameColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeNameColor");
				end,
			}),
			A = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.NameColorA,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustNameColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeNameColor");
				end,
			}),
		},
		value = {
			R = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ValueColorR,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustValueColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeValueColor");
				end,
			}),
			G = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ValueColorG,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustValueColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeValueColor");
				end,
			}),
			B = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ValueColorB,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustValueColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeValueColor");
				end,
			}),
			A = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.ValueColorA,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
					GGF.InnerInvoke(in_meta, "AdjustValueColor");
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
					GGF.InnerInvoke(in_meta, "SynchronizeValueColor");
				end,
			}),
		},
	},
	shadow = {
		color = {
			name = {
				R = GGC.ParameterContainer:Create({
					ctype = GGE.ContainerType.Property,
					cte = CTList.NameShadowColorR,
					fixed = false,

					[GGE.RuntimeMethod.Adjuster] = function(in_meta)
						GGF.InnerInvoke(in_meta, "AdjustNameShadowColor");
					end,
					[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
						GGF.InnerInvoke(in_meta, "SynchronizeNameShadowColor");
					end,
				}),
				G = GGC.ParameterContainer:Create({
					ctype = GGE.ContainerType.Property,
					cte = CTList.NameShadowColorG,
					fixed = false,

					[GGE.RuntimeMethod.Adjuster] = function(in_meta)
						GGF.InnerInvoke(in_meta, "AdjustNameShadowColor");
					end,
					[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
						GGF.InnerInvoke(in_meta, "SynchronizeNameShadowColor");
					end,
				}),
				B = GGC.ParameterContainer:Create({
					ctype = GGE.ContainerType.Property,
					cte = CTList.NameShadowColorB,
					fixed = false,

					[GGE.RuntimeMethod.Adjuster] = function(in_meta)
						GGF.InnerInvoke(in_meta, "AdjustNameShadowColor");
					end,
					[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
						GGF.InnerInvoke(in_meta, "SynchronizeNameShadowColor");
					end,
				}),
				A = GGC.ParameterContainer:Create({
					ctype = GGE.ContainerType.Property,
					cte = CTList.NameShadowColorA,
					fixed = false,

					[GGE.RuntimeMethod.Adjuster] = function(in_meta)
						GGF.InnerInvoke(in_meta, "AdjustNameShadowColor");
					end,
					[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
						GGF.InnerInvoke(in_meta, "SynchronizeNameShadowColor");
					end,
				}),
			},
			value = {
				R = GGC.ParameterContainer:Create({
					ctype = GGE.ContainerType.Property,
					cte = CTList.ValueShadowColorR,
					fixed = false,

					[GGE.RuntimeMethod.Adjuster] = function(in_meta)
						GGF.InnerInvoke(in_meta, "AdjustValueShadowColor");
					end,
					[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
						GGF.InnerInvoke(in_meta, "SynchronizeValueShadowColor");
					end,
				}),
				G = GGC.ParameterContainer:Create({
					ctype = GGE.ContainerType.Property,
					cte = CTList.ValueShadowColorG,
					fixed = false,

					[GGE.RuntimeMethod.Adjuster] = function(in_meta)
						GGF.InnerInvoke(in_meta, "AdjustValueShadowColor");
					end,
					[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
						GGF.InnerInvoke(in_meta, "SynchronizeValueShadowColor");
					end,
				}),
				B = GGC.ParameterContainer:Create({
					ctype = GGE.ContainerType.Property,
					cte = CTList.ValueShadowColorB,
					fixed = false,

					[GGE.RuntimeMethod.Adjuster] = function(in_meta)
						GGF.InnerInvoke(in_meta, "AdjustValueShadowColor");
					end,
					[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
						GGF.InnerInvoke(in_meta, "SynchronizeValueShadowColor");
					end,
				}),
				A = GGC.ParameterContainer:Create({
					ctype = GGE.ContainerType.Property,
					cte = CTList.ValueShadowColorA,
					fixed = false,

					[GGE.RuntimeMethod.Adjuster] = function(in_meta)
						GGF.InnerInvoke(in_meta, "AdjustValueShadowColor");
					end,
					[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
						GGF.InnerInvoke(in_meta, "SynchronizeValueShadowColor");
					end,
				}),
			},
		},
		offset = {
			name = {
				X = GGC.ParameterContainer:Create({
					ctype = GGE.ContainerType.Property,
					cte = CTList.NameShadowOffsetX,
					fixed = false,

					[GGE.RuntimeMethod.Adjuster] = function(in_meta)
						GGF.InnerInvoke(in_meta, "AdjustNameShadowOffset");
					end,
					[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
						GGF.InnerInvoke(in_meta, "SynchronizeNameShadowOffset");
					end,
				}),
				Y = GGC.ParameterContainer:Create({
					ctype = GGE.ContainerType.Property,
					cte = CTList.NameShadowOffsetY,
					fixed = false,

					[GGE.RuntimeMethod.Adjuster] = function(in_meta)
						GGF.InnerInvoke(in_meta, "AdjustNameShadowOffset");
					end,
					[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
						GGF.InnerInvoke(in_meta, "SynchronizeNameShadowOffset");
					end,
				}),
			},
			value = {
				X = GGC.ParameterContainer:Create({
					ctype = GGE.ContainerType.Property,
					cte = CTList.ValueShadowOffsetX,
					fixed = false,

					[GGE.RuntimeMethod.Adjuster] = function(in_meta)
						GGF.InnerInvoke(in_meta, "AdjustValueShadowOffset");
					end,
					[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
						GGF.InnerInvoke(in_meta, "SynchronizeValueShadowOffset");
					end,
				}),
				Y = GGC.ParameterContainer:Create({
					ctype = GGE.ContainerType.Property,
					cte = CTList.ValueShadowOffsetY,
					fixed = false,

					[GGE.RuntimeMethod.Adjuster] = function(in_meta)
						GGF.InnerInvoke(in_meta, "AdjustValueShadowOffset");
					end,
					[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
						GGF.InnerInvoke(in_meta, "SynchronizeValueShadowOffset");
					end,
				}),
			},
		},
	},
};

GGC.NamedValueFontString.elements = {};

GGF.Mark(GGC.NamedValueFontString);

--GGC.NamedValueFontString = GGF.TableSafeExtend(GGC.NamedValueFontString, GGC.CustomContainer);
GGC.NamedValueFontString = GGF.TableSafeExtend(GGC.NamedValueFontString, GGC.StandardFrame);

GGF.CreateClassWorkaround(GGC.NamedValueFontString);

local This = GGC.NamedValueFontString;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-- Взятие цвета строки имени
function This:ModifyNameColor(in_meta, in_color)
	GGF.InnerInvoke(in_meta, "ModifyNameColorR", in_color.R);
	GGF.InnerInvoke(in_meta, "ModifyNameColorG", in_color.G);
	GGF.InnerInvoke(in_meta, "ModifyNameColorB", in_color.B);
	GGF.InnerInvoke(in_meta, "ModifyNameColorA", in_color.A);
end


-- Взятие цвета строки значения
function This:ModifyValueColor(in_meta, in_color)
	GGF.InnerInvoke(in_meta, "ModifyValueColorR", in_color.R);
	GGF.InnerInvoke(in_meta, "ModifyValueColorG", in_color.G);
	GGF.InnerInvoke(in_meta, "ModifyValueColorB", in_color.B);
	GGF.InnerInvoke(in_meta, "ModifyValueColorA", in_color.A);
end


-- Взятие цвета тени строки имени
function This:ModifyNameShadowColor(in_meta, in_color)
	GGF.InnerInvoke(in_meta, "ModifyNameShadowColorR", in_color.R);
	GGF.InnerInvoke(in_meta, "ModifyNameShadowColorG", in_color.G);
	GGF.InnerInvoke(in_meta, "ModifyNameShadowColorB", in_color.B);
	GGF.InnerInvoke(in_meta, "ModifyNameShadowColorA", in_color.A);
end


-- Взятие цвета тени строки значения
function This:ModifyValueShadowColor(in_meta, in_color)
	GGF.InnerInvoke(in_meta, "ModifyValueShadowColorR", in_color.R);
	GGF.InnerInvoke(in_meta, "ModifyValueShadowColorG", in_color.G);
	GGF.InnerInvoke(in_meta, "ModifyValueShadowColorB", in_color.B);
	GGF.InnerInvoke(in_meta, "ModifyValueShadowColorA", in_color.A);
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Взятие цвета строки имени
function This:SetNameColor(in_meta, in_color)
	GGF.InnerInvoke(in_meta, "ModifyNameColor", in_color);
	GGF.InnerInvoke(in_meta, "AdjustNameColor", in_color);
end


-- Взятие цвета строки значения
function This:SetValueColor(in_meta, in_color)
	GGF.InnerInvoke(in_meta, "ModifyValueColor", in_color);
	GGF.InnerInvoke(in_meta, "AdjustValueColor", in_color);
end


-- Взятие цвета тени строки имени
function This:SetNameShadowColor(in_meta, in_color)
	GGF.InnerInvoke(in_meta, "ModifyNameShadowColor", in_color);
	GGF.InnerInvoke(in_meta, "AdjustNameShadowColor", in_color);
end


-- Взятие цвета тени строки значения
function This:SetValueShadowColor(in_meta, in_color)
	GGF.InnerInvoke(in_meta, "ModifyValueShadowColor", in_color);
	GGF.InnerInvoke(in_meta, "AdjustValueShadowColor", in_color);
end


-- Взятие цвета тени строки имени
function This:SetNameShadowOffset(in_meta, in_offset)
	GGF.InnerInvoke(in_meta, "ModifyNameShadowOffset", in_color);
	GGF.InnerInvoke(in_meta, "AdjustNameShadowColor", in_color);
end


-- Взятие цвета тени строки значения
function This:SetValueShadowOffset(in_meta, in_offset)
	GGF.InnerInvoke(in_meta, "ModifyValueShadowOffset", in_color);
	GGF.InnerInvoke(in_meta, "AdjustValueShadowColor", in_color);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие цвета строки имени
function This:GetNameColor(in_meta)
	local color = {
		R = GGF.InnerInvoke(in_meta, "GetNameColorR"),
		G = GGF.InnerInvoke(in_meta, "GetNameColorG"),
		B = GGF.InnerInvoke(in_meta, "GetNameColorB"),
		A = GGF.InnerInvoke(in_meta, "GetNameColorA"),
	};
	return color;
end


-- Взятие цвета строки значения
function This:GetValueColor(in_meta)
	local color = {
		R = GGF.InnerInvoke(in_meta, "GetValueColorR"),
		G = GGF.InnerInvoke(in_meta, "GetValueColorG"),
		B = GGF.InnerInvoke(in_meta, "GetValueColorB"),
		A = GGF.InnerInvoke(in_meta, "GetValueColorA"),
	};
	return color;
end


-- Взятие цвета тени строки имени
function This:GetNameShadowColor(in_meta)
	local color = {
		R = GGF.InnerInvoke(in_meta, "GetNameShadowColorR"),
		G = GGF.InnerInvoke(in_meta, "GetNameShadowColorG"),
		B = GGF.InnerInvoke(in_meta, "GetNameShadowColorB"),
		A = GGF.InnerInvoke(in_meta, "GetNameShadowColorA"),
	};
	return color;
end


-- Взятие цвета тени строки значения
function This:GetValueShadowColor(in_meta)
	local color = {
		R = GGF.InnerInvoke(in_meta, "GetValueShadowColorR"),
		G = GGF.InnerInvoke(in_meta, "GetValueShadowColorG"),
		B = GGF.InnerInvoke(in_meta, "GetValueShadowColorB"),
		A = GGF.InnerInvoke(in_meta, "GetValueShadowColorA"),
	};
	return color;
end


-- Взятие отступа тени строки имени
function This:GetNameShadowOffset(in_meta)
	local offset = {
		X = GGF.InnerInvoke(in_meta, "GetNameShadowOffsetX"),
		Y = GGF.InnerInvoke(in_meta, "GetNameShadowOffsetY"),
	};
	return offset;
end


-- Взятие отступа тени строки значения
function This:GetValueShadowOffset(in_meta)
	local offset = {
		X = GGF.InnerInvoke(in_meta, "GetValueShadowOffsetX"),
		Y = GGF.InnerInvoke(in_meta, "GetValueShadowOffsetY"),
	};
	return offset;
end


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Установка цвета строки имени
function This:AdjustNameColor(in_meta)
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetNameFontStringWrapper"),
		"SetColor",
		GGF.InnerInvoke(in_meta, "GetNameColor")
	);
end


-- Установка цвета строки значения
function This:AdjustValueColor(in_meta)
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetValueFontStringWrapper"),
		"SetColor",
		GGF.InnerInvoke(in_meta, "GetValueColor")
	);
end


-- Установка цвета тени строки имени
function This:AdjustNameShadowColor(in_meta)
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetNameFontStringWrapper"),
		"SetShadowColor",
		GGF.InnerInvoke(in_meta, "GetNameShadowColor")
	);
end


-- Установка цвета тени строки значения
function This:AdjustValueShadowColor(in_meta)
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetValueFontStringWrapper"),
		"SetShadowColor",
		GGF.InnerInvoke(in_meta, "GetValueShadowColor")
	);
end


-- Установка отступа тени строки имени
function This:AdjustNameShadowOffset(in_meta)
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetNameFontStringWrapper"),
		"SetShadowOffset",
		GGF.InnerInvoke(in_meta, "GetNameShadowOffset")
	);
end


-- Установка отступа тени строки значения
function This:AdjustValueShadowOffset(in_meta)
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetValueFontStringWrapper"),
		"SetShadowOffset",
		GGF.InnerInvoke(in_meta, "GetValueShadowOffset")
	);
end


-- Установка основных объектов табличного фрейма
function This:adjustServiceObjects(in_meta)
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetNameFontStringWrapper"),
		"Adjust"
	);
	GGF.OuterInvoke(
		GGF.InnerInvoke(in_meta, "GetValueFontStringWrapper"),
		"Adjust"
	);
end


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-- Синхронизация цвета строки имени
function This:SynchronizeNameColor(in_meta)
	local nfsw = GGF.InnerInvoke(in_meta, "GetNameFontStringWrapper");
	GGF.OuterInvoke(nfsw, "SynchronizeColor");
	GGF.InnerInvoke(in_meta, "ModifyNameColor", GGF.OuterInvoke(nfsw, "GetColor"));
end


-- Синхронизация цвета строки значения
function This:SynchronizeValueColor(in_meta)
	local vfsw = GGF.InnerInvoke(in_meta, "GetValueFontStringWrapper");
	GGF.OuterInvoke(vfsw, "SynchronizeColor");
	GGF.InnerInvoke(in_meta, "ModifyValueColor", GGF.OuterInvoke(vfsw, "GetColor"));
end


-- Синхронизация цвета тени строки имени
function This:SynchronizeNameShadowColor(in_meta)
	local nfsw = GGF.InnerInvoke(in_meta, "GetNameFontStringWrapper");
	GGF.OuterInvoke(nfsw, "SynchronizeShadowColor");
	GGF.InnerInvoke(in_meta, "ModifyNameShadowColor", GGF.OuterInvoke(nfsw, "GetShadowColor"));
end


-- Синхронизация цвета тени строки значения
function This:SynchronizeValueShadowColor(in_meta)
	local vfsw = GGF.InnerInvoke(in_meta, "GetValueFontStringWrapper");
	GGF.OuterInvoke(vfsw, "SynchronizeShadowColor");
	GGF.InnerInvoke(in_meta, "ModifyValueShadowColor", GGF.OuterInvoke(vfsw, "GetShadowColor"));
end


-- Синхронизация отступа тени строки имени
function This:SynchronizeNameShadowOffset(in_meta)
	local nfsw = GGF.InnerInvoke(in_meta, "GetNameFontStringWrapper");
	GGF.OuterInvoke(nfsw, "SynchronizeShadowOffset");
	GGF.InnerInvoke(in_meta, "ModifyNameShadowOffset", GGF.OuterInvoke(nfsw, "GetShadowOffset"));
end


-- Синхронизация отступа тени строки значения
function This:SynchronizeValueShadowOffset(in_meta)
	local vfsw = GGF.InnerInvoke(in_meta, "GetValueFontStringWrapper");
	GGF.OuterInvoke(vfsw, "SynchronizeShadowOffset");
	GGF.InnerInvoke(in_meta, "ModifyValueShadowOffset", GGF.OuterInvoke(vfsw, "GetShadowOffset"));
end


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Создание основных объектов табличного фрейма
function This:createServiceObjects(in_meta)
	local parentFrame = in_meta.object;
	GGF.InnerInvoke(in_meta, "SetNameFontStringWrapperOD", GGC.FontString:Create({}, {
		properties = {
			base = {
				parent = parentFrame,
				-- NOTE: вот это странно...
				--template = in_wrapper:GetTemplate(),
				wrapperName = "N",
			},
			miscellaneous = {
				layer = GGE.LayerType.Artwork,
				text = GGF.InnerInvoke(in_meta, "GetName"),
				tooltipText = GGF.InnerInvoke(in_meta, "GetNameTooltip"),
				fontHeight = GGF.InnerInvoke(in_meta, "GetFontHeight"),
			},
			size = {
				-- NOTE: ширина опеределяется дальше, в lrPercentage
				height = GGF.InnerInvoke(in_meta, "GetHeight"),
			},
			anchor = {
				point = GGE.PointType.Right,
				relativeTo = parentFrame,
				relativePoint = GGE.PointType.Center,--self:GetRelativePoint(),
				offsetX = 0,--self:GetOffsetX(),
				offsetY = 0,--self:GetOffsetY(),
				justifyH = GGE.JustifyHType.Right,
				justifyV = GGF.InnerInvoke(in_meta, "GetJustifyV"),
			},
			color = {
				R = GGF.InnerInvoke(in_meta, "GetNameColorR"),
				G = GGF.InnerInvoke(in_meta, "GetNameColorG"),
				B = GGF.InnerInvoke(in_meta, "GetNameColorB"),
				A = GGF.InnerInvoke(in_meta, "GetNameColorA"),
			},
			shadow = {
				color = {
					R = GGF.InnerInvoke(in_meta, "GetNameShadowColorR"),
					G = GGF.InnerInvoke(in_meta, "GetNameShadowColorG"),
					B = GGF.InnerInvoke(in_meta, "GetNameShadowColorB"),
					A = GGF.InnerInvoke(in_meta, "GetNameShadowColorA"),
				},
				offset = {
					X = GGF.InnerInvoke(in_meta, "GetNameShadowOffsetX"),
					Y = GGF.InnerInvoke(in_meta, "GetNameShadowOffsetY"),
				},
			},
		},
	}));

	GGF.InnerInvoke(in_meta, "SetValueFontStringWrapperOD", GGC.FontString:Create({}, {
		properties = {
			base = {
				parent = parentFrame,
				wrapperName = "V",
			},
			miscellaneous = {
				layer = GGE.LayerType.Artwork,
				text = GGF.InnerInvoke(in_meta, "GetValue"),
				tooltipText = GGF.InnerInvoke(in_meta, "GetValueTooltip"),
				fontHeight = GGF.InnerInvoke(in_meta, "GetFontHeight"),
			},
			size = {
				-- NOTE: ширина опеределяется дальше, в lrPercentage
				height = GGF.InnerInvoke(in_meta, "GetHeight"),
			},
			anchor = {
				point = GGE.PointType.Left,
				relativeTo = parentFrame,
				relativePoint = GGE.PointType.Center,--self:GetRelativePoint(),
				offsetX = 0,--self:GetOffsetX(),
				offsetY = 0,--self:GetOffsetY(),
				justifyH = GGE.JustifyHType.Left,
				justifyV = GGF.InnerInvoke(in_meta, "GetJustifyV"),
			},
			color = {
				R = GGF.InnerInvoke(in_meta, "GetValueColorR"),
				G = GGF.InnerInvoke(in_meta, "GetValueColorG"),
				B = GGF.InnerInvoke(in_meta, "GetValueColorB"),
				A = GGF.InnerInvoke(in_meta, "GetValueColorA"),
			},
			shadow = {
				color = {
					R = GGF.InnerInvoke(in_meta, "GetValueShadowColorR"),
					G = GGF.InnerInvoke(in_meta, "GetValueShadowColorG"),
					B = GGF.InnerInvoke(in_meta, "GetValueShadowColorB"),
					A = GGF.InnerInvoke(in_meta, "GetValueShadowColorA"),
				},
				offset = {
					X = GGF.InnerInvoke(in_meta, "GetValueShadowOffsetX"),
					Y = GGF.InnerInvoke(in_meta, "GetValueShadowOffsetY"),
				},
			},
		},
	}));
end