-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

local OBJSuffix = "BDM";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   BaseDataMeterSuffix = GGF.DockDomName(GGD.DefineIdentifier, "BaseDataMeterSuffix"),
   BaseDataMeter = GGF.DockDomName(GGD.ClassIdentifier, "BaseDataMeter"),
};

local CTList = {
   Value = GGF.CreateCT(OBJSuffix, "Value", "Value"),
   Average = GGF.CreateCT(OBJSuffix, "Average", "Value"),
   MeterUnits = GGF.CreateCT(OBJSuffix, "MeterUnits", "MeterUnits"),
   MinLimit = GGF.CreateCT(OBJSuffix, "MinLimit", "MinLimit"),
   MaxLimit = GGF.CreateCT(OBJSuffix, "MaxLimit", "MaxLimit"),
};

GGF.GRegister(DFList.BaseDataMeterSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
   Value = 0,
   MeterUnits = "u/s",
   MinLimit = 0,            -- NOTE: абс.
   MaxLimit = 100,       -- NOTE: абс.
}, GGE.RegValType.Default);

GGF.GRegister(DFList.BaseDataMeter, {
   meta = {
      name = "Base Data Meter",
      mark = "",  -- авт.
      suffix = GGD.BaseDataMeterSuffix,
   },
});

GGC.BaseDataMeter.objects = {};

GGC.BaseDataMeter.wrappers = {};

GGC.BaseDataMeter.properties = {
   miscellaneous = {
      absValue = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.Value,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Setter] = function(in_meta, in_val)
            -- HACK: проброс оверрайдов(по умолчанию работать нифига не будет)
            GGF.InnerInvoke(in_meta, "ModifyValue", in_val);
            GGF.InnerInvoke(in_meta, "AdjustValue");
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      avgValue = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.Average,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Setter] = function(in_meta, in_val)
            GGF.InnerInvoke(in_meta, "ModifyAverage", in_val);
            GGF.InnerInvoke(in_meta, "AdjustAverage");
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
   hardware = {
      meterUnits = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.MeterUnits,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      limits = {
         min = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.MinLimit,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Setter] = function(in_meta, in_val)
               GGF.InnerInvoke(in_meta, "ModifyMinLimit", in_val);
               GGF.InnerInvoke(in_meta, "AdjustMinLimit");
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         max = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.MaxLimit,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Setter] = function(in_meta, in_val)
               GGF.InnerInvoke(in_meta, "ModifyMaxLimit", in_val);
               GGF.InnerInvoke(in_meta, "AdjustMaxLimit");
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
      },
   },
};

GGC.BaseDataMeter.elements = {};

GGF.Mark(GGC.BaseDataMeter);

GGC.BaseDataMeter = GGF.TableSafeExtend(GGC.BaseDataMeter, GGC.StandardFrame);

GGF.CreateClassWorkaround(GGC.BaseDataMeter);

local This = GGC.BaseDataMeter;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------