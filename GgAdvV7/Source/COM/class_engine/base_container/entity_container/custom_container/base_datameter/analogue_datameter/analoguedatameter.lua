-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "ADM";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	ScaleType = GGF.DockDomName(GGD.EnumerationIdentifier, "ScaleType"),
	AnalogueDataMeterSuffix = GGF.DockDomName(GGD.DefineIdentifier, "AnalogueDataMeterSuffix"),
	AnalogueDataMeter = GGF.DockDomName(GGD.ClassIdentifier, "AnalogueDataMeter"),
};

local CTList = {
	AnalogueDataMeter = GGF.CreateCT(OBJSuffix, "AnalogueDataMeter", "AnalogueDataMeter"),
	MFile = GGF.CreateCT(OBJSuffix, "MFile", "MFile"),
	AFile = GGF.CreateCT(OBJSuffix, "AFile", "AFile"),
	ReductionList = GGF.CreateCT(OBJSuffix, "ReductionList", "ReductionList"),
	ScaleType = GGF.CreateCT(OBJSuffix, "ScaleType", "ScaleType"),
	ScaleBase = GGF.CreateCT(OBJSuffix, "ScaleBase", "ScaleBase"),
	RotationSpeed = GGF.CreateCT(OBJSuffix, "RotationSpeed", "ArrowRotationSpeed"),
	Initialized = GGF.CreateCT(OBJSuffix, "Initialized", "Initialized"),
	SubObjectList = GGF.CreateCT(OBJSuffix, "SubObjectList", "SubObjectList"),
	TargetAngle = GGF.CreateCT(OBJSuffix, "TargetAngle", "TargetAngle"),
	CurrentAngle = GGF.CreateCT(OBJSuffix, "CurrentAngle", "CurrentAngle"),
	CurrentSpeed = GGF.CreateCT(OBJSuffix, "CurrentSpeed", "CurrentSpeed"),
	MaximumSpeed = GGF.CreateCT(OBJSuffix, "MaximumSpeed", "MaximumSpeed"),
	AnimationAcceleration = GGF.CreateCT(OBJSuffix, "AnimationAcceleration", "AnimationAcceleration"),
	AnimationStep = GGF.CreateCT(OBJSuffix, "AnimationStep", "AnimationStep"),
	IsMoving = GGF.CreateCT(OBJSuffix, "IsMoving", "IsMoving"),
};

GGF.GRegister(DFList.AnalogueDataMeterSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.ScaleType, {
	Linear = "LINEAR",
	Indicative = "INDICATIVE",
	Exponental = "EXPONENTIAL",
});

GGF.GRegister(DFList.Default, {
	AnalogueDataMeter = 0,
	MFile = "",						   -- NOTE: meter file(текстура)
	AFile = "",						   -- NOTE: arrow file(текстура)
	ReductionList = {},
	ScaleType = GGE.ScaleType.Linear,
	ScaleBase = 1,
	ArrowRotationSpeed = 5,			-- NOTE: град/сек
	Initialized = false,			   -- NOTE: ну да....логично...
	SubObjectList = {},
	TargetAngle = 0.0,				-- NOTE: тут можно посчитать угол опережения
	CurrentAngle = 0.0,
	CurrentSpeed = 0.0,
	MaximumSpeed = 45.0,			   -- NOTE: град./сек.
	AnimationAcceleration = 45.0, -- NOTE: град./(сек.^2)
	AnimationStep = 0.1,			   -- NOTE: сек.
	IsMoving = false,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.AnalogueDataMeter, {
	meta = {
		name = "Analogue Data Meter",
		mark = "",	-- авт.
		suffix = GGD.AnalogueDataMeterSuffix,
	},
});

GGC.AnalogueDataMeter.objects = {
	frame = {
		standard = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.AnalogueDataMeter,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.AnalogueDataMeter.Default)) then
					local analogueDataMeter = CreateFrame(
						"Frame",
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.InnerInvoke(in_meta, "GetParent"),
                  GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
						   {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyAnalogueDataMeterOD", analogueDataMeter);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.AnalogueDataMeter.wrappers = {
	subObjectList = GGC.ParameterContainer:Create({
		ctype = GGE.ContainerType.Element,
		cte = CTList.SubObjectList,
		fixed = true,

		[GGE.RuntimeMethod.Adjuster] = function(in_meta)
		end,
		[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
		end,
	}),
};

GGC.AnalogueDataMeter.properties = {
	hardware = {
   		meterFile = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.MFile,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		arrowFile = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.AFile,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		-- NOTE: нужно для нестандартного сокращения
		reductionList = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ReductionList,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		scaleType = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ScaleType,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		-- NOTE: для показательной скорее всего(д.б. 2)
		scaleBase = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.ScaleBase,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		limits = {
			rspd = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.RotationSpeed,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
	},
};

GGC.AnalogueDataMeter.elements = {
	-- NOTE: не устанавливать эту структуру!!!
	runtime = {
		targetAngle = GGC.ParameterContainer:Create({		-- значение угла по величине
			ctype = GGE.ContainerType.Element,
			cte = CTList.TargetAngle,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		currentAngle = GGC.ParameterContainer:Create({		-- положение стрелки(град.)
			ctype = GGE.ContainerType.Element,
			cte = CTList.CurrentAngle,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		currentSpeed = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Element,
			cte = CTList.CurrentSpeed,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		maximumSpeed = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Element,
			cte = CTList.MaximumSpeed,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		animationAcceleration = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Element,
			cte = CTList.AnimationAcceleration,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		animationStep = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Element,
			cte = CTList.AnimationStep,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		isMoving = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Element,
			cte = CTList.IsMoving,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGF.Mark(GGC.AnalogueDataMeter);

GGC.AnalogueDataMeter = GGF.TableSafeExtend(GGC.AnalogueDataMeter, GGC.BaseDataMeter);

GGF.CreateClassWorkaround(GGC.AnalogueDataMeter);

local This = GGC.AnalogueDataMeter;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-- Установка среднего значения в доп. поле
function This:SetAverageValue(in_meta, in_value)
	GGF.InnerInvoke(in_meta, "GetSubObjectList").avg:SetText(GGF.StructColorToStringColor(GGD.Color.Title) .. "avg:" .. GGF.FlushStringColor() .. in_value);
end


function This:setCurrentValue(in_meta, in_value)
	GGF.InnerInvoke(in_meta, "GetSubObjectList").cur:SetText(GGF.StructColorToStringColor(GGD.Color.Title) .. "cur:" .. GGF.FlushStringColor() .. GGF.StructColorToStringColor(GGD.Color.Neutral) .. GGF.InnerInvoke(in_meta, "valToLabel", in_value) .. " " .. GGF.InnerInvoke(in_meta, "GetMeterUnits") .. GGF.FlushStringColor())
end

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие габаритов объекта
function This:GetSize(in_meta)
	local size = {
		width = GGF.InnerInvoke(in_meta, "GetWidth"),
		height = GGF.InnerInvoke(in_meta, "GetHeight"),
	};
	return size;
end


-- Взятие точки привязки(якоря) объекта
function This:GetAnchor(in_meta)
	local anchor = {
		point = GGF.InnerInvoke(in_meta, "GetPoint"),
		relativeTo = GGF.InnerInvoke(in_meta, "GetRelativeTo"),
		relativePoint = GGF.InnerInvoke(in_meta, "GetRelativePoint"),
		offsetX = GGF.InnerInvoke(in_meta, "GetOffsetX"),
		offsetY = GGF.InnerInvoke(in_meta, "GetOffsetY"),
	};
	return anchor;
end


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-- Оверрайд установки текущего значения
function This:AdjustValue(in_meta)
   local value = GGF.InnerInvoke(in_meta, "GetValue");
   local nwDeg = GGF.InnerInvoke(in_meta, "valToAngle", value);
   GGF.InnerInvoke(in_meta, "SetTargetAngle", nwDeg);
   if (not GGF.InnerInvoke(in_meta, "GetIsMoving")) then
      GGF.InnerInvoke(in_meta, "acceptNextMove");
   end
   GGF.InnerInvoke(in_meta, "setCurrentValue", value);
end

-- Установка основных объектов измерителя
function This:adjustServiceObjects(in_meta)
end


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- TODO: затестить
-- Создание основных объектов измерителя
function This:createServiceObjects(in_meta)
	local subobjlist = {
		meter = {},
		valfss = { count = 10 },
		arrow = {},
		units = {},	-- единицы измерения
		avg = {},
		cur = {},
	};
	local yOffset = -5;
	local extender = 30;		-- множитель увеличения текстуры(по умолчанию кусок табло болтается слева наверху)
	local arrowReducer = 25;--40;	-- уменьшитель тектуры стрелки(стрелка должна почти касаться рисок)
	local step = 20;
	local dialAngle = 120;		-- угол спидометра
	local sz = GGF.InnerInvoke(in_meta, "GetSize");
	local cx, cy = (sz.width + extender)/2, yOffset - (sz.height  + extender)/2;	-- точка крепления стрелки(оси поворота)
	local rad = sz.width/2 + 10;
	local min, max = GGF.InnerInvoke(in_meta, "GetMinLimit"), GGF.InnerInvoke(in_meta, "GetMaxLimit");
	local prefix = GGF.GenerateOTName(GGF.InnerInvoke(in_meta, "GetParentName"), GGF.InnerInvoke(in_meta, "GetWrapperName"), GGF.InnerInvoke(in_meta, "getClassSuffix"));
	local rotateExtender = 2^0.5;

	-- NOTE: на один больше, т.к. должны быть метки начала и конца
	for i = 1,(dialAngle/step + 1) do
		local deltaAngleGrad = (i - 1)*step;
		local deltaAngleRad = deltaAngleGrad*math.pi/180.0;
		subobjlist.valfss[#subobjlist.valfss + 1] = in_meta.object:CreateFontString(
			prefix .. "_VAL_FS_" .. tostring(#subobjlist.valfss + 1),
			"OVERLAY",
			"GameFontNormalSmall"
		);
		subobjlist.valfss[#subobjlist.valfss]:SetPoint(
			GGE.PointType.Center,
			in_meta.object,
			GGE.PointType.TopLeft,
			cx - rad*math.cos(deltaAngleRad),
			cy + rad*math.sin(deltaAngleRad)
		);

		subobjlist.valfss[#subobjlist.valfss]:SetText(
			GGF.StructColorToStringColor(GGD.Color.Title) .. GGF.InnerInvoke(in_meta, "valToLabel", GGF.InnerInvoke(in_meta, "angleToVal", deltaAngleGrad)) .. GGF.FlushStringColor()
		);	-- пока так
		--subobjlist.valfss[#subobjlist.valfss]:SetTextColor(0.46, 0.86, 0.90, 1);
	end

	subobjlist.meter = in_meta.object:CreateTexture(
		prefix .. "_MTR_T_",
		GGE.LayerType.Overlay
	);
	subobjlist.meter:SetTexture(GGF.InnerInvoke(in_meta, "GetMFile"));
	subobjlist.meter:SetPoint(
		GGE.PointType.TopLeft,
		in_meta.object,
		GGE.PointType.TopLeft,
		0,
		yOffset
	);
	subobjlist.meter:SetSize(
		sz.width + extender,
		sz.height + extender
	);
	subobjlist.meter:Show();
	local arwd, arhg = (sz.width + extender - 2*arrowReducer)*rotateExtender, (sz.height + extender - 2*arrowReducer)*rotateExtender;
	subobjlist.arrow = in_meta.object:CreateTexture(
		prefix .. "_ARR_T_",
		GGE.LayerType.Overlay
	);
	subobjlist.arrow:SetTexture(GGF.InnerInvoke(in_meta, "GetAFile"));
	--subobjlist.arrow:SetPoint("CENTER", self:GetObject(), "TopLeft", arwd/2, - arhg/2);
	subobjlist.arrow:SetPoint(
		GGE.PointType.Center,
		subobjlist.meter,
		GGE.PointType.Center,
		0,
		0
	);
	subobjlist.arrow:SetSize(arwd, arhg);
	subobjlist.arrow:Show();
	subobjlist.arrow:SetRotation(math.rad(90));

	subobjlist.units = in_meta.object:CreateFontString(
		prefix .. "_U_FS_",
		GGE.LayerType.Artwork,
		"GameFontNormalSmall"
	);
	--subobjlist.units:SetPoint("CENTER", self:GetObject(), "TopLeft", arwd/2, - arhg/2 + arhg/6);
	subobjlist.units:SetPoint(
		GGE.PointType.Center,
		subobjlist.meter,
		GGE.PointType.Center,
		0,
		0 + arhg/6
	);
	subobjlist.units:SetText(
		GGF.StructColorToStringColor(GGD.Color.Title) .. GGF.InnerInvoke(in_meta, "GetMeterUnits") .. GGF.FlushStringColor()
	);

	subobjlist.avg = in_meta.object:CreateFontString(
		prefix .. "_AVG_FS_",
		GGE.LayerType.Artwork,
		"GameFontNormalSmall"
	);
	--subobjlist.avg:SetPoint("CENTER", self:GetObject(), "TopLeft", arwd/2, - arhg/2 - arhg/7);
	subobjlist.avg:SetPoint(
		GGE.PointType.Center,
		subobjlist.meter,
		GGE.PointType.Center,
		0,
		0 - arhg/5.2
	);
	subobjlist.avg:SetText(
		GGF.StructColorToStringColor(GGD.Color.Title) .. "avg: " .. GGF.FlushStringColor()
	);

	subobjlist.cur = in_meta.object:CreateFontString(
		prefix .. "_CUR_FS_",
		GGE.LayerType.Artwork,
		"GameFontNormalSmall"
	);

	subobjlist.cur:SetPoint(
		GGE.PointType.Center,
		subobjlist.meter,
		GGE.PointType.Center,
		0,
		0 - arhg/8
	);
	subobjlist.cur:SetText(
		GGF.StructColorToStringColor(GGD.Color.Title) .. "cur: " .. GGF.FlushStringColor()
	);
	--print(cx, cy, (sz.width + extender)/2, - (sz.height + extender)/2, (sz.width + extender - 2*arrowReducer)/2 + arrowReducer, - (sz.height + extender - 2*arrowReducer)/2 - arrowReducer);

	GGF.InnerInvoke(in_meta, "SetSubObjectListOD", subobjlist);

	-- NOTE: установка дефолтного значения строго после
	GGF.InnerInvoke(in_meta, "SetAverageValue", GGF.StructColorToStringColor(GGD.Color.Neutral) .. "0 " .. GGF.InnerInvoke(in_meta, "GetMeterUnits") .. GGF.FlushStringColor());
	GGF.InnerInvoke(in_meta, "setCurrentValue", 0);
end


-- Преобразование значения в угол измерителя
function This:valToAngle(in_meta, in_value)
	local value;
	local min, max = GGF.InnerInvoke(in_meta, "GetMinLimit"), GGF.InnerInvoke(in_meta, "GetMaxLimit");
	local sType = GGF.InnerInvoke(in_meta, "GetScaleType");

	if (sType == GGE.ScaleType.Linear) then
		value = in_value;
	elseif (sType == GGE.ScaleType.Indicative) then
		value = math.log(in_value)/math.log(GGF.InnerInvoke(in_meta, "GetScaleBase"));
	elseif (sType == GGE.ScaleType.Exponental) then
		value = math.log(in_value);
	end

	if (value < min) then
		value = min;
	end
	if (value > max) then
		value = max;
	end

	local sType = GGF.InnerInvoke(in_meta, "GetScaleType");

	-- Значение 0, угол тоже 0
	return value/((max - min)/120.0);
end


-- Преобразование угла в значение измерителя
function This:angleToVal(in_meta, in_angle)
	local min, max = GGF.InnerInvoke(in_meta, "GetMinLimit"), GGF.InnerInvoke(in_meta, "GetMaxLimit");
	local sType = GGF.InnerInvoke(in_meta, "GetScaleType");
	local step = 20;

	if (sType == GGE.ScaleType.Linear) then
		value = min + in_angle*(max - min)/120.0;
	elseif (sType == GGE.ScaleType.Indicative) then
		if (in_angle < step) then
			value = in_angle*(math.pow(GGF.InnerInvoke(in_meta, "GetScaleBase"), 0))/step;
		else
			value = math.pow(GGF.InnerInvoke(in_meta, "GetScaleBase"), (in_angle - step)*(max - min)/(120.0 - step));
		end
	elseif (sType == GGE.ScaleType.Exponental) then
		-- NOTE: тут все сложно и не работает пока что
		value = math.exp((in_angle - step)*(max - min)/(120.0 - step));
	end

	return value;
end


-- Преобразование значения в строку для табла измерителя
function This:valToLabel(in_meta, in_value)
	local redList = GGF.InnerInvoke(in_meta, "GetReductionList");
	local sType = GGF.InnerInvoke(in_meta, "GetScaleType");

	local stsVal = in_value;
	local lStr = "";
	local idx = 0;
	if (sType == GGE.ScaleType.Linear) then
		while (stsVal >= 1000) do
			stsVal = stsVal/1000.0;
			lStr = lStr .. "K";
			idx = idx + 1;
		end
		if (#redList > idx  and idx > 0) then
			lStr = redList[idx];
		end
	elseif (sType == GGE.ScaleType.Indicative) then
		local redPower = math.ceil(math.log(1000)/math.log(GGF.InnerInvoke(in_meta, "GetScaleBase")));
		local multiplier = math.pow(GGF.InnerInvoke(in_meta, "GetScaleBase"), redPower);
		while (stsVal >= multiplier) do
			stsVal = stsVal/multiplier;
			lStr = lStr .. "K";
			idx = idx + 1;
		end
		if (#redList > idx and idx > 0) then
			lStr = redList[idx];
		end
	elseif (sType == GGE.ScaleType.Exponental) then
		-- NOTE: не работает
	end

	return string.format("%d", stsVal) .. lStr;
end


-- Установка текущего значения
function This:acceptNextMove(in_meta)
	GGF.InnerInvoke(in_meta, "SetIsMoving", true);
	local tAngle, cAngle = GGF.InnerInvoke(in_meta, "GetTargetAngle"), GGF.InnerInvoke(in_meta, "GetCurrentAngle");
	local cSpd, mSpd = GGF.InnerInvoke(in_meta, "GetCurrentSpeed"), GGF.InnerInvoke(in_meta, "GetMaximumSpeed");
	local anStep = GGF.InnerInvoke(in_meta, "GetAnimationStep");
	local mAcc = GGF.InnerInvoke(in_meta, "GetAnimationAcceleration");
	local accel = 0.0;

	-- NOTE: варианта, когда оба угла равны - просто стоим
	if (tAngle == cAngle) then
		GGF.InnerInvoke(in_meta, "SetIsMoving", false);
		return;
	end

	local timeToStop = math.abs(cSpd)/mAcc;
	local angleToStop = math.abs(cSpd)*timeToStop - (mAcc*timeToStop^2.0)/2.0;

	if (tAngle > cAngle) then			-- стрелка ниже реального значения
		-- NOTE: проверка на необходимость торможения
		if (tAngle - cAngle < angleToStop) then
			accel = - mAcc;
		elseif (cSpd < mSpd) then
			accel = mAcc;
		end
	else
		if (cAngle - tAngle < angleToStop) then
			accel = mAcc;
		elseif (cSpd > - mSpd) then
			accel = - mAcc;
		end
	end

	GGF.InnerInvoke(in_meta, "SetCurrentSpeed", cSpd + accel*anStep);
	local rtAngle = GGF.InnerInvoke(in_meta, "GetCurrentSpeed")*anStep;
	local cNext = true;
	-- NOTE: при этом условии следующий мув перегпрыгнет цель
	if ((cAngle + rtAngle - tAngle)*(cAngle - tAngle) <= 0) then
		rtAngle = tAngle - cAngle;
		cNext = false;
	end
	GGF.InnerInvoke(in_meta, "SetCurrentAngle", GGF.InnerInvoke(in_meta, "GetCurrentAngle") + rtAngle);

	local ra = GGC.RotateAnimation:Create({
		properties = {
			base = {
				--wrapperName = "RA",
				target = GGF.InnerInvoke(in_meta, "GetSubObjectList").arrow,
			},
			parameters = {
				time = anStep,
				startAngle = cAngle,
				finishAngle = rtAngle,
			},
		},
	}, {});
	GGF.OuterInvoke(ra, "Launch");

	if (cNext) then
		GGC.TriggerTimer:Create({
			time = anStep,
			callback = function()
				GGF.InnerInvoke(in_meta, "acceptNextMove")
			end,
		});
	else
		GGF.InnerInvoke(in_meta, "SetIsMoving", false);
		GGF.InnerInvoke(in_meta, "SetCurrentSpeed", 0);
	end
end