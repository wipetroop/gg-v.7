-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

local OBJSuffix = "BRDM";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   DataMeterBarType = GGF.DockDomName(GGD.EnumerationIdentifier, "DataMeterBarType"),
   DataMeterMetaSideType = GGF.DockDomName(GGD.EnumerationIdentifier, "DataMeterMetaSideType"),
   BarDataMeterSuffix = GGF.DockDomName(GGD.DefineIdentifier, "BarDataMeterSuffix"),
   BarDataMeter = GGF.DockDomName(GGD.ClassIdentifier, "BarDataMeter"),
};

local CTList = {
   BarDataMeter = GGF.CreateCT(OBJSuffix, "BarDataMeter", "Frame"),
   SideBorderOne = GGF.CreateCT(OBJSuffix, "SideBorderOne", "Frame"),
   SideBorderTwo = GGF.CreateCT(OBJSuffix, "SideBorderTwo", "Frame"),
   ButtBorder = GGF.CreateCT(OBJSuffix, "ButtBorder", "Frame"),
   BarShellList = GGF.CreateCT(OBJSuffix, "BarShellList", "BarShellList"),
   MinLabel = GGF.CreateCT(OBJSuffix, "MinLabel", "Frame"),
   MaxLabel = GGF.CreateCT(OBJSuffix, "MaxLabel", "Frame"),
   AverageLabel = GGF.CreateCT(OBJSuffix, "AverageLabel", "Frame"),
   CurrentLabel = GGF.CreateCT(OBJSuffix, "CurrentLabel", "Frame"),
   DataMeterBarType = GGF.CreateCT(OBJSuffix, "DataMeterBarType", "DataMeterBarType"),
   DataMeterMetaSideType = GGF.CreateCT(OBJSuffix, "DataMeterMetaSideType", "DataMeterMetaSideType"),
   ShellCount = GGF.CreateCT(OBJSuffix, "ShellCount", "ShellCount"),
   BorderWidth = GGF.CreateCT(OBJSuffix, "BorderWidth", "BorderWidth"),
   ObjectServiceGap = GGF.CreateCT(OBJSuffix, "ObjectServiceGap", "ObjectServiceGap"),
   MetaDataGap = GGF.CreateCT(OBJSuffix, "MetaDataGap", "MetaDataGap"),
   CrossShellGap = GGF.CreateCT(OBJSuffix, "CrossShellGap", "CrossShellGap"),
   LabelGap = GGF.CreateCT(OBJSuffix, "LabelGap", "LabelGap"),
   MovingSpeedForward = GGF.CreateCT(OBJSuffix, "MovingSpeedForward", "MovingSpeed"),
   MovingSpeedBackward = GGF.CreateCT(OBJSuffix, "MovingSpeedBackward", "MovingSpeed"),
   TargetMeterValue = GGF.CreateCT(OBJSuffix, "TargetMeterValue", "DMeterValue"),
   CurrentMeterValue = GGF.CreateCT(OBJSuffix, "CurrentMeterValue", "DMeterValue"),
   IsMoving = GGF.CreateCT(OBJSuffix, "IsMoving", "IsMoving"),
   VisibleShellAmount = GGF.CreateCT(OBJSuffix, "VisibleShellAmount", "VisibleShellAmount"),
   AnimationStep = GGF.CreateCT(OBJSuffix, "AnimationStep", "AnimationStep"),
   GapList = GGF.CreateCT(OBJSuffix, "GapList", "GapList"),
   AnchorList = GGF.CreateCT(OBJSuffix, "AnchorList", "AnchorList"),
};

GGF.GRegister(DFList.BarDataMeterSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.DataMeterBarType, {
   DownDirected = "DOWN_DIRECTED",
   UpDirected = "UP_DIRECTED",
   LeftDirected = "LEFT_DIRECTED",
   RightDirected = "RIGHT_DIRECTED",
});

GGF.GRegister(DFList.DataMeterMetaSideType, {
   Left = "LEFT",
   Right = "RIGHT",
   Up = "UP",
   Down = "DOWN",
});

GGF.GRegister(DFList.Default, {
   Frame = 0,
   BarShellList = {},
   DataMeterBarType = GGE.DataMeterBarType.RightDirected,
   DataMeterMetaSideType = GGE.DataMeterMetaSideType.Down,
   ShellCount = 10,
   BorderWidth = 2,
   ObjectServiceGap = 8,
   MetaDataGap = 50,
   CrossShellGap = 2,
   LabelGap = 10,
   MovingSpeed = 0.02,     -- NOTE: измеряется в фулбарах в секунду
   DMeterValue = 0,
   IsMoving = false,
   VisibleShellAmount = 0,
   AnimationStep = 0.02,    -- NOTE: сек.
   GapList = {},
   AnchorList = {},
}, GGE.RegValType.Default);

GGF.GRegister(DFList.BarDataMeter, {
   meta = {
      name = "Bar Data Meter",
      mark = "",  -- авт.
      suffix = GGD.BarDataMeterSuffix,
   },
});

GGC.BarDataMeter.objects = {
   frame = {
      standard = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Object,
         cte = CTList.BarDataMeter,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.BarDataMeter.Default)) then
               local barDataMeter = CreateFrame(
                  "Frame",
                  GGF.GenerateOTName(
                     GGF.InnerInvoke(in_meta, "GetParentName"),
                     GGF.InnerInvoke(in_meta, "GetWrapperName"),
                     GGF.InnerInvoke(in_meta, "getClassSuffix")
                  ),
                  GGF.InnerInvoke(in_meta, "GetParent"),
                  GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end
                     }
                  })
               );
               GGF.InnerInvoke(in_meta, "ModifyBarDataMeterOD", barDataMeter);
            end
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.BarDataMeter.wrappers = {
   borders = {
      sideBorderOne = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.SideBorderOne,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      sideBorderTwo = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.SideBorderTwo,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      buttBorder = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.ButtBorder,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
   filler = {
      barShellList = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.BarShellList,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
   labels = {
      min = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.MinLabel,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      max = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.MaxLabel,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      average = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.AverageLabel,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      current = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.CurrentLabel,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.BarDataMeter.properties = {
   hardware = {
      miscellaneous = {
         dataMeterBarType = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.DataMeterBarType,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         dataMeterMetaSideType = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.DataMeterMetaSideType,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         shellCount = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.ShellCount,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
      },
      geometry = {
         borderWidth = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.BorderWidth,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         objectServiceGap = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.ObjectServiceGap,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         metaDataGap = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.MetaDataGap,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         crossShellGap = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.CrossShellGap,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         labelGap = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.LabelGap,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
      },
      limits = {
         mspdfwd = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.MovingSpeedForward,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         mspdbwd = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.MovingSpeedBackward,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
      },
   },
};

GGC.BarDataMeter.elements = {
   runtime = {
      targetMeterValue = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.TargetMeterValue,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      currentMeterValue = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.CurrentMeterValue,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      isMoving = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.IsMoving,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      -- NOTE: количество активных шеллов
      visibleShellAmount = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.VisibleShellAmount,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Setter] = function(in_meta, in_val)
            local shellAmount = GGF.InnerInvoke(in_meta, "valueToShellAmount", in_val);
            GGF.InnerInvoke(in_meta, "ModifyVisibleShellAmount", shellAmount);
            GGF.InnerInvoke(in_meta, "AdjustVisibleShellAmount");
            GGF.InnerInvoke(in_meta, "resetMeterValue");
         end,
      }),
      animationStep = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.AnimationStep,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      geometry = {
         gapList = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.GapList,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         anchorList = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Property,
            cte = CTList.AnchorList,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
      },
   },
};

GGF.Mark(GGC.BarDataMeter);

GGC.BarDataMeter = GGF.TableSafeExtend(GGC.BarDataMeter, GGC.BaseDataMeter);

GGF.CreateClassWorkaround(GGC.BarDataMeter);

local This = GGC.BarDataMeter;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-- Оверрайд установки текущего значения
function This:AdjustValue(in_meta)
   -- NOTE: конвертация есть внутри
   --GGF.InnerInvoke(in_meta, "SetVisibleShellAmount", GGF.InnerInvoke(in_meta, "GetValue"));
   GGF.InnerInvoke(in_meta, "SetTargetMeterValue", GGF.InnerInvoke(in_meta, "GetValue"));
   if (not GGF.InnerInvoke(in_meta, "GetIsMoving")) then
      GGF.InnerInvoke(in_meta, "acceptNextMove");
   end
   GGF.OuterInvoke(
      GGF.InnerInvoke(in_meta, "GetCurrentLabel"),
      "SetValue",
      string.format("%.2f", GGF.InnerInvoke(in_meta, "GetValue"))
   );
end

-- Оверрайд установки текущего среднего значения
function This:AdjustAverage(in_meta)
   GGF.OuterInvoke(
      GGF.InnerInvoke(in_meta, "GetAverageLabel"),
      "SetValue",
      GGF.InnerInvoke(in_meta, "GetAverage")
   );
end

-- Оверрайд установки минимального значения
function This:AdjustMinLimit(in_meta)
   GGF.OuterInvoke(
      GGF.InnerInvoke(in_meta, "GetMinLabel"),
      "SetText",
      GGF.InnerInvoke(in_meta, "GetMinLimit")
   );
end

-- Оверрайд установки максимального значения
function This:AdjustMaxLimit(in_meta)
   GGF.OuterInvoke(
      GGF.InnerInvoke(in_meta, "GetMaxLabel"),
      "SetText",
      GGF.InnerInvoke(in_meta, "GetMaxLimit")
   );
end

-- Установка основных объектов измерителя
function This:adjustServiceObjects(in_meta)
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSideBorderOne"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSideBorderTwo"), "Adjust");
   local barShellList = GGF.InnerInvoke(in_meta, "GetBarShellList");
   for idx,shell in ipairs(barShellList) do
      GGF.OuterInvoke(shell.texture, "Adjust");
      GGF.OuterInvoke(shell.back, "Adjust");
   end
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetMinLabel"), "Adjust");
   GGF.InnerInvoke(in_meta, "AdjustMinLimit");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetMaxLabel"), "Adjust");
   GGF.InnerInvoke(in_meta, "AdjustMaxLimit");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetAverageLabel"), "Adjust");
   GGF.InnerInvoke(in_meta, "AdjustAverage");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetCurrentLabel"), "Adjust");
   GGF.InnerInvoke(in_meta, "AdjustValue");
end

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

-- Создание основных объектов измерителя
function This:createServiceObjects(in_meta)
   local parent = in_meta.object;
   GGF.InnerInvoke(in_meta, "checkConfig");
   GGF.InnerInvoke(in_meta, "recountBaseGeometry");
   GGF.InnerInvoke(in_meta, "createBorders");
   GGF.InnerInvoke(in_meta, "createShellList");
   GGF.InnerInvoke(in_meta, "createLabels");
end

-- Создание основных объектов измерителя
function This:checkConfig(in_meta)
   local btype = GGF.InnerInvoke(in_meta, "GetDataMeterBarType");
   local mstype = GGF.InnerInvoke(in_meta, "GetDataMeterMetaSideType");
   if ((btype == GGE.DataMeterBarType.DownDirected)
      or (btype == GGE.DataMeterBarType.UpDirected)) then
      if ((mstype == GGE.DataMeterMetaSideType.Up)
         or (mstype == GGE.DataMeterMetaSideType.Down)) then
            ATLASSERT(false);
      end
   else
      if ((mstype == GGE.DataMeterMetaSideType.Left)
         or (mstype == GGE.DataMeterMetaSideType.Right)) then
            ATLASSERT(false);
      end
   end
end

-- Создание основных объектов измерителя
function This:recountBaseGeometry(in_meta)
   local size = {
      width = GGF.InnerInvoke(in_meta, "GetWidth"),
      height = GGF.InnerInvoke(in_meta, "GetHeight"),
   };
   local metaDataGap = GGF.InnerInvoke(in_meta, "GetMetaDataGap");
   local objServGap = GGF.InnerInvoke(in_meta, "GetObjectServiceGap");
   local btype = GGF.InnerInvoke(in_meta, "GetDataMeterBarType");
   local mstype = GGF.InnerInvoke(in_meta, "GetDataMeterMetaSideType");
   local gapList = {
      left = GGF.SwitchCase(mstype,
         { GGE.DataMeterMetaSideType.Left, metaDataGap },
         { GGE.DataMeterMetaSideType.Right, 0 },
         { GGE.DataMeterMetaSideType.Up, 0 },
         { GGE.DataMeterMetaSideType.Down, 0 }
      ),
      right = GGF.SwitchCase(mstype,
         { GGE.DataMeterMetaSideType.Left, 0 },
         { GGE.DataMeterMetaSideType.Right, metaDataGap },
         { GGE.DataMeterMetaSideType.Up, 0 },
         { GGE.DataMeterMetaSideType.Down, 0 }
      ),
      up = GGF.SwitchCase(mstype,
         { GGE.DataMeterMetaSideType.Left, 0 },
         { GGE.DataMeterMetaSideType.Right, 0 },
         { GGE.DataMeterMetaSideType.Up, metaDataGap },
         { GGE.DataMeterMetaSideType.Down, 0 }
      ),
      down = GGF.SwitchCase(mstype,
         { GGE.DataMeterMetaSideType.Left, 0 },
         { GGE.DataMeterMetaSideType.Right, 0 },
         { GGE.DataMeterMetaSideType.Up, 0 },
         { GGE.DataMeterMetaSideType.Down, metaDataGap }
      )
   };
   local anchorList = GGF.SwitchCase(btype,
      -- 2------3
      -- |      |
      -- |      |
      -- |      |
      -- |      |
      -- |      |
      -- 1      4
      { GGE.DataMeterBarType.DownDirected, {
         relativePoint = GGE.PointType.Top,
         first = {
            offsetX = - size.width/2 + objServGap + gapList.left,
            offsetY = - size.height + objServGap,
         },
         second = {
            offsetX = - size.width/2 + objServGap + gapList.left,
            offsetY = - objServGap,
         },
         third = {
            offsetX = size.width/2 - objServGap - gapList.right,
            offsetY = - objServGap,
         },
         fourth = {
            offsetX = size.width/2 - objServGap - gapList.right,
            offsetY = - size.height + objServGap,
         },
      }},
      -- 1      4
      -- |      |
      -- |      |
      -- |      |
      -- |      |
      -- |      |
      -- 2------3
      { GGE.DataMeterBarType.UpDirected, {
         relativePoint = GGE.PointType.Bottom,
         first = {
            offsetX = - size.width/2 + objServGap + gapList.left,
            offsetY = size.height - objServGap,
         },
         second = {
            offsetX = - size.width/2 + objServGap + gapList.left,
            offsetY = objServGap,
         },
         third = {
            offsetX = size.width/2 - objServGap - gapList.right,
            offsetY = objServGap,
         },
         fourth = {
            offsetX = size.width/2 - objServGap - gapList.right,
            offsetY = size.height - objServGap,
         },
      }},
      -- 1-----------------2
      --                   |
      --                   |
      -- 4-----------------3
      { GGE.DataMeterBarType.LeftDirected, {
         relativePoint = GGE.PointType.Right,
         first = {
            offsetX = - size.width + objServGap,
            offsetY = size.height/2 - objServGap - gapList.up,
         },
         second = {
            offsetX = - objServGap,
            offsetY = size.height/2 - objServGap - gapList.up,
         },
         third = {
            offsetX = - objServGap,
            offsetY = - size.height/2 + objServGap + gapList.down,
         },
         fourth = {
            offsetX = - size.width + objServGap,
            offsetY = - size.height/2 + objServGap + gapList.down,
         },
      }},
      -- 2-----------------1
      -- |                  
      -- |                  
      -- 3-----------------4
      { GGE.DataMeterBarType.RightDirected, {
         relativePoint = GGE.PointType.Left,
         first = {
            offsetX = size.width - objServGap,
            offsetY = size.height/2 - objServGap - gapList.up,
         },
         second = {
            offsetX = objServGap,
            offsetY = size.height/2 - objServGap - gapList.up,
         },
         third = {
            offsetX = objServGap,
            offsetY = - size.height/2 + objServGap + gapList.down,
         },
         fourth = {
            offsetX = size.width - objServGap,
            offsetY = - size.height/2 + objServGap + gapList.down,
         },
      }}
   );
   GGF.InnerInvoke(in_meta, "SetGapList", gapList);
   GGF.InnerInvoke(in_meta, "SetAnchorList", anchorList);
end

-- Создание основных объектов измерителя
function This:createBorders(in_meta)
   local parent = in_meta.object;
   local borderWidth = GGF.InnerInvoke(in_meta, "GetBorderWidth");
   GGF.InnerInvoke(in_meta, "SetSideBorderOneOD", GGC.LineFrame:Create({}, {
      properties = {
         base = {
            parent = function() return parent end,
            wrapperName = "SSBO",
         },
         miscellaneous = {
            color = GGD.Color.Grey,
            hidden = false,
         },
         size = {
            width = borderWidth,
         },
         anchor = {
            relativeTo = function() return parent end,
         },
      },
   }));
   GGF.InnerInvoke(in_meta, "SetSideBorderTwoOD", GGC.LineFrame:Create({}, {
      properties = {
         base = {
            parent = function() return parent end,
            wrapperName = "SSBT",
         },
         miscellaneous = {
            color = GGD.Color.Grey,
            hidden = false,
         },
         size = {
            width = borderWidth,
         },
         anchor = {
            relativeTo = function() return parent end,
         },
      },
   }));
   GGF.InnerInvoke(in_meta, "SetButtBorderOD", GGC.LineFrame:Create({}, {
      properties = {
         base = {
            parent = function() return parent end,
            wrapperName = "SBB",
         },
         miscellaneous = {
            color = GGD.Color.Grey,
            hidden = false,
         },
         size = {
            width = borderWidth,
         },
         anchor = {
            relativeTo = function() return parent end,
         },
      },
   }));
   local anchorList = GGF.InnerInvoke(in_meta, "GetAnchorList");
   local anchorOne = {
      offsetX = anchorList.first.offsetX,
      offsetY = anchorList.first.offsetY,
      relativePoint = anchorList.relativePoint,
      secondOffsetX = anchorList.second.offsetX,
      secondOffsetY = anchorList.second.offsetY,
   };
   local anchorTwo = {
      offsetX = anchorList.second.offsetX,
      offsetY = anchorList.second.offsetY,
      relativePoint = anchorList.relativePoint,
      secondOffsetX = anchorList.third.offsetX,
      secondOffsetY = anchorList.third.offsetY,
   };
   local anchorButt = {
      offsetX = anchorList.third.offsetX,
      offsetY = anchorList.third.offsetY,
      relativePoint = anchorList.relativePoint,
      secondOffsetX = anchorList.fourth.offsetX,
      secondOffsetY = anchorList.fourth.offsetY,
   };
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSideBorderOne"), "SetAnchor", anchorOne);
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSideBorderTwo"), "SetAnchor", anchorTwo);
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetButtBorder"), "SetAnchor", anchorButt);
end

-- Создание шеллов
function This:createShellList(in_meta)
   local parent = in_meta.object;
   local metaDataGap = GGF.InnerInvoke(in_meta, "GetMetaDataGap");
   local objServGap = GGF.InnerInvoke(in_meta, "GetObjectServiceGap");
   local crossShellGap = GGF.InnerInvoke(in_meta, "GetCrossShellGap");
   local size = {
      width = GGF.InnerInvoke(in_meta, "GetWidth"),
      height = GGF.InnerInvoke(in_meta, "GetHeight"),
   };
   local barShellList = {};
   local shellCount = GGF.InnerInvoke(in_meta, "GetShellCount");
   local deltaX = (size.width - (shellCount-1)*crossShellGap - 4*objServGap)/shellCount + crossShellGap;
   local deltaY = (size.height - (shellCount-1)*crossShellGap - 4*objServGap)/shellCount + crossShellGap;
   local btype = GGF.InnerInvoke(in_meta, "GetDataMeterBarType");
   local mstype = GGF.InnerInvoke(in_meta, "GetDataMeterMetaSideType");
   local gapList = GGF.InnerInvoke(in_meta, "GetGapList");
   local shellWidth = GGF.SwitchCase(btype,
      { GGE.DataMeterBarType.DownDirected, size.width - 4*objServGap - gapList.left - gapList.right },
      { GGE.DataMeterBarType.UpDirected, size.width - 4*objServGap - gapList.left - gapList.right },
      { GGE.DataMeterBarType.LeftDirected, deltaX - crossShellGap },
      { GGE.DataMeterBarType.RightDirected, deltaX - crossShellGap }
   );
   local shellHeight = GGF.SwitchCase(btype,
      { GGE.DataMeterBarType.DownDirected, deltaY - crossShellGap },
      { GGE.DataMeterBarType.UpDirected, deltaY - crossShellGap },
      { GGE.DataMeterBarType.LeftDirected, size.height - 4*objServGap - gapList.up - gapList.down },
      { GGE.DataMeterBarType.RightDirected, size.height - 4*objServGap - gapList.up - gapList.down }
   );
   for i=1,shellCount do
      local relOffsetX = GGF.SwitchCase(btype,
         { GGE.DataMeterBarType.DownDirected, gapList.left/2 - gapList.right/2 },
         { GGE.DataMeterBarType.UpDirected, gapList.left/2 - gapList.right/2 },
         { GGE.DataMeterBarType.LeftDirected, - 2*objServGap - deltaX*(i - 1) - shellWidth/2 },
         { GGE.DataMeterBarType.RightDirected, 2*objServGap + deltaX*(i - 1) + shellWidth/2 }
      );
      local relOffsetY = GGF.SwitchCase(btype,
         { GGE.DataMeterBarType.DownDirected, - 2*objServGap - deltaY*(i - 1) - shellHeight/2 },
         { GGE.DataMeterBarType.UpDirected, 2*objServGap + deltaY*(i - 1) + shellHeight/2 },
         { GGE.DataMeterBarType.LeftDirected, - gapList.up/2 + gapList.down/2 },
         { GGE.DataMeterBarType.RightDirected, - gapList.up/2 + gapList.down/2 }
      );
      local relPt = GGF.SwitchCase(btype,
         { GGE.DataMeterBarType.DownDirected, GGE.PointType.Top },
         { GGE.DataMeterBarType.UpDirected, GGE.PointType.Bottom },
         { GGE.DataMeterBarType.LeftDirected, GGE.PointType.Right },
         { GGE.DataMeterBarType.RightDirected, GGE.PointType.Left }
      );
      --barShellList[i] = GGC.StandardFrame:Create({}, {
         --properties = {
            --base = {
               --parent = function() return parent end,
               --wrapperName = "BS_" .. tostring(i),
            --},
            --size = {
               --width = shellWidth,
               --height = shellHeight,
            --},
            --anchor = {
               --point = pt,
               --relativeTo = function() return parent end,
               --relativePoint = relPt,
               --offsetX = relOffsetX,
               --offsetY = relOffsetY,
            --},
            --backdrop = {
               --bgFile = "Interface\\Tooltips\\UI-Tooltip-Background",
               --edgeFile = "",
               --edgeSize = 16,
               --tileSize = 16,
               --insets = {
                  --left = 3,
                  --right = 3,
                  --top = 3,
                  --bottom = 3,
               --},
               --color = {
                  --R = 1.0,
                  --G = 0.0,
                  --B = 0.0,
                  --A = 0.9,
               --}
            --},
         --},
      --});
      barShellList[i] = {
         texture = GGC.StandardTexture:Create({}, {
            properties = {
               base = {
                  parent = function() return parent end,
                  wrapperName = "BST_" .. tostring(i),
               },
               miscellaneous = {
                  file = GGD.COM.TexturePath .. "shell.tga"
               },
               size = {
                  width = shellWidth,
                  height = shellHeight,
               },
               anchor = {
                  point = GGE.PointType.Center,
                  relativeTo = function() return parent end,
                  relativePoint = relPt,
                  offsetX = relOffsetX,
                  offsetY = relOffsetY,
               },
            }
         }),
         back = GGC.StandardTexture:Create({}, {
            properties = {
               base = {
                  parent = function() return parent end,
                  wrapperName = "BSB_" .. tostring(i),
               },
               miscellaneous = {
                  file = GGD.COM.TexturePath .. "shell_back.tga"
               },
               size = {
                  -- NOTE: расчет исходя из масштабов 256х512 -> 296x552
                  width = shellWidth*1.157,
                  height = shellHeight*1.078,
               },
               anchor = {
                  point = GGE.PointType.Center,
                  relativeTo = function() return parent end,
                  relativePoint = relPt,
                  offsetX = relOffsetX,
                  offsetY = relOffsetY,
               },
            },
         }),
      };
   end
   GGF.InnerInvoke(in_meta, "SetBarShellListOD", barShellList);
end

-- Создание обвязки мин/макс
function This:createLabels(in_meta)
   local parent = in_meta.object;
   local mstype = GGF.InnerInvoke(in_meta, "GetDataMeterMetaSideType");
   local btype = GGF.InnerInvoke(in_meta, "GetDataMeterBarType");
   GGF.InnerInvoke(in_meta, "SetMinLabelOD", GGC.FontString:Create({}, {
      properties = {
         base = {
            parent = function() return parent end,
            wrapperName = "MNL",
         },
         size = {
            width = 50,
            height = 14,
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = function() return parent end,
            justifyH = GGE.JustifyHType.Center,
         },
      },
   }));
   GGF.InnerInvoke(in_meta, "SetMaxLabelOD", GGC.FontString:Create({}, {
      properties = {
         base = {
            parent = function() return parent end,
            wrapperName = "MXL",
         },
         size = {
            width = 50,
            height = 14,
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = function() return parent end,
            justifyH = GGE.JustifyHType.Center,
         },
      },
   }));
   GGF.InnerInvoke(in_meta, "SetAverageLabelOD", GGC.NamedValueFontString:Create({}, {
      properties = {
         base = {
            parent = function() return parent end,
            wrapperName = "AL",
         },
         miscellaneous = {
            hidden = false,
            name = "avg:",
            value = "N/A",
         },
         size = {
            width = 200,
            lrPercentage = 0.3,
            height = 14,
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = function() return parent end,
         },
         backdrop = GGD.Backdrop.Empty,
         color = {
            name = GGF.TableCopy(GGD.Color.Title),
            value = GGF.TableCopy(GGD.Color.Neutral),
         },
      },
   }));
   GGF.InnerInvoke(in_meta, "SetCurrentLabelOD", GGC.NamedValueFontString:Create({}, {
      properties = {
         base = {
            parent = function() return parent end,
            wrapperName = "CL",
         },
         miscellaneous = {
            hidden = false,
            name = "cur:",
            value = "N/A",
         },
         size = {
            width = 200,
            lrPercentage = 0.3,
            height = 14,
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = function() return parent end,
         },
         backdrop = GGD.Backdrop.Empty,
         color = {
            name = GGF.TableCopy(GGD.Color.Title),
            value = GGF.TableCopy(GGD.Color.Neutral),
         },
      },
   }));
   local anchorList = GGF.InnerInvoke(in_meta, "GetAnchorList");
   local labelGap = GGF.InnerInvoke(in_meta, "GetLabelGap");
   local anchorMin = GGF.SwitchCase(mstype,
      { GGE.DataMeterMetaSideType.Left, {
         offsetX = anchorList.second.offsetX - labelGap,
         offsetY = anchorList.second.offsetY + labelGap*GGF.SwitchCase(btype,
            { GGE.DataMeterBarType.DownDirected, -1 },
            { GGE.DataMeterBarType.UpDirected, 1 },
            { GGE.DataMeterBarType.LeftDirected, 0 },
            { GGE.DataMeterBarType.RightDirected, 0 }
         ),
         relativePoint = anchorList.relativePoint,
      } },
      { GGE.DataMeterMetaSideType.Right, {
         offsetX = anchorList.third.offsetX + labelGap,
         offsetY = anchorList.third.offsetY + labelGap*GGF.SwitchCase(btype,
            { GGE.DataMeterBarType.DownDirected, -1 },
            { GGE.DataMeterBarType.UpDirected, 1 },
            { GGE.DataMeterBarType.LeftDirected, 0 },
            { GGE.DataMeterBarType.RightDirected, 0 }
         ),
         relativePoint = anchorList.relativePoint,
      } },
      { GGE.DataMeterMetaSideType.Up, {
         offsetX = anchorList.second.offsetX + labelGap*GGF.SwitchCase(btype,
            { GGE.DataMeterBarType.DownDirected, 0 },
            { GGE.DataMeterBarType.UpDirected, 0 },
            { GGE.DataMeterBarType.LeftDirected, -1 },
            { GGE.DataMeterBarType.RightDirected, 1 }
         ),
         offsetY = anchorList.second.offsetY + labelGap,
         relativePoint = anchorList.relativePoint,
      } },
      { GGE.DataMeterMetaSideType.Down, {
         offsetX = anchorList.third.offsetX + labelGap*GGF.SwitchCase(btype,
            { GGE.DataMeterBarType.DownDirected, 0 },
            { GGE.DataMeterBarType.UpDirected, 0 },
            { GGE.DataMeterBarType.LeftDirected, -1 },
            { GGE.DataMeterBarType.RightDirected, 1 }
         ),
         offsetY = anchorList.third.offsetY - labelGap,
         relativePoint = anchorList.relativePoint,
      } }
   );
   local anchorMax = GGF.SwitchCase(mstype,
      { GGE.DataMeterMetaSideType.Left, {
         offsetX = anchorList.first.offsetX - labelGap,
         offsetY = anchorList.first.offsetY + labelGap*GGF.SwitchCase(btype,
            { GGE.DataMeterBarType.DownDirected, 1 },
            { GGE.DataMeterBarType.UpDirected, -1 },
            { GGE.DataMeterBarType.LeftDirected, 0 },
            { GGE.DataMeterBarType.RightDirected, 0 }
         ),
         relativePoint = anchorList.relativePoint,
      } },
      { GGE.DataMeterMetaSideType.Right, {
         offsetX = anchorList.fourth.offsetX + labelGap,
         offsetY = anchorList.fourth.offsetY + labelGap*GGF.SwitchCase(btype,
            { GGE.DataMeterBarType.DownDirected, 1 },
            { GGE.DataMeterBarType.UpDirected, -1 },
            { GGE.DataMeterBarType.LeftDirected, 0 },
            { GGE.DataMeterBarType.RightDirected, 0 }
         ),
         relativePoint = anchorList.relativePoint,
      } },
      { GGE.DataMeterMetaSideType.Up, {
         offsetX = anchorList.first.offsetX + labelGap*GGF.SwitchCase(btype,
            { GGE.DataMeterBarType.DownDirected, 0 },
            { GGE.DataMeterBarType.UpDirected, 0 },
            { GGE.DataMeterBarType.LeftDirected, 1 },
            { GGE.DataMeterBarType.RightDirected, -1 }
         ),
         offsetY = anchorList.first.offsetY + labelGap,
         relativePoint = anchorList.relativePoint,
      } },
      { GGE.DataMeterMetaSideType.Down, {
         offsetX = anchorList.fourth.offsetX + labelGap*GGF.SwitchCase(btype,
            { GGE.DataMeterBarType.DownDirected, 0 },
            { GGE.DataMeterBarType.UpDirected, 0 },
            { GGE.DataMeterBarType.LeftDirected, 1 },
            { GGE.DataMeterBarType.RightDirected, -1 }
         ),
         offsetY = anchorList.fourth.offsetY - labelGap,
         relativePoint = anchorList.relativePoint,
      } }
   );
   local anchorAverage = {
      offsetX = math.floor((anchorMin.offsetX + 2.0*anchorMax.offsetX)/3.0),
      offsetY = math.floor((anchorMin.offsetY + 2.0*anchorMax.offsetY)/3.0),
      relativePoint = anchorList.relativePoint,
   };
   local anchorCurrent = {
      offsetX = math.floor((2.0*anchorMin.offsetX + anchorMax.offsetX)/3.0),
      offsetY = math.floor((2.0*anchorMin.offsetY + anchorMax.offsetY)/3.0),
      relativePoint = anchorList.relativePoint,
   };
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetMinLabel"), "SetAnchor", anchorMin);
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetMaxLabel"), "SetAnchor", anchorMax);
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetAverageLabel"), "SetAnchor", anchorAverage);
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetCurrentLabel"), "SetAnchor", anchorCurrent);
end

-- Абсолютное значение в количество баров к отображению
function This:valueToShellAmount(in_meta, in_val)
   local shellCount = GGF.InnerInvoke(in_meta, "GetShellCount");
   local minLimit = GGF.InnerInvoke(in_meta, "GetMinLimit");
   local maxLimit = GGF.InnerInvoke(in_meta, "GetMaxLimit");
   shellCapacity = (maxLimit - minLimit)/shellCount;
   return math.floor(in_val/shellCapacity);
end

-- Ресет отображения
function This:resetMeterValue(in_meta)
   local visibleShellCount = GGF.InnerInvoke(in_meta, "GetVisibleShellAmount");
   local barShellList = GGF.InnerInvoke(in_meta, "GetBarShellList");
   for idx,shell in ipairs(barShellList) do
      if (idx < visibleShellCount) then
         GGF.OuterInvoke(shell.texture, "SetHidden", false);
         GGF.OuterInvoke(shell.back, "SetHidden", false);
      else
         GGF.OuterInvoke(shell.texture, "SetHidden", true);
         GGF.OuterInvoke(shell.back, "SetHidden", true);
      end
   end
end

-- Установка текущего значения
function This:acceptNextMove(in_meta)
   local currentMeterValue = GGF.InnerInvoke(in_meta, "GetCurrentMeterValue");
   local targetMeterValue = GGF.InnerInvoke(in_meta, "GetTargetMeterValue");
   local msForward = GGF.InnerInvoke(in_meta, "GetMovingSpeedForward");
   local msBackward = GGF.InnerInvoke(in_meta, "GetMovingSpeedBackward");
   local animationStep = GGF.InnerInvoke(in_meta, "GetAnimationStep");
   local maxLimit, minLimit = GGF.InnerInvoke(in_meta, "GetMaxLimit"), GGF.InnerInvoke(in_meta, "GetMinLimit");

   --print("anm:", targetMeterValue, currentMeterValue);
   GGF.InnerInvoke(in_meta, "SetIsMoving", true);
   if (currentMeterValue == targetMeterValue) then
      GGF.InnerInvoke(in_meta, "SetIsMoving", false);
      GGF.InnerInvoke(in_meta, "SetVisibleShellAmount", GGF.InnerInvoke(in_meta, "GetTargetMeterValue"));
      return;
   end

   local cNext = true;
   local mspeed = GGF.TernExpSingle(targetMeterValue > currentMeterValue, msForward, -msBackward);
   local mvDiff = mspeed*(maxLimit - minLimit)*animationStep;
   local resultMeterValue = 0;
   -- NOTE: при этом условии следующий мув перегпрыгнет цель
   if ((targetMeterValue - currentMeterValue - mvDiff)*(mvDiff) <= 0) then
      resultMeterValue = targetMeterValue;
      cNext = false;
   else
      resultMeterValue = currentMeterValue + mvDiff;
   end
   GGF.InnerInvoke(in_meta, "SetCurrentMeterValue", resultMeterValue);
   GGF.InnerInvoke(in_meta, "SetVisibleShellAmount", GGF.InnerInvoke(in_meta, "GetCurrentMeterValue"));

   if (cNext) then
      GGC.TriggerTimer:Create({
         time = animationStep,
         callback = function()
            GGF.InnerInvoke(in_meta, "acceptNextMove")
         end,
      });
   else
      GGF.InnerInvoke(in_meta, "SetIsMoving", false);
   end
end