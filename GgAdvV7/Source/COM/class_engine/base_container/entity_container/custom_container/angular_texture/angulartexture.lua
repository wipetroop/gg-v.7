-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "AT";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	AngularTextureSuffix = GGF.DockDomName(GGD.DefineIdentifier, "AngularTextureSuffix"),
	AngularTexture = GGF.DockDomName(GGD.ClassIdentifier, "AngularTexture"),
};

local CTList = {
	AngularTexture = GGF.CreateCT(OBJSuffix, "AngularTexture", "Frame"),
};

GGF.GRegister(DFList.AngularTextureSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Frame = 0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.AngularTexture, {
	meta = {
		name = "Angular Texture",
		mark = "",	-- авт.
		suffix = GGD.AngularTextureSuffix,
	},
});

GGC.AngularTexture.objects = {
	frame = {
		texture = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.AngularTexture,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.AngularTexture.Default)) then
					local texture = GGF.InnerInvoke(in_meta, "GetParent"):CreateTexture(
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
					);
					GGF.InnerInvoke(in_meta, "ModifyAngularTextureOD", texture);
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.AngularTexture.wrappers = {};

GGC.AngularTexture.properties = {};

GGC.AngularTexture.elements = {};

GGF.Mark(GGC.AngularTexture);

GGC.AngularTexture = GGF.TableSafeExtend(GGC.AngularTexture, GGC.StandardTexture);

GGF.CreateClassWorkaround(GGC.AngularTexture);

local This = GGC.AngularTexture;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- TODO: запихать внутрь объекта
-- TODO: проверить работоспособность
-- Установка координат графического объекта внутри фрейма
function This:SetTexCoord(in_meta, in_c1, in_c2, in_c3, in_c4)
	in_meta.object:SetTexCoord(in_c1, in_c2, in_c3, in_c4);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------