-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

local OBJSuffix = "MZD";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   MuzzleDrawerSuffix = GGF.DockDomName(GGD.DefineIdentifier, "MuzzleDrawerSuffix"),
   MuzzleDrawer = GGF.DockDomName(GGD.ClassIdentifier, "MuzzleDrawer"),
};

local CTList = {
   MuzzleDrawer = GGF.CreateCT(OBJSuffix, "MuzzleDrawer", "MuzzleDrawer"),
   ParentInstance = GGF.CreateCT(OBJSuffix, "ParentInstance", "ParentInstance"),
   MuzzleIconSide = GGF.CreateCT(OBJSuffix, "MuzzleIconSide", "MuzzleIconSide"),
   MuzzleInset = GGF.CreateCT(OBJSuffix, "MuzzleInset", "MuzzleInset"),
   MuzzleBackgroundInset = GGF.CreateCT(OBJSuffix, "MuzzleBackgroundInset", "MuzzleBackgroundInset"),
   MuzzleEdgeSize = GGF.CreateCT(OBJSuffix, "MuzzleEdgeSize", "MuzzleEdgeSize"),
   MuzzleTileSize = GGF.CreateCT(OBJSuffix, "MuzzleTileSize", "MuzzleTileSize"),
   MuzzleIconShift = GGF.CreateCT(OBJSuffix, "MuzzleIconShift", "MuzzleIconShift"),
   ObjectGapHor = GGF.CreateCT(OBJSuffix, "ObjectGapHor", "ObjectGapHor"),
   RaceIconSet = GGF.CreateCT(OBJSuffix, "RaceIconSet", "RaceIconSet"),
   PetGlyphIcon = GGF.CreateCT(OBJSuffix, "PetGlyphIcon", "PetGlyphIcon"),
};

GGF.GRegister(DFList.MuzzleDrawerSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
   MuzzleDrawer = 0,
   ParentInstance = 0,
   MuzzleIconSide = 26,
   MuzzleInset = 1,
   MuzzleBackgroundInset = 0,
   MuzzleEdgeSize = 5,
   MuzzleTileSize = 1,
   MuzzleIconShift = 1,
   ObjectGapHor = 10,
   RaceIconSet = {
      -- male
      [2] = {
         ["BloodElf"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Bloodelf_Male.blp",
         ["Draenei"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Draenei_Male.blp",
         ["Dwarf"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Dwarf_Male.blp",
         ["Gnome"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Gnome_Male.blp",
         ["Goblin"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Goblin_Male.blp",
         ["Human"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Human_Male.blp",
         ["NightElf"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_NightElf_Male.blp",
         ["Orc"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Orc_Male.blp",
         ["Pandaren"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Pandaren_Male.blp",
         ["Tauren"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Tauren_Male.blp",
         ["Troll"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Troll_Male.blp",
         ["Scourge"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Undead_Male.blp",
         ["Worgen"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Worgen_Male.blp",
      },
      -- female
      [3] = {
         ["BloodElf"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Bloodelf_Female.blp",
         ["Draenei"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Draenei_Female.blp",
         ["Dwarf"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Dwarf_Female.blp",
         ["Gnome"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Gnome_Female.blp",
         ["Goblin"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Goblin_Female.blp",
         ["Human"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Human_Female.blp",
         ["NightElf"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_NightElf_Female.blp",
         ["Orc"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Orc_Female.blp",
         ["Pandaren"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Pandaren_Female.blp",
         ["Tauren"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Tauren_Female.blp",
         ["Troll"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Troll_Female.blp",
         ["Scourge"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Undead_Female.blp",
         ["Worgen"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Worgen_Female.blp",
      },
   },
   PetGlyphIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Ability_Hunter_MendPet.blp"
}, GGE.RegValType.Default);

GGF.GRegister(DFList.MuzzleDrawer, {
   meta = {
      name = "Muzzle Drawer",
      mark = "",  -- авт.
      suffix = GGD.MuzzleDrawerSuffix,
   },
});

GGC.MuzzleDrawer.objects = {};

GGC.MuzzleDrawer.wrappers = {};

GGC.MuzzleDrawer.properties = {
   base = {
      parentInstance = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.ParentInstance,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
   preset = {
      muzzleIconSide = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.MuzzleIconSide,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      muzzleInset = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.MuzzleInset,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      muzzleBackgroundInset = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.MuzzleBackgroundInset,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      muzzleEdgeSize = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.MuzzleEdgeSize,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      muzzleTileSize = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.MuzzleTileSize,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      muzzleIconShift = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.MuzzleIconShift,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      objectGapHor = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.ObjectGapHor,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      raceIconSet = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.RaceIconSet,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      petGlyphIcon = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.PetGlyphIcon,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.MuzzleDrawer.elements = {};

GGF.Mark(GGC.MuzzleDrawer);

GGC.MuzzleDrawer = GGF.TableSafeExtend(GGC.MuzzleDrawer, GGC.AbstractContainer);

GGF.CreateClassWorkaround(GGC.MuzzleDrawer);

local This = GGC.MuzzleDrawer;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-- TODO: найти возможность повернуть модельку(GG-41)
-- api
-- Установка мордашек в строку tvp
function This:SetUnitMuzzle(in_meta, in_Pet, in_src, in_basementFrame, in_modelFrame, in_altFrame, in_unitName, in_unitGUID)
   GGF.OuterInvoke(in_modelFrame, "ClearModel");
   GGF.OuterInvoke(in_modelFrame, "SetPortraitZoom", 1);
   GGF.OuterInvoke(in_modelFrame, "SetCamDistanceScale", 1);
   GGF.OuterInvoke(in_modelFrame, "SetPosition", 0, 0, 0);
   if UnitExists(in_unitName) then
      GGF.OuterInvoke(in_modelFrame, "SetUnit", in_unitName);
      GGF.OuterInvoke(in_modelFrame, "SetHidden", false);
      GGF.OuterInvoke(in_basementFrame, "SetHidden", false);
      GGF.OuterInvoke(in_altFrame, "SetHidden", true);
      --SetPortraitTexture(in_modelFrame, in_unitName);
   else
      -- TODO: переделать, а то костыль...
      -- NOTE: если там петик без, то все значения nil
      local unitInfo = GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetParentInstance"), "getPlayerInfoByGUIDEnhanced", in_unitGUID);
      --class, classFilename, race, raceFilename, sex, name, realm = GetPlayerInfoByGUiD(in_unitGUID);
      --print(in_unitName, class, classFilename, race, raceFilename, sex, name, realm);

      if (unitInfo ~= nil) then
         local raceIconSet = GGF.InnerInvoke(in_meta, "GetRaceIconSet");
         GGF.OuterInvoke(in_altFrame, "SetFile", raceIconSet[unitInfo.sex][unitInfo.raceFilename]);
      else
         local petGlyphIcon = GGF.InnerInvoke(in_meta, "GetPetGlyphIcon");
         GGF.OuterInvoke(in_altFrame, "SetFile", petGlyphIcon);
      end
      
      GGF.OuterInvoke(in_modelFrame, "SetHidden", true);
      GGF.OuterInvoke(in_basementFrame, "SetHidden", true);
      GGF.OuterInvoke(in_altFrame, "SetHidden", false);
      --SetPortraitTexture(in_texFrame, in_unitName);
      --in_frame:SetModel('interface\\buttons\\talktomequestionmark.m2');
   end
   --in_frame:SetCamera(0);
end

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-- api
-- Создание фрейма под модель мордахи
function This:GetNewMuzzleModelFrame(in_meta, in_strType, in_strIndex, in_parent)
   local muzzleIconSide = GGF.InnerInvoke(in_meta, "GetMuzzleIconSide");
   local muzzleIconShift = GGF.InnerInvoke(in_meta, "GetMuzzleIconShift");
   local objectGapHor = GGF.InnerInvoke(in_meta, "GetObjectGapHor");
   return GGC.PlayerModel:Create({
      properties = {
         base = {
            wrapperName = in_strType .. "SMIM_" .. tostring(in_strIndex),
            parent = in_parent,
         },
         size = {
            width = muzzleIconSide - muzzleIconShift,
            height = muzzleIconSide - muzzleIconShift,
         },
         anchor = {
            point = GGE.PointType.Left,
            relativeTo = in_parent,
            relativePoint = GGE.PointType.Left,
            offsetX = 0,--function(in_argline)
               --return in_argline.pOffsetX + in_argline.pWidth + objectGapHor - 0.5*muzzleIconShift
            --end,
            offsetY = 0,
         },
         backdrop = GGD.Backdrop.Empty,
      },
   }, {});
end

-- api
-- Создание фрейма под фон мордахи
function This:GetNewMuzzleBackgroundFrame(in_meta, in_strType, in_strIndex, in_parent, in_relativeTo)
   local muzzleEdgeSize = GGF.InnerInvoke(in_meta, "GetMuzzleEdgeSize");
   local muzzleTileSize = GGF.InnerInvoke(in_meta, "GetMuzzleTileSize");
   local muzzleBackgroundInset = GGF.InnerInvoke(in_meta, "GetMuzzleBackgroundInset");
   local muzzleIconSide = GGF.InnerInvoke(in_meta, "GetMuzzleIconSide");
   local objectGapHor = GGF.InnerInvoke(in_meta, "GetObjectGapHor");
   return GGC.StandardFrame:Create({
      properties = {
         base = {
            parent = in_parent,
            wrapperName = in_strType .. "SMIB_" .. tostring(in_strIndex),
         },
         miscellaneous = {
            enableMouse = false,
            hidden = false,
         },
         backdrop = {
            --bgFile = [[Interface\BUTTONS\WHITE8X8]],
            --edgeFile = "Interface\\Tooltips\\Ui-Tooltip-Border",
            edgeFile = "",
            tile = false,
            edgeSize = muzzleEdgeSize,
            tileSize = muzzleTileSize,
            insets = {
               left = muzzleBackgroundInset,
               right = muzzleBackgroundInset,
               top = muzzleBackgroundInset,
               bottom = muzzleBackgroundInset,
            },
            color = {
               R = 0.05,
               G = 0.1,
               B = 0.15,
               A = 1.0,
            },
            borderColor = {
               R = 1.0,
               G = 1.0,
               B = 1.0,
               A = 1.0,
            },
         },
         size = {
            width = muzzleIconSide,
            height = muzzleIconSide,
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = in_relativeTo,
            relativePoint = GGE.PointType.Center,
            offsetX = 0, --function(in_argline)
               --return in_argline.pOffsetX + in_argline.pWidth + objectGapHor
            --end,
            offsetY = 0,
         },
      },
   }, {});
end

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------