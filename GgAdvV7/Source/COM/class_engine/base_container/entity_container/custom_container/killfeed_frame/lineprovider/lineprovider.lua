-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

local OBJSuffix = "LNP";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   LineProviderSuffix = GGF.DockDomName(GGD.DefineIdentifier, "LineProviderSuffix"),
   LineProvider = GGF.DockDomName(GGD.ClassIdentifier, "LineProvider"),
};

local CTList = {
   ControlFrame = GGF.CreateCT(OBJSuffix, "ControlFrame", "Frame"),
   ParentInstance = GGF.CreateCT(OBJSuffix, "ParentInstance", "Frame"),
   MuzzleDrawerInstance = GGF.CreateCT(OBJSuffix, "MuzzleDrawerInstance", "Frame"),
   PetResolverInstance = GGF.CreateCT(OBJSuffix, "PetResolverInstance", "Frame"),
   FactoryInstance = GGF.CreateCT(OBJSuffix, "FactoryInstance", "Frame"),
   LayoutOrder = GGF.CreateCT(OBJSuffix, "LayoutOrder", "LayoutOrder"),
   Index = GGF.CreateCT(OBJSuffix, "Index", "Index"),
   Signature = GGF.CreateCT(OBJSuffix, "Signature", "Signature"),
   SourceHolder = GGF.CreateCT(OBJSuffix, "SourceHolder", "Frame"),
   SourceFactionIcon = GGF.CreateCT(OBJSuffix, "SourceFactionIcon", "Frame"),
   SourceMuzzleIconModel = GGF.CreateCT(OBJSuffix, "SourceMuzzleIconModel", "Frame"),
   SourceMuzzleIconBasement = GGF.CreateCT(OBJSuffix, "SourceMuzzleIconBasement", "Frame"),
   SourceMuzzleIconAlternate = GGF.CreateCT(OBJSuffix, "SourceMuzzleIconAlternate", "Frame"),
   SourceClassIcon = GGF.CreateCT(OBJSuffix, "SourceClassIcon", "Frame"),
   SourceText = GGF.CreateCT(OBJSuffix, "SourceText", "Frame"),
   SourceBackground = GGF.CreateCT(OBJSuffix, "SourceBackground", "Frame"),
   SourceExistenceFlag = GGF.CreateCT(OBJSuffix, "SourceExistenceFlag", "ExistenceFlag"),
   TargetHolder = GGF.CreateCT(OBJSuffix, "TargetHolder", "Frame"),
   TargetFactionIcon = GGF.CreateCT(OBJSuffix, "TargetFactionIcon", "Frame"),
   TargetMuzzleIconModel = GGF.CreateCT(OBJSuffix, "TargetMuzzleIconModel", "Frame"),
   TargetMuzzleIconBasement = GGF.CreateCT(OBJSuffix, "TargetMuzzleIconBasement", "Frame"),
   TargetMuzzleIconAlternate = GGF.CreateCT(OBJSuffix, "TargetMuzzleIconAlternate", "Frame"),
   TargetClassIcon = GGF.CreateCT(OBJSuffix, "TargetClassIcon", "Frame"),
   TargetText = GGF.CreateCT(OBJSuffix, "TargetText", "Frame"),
   TargetBackground = GGF.CreateCT(OBJSuffix, "TargetBackground", "Frame"),
   TargetExistenceFlag = GGF.CreateCT(OBJSuffix, "TargetExistenceFlag", "ExistenceFlag"),
   RemedyHolder = GGF.CreateCT(OBJSuffix, "RemedyHolder", "Frame"),
   RemedyIS = GGF.CreateCT(OBJSuffix, "RemedyIS", "Frame"),
   RemedyIC = GGF.CreateCT(OBJSuffix, "RemedyIC", "Frame"),
   RemedyIM = GGF.CreateCT(OBJSuffix, "RemedyIM", "Frame"),
   RemedyISBackground = GGF.CreateCT(OBJSuffix, "RemedyISBackground", "Frame"),
   RemedyICBackground = GGF.CreateCT(OBJSuffix, "RemedyICBackground", "Frame"),
   RemedyIMBackground = GGF.CreateCT(OBJSuffix, "RemedyIMBackground", "Frame"),
   ServiceHolder = GGF.CreateCT(OBJSuffix, "ServiceHolder", "Frame"),
   ServiceExistenceFlag = GGF.CreateCT(OBJSuffix, "ServiceExistenceFlag", "ExistenceFlag"),
   ServiceDelimiter = GGF.CreateCT(OBJSuffix, "ServiceDelimiter", "Frame"),
};

GGF.GRegister(DFList.LineProviderSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
   Frame = 0,
   LayoutOrder = function(in_meta)
      local sgn = GGF.InnerInvoke(in_meta, "GetSignature");
      local layoutOrder = {};
      layoutOrder.object = {
         [1] = { -- 1
            [1] = GGF.InnerInvoke(in_meta, "constructLayoutElementBundle", sgn.source.frontal.factionIcon, "srcFactionIcon"),
         },
         [2] = { -- 2
            [1] = GGF.InnerInvoke(in_meta, "constructLayoutElementBundle", sgn.source.frontal.muzzleIconModel, "srcMuzzleIconAlternate"),
            [2] = GGF.InnerInvoke(in_meta, "constructLayoutElementBundle", sgn.source.frontal.muzzleIconBasement, "srcMuzzleIconAlternate"),
            [3] = GGF.InnerInvoke(in_meta, "constructLayoutElementBundle", sgn.source.frontal.muzzleIconAlternate, "srcMuzzleIconAlternate"),
         },
         [3] = { -- 3
            [1] = GGF.InnerInvoke(in_meta, "constructLayoutElementBundle", sgn.source.frontal.classIcon, "srcClassIcon"),
         },
         [4] = { -- 4
            [1] = GGF.InnerInvoke(in_meta, "constructLayoutElementBundle", sgn.source.frontal.text, "srcText"),
         },
         [5] = { -- 5
            [1] = GGF.InnerInvoke(in_meta, "constructLayoutElementBundle", sgn.remedy.frontal.IS, "IS"),
         },
         [6] = { -- 6
            [1] = GGF.InnerInvoke(in_meta, "constructLayoutElementBundle", sgn.remedy.frontal.IC, "IC"),
         },
         [7] = { -- 7
            [1] = GGF.InnerInvoke(in_meta, "constructLayoutElementBundle", sgn.remedy.frontal.IM, "IM"),
         },
         [8] = { -- 8
            [1] = GGF.InnerInvoke(in_meta, "constructLayoutElementBundle", sgn.target.frontal.text, "trgText"),
         },
         [9] = { -- 9
            [1] = GGF.InnerInvoke(in_meta, "constructLayoutElementBundle", sgn.target.frontal.classIcon, "trgClassIcon"),
         },
         [10] = { -- 10
            [1] = GGF.InnerInvoke(in_meta, "constructLayoutElementBundle", sgn.target.frontal.muzzleIconModel, "trgMuzzleIconAlternate"),
            [2] = GGF.InnerInvoke(in_meta, "constructLayoutElementBundle", sgn.target.frontal.muzzleIconBasement, "trgMuzzleIconAlternate"),
            [3] = GGF.InnerInvoke(in_meta, "constructLayoutElementBundle", sgn.target.frontal.muzzleIconAlternate, "trgMuzzleIconAlternate"),
         },
         [11] = { -- 11
            [1] = GGF.InnerInvoke(in_meta, "constructLayoutElementBundle", sgn.target.frontal.factionIcon, "trgFactionIcon"),
         },
      };


      layoutOrder.background = {
         [1] = { -- 1
            [1] = GGF.InnerInvoke(in_meta, "constructBackgroundLayoutElementBundle", sgn.source.background.common, 1, 4, "srcBackground"),
         },
         [2] = { -- 2
            [1] = GGF.InnerInvoke(in_meta, "constructBackgroundLayoutElementBundle", sgn.remedy.background.spell, 5, 5, "spellBackgroundSpell"),
         },
         [3] = { -- 3
            [1] = GGF.InnerInvoke(in_meta, "constructBackgroundLayoutElementBundle", sgn.remedy.background.Critical, 6, 6, "spellBackgroundCritical"),
         },
         [4] = { -- 4
            [1] = GGF.InnerInvoke(in_meta, "constructBackgroundLayoutElementBundle", sgn.remedy.background.multistrike, 7, 7, "spellBackgroundMulticast"),
         },
         [5] = { -- 5
            [1] = GGF.InnerInvoke(in_meta, "constructBackgroundLayoutElementBundle", sgn.target.background.common, 8, 11, "trgBackground"),
         },
      };
   end,
   Index = 0,
   Signature = 0,
   ExistenceFlag = false,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.LineProvider, {
   meta = {
      name = "Line Provider",
      mark = "",  -- авт.
      suffix = GGD.LineProviderSuffix,
   },
});

GGC.LineProvider.objects = {
   frame = {
      control = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Object,
         cte = CTList.ControlFrame,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.ControlFrame.Default)) then
               local layoutFrame = CreateFrame(
                  "Frame",
                  GGF.GenerateOTName(
                     GGF.InnerInvoke(in_meta, "GetParentName"),
                     GGF.InnerInvoke(in_meta, "GetWrapperName"),
                     GGF.InnerInvoke(in_meta, "getClassSuffix")
                  ),
                  GGF.InnerInvoke(in_meta, "GetParent"),
                  GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              BackdropTemplateMixin and "BackdropTemplate" or nil
                           )
                        end,
                     }
                  })
               );
               GGF.InnerInvoke(in_meta, "ModifyControlFrameOD", layoutFrame);
            end
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.LineProvider.wrappers = {};

GGC.LineProvider.properties = {
   base = {
      parentInstance = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.ParentInstance,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      muzzleDrawerInstance = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.MuzzleDrawerInstance,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      petResolverInstance = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.PetResolverInstance,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      factoryInstance = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.FactoryInstance,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
   miscellaneous = {},
   preset = {
      layoutOrder = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.LayoutOrder,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      index = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.Index,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.LineProvider.elements = {
   runtime = {
      signature = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.Signature,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      source = {
         holder = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.SourceHolder,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         existenceFlag = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.SourceExistenceFlag,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         frontal = {
            factionIcon = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.SourceFactionIcon,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            muzzleIconModel = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.SourceMuzzleIconModel,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            muzzleIconBasement = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.SourceMuzzleIconBasement,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            muzzleIconAlternate = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.SourceMuzzleIconAlternate,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            classIcon = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.SourceClassIcon,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            text = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.SourceText,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
         },
         background = {
            common = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.SourceBackground,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
         },
      },
      target = {
         holder = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.TargetHolder,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         existenceFlag = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.TargetExistenceFlag,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         frontal = {
            factionIcon = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.TargetFactionIcon,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            muzzleIconModel = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.TargetMuzzleIconModel,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            muzzleIconBasement = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.TargetMuzzleIconBasement,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            muzzleIconAlternate = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.TargetMuzzleIconAlternate,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            classIcon = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.TargetClassIcon,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            text = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.TargetText,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
         },
         background = {
            common = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.TargetBackground,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
         },
      },
      remedy = {
         holder = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.RemedyHolder,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         frontal = {
            IS = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.RemedyIS,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            IC = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.RemedyIC,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            IM = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.RemedyIM,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
         },
         background = {
            IS = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.RemedyISBackground,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            IC = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.RemedyICBackground,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
            IM = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.RemedyIMBackground,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
            }),
         },
      },
      service = {
         holder = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.ServiceHolder,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         existenceFlag = GGC.ParameterContainer:Create({
            ctype = GGE.ContainerType.Element,
            cte = CTList.ServiceExistenceFlag,
            fixed = false,

            [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            end,
            [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
            end,
         }),
         frontal = {
            delimiter = GGC.ParameterContainer:Create({
               ctype = GGE.ContainerType.Element,
               cte = CTList.ServiceDelimiter,
               fixed = false,

               [GGE.RuntimeMethod.Adjuster] = function(in_meta)
               end,
               [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
               end,
            }),
         },
      },
   }
};

GGF.Mark(GGC.LineProvider);

GGC.LineProvider = GGF.TableSafeExtend(GGC.LineProvider, GGC.LayoutFrame);

GGF.CreateClassWorkaround(GGC.LineProvider);

local This = GGC.LineProvider;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-- Расположение объектов в соответствии с порядком
function This:AdjustLayoutOrder(in_meta)
   -- Relative offsets
   -- 14 for each icon and 8 between
   -- -29 -7 +15 (for 3)
   -- -18 +4 (for 2)
   -- -7 (for 1)
   -- тут переделано(вроде бы уже трижды...но это не точно); теперь зависимость функциональная
   --local layoutOrder = GGF.InnerInvoke(in_meta, "GetLayoutOrder");
   --local prevObjGeom = { width = 0, offsetX = 0 };
   --local prevObjGeomList = {};   -- саппортящий механизм для геометрии фонов
   local factoryInstance = GGF.InnerInvoke(in_meta, "GetFactoryInstance");
   local stringHeight = GGF.OuterInvoke(factoryInstance, "GetStringHeight");
   local sourceHolderDataOrder = {
      { GGF.InnerInvoke(in_meta, "GetSourceFactionIcon"), },
      { GGF.InnerInvoke(in_meta, "GetSourceMuzzleIconModel"), GGF.InnerInvoke(in_meta, "GetSourceMuzzleIconAlternate"), },
      { GGF.InnerInvoke(in_meta, "GetSourceClassIcon"), },
      { GGF.InnerInvoke(in_meta, "GetSourceText"), },
   };
   local remedyHolderDataOrder = {
      { GGF.InnerInvoke(in_meta, "GetRemedyIS"), },
      { GGF.InnerInvoke(in_meta, "GetRemedyIC"), },
      { GGF.InnerInvoke(in_meta, "GetRemedyIM"), },
   };
   local targetHolderDataOrder = {
      { GGF.InnerInvoke(in_meta, "GetTargetText"), },
      { GGF.InnerInvoke(in_meta, "GetTargetClassIcon"), },
      { GGF.InnerInvoke(in_meta, "GetTargetMuzzleIconModel"), GGF.InnerInvoke(in_meta, "GetTargetMuzzleIconAlternate"), },
      { GGF.InnerInvoke(in_meta, "GetTargetFactionIcon"), },
   };
   local backOrder = {
      { GGF.InnerInvoke(in_meta, "GetSourceBackground"), },
      { GGF.InnerInvoke(in_meta, "GetTargetBackground"), },
   };
   --GGF.OuterInvoke(in_meta, "");

   local src_width = GGF.InnerInvoke(in_meta, "adjustBlockLayoutOrder", sourceHolderDataOrder);
   local rem_width = GGF.InnerInvoke(in_meta, "adjustBlockLayoutOrder", remedyHolderDataOrder);
   local trg_width = GGF.InnerInvoke(in_meta, "adjustBlockLayoutOrder", targetHolderDataOrder);

   local rem_offset = GGF.TernExpSingle(GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSourceHolder"), "GetHidden"), 0, src_width);
   local trg_offset = GGF.TernExpSingle(GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSourceHolder"), "GetHidden"), rem_width, src_width + rem_width);
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetRemedyHolder"), "SetOffsetX", rem_offset);
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetTargetHolder"), "SetOffsetX", trg_offset);

   local srcBckg = GGF.InnerInvoke(in_meta, "GetSourceBackground");
   --GGF.OuterInvoke(srcBckg, "SetOffsetX", 0);
   GGF.OuterInvoke(srcBckg, "SetWidth", 
      GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSourceText"), "GetWidth") +
      GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSourceMuzzleIconModel"), "GetWidth") +
      GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSourceClassIcon"), "GetWidth") +
      GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetSourceFactionIcon"), "GetWidth")
   );

   local trgBckg = GGF.InnerInvoke(in_meta, "GetTargetBackground");
   --GGF.OuterInvoke(trgBckg, "SetOffsetX", GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetTargetText"), "GetOffsetX"));
   GGF.OuterInvoke(trgBckg, "SetWidth", 
      GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetTargetText"), "GetWidth") +
      GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetTargetMuzzleIconModel"), "GetWidth") +
      GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetTargetClassIcon"), "GetWidth") +
      GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetTargetFactionIcon"), "GetWidth")
   );
end

--
function This:adjustServiceObjects(in_meta)
   GGF.InnerInvoke(in_meta, "adjustSubjectSubsignature", "S");
   GGF.InnerInvoke(in_meta, "adjustSubjectSubsignature", "T");
   GGF.InnerInvoke(in_meta, "adjustRemedySubsignature");
   GGF.InnerInvoke(in_meta, "adjustServiceSubsignature");
end


function This:adjustSubjectSubsignature(in_meta, in_strType)
   local typeInserter = GGF.TernExpSingle(in_strType == "S", "Source", "Target");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "Get" .. typeInserter .. "Holder"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "Get" .. typeInserter .. "Text"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "Get" .. typeInserter .. "FactionIcon"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "Get" .. typeInserter .. "MuzzleIconBasement"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "Get" .. typeInserter .. "MuzzleIconModel"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "Get" .. typeInserter .. "MuzzleIconAlternate"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "Get" .. typeInserter .. "ClassIcon"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "Get" .. typeInserter .. "Background"), "Adjust");
end


function This:adjustRemedySubsignature(in_meta)
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetRemedyHolder"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetRemedyIS"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetRemedyIC"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetRemedyIM"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetRemedyISBackground"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetRemedyICBackground"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetRemedyIMBackground"), "Adjust");
end


function This:adjustServiceSubsignature(in_meta)
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetServiceHolder"), "Adjust");
   GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetServiceDelimiter"), "Adjust");
end

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

function This:adjustBlockLayoutOrder(in_meta, in_block)
   local offsetX = 0;
   for outerInd = 1, #in_block do
      for innerInd = 1, #in_block[outerInd] do
         local obj = in_block[outerInd][innerInd];
         --GGF.OuterInvoke(obj, "SetHeight", stringHeight);
         GGF.OuterInvoke(obj, "SetOffsetX", offsetX);
         if (not GGF.OuterInvoke(obj, "GetHidden")) then
            offsetX = offsetX + GGF.OuterInvoke(obj, "GetWidth");
         end
         --print("data offset:", offsetX);
      end
   end
   return offsetX;
end

function This:buildSubjectSubsignature(in_meta, in_strType)
   local factoryInstance = GGF.InnerInvoke(in_meta, "GetFactoryInstance");
   --GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetParentInstance"));--GGM.GUI.KillFeed:GetFrame();
   local strIndex = tostring(GGF.InnerInvoke(in_meta, "GetIndex"));
   local stringHeight = GGF.OuterInvoke(factoryInstance, "GetStringHeight");
   local factionIconUniSize = GGF.OuterInvoke(factoryInstance, "GetFactionIconUniSize");
   local classIconSide = GGF.OuterInvoke(factoryInstance, "GetClassIconSide");
   local muzzleDrawer = GGF.InnerInvoke(in_meta, "GetMuzzleDrawerInstance");
   local muzzleIconSide = GGF.OuterInvoke(muzzleDrawer, "GetMuzzleIconSide");
   local typeInserter = GGF.TernExpSingle(in_strType == "S", "Source", "Target");

   local holder = GGC.LayoutFrame:Create({}, {
      properties = {
         base = {
            parent = in_meta.object,
            wrapperName = in_strType .. "H_" .. strIndex,
         },
         size = {
            height = stringHeight,
         },
         anchor = {
            point = GGE.PointType.Left,
            relativeTo = in_meta.object,
            relativePoint = GGE.PointType.Left,
            offsetX = 0,
            offsetY = 0,
         },
      },
   });
   local ksFrame = GGF.INS.GetObjectRef(holder);
   GGF.InnerInvoke(in_meta, "Set" .. typeInserter .. "Holder", holder);
   -- Источник кила
   -- text(FS)
   local text = GGC.FontString:Create({}, {
      properties = {
         base = {
            --name = "ACR_KS_F_ST_FS_" .. tostring(#self.JR_ST_FS_OBJ + 1),
            parent = ksFrame,
            wrapperName = in_strType .. "T_" .. strIndex,
         },
         miscellaneous = {
            font = "ACR_LogStringDefault",
         },
         size = {
            height = stringHeight,
         },
         anchor = {
            point = GGE.PointType.Left,
            relativeTo = ksFrame,
            relativePoint = GGE.PointType.Left,
            justifyH = GGE.JustifyHType.Center,
            offsetX = 0,
            offsetY = 0,
         },
         shadow = {
            offset = {
               X = 2,
               Y = -2,
            },
         },
      },
   });
   --GGF.OuterInvoke(text, "AdjustParent");
   --GGF.OuterInvoke(text, "AdjustShadowOffset");
   --GGF.OuterInvoke(text, "Adjust");
   GGF.InnerInvoke(in_meta, "Set" .. typeInserter .. "Text", text);

   -- Иконка фракции источника
   -- factionIcon(STX)
   local factionIcon = GGC.StandardTexture:Create({}, {
      properties = {
         base = {
            parent = ksFrame,
            wrapperName = in_strType .. "FI_" .. strIndex,
         },
         miscellaneous = {
            layer = GGE.LayerType.Overlay,
         },
         size = {
            width = factionIconUniSize,
            height = factionIconUniSize,
         },
         anchor = {
            point = GGE.PointType.Left,
            relativeTo = ksFrame,
            relativePoint = GGE.PointType.Left,
            offsetX = 0,
            offsetY = 0,
         },
      },
   });
   --GGF.OuterInvoke(factionIcon, "AdjustParent");
   GGF.InnerInvoke(in_meta, "Set" .. typeInserter .. "FactionIcon", factionIcon);

   -- Модель мордахи источника
   -- MuzzleDrawer -> muzzleIconModelFrame
   local muzzleIconModel = GGF.OuterInvoke(muzzleDrawer, "GetNewMuzzleModelFrame", in_strType, strIndex, ksFrame);
   GGF.InnerInvoke(in_meta, "Set" .. typeInserter .. "MuzzleIconModel", muzzleIconModel);

      -- Фон мордахи источника
   -- MuzzleDrawer -> muzzleIconBasementFrame
   local muzzleIconBasement = GGF.OuterInvoke(muzzleDrawer, "GetNewMuzzleBackgroundFrame", in_strType, strIndex, ksFrame, GGF.INS.GetObjectRef(muzzleIconModel));
   GGF.InnerInvoke(in_meta, "Set" .. typeInserter .. "MuzzleIconBasement", muzzleIconBasement);

   -- Альтернатива мордахи источника
   -- muzzleIconAlternate(STX)
   local muzzleIconAlternate = GGC.StandardTexture:Create({}, {
      properties = {
         base = {
            parent = ksFrame,
            wrapperName = in_strType .. "MIA_" .. strIndex,
         },
         size = {
            width = muzzleIconSide,
            height = muzzleIconSide,
         },
         anchor = {
            point = GGE.PointType.Left,
            relativeTo = ksFrame,
            relativePoint = GGE.PointType.Left,
            offsetX = 0,
            offsetY = 0,
         },
      },
   });
   --GGF.OuterInvoke(muzzleIconAlternate, "AdjustParent");
   GGF.InnerInvoke(in_meta, "Set" .. typeInserter .. "MuzzleIconAlternate", muzzleIconAlternate);

   -- Иконка класса источника
   -- classIcon(STX)
   local classIcon = GGC.StandardTexture:Create({}, {
      properties = {
         base = {
            parent = ksFrame,
            wrapperName = in_strType .. "CI_" .. strIndex,
         },
         miscellaneous = {
            layer = GGE.LayerType.Overlay,
         },
         size = {
            width = classIconSide,
            height = classIconSide,
         },
         anchor = {
            point = GGE.PointType.Left,
            relativeTo = ksFrame,
            relativePoint = GGE.PointType.Left,
            offsetX = 0,
            offsetY = 0,
         },
      },
   });
   --GGF.OuterInvoke(classIcon, "AdjustParent");
   GGF.InnerInvoke(in_meta, "Set" .. typeInserter .. "ClassIcon", classIcon);

   -- Фон объектов источника(все иконки + текст)
   -- background(STX)
   local background = GGC.StandardTexture:Create({}, {
      properties = {
         base = {
            parent = ksFrame,
            wrapperName = in_strType .. "SB_" .. strIndex,
         },
         miscellaneous = {
            layer = GGE.LayerType.Background,
         },
         size = {
            width = classIconSide,
            height = classIconSide,
         },
         anchor = {
            point = GGE.PointType.Left,
            relativeTo = ksFrame,
            relativePoint = GGE.PointType.Left,
            offsetX = 0,
            offsetY = 0,
         },
      },
   });
   --GGF.OuterInvoke(background, "AdjustParent");
   GGF.InnerInvoke(in_meta, "Set" .. typeInserter .. "Background", background);

   GGF.InnerInvoke(in_meta, "Set" .. typeInserter .. "ExistenceFlag", false);
end

function This:buildRemedySubsignature(in_meta)
   local factoryInstance = GGF.InnerInvoke(in_meta, "GetFactoryInstance");
   local ksFrame = in_meta.object;--GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetParentInstance"));
   local strIndex = tostring(GGF.InnerInvoke(in_meta, "GetIndex"));
   local spellIconHeight = GGF.OuterInvoke(factoryInstance, "GetSpellIconHeight");
   local spellBackgroundHeight = GGF.OuterInvoke(factoryInstance, "GetSpellBackgroundHeight");
   local spellBackgroundInset = GGF.OuterInvoke(factoryInstance, "GetSpellBackgroundInset");
   local iconCriticalTexture = GGF.OuterInvoke(factoryInstance, "GetIconCriticalTexture");
   local iconMultistrikeTexture = GGF.OuterInvoke(factoryInstance, "GetIconMultistrikeTexture");
   local spellTypedBackgroundTexture = GGF.OuterInvoke(factoryInstance, "GetSpellTypedBackgroundTexture");
   local stringHeight = GGF.OuterInvoke(factoryInstance, "GetStringHeight");

   local holder = GGC.LayoutFrame:Create({}, {
      properties = {
         base = {
            parent = in_meta.object,
            wrapperName = "RH_" .. strIndex,
         },
         size = {
            height = stringHeight,
         },
         anchor = {
            point = GGE.PointType.Left,
            relativeTo = in_meta.object,
            relativePoint = GGE.PointType.Left,
            offsetX = 0,
            offsetY = 0,
         },
      },
   });
   local ksFrame = GGF.INS.GetObjectRef(holder);
   GGF.InnerInvoke(in_meta, "SetRemedyHolder", holder);
   -- Иконка индикатора спелла
   -- IS(STX)
   local IS = GGC.StandardTexture:Create({}, {
      properties = {
         base = {
            parent = ksFrame,
            wrapperName = "IS_" .. strIndex,
         },
         miscellaneous = {
            layer = GGE.LayerType.Artwork,
         },
         size = {
            width = spellIconHeight,
            height = spellIconHeight,
         },
         anchor = {
            point = GGE.PointType.Left,
            relativeTo = ksFrame,
            relativePoint = GGE.PointType.Left,
            offsetX = 0,
            offsetY = 0,
         },
      },
   });
   --GGF.OuterInvoke(IS, "AdjustParent");
   GGF.InnerInvoke(in_meta, "SetRemedyIS", IS);

   -- Иконка индикатора крита
   -- IС(STX)
   local IC = GGC.StandardTexture:Create({}, {
      properties = {
         base = {
            parent = ksFrame,
            wrapperName = "IC_" .. strIndex,
         },
         miscellaneous = {
            file = iconCriticalTexture,
            layer = GGE.LayerType.Artwork,
         },
         size = {
            width = spellIconHeight,
            height = spellIconHeight,
         },
         anchor = {
            point = GGE.PointType.Left,
            relativeTo = ksFrame,
            relativePoint = GGE.PointType.Left,
            offsetX = 0,
            offsetY = 0,
         },
      },
   });
   --GGF.OuterInvoke(IC, "AdjustParent");
   --GGF.OuterInvoke(IC, "AdjustFile");
   GGF.InnerInvoke(in_meta, "SetRemedyIC", IC);


   -- Иконка индикатора мультикаста
   -- IM(STX)
   local IM = GGC.StandardTexture:Create({}, {
      properties = {
         base = {
            parent = ksFrame,
            wrapperName = "IM_" .. strIndex,
         },
         miscellaneous = {
            file = iconMultistrikeTexture,
            layer = GGE.LayerType.Artwork,
         },
         size = {
            width = spellIconHeight,
            height = spellIconHeight,
         },
         anchor = {
            point = GGE.PointType.Left,
            relativeTo = ksFrame,
            relativePoint = GGE.PointType.Left,
            offsetX = 0,
            offsetY = 0,
         },
      },
   });
   --GGF.OuterInvoke(IM, "AdjustParent");
   --GGF.OuterInvoke(IM, "AdjustFile");
   GGF.InnerInvoke(in_meta, "SetRemedyIM", IM);

   -- Фон иконки индикатора спелла
   -- ISBackground(STX)
   local ISBackground = GGC.StandardTexture:Create({}, {
      properties = {
         base = {
            parent = ksFrame,
            wrapperName = "SBS_" .. strIndex,
         },
         miscellaneous = {
            file = spellTypedBackgroundTexture,
            layer = GGE.LayerType.Border,
         },
         size = {
            width = spellBackgroundHeight,
            height = spellBackgroundHeight + 2*spellBackgroundInset,
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = GGF.INS.GetObjectRef(IS),
            relativePoint = GGE.PointType.Center,
            offsetX = 0,
            offsetY = 0,
         },
      },
   });
   --GGF.OuterInvoke(ISBackground, "AdjustParent");
   --GGF.OuterInvoke(ISBackground, "AdjustFile");
   GGF.InnerInvoke(in_meta, "SetRemedyISBackground", ISBackground);

   -- Фон иконки индикатора крита
   -- ICBackground(STX)
   local ICBackground = GGC.StandardTexture:Create({}, {
      properties = {
         base = {
            parent = ksFrame,
            wrapperName = "SBC_" .. strIndex,
         },
         miscellaneous = {
            file = spellTypedBackgroundTexture,
            layer = GGE.LayerType.Border,
         },
         size = {
            width = spellBackgroundHeight,
            height = spellBackgroundHeight + 2*spellBackgroundInset,
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = GGF.INS.GetObjectRef(IC),
            relativePoint = GGE.PointType.Center,
            offsetX = 0,
            offsetY = 0,
         },
      },
   });
   --GGF.OuterInvoke(ICBackground, "AdjustParent");
   --GGF.OuterInvoke(ICBackground, "AdjustFile");
   GGF.InnerInvoke(in_meta, "SetRemedyICBackground", ICBackground);

   -- Фон иконки индикатора мультикаста
   -- IMBackground(STX)
   local IMBackground = GGC.StandardTexture:Create({}, {
      properties = {
         base = {
            parent = ksFrame,
            wrapperName = "SBM_" .. strIndex,
         },
         miscellaneous = {
            file = spellTypedBackgroundTexture,
            layer = GGE.LayerType.Border,
         },
         size = {
            width = spellBackgroundHeight,
            height = spellBackgroundHeight + 2*spellBackgroundInset,
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = GGF.INS.GetObjectRef(IM),
            relativePoint = GGE.PointType.Center,
            offsetX = 0,
            offsetY = 0,
         },
      },
   });
   --GGF.OuterInvoke(IMBackground, "AdjustParent");
   --GGF.OuterInvoke(IMBackground, "AdjustFile");
   GGF.InnerInvoke(in_meta, "SetRemedyIMBackground", IMBackground);
end

function This:buildServiceSubsignature(in_meta)
   local factoryInstance = GGF.InnerInvoke(in_meta, "GetFactoryInstance");
   --local ksFrame = in_meta.object;--GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetParentInstance"));
   local strIndex = tostring(GGF.InnerInvoke(in_meta, "GetIndex"));
   local stringHeight = GGF.OuterInvoke(factoryInstance, "GetStringHeight");
   local stringHeight = GGF.OuterInvoke(factoryInstance, "GetStringHeight");

   local holder = GGC.LayoutFrame:Create({}, {
      properties = {
         base = {
            parent = in_meta.object,
            wrapperName = "SRVH_" .. strIndex,
         },
         size = {
            height = stringHeight,
         },
         anchor = {
            point = GGE.PointType.Left,
            relativeTo = in_meta.object,
            relativePoint = GGE.PointType.Left,
            offsetX = 0,
            offsetY = 0,
         },
      },
   });
   local ksFrame = GGF.INS.GetObjectRef(holder);
   GGF.InnerInvoke(in_meta, "SetServiceHolder", holder);
   -- ServiceDelimiter(Разделитель)
   -- servDel(FS)
   local servDel = GGC.FontString:Create({}, {
      properties = {
         base = {
            parent = ksFrame,
            wrapperName = "SD_" .. strIndex,
         },
         miscellaneous = {
            layer = GGE.LayerType.Overlay,
            font = "ACR_LogStringDefault",
         },
         size = {
            width = GGM.GUI.KillFeed:GetWidth(),
            height = stringHeight,
         },
         anchor = {
            point = GGE.PointType.Center,
            relativeTo = ksFrame,
            relativePoint = GGE.PointType.Center,
            justifyH = GGE.JustifyHType.Center,
            offsetX = 0, 
            offsetY = 0,
         },
         shadow = {
            offset = {
               X = 0,
               Y = 0,
            },
         },
      },

   });
   --GGF.OuterInvoke(servDel, "AdjustParent");
   GGF.InnerInvoke(in_meta, "SetServiceDelimiter", servDel);

   GGF.InnerInvoke(in_meta, "SetServiceExistenceFlag", false);
end

-- Создание основных сервисных объектов
function This:createServiceObjects(in_meta)
   -- NOTE: общий аджастер вызывать нельзя, т.к. координата по Х и ширина - функциональные
   GGF.InnerInvoke(in_meta, "buildSubjectSubsignature", "S");
   GGF.InnerInvoke(in_meta, "buildSubjectSubsignature", "T");
   GGF.InnerInvoke(in_meta, "buildRemedySubsignature");
   GGF.InnerInvoke(in_meta, "buildServiceSubsignature");

   --GGF.InnerInvoke(in_meta, "SetSignature", signature);
end

--
function This:constructLayoutElementBundle(in_meta, in_object, in_counterToken)
   local offsetXPrototype = GGF.InnerInvoke(in_meta, "GetOffsetXPrototype");
   local widthPrototype = GGF.InnerInvoke(in_meta, "GetWidthPrototype");
   return {
      object = in_object,
      offsetXCounter = offsetXPrototype[in_counterToken],
      widthCounter = widthPrototype[in_counterToken],
   };
end

--
function This:constructBackgroundLayoutElementBundle(in_meta, in_object, in_start, in_finish, in_counterToken)
   local offsetXPrototype = GGF.InnerInvoke(in_meta, "GetOffsetXPrototype");
   local widthPrototype = GGF.InnerInvoke(in_meta, "GetWidthPrototype");
   return {
      object = in_object,
      range = {
         start = in_start,
         finish = in_finish,
      },
      offsetXCounter = offsetXPrototype[in_counterToken],
      widthCounter = widthPrototype[in_counterToken],
   };
end

--
function This:createObjectAnimation(in_meta, in_obj)
   local anim = GGC.FadeAnimation:Create({}, {
      properties = {
         base = {
            target = GGF.INS.GetObjectRef(in_obj),
         },
         parameters = {
            time = {
               duration = 5,
               delay = 5,
            },
         },
      }
   });
   GGF.OuterInvoke(anim, "Launch");
   return anim;
end

-- Установка аниматора исчезновения на линию
function This:fadeAnimateLine(in_meta)
   --local sgn = GGF.InnerInvoke(in_meta, "GetSignature");
   -- Animation
   GGF.InnerInvoke(in_meta, "createObjectAnimation", GGF.InnerInvoke(in_meta, "GetSourceHolder"));
   GGF.InnerInvoke(in_meta, "createObjectAnimation", GGF.InnerInvoke(in_meta, "GetRemedyHolder"));
   GGF.InnerInvoke(in_meta, "createObjectAnimation", GGF.InnerInvoke(in_meta, "GetTargetHolder"));
end