-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

local OBJSuffix = "LNFC";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   LineFactorySuffix = GGF.DockDomName(GGD.DefineIdentifier, "LineFactorySuffix"),
   LineFactory = GGF.DockDomName(GGD.ClassIdentifier, "LineFactory"),
};

local CTList = {
   LineProvider = GGF.CreateCT(OBJSuffix, "LineProvider", "Frame"),
   MuzzleDrawer = GGF.CreateCT(OBJSuffix, "MuzzleDrawer", "Frame"),
   PetResolver = GGF.CreateCT(OBJSuffix, "PetResolver", "Frame"),
   ParentInstance = GGF.CreateCT(OBJSuffix, "ParentInstance", "ParentInstance"),
   ObjectGapHor = GGF.CreateCT(OBJSuffix, "ObjectGapHor", "ObjectGapHor"),
   ObjectGapVer = GGF.CreateCT(OBJSuffix, "ObjectGapVer", "ObjectGapVer"),
   StringHeight = GGF.CreateCT(OBJSuffix, "StringHeight", "StringHeight"),
   FactionIconUniSize = GGF.CreateCT(OBJSuffix, "FactionIconUniSize", "FactionIconUniSize"),
   SpellBackgroundInset = GGF.CreateCT(OBJSuffix, "SpellBackgroundInset", "SpellBackgroundInset"),
   SpellTypedBackgroundTexture = GGF.CreateCT(OBJSuffix, "SpellTypedBackgroundTexture", "SpellTypedBackgroundTexture"),
   SpellIconHeight = GGF.CreateCT(OBJSuffix, "SpellIconHeight", "SpellIconHeight"),
   SpellBackgroundHeight = GGF.CreateCT(OBJSuffix, "SpellBackgroundHeight", "SpellBackgroundHeight"),
   IconCriticalTexture = GGF.CreateCT(OBJSuffix, "IconCriticalTexture", "IconCriticalTexture"),
   IconMultistrikeTexture = GGF.CreateCT(OBJSuffix, "IconMultistrikeTexture", "IconMultistrikeTexture"),
   ClassIconSide = GGF.CreateCT(OBJSuffix, "ClassIconSide", "ClassIconSide"),
   LineList = GGF.CreateCT(OBJSuffix, "LineList", "LineList"),
   CurrentLineIndex = GGF.CreateCT(OBJSuffix, "CurrentLineIndex", "CurrentLineIndex"),
};

GGF.GRegister(DFList.LineFactorySuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
   Frame = 0,
   ParentInstance = 0,
   ObjectGapHor = 10,
   ObjectGapVer = 26,
   StringHeight = 12,
   FactionIconUniSize = 26,
   SpellBackgroundInset = 4,
   SpellTypedBackgroundTexture = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\spell_background.tga",
   SpellIconHeight = 26,
   SpellBackgroundHeight = 30,
   IconCriticalTexture = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\critical.tga",
   IconMultistrikeTexture = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\multistrike.tga",
   ClassIconSide = 26,
   LineList = {},
   CurrentLineIndex = 1,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.LineFactory, {
   meta = {
      name = "Line Factory",
      mark = "",  -- авт.
      suffix = GGD.LineFactorySuffix,
   },
});

GGC.LineFactory.objects = {};

GGC.LineFactory.wrappers = {
   providers = {
      lineProvider = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.LineProvider,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      muzzleDrawer = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.MuzzleDrawer,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      petResolver = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.PetResolver,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.LineFactory.properties = {
   base = {
      parentInstance = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.ParentInstance,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
   miscellaneous = {
      objectGapHor = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.ObjectGapHor,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      objectGapVer = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.ObjectGapVer,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      stringHeight = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.StringHeight,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      factionIconUniSize = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.FactionIconUniSize,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      spellBackgroundInset = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.SpellBackgroundInset,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      spellTypedBackgroundTexture = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.SpellTypedBackgroundTexture,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      spellIconHeight = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.SpellIconHeight,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      spellBackgroundHeight = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.SpellBackgroundHeight,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      iconCriticalTexture = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.IconCriticalTexture,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      iconMultistrikeTexture = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.IconMultistrikeTexture,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      classIconSide = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.ClassIconSide,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.LineFactory.elements = {
   runtime = {
      lineList = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.LineList,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      currentLineIndex = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.CurrentLineIndex,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   }
};

GGF.Mark(GGC.LineFactory);

GGC.LineFactory = GGF.TableSafeExtend(GGC.LineFactory, GGC.AbstractContainer);

GGF.CreateClassWorkaround(GGC.LineFactory);

local This = GGC.LineFactory;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-- Запрос на линию
function This:GetLine(in_meta)
   local lineCount = GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetParentInstance"), "GetLineCount");
   local currentIndex = GGF.InnerInvoke(in_meta, "GetCurrentLineIndex");
   local nextIndex = 1;
   if (currentIndex < lineCount) then
      nextIndex = currentIndex + 1;
   end
   GGF.InnerInvoke(in_meta, "SetCurrentLineIndex", nextIndex);
   print("ri:", currentIndex, nextIndex);
   local line = GGF.InnerInvoke(in_meta, "GetLineList")[currentIndex];
   GGF.OuterInvoke(line, "SetHidden", false);
   return line;
end

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

-- Создание основных сервисных объектов
function This:createServiceObjects(in_meta)
   local parent = GGF.InnerInvoke(in_meta, "GetParentInstance");
   GGF.InnerInvoke(in_meta, "SetMuzzleDrawer", GGC.MuzzleDrawer:Create({}, {
      properties = {
         base = {
            parentInstance = parent,
         },
      },
   }));
   GGF.InnerInvoke(in_meta, "SetPetResolver", GGC.PetResolver:Create({}, {
      properties = {
         base = {
            parentInstance = parent,
         },
      },
   }));
   local lineCount = GGF.OuterInvoke(parent, "GetLineCount");
   local lineList = {};
   for lineIdx = 1,lineCount do
      lineList[#lineList + 1] = GGF.InnerInvoke(in_meta, "createDefaultLine", lineIdx);
   end
   GGF.InnerInvoke(in_meta, "SetLineList", lineList);
end

-- Создание дефолтной линии
function This:createDefaultLine(in_meta, in_idx)
   local stringHeight = GGF.InnerInvoke(in_meta, "GetStringHeight");
   local objectGapVer = GGF.InnerInvoke(in_meta, "GetObjectGapVer");
   local fontDelta = -(stringHeight + objectGapVer);
   local line = GGC.LineProvider:Create({}, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetParentInstance")),
            parentInstance = GGF.InnerInvoke(in_meta, "GetParentInstance"),
            muzzleDrawerInstance = GGF.InnerInvoke(in_meta, "GetMuzzleDrawer"),
            petResolverInstance = GGF.InnerInvoke(in_meta, "GetPetResolver"),
            factoryInstance = in_meta.instance,
            wrapperName = "LP_" .. tostring(in_idx),
         },
         miscellaneous = {
            hidden = true,
         },
         preset = {
            index = in_idx,
         },
         -- TODO: потом убрать!!!
         size = {
            width = 500,
            height = GGF.InnerInvoke(in_meta, "GetStringHeight"),
         },
         anchor = {
            point = GGE.PointType.TopLeft,
            relativeTo = GGF.INS.GetObjectRef(GGF.InnerInvoke(in_meta, "GetParentInstance")),
            relativePoint = GGE.PointType.TopLeft,
            offsetX = 0,
            offsetY = -fontDelta,
         },
         --backdrop = GGD.Backdrop.Segregate,
      },
   });
   GGF.OuterInvoke(line, "Adjust");
   return line;
end

--
function This:MasterRefresh(in_meta)
   local lineCount = GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetParentInstance"), "GetLineCount");
   local stringHeight = GGF.InnerInvoke(in_meta, "GetStringHeight");
   local objectGapVer = GGF.InnerInvoke(in_meta, "GetObjectGapVer");
   local lineList = GGF.InnerInvoke(in_meta, "GetLineList");
   local tvpId = 0; -- TODO: wtf?
   local currentIndex = GGF.InnerInvoke(in_meta, "GetCurrentLineIndex");
   if (currentIndex ~= 1) then
      currentIndex = currentIndex - 1;
   else
      currentIndex = lineCount;
   end
   local fontDelta = -(stringHeight + objectGapVer);
   GGF.OuterInvoke(lineList[currentIndex], "SetOffsetY", -20);
   for k,m in pairs(lineList) do
      --local v;
      --print("km : ",k, m)
      if k ~= currentIndex then
         GGF.OuterInvoke(m, "SetOffsetY", GGF.OuterInvoke(m, "GetOffsetY") + fontDelta);
      end
   end
end