-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

local OBJSuffix = "KF";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   EnvironmentDamageType = GGF.DockDomName(GGD.EnumerationIdentifier, "EnvironmentDamageType"),
   KillFeedFrameSuffix = GGF.DockDomName(GGD.DefineIdentifier, "KillFeedFrameSuffix"),
   KillFeedFrame = GGF.DockDomName(GGD.ClassIdentifier, "KillFeedFrame"),
};

local CTList = {
   KillFeedFrame = GGF.CreateCT(OBJSuffix, "KillFeedFrame", "Frame"),
   LineFactory = GGF.CreateCT(OBJSuffix, "LineFactory", "Frame"),
   LineLimit = GGF.CreateCT(OBJSuffix, "LineLimit", "LineLimit"),
   SpecificLengthBySymbol = GGF.CreateCT(OBJSuffix, "SpecificLengthBySymbol", "SpecificLengthBySymbol"),
   EnvironmentDamageTable = GGF.CreateCT(OBJSuffix, "EnvironmentDamageTable", "EnvironmentDamageTable"),
   ClassColorTable = GGF.CreateCT(OBJSuffix, "ClassColorTable", "ClassColorTable"),
   ClassIconTable = GGF.CreateCT(OBJSuffix, "ClassIconTable", "ClassIconTable"),
   ClassTokenTable = GGF.CreateCT(OBJSuffix, "ClassTokenTable", "ClassTokenTable"),
   ClassTokenReversedTable = GGF.CreateCT(OBJSuffix, "ClassTokenReversedTable", "ClassTokenReversedTable"),
   IconTexCoord = GGF.CreateCT(OBJSuffix, "IconTexCoord", "IconTexCoord"),
   MulticastedSpells = GGF.CreateCT(OBJSuffix, "MulticastedSpells", "MulticastedSpells"),
   FactionIconTable = GGF.CreateCT(OBJSuffix, "FactionIconTable", "FactionIconTable"),
   FactionBackgroundTextureTable = GGF.CreateCT(OBJSuffix, "FactionBackgroundTextureTable", "FactionBackgroundTextureTable"),
   LineCount = GGF.CreateCT(OBJSuffix, "LineCount", "LineCount"),
};

GGF.GRegister(DFList.KillFeedFrameSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.EnvironmentDamageType, {
   Drowning = "drowning",
   Falling = "falling",
   Fatigue = "fatigue",
   Fire = "fire",
   Lava = "lava",
   Slime = "slime",
   Undefined = "undefined",
});

GGF.GRegister(DFList.Default, {
   Frame = 0,
   LineLimit = 100,
   SpecificLengthBySymbol = 12,
   EnvironmentDamageTable = {
      [GGE.EnvironmentDamageType.Drowning] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\environment\\drowning_2.tga",
      [GGE.EnvironmentDamageType.Falling] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\environment\\falling_2.tga",
      [GGE.EnvironmentDamageType.Fatigue] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\environment\\fatigue_2.tga",
      [GGE.EnvironmentDamageType.Fire] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\environment\\fire_2.tga",
      [GGE.EnvironmentDamageType.Lava] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\environment\\lava_2.tga",
      [GGE.EnvironmentDamageType.Slime] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\environment\\slime.tga",
      [GGE.EnvironmentDamageType.Undefined] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\environment\\fatigue.tga",
   },
   ClassColorTable = {
      [1] = GGD.Color.Warrior,
      [2] = GGD.Color.Paladin,
      [3] = GGD.Color.Hunter,
      [4] = GGD.Color.Rogue,
      [5] = GGD.Color.Priest,
      [6] = GGD.Color.DeathKnight,
      [7] = GGD.Color.Shaman,
      [8] = GGD.Color.Mage,
      [9] = GGD.Color.Warlock,
      [10] = GGD.Color.Monk,
      [11] = GGD.Color.Druid,
      [12] = GGD.Color.DemonHunter,
      default = GGD.Color.Unclassed,
   },
   ClassIconTable = {
      [1] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Warrior.blp", -- Warrior
      [2] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Paladin.blp", -- Paladin
      [3] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Hunter.blp", -- Hunter
      [4] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Rogue.blp", -- Rogue
      [5] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Priest.blp", -- Priest
      [6] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_DeathKnight.blp", -- DeathKnight
      [7] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Shaman.blp", -- Shaman
      [8] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Mage.blp", -- Mage
      [9] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Warlock.blp", -- Warlock
      [10] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Monk.blp", -- Monk
      [11] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Druid.blp", -- DrUid
      [12] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_DemonHunter.blp", -- DemonHunter
      default = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\INV_Misc_QuestionMark.blp"
   },
   ClassTokenTable = {
      ["WARRIOR"] = 1,
      ["PALADIN"] = 2,
      ["HUNTER"] = 3,
      ["ROGUE"] = 4,
      ["PRIEST"] = 5,
      ["DEATHKNIGHT"] = 6,
      ["SHAMAN"] = 7,
      ["MAGE"] = 8,
      ["WARLOCK"] = 9,
      ["MONK"] = 10,
      ["DRUID"] = 11,
      ["DEMONHUNTER"] = 12,
   },
   ClassTokenReversedTable = {
      "WARRIOR",
      "PALADIN",
      "HUNTER",
      "ROGUE",
      "PRIEST",
      "DEATHKNIGHT",
      "SHAMAN",
      "MAGE",
      "WARLOCK",
      "MONK",
      "DRUID",
      "DEMONHUNTER",
   },
   IconTexCoord = {
      Pet   = { 0.00, 1.00, 0.00, 1.00 },
      noPet = { 0.05, 0.57, 0.05, 0.57 },
   },
   MulticastedSpells = {
      ["45284"] = true,
      ["45297"] = true,
      ["120588"] = true,
      ["77451"] = true,
   },
   FactionIconTable = {
      [GGE.UnitFaction.Alliance] = "Interface\\TargetingFrame\\UI-PVP-Alliance",
      [GGE.UnitFaction.Horde] = "Interface\\TargetingFrame\\UI-PVP-Horde",
      [GGE.UnitFaction.Default] = "Interface\\TargetingFrame\\UI-PVP-FFA",
   },
   FactionBackgroundTextureTable = {
      [GGE.UnitFaction.Default] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\gray_left.tga",
      [GGE.UnitFaction.Alliance] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\blue_left.tga",
      [GGE.UnitFaction.Horde] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\red_left.tga",
   },
   LineCount = 12,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.KillFeedFrame, {
   meta = {
      name = "KillFeed Frame",
      mark = "",  -- авт.
      suffix = GGD.KillFeedFrameSuffix,
   },
});

GGC.KillFeedFrame.objects = {
   frame = {
      line = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Object,
         cte = CTList.KillFeedFrame,
         fixed = true,

         -- NOTE: Нет такой функции
         -- NOTE: Да ладно? а внутри тогда что?..
         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.KillFeedFrame.Default)) then
               local killFeedFrame = CreateFrame(
                  "Frame",
                  GGF.GenerateOTName(
                     GGF.InnerInvoke(in_meta, "GetParentName"),
                     GGF.InnerInvoke(in_meta, "GetWrapperName"),
                     GGF.InnerInvoke(in_meta, "getClassSuffix")
                  ),
                  GGF.InnerInvoke(in_meta, "GetParent"),
                  GGF.VersionSplitterFunc({
                     {
                        versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.BFA },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              nil
                           )
                        end,
                     },
                     {
                        versionInterval = { GGD.TocVersionList.Shadowlands, GGD.TocVersionList.Invalid },
                        action = function()
                           return GGF.TernExpSingle(
                              GGF.InnerInvoke(in_meta, "IsTemplated"),
                              GGF.InnerInvoke(in_meta, "GetTemplate"),
                              --BackdropTemplateMixin and "BackdropTemplate" or nil
                              "BackdropTemplate" or nil
                           )
                        end,
                     },
                  })
               );
               GGF.InnerInvoke(in_meta, "ModifyKillFeedFrameOD", killFeedFrame);
            end
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.KillFeedFrame.wrappers = {
   providers = {
      lineFactory = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Wrapper,
         cte = CTList.LineFactory,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.KillFeedFrame.properties = {
   miscellaneous = {
      lineLimit = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.LineLimit,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            if (GGF.InnerInvoke(in_meta, "GetLineCount") > GGF.InnerInvoke(in_meta, "GetLineLimit")) then
               GGF.InnerInvoke(in_meta, "SetLineCount", GGF.InnerInvoke(in_meta, "GetLineLimit"));
            end
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGC.KillFeedFrame.elements = {
   runtime = {
      specificLengthBySymbol = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.SpecificLengthBySymbol,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      environmentDamageTable = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.EnvironmentDamageTable,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            local tbl = GGF.InnerInvoke(in_meta, "GetEnvironmentDamageTable");
            GGF.SetTableDefaultValue(tbl, tbl[GGE.EnvironmentDamageType.Undefined]);
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      classColorTable = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.ClassColorTable,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            local tbl = GGF.InnerInvoke(in_meta, "GetClassColorTable");
            GGF.SetTableDefaultValue(tbl, tbl.default);
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      classIconTable = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.ClassIconTable,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            local tbl = GGF.InnerInvoke(in_meta, "GetClassIconTable");
            GGF.SetTableDefaultValue(tbl, tbl.default);
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      classTokenTable = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.ClassTokenTable,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      classTokenReversedTable = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.ClassTokenReversedTable,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      iconTexCoord = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.IconTexCoord,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      multicastedSpells = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.MulticastedSpells,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      factionIconTable = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.FactionIconTable,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            local tbl = GGF.InnerInvoke(in_meta, "GetFactionIconTable");
            GGF.SetTableDefaultValue(tbl, tbl[GGE.UnitFaction.Default]);
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      factionBackgroundTextureTable = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.FactionBackgroundTextureTable,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
            local tbl = GGF.InnerInvoke(in_meta, "GetFactionBackgroundTextureTable");
            GGF.SetTableDefaultValue(tbl, tbl[GGE.UnitFaction.Default]);
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      lineCount = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Element,
         cte = CTList.LineCount,
         fixed = true,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   },
};

GGF.Mark(GGC.KillFeedFrame);

GGC.KillFeedFrame = GGF.TableSafeExtend(GGC.KillFeedFrame, GGC.StandardFrame);

GGF.CreateClassWorkaround(GGC.KillFeedFrame);

local This = GGC.KillFeedFrame;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-- Установка иконки класса по индексу
function This:getUnitClassIcon(in_meta, in_unitName, in_unitClassToken)
   --UFP = "UnitFramePortrait_UpDate";
   --UiCC = ACR_KS.classIcons.standartPane;
   --CIT = CLASS_ICON_TCOORDS;
   --t = CIT[in_unitClassToken]; 
   local factionIconTable = GGF.InnerInvoke(in_meta, "GetFactionIconTable");
   local classIconTable = GGF.InnerInvoke(in_meta, "GetClassIconTable");
   local classTokenTable = GGF.InnerInvoke(in_meta, "GetClassTokenTable");
   local iconTex = classIconTable[classTokenTable[in_unitClassToken]];
   --local iconTex = factionIconTable[nil];
   --print("class token:", in_unitClassToken, iconTex);
   return iconTex
end

-- Получение большей части инфы по игроку через гуид и запись в кластер
function This:getPlayerInfoByGUIDEnhanced(in_meta, in_guid)
   --print("guid enh", in_guid);
   local sessionPlayerCluster = GGM.ACR.SpecDispatcher:GetSessionPlayerCluster();
   if (sessionPlayerCluster.guid[in_guid] ~= nil) then
      return sessionPlayerCluster.guid[in_guid];
   end

   -- Pet/creature
   if (in_guid:find("Player") == nil) then
      --print("not a player");
      return nil;
   end
   
   local class, classFilename, race, raceFilename, sex, name, realm = GetPlayerInfoByGUID(in_guid);

   if (class == nil) then
      local counter = 0;

      while (class == nil and counter < 5) do
         class, classFilename, race, raceFilename, sex, name, realm = GetPlayerInfoByGUID(in_guid);
         counter = counter + 1;
      end

      if (class == nil) then
         return nil;
      end
   end

   --print("player/pet");
   --print(class, classFilename, race, raceFilename, sex, name, realm);
   sessionPlayerCluster.guid[in_guid] = {
      class = class,
      classFilename = classFilename,
      race = race,
      raceFilename = raceFilename,
      sex = sex,
      name = name,
      realm = realm,
   };

   return sessionPlayerCluster.guid[in_guid];
end

-- Определитель класса по GUiD чара(иногда стало критовать начиная с 7.2.5 примерно)
function This:getUnitClass(in_meta, in_idn)
   local classTokenTable = GGF.InnerInvoke(in_meta, "GetClassTokenTable");
   --print("idn: ", idn)
   --local gc, gcf, gr, grf, gs, gn, gr = GetPlayerInfoByGUiD(idn);
   local unitInfo = GGF.InnerInvoke(in_meta, "getPlayerInfoByGUIDEnhanced", in_idn);

   if unitInfo ~= nil then
      --print("class:", unitInfo.classFilename);
      return classTokenTable[unitInfo.classFilename];
   end

   --local _,_,uc = UnitClass(idn);
   --if uc ~= nil then
      --return uc;
   --end
   
   -- TODO: узнать/вспомнить/сколхозить что тут было раньше..
   local APCCluster = nil;
   -- ACR_SD.arenaPlayerClassCluster[idn]
   -- Там таблица спеков(т.е. асс. массив "имя" -> "спек")
   if APCCluster ~= nil then
      return APCCluster[idn];
   end

   return 0;
end

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

-- Создание основных сервисных объектов
function This:createServiceObjects(in_meta)
   GGF.InnerInvoke(in_meta, "SetLineFactory", GGC.LineFactory:Create({}, {
      properties = {
         base = {
            parentInstance = function() return in_meta.instance end,
         },
      },
   }));
end

-- Процессинг субъекта
-- NOTE: есть случаи, когда сурса нет в киллфиде(смерть от окр. среды)
function This:proceedSubject(in_meta, in_line, in_data, in_isSource)
   local fieldPrefix = GGF.TernExpSingle(in_isSource, "s", "t");
   local objectInserter = GGF.TernExpSingle(in_isSource, "Source", "Target");
   local existenceCheck = GGF.TernExpSingle(not in_isSource, true, in_data[fieldPrefix .. "pm_name"].data ~= nil and in_data[fieldPrefix .. "pm_name"].data ~= "SERVER");
   local specificLength = GGF.InnerInvoke(in_meta, "GetSpecificLengthBySymbol");
   local classColorTable = GGF.InnerInvoke(in_meta, "GetClassColorTable");
   local unusedLogFieldData = GGF.MDL.GetUnusedFiledData();
   local factionIconTable = GGF.InnerInvoke(in_meta, "GetFactionIconTable");
   local lineFactory = GGF.InnerInvoke(in_meta, "GetLineFactory");
   local muzzleDrawer = GGF.OuterInvoke(lineFactory, "GetMuzzleDrawer");
   local classTokenReversedTable = GGF.InnerInvoke(in_meta, "GetClassTokenReversedTable");
   local factionBackgroundTextureTable = GGF.InnerInvoke(in_meta, "GetFactionBackgroundTextureTable");
   local iconTexCoord = GGF.InnerInvoke(in_meta, "GetIconTexCoord");
   local environmentDamageTable = GGF.InnerInvoke(in_meta, "GetEnvironmentDamageTable");

   local typeFlag = "";
   local classIndex = 0;
   local srcExistingMultiplier = 0;
   local length = 0;

   print("nm:", in_data[fieldPrefix .. "pm_name"].data);
   GGF.OuterInvoke(in_line, "Set" .. objectInserter .. "ExistenceFlag", existenceCheck);
   if (existenceCheck) then
      typeFlag = GGM.ACR.CommonService:GetUnitTypeByFlag(in_data[fieldPrefix .. "pm_flag"].data);
      classIndex = GGF.InnerInvoke(in_meta, "getUnitClass", in_data[fieldPrefix .. "pm_id"].data);
      local name = GGM.ACR.CommonService:TruncRealm(in_data[fieldPrefix .. "pm_name"].data)
      local length = utf8len(name);

      GGF.OuterInvoke(GGF.OuterInvoke(in_line, "Get" .. objectInserter .. "Text"), "SetWidth", specificLength*length);
      GGF.OuterInvoke(GGF.OuterInvoke(in_line, "Get" .. objectInserter .. "Text"), "SetText", name);

      local clr = classColorTable[classIndex];
      GGF.OuterInvoke(GGF.OuterInvoke(in_line, "Get" .. objectInserter .. "Text"), "SetColor", clr);

      GGF.OuterInvoke(GGF.OuterInvoke(in_line, "Get" .. objectInserter .. "FactionIcon"), "SetFile", factionIconTable[in_data[fieldPrefix .. "pm_flag"].data]);
      local isNoOwner = in_data[fieldPrefix .. "pm_ownerName"].data == unusedLogFieldData;
      GGF.OuterInvoke(muzzleDrawer, "SetUnitMuzzle",
         not isNoOwner,
         true,
         GGF.OuterInvoke(in_line, "Get" .. objectInserter .. "MuzzleIconBasement"),
         GGF.OuterInvoke(in_line, "Get" .. objectInserter .. "MuzzleIconModel"),
         GGF.OuterInvoke(in_line, "Get" .. objectInserter .. "MuzzleIconAlternate"),
         in_data[fieldPrefix .. "pm_name"].data,
         in_data[fieldPrefix .. "pm_id"].data
      );

      local classIconFile = ""
      if (isNoOwner) then -- не пет
         classIconFile = GGF.InnerInvoke(in_meta, "getUnitClassIcon", in_data[fieldPrefix .. "pm_name"].data, classTokenReversedTable[classIndex]);
      else -- пет/прислужник
         --print("whos your daddy", in_data.spm_ownerName.data, unusedLogFieldData, in_data.spm_ownerName.data == unusedLogFieldData);
         if (in_data[fieldPrefix .. "pm_ownerId"].data ~= nil) then
            local ownerClassIndex = 0;
            local ownerData = GGM.ACR.SpecDispatcher:GetRuntimePlayerInfoByName(in_data[fieldPrefix .. "pm_ownerName"].data);
            if (ownerData == nil) then
               ownerData = GGM.GUI.PlayerCluster:GetPlayerInfoByGUIDEnhanced(in_data[fieldPrefix .. "pm_ownerId"].data);
            end
            classIconFile = GGF.InnerInvoke(in_meta, "getUnitClassIcon", in_data[fieldPrefix .. "pm_name"].data, ownerData.class);
         else
            classIconFile = GGF.InnerInvoke(in_meta, "getUnitClassIcon", in_data[fieldPrefix .. "pm_name"].data, nil);
         end
      end
      GGF.OuterInvoke(GGF.OuterInvoke(in_line, "Get" .. objectInserter .. "ClassIcon"), "SetFile", classIconFile);

      local texPrepath = GGM.ACR.CommonService:GetUnitFactionByFlag(in_data[fieldPrefix .. "pm_flag"].data);

      GGF.OuterInvoke(GGF.OuterInvoke(in_line, "Get" .. objectInserter .. "Background"), "SetFile", factionBackgroundTextureTable[texPrepath]);

      GGF.OuterInvoke(GGF.OuterInvoke(in_line, "Get" .. objectInserter .. "FactionIcon"), "SetTexCoord",
         iconTexCoord.noPet[1],
         iconTexCoord.noPet[2],
         iconTexCoord.noPet[3],
         iconTexCoord.noPet[4]
      );
   else
      if environmentDamageTable[string.lower(in_data.msd_id.data)] == nil then
         ATLASSERT(false);
         in_data.msd_id.data = "Undefined";
      end
   end
   --print("exists", existenceCheck);
   GGF.OuterInvoke(GGF.OuterInvoke(in_line, "Get" .. objectInserter .. "Holder"), "SetHidden", not existenceCheck);
end

--
function This:proceedRemedy(in_meta, in_line, in_data)
   local environmentDamageTable = GGF.InnerInvoke(in_meta, "GetEnvironmentDamageTable");
   local classIconTable = GGF.InnerInvoke(in_meta, "GetClassIconTable");
   if environmentDamageTable[string.lower(in_data.msd_id.data)] ~= environmentDamageTable[nil] then
      GGF.OuterInvoke(GGF.OuterInvoke(in_line, "GetRemedyIS"), "SetFile", environmentDamageTable[string.lower(in_data.msd_id.data)]);
   else
      local path = "";
      if (in_data.msd_id.data == 75) then   -- NOTE: autoshot
         path = classIconTable[nil];
      else
         path = GetSpellTexture(in_data.msd_id.data);--select(3,GetSpellInfo(in_data.spellID.data));
      end
      GGF.OuterInvoke(GGF.OuterInvoke(in_line, "GetRemedyIS"), "SetFile", path);
   end

   local criticalFlag = false;
   if in_data.msd_isCritical.data == true then
      criticalFlag = true;
   end

   -- multicast removed in Legion, but elemental Shaman mastery returned
   -- забавно, но это так, теперь мультикаст только для шаманов
   local multicastFlag = false;
   if GGF.InnerInvoke(in_meta, "GetMulticastedSpells")[tostring(in_data.msd_id.data)] ~= nil then
      multicastFlag = true;
   end

   -- спелл есть всегда, так что привязка сурса сюда логична
   GGF.OuterInvoke(GGF.OuterInvoke(in_line, "GetRemedyHolder"), "SetHidden", false);

   --tvpsgn.remedy.IMFlag = multicastFlag;
   GGF.OuterInvoke(GGF.OuterInvoke(in_line, "GetRemedyIM"), "SetHidden", not multicastFlag);
   GGF.OuterInvoke(GGF.OuterInvoke(in_line, "GetRemedyIMBackground"), "SetHidden", not multicastFlag);

   --tvpsgn.remedy.ICFlag = criticalFlag;
   GGF.OuterInvoke(GGF.OuterInvoke(in_line, "GetRemedyIC"), "SetHidden", not criticalFlag);
   GGF.OuterInvoke(GGF.OuterInvoke(in_line, "GetRemedyICBackground"), "SetHidden", not criticalFlag);
end

-- TODO: разгрести
-- Стандартный обработчик события кила из лога
function This:ProceedKill(in_meta, in_data)
   local lineFactory = GGF.InnerInvoke(in_meta, "GetLineFactory");
   --local statisticSkillNameBT = GGM.ACR.CombatInterceptor:GetStatisticSkillNameBT();
   local line = GGF.OuterInvoke(lineFactory, "GetLine");
   --local tvpsgn = GGF.OuterInvoke(line, "GetSignature");
   --tvpsgn, tvpId = GGM.GUI.AnLine:RequestLine();

   GGF.InnerInvoke(in_meta, "proceedRemedy", line, in_data);
   GGF.InnerInvoke(in_meta, "proceedSubject", line, in_data, true);
   GGF.InnerInvoke(in_meta, "proceedSubject", line, in_data, false);

   GGF.OuterInvoke(line, "AdjustLayoutOrder");

   -- Finishers
   --GGF.OuterInvoke(line, "fadeAnimateLine");
   GGF.OuterInvoke(lineFactory, "MasterRefresh");

   GGF.OuterInvoke(GGF.OuterInvoke(line, "GetServiceDelimiter"), "SetHidden", true);
end

-- Обработчик начала новой битвы(ставит в лог килов инфу о пб)
function This:ProceedBattleZoneEnter(in_meta, in_map, in_token)
   local line = GGF.OuterInvoke(GGF.InnerInvoke(in_meta, "GetLineFactory"), "GetLine");
   local lineFactory = GGF.InnerInvoke(in_meta, "GetLineFactory");
   --local tvpsgn, tvpId = GGM.GUI.AnLine:RequestLine();
   local service = GGF.OuterInvoke(line, "GetServiceDelimiter");

   GGF.OuterInvoke(line, "SetServiceExistenceFlag", true);

   GGF.OuterInvoke(service, "SetHidden", false);
   GGF.OuterInvoke(GGF.OuterInvoke(line, "GetRemedyHolder"), "SetHidden", true);
   GGF.OuterInvoke(GGF.OuterInvoke(line, "GetSourceHolder"), "SetHidden", true);
   GGF.OuterInvoke(GGF.OuterInvoke(line, "GetTargetHolder"), "SetHidden", true);
   --tvpsgn.ServiceDelimeter:SetPoint(ACR_KS.logGeometry.tvpS.servDel.anchor.point, ACR_KS.anFrame, ACR_KS.logGeometry.tvpS.servDel.anchor.relativeTo, ACR_KS.logGeometry.tvpS.servDel.anchor.offsetX(), ACR_KS.logGeometry.tvpS.servDel.anchor.offsetY());
   --tvpsgn.ServiceDelimeter:SetJustifyH(ACR_KS.logGeometry.tvpS.servDel.anchor.justifyH);
   --tvpsgn.ServiceDelimeter:SetHeight(ACR_KS.logGeometry.tvpS.servDel.size.height());
   --tvpsgn.ServiceDelimeter:SetWidth(ACR_KS.logGeometry.tvpS.servDel.size.width());   -- wide minus double border
   -- Если тут токен потянуть, то получится дедлок....так что пока так
   --tvpsgn.ServiceDelimeter:SetText("Z-"..Date("%d%m%y-%H%M%S").."-"..zid.." "..zname);  
   GGF.OuterInvoke(service, "SetText", in_map.."("..in_token..")");
   GGF.OuterInvoke(service, "SetAlpha", 1);

   --GGF.OuterInvoke(line, "fadeAnimateLine");

   GGF.OuterInvoke(lineFactory, "MasterRefresh");
   --self:anLogQueue_Add(tvpId);
end