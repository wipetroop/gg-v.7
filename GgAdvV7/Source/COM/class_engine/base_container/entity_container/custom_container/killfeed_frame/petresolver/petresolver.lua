-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

local OBJSuffix = "PTR";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   PetResolverSuffix = GGF.DockDomName(GGD.DefineIdentifier, "PetResolverSuffix"),
   PetResolver = GGF.DockDomName(GGD.ClassIdentifier, "PetResolver"),
};

local CTList = {
   PetResolver = GGF.CreateCT(OBJSuffix, "PetResolver", "PetResolver"),
   ParentInstance = GGF.CreateCT(OBJSuffix, "ParentInstance", "ParentInstance"),
};

GGF.GRegister(DFList.PetResolverSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
   PetResolver = 0,
   ParentInstance = 0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.PetResolver, {
   meta = {
      name = "Pet Resolver",
      mark = "",  -- авт.
      suffix = GGD.PetResolverSuffix,
   },
});

GGC.PetResolver.objects = {};

GGC.PetResolver.wrappers = {};

GGC.PetResolver.properties = {
   base = {
      parentInstance = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.ParentInstance,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
   }
};

GGC.PetResolver.elements = {};

GGF.Mark(GGC.PetResolver);

GGC.PetResolver = GGF.TableSafeExtend(GGC.PetResolver, GGC.AbstractContainer);

GGF.CreateClassWorkaround(GGC.PetResolver);

local This = GGC.PetResolver;

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------