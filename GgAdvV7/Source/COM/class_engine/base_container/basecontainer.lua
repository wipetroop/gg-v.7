-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "BC";

local DFList = {
	BaseContainerSuffix = GGF.DockDomName(GGD.DefineIdentifier, "BaseContainerSuffix"),
	BaseContainer = GGF.DockDomName(GGD.ClassIdentifier, "BaseContainer"),
};

GGF.GRegister(DFList.BaseContainerSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.BaseContainer, {
	meta = {
		name = "Base Container",
		mark = "",	-- авт.
		suffix = GGD.BaseContainerSuffix,	-- прототип(кривой аналог чисто виртуальной функции)
	},

	objectMap = {},
	instanceTemplate = {
		objects = {},
		wrappers = {},
		properties = {},
		elements = {},
	},
	adjustOrders = {
		object = {},
		wrapper = {},
		property = {},
		element = {},
	},
	--objectTemplate = {},
	--overrideParams = {},
   instanceList = {},
});

GGF.Mark(GGC.BaseContainer);

-- NOTE: тут его нельзя вызывать, т.к. еще нет методов объекта, да и экземпляр создавать нельзя
--GGF.CreateClassWorkaround(GGC.BaseContainer);

local This = GGC.BaseContainer;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- Конструктор базового класса
function This:Create(in_objectTemplate, in_overrideParams)
   local instance = self:getUnusedInstance();
   if (instance) then
      print("reusing instance:", instance, self:getClassSuffix());
      return instance;
   end
	instance = GGF.TableCopy(GGF.INS.GetClassInstanceTemplate());
	ATLASSERT(in_objectTemplate);
	ATLASSERT(in_overrideParams);
	-- NOTE: для параметрического контейнера только 1 аргумент
	--ATLASSERT(in_overrideParams);
	-- копируем все поля и методы класса прототипа
	GGF.INS.SetObjectTemplate(instance, in_objectTemplate);
	GGF.INS.SetOverrideParams(instance, in_overrideParams);
	-- NOTE: от этой концепции уходим
	--obj = GGF.TableCopy(self);
	GGF.INS.SetObjectSuffix(instance, self:getClassSuffix());
	-- NOTE: лишнее
	--print("obj cr:", obj);
	--in_instance = GGF.TableSafeExtend(in_instance, in_class.instanceTemplate);
	instance = GGF.TableSafeExtend(instance, self:getInstanceTemplate());
	--obj:initialize(in_objectTemplate, in_overrideParams);
	--print("cr", self:getClassSuffix(), obj);
	--GGF.TableDump(self:getInstanceTemplate());
	local meta = {
		instance = instance,
		class = self,
		object = 0,	-- WARNING: должен совпадать со значением по умолчанию
	};
	GGF.InitializeClass(meta);
   self:addInstance(instance);
	--print("ret inst:", obj);
	return instance;
end


-- Применение всех свойств к объекту класса(перегружен)
function This:Adjust(in_instance)
	ATLASSERT(false);
end


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Установка порядков загрузки
function This:SetAdjustOrders(in_orders)
	self.adjustOrders = GGF.TableCopy(in_orders);
end

function This:SetUnused(in_meta, in_unused)
   GGF.INS.SetUnused(in_meta.instance, in_unused);
end

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие суффикса класса(объектный должен с ним совпадать)
function This:getClassSuffix()
	return self.meta.suffix;
end


-- Взятие суффикса класса(внешний, только для GTemplateTree)
function This:GetClassSuffix()
	return self:getClassSuffix();
end


-- Взятие суффикса класса(объектный должен с ним совпадать)
function This:GetClassObjectMap()
	return self.objectMap;
end


-- Взятие метки класса(для проверки уровней)
function This:GetClassMark()
	return self.meta.mark;
end


-- NOTE: должен скопироваться из шаблона, а там генерится при создании карты джамперов
-- Взятие токена джампера объекта
function This:GetJumperToken()
	return self.meta.jrtoken;
end


-- Взятие порядков загрузки
function This:GetAdjustOrders()
	return self.adjustOrders;
end


-- Взятие шаблона параметров объекта
function This:getInstanceTemplate()
	return self.instanceTemplate;
end


-- Взятие свободного объекта из списка
function This:getUnusedInstance()
   for idx, obj in ipairs(self.instanceList) do
      if GGF.INS.GetUnused(obj) then
         return obj;
      end
   end
   return false;
end


function This:getUnused(in_meta)
   return GGF.INS.GetUnused(in_meta.instance);
end

-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "synchronizer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Добавление инстанса в список для конкретного объекта
function This:addInstance(in_instance)
   self.instanceList[#self.instanceList + 1] = in_instance;
end

-- NOTE: временно отключен из-за пересечений хвоста в некоторых классах геометрии
-- Генерация токена(хвостовой части функций обработки переменных)
function This:generateToken(in_path)
	local result = "";
	for v in string.gmatch(in_path, "%w+") do
		result = v;
	end

	local newResult = string.upper(result:sub(1,1));
	for i=2,#result do
		newResult = newResult .. result:sub(i,i);
	end

	return newResult;
end


-- Этот метод пытаемся убрать
function This:Invoke(in_instance, in_method, ...)
	ATLASSERT(false);
	--print("invoke:", self, ...);
	if (self:GetClassSuffix() ~= GGF.INS.GetObjectSuffix(in_instance)) then
		print("invoke suffix missmatch:", self:GetClassSuffix(), GGF.INS.GetObjectSuffix(in_instance));
		ATLASSERT(false);
	end
	if (self[in_method] == nil) then
		print("invoke no method:", in_method);
		ATLASSERT(false);
	end
	-- NOTE: объект туда же передается. но на след стадии
	return self[in_method](self, in_instance, GGF.INS.GetObjectRef(in_instance), ...);
end

-- Пусть там резервы лежат...может еще пригодятся


-- Обработка поля аттрибута/элемента
-- Алгоритм:
-- Если есть перегруженный параметр в конструкторе - ставим его,
-- если нет - чекаем наличие поля в объекте и, если его нет, ставим дефолт
--function COM_OE_BO.baseObject:processDataField(in_overrideParams, in_path)
	-- перевести на template
	--if (in_overrideParams.config[in_path] ~= nil) then
		--self:assignOverrideParam(in_overrideParams, in_path);
	--else
		--if (COM_CS.crossmodule:splitCallGet(self.config, in_path) == nil) then
			--self:assignDefault(in_path);
		--end
	--end
--end

-- NOTE: используется ТОЛЬКО на стадии инициализации объекта(дальше бесполезен)
-- Перегрузка параметра в шаблоне, если он имеется там и в перегруженном конфиге
--function COM_OE_BO.baseObject:modifyTemplateParameter(in_overrideConfig, in_path)
	--if (COM_CS.crossmodule:splitCallCheck(self.template.config, in_path)
		--and COM_CS.crossmodule:splitCallCheck(in_overrideConfig, in_path)) then
		--COM_CS.crossmodule:splitCallSet(self.template.config, in_path, COM_CS.crossmodule:splitCallGet(in_overrideConfig, in_path));
	--end
--end

-- Однострочное применение изменений свойств объекта
--function COM_OE_BO.baseObject:configChangeOption(in_path, in_val)
	--local isfn, val = self:transtypeExp(in_val);
	--local field = COM_CS.crossmodule:splitCallGet(self.config, in_path);
	--if (field ~= nil) then -- есть в объекте
		--if ((type(field) == 'function') == isfn) then
			--COM_CS.crossmodule:splitCallSet(self.config, in_path, in_val);
		--else
			--ATLASSERT(false);
		--end
	--else
		--ATLASSERT(false);
	--end
--end


-- Получение значения поля
--function COM_OE_BO.baseObject:dataValueGet(in_path, ...)
	--return select(2, self:transtypeExp(self:dataFieldGet(in_path), ...));
--end


-- Получение поля с проверкой на переопределение
--function COM_OE_BO.baseObject:dataFieldGet(in_path)
	--return COM_CS.crossmodule:splitCallGet(self.config, in_path);
--end


-- NOTE: вот только не надо спрашивать как это работает...я заебусь объяснять...(P.S. гребаная неявная типизация...)
-- Транстипное выполнение выражения(для морф типов в классах)
--function COM_OE_BO.baseObject:transtypeExp(in_type, ...)
	--return self:forceCall(COM_CS.crossmodule:ternExpComplex(type(in_type) == 'function', function(...) return in_type(...) end,  function() return in_type end), ...);
--end


-- Принудительный вызов второго аргумента
--function COM_OE_BO.baseObject:forceCall(in_bool, in_val, ...)
 	--return in_bool, in_val(...);
--end


