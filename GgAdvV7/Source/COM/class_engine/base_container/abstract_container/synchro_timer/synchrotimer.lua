-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "ST";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	SynchroTimerSuffix = GGF.DockDomName(GGD.DefineIdentifier, "SynchroTimerSuffix"),
	SynchroTimer = GGF.DockDomName(GGD.ClassIdentifier, "SynchroTimer"),
};

local CTList = {
	FictiveFrame = GGF.CreateCT(OBJSuffix, "FictiveFrame", "FictiveFrame"),
	Time = GGF.CreateCT(OBJSuffix, "Time", "Time"),
	CallbackList = GGF.CreateCT(OBJSuffix, "CallbackList", "CallbackList"),
	Tick = GGF.CreateCT(OBJSuffix, "Tick", "Tick"),
};

GGF.GRegister(DFList.SynchroTimerSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	FictiveFrame = 0,
	Time = 0,
	CallbackList = {},
	Tick = 0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.SynchroTimer, {
	meta = {
		name = "Synchro Timer",
		mark = "",	-- авт.
		suffix = GGD.SynchroTimerSuffix,
	},
});

GGC.SynchroTimer.objects = {
	frame = {
		fictive = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.FictiveFrame,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.SynchroTimer.wrappers = {};

GGC.SynchroTimer.properties = {
	parameters = {
		time = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Time,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		callbackList = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.CallbackList,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.SynchroTimer.elements = {
	runtime = {
		tick = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Tick,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGF.Mark(GGC.SynchroTimer);

GGC.SynchroTimer = GGF.TableSafeExtend(GGC.SynchroTimer, GGC.BlzContainer);

GGF.CreateClassWorkaround(GGC.SynchroTimer);

local This = GGC.SynchroTimer;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Реакция на внешний таймер
function This:Step(in_meta, in_elapsed)
	local tick = GGF.InnerInvoke(in_meta, "GetTick");
	local time = GGF.InnerInvoke(in_meta, "GetTime");
	local callbackList = GGF.InnerInvoke(in_meta, "GetCallbackList");
	tick = tick + in_elapsed;
	if (tick > time) then
		tick = 0;
		for token, callback in pairs(callbackList) do
			--print("c st run:", self:GetTick(), time, token);
			callback();
		end
	end
	GGF.InnerInvoke(in_meta, "SetTickOD", tick);
end


-- Добавления коллбека в список
function This:AddCallback(in_meta, in_token, in_callback)
	local callbackList = GGF.InnerInvoke(in_meta, "GetCallbackList");
	callbackList[in_token] = in_callback;
	GGF.InnerInvoke(in_meta, "SetCallbackList", callbackList);
end


-- Удаления коллбека из списка
function This:RemoveCallback(in_meta, in_token)
	local callbackList = GGF.InnerInvoke(in_meta, "GetCallbackList");
	callbackList[in_token] = nil;
	GGF.InnerInvoke(in_meta, "SetCallbackList", callbackList);
end