-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "RA";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	RotateAnimationSuffix = GGF.DockDomName(GGD.DefineIdentifier, "RotateAnimationSuffix"),
	RotateAnimation = GGF.DockDomName(GGD.ClassIdentifier, "RotateAnimation"),
};

local CTList = {
	Time = GGF.CreateCT(OBJSuffix, "Time", "Time"),
	StartAngle = GGF.CreateCT(OBJSuffix, "StartAngle", "StartAngle"),
	FinishAngle = GGF.CreateCT(OBJSuffix, "FinishAngle", "FinishAngle"),
};

GGF.GRegister(DFList.RotateAnimationSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Time = 0,
	StartAngle = 0,
	FinishAngle = 0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.RotateAnimation, {
	meta = {
		name = "Rotate Animation",
		mark = "",	-- авт.
		suffix = GGD.RotateAnimationSuffix,
	},
});

GGC.RotateAnimation.objects = {};

GGC.RotateAnimation.wrappers = {};

GGC.RotateAnimation.properties = {
	parameters = {
		time = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Time,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		startAngle = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.StartAngle,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		finishAngle = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.FinishAngle,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.RotateAnimation.elements = {};

GGF.Mark(GGC.RotateAnimation);

GGC.RotateAnimation = GGF.TableSafeExtend(GGC.RotateAnimation, GGC.StandardAnimation);

GGF.CreateClassWorkaround(GGC.RotateAnimation);

local This = GGC.RotateAnimation;


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Запуск анимации
function This:Launch(in_meta)
	local frame = GGF.InnerInvoke(in_meta, "GetTargetFrame");
	local ag = frame:CreateAnimationGroup();
	local ra = ag:CreateAnimation("Rotation");

	ra:SetDegrees(- GGF.InnerInvoke(in_meta, "GetFinishAngle"));
	ra:SetDuration(GGF.InnerInvoke(in_meta, "GetTime"));
	ra:SetSmoothing("OUT");
	ag:SetScript ("OnFinished", function(...)
		--print(in_frame:GetRotation());
		-- WARNING: костыль, т.к. объект откатывается после завершения анимации
		frame:SetRotation(math.rad(90 - GGF.InnerInvoke(in_meta, "GetStartAngle") + ra:GetDegrees()));
	end)
	ag:Play();

	return ra;
end