-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "SA";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	StandardAnimationSuffix = GGF.DockDomName(GGD.DefineIdentifier, "StandardAnimationSuffix"),
	StandardAnimation = GGF.DockDomName(GGD.ClassIdentifier, "StandardAnimation"),
};

local CTList = {
	TargetFrame = GGF.CreateCT(OBJSuffix, "TargetFrame", "TargetFrame"),
};

GGF.GRegister(DFList.StandardAnimationSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	TargetFrame = 0,
}, GGE.RegValType.Default);

--GGF.GRegister(DFList.DDelay, 0);

GGF.GRegister(DFList.StandardAnimation, {
	meta = {
		name = "Standard Animation",
		mark = "",	-- авт.
		suffix = GGD.StandardAnimationSuffix,
	},
});

GGC.StandardAnimation.objects = {};

GGC.StandardAnimation.wrappers = {};

GGC.StandardAnimation.properties = {
	base = {
		target = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.TargetFrame,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.StandardAnimation.elements = {};

GGF.Mark(GGC.StandardAnimation);

GGC.StandardAnimation = GGF.TableSafeExtend(GGC.StandardAnimation, GGC.EntityContainer);
--GGC.StandardAnimation = GGF.TableSafeExtend(GGC.StandardAnimation, GGC.AbstractContainer);	-- NOTE: не факт

GGF.CreateClassWorkaround(GGC.StandardAnimation);

local This = GGC.StandardAnimation;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Применение всех свойств к объекту класса
function This:Adjust()
	-- NOTE: затычка, чтоб не резать в уях целый стейдж
end

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------