-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "BA";

local DFList = {
	BlinkAnimationType = GGF.DockDomName(GGD.EnumerationIdentifier, "BlinkAnimationType"),
	BlinkAnimationState = GGF.DockDomName(GGD.EnumerationIdentifier, "BlinkAnimationState"),
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	BlinkAnimationSuffix = GGF.DockDomName(GGD.DefineIdentifier, "BlinkAnimationSuffix"),
	BlinkAnimation = GGF.DockDomName(GGD.ClassIdentifier, "BlinkAnimation"),
};

local CTList = {
   Type = GGF.CreateCT(OBJSuffix, "Type", "Type"),
	Interval = GGF.CreateCT(OBJSuffix, "Interval", "Interval"),
	Duration = GGF.CreateCT(OBJSuffix, "Duration", "Duration"),
	Delay = GGF.CreateCT(OBJSuffix, "Delay", "Delay"),
	Sound = GGF.CreateCT(OBJSuffix, "Sound", "Sound"),
   State = GGF.CreateCT(OBJSuffix, "State", "State"),
   AnimationGroupForward = GGF.CreateCT(OBJSuffix, "AnimationGroupForward", "Frame"),
   AnimationGroupBackward = GGF.CreateCT(OBJSuffix, "AnimationGroupBackward", "Frame"),
   AnimationInstanceForward = GGF.CreateCT(OBJSuffix, "AnimationInstanceForward", "Frame"),
   AnimationInstanceBackward = GGF.CreateCT(OBJSuffix, "AnimationInstanceBackward", "Frame"),
};

GGF.GRegister(DFList.BlinkAnimationSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.BlinkAnimationType, {
	Single = "SINGLE",
   Limited = "LIMITED",
	Cycled = "CYCLED",
});

GGF.GRegister(DFList.BlinkAnimationState, {
	Running = "RUNNING",
	Stopped = "STOPPED",
});

GGF.GRegister(DFList.Default, {
   Frame = 0,
   Type = GGE.BlinkAnimationType.Cycled,
	Interval = 0,
	Duration = 0,	-- interval/7(check) interval/50(alert)
	Delay = 0,		-- interval/7(check) interval/2 - interval/100(alert)
	Sound = "",
   State = GGE.BlinkAnimationState.Stopped,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.BlinkAnimation, {
	meta = {
		name = "Blink Animation",
		mark = "",	-- авт.
		suffix = GGD.BlinkAnimationSuffix,
	},
});

GGC.BlinkAnimation.objects = {};

GGC.BlinkAnimation.wrappers = {};

GGC.BlinkAnimation.properties = {
	parameters = {
      type = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.Type,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
		time = {
			interval = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.Interval,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			duration = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.Duration,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			delay = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.Delay,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
		sound = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Sound,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.BlinkAnimation.elements = {
	runtime = {
		state = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.State,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      animationGroupForward = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.AnimationGroupForward,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      animationGroupBackward = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.AnimationGroupBackward,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      animationInstanceForward = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.AnimationInstanceForward,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
      animationInstanceBackward = GGC.ParameterContainer:Create({
         ctype = GGE.ContainerType.Property,
         cte = CTList.AnimationInstanceBackward,
         fixed = false,

         [GGE.RuntimeMethod.Adjuster] = function(in_meta)
         end,
         [GGE.RuntimeMethod.Synchronizer] = function(in_meta)
         end,
      }),
	},
};

GGF.Mark(GGC.BlinkAnimation);

GGC.BlinkAnimation = GGF.TableSafeExtend(GGC.BlinkAnimation, GGC.StandardAnimation);

GGF.CreateClassWorkaround(GGC.BlinkAnimation);

local This = GGC.BlinkAnimation;


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


function This:prepareStageForward(in_meta)
   local frame = GGF.InnerInvoke(in_meta, "GetTargetFrame");
   local interval = GGF.InnerInvoke(in_meta, "GetInterval");
   local sound = GGF.InnerInvoke(in_meta, "GetSound");
   local animationGroupForward = GGF.InnerInvoke(in_meta, "GetAnimationGroupForward");
   local animationInstanceForward = GGF.InnerInvoke(in_meta, "GetAnimationInstanceForward");

   animationInstanceForward:SetOrder(1);
   --animationGroupForward:SetLooping("BOUNCE");
   GGF.VersionSplitterProc({
      {
         versionInterval = {GGD.TocVersionList.Vanilla, GGD.TocVersionList.Draenor},
         action = function()
            animationInstanceForward:SetChange(1);
         end,
      },
      {
         versionInterval = {GGD.TocVersionList.Legion, GGD.TocVersionList.Invalid},
         action = function()
            animationInstanceForward:SetFromAlpha(0);
            animationInstanceForward:SetToAlpha(1);
         end,
      },
   });
   --a4:SetDuration(interval/7);
   animationInstanceForward:SetDuration(3*interval/10);
   --a4:SetStartDelay(interval/7);
   animationInstanceForward:SetStartDelay(interval/10);

   animationGroupForward:SetScript("OnFinished", function()
      --animationGroupForward:Restart();
      frame:SetAlpha(1);
      GGF.InnerInvoke(in_meta, "animationStageBackward");
   end)
end

function This:prepareStageBackward(in_meta)
   local frame = GGF.InnerInvoke(in_meta, "GetTargetFrame");
   local interval = GGF.InnerInvoke(in_meta, "GetInterval");
   local sound = GGF.InnerInvoke(in_meta, "GetSound");
   local animationGroupBackward = GGF.InnerInvoke(in_meta, "GetAnimationGroupBackward");
   local animationInstanceBackward = GGF.InnerInvoke(in_meta, "GetAnimationInstanceBackward");

   animationInstanceBackward:SetOrder(-1);
   GGF.VersionSplitterProc({
      {
         versionInterval = {GGD.TocVersionList.Vanilla, GGD.TocVersionList.Draenor},
         action = function()
            animationInstanceBackward:SetChange(-1);
         end,
      },
      {
         versionInterval = {GGD.TocVersionList.Legion, GGD.TocVersionList.Invalid},
         action = function()
            animationInstanceBackward:SetFromAlpha(1);
            animationInstanceBackward:SetToAlpha(0);
         end,
      },
   });
   --a4:SetDuration(interval/7);
   animationInstanceBackward:SetDuration(2*interval/10);
   --a4:SetStartDelay(interval/7);
   animationInstanceBackward:SetStartDelay(2*interval/10);

   animationGroupBackward:SetScript("OnFinished", function()
      --animationGroupBackward:Restart();
      frame:SetAlpha(0);
      GGF.InnerInvoke(in_meta, "animationStageForward");
   end)
end

-- Стык для зацикленной анимации
function This:animationStageForward(in_meta)
   if (GGF.InnerInvoke(in_meta, "GetState") == GGE.BlinkAnimationState.Stopped) then
      return;
   end
   local interval = GGF.InnerInvoke(in_meta, "GetInterval");
   local sound = GGF.InnerInvoke(in_meta, "GetSound");
   local animationGroupForward = GGF.InnerInvoke(in_meta, "GetAnimationGroupForward");

   -- TODO: как-то бы заменить это дело, а то много объектов плодится...
   local timer = GGC.TriggerTimer:Create({
      time = interval/14,
      callback = function() PlaySoundFile(sound) end
   });
   --C_Timer.NewTimer(interval/14, function() PlaySoundFile(sound) end);

   animationGroupForward:Play();
end

function This:animationStageBackward(in_meta)
   local animationGroupBackward = GGF.InnerInvoke(in_meta, "GetAnimationGroupBackward");
   --PlaySoundFile(sound);

   animationGroupBackward:Play();
end

-- Запуск анимации
function This:Launch(in_meta)
   if (GGF.InnerInvoke(in_meta, "GetState") == GGE.BlinkAnimationState.Running) then
      return;
   end
   local frame = GGF.InnerInvoke(in_meta, "GetTargetFrame");
   local animationGroupForward = frame:CreateAnimationGroup();
   local animationGroupBackward = frame:CreateAnimationGroup();
   GGF.InnerInvoke(in_meta, "SetAnimationGroupForward", animationGroupForward);
   GGF.InnerInvoke(in_meta, "SetAnimationInstanceForward", animationGroupForward:CreateAnimation("Alpha"));
   GGF.InnerInvoke(in_meta, "SetAnimationGroupBackward", animationGroupBackward);
   GGF.InnerInvoke(in_meta, "SetAnimationInstanceBackward", animationGroupBackward:CreateAnimation("Alpha"));
   GGF.InnerInvoke(in_meta, "prepareStageForward");
   GGF.InnerInvoke(in_meta, "prepareStageBackward");

   GGF.InnerInvoke(in_meta, "SetState", GGE.BlinkAnimationState.Running);

   GGF.InnerInvoke(in_meta, "animationStageForward");
end

-- Функция шага анимации
function This:Step(in_meta)
end

-- Функция остановки анимации
function This:Stop(in_meta)
   GGF.InnerInvoke(in_meta, "SetState", GGE.BlinkAnimationState.Stopped);
end