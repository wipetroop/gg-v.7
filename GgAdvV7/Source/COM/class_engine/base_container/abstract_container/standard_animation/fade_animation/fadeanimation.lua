-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------


local OBJSuffix = "FA";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	FadeAnimationSuffix = GGF.DockDomName(GGD.DefineIdentifier, "FadeAnimationSuffix"),
	FadeAnimation = GGF.DockDomName(GGD.ClassIdentifier, "FadeAnimation"),
};

local CTList = {
	Duration = GGF.CreateCT(OBJSuffix, "DurationTime", "Time"),
	Delay = GGF.CreateCT(OBJSuffix, "DelayTime", "Delay"),
};

GGF.GRegister(DFList.FadeAnimationSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.Default, {
	Time = 0,
	Delay = 0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.FadeAnimation, {
	meta = {
		name = "Fade Animation",
		mark = "",	-- авт.
		suffix = GGD.FadeAnimationSuffix,
	},
});

GGC.FadeAnimation.objects = {};

GGC.FadeAnimation.wrappers = {};

GGC.FadeAnimation.properties = {
	parameters = {
		time = {
			duration = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.Duration,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
			delay = GGC.ParameterContainer:Create({
				ctype = GGE.ContainerType.Property,
				cte = CTList.Delay,
				fixed = false,

				[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				end,
				[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				end,
			}),
		},
	},
};

GGC.FadeAnimation.elements = {};

GGF.Mark(GGC.FadeAnimation);

GGC.FadeAnimation = GGF.TableSafeExtend(GGC.FadeAnimation, GGC.StandardAnimation);

GGF.CreateClassWorkaround(GGC.FadeAnimation);

local This = GGC.FadeAnimation;


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Запуск анимации
function This:Launch(in_meta)
	-- NOTE: метод реентерабельный, если че
	local frame = GGF.InnerInvoke(in_meta, "GetTargetFrame");
	local ag = frame:CreateAnimationGroup();
	local a4 = ag:CreateAnimation("Alpha");

	-- для тестов, если не нужна анимация исчезновения
	--if (true) then
		--return;
	--end

	frame:SetAlpha(1);
	a4:SetOrder(1);
	a4:SetFromAlpha(1);
	a4:SetToAlpha(0);
	a4:SetDuration(GGF.InnerInvoke(in_meta, "GetDurationTime"));
	a4:SetStartDelay(GGF.InnerInvoke(in_meta, "GetDelayTime"));
	ag:SetScript ("OnFinished", function()
		frame:SetAlpha(0);
	end)

	ag:Play();
end