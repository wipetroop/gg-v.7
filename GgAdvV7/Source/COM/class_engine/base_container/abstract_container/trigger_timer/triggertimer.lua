-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

local OBJSuffix = "TT";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	TriggerTimerSuffix = GGF.DockDomName(GGD.DefineIdentifier, "TriggerTimerSuffix"),
	TriggerTimer = GGF.DockDomName(GGD.ClassIdentifier, "TriggerTimer"),
};

GGF.GRegister(DFList.TriggerTimerSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.TriggerTimer, {
	meta = {
		name = "Trigger Timer",
		mark = "",	-- авт.
		suffix = GGD.TriggerTimerSuffix,
	},

	--time = 0,
	--callback = function() end,

	--frame = CreateFrame("FRAME"),
	--tick = 0,
});

GGF.Mark(GGC.TriggerTimer);

GGC.TriggerTimer = GGF.TableSafeExtend(GGC.TriggerTimer, GGC.AbstractContainer);
GGC.TriggerTimer = GGF.TableSafeExtend(GGC.TriggerTimer, GGC.ServiceContainer);

GGF.CreateClassWorkaround(GGC.TriggerTimer);

local This = GGC.TriggerTimer;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- Инициализатор базового класса(перегружен)
function This:Create(in_overrideParams)
	local obj = {
		frame = CreateFrame("FRAME"),
		callback = in_overrideParams.callback,
		tick = 0,
		time = in_overrideParams.time,
	};
	--self:safeAssignValue(in_overrideParams, "time");
	--self:safeAssignValue(in_overrideParams, "callback");

	obj.frame:SetScript("OnUpdate", function(in_self, in_elapsed) self:timer(in_elapsed, obj) end);
end


-- Применение всех свойств к объекту класса(перегружен на пустой)
function This:Adjust()
end


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Таймер
function This:timer(in_elapsed, in_instance)
	in_instance.tick = in_instance.tick + in_elapsed;
	if (in_instance.tick > in_instance.time) then
		in_instance.frame:SetScript("OnUpdate", nil);
		in_instance.frame = nil;
		in_instance.callback();
	end
end
