-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

-- NOTE: не добавляет функционала, просто указывает на принадлежность кастомному типу

local OBJSuffix = "AC";

local DFList = {
	Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
	AbstractContainerSuffix = GGF.DockDomName(GGD.DefineIdentifier, "AbstractContainerSuffix"),
	AbstractContainer = GGF.DockDomName(GGD.ClassIdentifier, "AbstractContainer"),
};

GGF.GRegister(DFList.AbstractContainerSuffix, OBJSuffix, GGE.RegValType.Suffix);

GGF.GRegister(DFList.AbstractContainer, {
	meta = {
		name = "Abstract Container",
		mark = "",	-- авт.
		suffix = GGD.AbstractContainerSuffix,
	},
	--objPropList = {},	-- TODO: выяснить, что это
});

GGF.Mark(GGC.AbstractContainer);

GGC.AbstractContainer = GGF.TableSafeExtend(GGC.AbstractContainer, GGC.BaseContainer);

GGF.CreateClassWorkaround(GGC.AbstractContainer);

local This = GGC.AbstractContainer;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------