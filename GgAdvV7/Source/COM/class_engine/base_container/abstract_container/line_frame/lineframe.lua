-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

-- template

-- properties = {
--    base = {
--       suffix = ,
--    },
--    miscellaneous = {
--    hidden = ,
--       color = ,
--    },
--    size = {
--       width = ,
--    },
--    anchor = {
--       relativeTo = ,
--       relativePoint = ,
--       startOffsetX = ,
--       startOffsetY = ,
--       endOffsetX = ,
--       endOffsetY = ,
--    },
-- }

local OBJSuffix = "LNF";

local DFList = {
   Default = GGF.DockDomName(GGD.DefineIdentifier, OBJSuffix, GGD.DefaultIdentifier),
   LineFrameSuffix = GGF.DockDomName(GGD.DefineIdentifier, "LineFrameSuffix"),
   LineFrame = GGF.DockDomName(GGD.ClassIdentifier, "LineFrame"),
};

local CTList = {
   LineFrame = GGF.CreateCT(OBJSuffix, "LineFrame", "Frame"),
   Color = GGF.CreateCT(OBJSuffix, "Color", "Color"),
   Hidden = GGF.CreateCT(OBJSuffix, "Hidden", "Hidden"),
   AnchorType = GGF.CreateCT(OBJSuffix, "AnchorType", "AnchorType"),
   Width = GGF.CreateCT(OBJSuffix, "Width", "Width"),
   RelativeTo = GGF.CreateCT(OBJSuffix, "RelativeTo", "RelativeTo"),
   RelativePoint = GGF.CreateCT(OBJSuffix, "RelativePoint", "RelativePoint"),
   StartOffsetX = GGF.CreateCT(OBJSuffix, "StartOffsetX", "StartOffsetX"),
   StartOffsetY = GGF.CreateCT(OBJSuffix, "StartOffsetY", "StartOffsetY"),
   EndOffsetX = GGF.CreateCT(OBJSuffix, "EndOffsetX", "EndOffsetX"),
   EndOffsetY = GGF.CreateCT(OBJSuffix, "EndOffsetY", "EndOffsetY"),
};

GGF.GRegister(DFList.LineFrameSuffix, OBJSuffix, GGE.RegValType.Suffix);   -- NOTE: LF уже есть

GGF.GRegister(DFList.Default, {
   Frame = 0,
   Color = 0,
   Hidden = false,
   AnchorType = GGE.AnchorType.Double,
   Width = 50,
   RelativeTo = function(in_meta) return UIParent end,
   RelativePoint = GGE.PointType.TopLeft,
   StartOffsetX = 0,
   StartOffsetY = 0,
   EndOffsetX = 0,
   EndOffsetY = 0,
}, GGE.RegValType.Default);

GGF.GRegister(DFList.LineFrame, {
   meta = {
      name = "Line Frame",
      mark = "",	-- авт.
      suffix = GGD.LineFrameSuffix,
   },
});

GGC.LineFrame.objects = {
	frame = {
		line = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Object,
			cte = CTList.LineFrame,
			fixed = true,

			-- NOTE: Нет такой функции
			-- NOTE: Да ладно? а внутри тогда что?..
			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (in_meta.object == GGF.GetDefaultValue(OBJSuffix, CTList.LineFrame.Default)) then
					local lineFrame = GGF.InnerInvoke(in_meta, "GetParent"):CreateTexture(
						GGF.GenerateOTName(
							GGF.InnerInvoke(in_meta, "GetParentName"),
							GGF.InnerInvoke(in_meta, "GetWrapperName"),
							GGF.InnerInvoke(in_meta, "getClassSuffix")
						),
						GGF.TernExpSingle(
							GGF.InnerInvoke(in_meta, "IsTemplated"),
							GGF.InnerInvoke(in_meta, "GetTemplate"),
							nil
						)
					);
					GGF.InnerInvoke(in_meta, "ModifyLineFrameOD", lineFrame);
					-- WARNING: не переделывать, автоматика еще не отработала изменение корневого объекта
					--GGF.InnerInvoke(in_meta, "recountLineGeometry");
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGC.LineFrame.wrappers = {};

GGC.LineFrame.properties = {
	miscellaneous = {
		color = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Color,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "recountLineGeometry");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
		hidden = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Hidden,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				if (GGF.InnerInvoke(in_meta, "GetHidden")) then
					in_meta.object:Hide();
				else
					in_meta.object:Show();
				end
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
				GGF.InnerInvoke(in_meta, "ModifyHidden", in_meta.object:IsHidden());
			end,
		}),
		anchorType = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.AnchorType,
			fixed = true,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
	size = {
		width = GGC.ParameterContainer:Create({
			ctype = GGE.ContainerType.Property,
			cte = CTList.Width,
			fixed = false,

			[GGE.RuntimeMethod.Adjuster] = function(in_meta)
				GGF.InnerInvoke(in_meta, "recountLineGeometry");
			end,
			[GGE.RuntimeMethod.Synchronizer] = function(in_meta)
			end,
		}),
	},
};

GGF.Mark(GGC.LineFrame);

GGC.LineFrame = GGF.TableSafeExtend(GGC.LineFrame, GGC.StandardTexture);

GGF.CreateClassWorkaround(GGC.LineFrame);

local This = GGC.LineFrame;


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "modify" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "adjuster" block
-------------------------------------------------------------------------------


-- Установка точки привязки исходного объекта
function This:AdjustAnchor(in_meta)
	GGF.InnerInvoke(in_meta, "recountLineGeometry");
end


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------


-- Пересчет геометрии объекта
function This:recountLineGeometry(in_meta)
	-- TODO: ввести инсет
	-- NOTE: коэффициенты подгона, рассчитаны для близзовского таксирута(taxiroute)
	local TAXIROUTE_LINEFACTOR = 1;--32/30; -- Multiplying factor for texture coordinates
	local TAXIROUTE_LINEFACTOR_2 = TAXIROUTE_LINEFACTOR / 2; -- Half o that

	local texture = in_meta.object;
	local relPoint = GGF.InnerInvoke(in_meta, "GetRelativePoint");
	local relTo = GGF.InnerInvoke(in_meta, "GetRelativeTo");
	--local eRelPoint = GGF.InnerInvoke(in_meta, "GetSecondRelativePoint");
	--local eRelTo = GGF.InnerInvoke(in_meta, "GetSecondRelativeTo");
	local sx, sy = GGF.InnerInvoke(in_meta, "GetOffsetX"), GGF.InnerInvoke(in_meta, "GetOffsetY");
	local ex, ey = GGF.InnerInvoke(in_meta, "GetSecondOffsetX"), GGF.InnerInvoke(in_meta, "GetSecondOffsetY");
	local w = GGF.InnerInvoke(in_meta, "GetWidth");
	local texclr = GGF.InnerInvoke(in_meta, "GetColor");

	-- Determine dimensions and center point of line
	local dx,dy = ex - sx, ey - sy;
	local cx,cy = (sx + ex) / 2, (sy + ey) / 2;

	-- Normalize direction if necessary
	if (dx < 0) then
		dx,dy = -dx,-dy;
	end

	-- Calculate actual length of line
	local l = sqrt((dx * dx) + (dy * dy));

	-- QUick escape if it's zero length
	if (l == 0) then
		texture:SetTexCoord(0, 0, 0, 0, 0, 0, 0, 0);
		texture:SetPoint("BottomLeft", relTo, relPoint, cx, cy);
		texture:SetPoint("TopRight", relTo, relPoint, cx, cy);
		return;
	end

	-- Sin and Cosine of rotation, and combination (for later)
	local s,c = -dy / l, dx / l;
	local sc = s * c;

	-- Calculate bounding box size and texture coordinates
	local Bwid, Bhgt, BLx, BLy, TLx, TLy, TRx, TRy, BRx, BRy;
	if (dy >= 0) then
		Bwid = ((l * c) - (w * s)) * TAXIROUTE_LINEFACTOR_2;
		Bhgt = ((w * c) - (l * s)) * TAXIROUTE_LINEFACTOR_2;
		BLx, BLy, BRy = (w / l) * sc, s * s, (l / w) * sc;
		BRx, TLx, TLy, TRx = 1 - BLy, BLy, 1 - BRy, 1 - BLx; 
		TRy = BRx;
	else
		Bwid = ((l * c) + (w * s)) * TAXIROUTE_LINEFACTOR_2;
		Bhgt = ((w * c) + (l * s)) * TAXIROUTE_LINEFACTOR_2;
		BLx, BLy, BRx = s * s, -(l / w) * sc, 1 + (w / l) * sc;
		BRy, TLx, TLy, TRy = BLx, 1 - BRx, 1 - BLx, 1 - BLy;
		TRx = TLy;
	end

	-- Set texture coordinates and anchors
	texture:ClearAllPoints();
	texture:SetTexCoord(TLx, TLy, BLx, BLy, TRx, TRy, BRx, BRy);
	texture:SetPoint("BottomLeft", relTo, relPoint, cx - Bwid, cy - Bhgt);
	texture:SetPoint("TopRight", relTo, relPoint, cx + Bwid, cy + Bhgt);
	if (texclr ~= 0) then
		GGF.VersionSplitterProc({
			{
				versionInterval = { GGD.TocVersionList.Vanilla, GGD.TocVersionList.Draenor },
				action = function()
					-- NOTE: до 7.0.3
					texture:SetTexture(texclr.R, texclr.G, texclr.B, texclr.A);
				end,
			},
			{
				versionInterval = { GGD.TocVersionList.Legion, GGD.TocVersionList.Invalid },
				action = function()
					texture:SetColorTexture(texclr.R, texclr.G, texclr.B, texclr.A);
				end,
			},
		});
	end
end