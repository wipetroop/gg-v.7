-- NOTE: вынесен, т.к. при локальном объявлении не работала рекурсия
-- Рекурсивный обход с составлением карты
local function traverseGenerator(in_map, in_curpath, in_class)
	-- NOTE: выбрасываем из рассмотрения саму мапу объектов
	if (type(in_class) ~= 'table' or (in_class.container ~= nil and in_class.path ~= nil)) then
		return;
	end

	-- NOTE: Если условие проходит, то генератор наткнулся на какой-то параметр объекта
	if (in_class.cntchecker) then
		--local cntnr = COMSPLTGT(self, in_curpath);
		in_map[in_class.token] = { 
			container = in_class,
			path = in_curpath,
		};
		return;
	end

	for token, value in pairs(in_class) do
		traverseGenerator(in_map, (#in_curpath == 0) and token or (in_curpath .. "." .. token), value);
	end

	return;
end


-- Генерация карты контейнеров объекта
local function generateContainerMap(in_meta)
	local class = in_meta.class;
	-- NOTE: сначала зачистка от генераций для других объектов
	class.objectMap = nil;
	class.objectMap = {
		--__objSuffix = in_class.meta.suffix;
	};
	traverseGenerator(class.objectMap, "", class);
end


-- Рекурсивный конструктор шаблона
local function traverseTemplateCreator(in_classBranch, in_templateBranch)
	-- NOTE: выбрасываем из рассмотрения саму мапу объектов
	if (type(in_classBranch) ~= 'table' or (in_classBranch.container ~= nil and in_classBranch.path ~= nil)) then
		return;
	end

	-- NOTE: Если условие проходит, то генератор наткнулся на какой-то параметр объекта
	if (in_classBranch.cntchecker) then
		return;
	end

	for name, value in pairs(in_classBranch) do
		in_templateBranch[name] = {};
		--print("add branch token:", name);
		traverseTemplateCreator(value, in_templateBranch[name]);
	end

	return;
end

-- Генерация шаблона экземпляра класса
local function generateInstanceTemplate(in_meta)
	local class = in_meta.class;
	traverseTemplateCreator(class.objects, class.instanceTemplate.objects);
	traverseTemplateCreator(class.wrappers, class.instanceTemplate.wrappers);
	traverseTemplateCreator(class.properties, class.instanceTemplate.properties);
	traverseTemplateCreator(class.elements, class.instanceTemplate.elements);
	--GGF.TableDump(in_class.instanceTemplate);
	--in_class.instanceTemplate = GGF.TableCopy(tempMap);
	--print("git cr");
end


-- NOTE: для блока создания низкоуровневых объектов
-- Проверка принадлежности объекта верхнему уровню наследования(чек по метке)
local function checkHighLevel(in_meta, in_mark)
	--print("chl", in_class:GetClassSuffix(), in_mark, in_class:GetClassMark());
	return in_meta.class:GetClassMark() == in_mark;
end


-- Генерация порядков применения свойств
local function generateAdjustOrders(in_meta)
	local class = in_meta.class;
	local adjustOrders = class:GetAdjustOrders();
	local objectOrder = {};
	local wrapperOrder = {};
	local propertyOrder = {};
	local elementOrder = {};

	local objFun = function(in_list, in_token, in_container)
		if (checkHighLevel(in_meta, in_container.mark) ~= false) then
			--print("token", in_token);
			in_list[#in_list + 1] = in_token;
			if (#in_list > 1 and in_token == "MasterFrame") then
				ATLASSERT(false);
			end
		end
	end
	local wrpFun = function(in_list, in_token, in_container)
	end
	local propFun = function(in_list, in_token)
		in_list[#in_list + 1] = in_token;
	end
	local elemFun = function(in_list, in_token)
		in_list[#in_list + 1] = in_token;
	end

	for token, data in pairs(class:GetClassObjectMap()) do
		local ctype = data.container.ctype;
		if (not ctype) then
			-- TODO: нормально обработать ошибку
			print("error: unhandled object ctype - ", ctype);
		else
			local orderList = GGF.SwitchCase(ctype,
				{ GGE.ContainerType.Object, objectOrder },
				{ GGE.ContainerType.Wrapper, wrapperOrder },
				{ GGE.ContainerType.Property, propertyOrder },
				{ GGE.ContainerType.Element, elementOrder }
			);
			local orderListFun = GGF.SwitchCase(ctype,
				{ GGE.ContainerType.Object, objFun },
				{ GGE.ContainerType.Wrapper, wrpFun },
				{ GGE.ContainerType.Property, propFun },
				{ GGE.ContainerType.Element, elemFun }
			);
			if (not orderList) then
				print("error: unhandled object ctype - ", ctype);
				ATLASSERT(false);
			end
			orderListFun(orderList, token, data.container);
		end
	end

	class:SetAdjustOrders({
		object = objectOrder,
		wrapper = wrapperOrder,
		property = propertyOrder,
		element = elementOrder,
	});
end


-- Обертка вызова с проверкой источника вызова
local function sourceCheckWrapper(in_func, in_meta, ...)
	-- TODO: реализовать проверку(GG-69)
	return in_func(in_meta, ...);
end


-- Обертка вызова с проверкой флага фиксации целевого элемента операции
local function fixationCheckWrapper(in_func, in_container, in_meta, ...)
	local errmsg = "trying to change constant parameter";
	local fnc = GGF.TernExpSingle(in_container.fixed, function() print(errmsg, in_container.token); ATLASSERT(false); end, in_func);
	--print("entitycontainer fcw:", ...);
	return fnc(in_meta, ...);
end


-- Обобщенный маунтер методов на каждый контейнер класса
local function containerMethodMounter(in_meta, in_container, in_token, in_table)
	local class = in_meta.class;
	for methodName, methodMeta in pairs(in_table) do
		-- NOTE: метод устанавливается, только если там пусто
      -- WARNING: временно отключено!!!
		--if (class[methodName .. in_token .. "OD"] == nil) then
			class[methodName .. in_token .. "OD"] = function(in_class, in_ameta, ...)
				return sourceCheckWrapper(methodMeta[2], in_ameta, ...)
			end;
			class[methodName .. in_token] = GGF.TernExpSingle(methodMeta[1] == 0,
				class[methodName .. in_token .. "OD"],
				function(in_class, in_ameta, ...)
					return fixationCheckWrapper(methodMeta[2], in_container, in_ameta, ...)
				end
			);
		--end
	end
end


-- Смена активного параметра вычисления(функции/значения)
local function standardModify(in_meta, in_path, in_value)
	--if (type(in_value) ~= GGE.ParameterType.Function) then
		--in_container.ptype = GGE.ParameterType.Value;
		--in_container.parameter.value = in_value;
	--else
		--in_container.ptype = GGE.ParameterType.Function;
		--in_container.parameter.func = in_value;
	--end
	--print("std mod:", in_path, in_value, in_meta.instance);
	if (in_value == nil) then
		--print("std modify ignored by nil value:", in_path);
		return;
	end

	if (GGF.SplitCallCheck(in_meta.instance, in_path)) then
		GGF.SplitCallSet(in_meta.instance, in_path, in_value);
	else
		print("no path:", in_path);
		-- TODO: выбросить ошибку
		ATLASSERT(false);
	end
end


-- Получение значения поля
local function standardGet(in_meta, in_path)
	local val = GGF.SplitCallGet(in_meta.instance, in_path);
   if string.find(in_path, "parentInstance") then
      --print("std get:", in_meta.instance, val, in_path);
   end
	--GGF.TableDump(in_instance);
	if (type(val) == GGE.ParameterType.Function) then
		return val(in_meta);
	else
		return val;
	end
	--if (in_container.ptype == GGE.ParameterType.Value) then
		--return in_container.parameter.value;
	--else
		--return in_container.parameter.func(self, in_container.interdata.argline);
	--end
	ATLASSERT(false);
end


-- Получение дефолтного значения поля
local function standardDefaultGet(in_container)
	local defvaltoken = in_container.default.token;
	local defvalsuff = in_container.default.objSuffix;
	local value = GGF.GetDefaultValue(defvalsuff, defvaltoken);
	if (type(value) == GGE.ParameterType.Value) then
		return value;
	else
		return value();
	end
	ATLASSERT(false);
end


-- Получение вычислителя поля
local function standardCounterGet(in_meta, in_path)
	local val = GGF.SplitCallGet(in_meta.instance, in_path);
	if (type(val) == GGE.ParameterType.Function) then
		return val;
	else
		return nil;
	end
	--if (in_container.ptype == GGE.ParameterType.Value) then
		--return nil;
	--else
		--return in_container.parameter.func;
	--end
	ATLASSERT(false);
end


-- Получение дефолтного вычислителя поля
local function standardDefaultCounterGet(in_container)
	local defvaltoken = in_container.default.token;
	local defvalsuff = in_container.default.objSuffix;
	local value = GGF.GetDefaultValue(defvalsuff, defvaltoken);
	if (type(value) == GGE.ParameterType.Value) then
		return nil;
	else
		return value;
	end
	ATLASSERT(false);
end


-- Получение транспортной ссылки поля
local function standardTrLinkGet(in_meta, in_path)
	-- NOTE: отличие от каунтера и просто в геттера в типе возвращаемого значения(переменная/функция)
	local val = GGF.SplitCallGet(in_meta.instance, in_path);
	return val;
	--if (in_container.ptype == GGE.ParameterType.Value) then
		--return in_container.parameter.value;
	--else
		--return in_container.parameter.func;
	--end
	--ATLASSERT(false);
end


-- Получение дефолтной транспортной ссылки поля
local function standardDefaultTrLinkGet(in_container)
	local defvaltoken = in_container.default.token;
	local defvalsuff = in_container.default.objSuffix;
	local value = GGF.GetDefaultValue(defvalsuff, defvaltoken);
	return value;
	--if (in_container.ptype == GGE.ParameterType.Value) then
		--return in_container.parameter.default;
	--else
		--return in_container.parameter.default;
	--end
	--ATLASSERT(false);
end


-- Получение списка объектов для параметра(linked to list)
--local function defaultLinkedToListGet(in_container)
	--ATLASSERT(false);
--end


-- Применение параметра к объекту
local function standardAdjust(in_meta)
	print("adj fail:", GGF.INS.GetObjectSuffix(in_meta.instance));
	-- NOTE: Вот эти точно все разные....и должны быть у каждого свои
	ATLASSERT(false);
end


-- Применение объекта к параметру
local function standardSynchronize(in_meta)
	print("sync fail:", GGF.INS.GetObjectSuffix(in_meta.instance));
	-- NOTE: Вот эти точно все разные....и должны быть у каждого свои
	ATLASSERT(false);
end


-- Дополнение активного набора аргументов функционального вычислителя
--local function defaultAddArgline(in_container, in_key, in_value)
	--in_container.interdata.argline[in_key] = in_value;
--end


-- Смена активного набора аргументов функционального вычислителя
--function This:DefaultSetArgline(in_container, in_argline)
	--in_container.interdata.argline = in_argline;
--end


-- Специальная обертка присвоения свойств
local function adjustWrapper(in_meta, in_adjfunc, in_token, in_ctype, in_mark)
	-- NOTE: враппер выведен из эксплуатации
	-- NOTE: ...и введен обратно, но не аджастится
	if (in_ctype == GGE.ContainerType.Wrapper) then
		-- NOTE: тут просто игнорим
		--ATLASSERT(false);
		return;
	end
	if (in_ctype == GGE.ContainerType.Object) then
		if (checkHighLevel(in_meta, in_mark)) then
			in_adjfunc(in_meta);
			--print("adj wrp:", in_instance, in_meta.class["Get" .. in_token](in_meta.class, in_meta), in_token);
			GGF.INS.SetObjectRef(in_meta.instance, in_meta.class["Get" .. in_token](in_meta.class, in_meta));
			--self.objectRef = self["Get"..in_token](self);
		end
		return;
	end
	--if (in_ctype == GGE.ContainerType.Element) then
		--in_adjfunc(self);
		--return;
	--end
	--print("entity container adj:", self.objectRef);
	in_adjfunc(in_meta);
end


local function syncWrapper(in_meta, in_syncfunc, in_token, in_ctype, in_mark, in_defval)
	if (in_ctype == GGE.ContainerType.Wrapper) then
		-- NOTE: тут просто игнорим
		return;
	end
	if (in_ctype == GGE.ContainerType.Object) then
		return;
	end
	in_syncfunc(in_meta);
end


-- Создание виртуального поля функций
local function createRuntime(in_meta)
	local objMap = in_meta.class:GetClassObjectMap();
	-- NOTE: Object Definer(OD) методы могут быть вызваны только из определений классов ООП
	for token, data in pairs(objMap) do
		local container, path = data.container, data.path;

		-- TODO: разобраться с линками
		-- Modify
		local stdModify = function(in_ameta, in_val)
			standardModify(in_ameta, data.path, in_val);
		end
		local modifyFunc = container[GGE.RuntimeMethod.Modifier] or stdModify;

		-- Get
		local stdGet = function(in_ameta)
			return standardGet(in_ameta, data.path);
		end
		local getFunc = container[GGE.RuntimeMethod.ValueGetter] or stdGet;

		-- DefaultGet
		local stdDefaultGet = function(in_ameta)
			return standardDefaultGet(container);
		end
		local defaultGetFunc = stdDefaultGet;	-- NOTE: оверрайда нет

		-- CounterGet
		local stdCounterGet = function(in_ameta)
			return standardCounterGet(in_ameta, data.path);
		end
		local counterGetFunc = container[GGE.RuntimeMethod.CounterGetter] or stdCounterGet;

		-- DefaultCounterGet
		local stdDefaultCounterGet = function(in_ameta)
			return standardDefaultCounterGet(container);
		end
		local defaultCounterGetFunc = stdDefaultCounterGet;	-- NOTE: оверрайда нет

		-- TrLinkGet
		local stdTrLinkGet = function(in_ameta)
			return standardTrLinkGet(in_ameta, data.path);
		end
		local trLinkGetFunc = container[GGE.RuntimeMethod.TrLinkGetter] or stdTrLinkGet;

		-- DefaultTrLinkGet
		local stdDefaultTrLinkGet = function(in_ameta)
			return standardDefaultTrLinkGet(container);
		end
		local defaultTrLinkGetFunc = stdDefaultTrLinkGet;	-- NOTE: оверрайда нет

		-- Adjust
		local stdAdjust = function(in_ameta)
			standardAdjust(in_ameta);
		end
		local adjFunc = container[GGE.RuntimeMethod.Adjuster] or stdAdjust;
		ATLASSERT(container[GGE.RuntimeMethod.Adjuster]);
		local adjustFunc = function(in_ameta)
			adjustWrapper(in_ameta, adjFunc, token, container.ctype, container.mark);
		end

		-- Set
		local stdSet = function(in_ameta, in_val)
			modifyFunc(in_ameta, in_val);
			adjustFunc(in_ameta);
		end;
		local setFunc = container[GGE.RuntimeMethod.Setter] or stdSet;


		-- Synchronize
		local stdSynchronize = function(in_ameta)
			standardSynchronize(in_ameta);
		end;
		local syncFunc = container[GGE.RuntimeMethod.Synchronizer] or stdSynchronize;
		--print("md token:", token, container[GGE.RuntimeMethod.Synchronizer], container[GGE.RuntimeMethod.Adjuster], in_meta.class:getClassSuffix());
		ATLASSERT(container[GGE.RuntimeMethod.Synchronizer]);
		local synchronizeFunc = function(in_ameta)
			syncWrapper(in_ameta, syncFunc, token, container.ctype, container.mark);
		end

		--local defArglineAdd = function(in_wrapper, in_key, in_value)
			--in_wrapper:DefaultAddArgline(container, in_key, in_value);
		--end;
		--local arglineAddFunc = container[GGE.RuntimeMethod.ArglineAdder] or defArglineAdd;

		--local defArglineSet = function(in_wrapper, in_argline)
			--in_wrapper:DefaultSetArgline(container, in_argline);
		--end;
		--local arglineSetFunc = container[GGE.RuntimeMethod.ArglineSetter] or defArglineSet;

		local methodList = {
			["Modify"] = { 1, modifyFunc },
			["Set"] = { 1, setFunc },
			["Get"] = { 0, getFunc },
			["DefaultGet"] = { 0, defaultGetFunc },
			["CounterGet"] = { 0, counterGetFunc },
			["DefaultCounterGet"] = { 0, defaultCounterGetFunc },
			["TrLinkGet"] = { 0, trLinkGetFunc },
			["DefaultTrLinkGet"] = { 0, defaultTrLinkGetFunc },
			["Adjust"] = { 1, adjustFunc },
			["Synchronize"] = { 1, synchronizeFunc },
		};

		containerMethodMounter(in_meta, container, token, methodList);
	end
end


-- NOTE: не используется
-- Инициализатор контейнеров(установка данных из шаблона или оверрайда)
function checkContainerValue(in_object, in_path)
	-- NOTE: временная конструкция для старых полей
	ATLASSERT(GGF.SplitCallGet(self, in_path .. ".parameter.value") == nil);
	ATLASSERT(GGF.SplitCallGet(self, in_path .. ".parameter.func") == nil);
end


-- Инициализатор контейнеров(установка данных из шаблона или оверрайда)
function GGF.InitContainerValue(in_meta, in_token, in_template, in_override, in_path, in_container)
	local defpath = "";
	--if (in_container.ptype == GGE.ParameterType.Value) then
		--defpath = in_path .. ".parameter.value";
	--else
		--defpath = in_path .. ".parameter.func";
	--end
	defvaltokenpath = in_path .. ".default.token";
	defvalsuffpath = in_path .. ".default.objSuffix";
	local override = GGF.SplitCallGet(in_override, in_path);
	local template = GGF.SplitCallGet(in_template, in_path);
	local default = GGF.GetDefaultValue(GGF.SplitCallGet(in_meta.class, defvalsuffpath), GGF.SplitCallGet(in_meta.class, defvaltokenpath));
	--print("init", in_token, in_path, override, template, default);
	if (default == nil) then
		print("def fault", in_token, in_path);
		ATLASSERT(false);
	end
	local value = GGF.TernExpSingle(override ~= nil, override, GGF.TernExpSingle(template ~= nil, template, default));
	--print("icv:", default, override, template)
	--if (in_token == "Parent") then
		--print("ent init", in_token, default, GGF.SplitCallGet(self, defpath), UIParent, GGF.GetDefaultValue("EC", "Parent")());
	--end
	-- NOTE: по умолчанию сюда 0 провалится
	if (value ~= nil) then
		if (in_container.fixed) then
			if (override ~= nil or template ~= nil) then
				print("Trying to override fixed parameter", in_token);
				ATLASSERT(false);
			else
				GGF.InnerInvoke(in_meta, 'Modify'..in_token..'OD', value);
			end
			-- NOTE: резерв(если сверху отлюбнёт)
			--self:DefaultDataModify(in_container, value);
		else
			GGF.InnerInvoke(in_meta, 'Modify'..in_token, value);
			-- TODO: по нормальному кинуть код ошибки
		end
	else
		-- NOTE: паренты не чекаем - там особая логика
		if (in_container.token == CTList.Parent.Token or in_container.token == CTList.ParentName.Token) then
			print("parent name", in_container.token);
			return;
		end
		-- NOTE: принудительный чек на объявленное значение
		print("Nil value:", in_container.token, defpath);
		ATLASSERT(false);
	end
end


-- Проверка контейнера
local function isParameterContainer(in_cont)
	-- NOTE: переделано после ухода от массового копирования объектов
	return in_cont and in_cont.cntchecker;--in_cont.meta and in_cont.meta.suffix == GGD.ParameterContainerSuffix;
end


-- Рекурсивный обход дерева параметров
local function recursiveParameterCheck(in_class, in_path, in_value, in_wrn)
	-- HACK: отсекаем близзовсие объекты
	if (type(in_value) == 'table' and not isParameterContainer(GGF.SplitCallGet(in_class, in_path))) then
		for name, parameter in pairs(in_value) do
			--print("rpc:", in_path, isParameterContainer(GGF.SplitCallGet(in_class, in_path)));
			recursiveParameterCheck(in_class, in_path .. "." .. name, parameter, in_wrn);
		end
	else
		--local defpathFunc = in_path .. ".parameter.func";
		--local defpathVal = in_path .. ".parameter.value";
		local defpath = in_path .. ".default.token";
		--if (GGF.SplitCallGet(self, defpathFunc) == nil and GGF.SplitCallGet(self, defpathVal) == nil and string.find(in_path, "jrtoken") == nil) then
		if (GGF.SplitCallGet(in_class, defpath) == nil and string.find(in_path, "jrtoken") == nil) then
			print("parameter overmuch:", in_path, GGF.SplitCallGet(in_class, in_path .. ".parameter"), in_wrn);
			ATLASSERT(false);
		end
	end
end


-- NOTE: по сути, ответ на вопрос "нет ли лишних полей?"
-- Проверка состоятельности дополнительных параметров(шаблон/перегрузка)
function GGF.CheckParameterConsistency(in_class, in_template, in_override)
	local override = GGF.SplitCallGet(in_override, "properties.base.wrapperName");
	local template = GGF.SplitCallGet(in_template, "properties.base.wrapperName");
	local default = GGF.SplitCallGet(in_class, "properties.base.wrapperName.parameter.value");
	local wrn = override or template or default;

	for name, parameter in pairs(in_template) do
		recursiveParameterCheck(in_class, name, parameter, wrn);
	end
	for name, parameter in pairs(in_override) do
		recursiveParameterCheck(in_class, name, parameter, wrn);
	end
end


-- NOTE: не используется
-- Проверка наличия токена свойства в массиве свойств объекта
local function checkPropertyExistence(in_propList, in_token)
	for propIdx, propToken in ipairs(in_propList) do
		if (propToken == in_token) then
			return true;
		end
	end
	return false;
end


-- Безопасная установка значения
function GGF.SafeAssignValue(in_object, in_params, in_path, in_diffPath)
	-- NOTE: дифф путь нужен, если значение в другом месте
	-- NOTE: работает для инстансов и классов
	-- TODO: придумать чек на недопустимые поля(возможно не требуется)(GG-57)
	if (GGF.SplitCallCheck(in_params, in_path)) then
		GGF.SplitCallSet(in_object, in_diffPath or in_path, GGF.SplitCallGet(in_params, in_path));
	end
end


-- Создание необходимых вспомогательных конструкция для класса
function GGF.CreateClassWorkaround(in_class)
	local meta = {
		instance = nil,
		class = in_class,
		object = nil,
	};
	generateContainerMap(meta);
	createRuntime(meta);
	generateInstanceTemplate(meta);
	generateAdjustOrders(meta);
	GGF.RewriteObjectBySuffix(meta.class:getClassSuffix(), meta.class);
end


-- Инициализатор базового класса
function GGF.InitializeClass(in_meta)
	--self:generateContainerMap();
	-- NOTE: первый аргумент уже проверен
	--self:createRuntime(self.objectMap);
	--local class = GGF.GetObjectBySuffix(GGF.GetObjectSuffix(in_instance));
	local overrideParams = GGF.INS.GetOverrideParams(in_meta.instance);
	local objectTemplate = GGF.INS.GetObjectTemplate(in_meta.instance);
	GGF.CheckParameterConsistency(in_meta.class, objectTemplate, overrideParams);
	-- NOTE: не менять эти два куска кода местами!!!(и не объединять)
	for token, data in pairs(in_meta.class.objectMap) do
		if (data.container.ctype ~= GGE.ContainerType.Object and data.container.ctype ~= GGE.ContainerType.Wrapper) then
			GGF.InitContainerValue(in_meta, token, objectTemplate, overrideParams, data.path, data.container);
		end
	end
	for token, data in pairs(in_meta.class.objectMap) do
		if (data.container.ctype == GGE.ContainerType.Object) then
			local adjustFn = data.container[GGE.RuntimeMethod.Adjuster] or ATLASSERT(false);
			-- NOTE: nil, т.к. объект возьмется внутри
			adjustWrapper(in_meta, adjustFn, token, data.container.ctype, data.container.mark, GGF.GetDefaultValue(data.container.default.objSuffix, data.container.default.token));
		end
	end
	in_meta.object = GGF.INS.GetObjectRef(in_meta.instance);
	GGF.NotNullInvoke(in_meta, "createServiceObjects");
end


-- Аджаст близзовского объекта
function GGF.BlzAdjust(in_meta)
	local adjustOrders = in_meta.class:GetAdjustOrders();
	local objectOrder = adjustOrders.object;
	local wrapperOrder = adjustOrders.wrapper;
	local propertyOrder = adjustOrders.property;
	local elementOrder = adjustOrders.element;

	if (#objectOrder ~= 1) then
		for idx,tkn in ipairs(objectOrder) do
			print("obj token:", tkn);
		end
	end
	ATLASSERT(#objectOrder == 1);
	--self.objectRef = self["Get"..objectOrder[1]](self);
	GGF.NotNullInvoke(in_meta, "adjustServiceObjects");
	--print("blzcontainer adj:", self, self.objectRef);
	--for token, data in pairs(ObjectMap) do
		--self['Adjust'..token](self);
	--end
	-- WARNING: не менять местами!!!
	-- NOTE: там аджаст свойств в объекты из листа
	--for index, token in pairs(objectOrder) do
		--self['Adjust'..token..'OD'](self);
	--end
	for index, token in pairs(propertyOrder) do
		GGF.InnerInvoke(in_meta, 'Adjust'..token..'OD');
	end
	for index, token in pairs(elementOrder) do
		GGF.InnerInvoke(in_meta, 'Adjust'..token..'OD');
	end
end


-- Откат до заводских настроек(in_objectTemplate + in_overrideParams)
function GGF.MasterReset(in_instance)
	-- WARNING: не двигать проверку!!!
	--for token, data in pairs(self.objectMap) do
		--if (data.container.ctype ~= GGE.ContainerType.Object and data.container.ctype ~= GGE.ContainerType.Wrapper) then
			--self:checkContainerValue(data.path);
		--end
	--end
	local class = GGF.GetObjectBySuffix(in_instance.objectSuffix);
	for token, data in pairs(class.objectMap) do
		if (data.container.ctype ~= GGE.ContainerType.Object and data.container.ctype ~= GGE.ContainerType.Wrapper) then
			GGF.InitContainerValue(class, token, in_instance.objectTemplate, in_instance.overrideParams, data.path, data.container);
		end
	end
	local meta = {
		instance = in_instance,
		class = class,
		object = GGF.INS.GetObjectRef(in_instance),
	};
	GGF.NotNullInvoke(meta, "Adjust");
end