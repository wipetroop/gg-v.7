-------------------------------------------------------------------------------
-- "declaration" block
-------------------------------------------------------------------------------

function GGF.DockDomName(...)
	local list = {...}
	local summary = "";
	for k,v in ipairs(list) do
		if (summary == "") then
			summary = v;
		else
			summary = summary .. "." .. v;
		end
	end

	return summary;
end

function GGF.CreateCT(in_objSuffix, in_token, in_defkey)
	ATLASSERT(in_objSuffix);
	ATLASSERT(in_token);
	ATLASSERT(in_defkey);
	return { Token = in_token, Default = in_defkey, ObjSuffix = in_objSuffix };
end

-- NOTE: на всякий случай вынесено
local pDFList = {
	DefineIdentifier = "GGD.DefineIdentifier",
	FunctionIdentifier = "GGD.FunctionIdentifier",
	ClassIdentifier = "GGD.ClassIdentifier",
	EnumerationIdentifier = "GGD.EnumerationIdentifier",
	ModuleIdentifier = "GGD.ModuleIdentifier",
	DefaultIdentifier = "GGD.DefaultIdentifier",
};

GGF.GRegister(pDFList.DefineIdentifier, "GGD");
GGF.GRegister(pDFList.FunctionIdentifier, "GGF");
GGF.GRegister(pDFList.ClassIdentifier, "GGC");
GGF.GRegister(pDFList.EnumerationIdentifier, "GGE");
GGF.GRegister(pDFList.ModuleIdentifier, "GGM");
GGF.GRegister(pDFList.DefaultIdentifier, "Default");

local DFList = {
	LayerType = GGF.DockDomName(GGD.EnumerationIdentifier, "LayerType"),
	PointType = GGF.DockDomName(GGD.EnumerationIdentifier, "PointType"),
	JustifyHType = GGF.DockDomName(GGD.EnumerationIdentifier, "JustifyHType"),
	JustifyVType = GGF.DockDomName(GGD.EnumerationIdentifier, "JustifyVType"),
	ParameterType = GGF.DockDomName(GGD.EnumerationIdentifier, "ParameterType"),
	ContainerType = GGF.DockDomName(GGD.EnumerationIdentifier, "ContainerType"),
	RuntimeMethod = GGF.DockDomName(GGD.EnumerationIdentifier, "RuntimeMethod"),
};

GGF.GRegister(DFList.LayerType, {
	Background = "BACKGROUND",
	Border = "BORDER",
	Artwork = "ARTWORK",
	Overlay = "OVERLAY",
	Highlight = "HIGHLIGHT",
});

GGF.GRegister(DFList.PointType, {
	Top = "TOP",
	Bottom = "BOTTOM",
	Left = "LEFT",
	Right = "RIGHT",
	TopLeft = "TOPLEFT",
	TopRight = "TOPRIGHT",
	BottomLeft = "BOTTOMLEFT",
	BottomRight = "BOTTOMRIGHT",
	Center = "CENTER",
});

GGF.GRegister(DFList.JustifyHType, {
	Left = "LEFT",
	Center = "CENTER",
	Right = "RIGHT",
});

GGF.GRegister(DFList.JustifyVType, {
	Top = "TOP",
	Middle = "MIDDLE",
	Bottom = "BOTTOM",
});

GGF.GRegister(DFList.ParameterType, {
	Value = 'value',
	Function = 'function',
});

GGF.GRegister(DFList.ContainerType, {
	Object = 'object',				-- уевый объект
	Property = 'property',			-- свойство уевого объекта
	Element = 'element',			-- автономный элементы класса
	Transceiver = 'transceiver',	-- передатчик на параметр элемента аггрегатора
	Wrapper = 'wrapper',			-- враппера уевого объекта(д.б. объект от объектной базы)
});

GGF.GRegister(DFList.RuntimeMethod, {
	Modifier = "Modifier",
	Setter = "Setter",
	CounterGetter = "CntGetter",
	TrLinkGetter = "TrLinkGetter",
	ValueGetter = "ValGetter",
	--RPTListGetter = "RPTListGetter",
	Adjuster = "Adjuster",
	Synchronizer = "Synchronizer",
	ArglineAdder = "ArglineAdder",
	ArglineSetter = "ArglineSetter",
});

function GGF.GetDefaultValue(in_objSuffix, in_field)
	--print(GGF.DockDomName(GGD.DefineIdentifier, in_objSuffix, GGD.DefaultIdentifier, in_field));
	--print(GGF.SplitCallGet(_G, GGF.DockDomName(GGD.DefineIdentifier, in_objSuffix, GGD.DefaultIdentifier, in_field)));
	-- NOTE: недопустим только nil
	ATLASSERT(GGF.SplitCallGet(_G, GGF.DockDomName(GGD.DefineIdentifier, in_objSuffix, GGD.DefaultIdentifier, in_field)) ~= nil);
	return GGF.SplitCallGet(_G, GGF.DockDomName(GGD.DefineIdentifier, in_objSuffix, GGD.DefaultIdentifier, in_field));
end