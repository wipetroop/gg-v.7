----------------------------------------------------------------------------
-- "functional" block
----------------------------------------------------------------------------


----------------------------------------------------------------------------
-- "define" block
----------------------------------------------------------------------------

GGM.ECP = {};
GGD.ECP = {};
GGD.ECP.ClusterName = "Entity Connection Protocol";
GGD.ECP.ClusterToken = "ECP";
GGD.ECP.ClusterVersion = GGD.BuildMeta.Version;
GGD.ECP.ClusterBuild = "AT-"..GGD.BuildMeta.Build;
GGD.ECP.ClusterMBI = "8102-6082";