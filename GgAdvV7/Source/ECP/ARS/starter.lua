-- Инициализация на стадии загрузки аддона
function GGM.ECP:OnAddonLoad()
	GGM.COM:RegisterInterComm(GGD.ECP.ClusterToken, GGM.ECP.EvokeController);

	GGM.IOC.OutputController:AddonWelcomeMessage({
		cluster = GGD.ECP.ClusterToken,
		version = GGD.ECP.ClusterVersion,
		build = GGD.ECP.ClusterBuild,
		mbi = GGD.ECP.ClusterMBI,
	});
end