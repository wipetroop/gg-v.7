local This = GGF.ModuleCreateWrapper("GUI-1", true, {
	dashboardWidth = function() return GGM.GUI.ParameterStorage:getDMDashboardWidth() end,
	dashboardHeight = function() return GGM.GUI.ParameterStorage:getDMDashboardHeight() end,

	dashlightSide = function() return GGM.GUI.ParameterStorage:getDMDashlightSide() end,
	synchroTimer = function() return GGM.GUI.MainDashboard.JR_MD_F_C_ST_OBJ; end,

	-- TODO: разобраться, что к чему
	dashlightGap = function()
		-- NOTE: 12 - количество скиллов на панели
		local gap = 0;
		GGF.VersionSplitterProc({
			{
				versionInterval = {GGD.TocVersionList.Vanilla, GGD.TocVersionList.Legion},
				action = function()
					gap = GGM.GUI.ParameterStorage:getDMDashboardWidth()/14.0 - GGM.GUI.ParameterStorage:getDMDashlightSide()*12.0/14.0;
				end,
			},
			{
				versionInterval = {GGD.TocVersionList.BFA, GGD.TocVersionList.Invalid},
				action = function()
					gap = 0;
				end,
			},
		});
		return gap;
	end,

	combatInstrumentationWidth = function() return GGM.GUI.ParameterStorage:GetSCDashboardWidth() end,
	combatInstrumentationHeight = function() return GGM.GUI.ParameterStorage:GetSCDashboardHeight() end,
	drivecontrolInstrumentationWidth = function() return GGM.GUI.ParameterStorage:GetDMDashboardWidth() end,
	drivecontrolInstrumentationHeight = function() return GGM.GUI.ParameterStorage:GetDMDashboardHeight() end,
	maintenanceInstrumentationWidth = function() return GGM.GUI.ParameterStorage:GetDMDashboardWidth() end,
	maintenanceInstrumentationHeight = function() return GGM.GUI.ParameterStorage:GetDMDashboardHeight() end,
   --drivecontrolInstrumentationWidth = 430,
   --drivecontrolInstrumentationHeight = 120,
   --maintenanceInstrumentationWidth = 430,
   --maintenanceInstrumentationHeight = 120,
	systemInstrumentationWidth = function() return GGM.GUI.ParameterStorage:GetSCDashboardWidth() end,
	systemInstrumentationHeight = function() return GGM.GUI.ParameterStorage:GetSCDashboardHeight() end,

	skillBarGap = 7,
	centralPanelGap = 0,
	blinkInterval = 1,
	checkEngineSound = GGD.GUI.SoundPath.."mp3\\check_engine.mp3",
});

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Functors
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------