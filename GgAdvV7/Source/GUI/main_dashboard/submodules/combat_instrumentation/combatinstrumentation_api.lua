local This = GGF.ModuleGetWrapper("GUI-1-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Смена видимости основной панели
function This:SetDashboardPanelHidden(in_hdn)
	self:setDashboardPanelHidden(in_hdn);
end

-- Смена геометрического места панели по оси X
function This:SetDashboardPanelOffsetX(in_offsetX)
	self:setDashboardPanelOffsetX(in_offsetX);
end

-- Смена геометрического места панели по оси Y
function This:SetDashboardPanelOffsetY(in_offsetY)
	self:setDashboardPanelOffsetY(in_offsetY);
end

-- Установка текущего значения затрат ram в измеритель
function This:SetRamUsageMeterValue(in_val)
   self:setRamUsageMeterValue(in_val);
end

-- Установка среднего значения затрат ram в доп. поле измерителя
function This:SetRamUsageMeterAverageValue(in_val)
   self:setRamUsageMeterAverageValue(in_val);
end

-- Управление заголовком названия пб
function This:SetLoggerRuntimeStatus(in_name, in_color)
   self:setLoggerRuntimeStatus(in_name, in_color);
end

-- Установка текста в поле
function This:SetLoginSessionUptime(in_hrs, in_min, in_sec)
   self:setLoginSessionUptime(in_hrs, in_min, in_sec);
end

-- Управление заголовком токена пб
function This:SetLogTokenSignature(in_token)
   self:setLogTokenSignature(in_token);
end

-- Установка текста в поле
function This:SetBattleSessionUptime(in_hrs, in_min, in_sec)
   self:setBattleSessionUptime(in_hrs, in_min, in_sec);
end

-- Установка текста в поле
function This:SetFrm(in_fps)
   self:setFrm(in_fps);
end

-- Установка текста в поле
function This:SetLatency(in_home, in_world)
   self:setLatency(in_home, in_world);
end


-- Управление статусом индикатора transmission
function This:SetTRMState(in_state)
   self:setTRMState(in_state);
end

-- Управление статусом индикатора процесса загрузки
function This:SetLPRState(in_state)
   self:setLPRState(in_state);
end

-- Управление статусом индикатора оперативной памяти
function This:SetRAMState(in_state)
   self:setRAMState(in_state);
end

-- Управление статусом индикатора задержки
function This:SetLATState(in_state)
   self:setLATState(in_state);
end

-- Управление статусом индикатора фпс
function This:SetFPSState(in_state)
	self:setFPSState(in_state);
end

-- Управление статусом индикатора кластера ASB
function This:SetASBState(in_state)
   self:setASBState(in_state);
end

-- Управление статусом индикатора кластера ACR
function This:SetACRState(in_state)
   self:setACRState(in_state);
end

-- Управление статусом индикатора кластера COM
function This:SetCOMState(in_state)
   self:setCOMState(in_state);
end

-- Управление статусом индикатора кластера FCS
function This:SetFCSState(in_state)
   self:setFCSState(in_state);
end

-- Управление статусом индикатора кластера IOC
function This:SetIOCState(in_state)
   self:setIOCState(in_state);
end

-- Управление статусом индикатора кластера GUI
function This:SetGUIState(in_state)
   self:setGUIState(in_state);
end

-- Управление статусом индикатора кластера SND
function This:SetSNDState(in_state)
   self:setSNDState(in_state);
end

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие ширины основной панели
function This:GetDashboardPanelWidth()
	return self:getDashboardPanelWidth();
end


-- Взятие высоты основной панели
function This:GetDashboardPanelHeight()
	return self:getDashboardPanelHeight();
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------