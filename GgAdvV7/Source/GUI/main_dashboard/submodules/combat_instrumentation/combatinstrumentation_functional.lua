local This = GGF.ModuleGetWrapper("GUI-1-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-- api
-- Смена видимости основной панели
function This:setDashboardPanelHidden(in_hdn)
	GGF.OuterInvoke(self.JR_CDL_LF_OBJ, "SetHidden", in_hdn);
	GGF.OuterInvoke(self.JR_CDU_LF_OBJ, "SetHidden", in_hdn);
end

-- api
-- Смена геометрического места панели по оси X
function This:setDashboardPanelOffsetX(in_offsetX)
	GGF.OuterInvoke(self.JR_CDU_LF_OBJ, "SetOffsetX", in_offsetX);
end

-- api
-- Смена геометрического места панели по оси Y
function This:setDashboardPanelOffsetY(in_offsetY)
	GGF.OuterInvoke(self.JR_CDU_LF_OBJ, "SetOffsetY", in_offsetY);
end

-- api
-- Установка текущего значения затрат ram в измеритель
function This:setRamUsageMeterValue(in_val)
   GGF.OuterInvoke(self.JR_CDU_LF_R_BRDM_OBJ, "SetValue", in_val);
end

-- api
-- Установка среднего значения затрат ram в доп. поле измерителя
function This:setRamUsageMeterAverageValue(in_val)
   GGF.OuterInvoke(self.JR_CDU_LF_R_BRDM_OBJ, "SetAverage", in_val);
end

-- api
-- Управление заголовком названия пб
function This:setLoggerRuntimeStatus(in_name, in_color)
   GGF.OuterInvoke(self.JR_CDU_LF_LRS_NVFS_OBJ, "SetValue", in_name);
   GGF.OuterInvoke(self.JR_CDU_LF_LRS_NVFS_OBJ, "SetValueColor", in_color);
end

-- api
-- Установка текста в поле
function This:setLoginSessionUptime(in_hrs, in_min, in_sec)
   GGF.OuterInvoke(self.JR_CDU_LF_LSU_NVFS_OBJ, "SetValue", string.format("%02d:%02d:%02d", in_hrs, in_min, in_sec));
end

-- api
-- Управление заголовком токена пб
function This:setLogTokenSignature(in_token)
   GGF.OuterInvoke(self.JR_CDU_LF_LTS_NVFS_OBJ, "SetValue", in_token);
end

-- api
-- Установка текста в поле
function This:setBattleSessionUptime(in_hrs, in_min, in_sec)
   GGF.OuterInvoke(self.JR_CDU_LF_BSU_NVFS_OBJ, "SetValue", string.format("%02d:%02d:%02d", in_hrs, in_min, in_sec));
end

-- api
-- Установка текста в поле
function This:setFrm(in_fps)
   GGF.OuterInvoke(self.JR_CDU_LF_F_NVFS_OBJ, "SetValue", math.floor(in_fps) .. " fps");
end

-- api
-- Установка текста в поле
function This:setLatency(in_home, in_world)
   if (in_world) then
      GGF.OuterInvoke(self.JR_CDU_LF_L_NVFS_OBJ, "SetValue", in_home .. "/" .. in_world .. " ms");
   else
      GGF.OuterInvoke(self.JR_CDU_LF_L_NVFS_OBJ, "SetValue", in_home .. " ms");
   end
end

-- api
-- Управление статусом индикатора transmission
function This:setTRMState(in_state)
   GGF.OuterInvoke(self.JR_CDL_LF_CT_TIS_OBJ, "SetState", in_state);
end

-- api
-- Управление статусом индикатора процесса загрузки
function This:setLPRState(in_state)
   GGF.OuterInvoke(self.JR_CDL_LF_CLP_TIS_OBJ, "SetState", in_state);
end

-- api
-- Управление статусом индикатора оперативной памяти
function This:setRAMState(in_state)
   GGF.OuterInvoke(self.JR_CDL_LF_CR_TIS_OBJ, "SetState", in_state);
end

-- api
-- Управление статусом индикатора задержки
function This:setLATState(in_state)
   GGF.OuterInvoke(self.JR_CDL_LF_CL_TIS_OBJ, "SetState", in_state);
end

-- api
-- Управление статусом индикатора фпс
function This:setFPSState(in_state)
   GGF.OuterInvoke(self.JR_CDL_LF_CFP_TIS_OBJ, "SetState", in_state);
end

-- api
-- Управление статусом индикатора модуля ASB
function This:setASBState(in_state)
   GGF.OuterInvoke(self.JR_CDL_LF_CAS_TIS_OBJ, "SetState", in_state);
end

-- api
-- Управление статусом индикатора модуля ACR
function This:setACRState(in_state)
   GGF.OuterInvoke(self.JR_CDL_LF_CAC_TIS_OBJ, "SetState", in_state);
end

-- api
-- Управление статусом индикатора модуля COM
function This:setCOMState(in_state)
   GGF.OuterInvoke(self.JR_CDL_LF_CC_TIS_OBJ, "SetState", in_state);
end

-- api
-- Управление статусом индикатора модуля FCS
function This:setFCSState(in_state)
   GGF.OuterInvoke(self.JR_CDL_LF_CF_TIS_OBJ, "SetState", in_state);
end

-- api
-- Управление статусом индикатора модуля IOC
function This:setIOCState(in_state)
   GGF.OuterInvoke(self.JR_CDL_LF_CI_TIS_OBJ, "SetState", in_state);
end

-- api
-- Управление статусом индикатора модуля GUI
function This:setGUIState(in_state)
   GGF.OuterInvoke(self.JR_CDL_LF_CG_TIS_OBJ, "SetState", in_state);
end

-- api
-- Управление статусом индикатора модуля SND
function This:setSNDState(in_state)
   GGF.OuterInvoke(self.JR_CDL_LF_CS_TIS_OBJ, "SetState", in_state);
end

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-- Взятие ширины основной панели
function This:getDashboardPanelWidth()
	return GGF.OuterInvoke(self.JR_CDU_LF_OBJ, "GetWidth");
end


-- Взятие высоты основной панели
function This:getDashboardPanelHeight()
	return GGF.OuterInvoke(self.JR_CDL_LF_OBJ, "GetHeight") + GGF.OuterInvoke(self.JR_CDU_LF_OBJ, "GetHeight");
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------

-- Установка порядка расположения элементов панели
function This:setLayoutOrder()
   self.vs_labelMap = {
      { self.JR_CDU_LF_LRS_NVFS_OBJ, self.JR_CDU_LF_LSU_NVFS_OBJ, self.JR_CDU_LF_LTS_NVFS_OBJ },
      { self.JR_CDU_LF_BSU_NVFS_OBJ, self.JR_CDU_LF_F_NVFS_OBJ,   self.JR_CDU_LF_L_NVFS_OBJ }
   };
end

-- Ресет расположения элементов панели
function This:resetLayout()
   local MSC_CONFIG = self:getConfig();
   for idx,dataarray in ipairs(self.vs_labelMap) do
      for jdx,data in ipairs(dataarray) do
         GGF.OuterInvoke(data, "SetAnchor", {
            point = GGE.PointType.BottomLeft,
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
            relativePoint = GGE.PointType.BottomLeft,
            offsetX = MSC_CONFIG.dashlightGap() + (jdx - 1)*(MSC_CONFIG.dashlightGap() + MSC_CONFIG.statusWidth),
            offsetY = MSC_CONFIG.dashlightGap() - (idx - 1)*(MSC_CONFIG.dashlightGap() + MSC_CONFIG.statusHeight),
         });
      end
   end
end

-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------