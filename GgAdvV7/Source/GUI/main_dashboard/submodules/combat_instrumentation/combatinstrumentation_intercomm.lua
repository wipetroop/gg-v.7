local This = GGF.ModuleGetWrapper("GUI-1-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------

	-- combatDashboardUpper(LF)
	self.JR_CDU_LF_OBJ = GGC.LayoutFrame:Create(self.JR_CDU_LF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.MainDashboard:GetCombatInstrumentationFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.MainDashboard:GetCombatInstrumentationFrame(),
			},
		},
	});

   -- combatDashboardUpper(LF)
      -- rammeter(BRDM)
   self.JR_CDU_LF_R_BRDM_OBJ = GGC.BarDataMeter:Create(self.JR_CDU_LF_R_BRDM_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
         hardware = {
            miscellaneous = {
               dataMeterBarType = GGE.DataMeterBarType.LeftDirected,
               dataMeterMetaSideType = GGE.DataMeterMetaSideType.Down,
            },
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
      },
   });

   -- combatDashboardUpper(LF)
      -- rammeter_tmp(BRDM)
   self.JR_CDU_LF_RT_BRDM_OBJ = GGC.BarDataMeter:Create(self.JR_CDU_LF_RT_BRDM_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
         hardware = {
            miscellaneous = {
               dataMeterBarType = GGE.DataMeterBarType.UpDirected,
               dataMeterMetaSideType = GGE.DataMeterMetaSideType.Left,
            },
         },
         anchor = {
            point = GGE.PointType.BottomRight,
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDU_LF_R_BRDM_OBJ),
            relativePoint = GGE.PointType.BottomLeft,
         },
      },
   });

   -- combatDashboardUpper(LF)
      -- loggerRuntimeStatus(NVFS)
   self.JR_CDU_LF_LRS_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_CDU_LF_LRS_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
      },
   });

   -- combatDashboardUpper(LF)
      -- loginSessionUptime(NVFS)
   self.JR_CDU_LF_LSU_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_CDU_LF_LSU_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
      },
   });

   -- combatDashboardUpper(LF)
      -- logTokenSignature(NVFS)
   self.JR_CDU_LF_LTS_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_CDU_LF_LTS_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
      },
   });

   -- combatDashboardUpper(LF)
      -- battleSessionUptime(NVFS)
   self.JR_CDU_LF_BSU_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_CDU_LF_BSU_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
      },
   });

   -- combatDashboardUpper(LF)
      -- frm(NVFS)
   self.JR_CDU_LF_F_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_CDU_LF_F_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
      },
   });

   -- combatDashboardUpper(LF)
      -- latency(NVFS)
   self.JR_CDU_LF_L_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_CDU_LF_L_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
      },
   });

	-- combatDashboardLower(LF)
	self.JR_CDL_LF_OBJ = GGC.LayoutFrame:Create(self.JR_CDL_LF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.MainDashboard:GetCombatInstrumentationFrame(),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
			},
		},
	});
end


-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- combatDashboardUpper(LF)
	GGF.OuterInvoke(self.JR_CDU_LF_OBJ, "Adjust");

   -- combatDashboardUpper(LF)
      -- rammeter(BRDM)
   GGF.OuterInvoke(self.JR_CDU_LF_R_BRDM_OBJ, "Adjust");

   -- combatDashboardUpper(LF)
      -- rammeter_tmp(BRDM)
   --GGF.OuterInvoke(self.JR_CDU_LF_RT_BRDM_OBJ, "Adjust");

   -- combatDashboardUpper(LF)
      -- loggerRuntimeStatus(NVFS)
   GGF.OuterInvoke(self.JR_CDU_LF_LRS_NVFS_OBJ, "Adjust");

   -- combatDashboardUpper(LF)
      -- loginSessionUptime(NVFS)
   GGF.OuterInvoke(self.JR_CDU_LF_LSU_NVFS_OBJ, "Adjust");

   -- combatDashboardUpper(LF)
      -- logTokenSignature(NVFS)
   GGF.OuterInvoke(self.JR_CDU_LF_LTS_NVFS_OBJ, "Adjust");

   -- combatDashboardUpper(LF)
      -- battleSessionUptime(NVFS)
   GGF.OuterInvoke(self.JR_CDU_LF_BSU_NVFS_OBJ, "Adjust");

   -- combatDashboardUpper(LF)
      -- frm(NVFS)
   GGF.OuterInvoke(self.JR_CDU_LF_F_NVFS_OBJ, "Adjust");

   -- combatDashboardUpper(LF)
      -- latency(NVFS)
   GGF.OuterInvoke(self.JR_CDU_LF_L_NVFS_OBJ, "Adjust");

	-- combatDashboardLower(LF)
	GGF.OuterInvoke(self.JR_CDL_LF_OBJ, "Adjust");
end

-- п.2.8 (After-load Settings)
function This:MPRC_ALS_INITIALIZE()
   This:setLayoutOrder();
   This:resetLayout();


   --This:SetENGState("nonCritical");
   --This:SetTRMState("triggered");
   --This:SetLPRState("triggered");
   --This:SetRAMState("triggered");
   --This:SetLATState("triggered");
   --This:SetFPSState("triggered");
   --This:SetASBState("nonCritical");
   --This:SetACRState("nonCritical");
   --This:SetCOMState("nonCritical");
   --This:SetFCSState("nonCritical");
   --This:SetIOCState("nonCritical");
   --This:SetGUIState("nonCritical");
   --This:SetSNDState("nonCritical");
end

-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------

-- п.3.0 (Addon System Timer)
function This:MPRC_AST_RUNTIME(in_elapsed)
   --print("md ast rtm:", in_elapsed);
   -- TODO: узнать, что за зверь ниже
   --GGF.OuterInvoke(self.JR_CDL_LF_C_ST_OBJ, "Step", in_elapsed);
   --if (true) then return; end
   GGF.Timer(self.vs_checkStepTimer, in_elapsed, function()
      --if (GGF.OuterInvoke(self.JR_CDL_LF_CE_TIS_OBJ, "IsBlinking")) then
         --GGF.OuterInvoke(self.JR_CDL_LF_CE_BA_OBJ, "Launch");
      --end
      --if (not GGF.OuterInvoke(self.JR_CDL_LF_CE_TIS_OBJ, "IsBlinking")) then
         --GGF.OuterInvoke(self.JR_CDL_LF_CE_BA_OBJ, "Stop");
      --end
   end)
end

-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------