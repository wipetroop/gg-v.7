local This = GGF.ModuleGetWrapper("GUI-1-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_combatInstrumentationGeometry = {
   layoutFrame = GGF.GTemplateTree(GGC.LayoutFrame, {
      combatDashboardUpper = {            -- [CDU_LF]
         inherit = {
            barDataMeter = GGF.GTemplateTree(GGC.BarDataMeter, {
               rammeter = {},             -- [R_BRDM]
               rammeter_tmp = {},         -- [RT_BRDM]
            }),
            namedValueFontString = GGF.GTemplateTree(GGC.NamedValueFontString, {
               loggerRuntimeStatus = {},  -- [LRS_NVFS]
               loginSessionUptime = {},   -- [LSU_NVFS]
               logTokenSignature = {},    -- [LTS_NVFS]
               battleSessionUptime = {},  -- [BSU_NVFS]
               frm = {},                  -- [F_NVFS]
               latency = {},              -- [L_NVFS]
            }),
         },
      },
      combatDashboardLower = {            -- [CDL_LF]
         inherit = {
         },
      },
   }),
};

local CIG_LF = This.oc_combatInstrumentationGeometry.layoutFrame;
local CIG_LF_CDU_BRDM = CIG_LF.combatDashboardUpper.inherit.barDataMeter;
local CIG_LF_CDU_NVFS = CIG_LF.combatDashboardUpper.inherit.namedValueFontString;

-- combatDashboardUpper(LF)
CIG_LF.combatDashboardUpper.object = {};
CIG_LF.combatDashboardUpper.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "CDU",
		},
		miscellaneous = {
			hidden = false,
		},
		size = {
			width = MSC_CONFIG.dashWidth,
			height = MSC_CONFIG.dashHeight,
		},
		anchor = {
			point = GGE.PointType.TopLeft,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.TopLeft,
			offsetX = 0,
			offsetY = 0,
		},
	},
};

-- combatDashboardUpper(LF)
   -- rammeter(BRDM)
CIG_LF_CDU_BRDM.rammeter.object = {};
CIG_LF_CDU_BRDM.rammeter.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "R",
      },
      miscellaneous = {
         hidden = false,
      },
      hardware = {
         miscellaneous = {
            shellCount = 80,
         },
         limits = {
            min = 0,
            max = 1000,
            mspdfwd = 0.05,
            mspdbwd = 0.05,
         },
      },
      size = {
         width = MSC_CONFIG.dashWidth,
         height = MSC_CONFIG.dashHeight,
      },
      anchor = {
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Top,
         offsetX = 0,
         offsetY = 0,
      },
      backdrop = GGD.Backdrop.Empty,
   },
};

-- combatDashboardUpper(LF)
   -- rammeter_tmp(BRDM)
CIG_LF_CDU_BRDM.rammeter_tmp.object = {};
CIG_LF_CDU_BRDM.rammeter_tmp.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "RT",
      },
      miscellaneous = {
         hidden = false,
      },
      hardware = {
         limits = {
            min = 0,
            max = 400,
         },
      },
      size = {
         width = MSC_CONFIG.dashHeight,
         height = MSC_CONFIG.dashWidth,
      },
      anchor = {
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Top,
         offsetX = 0,
         offsetY = 0,
      },
      backdrop = GGD.Backdrop.Segregate,
   },
};

-- combatDashboardUpper(LF)
   -- loggerRuntimeStatus(NVFS)
CIG_LF_CDU_NVFS.loggerRuntimeStatus.object = {};
CIG_LF_CDU_NVFS.loggerRuntimeStatus.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "LRS",
      },
      miscellaneous = {
         hidden = false,
         name = "LRS:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot11OffsetX,
         offsetY = MSC_CONFIG.slot11OffsetY,
      },
      backdrop = GGD.Backdrop.Empty,
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};

-- combatDashboardUpper(LF)
   -- loginSessionUptime(NVFS)
CIG_LF_CDU_NVFS.loginSessionUptime.object = {};
CIG_LF_CDU_NVFS.loginSessionUptime.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "LSU",
      },
      miscellaneous = {
         hidden = false,
         name = "LSU:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot13OffsetX,
         offsetY = MSC_CONFIG.slot13OffsetY,
      },
      backdrop = GGD.Backdrop.Empty,
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};

-- combatDashboardUpper(LF)
   -- logTokenSignature(NVFS)
CIG_LF_CDU_NVFS.logTokenSignature.object = {};
CIG_LF_CDU_NVFS.logTokenSignature.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "LTS",
      },
      miscellaneous = {
         hidden = false,
         name = "LTS:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot22OffsetX,
         offsetY = MSC_CONFIG.slot22OffsetY,
      },
      backdrop = GGD.Backdrop.Empty,
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};

-- combatDashboardUpper(LF)
   -- battleSessionUptime(NVFS)
CIG_LF_CDU_NVFS.battleSessionUptime.object = {};
CIG_LF_CDU_NVFS.battleSessionUptime.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "BSU",
      },
      miscellaneous = {
         hidden = false,
         name = "BSU:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot23OffsetX,
         offsetY = MSC_CONFIG.slot23OffsetY,
      },
      backdrop = GGD.Backdrop.Empty,
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};

-- combatDashboardUpper(LF)
   -- frm(NVFS)
CIG_LF_CDU_NVFS.frm.object = {};
CIG_LF_CDU_NVFS.frm.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "F",
      },
      miscellaneous = {
         hidden = false,
         name = "FRM:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot31OffsetX,
         offsetY = MSC_CONFIG.slot31OffsetY,
      },
      backdrop = GGD.Backdrop.Empty,
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};

-- combatDashboardUpper(LF)
   -- latency(NVFS)
CIG_LF_CDU_NVFS.latency.object = {};
CIG_LF_CDU_NVFS.latency.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "L",
      },
      miscellaneous = {
         hidden = false,
         name = "LAT:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot32OffsetX,
         offsetY = MSC_CONFIG.slot32OffsetY,
      },
      backdrop = GGD.Backdrop.Empty,
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};

-- combatDashboardLower(LF)
CIG_LF.combatDashboardLower.object = {};
CIG_LF.combatDashboardLower.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "CDL",
		},
		miscellaneous = {
			hidden = false,
		},
		size = {
			width = MSC_CONFIG.dashWidth,
			height = MSC_CONFIG.dashHeight,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = 0,
		},
	},
};