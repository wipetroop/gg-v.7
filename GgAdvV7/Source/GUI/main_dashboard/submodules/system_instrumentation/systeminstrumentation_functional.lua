local This = GGF.ModuleGetWrapper("GUI-1-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------

-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-- api
-- Смена видимости основной панели
function This:setDashboardPanelHidden(in_hdn)
	GGF.OuterInvoke(self.JR_SDU_LF_OBJ, "SetHidden", in_hdn);
	GGF.OuterInvoke(self.JR_SDL_LF_OBJ, "SetHidden", in_hdn);
end

-- api
-- Смена геометрического места панели по оси X
function This:setDashboardPanelOffsetX(in_offsetX)
	GGF.OuterInvoke(self.JR_SDU_LF_OBJ, "SetOffsetX", in_offsetX);
end

-- api
-- Смена геометрического места панели по оси Y
function This:setDashboardPanelOffsetY(in_offsetY)
	GGF.OuterInvoke(self.JR_SDU_LF_OBJ, "SetOffsetY", in_offsetY);
end

-- api
-- Установка текущего значения скорости в измеритель
function This:setMoveSpeedMeterValue(in_val)
   GGF.OuterInvoke(self.JR_SDU_LF_S_BRDM_OBJ, "SetValue", in_val);
   --GGF.OuterInvoke(self.JR_SDU_LF_ST_BRDM_OBJ, "SetValue", in_val);
end

-- api
-- Установка среднего значения скорости в доп. поле измерителя
function This:setMoveSpeedMeterAverageValue(in_val)
   GGF.OuterInvoke(self.JR_SDU_LF_S_BRDM_OBJ, "SetAverage", in_val);
end

-- api
-- Управление заголовком названия пб
function This:setCurrentZoneSignature(in_name)
	GGF.OuterInvoke(self.JR_SDU_LF_CZS_NVFS_OBJ, "SetValue", in_name);
end

-- api
-- Управление индикатором количества хонор килов
function This:setHonorKillsCount(in_count)
   GGF.OuterInvoke(self.JR_SDU_LF_HKM_DD_OBJ, "SetText", in_count);
end

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-- api
-- Взятие ширины основной панели
function This:getDashboardPanelWidth()
	return GGF.OuterInvoke(self.JR_SDU_LF_OBJ, "GetWidth");
end

-- api
-- Взятие высоты основной панели
function This:getDashboardPanelHeight()
	return GGF.OuterInvoke(self.JR_SDL_LF_OBJ, "GetHeight") + GGF.OuterInvoke(self.JR_SDU_LF_OBJ, "GetHeight");
end

-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------

-- Установка порядка расположения элементов панели
function This:setLayoutOrder()
   self.vs_labelMapLeft = {
      { self.JR_SDU_LF_CZS_NVFS_OBJ }
   };
   self.vs_lineMap = {
      { self.JR_SDU_LF_DDL_LNF_OBJ, self.JR_SDU_LF_DDM_LNF_OBJ, self.JR_SDU_LF_DDU_LNF_OBJ }
   };
   self.vs_labelMapRight = {
      { self.JR_SDU_LF_HKM_DD_OBJ }
   };
end

-- Ресет расположения элементов панели
function This:resetLayout()
   local MSC_CONFIG = self:getConfig();
   for idx,dataarray in ipairs(self.vs_labelMapLeft) do
      for jdx,data in ipairs(dataarray) do
         if (data ~= self.cl_skipLine) then
            GGF.OuterInvoke(data, "SetAnchor", {
               point = GGE.PointType.Left,
               relativeTo = GGF.INS.GetObjectRef(self.JR_SDU_LF_OBJ),
               relativePoint = GGE.PointType.BottomLeft,
               offsetX = -100,
               offsetY = 10,
            });
         end
      end
   end
   for idx,dataarray in ipairs(self.vs_labelMapRight) do
      for jdx,data in ipairs(dataarray) do
         if (data ~= self.cl_skipLine) then
            local offset = 200;
            GGF.OuterInvoke(data, "SetAnchor", {
               point = GGE.PointType.Left,
               relativeTo = GGF.INS.GetObjectRef(self.JR_SDU_LF_OBJ),
               relativePoint = GGE.PointType.BottomLeft,
               offsetX = offset,
               offsetY = 10,
            });
            GGF.OuterInvoke(data, "SetWidth", GGF.OuterInvoke(self.JR_SDU_LF_OBJ, "GetWidth") - offset);
         end
      end
   end
   local gap = 8;
   local lineMapCoords = { {
      offsetX = gap,
      offsetY = 0,
   }, {
      offsetX = 100,
      offsetY = 0,
   }, {
      offsetX = 150,
      offsetY = 20,
   }, {
      offsetX = MSC_CONFIG.dashWidth - gap,
      offsetY = 20,
   }, };
   --for idx,dataarray in ipairs(self.vs_lineMap) do
      --for jdx,data in ipairs(dataarray) do
         --GGF.OuterInvoke(data, "SetAnchor", {
            --point = GGE.PointType.Left,
            --relativeTo = GGF.INS.GetObjectRef(self.JR_SDU_LF_OBJ),
            --relativePoint = GGE.PointType.BottomLeft,
            --offsetX = lineMapCoords[jdx].offsetX,
            --offsetY = lineMapCoords[jdx].offsetY,
            --secondOffsetX = lineMapCoords[jdx + 1].offsetX,
            --secondOffsetY = lineMapCoords[jdx + 1].offsetY,
         --});
      --end
   --end
end

-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------