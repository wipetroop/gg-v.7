local This = GGF.ModuleGetWrapper("GUI-1-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_systemInstrumentationGeometry = {
   layoutFrame = GGF.GTemplateTree(GGC.LayoutFrame, {
      systemDashboardUpper = {               -- [SDU_LF]
         inherit = {
            barDataMeter = GGF.GTemplateTree(GGC.BarDataMeter, {
               speedometer = {},             -- [S_BRDM]
               speedometer_tmp = {},         -- [ST_BRDM]
            }),
            namedValueFontString = GGF.GTemplateTree(GGC.NamedValueFontString, {
               currentZoneSignature = {},    -- [CZS_NVFS]
            }),
            lineFrame = GGF.GTemplateTree(GGC.LineFrame, {
               dashDelimiterUpper = {},      -- [DDU_LNF]
               dashDelimiterMiddle = {},     -- [DDM_LNF]
               dashDelimiterLower = {},      -- [DDL_LNF]
            }),
            digitalDisplay = GGF.GTemplateTree(GGC.DigitalDisplay, {
               honorKillsMeter = {},         -- [HKM_DD]
            }),
         },
      },
      systemDashboardLower = {               -- [SDL_LF]
         inherit = {
         },
      },
   }),
};

local SIG_LF = This.oc_systemInstrumentationGeometry.layoutFrame;
local SIG_LF_SDU_BRDM = SIG_LF.systemDashboardUpper.inherit.barDataMeter;
local SIG_LF_SDU_NVFS = SIG_LF.systemDashboardUpper.inherit.namedValueFontString;
local SIG_LF_SDU_LF = SIG_LF.systemDashboardUpper.inherit.lineFrame;
local SIG_LF_SDU_DD = SIG_LF.systemDashboardUpper.inherit.digitalDisplay;

-- systemDashboardUpper(LF)
SIG_LF.systemDashboardUpper.object = {};
SIG_LF.systemDashboardUpper.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "SDU",
		},
		miscellaneous = {
			hidden = false,
		},
		size = {
			width = MSC_CONFIG.dashWidth,
			height = MSC_CONFIG.dashHeight,
		},
		anchor = {
			point = GGE.PointType.TopLeft,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.TopLeft,
			offsetX = 0,
			offsetY = 0,
		},
	},
};

-- systemDashboardUpper(LF)
   -- speedometer(BRDM)
SIG_LF_SDU_BRDM.speedometer.object = {};
SIG_LF_SDU_BRDM.speedometer.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "S",
      },
      miscellaneous = {
         hidden = false,
      },
      hardware = {
         miscellaneous = {
            shellCount = 100,
         },
         limits = {
            min = 0,
            max = 400,
            mspdfwd = 0.5,
            mspdbwd = 1.0,
         },
      },
      size = {
         width = MSC_CONFIG.dashWidth,
         height = MSC_CONFIG.dashHeight,
      },
      anchor = {
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Top,
         offsetX = 0,
         offsetY = 0,
      },
      backdrop = GGD.Backdrop.Empty,
   },
};

-- systemDashboardUpper(LF)
   -- speedometer_tmp(BRDM)
SIG_LF_SDU_BRDM.speedometer_tmp.object = {};
SIG_LF_SDU_BRDM.speedometer_tmp.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "ST",
      },
      miscellaneous = {
         hidden = false,
      },
      hardware = {
         limits = {
            min = 0,
            max = 400,
         },
      },
      size = {
         width = MSC_CONFIG.dashHeight,
         height = MSC_CONFIG.dashWidth,
      },
      anchor = {
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Top,
         offsetX = 0,
         offsetY = 0,
      },
      backdrop = GGD.Backdrop.Segregate,
   },
};

-- systemDashboardUpper(LF)
   -- currentZoneSignature(NVFS)
SIG_LF_SDU_NVFS.currentZoneSignature.object = {};
SIG_LF_SDU_NVFS.currentZoneSignature.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "CZS",
      },
      miscellaneous = {
         hidden = false,
         name = "CZS:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot12OffsetX,
         offsetY = MSC_CONFIG.slot12OffsetY,
      },
      backdrop = GGD.Backdrop.Empty,
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};

-- systemDashboardUpper(LF)
   -- dashDelimiterUpper(LNF)
SIG_LF_SDU_LF.dashDelimiterUpper.object = {};
SIG_LF_SDU_LF.dashDelimiterUpper.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "DDU",
      },
      miscellaneous = {
         hidden = false,
         color = GGD.Color.Grey,
      },
      size = {
         width = MSC_CONFIG.borderWidth,
      },
      anchor = {
         relativeTo = nil, -- fwd
      },
   },
};

-- systemDashboardUpper(LF)
   -- dashDelimiterMiddle(LNF)
SIG_LF_SDU_LF.dashDelimiterMiddle.object = {};
SIG_LF_SDU_LF.dashDelimiterMiddle.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "DDM",
      },
      miscellaneous = {
         hidden = false,
         color = GGD.Color.Grey,
      },
      size = {
         width = MSC_CONFIG.borderWidth,
      },
      anchor = {
         relativeTo = nil, -- fwd
      },
   },
};

-- systemDashboardUpper(LF)
   -- dashDelimiterLower(LNF)
SIG_LF_SDU_LF.dashDelimiterLower.object = {};
SIG_LF_SDU_LF.dashDelimiterLower.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "DDL",
      },
      miscellaneous = {
         hidden = false,
         color = GGD.Color.Grey,
      },
      size = {
         width = MSC_CONFIG.borderWidth,
      },
      anchor = {
         relativeTo = nil, -- fwd
      },
   },
};

-- systemDashboardUpper(LF)
   -- honorKillsMeter(DD)
SIG_LF_SDU_DD.honorKillsMeter.object = {};
SIG_LF_SDU_DD.honorKillsMeter.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "HKM",
      },
      miscellaneous = {
         hidden = false,
         fontHeight = function() return MSC_CONFIG.dashlightSide() end,
      },
      size = {
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
      color = GGD.Color.Green,
   },
};

-- systemDashboardLower(LF)
SIG_LF.systemDashboardLower.object = {};
SIG_LF.systemDashboardLower.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "SDL",
		},
		miscellaneous = {
			hidden = false,
		},
		size = {
			width = MSC_CONFIG.dashWidth,
			height = MSC_CONFIG.dashHeight,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = 0,
		},
	},
};