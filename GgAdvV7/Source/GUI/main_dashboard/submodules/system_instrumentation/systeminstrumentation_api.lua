local This = GGF.ModuleGetWrapper("GUI-1-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-- Смена видимости основной панели
function This:SetDashboardPanelHidden(in_hdn)
	self:setDashboardPanelHidden(in_hdn);
end

-- Смена геометрического места панели по оси X
function This:SetDashboardPanelOffsetX(in_offsetX)
	self:setDashboardPanelOffsetX(in_offsetX);
end

-- Смена геометрического места панели по оси Y
function This:SetDashboardPanelOffsetY(in_offsetY)
	self:setDashboardPanelOffsetY(in_offsetY);
end

-- Установка текущего значения скорости в измеритель
function This:SetMoveSpeedMeterValue(in_val)
   self:setMoveSpeedMeterValue(in_val);
end

-- Установка среднего значения скорости в доп. поле измерителя
function This:SetMoveSpeedMeterAverageValue(in_val)
   self:setMoveSpeedMeterAverageValue(in_val);
end

-- Управление заголовком названия пб
function This:SetCurrentZoneSignature(in_name)
	self:setCurrentZoneSignature(in_name);
end

-- Управление индикатором количества хонор килов
function This:SetHonorKillsCount(in_count)
   self:setHonorKillsCount(in_count);
end

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-- Взятие ширины основной панели
function This:GetDashboardPanelWidth()
	return self:getDashboardPanelWidth();
end

-- Взятие высоты основной панели
function This:GetDashboardPanelHeight()
	return self:getDashboardPanelHeight();
end

-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------