local This = GGF.ModuleGetWrapper("GUI-1-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------

-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end

-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------

-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------

	-- systemDashboardUpper(LF)
	self.JR_SDU_LF_OBJ = GGC.LayoutFrame:Create(self.JR_SDU_LF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.MainDashboard:GetSystemInstrumentationFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.MainDashboard:GetSystemInstrumentationFrame(),
			},
		},
	});

   -- systemDashboardUpper(LF)
      -- speedometer(BRDM)
   self.JR_SDU_LF_S_BRDM_OBJ = GGC.BarDataMeter:Create(self.JR_SDU_LF_S_BRDM_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_SDU_LF_OBJ),
         },
         hardware = {
            miscellaneous = {
               dataMeterBarType = GGE.DataMeterBarType.RightDirected,
               dataMeterMetaSideType = GGE.DataMeterMetaSideType.Down,
            },
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_SDU_LF_OBJ),
         },
      },
   });

   -- systemDashboardUpper(LF)
      -- speedometer_tmp(BRDM)
   self.JR_SDU_LF_ST_BRDM_OBJ = GGC.BarDataMeter:Create(self.JR_SDU_LF_ST_BRDM_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_SDU_LF_OBJ),
         },
         hardware = {
            miscellaneous = {
               dataMeterBarType = GGE.DataMeterBarType.DownDirected,
               dataMeterMetaSideType = GGE.DataMeterMetaSideType.Right,
            },
         },
         anchor = {
            point = GGE.PointType.BottomLeft,
            relativeTo = GGF.INS.GetObjectRef(self.JR_SDU_LF_S_BRDM_OBJ),
            relativePoint = GGE.PointType.BottomRight,
         },
      },
   });

   -- systemDashboardUpper(LF)
      -- currentZoneSignature(NVFS)
   self.JR_SDU_LF_CZS_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_SDU_LF_CZS_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_SDU_LF_OBJ),
         },
      },
   });

   -- systemDashboardUpper(LF)
      -- dashDelimiterUpper(LNF)
   self.JR_SDU_LF_DDU_LNF_OBJ = GGC.LineFrame:Create(self.JR_SDU_LF_DDU_LNF_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_SDU_LF_OBJ),
         },
      },
   });

   -- systemDashboardUpper(LF)
      -- dashDelimiterMiddle(LNF)
   self.JR_SDU_LF_DDM_LNF_OBJ = GGC.LineFrame:Create(self.JR_SDU_LF_DDM_LNF_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_SDU_LF_OBJ),
         },
      },
   });

   -- systemDashboardUpper(LF)
      -- dashDelimiterLower(LF)
   self.JR_SDU_LF_DDL_LNF_OBJ = GGC.LineFrame:Create(self.JR_SDU_LF_DDL_LNF_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_SDU_LF_OBJ),
         },
      },
   });

   -- systemDashboardUpper(LF)
      -- honorKillsMeter(DD)
   self.JR_SDU_LF_HKM_DD_OBJ = GGC.DigitalDisplay:Create(self.JR_SDU_LF_HKM_DD_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_SDU_LF_OBJ),
         },
      },
   });

	-- systemDashboardLower(LF)
	self.JR_SDL_LF_OBJ = GGC.LayoutFrame:Create(self.JR_SDL_LF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.MainDashboard:GetSystemInstrumentationFrame(),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_SDU_LF_OBJ),
			},
		},
	});
end

-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- systemDashboardUpper(LF)
	GGF.OuterInvoke(self.JR_SDU_LF_OBJ, "Adjust");

   -- systemDashboardUpper(LF)
      -- speedometer(BRDM)
   GGF.OuterInvoke(self.JR_SDU_LF_S_BRDM_OBJ, "Adjust");

   -- systemDashboardUpper(LF)
      -- speedometer_tmp(BRDM)
   --GGF.OuterInvoke(self.JR_SDU_LF_ST_BRDM_OBJ, "Adjust");

   -- systemDashboardUpper(LF)
      -- currentZoneSignature(NVFS)
   GGF.OuterInvoke(self.JR_SDU_LF_CZS_NVFS_OBJ, "Adjust");

   -- systemDashboardUpper(LF)
      -- dashDelimiterUpper(LNF)
   --GGF.OuterInvoke(self.JR_SDU_LF_DDU_LNF_OBJ, "Adjust");

   -- systemDashboardUpper(LF)
      -- dashDelimiterMiddle(LNF)
   --GGF.OuterInvoke(self.JR_SDU_LF_DDU_LNF_OBJ, "Adjust");

   -- systemDashboardUpper(LF)
      -- dashDelimiterLower(LNF)
   --GGF.OuterInvoke(self.JR_SDU_LF_DDU_LNF_OBJ, "Adjust");

   -- systemDashboardUpper(LF)
      -- honorKillsMeter(DD)
   GGF.OuterInvoke(self.JR_SDU_LF_HKM_DD_OBJ, "Adjust");

	-- systemDashboardLower(LF)
	GGF.OuterInvoke(self.JR_SDL_LF_OBJ, "Adjust");
end

-- п.2.8 (After-load Settings)
function This:MPRC_ALS_INITIALIZE()
   This:setLayoutOrder();
   This:resetLayout();
end

-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------

-- п.3.0 (Addon System Timer)
function This:MPRC_AST_RUNTIME(in_elapsed)
   -- NOTE: этот таймер отладочный пока что
   GGF.Timer(self.vs_telemetryTimer, in_elapsed, function()
      --if (MirrorTimer1:IsVisible()) then
         --print(MirrorTimer1:GetBackdropColor());
      --send
      --print("dat 1: ", MirrorTimer1:IsVisible());
      --print("dat 2: ", MirrorTimer2:IsVisible());
      --print("dat 3: ", MirrorTimer3:IsVisible());



      --print("bonds:", IsStealthed());
      --print("swim:", IsSwimming());
      --print("pos: ", GetPlayerMapPosition("Player"));
      --print("spd: ", GetUnitSpeed("Player"));
      --print("fly: ", IsFlying());
      --print("mnt: ", IsMounted());
      --print("fll: ", IsFalling());
      --print("txi: ", UnitOnTaxi("Player"));
   end);
end

-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------