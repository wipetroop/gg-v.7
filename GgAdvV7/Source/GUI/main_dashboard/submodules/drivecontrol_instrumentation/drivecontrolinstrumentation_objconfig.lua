local This = GGF.ModuleGetWrapper("GUI-1-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_drivecontrolInstrumentationGeometry = {
	layoutFrame = GGF.GTemplateTree(GGC.LayoutFrame, {
		drivecontrolDashboard = {					-- [DD_LF]
			inherit = {
				triggeringIconSignal = GGF.GTemplateTree(GGC.TriggeringIconSignal, {
               fallAlert = {},               -- [FLA_TIS]
               fatigueAlert = {},            -- [FTA_TIS]
               drowningAlert = {},           -- [DA_TIS]
               obstacleCollisionAlert = {},  -- [OCA_TIS]
               -- NOTE: swimming/not swimming
               swimState = {},               -- [SWS_TIS]
               -- NOTE: flying/not flying
               flyState = {},                -- [FS_TIS]
               -- NOTE: mounted/not mounted
               mountState = {},              -- [MTS_TIS]
               -- NOTE: stay/walk/run
               movementState = {},           -- [MVS_TIS]
               -- NOTE: stealthed/not stealthed
               stealthState = {},            -- [STS_TIS]
               -- NOTE: cc/no cc
               cruiseControlState = {},      -- [CCS_TIS]
               -- NOTE: on taxi/not on taxi
               taxiState = {},               -- [TS_TIS]
            }),
            --informDisplay = GGF.GTemplateTree(GGC.InformDisplay, {
               --status = {},                  -- [S_ID]
            --}),
            errorDisplay = GGF.GTemplateTree(GGC.ErrorDisplay, {
               status = {},                  -- [S_ED]
            }),
            synchroTimer = GGF.GTemplateTree(GGC.SynchroTimer, {
               check = {},                   -- [C_ST]
            }),
            blinkAnimation = GGF.GTemplateTree(GGC.BlinkAnimation, {
               fallAlert = {},               -- [FLA_BA]
               fatigueAlert = {},            -- [FTA_BA]
               drowningAlert = {},           -- [DA_BA]
            }),
			},
		},
	}),
};

local DIG_LF = This.oc_drivecontrolInstrumentationGeometry.layoutFrame;
local DIG_LF_DD_TIS = DIG_LF.drivecontrolDashboard.inherit.triggeringIconSignal;
local DIG_LF_DD_ED = DIG_LF.drivecontrolDashboard.inherit.errorDisplay;
local DIG_LF_DD_ST = DIG_LF.drivecontrolDashboard.inherit.synchroTimer;
local DIG_LF_DD_BA = DIG_LF.drivecontrolDashboard.inherit.blinkAnimation;

-- drivecontrolDashboard(LF)
DIG_LF.drivecontrolDashboard.object = {};
DIG_LF.drivecontrolDashboard.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "DD",
		},
		miscellaneous = {
			hidden = false,
		},
		size = {
			width = MSC_CONFIG.dashboardWidth,
			height = MSC_CONFIG.dashboardHeight,
		},
		anchor = {
			point = GGE.PointType.TopLeft,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.TopLeft,
			offsetX = 0, -- меняются через конфиг
			offsetY = 0,
		},
	},
};

-- drivecontrolDashboard(LF)
   -- fallAlert(TIS)
DIG_LF_DD_TIS.fallAlert.object = {};
DIG_LF_DD_TIS.fallAlert.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "FLA",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function() end,
               onDeactivate = function() end,
            },
            ["nonCritical"] = {
               texture = GGD.GUI.TexturePath .. "fall_alert_yellow.tga",
               blinking = false,
               alpha = 1,
               onActivate = function()
                  PlaySoundFile(MSC_CONFIG.dashsoundList().fallAlertSound);
               end,
               onDeactivate = function()
               end,
            },
            ["critical"] = {
               texture = GGD.GUI.TexturePath .. "fall_alert_red.tga",
               blinking = false,
               alpha = 1,
               onActivate = function(in_object)
               end,
               onDeactivate = function(in_object)
               end,
            },
         },
         --initialState = "idle",
         initialState = "nonCritical",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};

-- drivecontrolDashboard(LF)
   -- fatigueAlert(TIS)
DIG_LF_DD_TIS.fatigueAlert.object = {};
DIG_LF_DD_TIS.fatigueAlert.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "FTA",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["nonCritical"] = {
               texture = GGD.GUI.TexturePath .. "fatigue_alert_yellow.tga",
               blinking = false,
               alpha = 1,
               onActivate = function()
                  PlaySoundFile(MSC_CONFIG.dashsoundList().fatigueAlertSound);
               end,
               onDeactivate = function()
               end,
            },
            ["critical"] = {
               texture = GGD.GUI.TexturePath .. "fatigue_alert_red.tga",
               blinking = true,
               alpha = 1,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
         },
         --initialState = "idle",
         initialState = "nonCritical",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};

-- drivecontrolDashboard(LF)
   -- drowningAlert(TIS)
DIG_LF_DD_TIS.drowningAlert.object = {};
DIG_LF_DD_TIS.drowningAlert.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "DA",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function() end,
               onDeactivate = function() end,
            },
            ["nonCritical"] = {
               texture = GGD.GUI.TexturePath .. "drowning_alert_yellow.tga",
               blinking = false,
               alpha = 1,
               onActivate = function()
                  PlaySoundFile(MSC_CONFIG.dashsoundList().drowningAlertSound);
               end,
               onDeactivate = function()
               end,
            },
            ["critical"] = {
               texture = GGD.GUI.TexturePath .. "drowning_alert_red.tga",
               blinking = true,
               alpha = 1,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
         },
         --initialState = "idle",
         initialState = "nonCritical",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};

-- drivecontrolDashboard(LF)
   -- obstacleCollisionAlert(TIS)
DIG_LF_DD_TIS.obstacleCollisionAlert.object = {};
DIG_LF_DD_TIS.obstacleCollisionAlert.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "OCA",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function() end,
               onDeactivate = function() end,
            },
            ["triggered"] = {
               texture = GGD.GUI.TexturePath .. "obstacle_alert.tga",
               blinking = true,
               alpha = 1,
               onActivate = function() end,
               onDeactivate = function() end,
            },
         },
         --initialState = "idle",
         initialState = "triggered",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};

-- drivecontrolDashboard(LF)
   -- swimState(TIS)
DIG_LF_DD_TIS.swimState.object = {};
DIG_LF_DD_TIS.swimState.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "SWS",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["noswim"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function() end,
               onDeactivate = function() end,
            },
            ["swim"] = {
               texture = GGD.GUI.TexturePath .. "swim_state_nw.tga",
               blinking = false,
               alpha = 1,
               onActivate = function() end,
               onDeactivate = function() end,
            },
         },
         --initialState = "idle",
         initialState = "swim",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};

-- drivecontrolDashboard(LF)
   -- flyState(TIS)
DIG_LF_DD_TIS.flyState.object = {};
DIG_LF_DD_TIS.flyState.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "FS",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["nofly"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function() end,
               onDeactivate = function() end,
            },
            ["fly"] = {
               texture = GGD.GUI.TexturePath .. "fly_state.tga",
               blinking = false,
               alpha = 1,
               onActivate = function() end,
               onDeactivate = function() end,
            },
         },
         --initialState = "idle",
         initialState = "fly",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};

-- drivecontrolDashboard(LF)
   -- mountState(TIS)
DIG_LF_DD_TIS.mountState.object = {};
DIG_LF_DD_TIS.mountState.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "MTS",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["nomounted"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function() end,
               onDeactivate = function() end,
            },
            ["mounted"] = {
               texture = GGD.GUI.TexturePath .. "mount_state.tga",
               blinking = false,
               alpha = 1,
               onActivate = function() end,
               onDeactivate = function() end,
            },
         },
         --initialState = "nomounted",
         initialState = "mounted",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};

-- drivecontrolDashboard(LF)
   -- movementState(TIS)
DIG_LF_DD_TIS.movementState.object = {};
DIG_LF_DD_TIS.movementState.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "MVS",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function() end,
               onDeactivate = function() end,
            },
            ["stay"] = {
               texture = GGD.GUI.TexturePath .. "movement_type_stay.tga",
               blinking = false,
               alpha = 1,
               onActivate = function() end,
               onDeactivate = function() end,
            },
            ["walk"] = {
               texture = GGD.GUI.TexturePath .. "movement_type_walk.tga",
               blinking = false,
               alpha = 1,
               onActivate = function() end,
               onDeactivate = function() end,
            },
            ["run"] = {
               texture = GGD.GUI.TexturePath .. "movement_type_run.tga",
               blinking = false,
               alpha = 1,
               onActivate = function() end,
               onDeactivate = function() end,
            },
         },
         initialState = "stay",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};

-- drivecontrolDashboard(LF)
   -- stealthState(TIS)
DIG_LF_DD_TIS.stealthState.object = {};
DIG_LF_DD_TIS.stealthState.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "STS",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["nostealth"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function() end,
               onDeactivate = function() end,
            },
            ["stealth"] = {
               texture = GGD.GUI.TexturePath .. "stealth_state.tga",
               blinking = false,
               alpha = 1,
               onActivate = function() end,
               onDeactivate = function() end,
            },
         },
         --initialState = "nostealth",
         initialState = "stealth",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};

-- drivecontrolDashboard(LF)
   -- cruiseControlState(TIS)
DIG_LF_DD_TIS.cruiseControlState.object = {};
DIG_LF_DD_TIS.cruiseControlState.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "CCS",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["cruiseoff"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function() end,
               onDeactivate = function() end,
            },
            ["cruiseon"] = {
               texture = GGD.GUI.TexturePath .. "cruise_control_state.tga",
               blinking = false,
               alpha = 1,
               onActivate = function() end,
               onDeactivate = function() end,
            },
         },
         --initialState = "cruiseoff",
         initialState = "cruiseon",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};

-- drivecontrolDashboard(LF)
   -- taxiState(TIS)
DIG_LF_DD_TIS.taxiState.object = {};
DIG_LF_DD_TIS.taxiState.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "TS",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["notaxi"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function() end,
               onDeactivate = function() end,
            },
            ["taxi"] = {
               texture = GGD.GUI.TexturePath .. "taxi_state.tga",
               blinking = false,
               alpha = 1,
               onActivate = function() end,
               onDeactivate = function() end,
            },
         },
         --initialState = "notaxi",
         initialState = "taxi",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};

-- drivecontrolDashboard(LF)
   -- status(ID)
--DIG_LF_DD_ID.status.object = {};
--DIG_LF_DD_ID.status.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   --properties = {
      --base = {
         --parentName = nil, -- fwd
         --parent = nil,     -- fwd
         --wrapperName = "S",
      --},
      --miscellaneous = {
         --hidden = false,
         --fontHeight = function() return MSC_CONFIG.dashlightSide() end,
      --},
      --size = {
         --width = MSC_CONFIG.dashlightSide,
         --height = MSC_CONFIG.dashlightSide,
      --},
      --anchor = {
         --point = GGE.PointType.Center,
         --relativeTo = nil, -- fwd
         --relativePoint = GGE.PointType.Center,
         --offsetX = 0,
         --offsetY = 0,
      --},
      --color = GGD.Color.Green,
   --},
--};

-- drivecontrolDashboard(LF)
   -- status(ED)
DIG_LF_DD_ED.status.object = {};
DIG_LF_DD_ED.status.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "S",
      },
      miscellaneous = {
         hidden = false,
         fontHeight = function() return MSC_CONFIG.dashlightSide() end,
      },
      size = {
         width = function() return 2*MSC_CONFIG.dashlightSide() end,
         height = function() return MSC_CONFIG.dashlightSide() end,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
      color = GGD.Color.Green,
      --backdrop = GGD.Backdrop.Segregate,
      backdrop = GGD.Backdrop.Empty,
   },
};

-- drivecontrolDashboard(LF)
   -- check(ST)
DIG_LF_DD_ST.check.object = {};
DIG_LF_DD_ST.check.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         wrapperName = "C",
      },
      parameters = {
         time = 1,   -- NOTE: вроде как выверенный интервал(должен совпадать с интервалом чека из конфига)
         callbackList = {},
      },
   },
};

-- drivecontrolDashboard(LF)
   -- fallAlert(BA)
DIG_LF_DD_BA.fallAlert.object = {};
DIG_LF_DD_BA.fallAlert.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         wrapperName = "FLA",
      },
      parameters = {
         time = {
            interval = MSC_CONFIG.blinkInterval,
         },
      },
   },
};

-- drivecontrolDashboard(LF)
   -- fatigueAlert(BA)
DIG_LF_DD_BA.fatigueAlert.object = {};
DIG_LF_DD_BA.fatigueAlert.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         wrapperName = "FTA",
      },
      parameters = {
         time = {
            interval = MSC_CONFIG.blinkInterval,
         },
      },
   },
};

-- drivecontrolDashboard(LF)
   -- drowningAlert(BA)
DIG_LF_DD_BA.drowningAlert.object = {};
DIG_LF_DD_BA.drowningAlert.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         wrapperName = "DA",
      },
      parameters = {
         time = {
            interval = MSC_CONFIG.blinkInterval,
         },
      },
   },
};