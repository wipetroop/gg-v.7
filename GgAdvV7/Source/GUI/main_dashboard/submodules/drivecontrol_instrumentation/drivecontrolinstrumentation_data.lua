local This = GGF.ModuleCreateWrapper("GUI-1-2", true, {
	dashboardWidth = function() return GGM.GUI.ParameterStorage:getDMDashboardWidth() end,
	dashboardHeight = function() return GGM.GUI.ParameterStorage:getDMDashboardHeight() end,

	dashlightSide = function() return GGM.GUI.ParameterStorage:getDMDashlightSide() end,
	-- NOTE: тут какая-то дичь...

   dashsoundList = function() return GGM.GUI.DrivecontrolInstrumentation:GetConfigDashsoundList() end,

	dashlightGap = function()
		return GGM.GUI.ParameterStorage:getDMDashlightSide()/6.0;
		-- NOTE: 12 - количество скиллов на панели
		--return GGM.GUI.ParameterStorage:getDMDashboardWidth()/14.0 - GGM.GUI.ParameterStorage:getDMDashlightSide()*12.0/14.0;
	end,

	blinkInterval = 0.5,
});

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------

This.cl_fallAlertSound = GGD.GUI.SoundPath.."mp3\\fall_alert.mp3";
This.cl_fatigueAlertSound = GGD.GUI.SoundPath.."mp3\\fatigue_alert.mp3";
This.cl_drowningAlertSound = GGD.GUI.SoundPath.."mp3\\drowning_alert.mp3";

This.cl_skipLine = "skip";

-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Functors
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------


-- Таймер мигания значка(fallAlert)
This.vs_flaBlinkTimer = {
	tick = 0,
	interval = 0.485,	-- NOTE: не трогать!!! подгон!!!
	stopped = false,
};


-- Таймер мигания значка(fatigueAlert)
This.vs_ftaBlinkTimer = {
	tick = 0,
	interval = 0.5,
	stopped = true,
};


-- Таймер мигания значка(drowningAlert)
This.vs_daBlinkTimer = {
	tick = 0,
	interval = 0.5,
	stopped = true,
};


-- Таймер обновления телеметрии
This.vs_telemetryTimer = {
	tick = 0,
	interval = 0.5,
	stopped = false,
};

This.vs_iconMap = {};

-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------