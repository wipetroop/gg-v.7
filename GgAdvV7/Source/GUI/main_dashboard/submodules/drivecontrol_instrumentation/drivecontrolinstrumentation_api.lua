local This = GGF.ModuleGetWrapper("GUI-1-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Управление геометрической местом панели по оси X
function This:SetDashboardPanelOffsetX(in_offsetX)
	self:setDashboardPanelOffsetX(in_offsetX);
end


-- Управление геометрической местом панели по оси Y
function This:SetDashboardPanelOffsetY(in_offsetY)
	self:setDashboardPanelOffsetY(in_offsetY);
end


-- Управление видимостью панели
function This:SetDashboardPanelHidden(in_vis)
	self:setDashboardPanelHidden(in_vis);
end

-- Установка состояния чека FLA системы TCEWS(СРПСЗ)
function This:SetFLAState(in_state)
   self:setFLAState(in_state);
end

-- Установка состояния чека FTA системы DFEWS(СРПСУ)
function This:SetFTAState(in_state)
   self:setFTAState(in_state);
end

-- Установка состояния чека DA системы DDEWS(СРПСП)
function This:SetDAState(in_state)
   self:setDAState(in_state);
end

-- Установка состояния OCA системы PMPCS(СПКПИ)
function This:SetOCAState(in_state)
   self:setOCAState(in_state);
end

-- Установка состояния SWS системы PMPCS(СПКПИ)
function This:SetSWSState(in_state)
   self:setSWSState(in_state);
end

-- Установка состояния FS системы PMPCS(СПКПИ)
function This:SetFSState(in_state)
   self:setFSState(in_state);
end

-- Установка состояния MTS системы PMPCS(СПКПИ)
function This:SetMTSState(in_state)
   self:setMTSState(in_state);
end

-- Установка состояния MVS системы PMPCS(СПКПИ)
function This:SetMVSState(in_state)
   self:setMVSState(in_state);
end

-- Установка состояния STS системы PMPCS(СПКПИ)
function This:SetSTSState(in_state)
   self:setSTSState(in_state);
end

-- Установка состояния CCS системы PMPCS(СПКПИ)
function This:SetCCSState(in_state)
   self:setCCSState(in_state);
end

-- Установка состояния TS системы PMPCS(СПКПИ)
function This:SetTSState(in_state)
   self:setTSState(in_state);
end

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-- Взятие ширины основной панели
function This:GetDashboardPanelWidth()
	return self:getDashboardPanelWidth();
end

-- Взятие высоты основной панели
function This:GetDashboardPanelHeight()
	return self:getDashboardPanelHeight();
end

-- Взятие списка звуков панели контроля движения
function This:GetConfigDashsoundList()
   return self:getConfigDashsoundList();
end

-- Взятие состояния FLA системы TCEWS(СРПСЗ)
function This:GetFLAState()
   return self:getFLAState();
end

-- Взятие состояния FTA системы DFEWS(СРПСУ)
function This:GetFTAState()
   return self:getFTAState();
end

-- Взятие состояния DA системы DDEWS(СРПСП)
function This:GetDAState()
   return self:getDAState();
end

-- Взятие состояния OCA системы PMPCS(СПКПИ)
function This:GetOCAState()
   return self:getOCAState();
end

-- Взятие состояния SWS системы PMPCS(СПКПИ)
function This:GetSWSState()
   return self:getSWSState();
end

-- Взятие состояния FS системы PMPCS(СПКПИ)
function This:GetFSState()
   return self:getFSState();
end

-- Взятие состояния MS системы PMPCS(СПКПИ)
function This:GetMSState()
   return self:getMSState();
end

-- Взятие состояния TS системы PMPCS(СПКПИ)
function This:GetTSState()
   return self:getTSState();
end

-- Взятие состояния MT системы PMPCS(СПКПИ)
function This:GetMTState()
   return self:getMTState();
end

-- Взятие состояния CCS системы PMPCS(СПКПИ)
function This:GetCCSState()
   return self:getCCSState();
end

-- Взятие состояния STS системы PMPCS(СПКПИ)
function This:GetSTSState()
   return self:getSTSState();
end

-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------