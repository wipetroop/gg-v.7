local This = GGF.ModuleGetWrapper("GUI-1-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	local MSC_CONFIG = self:getConfig();
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------

	-- drivecontrolDashboard(LF)
	self.JR_DD_LF_OBJ = GGC.LayoutFrame:Create(self.JR_DD_LF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.MainDashboard:GetDrivecontrolInstrumentationFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.MainDashboard:GetDrivecontrolInstrumentationFrame(),
			},
		},
	});

   -- drivecontrolDashboard(LF)
      -- fallAlert(TIS)
   self.JR_DD_LF_FLA_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_DD_LF_FLA_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_DD_LF_OBJ),
         },
      },
   });

   -- drivecontrolDashboard(LF)
      -- fatigueAlert(TIS)
   self.JR_DD_LF_FTA_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_DD_LF_FTA_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_DD_LF_OBJ),
         },
      },
   });

   -- drivecontrolDashboard(LF)
      -- drowningAlert(TIS)
   self.JR_DD_LF_DA_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_DD_LF_DA_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_DD_LF_OBJ),
         },
      },
   });

   -- drivecontrolDashboard(LF)
      -- obstacleCollisionAlert(TIS)
   self.JR_DD_LF_OCA_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_DD_LF_OCA_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_DD_LF_OBJ),
         },
      },
   });

   -- drivecontrolDashboard(LF)
      -- swimState(TIS)
   self.JR_DD_LF_SWS_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_DD_LF_SWS_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_DD_LF_OBJ),
         },
      },
   });

   -- drivecontrolDashboard(LF)
      -- flyState(TIS)
   self.JR_DD_LF_FS_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_DD_LF_FS_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_DD_LF_OBJ),
         },
      },
   });

   -- drivecontrolDashboard(LF)
      -- mountState(TIS)
   self.JR_DD_LF_MTS_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_DD_LF_MTS_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_DD_LF_OBJ),
         },
      },
   });

   -- drivecontrolDashboard(LF)
      -- movementState(TIS)
   self.JR_DD_LF_MVS_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_DD_LF_MVS_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_DD_LF_OBJ),
         },
      },
   });

   -- drivecontrolDashboard(LF)
      -- stealthState(TIS)
   self.JR_DD_LF_STS_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_DD_LF_STS_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_DD_LF_OBJ),
         },
      },
   });

   -- drivecontrolDashboard(LF)
      -- cruiseControlState(TIS)
   self.JR_DD_LF_CCS_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_DD_LF_CCS_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_DD_LF_OBJ),
         },
      },
   });

   -- drivecontrolDashboard(LF)
      -- taxiState(TIS)
   self.JR_DD_LF_TS_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_DD_LF_TS_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_DD_LF_OBJ),
         },
      },
   });

   -- drivecontrolDashboard(LF)
      -- status(ID)
   --self.JR_DD_LF_S_ID_OBJ = GGC.InformDisplay:Create(self.JR_DD_LF_S_ID_TMP, {
      --properties = {
         --base = {
            --parent = GGF.INS.GetObjectRef(self.JR_DD_LF_OBJ),
         --},
      --},
   --});

   -- drivecontrolDashboard(LF)
      -- status(ED)
   self.JR_DD_LF_S_ED_OBJ = GGC.ErrorDisplay:Create(self.JR_DD_LF_S_ED_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_DD_LF_OBJ),
         },
      },
   });

   -- drivecontrolDashboard(LF)
      -- check(ST)
   self.JR_DD_LF_C_ST_OBJ = GGC.SynchroTimer:Create(self.JR_DD_LF_C_ST_TMP, {
      properties = {
         parameters = {
            callbackList = {
               ["Common"] = function()
                  local ce = GGF.OuterInvoke(self.JR_DD_LF_FLA_TIS_OBJ, "IsBlinking")
                     and GGF.OuterInvoke(self.JR_DD_LF_FLA_BA_OBJ, "Launch");
                  local ca = GGF.OuterInvoke(self.JR_DD_LF_FTA_TIS_OBJ, "IsBlinking")
                     and GGF.OuterInvoke(self.JR_DD_LF_FTA_BA_OBJ, "Launch");
                  local cc = GGF.OuterInvoke(self.JR_DD_LF_DA_TIS_OBJ, "IsBlinking")
                     and GGF.OuterInvoke(self.JR_DD_LF_DA_BA_OBJ, "Launch");
               end,
            },
         },
      },
   });

   -- drivecontrolDashboard(LF)
      -- fallAlert(BA)
   self.JR_DD_LF_FLA_BA_OBJ = GGC.BlinkAnimation:Create(self.JR_DD_LF_FLA_BA_TMP, {
      properties = {
         base = {
            target = GGF.INS.GetObjectRef(self.JR_DD_LF_FLA_TIS_OBJ),
         },
      },
   });

   -- drivecontrolDashboard(LF)
      -- fatigueAlert(BA)
   self.JR_DD_LF_FTA_BA_OBJ = GGC.BlinkAnimation:Create(self.JR_DD_LF_FTA_BA_TMP, {
      properties = {
         base = {
            target = GGF.INS.GetObjectRef(self.JR_DD_LF_FTA_TIS_OBJ),
         },
      },
   });

   -- drivecontrolDashboard(LF)
      -- drowningAlert(BA)
   self.JR_DD_LF_DA_BA_OBJ = GGC.BlinkAnimation:Create(self.JR_DD_LF_DA_BA_TMP, {
      properties = {
         base = {
            target = GGF.INS.GetObjectRef(self.JR_DD_LF_DA_TIS_OBJ),
         },
      },
   });
end


-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- drivecontrolDashboard(LF)
	GGF.OuterInvoke(self.JR_DD_LF_OBJ, "Adjust");

	-- drivecontrolDashboard(LF)
      -- fallAlert(TIS)
   GGF.OuterInvoke(self.JR_DD_LF_FLA_TIS_OBJ, "Adjust");

   -- drivecontrolDashboard(LF)
      -- fatigueAlert(TIS)
   GGF.OuterInvoke(self.JR_DD_LF_FTA_TIS_OBJ, "Adjust");

   -- drivecontrolDashboard(LF)
      -- drowningAlert(TIS)
   GGF.OuterInvoke(self.JR_DD_LF_DA_TIS_OBJ, "Adjust");

   -- drivecontrolDashboard(LF)
      -- obstacleCollisionAlert(TIS)
   GGF.OuterInvoke(self.JR_DD_LF_OCA_TIS_OBJ, "Adjust");

   -- drivecontrolDashboard(LF)
      -- swimState(TIS)
   GGF.OuterInvoke(self.JR_DD_LF_SWS_TIS_OBJ, "Adjust");

   -- drivecontrolDashboard(LF)
      -- flyState(TIS)
   GGF.OuterInvoke(self.JR_DD_LF_FS_TIS_OBJ, "Adjust");

   -- drivecontrolDashboard(LF)
      -- mountState(TIS)
   GGF.OuterInvoke(self.JR_DD_LF_MTS_TIS_OBJ, "Adjust");

   -- drivecontrolDashboard(LF)
      -- movementState(TIS)
   GGF.OuterInvoke(self.JR_DD_LF_MVS_TIS_OBJ, "Adjust");

   -- drivecontrolDashboard(LF)
      -- stealthState(TIS)
   GGF.OuterInvoke(self.JR_DD_LF_STS_TIS_OBJ, "Adjust");

   -- drivecontrolDashboard(LF)
      -- cruiseControlState(TIS)
   GGF.OuterInvoke(self.JR_DD_LF_CCS_TIS_OBJ, "Adjust");

   -- drivecontrolDashboard(LF)
      -- taxiState(TIS)
   GGF.OuterInvoke(self.JR_DD_LF_TS_TIS_OBJ, "Adjust");

   -- drivecontrolDashboard(LF)
      -- status(ID)
   --GGF.OuterInvoke(self.JR_DD_LF_S_ID_OBJ, "Adjust");
   --GGF.OuterInvoke(self.JR_DD_LF_S_ID_OBJ, "Launch");

   -- drivecontrolDashboard(LF)
      -- status(ED)
   GGF.OuterInvoke(self.JR_DD_LF_S_ED_OBJ, "Adjust");
   GGF.OuterInvoke(self.JR_DD_LF_S_ED_OBJ, "Launch");

   -- drivecontrolDashboard(LF)
      -- check(ST)
   GGF.OuterInvoke(self.JR_DD_LF_C_ST_OBJ, "Adjust");

   -- drivecontrolDashboard(LF)
      -- fallAlert(BA)
   GGF.OuterInvoke(self.JR_DD_LF_FLA_BA_OBJ, "Adjust");

   -- drivecontrolDashboard(LF)
      -- fatigueAlert(BA)
   GGF.OuterInvoke(self.JR_DD_LF_FTA_BA_OBJ, "Adjust");

   -- drivecontrolDashboard(LF)
      -- drowningAlert(BA)
   GGF.OuterInvoke(self.JR_DD_LF_DA_BA_OBJ, "Adjust");
end

-- п.2.8 (After-load Settings)
function This:MPRC_ALS_INITIALIZE()
   This:setLayoutOrder();
   This:resetLayout();
end

-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-- п.3.0 (Addon System Timer)
function This:MPRC_AST_RUNTIME(in_elapsed)
   GGF.OuterInvoke(self.JR_DD_LF_C_ST_OBJ, "Step", in_elapsed);
	--GGF.Timer(self.vs_flaBlinkTimer, in_elapsed, function()
		--GGM.GUI.AnimationEngine:CreateAlertBlinkAnimationOnFrame(self.JR_DD_F_FLAR_T_OBJ:GetObject(), self.vs_flaBlinkTimer.interval, self.cl_fallAlertSound);
	--end);

	--GGF.Timer(self.vs_ftaBlinkTimer, in_elapsed, function()
		--GGM.GUI.AnimationEngine:CreateAlertBlinkAnimationOnFrame(self.JR_DD_F_FTAR_T_OBJ:GetObject(), self.vs_ftaBlinkTimer.interval, self.cl_fatigueAlertSound);
	--end);

	--GGF.Timer(self.vs_daBlinkTimer, in_elapsed, function()
		--GGM.GUI.AnimationEngine:CreateAlertBlinkAnimationOnFrame(self.JR_DD_F_DAR_T_OBJ:GetObject(), self.vs_daBlinkTimer.interval, self.cl_drowningAlertSound);
	--end);

	--GGF.OuterInvoke(self.JR_DD_LF_C_ST_OBJ, "Step", in_elapsed);


	-- NOTE: этот таймер отладочный пока что
	--GGF.Timer(self.vs_telemetryTimer, in_elapsed, function()
		--if (MirrorTimer1:IsVisible()) then
			--print(MirrorTimer1:GetBackdropColor());
		--send
		--print("dat 1: ", MirrorTimer1:IsVisible());
		--print("dat 2: ", MirrorTimer2:IsVisible());
		--print("dat 3: ", MirrorTimer3:IsVisible());



		--print("bonds:", IsStealthed());
		--print("swim:", IsSwimming());
		--print("pos: ", GetPlayerMapPosition("Player"));
		--print("spd: ", GetUnitSpeed("Player"));
		--print("fly: ", IsFlying());
		--print("mnt: ", IsMounted());
		--print("fll: ", IsFalling());
		--print("txi: ", UnitOnTaxi("Player"));
	--end);
end


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------