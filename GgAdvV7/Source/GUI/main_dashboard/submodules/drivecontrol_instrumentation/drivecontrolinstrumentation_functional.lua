local This = GGF.ModuleGetWrapper("GUI-1-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- api
-- Управление геометрической местом панели по оси X
function This:setDashboardPanelOffsetX(in_offsetX)
	GGF.OuterInvoke(self.JR_DD_LF_OBJ, "SetOffsetX", in_offsetX);
end


-- api
-- Управление геометрической местом панели по оси Y
function This:setDashboardPanelOffsetY(in_offsetY)
	GGF.OuterInvoke(self.JR_DD_LF_OBJ, "SetOffsetY", in_offsetY);
end


-- api
-- Управление видимостью панели
function This:setDashboardPanelHidden(in_hdn)
	GGF.OuterInvoke(self.JR_DD_LF_OBJ, "SetHidden", in_hdn);
end

-- api
-- Установка состояния чека FLA системы TCEWS(СРПСЗ)
function This:setFLAState(in_state)
   GGF.OuterInvoke(self.JR_DD_LF_FLA_TIS_OBJ, "SetState", in_state);
end

-- api
-- Установка состояния чека FTA системы DFEWS(СРПСУ)
function This:setFTAState(in_state)
   GGF.OuterInvoke(self.JR_DD_LF_FTA_TIS_OBJ, "SetState", in_state);
end

-- api
-- Установка состояния чека DA системы DDEWS(СРПСП)
function This:setDAState(in_state)
   GGF.OuterInvoke(self.JR_DD_LF_DA_TIS_OBJ, "SetState", in_state);
end

-- api
-- Установка состояния OCA системы PMPCS(СПКПИ)
function This:setOCAState(in_state)
   GGF.OuterInvoke(self.JR_DD_LF_OCA_TIS_OBJ, "SetState", in_state);
end

-- api
-- Установка состояния SWS системы PMPCS(СПКПИ)
function This:setSWSState(in_state)
   GGF.OuterInvoke(self.JR_DD_LF_SWS_TIS_OBJ, "SetState", in_state);
end

-- api
-- Установка состояния FS системы PMPCS(СПКПИ)
function This:setFSState(in_state)
   GGF.OuterInvoke(self.JR_DD_LF_FS_TIS_OBJ, "SetState", in_state);
end

-- api
-- Установка состояния MTS системы PMPCS(СПКПИ)
function This:setMTSState(in_state)
   GGF.OuterInvoke(self.JR_DD_LF_MTS_TIS_OBJ, "SetState", in_state);
end

-- api
-- Установка состояния MVS системы PMPCS(СПКПИ)
function This:setMVSState(in_state)
   GGF.OuterInvoke(self.JR_DD_LF_MVS_TIS_OBJ, "SetState", in_state);
end

-- api
-- Установка состояния STS системы PMPCS(СПКПИ)
function This:setSTSState(in_state)
   GGF.OuterInvoke(self.JR_DD_LF_STS_TIS_OBJ, "SetState", in_state);
end

-- api
-- Установка состояния CCS системы PMPCS(СПКПИ)
function This:setCCSState(in_state)
   GGF.OuterInvoke(self.JR_DD_LF_CCS_TIS_OBJ, "SetState", in_state);
end

-- api
-- Установка состояния TS системы PMPCS(СПКПИ)
function This:setTSState(in_state)
   GGF.OuterInvoke(self.JR_DD_LF_TS_TIS_OBJ, "SetState", in_state);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-- api
-- Взятие ширины основной панели
function This:getDashboardPanelWidth()
	print("get width");
	return GGF.OuterInvoke(self.JR_DD_LF_OBJ, "GetWidth");
end

-- api
-- Взятие ширины основной панели
function This:getDashboardPanelHeight()
	return GGF.OuterInvoke(self.JR_DD_LF_OBJ, "GetHeight");
end

-- api
-- Взятие списка звуков панели контроля движения
function This:getConfigDashsoundList()
   return {
      fallAlertSound = self.cl_fallAlertSound,
      fatigueAlertSound = self.cl_fatigueAlertSound,
      drowningAlertSound = self.cl_drowningAlertSound,
   };
end

-- api
-- Взятие состояния FLA системы TCEWS(СРПСЗ)
function This:getFLAState()
   return GGF.OuterInvoke(self.JR_DD_LF_FLA_TIS_OBJ, "GetCurrentState");
end

-- api
-- Взятие состояния FTA системы DFEWS(СРПСУ)
function This:getFTAState()
   return GGF.OuterInvoke(self.JR_DD_LF_FTA_TIS_OBJ, "GetCurrentState");
end

-- api
-- Взятие состояния DA системы DDEWS(СРПСП)
function This:getDAState()
   return GGF.OuterInvoke(self.JR_DD_LF_DA_TIS_OBJ, "GetCurrentState");
end

-- api
-- Взятие состояния OCA системы PMPCS(СПКПИ)
function This:getOCAState()
   return GGF.OuterInvoke(self.JR_DD_LF_OCA_TIS_OBJ, "GetCurrentState");
end

-- api
-- Взятие состояния SWS системы PMPCS(СПКПИ)
function This:getSWSState()
   return GGF.OuterInvoke(self.JR_DD_LF_SWS_TIS_OBJ, "GetCurrentState");
end

-- api
-- Взятие состояния FS системы PMPCS(СПКПИ)
function This:getFSState()
   return GGF.OuterInvoke(self.JR_DD_LF_FS_TIS_OBJ, "GetCurrentState");
end

-- api
-- Взятие состояния MS системы PMPCS(СПКПИ)
function This:getMSState()
   return GGF.OuterInvoke(self.JR_DD_LF_MS_TIS_OBJ, "GetCurrentState");
end

-- api
-- Взятие состояния TS системы PMPCS(СПКПИ)
function This:getTSState()
   return GGF.OuterInvoke(self.JR_DD_LF_TS_TIS_OBJ, "GetCurrentState");
end

-- api
-- Взятие состояния MT системы PMPCS(СПКПИ)
function This:getMTState()
   return GGF.OuterInvoke(self.JR_DD_LF_MT_TIS_OBJ, "GetCurrentState");
end

-- api
-- Взятие состояния CCS системы PMPCS(СПКПИ)
function This:getCCSState()
   return GGF.OuterInvoke(self.JR_DD_LF_CCS_TIS_OBJ, "GetCurrentState");
end

-- api
-- Взятие состояния STS системы PMPCS(СПКПИ)
function This:getSTSState()
   return GGF.OuterInvoke(self.JR_DD_LF_STS_TIS_OBJ, "GetCurrentState");
end

-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------

-- Установка порядка расположения элементов панели
function This:setLayoutOrder()
   -- [FLA] [FTA] [DA]  [OCA] []    []    []   [S]
   -- [SWS] [FS]  [MTS] [MVS] [STS] [CCS] [TS] []
   self.vs_iconMap = {
      { self.JR_DD_LF_FLA_TIS_OBJ, self.JR_DD_LF_FTA_TIS_OBJ, self.JR_DD_LF_DA_TIS_OBJ,  self.JR_DD_LF_OCA_TIS_OBJ, self.cl_skipLine,          self.cl_skipLine,          self.cl_skipLine,         self.JR_DD_LF_S_ED_OBJ }, --self.JR_DD_LF_S_ID_OBJ },
      { self.JR_DD_LF_SWS_TIS_OBJ, self.JR_DD_LF_FS_TIS_OBJ,  self.JR_DD_LF_MTS_TIS_OBJ, self.JR_DD_LF_MVS_TIS_OBJ, self.JR_DD_LF_STS_TIS_OBJ, self.JR_DD_LF_CCS_TIS_OBJ, self.JR_DD_LF_TS_TIS_OBJ, self.cl_skipLine }
   };
end

-- Ресет расположения элементов панели
function This:resetLayout()
   local MSC_CONFIG = self:getConfig();
   for idx,dataarray in ipairs(self.vs_iconMap) do
      for jdx,data in ipairs(dataarray) do
         if (data ~= self.cl_skipLine) then
            GGF.OuterInvoke(data, "SetAnchor", {
               point = GGE.PointType.BottomLeft,
               relativeTo = GGF.INS.GetObjectRef(self.JR_DD_LF_OBJ),
               relativePoint = GGE.PointType.BottomLeft,
               offsetX = MSC_CONFIG.dashlightGap() + (jdx - 1)*(MSC_CONFIG.dashlightGap() + MSC_CONFIG.dashlightSide()),
               offsetY = MSC_CONFIG.dashlightGap() + (idx - 1)*(MSC_CONFIG.dashlightGap() + MSC_CONFIG.dashlightSide()),
            });
         end
      end
   end
end

-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------