local This = GGF.ModuleGetWrapper("GUI-1-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------

-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-- api
-- Управление геометрической местом панели по оси X
function This:setDashboardPanelOffsetX(in_offsetX)
	GGF.OuterInvoke(self.JR_MD_LF_OBJ, "SetOffsetX", in_offsetX);
end

-- api
-- Управление геометрической местом панели по оси Y
function This:setDashboardPanelOffsetY(in_offsetY)
	GGF.OuterInvoke(self.JR_MD_LF_OBJ, "SetOffsetY", in_offsetY);
end

-- api
-- Управление видимостью панели
function This:setDashboardPanelHidden(in_hdn)
	GGF.OuterInvoke(self.JR_MD_LF_OBJ, "SetHidden", in_hdn);
end

-- api
-- Управление статусом индикатора check engine
function This:setENGState(in_state)
   GGF.OuterInvoke(self.JR_MD_LF_CE_TIS_OBJ, "SetState", in_state);
end

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-- api
-- Взятие ширины основной панели
function This:getDashboardPanelWidth()
	return GGF.OuterInvoke(self.JR_MD_LF_OBJ, "GetWidth");
end

-- api
-- Взятие ширины основной панели
function This:getDashboardPanelHeight()
	return GGF.OuterInvoke(self.JR_MD_LF_OBJ, "GetHeight");
end

-- api
-- Взятие конфиговой стороны иконки панели приборов
function This:getConfigDashlightSide()
	return self:getConfig().dashlightSide;
end

-- api
-- Взятие конфиговой ширины панели
function This:getConfigDashboardWidth()
	return self:getConfig().dashboardWidth;
end

-- api
-- Взятие конфиговой высоты панели
function This:getConfigDashboardHeight()
	return self:getConfig().dashboardHeight;
end

-- api
-- Взятие основной панели
function This:getDashboardPanelFrame()
	return GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ);
end

-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------

-- Установка порядка расположения элементов панели
function This:setLayoutOrder()
   -- [ENG] [TRM] [LPR] [RAM] [LAT] [FPS] []
   -- [ASB] [ACR] [COM] [FCS] [IOC] [GUI] [SND]
   self.vs_iconMap = {
      { self.JR_MD_LF_CE_TIS_OBJ,  self.JR_MD_LF_CT_TIS_OBJ,  self.JR_MD_LF_CLP_TIS_OBJ, self.JR_MD_LF_CR_TIS_OBJ, self.JR_MD_LF_CL_TIS_OBJ, self.JR_MD_LF_CFP_TIS_OBJ },
      { self.JR_MD_LF_CAS_TIS_OBJ, self.JR_MD_LF_CAC_TIS_OBJ, self.JR_MD_LF_CC_TIS_OBJ,  self.JR_MD_LF_CF_TIS_OBJ, self.JR_MD_LF_CI_TIS_OBJ, self.JR_MD_LF_CG_TIS_OBJ, self.JR_MD_LF_CS_TIS_OBJ }
   };
end

-- Ресет расположения элементов панели
function This:resetLayout()
   local MSC_CONFIG = self:getConfig();
   for idx,dataarray in ipairs(self.vs_iconMap) do
      for jdx,data in ipairs(dataarray) do
         GGF.OuterInvoke(data, "SetAnchor", {
            point = GGE.PointType.BottomLeft,
            relativeTo = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
            relativePoint = GGE.PointType.BottomLeft,
            offsetX = MSC_CONFIG.dashlightGap() + (jdx - 1)*(MSC_CONFIG.dashlightGap() + MSC_CONFIG.dashlightSide()),
            offsetY = MSC_CONFIG.dashlightGap() + (idx - 1)*(MSC_CONFIG.dashlightGap() + MSC_CONFIG.dashlightSide()),
         });
      end
   end
end

-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------