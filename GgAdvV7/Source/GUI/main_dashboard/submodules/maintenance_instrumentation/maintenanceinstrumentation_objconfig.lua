local This = GGF.ModuleGetWrapper("GUI-1-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- TODO: определиться, нужны ли чеки по кластерам
-- Hierarchy
This.oc_maintenanceInstrumentationGeometry = {
	layoutFrame = GGF.GTemplateTree(GGC.LayoutFrame, {
		maintenanceDashboard = {	-- [MD_LF]
			inherit = {
            namedValueFontString = GGF.GTemplateTree(GGC.NamedValueFontString, {
            }),
            triggerIconSignal = GGF.GTemplateTree(GGC.TriggeringIconSignal, {
               checkEngine = {},          -- [CE_TIS]
               -- NOTE: зависит от transmission_pipeline
               checkTransmission = {},    -- [CT_TIS]
               -- NOTE: перезагрузка с холодного старта
               checkLoadProcess = {},     -- [CLP_TIS]
               checkRam = {},             -- [CR_TIS]
               checkLatency = {},         -- [CL_TIS]
               checkFps = {},             -- [CFP_TIS]
               checkAsb = {},             -- [CAS_TIS]
               checkAcr = {},             -- [CAC_TIS]
               checkCom = {},             -- [CC_TIS]
               checkFcs = {},             -- [CF_TIS]
               checkIoc = {},             -- [CI_TIS]
               checkGui = {},             -- [CG_TIS]
               checkSnd = {},             -- [CS_TIS]
            }),
            synchroTimer = GGF.GTemplateTree(GGC.SynchroTimer, {
               check = {},                -- [C_ST]
            }),
            blinkAnimation = GGF.GTemplateTree(GGC.BlinkAnimation, {
               checkEngine = {},          -- [CE_BA]
               checkAsb = {},             -- [CAS_BA]
               checkAcr = {},             -- [CAC_BA]
               checkCom = {},             -- [CC_BA]
               checkFcs = {},             -- [CF_BA]
               checkIoc = {},             -- [CI_BA]
               checkGui = {},             -- [CG_BA]
               checkSnd = {},             -- [CS_BA]
            }),
			},
		},
	}),
};

local MIG_LF = This.oc_maintenanceInstrumentationGeometry.layoutFrame;
local MIG_LF_MD_TIS = MIG_LF.maintenanceDashboard.inherit.triggerIconSignal;
local MIG_LF_MD_ST = MIG_LF.maintenanceDashboard.inherit.synchroTimer;
local MIG_LF_MD_BA = MIG_LF.maintenanceDashboard.inherit.blinkAnimation;

-- maintenanceDashboard(LF)
MIG_LF.maintenanceDashboard.object = {};
MIG_LF.maintenanceDashboard.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "MD",
		},
		miscellaneous = {
			hidden = false,
		},
		size = {
			width = MSC_CONFIG.dashboardWidth,
			height = MSC_CONFIG.dashboardHeight,
		},
		anchor = {
			point = GGE.PointType.TopLeft,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.TopLeft,
			offsetX = 0,
			offsetY = 0,
		},
	},
};

-- maintenanceDashboard(LF)
   -- checkEngine(TIS)
MIG_LF_MD_TIS.checkEngine.object = {};
MIG_LF_MD_TIS.checkEngine.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "CE",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["nonCritical"] = {
               texture = GGD.GUI.TexturePath .. "check_engine_ext_a.tga",
               blinking = false,
               alpha = 1,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["critical"] = {
               texture = GGD.GUI.TexturePath .. "check_engine_ext_a.tga",
               blinking = true,
               alpha = 1,
               onActivate = function(in_meta)
                  GGF.InnerInvoke(in_meta, "Launch");
               end,
               onDeactivate = function(in_meta)
                  GGF.InnerInvoke(in_meta, "Stop");
               end,
            },
         },
         initialState = "idle",
         --initialState = "critical",
      },
      anitronic = {
         animationType = GGE.BlinkAnimationType.Cycled,
         time = {
            interval = 1,
         },
         sound = MSC_CONFIG.checkEngineSound,
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};


-- NOTE: запаска(не трогать!!!)
-- maintenanceDashboard(LF)
   -- checkTransmission(TIS)
MIG_LF_MD_TIS.checkTransmission.object = {};
MIG_LF_MD_TIS.checkTransmission.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil,  -- fwd
         parent = nil,      -- fwd
         wrapperName = "CT",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["triggered"] = {
               texture = GGD.GUI.TexturePath .. "check_transmission.tga",
               blinking = false,
               alpha = 1,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
         },
         --initialState = "idle",
         initialState = "triggered",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Left,
         relativeTo = nil,  -- fwd
         relativePoint = GGE.PointType.Right,
         offsetX = 0,
         offsetY = 0,
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkLoadProcess(TIS)
MIG_LF_MD_TIS.checkLoadProcess.object = {};
MIG_LF_MD_TIS.checkLoadProcess.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "CLP",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["triggered"] = {
               texture = GGD.GUI.TexturePath .. "check_load_process.tga",
               blinking = false,
               alpha = 1,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
         },
         --initialState = "idle",
         initialState = "triggered",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkRam(TIS)
MIG_LF_MD_TIS.checkRam.object = {};
MIG_LF_MD_TIS.checkRam.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "CR",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["triggered"] = {
               texture = GGD.GUI.TexturePath .. "check_ram.tga",
               blinking = false,
               alpha = 1,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
         },
         --initialState = "idle",
         initialState = "triggered",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkLatency(TIS)
MIG_LF_MD_TIS.checkLatency.object = {};
MIG_LF_MD_TIS.checkLatency.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "CL",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["triggered"] = {
               texture = GGD.GUI.TexturePath .. "check_latency.tga",
               blinking = false,
               alpha = 1,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
         },
         --initialState = "idle",
         initialState = "triggered",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkFps(TIS)
MIG_LF_MD_TIS.checkFps.object = {};
MIG_LF_MD_TIS.checkFps.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "CFP",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function(in_meta)
               end,
               onDeactivate = function(in_meta)
               end,
            },
            ["triggered"] = {
               texture = GGD.GUI.TexturePath .. "check_fps.tga",
               blinking = true,
               alpha = 1,
               onActivate = function(in_meta)
                  GGF.InnerInvoke(in_meta, "Launch");
               end,
               onDeactivate = function(in_meta)
                  GGF.InnerInvoke(in_meta, "Stop");
               end,
            },
         },
         --initialState = "idle",
         initialState = "triggered",
      },
      anitronic = {
         animationType = GGE.BlinkAnimationType.Limited,
         time = {
            interval = 1,
         },
         stepCount = 3,
         sound = MSC_CONFIG.fpwWarningSound,
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkAsb(TIS)
MIG_LF_MD_TIS.checkAsb.object = {};
MIG_LF_MD_TIS.checkAsb.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "CAS",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["nonCritical"] = {
               texture = GGD.GUI.TexturePath .. "check_asb.tga",
               blinking = false,
               alpha = 1,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["critical"] = {
               texture = GGD.GUI.TexturePath .. "check_asb.tga",
               blinking = true,
               alpha = 1,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
         },
         --initialState = "idle",
         initialState = "critical",
      },
      anitronic = {
         animationType = GGE.BlinkAnimationType.Cycled,
         time = {
            interval = 1,
         },
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkAcr(TIS)
MIG_LF_MD_TIS.checkAcr.object = {};
MIG_LF_MD_TIS.checkAcr.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "CAC",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["nonCritical"] = {
               texture = GGD.GUI.TexturePath .. "check_acr.tga",
               blinking = false,
               alpha = 1,
               onActivate = function(in_wrapper)
               end,
               onDeactivate = function(in_wrapper)
               end,
            },
            ["critical"] = {
               texture = GGD.GUI.TexturePath .. "check_acr.tga",
               blinking = true,
               alpha = 1,
               onActivate = function(in_wrapper)
               end,
               onDeactivate = function(in_wrapper)
               end,
            },
         },
         --initialState = "idle",
         initialState = "critical",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkCom(TIS)
MIG_LF_MD_TIS.checkCom.object = {};
MIG_LF_MD_TIS.checkCom.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "CC",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["nonCritical"] = {
               texture = GGD.GUI.TexturePath .. "check_com.tga",
               blinking = false,
               alpha = 1,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["critical"] = {
               texture = GGD.GUI.TexturePath .. "check_com.tga",
               blinking = true,
               alpha = 1,
               onActivate = function(in_wrapper)
               end,
               onDeactivate = function(in_wrapper)
               end,
            },
         },
         --initialState = "idle",
         initialState = "critical",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkFcs(TIS)
MIG_LF_MD_TIS.checkFcs.object = {};
MIG_LF_MD_TIS.checkFcs.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "CF",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["nonCritical"] = {
               texture = GGD.GUI.TexturePath .. "check_fcs.tga",
               blinking = false,
               alpha = 1,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["critical"] = {
               texture = GGD.GUI.TexturePath .. "check_fcs.tga",
               blinking = true,
               alpha = 1,
               onActivate = function(in_wrapper)
               end,
               onDeactivate = function(in_wrapper)
               end,
            },
         },
         --initialState = "idle",
         initialState = "critical",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkIoc(TIS)
MIG_LF_MD_TIS.checkIoc.object = {};
MIG_LF_MD_TIS.checkIoc.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "CI",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["nonCritical"] = {
               texture = GGD.GUI.TexturePath .. "check_ioc.tga",
               blinking = false,
               alpha = 1,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["critical"] = {
               texture = GGD.GUI.TexturePath .. "check_ioc.tga",
               blinking = true,
               alpha = 1,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
         },
         --initialState = "idle",
         initialState = "critical",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkGui(TIS)
MIG_LF_MD_TIS.checkGui.object = {};
MIG_LF_MD_TIS.checkGui.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "CG",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["nonCritical"] = {
               texture = GGD.GUI.TexturePath .. "check_gui.tga",
               blinking = false,
               alpha = 1,
               onActivate = function()
               end,
               onDeactivate = function()
               end,
            },
            ["critical"] = {
               texture = GGD.GUI.TexturePath .. "check_gui.tga",
               blinking = true,
               alpha = 1,
               onActivate = function(in_wrapper)
               end,
               onDeactivate = function(in_wrapper)
               end,
            },
         },
         --initialState = "idle",
         initialState = "critical",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkSnd(TIS)
MIG_LF_MD_TIS.checkSnd.object = {};
MIG_LF_MD_TIS.checkSnd.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "CS",
      },
      miscellaneous = {
         hidden = false,
         stateList = {
            ["idle"] = {
               texture = "",
               blinking = false,
               alpha = 0,
               onActivate = function(in_meta)
               end,
               onDeactivate = function(in_meta)
               end,
            },
            ["nonCritical"] = {
               texture = GGD.GUI.TexturePath .. "check_snd.tga",
               blinking = false,
               alpha = 1,
               onActivate = function(in_meta)
               end,
               onDeactivate = function(in_meta)
               end,
            },
            ["critical"] = {
               texture = GGD.GUI.TexturePath .. "check_snd.tga",
               blinking = true,
               alpha = 1,
               onActivate = function(in_meta)
               end,
               onDeactivate = function(in_meta)
               end,
            },
         },
         --initialState = "idle",
         initialState = "critical",
      },
      size = {
         width = MSC_CONFIG.dashlightSide,
         height = MSC_CONFIG.dashlightSide,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
   },
};


-- maintenanceDashboard(LF)
   -- check(ST)
MIG_LF_MD_ST.check.object = {};
MIG_LF_MD_ST.check.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         wrapperName = "C",
      },
      parameters = {
         time = 1,   -- NOTE: вроде как выверенный интервал(должен совпадать с интервалом чека из конфига)
         callbackList = {},
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkEngine(BA)
MIG_LF_MD_BA.checkEngine.object = {};
MIG_LF_MD_BA.checkEngine.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         wrapperName = "CE",
      },
      parameters = {
         time = {
            interval = MSC_CONFIG.blinkInterval,
         },
         sound = MSC_CONFIG.checkEngineSound,
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkAsb(BA)
MIG_LF_MD_BA.checkAsb.object = {};
MIG_LF_MD_BA.checkAsb.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         wrapperName = "CAS",
      },
      parameters = {
         time = {
            interval = MSC_CONFIG.blinkInterval,
         },
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkAcr(BA)
MIG_LF_MD_BA.checkAcr.object = {};
MIG_LF_MD_BA.checkAcr.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         wrapperName = "CAC",
      },
      parameters = {
         time = {
            interval = MSC_CONFIG.blinkInterval,
         },
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkCom(BA)
MIG_LF_MD_BA.checkCom.object = {};
MIG_LF_MD_BA.checkCom.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         wrapperName = "CC",
      },
      parameters = {
         time = {
            interval = MSC_CONFIG.blinkInterval,
         },
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkFcs(BA)
MIG_LF_MD_BA.checkFcs.object = {};
MIG_LF_MD_BA.checkFcs.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         wrapperName = "CF",
      },
      parameters = {
         time = {
            interval = MSC_CONFIG.blinkInterval,
         },
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkIoc(BA)
MIG_LF_MD_BA.checkIoc.object = {};
MIG_LF_MD_BA.checkIoc.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         wrapperName = "CI",
      },
      parameters = {
         time = {
            interval = MSC_CONFIG.blinkInterval,
         },
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkGui(BA)
MIG_LF_MD_BA.checkGui.object = {};
MIG_LF_MD_BA.checkGui.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         wrapperName = "CG",
      },
      parameters = {
         time = {
            interval = MSC_CONFIG.blinkInterval,
         },
      },
   },
};


-- maintenanceDashboard(LF)
   -- checkSnd(BA)
MIG_LF_MD_BA.checkSnd.object = {};
MIG_LF_MD_BA.checkSnd.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         wrapperName = "CS",
      },
      parameters = {
         time = {
            interval = MSC_CONFIG.blinkInterval,
         },
      },
   },
};
-- TODO: тут все чеки замутить(а то чека движка мало)
-- NOTE: вроде же сделано было?