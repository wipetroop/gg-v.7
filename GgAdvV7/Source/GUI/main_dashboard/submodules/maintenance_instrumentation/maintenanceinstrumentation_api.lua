local This = GGF.ModuleGetWrapper("GUI-1-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Управление геометрической местом панели по оси X
function This:SetDashboardPanelOffsetX(in_offsetX)
	self:setDashboardPanelOffsetX(in_offsetX);
end


-- Управление геометрической местом панели по оси Y
function This:SetDashboardPanelOffsetY(in_offsetY)
	self:setDashboardPanelOffsetY(in_offsetY);
end


-- Управление видимостью панели
function This:SetDashboardPanelHidden(in_vis)
	self:setDashboardPanelHidden(in_vis);
end

-- Управление статусом индикатора check engine
function This:SetENGState(in_state)
   self:setENGState(in_state);
end

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие ширины основной панели
function This:GetDashboardPanelWidth()
	return self:getDashboardPanelWidth();
end


-- Взятие высоты основной панели
function This:GetDashboardPanelHeight()
	return self:getDashboardPanelHeight();
end


-- Взятие конфиговой стороны иконки панели приборов
function This:GetConfigDashlightSide()
	return self:getConfigDashlightSide();
end


-- Взятие конфиговой ширины панели
function This:GetConfigDashboardWidth()
	return self:getConfigDashboardWidth();
end


-- Взятие конфиговой высоты панели
function This:GetConfigDashboardHeight()
	return self:getConfigDashboardHeight();
end


-- Взятие основной панели
function This:GetDashboardPanelFrame()
	return self:getDashboardPanelFrame();
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------