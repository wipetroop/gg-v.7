local This = GGF.ModuleGetWrapper("GUI-1-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	local MSC_CONFIG = self:getConfig();
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------

	-- maintenanceDashboard(LF)

	self.JR_MD_LF_OBJ = GGC.LayoutFrame:Create(self.JR_MD_LF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.MainDashboard:GetMaintenanceInstrumentationFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.MainDashboard:GetMaintenanceInstrumentationFrame(),
			},
		},
	});

   -- maintenanceDashboard(LF)
      -- checkEngine(TIS)
   self.JR_MD_LF_CE_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_MD_LF_CE_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
         },
      },
   });

   -- maintenanceDashboard(LF)
      -- checkTransmission(TIS)
   self.JR_MD_LF_CT_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_MD_LF_CT_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkLoadProcess(TIS)
   self.JR_MD_LF_CLP_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_MD_LF_CLP_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkRam(TIS)
   self.JR_MD_LF_CR_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_MD_LF_CR_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkLatency(TIS)
   self.JR_MD_LF_CL_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_MD_LF_CL_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkFps(TIS)
   self.JR_MD_LF_CFP_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_MD_LF_CFP_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkAsb(TIS)
   self.JR_MD_LF_CAS_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_MD_LF_CAS_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkAcr(TIS)
   self.JR_MD_LF_CAC_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_MD_LF_CAC_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkCom(TIS)
   self.JR_MD_LF_CC_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_MD_LF_CC_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkFcs(TIS)
   self.JR_MD_LF_CF_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_MD_LF_CF_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkIoc(TIS)
   self.JR_MD_LF_CI_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_MD_LF_CI_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkGui(TIS)
   self.JR_MD_LF_CG_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_MD_LF_CG_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkSnd(TIS)
   self.JR_MD_LF_CS_TIS_OBJ = GGC.TriggeringIconSignal:Create(self.JR_MD_LF_CS_TIS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- check(ST)
   self.JR_MD_LF_C_ST_OBJ = GGC.SynchroTimer:Create(self.JR_MD_LF_C_ST_TMP, {
      properties = {
         parameters = {
            callbackList = {
               ["Common"] = function()
                  local ce = GGF.OuterInvoke(self.JR_MD_LF_CE_TIS_OBJ, "IsBlinking")
                     and GGF.OuterInvoke(self.JR_MD_LF_CE_BA_OBJ, "Launch");
                  local ca = GGF.OuterInvoke(self.JR_MD_LF_CAS_TIS_OBJ, "IsBlinking")
                     and GGF.OuterInvoke(self.JR_MD_LF_CAS_BA_OBJ, "Launch");
                  local ca = GGF.OuterInvoke(self.JR_MD_LF_CAC_TIS_OBJ, "IsBlinking")
                     and GGF.OuterInvoke(self.JR_MD_LF_CAC_BA_OBJ, "Launch");
                  local cc = GGF.OuterInvoke(self.JR_MD_LF_CC_TIS_OBJ, "IsBlinking")
                     and GGF.OuterInvoke(self.JR_MD_LF_CC_BA_OBJ, "Launch");
                  local cf = GGF.OuterInvoke(self.JR_MD_LF_CF_TIS_OBJ, "IsBlinking")
                     and GGF.OuterInvoke(self.JR_MD_LF_CF_BA_OBJ, "Launch");
                  local ci = GGF.OuterInvoke(self.JR_MD_LF_CI_TIS_OBJ, "IsBlinking")
                     and GGF.OuterInvoke(self.JR_MD_LF_CI_BA_OBJ, "Launch");
                  local cg = GGF.OuterInvoke(self.JR_MD_LF_CG_TIS_OBJ, "IsBlinking")
                     and GGF.OuterInvoke(self.JR_MD_LF_CG_BA_OBJ, "Launch");
                  local cs = GGF.OuterInvoke(self.JR_MD_LF_CS_TIS_OBJ, "IsBlinking")
                     and GGF.OuterInvoke(self.JR_MD_LF_CS_BA_OBJ, "Launch");
               end,
            },
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkEngine(BA)
   self.JR_MD_LF_CE_BA_OBJ = GGC.BlinkAnimation:Create(self.JR_MD_LF_CE_BA_TMP, {
      properties = {
         base = {
            target = GGF.INS.GetObjectRef(self.JR_MD_LF_CE_TIS_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkAsb(BA)
   self.JR_MD_LF_CAS_BA_OBJ = GGC.BlinkAnimation:Create(self.JR_MD_LF_CAS_BA_TMP, {
      properties = {
         base = {
            target = GGF.INS.GetObjectRef(self.JR_MD_LF_CAS_TIS_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkAcr(BA)
   self.JR_MD_LF_CAC_BA_OBJ = GGC.BlinkAnimation:Create(self.JR_MD_LF_CAC_BA_TMP, {
      properties = {
         base = {
            target = GGF.INS.GetObjectRef(self.JR_MD_LF_CAC_TIS_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkCom(BA)
   self.JR_MD_LF_CC_BA_OBJ = GGC.BlinkAnimation:Create(self.JR_MD_LF_CC_BA_TMP, {
      properties = {
         base = {
            target = GGF.INS.GetObjectRef(self.JR_MD_LF_CC_TIS_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkFcs(BA)
   self.JR_MD_LF_CF_BA_OBJ = GGC.BlinkAnimation:Create(self.JR_MD_LF_CF_BA_TMP, {
      properties = {
         base = {
            target = GGF.INS.GetObjectRef(self.JR_MD_LF_CF_TIS_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkIoc(BA)
   self.JR_MD_LF_CI_BA_OBJ = GGC.BlinkAnimation:Create(self.JR_MD_LF_CI_BA_TMP, {
      properties = {
         base = {
            target = GGF.INS.GetObjectRef(self.JR_MD_LF_CI_TIS_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkGui(BA)
   self.JR_MD_LF_CG_BA_OBJ = GGC.BlinkAnimation:Create(self.JR_MD_LF_CG_BA_TMP, {
      properties = {
         base = {
            target = GGF.INS.GetObjectRef(self.JR_MD_LF_CG_TIS_OBJ),
         },
      },
   });


   -- maintenanceDashboard(LF)
      -- checkSnd(BA)
   self.JR_MD_LF_CS_BA_OBJ = GGC.BlinkAnimation:Create(self.JR_MD_LF_CS_BA_TMP, {
      properties = {
         base = {
            target = GGF.INS.GetObjectRef(self.JR_MD_LF_CS_TIS_OBJ),
         },
      },
   });
end

-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- maintenanceDashboard(LF)

	GGF.OuterInvoke(self.JR_MD_LF_OBJ, "Adjust");

	-- maintenanceDashboard(LF)
      -- checkEngine(TIS)
   GGF.OuterInvoke(self.JR_MD_LF_CE_TIS_OBJ, "Adjust");

   -- maintenanceDashboard(LF)
      -- checkTransmission(TIS)
   GGF.OuterInvoke(self.JR_MD_LF_CT_TIS_OBJ, "Adjust");

   -- maintenanceDashboard(LF)
      -- checkLoadProcess(TIS)
   GGF.OuterInvoke(self.JR_MD_LF_CLP_TIS_OBJ, "Adjust");

   -- maintenanceDashboard(LF)
      -- checkRam(TIS)
   GGF.OuterInvoke(self.JR_MD_LF_CR_TIS_OBJ, "Adjust");

   -- maintenanceDashboard(LF)
      -- checkLatency(TIS)
   GGF.OuterInvoke(self.JR_MD_LF_CL_TIS_OBJ, "Adjust");

   -- maintenanceDashboard(LF)
      -- checkFps(TIS)
   GGF.OuterInvoke(self.JR_MD_LF_CFP_TIS_OBJ, "Adjust");

   -- maintenanceDashboard(LF)
      -- checkAsb(TIS)
   GGF.OuterInvoke(self.JR_MD_LF_CAS_TIS_OBJ, "Adjust");

   -- maintenanceDashboard(LF)
      -- checkAcr(TIS)
   GGF.OuterInvoke(self.JR_MD_LF_CAC_TIS_OBJ, "Adjust");

   -- maintenanceDashboard(LF)
      -- checkCom(TIS)
   GGF.OuterInvoke(self.JR_MD_LF_CC_TIS_OBJ, "Adjust");

   -- maintenanceDashboard(LF)
      -- checkFcs(TIS)
   GGF.OuterInvoke(self.JR_MD_LF_CF_TIS_OBJ, "Adjust");

   -- maintenanceDashboard(LF)
      -- checkIoc(TIS)
   GGF.OuterInvoke(self.JR_MD_LF_CI_TIS_OBJ, "Adjust");

   -- maintenanceDashboard(LF)
      -- checkGui(TIS)
   GGF.OuterInvoke(self.JR_MD_LF_CG_TIS_OBJ, "Adjust");

   -- maintenanceDashboard(LF)
      -- checkSnd(TIS)
   GGF.OuterInvoke(self.JR_MD_LF_CS_TIS_OBJ, "Adjust");

   -- maintenanceDashboard(LF)
      -- check(ST)
   GGF.OuterInvoke(self.JR_MD_LF_C_ST_OBJ, "Adjust");

   -- maintenanceDashboard(LF)
      -- checkEngine(BA)
   GGF.OuterInvoke(self.JR_MD_LF_CE_BA_OBJ, "Adjust");

   -- maintenanceDashboard(LF)
      -- checkAsb(BA)
   GGF.OuterInvoke(self.JR_MD_LF_CAS_BA_OBJ, "Adjust");


   -- maintenanceDashboard(LF)
      -- checkAcr(BA)
   GGF.OuterInvoke(self.JR_MD_LF_CAC_BA_OBJ, "Adjust");


   -- maintenanceDashboard(LF)
      -- checkCom(BA)
   GGF.OuterInvoke(self.JR_MD_LF_CC_BA_OBJ, "Adjust");


   -- maintenanceDashboard(LF)
      -- checkFcs(BA)
   GGF.OuterInvoke(self.JR_MD_LF_CF_BA_OBJ, "Adjust");


   -- maintenanceDashboard(LF)
      -- checkIoc(BA)
   GGF.OuterInvoke(self.JR_MD_LF_CI_BA_OBJ, "Adjust");


   -- maintenanceDashboard(LF)
      -- checkGui(BA)
   GGF.OuterInvoke(self.JR_MD_LF_CG_BA_OBJ, "Adjust");


   -- maintenanceDashboard(LF)
      -- checkSnd(BA)
   GGF.OuterInvoke(self.JR_MD_LF_CS_BA_OBJ, "Adjust");
end

-- п.2.8 (After-load Settings)
function This:MPRC_ALS_INITIALIZE()
   This:setLayoutOrder();
   This:resetLayout();

   GGC.TriggerTimer:Create({
      time = 40,
      callback = function() This:SetENGState("nonCritical") end
   });

   GGC.TriggerTimer:Create({
      time = 47,
      callback = function() This:SetENGState("critical") end
   });

   GGC.TriggerTimer:Create({
      time = 49,
      callback = function() This:SetENGState("nonCritical") end
   });

   --GGC.TriggerTimer:Create({
      --time = 52,
      --callback = function() This:SetENGState("idle") end
   --});
end

-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------

-- п.3.0 (Addon System Timer)
function This:MPRC_AST_RUNTIME(in_elapsed)
end

-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------