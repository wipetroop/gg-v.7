local This = GGF.ModuleGetWrapper("GUI-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	local MSC_CONFIG = self:getConfig();
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------

	-- mainDashboard(LF)
	self.JR_MD_LF_OBJ = GGC.LayoutFrame:Create(self.JR_MD_LF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.MasterFrame:GetLeadingFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.MasterFrame:GetLeadingFrame(),
			},
		},
	});


	-- mainDashboard(LF)
		-- combatInstrumentation(SF)
	self.JR_MD_LF_CI_SF_OBJ = GGC.StandardFrame:Create(self.JR_MD_LF_CI_SF_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
			},
			size = {
				width = MSC_CONFIG.combatInstrumentationWidth,
				--height = 2*MSC_CONFIG.combatInstrumentationHeight,
            height = MSC_CONFIG.combatInstrumentationHeight,
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
				--offsetX = math.floor(GGM.FCS.RuntimeStorage:GetScreenWidth()),
				--offsetY = - math.floor(GGM.FCS.RuntimeStorage:GetScreenHeight()*3.6/5.2),
			},
		},
	});


	-- mainDashboard(LF)
		-- drivecontrolInstrumentation(SF)
	self.JR_MD_LF_DI_SF_OBJ = GGC.StandardFrame:Create(self.JR_MD_LF_DI_SF_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
			},
			size = {
				width = MSC_CONFIG.drivecontrolInstrumentationWidth(),
				height = MSC_CONFIG.drivecontrolInstrumentationHeight(),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
				--offsetX = function() return math.floor(GGM.FCS.RuntimeStorage:GetScreenWidth()/2.0) - MSC_CONFIG.centralPanelGap - GGF.OuterInvoke(self.JR_MD_SF_DI_SF_OBJ, "GetWidth") end,
				--offsetY = function()
					--local mbbl_hg = MultiBarBottomLeft:GetHeight();
					--return - GGM.FCS.RuntimeStorage:GetScreenHeight() + GGM.GUI.MaintenanceInstrumentation:GetDashboardPanelHeight() + 2*mbbl_hg + 2*MSC_CONFIG.skillBarGap;
				--end,
			},
		},
	});


	-- mainDashboard(LF)
		-- maintenanceInstrumentation(SF)
	self.JR_MD_LF_MI_SF_OBJ = GGC.StandardFrame:Create(self.JR_MD_LF_MI_SF_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
			},
			size = {
				width = MSC_CONFIG.maintenanceInstrumentationWidth(),
				height = MSC_CONFIG.maintenanceInstrumentationHeight(),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
				--offsetX = math.floor(GGM.FCS.RuntimeStorage:GetScreenWidth()/2.0) + MSC_CONFIG.centralPanelGap,
				--offsetY = function()
					--local mbbl_hg = MultiBarBottomLeft:GetHeight();
					--return - GGM.FCS.RuntimeStorage:GetScreenHeight() + GGF.OuterInvoke(self.JR_MD_SF_MI_SF_OBJ, "GetHeight") + 2*mbbl_hg + 2*MSC_CONFIG.skillBarGap;
				--end,
			},
		},
	});


	-- mainDashboard(LF)
		-- systemInstrumentation(SF)
	self.JR_MD_LF_SI_SF_OBJ = GGC.StandardFrame:Create(self.JR_MD_LF_SI_SF_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
			},
			size = {
				width = MSC_CONFIG.systemInstrumentationWidth,
				--height = 2*MSC_CONFIG.systemInstrumentationHeight,
				height = MSC_CONFIG.systemInstrumentationHeight,
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
				--offsetX = 0,
				--offsetY = - math.floor(GGM.FCS.RuntimeStorage:GetScreenHeight()*3.6/5.2),
			},
		},
	});


	-- mainDashboard(LF)
		-- leftTransmitter(AT)
	self.JR_MD_LF_LT_AT_OBJ = GGC.AngularTexture:Create(self.JR_MD_LF_LT_AT_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ),
			},
			anchor = {
				relativeTo = GGM.GUI.MasterFrame:GetLeadingFrame(),
			},
		},
	});
end



-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- mainDashboard(LF)
	GGF.OuterInvoke(self.JR_MD_LF_OBJ, "Adjust");

	-- mainDashboard(LF)
		-- combatInstrumentation(SF)
	GGF.OuterInvoke(self.JR_MD_LF_CI_SF_OBJ, "Adjust");

	-- mainDashboard(LF)
		-- drivecontrolInstrumentation(SF)
	GGF.OuterInvoke(self.JR_MD_LF_DI_SF_OBJ, "Adjust");

	-- mainDashboard(LF)
		-- maintenanceInstrumentation(SF)
	GGF.OuterInvoke(self.JR_MD_LF_MI_SF_OBJ, "Adjust");

	-- mainDashboard(LF)
		-- systemInstrumentation(SF)
	GGF.OuterInvoke(self.JR_MD_LF_SI_SF_OBJ, "Adjust");

	-- mainDashboard(LF)
		-- leftTransmitter(AT)
	GGF.OuterInvoke(self.JR_MD_LF_LT_AT_OBJ, "Adjust");
end


-- п.2.8 (After-load Settings)
function This:MPRC_ALS_INITIALIZE()
	GGF.OuterInvoke(self.JR_MD_LF_LT_AT_OBJ, "SetTexCoord", 1.0, 1.0, 0.75, 0.75);
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-- п.3.0 (Addon System Timer)
function This:MPRC_AST_RUNTIME(in_elapsed)
end


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------