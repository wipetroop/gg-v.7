local This = GGF.ModuleGetWrapper("GUI-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Управление видимостью панели
function This:SetDashboardPanelHidden(in_vis)
	self:setDashboardPanelHidden(in_vis);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие основной панели
function This:GetDashboardPanelFrame()
	return self:getDashboardPanelFrame();
end


-- Взятие боевой панели
function This:GetCombatInstrumentationFrame()
	return self:getCombatInstrumentationFrame();
end


-- Взятие панели управления движением
function This:GetDrivecontrolInstrumentationFrame()
	return self:getDrivecontrolInstrumentationFrame();
end


-- Взятие сервисной панели
function This:GetMaintenanceInstrumentationFrame()
	return self:getMaintenanceInstrumentationFrame();
end


-- Взятие системной панели
function This:GetSystemInstrumentationFrame()
	return self:getSystemInstrumentationFrame();
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------