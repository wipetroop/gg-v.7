local This = GGF.ModuleGetWrapper("GUI-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- TODO: определиться, нужны ли чеки по кластерам
-- Hierarchy
This.oc_mainDashboardGeometry = {
	layoutFrame = GGF.GTemplateTree(GGC.LayoutFrame, {
		mainDashboard = {	-- [MD_LF]
			inherit = {
				standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
					combatInstrumentation = {		-- [CI_SF]
					},
					drivecontrolInstrumentation = {	-- [DI_SF]
					},
					maintenanceInstrumentation = {	-- [MI_SF]
					},
					systemInstrumentation = {		-- [SI_SF]
					},
				}),
				angularTexture = GGF.GTemplateTree(GGC.AngularTexture, {
					leftTransmitter = {				-- [LT_AT]
					},
					--rightTransmitter = {			-- [RT_AT]
					--},
				}),
			},
		},
	}),
};

local MDG_LF = This.oc_mainDashboardGeometry.layoutFrame;
local MDG_LF_MD_SF = MDG_LF.mainDashboard.inherit.standardFrame;
local MDG_LF_MD_AT = MDG_LF.mainDashboard.inherit.angularTexture;

-- mainDashboard(LF)
MDG_LF.mainDashboard.object = {};
MDG_LF.mainDashboard.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "MD",
		},
		miscellaneous = {
			hidden = false,
			--movable = false,
			--enableMouse = false,
			--enableKeyboard = false,
		},
		size = {
         width = function() return GGM.FCS.RuntimeStorage:GetScreenWidth() end,
         height = function() return GGM.FCS.RuntimeStorage:GetScreenHeight() end,
			--width = MSC_CONFIG.dashboardWidth,
			--height = MSC_CONFIG.dashboardHeight,
		},
		--backdrop = GGD.Backdrop.Empty,
		anchor = {
			point = GGE.PointType.TopLeft,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.TopLeft,
			offsetX = 0,
			offsetY = 0,
		},
	},
};


-- mainDashboard(SF)
	-- combatInstrumentation(SF)
MDG_LF_MD_SF.combatInstrumentation.object = {};
MDG_LF_MD_SF.combatInstrumentation.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "CI",
		},
		miscellaneous = {
			hidden = false,
			movable = false,
			enableMouse = false,
			enableKeyboard = false,
		},
		size = {
			width = MSC_CONFIG.dashlightSide,
			height = MSC_CONFIG.dashlightSide,
		},
		backdrop = GGD.Backdrop.Borderless,
		anchor = {
			--point = GGE.PointType.TopRight,
         point = GGE.PointType.BottomLeft,
			relativeTo = nil,	-- fwd
			--relativePoint = GGE.PointType.TopLeft,
         relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = 0,
		},
	},
};


-- mainDashboard(LF)
	-- drivecontrolInstrumentation(SF)
MDG_LF_MD_SF.drivecontrolInstrumentation.object = {};
MDG_LF_MD_SF.drivecontrolInstrumentation.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "DI",
		},
		miscellaneous = {
			hidden = false,
			movable = false,
			enableMouse = false,
			enableKeyboard = false,
		},
		size = {
			width = MSC_CONFIG.dashlightSide,
			height = MSC_CONFIG.dashlightSide,
		},
		backdrop = GGD.Backdrop.Borderless,
		anchor = {
			--point = GGE.PointType.TopLeft,
         point = GGE.PointType.BottomLeft,
			relativeTo = nil,	-- fwd
			--relativePoint = GGE.PointType.TopLeft,
         relativePoint = GGE.PointType.BottomLeft,
			offsetX = 0,
			offsetY = 0,
		},
	},
};


-- maintDashboard(LF)
	-- maintenanceInstrumentation(SF)
MDG_LF_MD_SF.maintenanceInstrumentation.object = {};
MDG_LF_MD_SF.maintenanceInstrumentation.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "MI",
		},
		miscellaneous = {
			hidden = false,
			movable = false,
			enableMouse = false,
			enableKeyboard = false,
		},
		size = {
			width = MSC_CONFIG.dashlightSide,
			height = MSC_CONFIG.dashlightSide,
		},
		backdrop = GGD.Backdrop.Borderless,
		anchor = {
			--point = GGE.PointType.TopLeft,
         point = GGE.PointType.BottomRight,
			relativeTo = nil,	-- fwd
			--relativePoint = GGE.PointType.TopLeft,
         relativePoint = GGE.PointType.BottomRight,
			offsetX = 0,
			offsetY = 0,
		},
	},
};


-- maintDashboard(LF)
	-- systemInstrumentation(SF)
MDG_LF_MD_SF.systemInstrumentation.object = {};
MDG_LF_MD_SF.systemInstrumentation.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "SI",
		},
		miscellaneous = {
			hidden = false,
			movable = false,
			enableMouse = false,
			enableKeyboard = false,
		},
		size = {
			width = MSC_CONFIG.dashlightSide,
			height = MSC_CONFIG.dashlightSide,
		},
		backdrop = GGD.Backdrop.Borderless,
		anchor = {
			--point = GGE.PointType.TopLeft,
         point = GGE.PointType.BottomRight,
			relativeTo = nil,	-- fwd
			--relativePoint = GGE.PointType.TopLeft,
         relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = 0,
		},
	},
};


-- maintDashboard(LF)
	-- leftTransmitter(AT)
MDG_LF_MD_AT.leftTransmitter.object = {};
MDG_LF_MD_AT.leftTransmitter.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "LT",
		},
		miscellaneous = {
			hidden = true,
			file = "Interface\\DialogFrame\\Ui-DialogBox-Background",
		},
		size = {
			width = MSC_CONFIG.dashlightSide,
			height = MSC_CONFIG.dashlightSide,
		},
		anchor = {
			point = GGE.PointType.Center,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Center,
			offsetX = 0,
			offsetY = 0,
		},
	},
};