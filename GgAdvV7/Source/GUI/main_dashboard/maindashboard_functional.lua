local This = GGF.ModuleGetWrapper("GUI-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- api
-- Управление видимостью панели
function This:setDashboardPanelHidden(in_hdn)
	GGF.OuterInvoke(self.JR_MD_LF_OBJ, "SetHidden", in_hdn);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Взятие основной панели
function This:getDashboardPanelFrame()
	return GGF.INS.GetObjectRef(self.JR_MD_LF_OBJ);
end


-- api
-- Взятие боевой панели
function This:getCombatInstrumentationFrame()
	return GGF.INS.GetObjectRef(self.JR_MD_LF_CI_SF_OBJ);
end


-- api
-- Взятие панели управления движением
function This:getDrivecontrolInstrumentationFrame()
	return GGF.INS.GetObjectRef(self.JR_MD_LF_DI_SF_OBJ);
end


-- api
-- Взятие сервисной панели
function This:getMaintenanceInstrumentationFrame()
	return GGF.INS.GetObjectRef(self.JR_MD_LF_MI_SF_OBJ);
end


-- api
-- Взятие системной панели
function This:getSystemInstrumentationFrame()
	return GGF.INS.GetObjectRef(self.JR_MD_LF_SI_SF_OBJ);
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------