local This = GGF.ModuleGetWrapper("GUI-5");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------
	-- WARNING: временное решение

	-- mainWindow(SF)
	self.JR_MW_SF_OBJ = GGC.StandardFrame:Create(self.JR_MW_SF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.MasterFrame:GetLeadingFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.MasterFrame:GetLeadingFrame(),
				offsetX = GGM.FCS.RuntimeStorage:GetScreenWidth()/2.0,
				offsetY = - GGM.FCS.RuntimeStorage:GetScreenHeight()/4.0,
			},
		},
	});
	--print("mainwn mobji:", This:getConfig().frameWidth(), This:getConfig().frameHeight());

	-- mainWindow(SF)
		-- informationPanel(TS)
	self.JR_MW_SF_IP_TS_OBJ = GGC.TabSwitch:Create(self.JR_MW_SF_IP_TS_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_MW_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_MW_SF_OBJ),
			},
		},
	});


	-- mainWindow(SF)
		-- controlPanel(TS)
	self.JR_MW_SF_CP_TS_OBJ = GGC.TabSwitch:Create(self.JR_MW_SF_CP_TS_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_MW_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_MW_SF_IP_TS_OBJ),
			},
		},
	});


	-- mainWindow(SF)
		-- alertDispatcher(TS)
	self.JR_MW_SF_AD_TS_OBJ = GGC.TabSwitch:Create(self.JR_MW_SF_AD_TS_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_MW_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_MW_SF_CP_TS_OBJ),
			},
		},
	});


	-- mainWindow(SF)
		-- dataServicePanel(TS)
	self.JR_MW_SF_DSP_TS_OBJ = GGC.TabSwitch:Create(self.JR_MW_SF_DSP_TS_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_MW_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_MW_SF_AD_TS_OBJ),
			},
		},
	});
end


-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- mainWindow(SF)
	GGF.OuterInvoke(self.JR_MW_SF_OBJ, "Adjust");
	print("mainwnd adjust:", GGF.OuterInvoke(self.JR_MW_SF_OBJ, "GetRelativeTo"), GGF.INS.GetObjectRef(self.JR_MW_SF_OBJ):GetPoint());

	-- mainWindow(SF)
		-- informationPanel(TS)
	GGF.OuterInvoke(self.JR_MW_SF_IP_TS_OBJ, "Adjust");


	-- mainWindow(SF)
		-- controlPanel(TS)
	GGF.OuterInvoke(self.JR_MW_SF_CP_TS_OBJ, "Adjust");


	-- mainWindow(SF)
		-- alertDispatcher(TS)
	GGF.OuterInvoke(self.JR_MW_SF_AD_TS_OBJ, "Adjust");


	-- mainWindow(SF)
		-- dataService(TS)
	GGF.OuterInvoke(self.JR_MW_SF_DSP_TS_OBJ, "Adjust");
end


-- п.2.8 (After-load Settings)
function This:MPRC_ALS_INITIALIZE()
	-- NOTE: это пока так останется(уевые заморочки)
	PanelTemplates_SetNumTabs(GGF.INS.GetObjectRef(self.JR_MW_SF_OBJ), 4);	-- 4 total

	-- HACK: гоняем кнопки туда-сюда, чтоб шаблон схомякал ширину текста
	local hdn = GGF.OuterInvoke(self.JR_MW_SF_IP_TS_OBJ, "GetHidden");
	GGF.OuterInvoke(self.JR_MW_SF_IP_TS_OBJ, "SetHidden", not hdn);
	GGF.OuterInvoke(self.JR_MW_SF_IP_TS_OBJ, "SetHidden", hdn);
	GGF.OuterInvoke(self.JR_MW_SF_CP_TS_OBJ, "SetHidden", not hdn);
	GGF.OuterInvoke(self.JR_MW_SF_CP_TS_OBJ, "SetHidden", hdn);
	GGF.OuterInvoke(self.JR_MW_SF_AD_TS_OBJ, "SetHidden", not hdn);
	GGF.OuterInvoke(self.JR_MW_SF_AD_TS_OBJ, "SetHidden", hdn);
	GGF.OuterInvoke(self.JR_MW_SF_DSP_TS_OBJ, "SetHidden", not hdn);
	GGF.OuterInvoke(self.JR_MW_SF_DSP_TS_OBJ, "SetHidden", hdn);

	-- TODO: убрать...наверное
	self:adjustChildFrameSize();
	-- NOTE: Пока что будет тут
	self:managePanels(3);
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------