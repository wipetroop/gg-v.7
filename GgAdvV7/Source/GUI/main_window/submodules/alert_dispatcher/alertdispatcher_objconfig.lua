local This = GGF.ModuleGetWrapper("GUI-5-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_alertDispatcherGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		alertDispatcher = {			-- [AD_SF]
			inherit = {
            tabSwitch = GGF.GTemplateTree(GGC.TabSwitch, {
               tableVision = {},       -- [TV_TS]
               graphicVision = {},     -- [GV_TS]
            }),
         },
		},
	}),
};

local ADG_SF = This.oc_alertDispatcherGeometry.standardFrame;
local ADG_SF_DS_TS = ADG_SF.alertDispatcher.inherit.tabSwitch;

-- alertDispatcher(SF)
ADG_SF.alertDispatcher.object = {};
ADG_SF.alertDispatcher.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "AD",
		},
		miscellaneous = {
			hidden = true,
		},
		size = {
			width = function() return MSC_CONFIG.frameWidth() end,
			height = function() return MSC_CONFIG.frameHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Segregate,
	},
};

-- dataService(SF)
   -- tableVision(TS)
ADG_SF_DS_TS.tableVision.object = {};
ADG_SF_DS_TS.tableVision.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         template = "OptionsFrameTabButtonTemplate",
         wrapperName = "TV",
      },
      miscellaneous = {
         gxname = "Tab1",
         text = "Table",
         --tooltipText = "GUi_MW_F_DS_F_TV_TS",
      },
      size = {
         width = function() return MSC_CONFIG.buttonWidth() end,
         height = function() return MSC_CONFIG.buttonHeight() end,
      },
      anchor = {
         point = GGE.PointType.BottomLeft,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.BottomLeft,
         offsetX = function() return MSC_CONFIG.tabSwitchOffsetX() end,
         offsetY = function() return MSC_CONFIG.tabSwitchOffsetY() end,
      },
      callback = {
         onClick = function(...) This:on_tableVisionTS_clicked(...) end,
      },
   },
};


-- dataService(SF)
   -- graphicVision(TS)
ADG_SF_DS_TS.graphicVision.object = {};
ADG_SF_DS_TS.graphicVision.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         template = "OptionsFrameTabButtonTemplate",
         wrapperName = "GV",
      },
      miscellaneous = {
         gxname = "Tab2",
         text = "Graphic",
         --tooltipText = "GUi_MW_F_DS_F_GV_TS",
      },
      size = {
         width = function() return MSC_CONFIG.buttonWidth() end,
         height = function() return MSC_CONFIG.buttonHeight() end,
      },
      anchor = {
         point = GGE.PointType.Left,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Right,
         offsetX = function() return MSC_CONFIG.tabSwitchHorGap() end,
         offsetY = 0,
      },
      callback = {
         onClick = function(...) This:on_graphicVisionTS_clicked(...) end,
      },
   },
};