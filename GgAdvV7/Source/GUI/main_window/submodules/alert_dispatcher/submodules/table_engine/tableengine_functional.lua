local This = GGF.ModuleGetWrapper("GUI-5-1-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- api
-- Смена видимости основного фрейма
function This:setHidden(in_vis)
	GGF.OuterInvoke(self.JR_TV_SF_OBJ, "SetHidden", in_vis);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Включение/выключение таймингового дампа
function This:adjustTumbler(in_tumbler)
	self.vs_tableRefreshTimer.stopped = not in_tumbler;
	if (in_tumbler) then
		self:dataDump();
	end
end


-- api
-- Установка линейных размеров основного фрейма
function This:adjustSize(in_width, in_height)
	local MSC_CONFIG = GGM.GUI.DSTableEngine:getConfig();
	local tableWidth = in_width - 2*math.abs(MSC_CONFIG.tableOffsetX);
	local tableHeight = in_height - math.abs(MSC_CONFIG.tableOffsetY) - math.abs(MSC_CONFIG.tableOffsetX);

	GGF.OuterInvoke(self.JR_TV_SF_OBJ, "SetWidth", in_width);
	GGF.OuterInvoke(self.JR_TV_SF_OBJ, "SetHeight", in_height);

	GGF.OuterInvoke(self.JR_TV_SF_AT_F_OBJ, "SetWidth", tableWidth);
	GGF.OuterInvoke(self.JR_TV_SF_AT_F_OBJ, "SetHeight", tableHeight);

	GGF.OuterInvoke(self.JR_TV_SF_AT_F_OBJ, "SetHidden", false);
end


-- Заполнение логфилда с частью основных пересчетов
-- TODO: и где?(GG-17)
function This:dataDump()
	local errTree = GGM.COM.ModuleInterrogator:GetErrorTree();

	GGF.OuterInvoke(self.JR_TV_F_AT_F_OBJ, "SetHidden", false);

	local tableData = {};

	for name, moduleList in pairs(errTree.cluster) do
		self:marshallErrTree(tableData, name, moduleList);
	end

	GGF.OuterInvoke(self.JR_TV_F_AT_F_OBJ, "SetData", tableData);
end


-- Рекурсивное преобразование дерева в список
function This:marshallErrTree(in_tableData, in_name, in_errTree)
	-- NOTE: если так, то это модуль
	if (in_name:find("_") and not in_name:find("module")) then
		print("module: ", in_name);
		self:pushbackErrListShard(in_tableData, in_name, _G[in_name]:GetErrorList());
	end
	for name, moduleList in pairs(in_errTree) do
		if (name ~= "data") then
			print("next: ", in_name);
			local newName = in_name;
			if (name ~= "module") then
				newName = newName .. "_" .. name;
			end
			self:marshallErrTree(in_tableData, newName, moduleList);
		end
	end
end


-- Запись в поля для таблицы напрямую из списка ошибок модуля
function This:pushbackErrListShard(in_tableData, in_name, in_shard)
	local checkList = in_shard.errList;
	local datatransport = {};
	for code, errsgn in pairs(checkList) do
		--local button = CreateFrame("Button", "only_for_testing"..tostring(code), self.JR_TV_F_AT_F_OBJ:GetObject().frame);
		--button:SetPoint("CENTER", self.JR_TV_F_AT_F_OBJ:GetObject().frame, "CENTER", 0, 0)
		--button:SetWidth(200)
		--button:SetHeight(50)

		--button:SetText("My Test")
		--button:SetNormalFontObject("GameFontNormalSmall")

		--button:SetNormalTexture("Interface/Buttons/Ui-Panel-Button-Up")
		--button:SetHighlightTexture("Interface/Buttons/Ui-Panel-Button-Highlight")
		--button:SetPushedTexture("Interface/Buttons/Ui-Panel-Button-Down")

		GGM.GUI.CommonService:SetTableElementData(datatransport, 1, #in_tableData + 1, GGD.Color.Neutral);
		GGM.GUI.CommonService:SetTableElementData(datatransport, 2, in_name, GGD.Color.Neutral);
		GGM.GUI.CommonService:SetTableElementData(datatransport, 3, errsgn.codeToken, GGD.Color.Neutral);
		GGM.GUI.CommonService:SetTableElementData(datatransport, 4, errsgn.identifier, GGD.Color.Neutral);
		GGM.GUI.CommonService:SetTableElementData(datatransport, 5, button, GGD.Color.Neutral);

		in_tableData[#in_tableData + 1] = {
			cols = datatransport,
		};
	end
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------