local This = GGF.ModuleCreateWrapper("GUI-5-1-1", false, {
	frameWidth = function() return GGM.GUI.ParameterStorage:GetMWFrameWidth() end,
	frameHeight = function() return GGM.GUI.ParameterStorage:GetADTEFrameHeight() end,

	tableFontHeight = 14,
	tableRowCount = 39, -- а че не 40?

	tablePoint = GGE.PointType.TopLeft,
	tableRelativePoint = GGE.PointType.TopLeft,

	tableOffsetX = 2,
	tableOffsetY = -20,

	onCellEnter = function(...) GGM.GUI.CommonService:OnCommonTableCellEnter(...) end,
	onCellLeave = function(...) GGM.GUI.CommonService:OnCommonTableCellLeave(...) end,
});

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------


This.vs_hInstance = {
	gui = nil,
	--con = nil,
	tbl = nil
};

This.vs_tableRefreshTimer = {
	tick = 0,
	interval = 10,
	stopped = true,
};

This.vs_testDataSet = {
	{{
		text = "v11",
		textColor = GGD.Color.Green,
		tooltip = "v11t",
		tooltipColor = GGD.Color.Red
	}, {
		text = "v12",
		textColor = GGD.Color.Blue,
		tooltip = "v12t",
		tooltipColor = GGD.Color.Yellow
	}},
	{{
		text = "v21",
		textColor = GGD.Color.Green,
		tooltip = "v21t",
		tooltipColor = GGD.Color.Red
	}, {
		text = "v22",
		textColor = GGD.Color.Blue,
		tooltip = "v22t",
		tooltipColor = GGD.Color.Yellow
	}}
};


-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------