local This = GGF.ModuleGetWrapper("GUI-5-1-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.3 (MDT Check)
function This:MS_CHECK_MDT_0()
	return 1;
end


-------------------------------------------------------------------------------
-- "multitudinous data" block
-------------------------------------------------------------------------------


This.md_prototypeMap = {
	alertTable = {
		{ ["name"] = "No",         ["width"] = 35,  ["align"] = "CENTER" }, -- [1]
		{ ["name"] = "Module",     ["width"] = 155, ["align"] = "CENTER" }, -- [2]
		{ ["name"] = "Code",       ["width"] = 105, ["align"] = "CENTER" }, -- [2]
		{ ["name"] = "Identifier", ["width"] = 235, ["align"] = "LEFT"   }, -- [3]
		{ ["name"] = "Button",     ["width"] = 55,  ["align"] = "CENTER" }, -- [2]
		
		--lbtext = {
			--{	
				--data = "No", 
				--color = "#standart#",
			--},
			--{
				--data = "Code", 
				--color = "#standart#",
			--},
			--{
				--data = "Alert", 
				--color = "#standart#",
			--},
		--},
		--meta = {
			--columns = 3,
			--length = {100, 100, 100},
		--}
	},
};