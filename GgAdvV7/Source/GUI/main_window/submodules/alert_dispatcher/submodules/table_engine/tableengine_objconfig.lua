local This = GGF.ModuleGetWrapper("GUI-5-1-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_tableEngineGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		tableVision = {					-- [TV_SF]
			inherit = {
				chargedTable = GGF.GTemplateTree(GGC.ChargedTable, {
					errorList = {},		-- [EL_CT]
				}),
			},
		},
	}),
};

local TEG_SF = This.oc_tableEngineGeometry.standardFrame;
local TEG_SF_TV_CT = TEG_SF.tableVision.inherit.chargedTable;

-- tableVision(SF)
TEG_SF.tableVision.object = {};
TEG_SF.tableVision.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "TV",
		},
		--miscellaneous = {
			--hidden = false,
		--},
		size = {
			width = function() return MSC_CONFIG.frameWidth() end,
			height = function() return MSC_CONFIG.frameHeight() end,
		},
		anchor = {
			point = GGE.PointType.Bottom,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Segregate,
	},
};


-- tableVision(SF)
	-- errorList(CT)
TEG_SF_TV_CT.errorList.object = {};
TEG_SF_TV_CT.errorList.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "EL",
		},
		hardware = {
			--fontHeight = function() return MSC_CONFIG.tableFontHeight() end,
			--rowCount = MSC_CONFIG.tableRowCount,
		},
		data = {},
		anchor = {
			point = GGE.PointType.TopLeft,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.TopLeft,
			offsetX = MSC_CONFIG.tableOffsetX,
			offsetY = MSC_CONFIG.tableOffsetY,
		},
		backdrop = GGD.Backdrop.Segregate,
		callback = {
			onCellClick = function(...) end,
			onCellEnter = function(...) end,
			onCellLeave = function(...) end,
		},
	},
};