local This = GGF.ModuleGetWrapper("GUI-5-1-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.1 (Miscellaneous Variables)
function This:MPRC_MVR_INITIALIZE()
end


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------

	-- tableVision(SF)

	self.JR_TV_SF_OBJ = GGC.StandardFrame:Create(self.JR_TV_SF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.AlertDispatcher:GetLeadingFrame(),
			},
			--size = {
				--width = GGM.GUI.AlertDispatcher:GetWidth(),
				--height = GGM.GUI.AlertDispatcher:GetHeight(),
			--},
			anchor = {
				relativeTo = GGM.GUI.AlertDispatcher:GetLeadingFrame(),
			},
		},
	});


	-- tableVision(SF)
		-- errorList(CT)

	--self.JR_TV_SF_EL_CT_OBJ = GGC.ChargedTable:Create(self.JR_TV_SF_EL_CT_TMP, {
		--properties = {
			--base = {
				--parent = GGF.INS.GetObjectRef(self.JR_TV_SF_OBJ),
			--},
			--anchor = {
				--relativeTo = GGF.INS.GetObjectRef(self.JR_TV_SF_OBJ),
			--},
		--},
	--});


	-- TODO: убрать или вынести в юнит тест(GG-3)

	--local data = {
		--{cols = {1, "100", "Check engine"}},
		--{cols = {2, "101", "Check transmission"}}
	--};

	-- TODO: понять, откуда ноги растут(GG-3)
	--self.JR_TV_F_AT_F_OBJ:SetData(data);
end


-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- tableVision(SF)

	GGF.OuterInvoke(self.JR_TV_SF_OBJ, "Adjust");


	-- tableVision(SF)
		-- errorList(CT)

	--GGF.OuterInvoke(self.JR_TV_SF_EL_CT_OBJ, "Adjust");
end


-- п.2.8 (After-load Settings)
function This:MPRC_ALS_INITIALIZE()
	--self.JR_TV_SF_EL_CT_OBJ:SetData(self.vs_testDataSet);
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-- п.3.0 (Addon System Timer)
function This:MPRC_AST_RUNTIME(in_elapsed)
	-- TODO: разобраться. откуда ноги растут
	--GGF.Timer(self.vs_tableRefreshTimer, in_elapsed, function()
		--self:dataDump();
	--end);
end


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------