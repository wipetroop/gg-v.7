local This = GGF.ModuleGetWrapper("GUI-5-1-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_graphicEngineGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		graphicVision = {		-- [GV_SF]
			inherit = {
				graphicFrame = GGF.GTemplateTree(GGC.GraphicFrame, {
					common = {},		-- [C_GF]
				}),
			},
		},
	}),
};


local GEG_SF = This.oc_graphicEngineGeometry.standardFrame;
local GEG_SF_GV_GF = GEG_SF.graphicVision.inherit.graphicFrame;

-- graphicVision(SF)
GEG_SF.graphicVision.object = {};
GEG_SF.graphicVision.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "GV",
		},
		size = {
			width = function() return MSC_CONFIG.frameWidth() end,
			height = function() return MSC_CONFIG.frameHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Segregate,
	},
};


-- graphicVision(SF)
	-- common(GF)
GEG_SF_GV_GF.common.object = {};
GEG_SF_GV_GF.common.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "GV",
		},
		anchor = {
			point = MSC_CONFIG.graphicPoint,
			relativeTo = nil,	-- fwd
			relativePoint = MSC_CONFIG.graphicRelativePoint,
			offsetX = MSC_CONFIG.graphicOffsetX,
			offsetY = MSC_CONFIG.graphicOffsetY,
		},
		geometry = {
			gridColor = {
				R = 0.5,
				G = 0.5,
				B = 0.5,
				A = 0.5,
			},
			axisDrawing = {
				X = true,
				Y = true,
			},
			axisColor = {
				R = 1.0,
				G = 1.0,
				B = 1.0,
				A = 1.0,
			},
			yLabels = {
				left = true,
				right = true,
			},
			gridSpacing = {
				X = 0.25,
				Y = 0.25,
			},
			xAxisInterval = {
				start = 0,
				finish = 0,
			},
			yAxisInterval = {
				start = 0,
				finish = 0,
			},
		},
	},
};