local This = GGF.ModuleGetWrapper("GUI-5-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- api
-- Смена видиомсти основного фрейма
function This:setHidden(in_vis)
	GGF.OuterInvoke(self.JR_AD_SF_OBJ, "SetHidden", in_vis);
	--GGM.GUI.ADTableEngine:AdjustTumbler(not in_vis);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Взятие ширины основного фрейма
function This:getWidth()
	print("ad gw", self.JR_AD_SF_OBJ, GGF.INS.GetObjectSuffix(self.JR_AD_SF_OBJ));
	return GGF.OuterInvoke(self.JR_AD_SF_OBJ, "GetWidth");
end


-- api
-- Взятие высоты основного фрейма
function This:getHeight()
	return GGF.OuterInvoke(self.JR_AD_SF_OBJ, "GetHeight");
end


-- api
-- Взятие объекта главного окна
function This:getLeadingFrame()
	return GGF.INS.GetObjectRef(self.JR_AD_SF_OBJ);
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Установка линейных размеров основного фрейма
function This:adjustSize(in_width, in_height)
	GGF.OuterInvoke(self.JR_AD_SF_OBJ, "SetWidth", in_width);
	GGF.OuterInvoke(self.JR_AD_SF_OBJ, "SetHeight", in_height);

	--local width, height = self.JR_AD_F_OBJ:GetWidth(), self.JR_AD_F_OBJ:GetHeight();
	--local offsetNT, offsetT = GGM.GUI.MainWindow:GetChildTabbedFrameOffsetX(), GGM.GUI.MainWindow:GetChildTabbedFrameOffsetY();

	--GGM.GUI.ADTableEngine:AdjustSize(width - 2*offsetNT, height - offsetNT - math.abs(offsetT));
	--GGM.GUI.ADTableEngine:SetHidden(false);
end


-- Компактный переключатель панелей
function This:managePanels(in_panelNum)
   ATLASSERT(in_panelNum > 0 and in_panelNum < 3);
   local pn1, pn2 = (in_panelNum == 1), (in_panelNum == 2);

   self.vn_currentTabNum = in_panelNum;
   GGF.OuterInvoke(self.JR_AD_SF_TV_TS_OBJ, "SetEnabled", pn1);
   GGF.OuterInvoke(self.JR_AD_SF_GV_TS_OBJ, "SetEnabled", pn2);

   GGF.OuterInvoke(self.JR_AD_SF_TV_TS_OBJ, "SetHighlighted", pn1);
   GGF.OuterInvoke(self.JR_AD_SF_GV_TS_OBJ, "SetHighlighted", pn2);

   PanelTemplates_SetTab(GGF.OuterInvoke(self.JR_AD_SF_OBJ, "GetStandardFrame"), in_panelNum);

   GGM.GUI.ADTableEngine:SetHidden(not pn1);
   GGM.GUI.ADGraphicEngine:SetHidden(not pn2);

   self:adjustRelativeFrames();
   self:checkDataviewPossibility();
end

-- Применение значений некоторых зависимых фреймов
function This:adjustRelativeFrames()
   --self.JR_DS_SF_DS_SF_OBJ:Adjust();
   --self.JR_DS_SF_DS_SF_A_F_OBJ:Adjust();
   --self.JR_DS_SF_DS_SF_AP_F_OBJ:Adjust();
   --self.JR_DS_SF_DS_SF_V_S_OBJ:Adjust();
end

-- Проверка возможности отображения данных в таблицу и график
function This:checkDataviewPossibility()
   --if (self:checkGGM.GUI.DataFiltering()
      --and (self.vs_menuSelector.logClusterTitle ~= "")) then
      --GGM.GUI.DSTableEngine:DataDump(self.vs_menuSelector);
      --self:changeGSButtonState(true);
   --else
      --GGM.GUI.DSTableEngine:DataDrop();
   --end
end

-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------

-- Обработчик свича в таблицу
function This:on_tableVisionTS_clicked(in_meta)
   self:managePanels(1);
end

-- Обработчик свича в график
function This:on_graphicVisionTS_clicked(in_meta)
   self:managePanels(2);
end

-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------