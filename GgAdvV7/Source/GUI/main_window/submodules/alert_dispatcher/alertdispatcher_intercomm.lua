local This = GGF.ModuleGetWrapper("GUI-5-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------

	-- alertDispatcher(SF)
	self.JR_AD_SF_OBJ = GGC.StandardFrame:Create(self.JR_AD_SF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.MainWindow:GetLeadingFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.MainWindow:GetLeadingFrame(),
			},
		},
	});

   -- alertDispatcher(SF)
      -- tableVision(TS)
   self.JR_AD_SF_TV_TS_OBJ = GGC.TabSwitch:Create(self.JR_AD_SF_TV_TS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_AD_SF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_AD_SF_OBJ),
         },
      },
   });

   -- alertDispatcher(SF)
      -- graphicVision(TS)
   self.JR_AD_SF_GV_TS_OBJ = GGC.TabSwitch:Create(self.JR_AD_SF_GV_TS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_AD_SF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_AD_SF_TV_TS_OBJ),
         },
      },
   });
end


-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- alertDispatcher(SF)
	GGF.OuterInvoke(self.JR_AD_SF_OBJ, "Adjust");

   -- dataService(SF)
      -- tableVision(TS)
   GGF.OuterInvoke(self.JR_AD_SF_TV_TS_OBJ, "Adjust");

   -- dataService(SF)
      -- graphicVision(TS)
   GGF.OuterInvoke(self.JR_AD_SF_GV_TS_OBJ, "Adjust");
end

-- п.2.8 (After-load Settings)
function This:MPRC_ALS_INITIALIZE()
   PanelTemplates_SetNumTabs(GGF.INS.GetObjectRef(self.JR_AD_SF_OBJ), 2);  -- 2 total

   self:managePanels(1);
end

-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------