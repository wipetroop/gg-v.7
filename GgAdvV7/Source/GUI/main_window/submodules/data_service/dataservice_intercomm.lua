local This = GGF.ModuleGetWrapper("GUI-5-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.1 (Miscellaneous Variables)
function This:MPRC_MVR_INITIALIZE()
	-- NOTE: вроде больше не нужно
	--self.vs_hInstance.gUi = LibStub("AceGUi-3.0");
	--self.vs_hInstance.tbl = LibStub("ScrollingTable");
end


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------
	local MSC_CONFIG = self:getConfig();

	-- dataService(SF)

	self.JR_DS_SF_OBJ = GGC.StandardFrame:Create(self.JR_DS_SF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.MainWindow:GetLeadingFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.MainWindow:GetLeadingFrame(),
			},
		},
	});


	-- dataService(SF)
		-- dataSettings(SF)

	self.JR_DS_SF_DS_SF_OBJ = GGC.StandardFrame:Create(self.JR_DS_SF_DS_SF_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_DS_SF_OBJ),
			},
			size = {
				height = function()
					return GGM.GUI.MainWindow:GetHeight()
						+ GGF.OuterInvoke(self.JR_DS_SF_OBJ, "GetHeight")
						+ GGF.TernExpSingle(
							GGF.OuterInvoke(
								self.JR_DS_SF_GV_TS_OBJ,
								"GetHighlighted"
							),
							GGM.GUI.DSGraphicEngine:GetHeight(),
							GGM.GUI.DSTableEngine:GetHeight()
						)
					end,
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_DS_SF_OBJ),
			},
		},
	});

	-- HACK: аджастится при появлении, чтобы не критовал вызов параметров других окон


	-- dataService(SF)
		-- dataSettings(SF)
			-- filterSettings(AGF)

	self.JR_DS_SF_DS_SF_FS_AGF_OBJ = GGC.AggregatorFrame:Create(self.JR_DS_SF_DS_SF_FS_AGF_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_DS_SF_DS_SF_OBJ),
			},
			size = {
				--width = function() return MSC_CONFIG.operationWidth() - 2*MSC_CONFIG.aFrameGap end,-- - self.JR_DS_SF_DS_SF_V_S_OBJ:GetWidth() end,
				-- NOTE: костыль
				height = 600,--function() return self.JR_DS_SF_DS_SF_OBJ:GetHeight() + self.JR_DS_SF_DS_SF_FS_AGF_TMP.properties.anchor.offsetY - MSC_CONFIG.aFrameGap end,
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_DS_SF_OBJ),
			},
		},
	});

	-- HACK: вынесено в конец, чтоб не падал вызов высоты основоного фрейма


	-- dataService(SF)
		-- dataSettings(SF)
			-- aggregator(SF)

	--self.JR_DS_SF_DS_SF_A_SF_OBJ = GGC.StandardFrame:Create(self.JR_DS_SF_DS_SF_AP_SF_TMP, {
		--properties = {
			--base = {
				--parent = self.JR_DS_SF_DS_SF_OBJ:GetStandardFrame(),
			--},
			--size = {
				--width = function() return MSC_CONFIG.operationWidth() - 2*MSC_CONFIG.aFrameGap - self.JR_DS_SF_DS_SF_V_SS_OBJ:GetWidth() end,
				--height = function() return self.JR_DS_SF_DS_SF_OBJ:GetHeight() + self.JR_DS_SF_DS_SF_A_SF_TMP.properties.anchor.offsetY - MSC_CONFIG.aFrameGap end,
			--},
			--anchor = {
				--relativeTo = self.JR_DS_SF_DS_SF_OBJ:GetStandardFrame(),
			--},
		--},
	--});


	-- dataService(SF)
		-- dataSettings(SF)
			-- aggregatorPhantom(SF)

	--self.JR_DS_SF_DS_SF_AP_SF_OBJ = GGC.StandardFrame:Create(self.JR_DS_SF_DS_SF_AP_SF_TMP, {
		--properties = {
			--base = {
				--parent = self.JR_DS_SF_DS_SF_OBJ:GetStandardFrame(),
			--},
			--size = {
				--width = function() return MSC_CONFIG.operationWidth() - 2*MSC_CONFIG.aFrameGap - self.JR_DS_SF_DS_SF_V_SS_OBJ:GetWidth() end,
			--},
			--anchor = {
				--relativeTo = self.JR_DS_SF_DS_SF_OBJ:GetStandardFrame(),
			--},
		--},
	--});

	-- HACK: вынесено в конец, чтоб не падал вызов высоты основоного фрейма


	-- dataService(SF)
		-- dataSettings(SF)
			-- title(FS)

	--self.JR_DS_SF_DS_SF_T_FS_OBJ = GGC.FontString:Create(self.JR_DS_SF_DS_SF_T_FS_TMP, {
		--properties = {
			--base = {
				--parent = self.JR_DS_SF_DS_SF_OBJ:GetStandardFrame(),
			--},
			--anchor = {
				--relativeTo = self.JR_DS_SF_DS_SF_OBJ:GetStandardFrame(),
			--},
		--},
	--});


	-- dataService(SF)
		-- dataSettings(SF)
			-- vertical(SS)

	--self.JR_DS_SF_DS_SF_V_SS_OBJ = GGC.StandardSlider:Create(self.JR_DS_SF_DS_SF_V_SS_TMP, {
		--properties = {
			--base = {
				--parent = self.JR_DS_SF_DS_SF_OBJ:GetStandardFrame(),
			--},
			--size = {
				--height = function() return self.JR_DS_SF_DS_SF_OBJ:GetHeight() + self.JR_DS_SF_DS_SF_A_SF_TMP.properties.anchor.offsetY - MSC_CONFIG.aFrameGap - MSC_CONFIG.vSliderHeightSpike end,
			--},
			--anchor = {
				--relativeTo = self.JR_DS_SF_DS_SF_OBJ:GetStandardFrame(),
			--},
		--},
	--});

	-- HACK: вынесено в конец, чтоб не падал вызов высоты основоного фрейма


	-- dataService(SF)
		-- dataSettings(SF)
			-- addCombatLog(STB)

	--self.JR_DS_SF_DS_SF_ACL_STB_OBJ = GGC.StuckingButton:Create(self.JR_DS_SF_DS_SF_ACL_STB_TMP, {
		--properties = {
			--base = {
				--parent = self.JR_DS_SF_DS_SF_OBJ:GetStandardFrame(),
			--},
			--anchor = {
				--relativeTo = self.JR_DS_SF_DS_SF_OBJ:GetStandardFrame(),
			--},
		--},
	--});


	-- dataService(SF)
		-- dataSettings(SF)
			-- addFilter(STB)

	--self.JR_DS_SF_DS_SF_AF_STB_OBJ = GGC.StuckingButton:Create(self.JR_DS_SF_DS_SF_AF_STB_TMP, {
		--properties = {
			--base = {
				--parent = self.JR_DS_SF_DS_SF_OBJ:GetStandardFrame(),
			--},
			--anchor = {
				--relativeTo = self.JR_DS_SF_DS_SF_ACL_STB_OBJ:GetStuckingButton(),
			--},
		--},
	--});


	-- dataService(SF)
		-- dataSettings(SF)
			-- filterSettings(AF)

	--self.JR_DS_F_DS_F_FS_AF_OBJ = GGE.FilterType:Create(self.JR_DS_F_DS_F_FS_AF_TMP, {

	--});


	-- dataService(SF)
		-- dataSettings(STB)

	self.JR_DS_SF_DS_STB_OBJ = GGC.StuckingButton:Create(self.JR_DS_SF_DS_STB_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_DS_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_DS_SF_OBJ),
			},
		},
	});


	-- dataService(SF)
		-- сhatReport(STB)

	self.JR_DS_SF_CR_STB_OBJ = GGC.StuckingButton:Create(self.JR_DS_SF_CR_STB_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_DS_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_DS_SF_OBJ),
			},
		},
	});


	-- dataService(SF)
		-- tableVision(TS)

	self.JR_DS_SF_TV_TS_OBJ = GGC.TabSwitch:Create(self.JR_DS_SF_TV_TS_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_DS_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_DS_SF_OBJ),
			},
		},
	});


	-- dataService(SF)
		-- graphicVision(TS)

	self.JR_DS_SF_GV_TS_OBJ = GGC.TabSwitch:Create(self.JR_DS_SF_GV_TS_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_DS_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_DS_SF_TV_TS_OBJ),
			},
		},
	});
end


-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- dataService(SF)
	GGF.OuterInvoke(self.JR_DS_SF_OBJ, "Adjust");


	-- dataService(SF)
		-- dataSettings(SF)

	--self.JR_DS_SF_DS_SF_OBJ:Adjust();


	-- dataService(SF)
		-- dataSettings(SF)
			-- filterSettings(AGF)
	GGF.OuterInvoke(self.JR_DS_SF_DS_SF_FS_AGF_OBJ, "Adjust");


	-- dataService(SF)
		-- dataSettings(SF)
			-- aggregator(SF)

	--self.JR_DS_SF_DS_SF_A_SF_OBJ:Adjust();


	-- dataService(SF)
		-- dataSettings(SF)
			-- aggregatorPhantom(SF)

	--self.JR_DS_SF_DS_SF_AP_SF_OBJ:Adjust();


	-- dataService(SF)
		-- dataSettings(SF)
			-- title(FS)

	--self.JR_DS_SF_DS_SF_T_FS_OBJ:Adjust();


	-- dataService(SF)
		-- dataSettings(SF)
			-- vertical(SS)

	--self.JR_DS_SF_DS_SF_V_SS_OBJ:Adjust();


	-- dataService(SF)
		-- dataSettings(SF)
			-- addCombatLog(STB)

	--self.JR_DS_SF_DS_SF_ACL_STB_OBJ:Adjust();


	-- dataService(SF)
		-- dataSettings(SF)
			-- addFilter(STB)

	--self.JR_DS_SF_DS_SF_AF_STB_OBJ:Adjust();


	-- dataService(SF)
		-- dataSettings(STB)
	GGF.OuterInvoke(self.JR_DS_SF_DS_STB_OBJ, "Adjust");


	-- dataService(SF)
		-- chatReport(STB)
	GGF.OuterInvoke(self.JR_DS_SF_CR_STB_OBJ, "Adjust");


	-- dataService(SF)
		-- tableVision(TS)
	GGF.OuterInvoke(self.JR_DS_SF_TV_TS_OBJ, "Adjust");


	-- dataService(SF)
		-- graphicVision(TS)
	GGF.OuterInvoke(self.JR_DS_SF_GV_TS_OBJ, "Adjust");
end


-- п.2.8 (After-load Settings)
function This:MPRC_ALS_INITIALIZE()
	PanelTemplates_SetNumTabs(GGF.INS.GetObjectRef(self.JR_DS_SF_OBJ), 2);	-- 2 total

	self:managePanels(1);

	self:changeDSButtonState(false);
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------