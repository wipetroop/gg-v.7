local This = GGF.ModuleGetWrapper("GUI-5-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- api
-- Смена видиомсти основного фрейма
function This:setHidden(in_vis)
	GGF.OuterInvoke(self.JR_DS_SF_OBJ, "SetHidden", in_vis);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Взятие объекта главного окна
function This:getLeadingFrame()
	return GGF.INS.GetObjectRef(self.JR_DS_SF_OBJ);
end


-- api
-- Взятие объекта окна настроек
function This:getFilterSettingsFrame()
	return GGF.INS.GetObjectRef(self.JR_DS_SF_DS_SF_FS_AGF_OBJ);
end


-- api
-- Взятие ширины основного фрейма
function This:getWidth()
	return GGF.OuterInvoke(self.JR_DS_SF_OBJ, "GetWidth");
end


-- api
-- Взятие высоты основного фрейма
function This:getHeight()
	return GGF.OuterInvoke(self.JR_DS_SF_OBJ, "GetHeight");
end


-- api
-- Взятие списка активных фильтров
function This:getActiveFilterList()
	return GGF.OuterInvoke(self.JR_DS_SF_DS_SF_FS_AF_OBJ, "GetActiveFilterList", GGE.FilterType.Data);
end


-- api
-- Взятие списка активных логов
function This:getActiveLogList()
	return GGF.OuterInvoke(self.JR_DS_SF_DS_SF_FS_AF_OBJ, "GetActiveFilterList", GGE.FilterType.Log);
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------

-- Применение значений некоторых зависимых фреймов
function This:adjustRelativeFrames()
	--self.JR_DS_SF_DS_SF_OBJ:Adjust();
	--self.JR_DS_SF_DS_SF_A_F_OBJ:Adjust();
	--self.JR_DS_SF_DS_SF_AP_F_OBJ:Adjust();
	--self.JR_DS_SF_DS_SF_V_S_OBJ:Adjust();
end


-- api
-- Установка линейных размеров основного фрейма
function This:adjustSize(in_width, in_height)
	-- NOTE: похоже, эта функция выведена из эксплуатации....но это не точно
	local offsetX = math.abs(GGM.GUI.MainWindow:GetChildTabbedFrameOffsetX());
	local offsetYst = math.abs(GGM.GUI.MainWindow:GetChildTabbedFrameOffsetX());
	local offsetYenh = math.abs(GGM.GUI.MainWindow:GetChildTabbedFrameOffsetY()) + math.abs(GGM.GUI.MainWindow:GetButtonExistenceOffsetYCompensator());

	local frameWidth = in_width - 2*offsetX;
	local frameHeight = in_height - offsetYst - offsetYenh;

	--self.JR_DS_F_OBJ:SetWidth(in_width);
	--self.JR_DS_F_OBJ:SetHeight(in_height);

	--self.JR_DS_F_GS_F_OBJ:SetHeight(in_height);
	--self.JR_DS_F_DS_F_OBJ:SetHeight(in_height);

	--self.JR_DS_F_BL_F_CT_F_OBJ:SetHeight(self.JR_DS_F_BL_F_OBJ:GetHeight() - math.abs(self.JR_DS_F_BL_F_CT_F_OBJ:GetOffsetX()) - math.abs(self.JR_DS_F_BL_F_CT_F_OBJ:GetOffsetY()));

	--GGM.GUI.DSTableEngine:AdjustSize(frameWidth, frameHeight);
	--GGM.GUI.DSGraphicEngine:AdjustSize(frameWidth, frameHeight);
end


-- Компактный переключатель панелей
function This:managePanels(in_panelNum)
	ATLASSERT(in_panelNum > 0 and in_panelNum < 3);
	local pn1, pn2 = (in_panelNum == 1), (in_panelNum == 2);

	self.vn_currentTabNum = in_panelNum;
	GGF.OuterInvoke(self.JR_DS_SF_TV_TS_OBJ, "SetEnabled", pn1);
	GGF.OuterInvoke(self.JR_DS_SF_GV_TS_OBJ, "SetEnabled", pn2);

	GGF.OuterInvoke(self.JR_DS_SF_TV_TS_OBJ, "SetHighlighted", pn1);
	GGF.OuterInvoke(self.JR_DS_SF_GV_TS_OBJ, "SetHighlighted", pn2);

	PanelTemplates_SetTab(GGF.OuterInvoke(self.JR_DS_SF_OBJ, "GetStandardFrame"), in_panelNum);

	GGM.GUI.DSTableEngine:SetHidden(not pn1);
	GGM.GUI.DSGraphicEngine:SetHidden(not pn2);

	self:adjustRelativeFrames();
	self:checkDataviewPossibility();
end


-- Смена состояния кнопки вызова окна фильтров
function This:changeDSButtonState(in_state)
	if (GGF.OuterInvoke(self.JR_DS_SF_DS_STB_OBJ, "GetEnabled") == in_state) then
		return; -- ничего не делаем
	end
	GGF.OuterInvoke(self.JR_DS_SF_DS_STB_OBJ, "SetHighlighted", in_state);
	GGF.OuterInvoke(self.JR_DS_SF_DS_SF_OBJ, "SetHidden", not in_state);
end


-- Функция проверки возможности дампа данных в чат(перегруз не учтен(использовать аккуратно))
function This:checkDumpPossibility()
	if (self:checkDataFiltering() 
		and (self.vs_menuSelector.logClusterTitle ~= "")
		and (self.vs_menuSelector.chatType ~= "")) then
		GGF.OuterInvoke(self.JR_DS_SF_CR_SF_D_B_OBJ, "SetEnabled", true);
	else
		GGF.OuterInvoke(self.JR_DS_SF_CR_SF_D_B_OBJ, "SetEnabled", false);
	end
end


-- api
-- Проверка отфильтрованности данных
function This:checkDataFiltering()
	if (self.vs_menuSelector.filter.evType == "Damage") then
		if ((self.vs_menuSelector.filter.atkType ~= "None")
			and (self.vs_menuSelector.filter.defType ~= "None")) then
			return true;
		end
	elseif (self.vs_menuSelector.filter.evType == "Miss") then
		if ((self.vs_menuSelector.filter.atkType ~= "None")
		and (self.vs_menuSelector.filter.missType ~= "None")
		and (self.vs_menuSelector.filter.defType ~= "None")) then
			return true;
		end
	elseif (self.vs_menuSelector.filter.evType == "Kill") then
		if (self.vs_menuSelector.filter.defType ~= "None") then
			return true;
		end
	end

	return false;

	-- Вот тут уже другой модуль вроде бы
	--if (self.isGGM.GUI.DataFiltered == false) then
		--self.ThisGeometry.dataTable.Default.handler.frame:Hide();
	 	--self.ThisGeometry.dataTable.daMage.handler.frame:Hide();
	 	--self.ThisGeometry.dataTable.miss.handler.frame:Hide();
	 	--self.ThisGeometry.dataTable.kill.handler.frame:Hide();
	--end
end


-- Проверка возможности отображения данных в таблицу и график
function This:checkDataviewPossibility()
	--if (self:checkGGM.GUI.DataFiltering()
		--and (self.vs_menuSelector.logClusterTitle ~= "")) then
		--GGM.GUI.DSTableEngine:DataDump(self.vs_menuSelector);
		--self:changeGSButtonState(true);
	--else
		--GGM.GUI.DSTableEngine:DataDrop();
	--end
end


-- Проверка наличия значений в списке
function This:checkList(in_list)
	for k,v in pairs(in_list) do
		return true;
	end
	return false;
end


-- Обработчик слеш-команды
function This:slashcommand(in_dspec)
	-- [kl, dm, ms] [al, sw, rg, sl, sp, en] [al, ab, bl, de, do, ev, im, ms, pr, rf, rs] [al, df, un] [fl, sg]
	
	-- [kl] [al, df, un]
	-- [dm] [al, sw, rg, sl, sp, en] [al, df, un] [fl, sg]
	-- [ms] [al, sw, rg, sl, sp, en] [al, ab, bl, de, do, ev, im, ms, pr, rf, rs] [al, df, un] [fl, sg]

	local lv1type, lv2type, lv3type, lv4type, lv5type;

	lv1type = dspec[2];

	if (lv1type == "kl") then
		self:on_dataFilterCB_clicked("evType", "Kill");

		lv4type = dspec[3];
	elseif (lv1type == "dm") or (lv1type == "ms") then
		if (lv1type == "dm") then
			self:on_dataFilterCB_clicked("evType", "DaMage");

			lv4type = dspec[4];
			lv5type = dspec[5];
		elseif (lv1type == "ms") then
			self:on_dataFilterCB_clicked("evType", "Miss");

			lv3type = dspec[4];

			if (lv3type == "al") then
				self:on_dataFilterCB_clicked("missType", "All");
			elseif (lv3type == "ab") then
				self:on_dataFilterCB_clicked("missType", "Absorb");
			elseif (lv3type == "bl") then
				self:on_dataFilterCB_clicked("missType", "Block");
			elseif (lv3type == "de") then
				self:on_dataFilterCB_clicked("missType", "Deflect");
			elseif (lv3type == "do") then
				self:on_dataFilterCB_clicked("missType", "Dodge");
			elseif (lv3type == "ev") then
				self:on_dataFilterCB_clicked("missType", "Evade");
			elseif (lv3type == "im") then
				self:on_dataFilterCB_clicked("missType", "Immune");
			elseif (lv3type == "ms") then
				self:on_dataFilterCB_clicked("missType", "Miss");
			elseif (lv3type == "pr") then
				self:on_dataFilterCB_clicked("missType", "Parry");
			elseif (lv3type == "rf") then
				self:on_dataFilterCB_clicked("missType", "Reflect");
			elseif (lv3type == "rs") then
				self:on_dataFilterCB_clicked("missType", "Resist");
			else
				print("lv3 parameter mismatch");
				return;
			end
			
			lv4type = dspec[5];
			lv5type = dspec[6];
		end

		lv2type = dspec[3];

		if (lv2type == "al") then
			self:on_dataFilterCB_clicked("atkType", "All");
		elseif (lv2type == "sw") then
			self:on_dataFilterCB_clicked("atkType", "Swing");
		elseif (lv2type == "rg") then
			self:on_dataFilterCB_clicked("atkType", "Range");
		elseif (lv2type == "sl") then
			self:on_dataFilterCB_clicked("atkType", "Spell");
		elseif (lv2type == "sp") then
			self:on_dataFilterCB_clicked("atkType", "SpellPeriodic");
		elseif (lv2type == "en") then
			self:on_dataFilterCB_clicked("atkType", "Environmental");
		else
			print("lv2 parameter mismatch");
			return;
		end
	else
		print("lv1 parameter mismatch");
		return;
	end

	if (lv4type == "al") then
		self:on_dataFilterCB_clicked("defType", "All")
	elseif (lv4type == "df") then
		self:on_dataFilterCB_clicked("defType", "Defined")
	elseif (lv4type == "un") then
		self:on_dataFilterCB_clicked("defType", "Undefined")
	else
		print("lv4 parameter mismatch");
		return;
	end

	--self.upDateLogList();

	--if (#self.vs_logList == 0) then
		--return;
	--end

	-- TODO: приделать взятие последнего лога
	-- по дефолту ласт бой берется
	--self:uncheckLogButtons();
	--self:on_battleLogChoise_click(self.vs_GGC_CheckButtonList.battleLogFrame[#self.vs_GGC_CheckButtonList.battleLogFrame - 1]);

	-- делаем панель дата сервиса активной и видимой
	GGM.GUI.MainWindow:SetFrameVisibility(true);
	GGM.GUI.MainWindow:DataServiceSwitch();
end


-- api
-- Смена состояния кнопки переключателя чат репорта
function This:changeChatReportTSState()
	self:on_chatReportTS_clicked();
end


-- api
-- Смена состояния кнопки переключателя добавления фильтра
function This:changeAddFilterTSState()
	self:on_addFilterTS_clicked();
end


-- api
-- Смена состояния кнопки переключателя добавления лога
function This:changeAddCombatLogTSState()
	self:on_addCombatLogTS_clicked();
end


-- api
-- Добавление лога в аггрегатор
function This:addLogFilter(in_token)
	--NOTE: пока так, потом добавить метаданные по логам
	GGF.OuterInvoke(self.JR_DS_SF_DS_SF_FS_AGF_OBJ, "AddNewFilter", GGE.FilterType.Log, in_token);
end


-- api
-- Добавление фильтра данных в аггрегатор
function This:addDataFilter(in_variant)
	--NOTE: пока так, потом добавить метаданные по фильтрам
	GGF.OuterInvoke(self.JR_DS_SF_DS_SF_FS_AGF_OBJ, "AddNewFilter", GGE.FilterType.Data, in_variant);
end


-- api
-- Подсчет количества фильтров по шаблону
function This:countFiltersByPattern(...)
	return GGF.OuterInvoke(self.JR_DS_SF_DS_SF_FS_AGF_OBJ, "CountFiltersByPattern", ...);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-- Обработчик тумблера фильтров данных
function This:on_DS_STB_stateChanged(in_state)
	--local hglt = self.JR_DS_SF_DS_STB_OBJ:GetHighlighted();
	--self.JR_DS_SF_DS_STB_OBJ:SetHighlighted(not hglt);
	GGF.OuterInvoke(self.JR_DS_SF_DS_SF_OBJ, "SetHidden", not in_state);
	if (in_state) then
		self:adjustRelativeFrames();
	end
end


-- Обработчик тумблера меню чат репорта
function This:on_ACL_STB_stateChanged(in_state)
	--local hglt = self.JR_DS_F_DS_F_ACL_TS_OBJ:GetHighlighted();
	--self.JR_DS_F_DS_F_ACL_TS_OBJ:SetHighlighted(not hglt);

	-- NOTE: тут не зачищаем данные
	GGM.GUI.LogFilter:SetHidden(not in_state);
end


-- Обработчик тумблера меню чат репорта
function This:on_AF_STB_stateChanged(in_state)
	--local hglt = self.JR_DS_SF_DS_SF_AF_STB_OBJ:GetHighlighted();
	--self.JR_DS_SF_DS_SF_AF_STB_OBJ:SetHighlighted(not hglt);

	-- NOTE: тут не зачищаем данные
	GGM.GUI.DataFilter:SetHidden(not in_state);
end


-- Обработчик тумблера меню чат репорта
function This:on_CR_STB_stateChanged(in_state)
	--local hglt = self.JR_DS_SF_CR_STB_OBJ:GetHighlighted();
	--self.JR_DS_SF_CR_STB_OBJ:SetHighlighted(not hglt);

	-- NOTE: тут не зачищаем данные
	GGM.GUI.ChatReport:SetHidden(not in_state);

	if (This.vn_currentTabNum == 1) then
		GGM.GUI.DSTableEngine:SetHidden(in_state);
	else
		GGM.GUI.DSGraphicEngine:SetHidden(in_state);
	end
end


-- Обработчик свича в таблицу
function This:on_tableVisionTS_clicked()
	self:managePanels(1);
end


-- Обработчик свича в график
function This:on_graphicVisionTS_clicked()
	self:managePanels(2);
end


-- Обработчик изменений настроек графика
function This:on_graphicSettingsCB_clicked(in_level, in_button)
	if (self.vs_checkButtonList.graphic.settingsFrame[in_level][in_button]:GetChecked()) then
		if (in_level == "statType") then
			if (in_button == "avg") then
				self:showGSLevel("avgType");
				self.vs_menuSelector.gsettings.statList[in_button] = true;
			elseif (in_button == "summ") then
				self:showGSLevel("summType");
				self.vs_menuSelector.gsettings.statList[in_button] = true;
			else
				return;
			end
		elseif (in_level == "avgType") then
			self.vs_menuSelector.gsettings.avgList[in_button] = true;
			-- NOTE: потом надо вышибать лишние кнопки
		elseif (in_level == "summType") then
			self.vs_menuSelector.gsettings.summList[in_button] = true;
			-- NOTE: потом надо вышибать лишние кнопки
		else
			return;
		end
	else
		if (in_level == "statType") then
			if (in_button == "avg") then
				self:hideGSLevel("avgType");
				self.vs_menuSelector.gsettings.avgList = {};
				self.vs_menuSelector.gsettings.statList[in_button] = nil;
			elseif (in_button == "summ") then
				self:hideGSLevel("summType");
				self.vs_menuSelector.gsettings.summList = {};
				self.vs_menuSelector.gsettings.statList[in_button] = nil;
			else
				return;
			end
		elseif (in_level == "avgType") then
			self.vs_menuSelector.gsettings.avgList[in_button] = nil;
			-- NOTE: потом надо вышибать лишние кнопки
		elseif (in_level == "summType") then
			self.vs_menuSelector.gsettings.summList[in_button] = nil;
			-- NOTE: потом надо вышибать лишние кнопки
		else
			return;
		end
	end

	self:checkGraphicPossibility();
end


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------