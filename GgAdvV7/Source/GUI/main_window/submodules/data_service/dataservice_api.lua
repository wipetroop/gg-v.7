local This = GGF.ModuleGetWrapper("GUI-5-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Смена видиомсти основного фрейма
function This:SetHidden(in_vis)
	self:setHidden(in_vis);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие объекта главного окна
function This:GetLeadingFrame()
	return self:getLeadingFrame();
end


-- Взятие объекта окна настроек
function This:GetFilterSettingsFrame()
	return self:getFilterSettingsFrame();
end


-- Взятие ширины основного фрейма
function This:GetWidth()
	return self:getWidth();
end


-- Взятие высоты основного фрейма
function This:GetHeight()
	return self:getHeight();
end


-- Взятие списка активных фильтров
function This:GetActiveFilterList()
	return self:getActiveFilterList();
end


-- Взятие списка активных логов
function This:GetActiveLogList()
	return self:getActiveLogList();
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- Установка линейных размеров дочерних элементов
function This:AdjustSize(in_width, in_height)
	self:adjustSize(in_width, in_height)
end


-- Проверка отфильтрованности данных(только для сабов)
function This:CheckDataFiltering()
	self:checkDataFiltering();
end


-- Обработчик слеш-команды
function This:Slashcommand(in_dspec)
	self:slashcommand(in_dspec);
end


-- Смена состояния кнопки переключателя чат репорта
function This:ChangeChatReportTSState()
	self:changeChatReportTSState();
end


-- Смена состояния кнопки переключателя добавления фильтра
function This:ChangeAddFilterTSState()
	self:changeAddFilterTSState();
end


-- Смена состояния кнопки переключателя добавления лога
function This:ChangeAddCombatLogTSState()
	self:changeAddCombatLogTSState();
end


-- Добавление лога в аггрегатор
function This:AddLogFilter(in_token)
	self:addLogFilter(in_token);
end


-- Добавление фильтра данных в аггрегатор
function This:AddDataFilter(in_variant)
	self:addDataFilter(in_variant);
end


-- Подсчет количества фильтров по шаблону
function This:CountFiltersByPattern(...)
	return self:countFiltersByPattern(...);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------