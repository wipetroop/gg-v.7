local This = GGF.ModuleGetWrapper("GUI-5-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_dataServiceGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		dataService = {					-- [DS_SF]
			inherit = {
				standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
					dataSettings = {				-- [DS_SF]
						inherit = {
							aggregatorFrame = GGF.GTemplateTree(GGC.AggregatorFrame, {
								filterSettings = {},		-- [FS_AGF]
							}),
							--standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
								--aggregator = {},			-- [A_SF]
								-- NOTE: разница между двумя фреймами в наличии физ. фрейма у последнего
								--aggregatorPhantom = {},		-- [AP_SF]
							--}),
							--fontString = GGF.GTemplateTree(GGC.FontString, {
								--title = {},					-- [T_FS]
							--}),
							--standardSlider = GGF.GTemplateTree(GGC.StandardSlider, {
								--vertical = {},				-- [V_SS]
							--}),
							--stuckingButton = GGF.GTemplateTree(GGC.StuckingButton, {
								--addCombatLog = {},			-- [ACL_STB]
								--addFilter = {},				-- [AF_STB]
							--}),
						},
					},
				}),
				stuckingButton = GGF.GTemplateTree(GGC.StuckingButton, {
					dataSettings = {},		-- [DS_STB]
					chatReport = {},		-- [CR_STB]
				}),
				tabSwitch = GGF.GTemplateTree(GGC.TabSwitch, {
					tableVision = {},		-- [TV_TS]
					graphicVision = {},		-- [GV_TS]
				}),
			},
		},
	}),
};

local DSG_SF = This.oc_dataServiceGeometry.standardFrame;
local DSG_SF_DS_SF = DSG_SF.dataService.inherit.standardFrame;
local DSG_SF_DS_SF_DS_AGF = DSG_SF_DS_SF.dataSettings.inherit.aggregatorFrame;
--local DSG_SF_DS_SF_DS_SF = DSG_SF_DS_SF.dataSettings.inherit.standardFrame;
--local DSG_SF_DS_SF_DS_FS = DSG_SF_DS_SF.dataSettings.inherit.fontString;
--local DSG_SF_DS_SF_DS_SS = DSG_SF_DS_SF.dataSettings.inherit.standardSlider;
--local DSG_SF_DS_SF_DS_STB = DSG_SF_DS_SF.dataSettings.inherit.stuckingButton;
local DSG_SF_DS_STB = DSG_SF.dataService.inherit.stuckingButton;
local DSG_SF_DS_TS = DSG_SF.dataService.inherit.tabSwitch;

-- dataService(SF)
DSG_SF.dataService.object = {};
DSG_SF.dataService.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "DS",
		},
		size = {
			width = function() return MSC_CONFIG.frameWidth() end,
			height = function() return MSC_CONFIG.frameHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Segregate,
	},
};


-- dataService(SF)
	-- dataSettings(SF)
DSG_SF_DS_SF.dataSettings.object = {};
DSG_SF_DS_SF.dataSettings.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "DS",
		},
		size = {
			width = function() return MSC_CONFIG.operationWidth() end,	-- 812
			--height = 0,	-- 406
		},
		anchor = {
			point = GGE.PointType.TopRight,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.TopLeft,
			offsetX = 0,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Segregate,
	},
};


-- dataService(SF)
	-- dataSettings(SF)
		-- filterSettings(AGF)
DSG_SF_DS_SF_DS_AGF.filterSettings.object = {};
DSG_SF_DS_SF_DS_AGF.filterSettings.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "FS",
		},
		size = {
			width = function() return MSC_CONFIG.operationWidth() end,	-- 812
			--height = 0,	-- 406
		},
		callback = {
			onACLStateChanged = function(in_wrapper, ...) This:on_ACL_STB_stateChanged(...) end,
			onAFStateChanged = function(in_wrapper, ...) This:on_AF_STB_stateChanged(...) end,
		},
		anchor = {
			point = GGE.PointType.TopRight,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.TopLeft,
			offsetX = MSC_CONFIG.aFrameOffsetX,
			offsetY = MSC_CONFIG.aFrameOffsetY,
		},
		backdrop = GGD.Backdrop.Segregate,
	},
};


-- dataService(SF)
	-- dataSettings(SF)
		-- aggregator(SF)
--DSG_SF_DS_SF_DS_SF.aggregator.object = {};
--DSG_SF_DS_SF_DS_SF.aggregator.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	--properties = {
		--base = {
			--parentName = nil,	-- fwd
			--parent = nil,		-- fwd
			--wrapperName = "A",
		--},
		--size = {
			--width = function() return MSC_CONFIG.operationWidth() - 2*MSC_CONFIG.aFrameGap end,	-- 812
			--height = 0,	-- 406
		--},
		--anchor = {
			--point = GGE.PointType.TopLeft,
			--relativeTo = nil,	-- fwd
			--relativePoint = GGE.PointType.TopLeft,
			--offsetX = MSC_CONFIG.aFrameOffsetX,
			--offsetY = MSC_CONFIG.aFrameOffsetY,
		--},
		--backdrop = GGD.Backdrop.Nested,
		--backdrop = GGD.Backdrop.Segregate,
	--},
--};


-- dataService(SF)
	-- dataSettings(SF)
		-- aggregatorPhantom(SF)
--DSG_SF_DS_SF_DS_SF.aggregatorPhantom.object = {};
--DSG_SF_DS_SF_DS_SF.aggregatorPhantom.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	--properties = {
		--base = {
			--parentName = nil,	-- fwd
			--parent = nil,		-- fwd
			--wrapperName = "AP",
		--},
		--size = {
			--width = function() return MSC_CONFIG.operationWidth() - 2*MSC_CONFIG.aFrameGap end,	-- 812
			--height = 0,	-- 406
		--},
		--anchor = {
			--point = GGE.PointType.TopLeft,
			--relativeTo = nil,	-- fwd
			--relativePoint = GGE.PointType.TopLeft,
			--offsetX = 0,
			--offsetY = 0,
		--},
		--backdrop = GGD.Backdrop.Empty,
		--backdrop = GGD.Backdrop.Segregate,
	--},
--};


-- dataService(SF)
	-- dataSettings(SF)
		-- title(FS)
--DSG_SF_DS_SF_DS_FS.title.object = {};
--DSG_SF_DS_SF_DS_FS.title.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	--properties = {
		--base = {
			--parentName = nil,	-- fwd
			--parent = nil,		-- fwd
			--wrapperName = "T",
		--},
		--miscellaneous = {
			--text = "Default",
			--tooltipText = "GUi_MW_F_DS_F_DF_F_T_FS",
		--},
		--size = {
			--width = function() return MSC_CONFIG.titleWidth() end,
			--height = function() return MSC_CONFIG.titleHeight() end,
		--},
		--anchor = {
			--point = GGE.PointType.Top,
			--relativeTo = nil,	-- fwd
			--relativePoint = GGE.PointType.Top,
			--offsetX = function() return MSC_CONFIG.titleOffsetX() end,
			--offsetY = function() return MSC_CONFIG.titleOffsetY() end,
		--},
	--},
--};


-- dataService(SF)
	-- dataSettings(SF)
		-- vertical(SS)
--DSG_SF_DS_SF_DS_SS.vertical.object = {};
--DSG_SF_DS_SF_DS_SS.vertical.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	--properties = {
		--base = {
			--parentName = nil,	-- fwd
			--parent = nil,		-- fwd
			--wrapperName = "V",
		--},
		--miscellaneous = {
			--orientation = "VERTICAL",
			--values = {
				--min = 0.0,
				--max = 10.0,
				--step = 1.0,
				--stepsPerPage = 1.0,
			--},
		--},
		--size = {
			--width = function() return MSC_CONFIG.vSliderWidth() end,
			--height = function() return MSC_CONFIG.buttonHeight() end, -- rel
		--},
		--anchor = {
			--point = GGE.PointType.BottomRight,
			--relativeTo = nil,	-- fwd
			--relativePoint = GGE.PointType.BottomRight,
			--offsetX = MSC_CONFIG.sliderBRSpikeOffsetX,
			--offsetY = MSC_CONFIG.sliderBRSpikeOffsetY,
		--},
		--backdrop = function() return GGF.TableCopy(MSC_CONFIG.vSliderBackdrop()) end;
		--callback = {
			--onValueChanged = function(...) This:on_verticalS_valueChanged(...) end,
			--onEnter = function(...) MSC_CONFIG.on_frame_entered(...) end,
			--onLeave = function(...) MSC_CONFIG.on_frame_leaved(...) end,
		--},
	--},
--};


-- dataService(SF)
	-- dataSettings(SF)
		-- addCombatLog(STB)
--DSG_SF_DS_SF_DS_STB.addCombatLog.object = {};
--DSG_SF_DS_SF_DS_STB.addCombatLog.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	--properties = {
		--base = {
			--parentName = nil,	-- fwd
			--parent = nil,		-- fwd
			--wrapperName = "ACL",
		--},
		--miscellaneous = {
			--text = "Add Combat Log",
			--tooltipText = "GUi_MW_F_DS_F_DS_F_ACL_TS",
		--},
		--size = {
			--width = function() return MSC_CONFIG.buttonWidth() end,
			--eight = function() return MSC_CONFIG.buttonHeight() end,
		--},
		--anchor = {
			--point = GGE.PointType.TopLeft,
			--relativeTo = nil,	-- fwd
			--relativePoint = GGE.PointType.TopLeft,
			--offsetX = MSC_CONFIG.aclButtonOffsetX,
			--offsetY = MSC_CONFIG.aclButtonOffsetY,
		--},
		--callback = {
			--onStateChanged = function(in_wrapper, ...) This:on_ACL_STB_stateChanged(...) end,
			--onEnter = function(...) MSC_CONFIG.on_frame_entered(...) end,
			--onLeave = function(...) MSC_CONFIG.on_frame_leaved(...) end,
		--},
	--},
--};


-- dataService(SF)
	-- dataSettings(SF)
		-- addFilter(STB)
--DSG_SF_DS_SF_DS_STB.addFilter.object = {};
--DSG_SF_DS_SF_DS_STB.addFilter.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	--properties = {
		--base = {
			--parentName = nil,	-- fwd
			--parent = nil,		-- fwd
			--wrapperName = "AF",
		--},
		--miscellaneous = {
			--text = "Add Filter",
			--tooltipText = "GUi_MW_F_DS_F_DS_F_AF_TS",
		--},
		--size = {
			--width = function() return MSC_CONFIG.buttonWidth() end,
			--height = function() return MSC_CONFIG.buttonHeight() end,
		--},
		--anchor = {
			--point = GGE.PointType.TopRight,
			--relativeTo = nil,	-- fwd
			--relativePoint = GGE.PointType.TopRight,
			--offsetX = MSC_CONFIG.afButtonOffsetX,
			--offsetY = MSC_CONFIG.afButtonOffsetY,
		--},
		--callback = {
			--onStateChanged = function(in_wrapper, ...) This:on_AF_STB_stateChanged(...) end,
			--onEnter = function(...) MSC_CONFIG.on_frame_entered(...) end,
			--onLeave = function(...) MSC_CONFIG.on_frame_leaved(...) end,
		--},
	--},
--};


-- dataService(SF)
	-- dataSettings(STB)
DSG_SF_DS_STB.dataSettings.object = {};
DSG_SF_DS_STB.dataSettings.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "DS",
		},
		miscellaneous = {
			text = "Data Settings",
			--tooltipText = "GUi_MW_F_DS_F_DS_TS",
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil, -- fwd
			relativePoint = GGE.PointType.Left,
			offsetX = MSC_CONFIG.dstsButtonOffsetX,
			offsetY = 0,--MSC_CONFIG.dstsButtonOffsetY,
		},
		callback = {
			onStateChanged = function(in_wrapper, ...) This:on_DS_STB_stateChanged(...) end,
		},
	},
};


-- dataService(SF)
	-- chatReport(STB)
DSG_SF_DS_STB.chatReport.object = {};
DSG_SF_DS_STB.chatReport.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "CR",
		},
		miscellaneous = {
			text = "Chat Report",
			--tooltipText = "GUi_MW_F_DS_F_CR_TS",
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.Right,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Right,
			offsetX = MSC_CONFIG.crtsButtonOffsetX,
			offsetY = 0,--MSC_CONFIG.crtsButtonOffsetY,
		},
		callback = {
			onStateChanged = function(in_wrapper, ...) This:on_CR_STB_stateChanged(...) end,
		},
	},
};


-- dataService(SF)
	-- tableVision(TS)
DSG_SF_DS_TS.tableVision.object = {};
DSG_SF_DS_TS.tableVision.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			template = "OptionsFrameTabButtonTemplate",
			wrapperName = "TV",
		},
		miscellaneous = {
			gxname = "Tab1",
			text = "Table",
			--tooltipText = "GUi_MW_F_DS_F_TV_TS",
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.BottomLeft,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.BottomLeft,
			offsetX = function() return MSC_CONFIG.tabSwitchOffsetX() end,
			offsetY = function() return MSC_CONFIG.tabSwitchOffsetY() end,
		},
		callback = {
			onClick = function(...) This:on_tableVisionTS_clicked(...) end,
		},
	},
};


-- dataService(SF)
	-- graphicVision(TS)
DSG_SF_DS_TS.graphicVision.object = {};
DSG_SF_DS_TS.graphicVision.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			template = "OptionsFrameTabButtonTemplate",
			wrapperName = "GV",
		},
		miscellaneous = {
			gxname = "Tab2",
			text = "Graphic",
			--tooltipText = "GUi_MW_F_DS_F_GV_TS",
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Right,
			offsetX = function() return MSC_CONFIG.tabSwitchHorGap() end,
			offsetY = 0,
		},
		callback = {
			onClick = function(...) This:on_graphicVisionTS_clicked(...) end,
		},
	},
};