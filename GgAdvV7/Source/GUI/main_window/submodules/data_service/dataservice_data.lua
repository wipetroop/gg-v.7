local This = GGF.ModuleCreateWrapper("GUI-5-3", false, {
	frameWidth = function() return GGM.GUI.ParameterStorage:GetMWFrameWidth() end,
	frameHeight = function() return GGM.GUI.ParameterStorage:GetDSFrameHeight() end,

	operationWidth = function() return GGM.GUI.ParameterStorage:GetOperationWidth() end,

	buttonWidth = function() return GGM.GUI.ParameterStorage:GetButtonWidth() end,
	buttonHeight = function() return GGM.GUI.ParameterStorage:GetButtonHeight() end,

	vSliderWidth = function() return GGM.GUI.ParameterStorage:GetVSliderWidth() end,

	-- NOTE: кнопка + зазоры
	tabSwitchOffsetX = function() return GGM.GUI.ParameterStorage:GetTabSwitchOffsetX() + GGM.GUI.ParameterStorage:GetButtonWidth() + 10 end,
	tabSwitchOffsetY = function() return GGM.GUI.ParameterStorage:GetTabSwitchOffsetY() end,

	tabSwitchHorGap = function() return GGM.GUI.ParameterStorage:GetTabSwitchHorGap() end,

	titleWidth = function() return GGM.GUI.ParameterStorage:GetTitleWidth() end,
	titleHeight = function() return GGM.GUI.ParameterStorage:GetTitleHeight() end,

	titleOffsetX = function() return GGM.GUI.ParameterStorage:GetTitleOffsetX() end,
	titleOffsetY = function() return GGM.GUI.ParameterStorage:GetTitleOffsetY() end,

	dstsButtonOffsetX = 10,
	dstsButtonOffsetY = -10,

	crtsButtonOffsetX = -10,
	crtsButtonOffsetY = -10,

	aFrameOffsetX = 0,--10,
	aFrameOffsetY = 0,---10,---70,
	aFrameGap = 10,

	sliderBRSpikeOffsetX = -7,
	sliderBRSpikeOffsetY = 28,
	vSliderHeightSpike = 35,

	on_cell_entered = function(...) GGM.GUI.ParameterStorage:GetCellEnterCallback()(...) end,
	on_cell_leaved = function(...) GGM.GUI.ParameterStorage:GetCellLeaveCallback()(...) end,
});

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------


-- Значения соответствуют апи близов
This.cs_chatTypes = {
	none = "None",
	console = "Console",
	gUild = "GUild",
	inst = "Instance",
	rw = "RaidWarning",
	yell = "Yell",
	party = "Party",
	whisper = "Whisper",
	say = "Say",
	raid = "Raid",
};

This.cs_chatTypeMapper = {
	gUi = {
		[0] = "none",
		[1] = "console",
		[2] = "gUild",
		[3] = "inst",
		[4] = "rw",
		[5] = "yell",
		[6] = "party",
		[7] = "whisper",
		[8] = "say",
		[9] = "raid",
	},
	mvc = {
		none = "none",
		console = "console",
		gUild = "gUild",
		instance = "inst",
		raidWarning = "rw",
		yell = "yell",
		party = "party",
		whisper = "whisper",
		say = "say",
		raid = "raid",
	},
};


-------------------------------------------------------------------------------
-- Functors
-------------------------------------------------------------------------------


This.fn_chatTyPetranscevier = {
	["Console"] = function(in_msg, in_trg) print(in_msg) end,
	["GUild"] = function(in_msg, in_trg) SendChatMessage(in_msg, "GUiLD") end,
	["Instance"] = function(in_msg, in_trg) SendChatMessage(in_msg, "INSTANCE_CHAT") end,
	["RaidWarning"] = function(in_msg, in_trg) SendChatMessage(in_msg, "RAID_Warning") end,
	["Yell"] = function(in_msg, in_trg) SendChatMessage(in_msg, "YELL") end,
	["Party"] = function(in_msg, in_trg) SendChatMessage(in_msg, "PARTY") end,
	["Whisper"] = function(in_msg, in_trg) SendChatMessage(in_msg, "WHISPER", nil, in_trg) end,
	["Say"] = function(in_msg, in_trg) SendChatMessage(in_msg, "SAY") end,
	["Raid"] = function(in_msg, in_trg) SendChatMessage(in_msg, "RAID") end,
};


-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------


-- Пока что хардкод тут, иначе выкидывает
--GGM.GUI.DataService.varnum.graph = LibStub:GetLibrary("LibGraph-2.0");
This.vn_currentTabNum = 0;


-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------


This.vf_warmedUp = false;

-- Замена на GGM.GUI.DataService.runtime.checkGGM.GUI.DataFiltering()
--GGM.GUI.DataService.varflag.isGGM.GUI.DataFiltered = false;


-------------------------------------------------------------------------------
-- Variable lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable struct
-------------------------------------------------------------------------------


-- TODO: разобраться, что за зверь такой..(GG-3)
This.vs_frame = nil;

This.vs_hInstance = {
	gUi = nil,
	tbl = nil,
};

This.vs_labelMap = {
	data = {	-- массивы фонтстрингов
		header = {},
		logfield = {},
	},
	meta = {
		allocated = {
			columns = 0,
			rows = 0
		},
		used = {
			columns = 0,
			rows = 0
		},
	},
};

-- Переменные для выбора

-- Мапа соответсвия для лог-листа, чтоб при речеке не делать лишних кнопок
This.vs_logChooseList = {};

-- Список инстансов кнопок
This.vs_checkButtonList = {
	dataFilterFrame = {
		evType = {},
		atkType = {},
		missType = {},
		defType = {},
	},
	graphicSettingsFrame = {
		statType = {},
		avgType = {},
		summType = {},
	},
	chatReportFrame = {
		common = {},
	},
};


-- NOTE: бля...."что" я этим заменил??...
-- Селектор, отвечающий за три основных панели чеков
This.vs_menuSelector = {
	logClusterTitle = "",

	filter = {
		evType = "None",
		atkType = "None",
		missType = "None",
		defType = "None",
	},

	gsettings = {
		statList = {},
		avgList = {},
		summList = {},
	},

	chatType = "",

	logClusterData = nil,
};


-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------