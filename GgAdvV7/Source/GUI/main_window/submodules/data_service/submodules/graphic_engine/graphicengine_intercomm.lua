local This = GGF.ModuleGetWrapper("GUI-5-3-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.1 (Miscellaneous Variables)
function This:MPRC_MVR_INITIALIZE()
	self.vs_hInstance.grp = LibStub:GetLibrary("LibGraph-2.0");
end


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------

	-- graphicVision(SF)

	self.JR_GV_SF_OBJ = GGC.StandardFrame:Create(self.JR_GV_SF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.DataService:GetLeadingFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.DataService:GetLeadingFrame(),
			},
		},
	});

	if (true) then return; end
	-- graphicVision(SF)
		-- commonGraphic(GF)

	self.JR_GV_F_CG_F_OBJ = COMCSTMGRPHFRM:Create(self.JR_GV_F_CG_F_TMP, {
		attributes = {
			Object = self.vs_hInstance.grp:CreateGraphLine(
				self.JR_GV_F_CG_F_TMP.attributes.name,
				self.JR_GV_F_CG_F_TMP.attributes.parent,
				self.JR_GV_F_CG_F_TMP.elements.anchor.point,
				self.JR_GV_F_CG_F_TMP.elements.anchor.relativePoint,
				self.JR_GV_F_CG_F_TMP.elements.anchor.offsetX,
				self.JR_GV_F_CG_F_TMP.elements.anchor.offsetY,
				800,		-- NOTE: все равно заменятся
				400
				--GGM.GUI.DataService:GetFrameWidth(),
				--GGM.GUI.DataService:GetFrameHeight()
			),
		},
		elements = {
			--size = {
				--width = GGM.GUI.DataService:GetFrameWidth(),
				--height = GGM.GUI.DataService:GetFrameHeight(),
			--},
			anchor = {
				relativeTo = self.JR_GV_F_CG_F_TMP.attributes.parent,
			},
		},
	});
end


-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- graphicVision(F)

	GGF.OuterInvoke(self.JR_GV_SF_OBJ, "Adjust");

	if (true) then return; end
	-- graphicVision(F)
		-- commonGraphic(F)

	self.JR_GV_F_CG_F_OBJ:Adjust();
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------