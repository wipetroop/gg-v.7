local This = GGF.ModuleGetWrapper("GUI-5-3-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- api
-- Смена видимости основного фрейма
function This:setHidden(in_vis)
	GGF.OuterInvoke(self.JR_GV_SF_OBJ, "SetHidden", in_vis);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Взятие высоты основной панели
function This:getHeight()
	return GGF.OuterInvoke(self.JR_GV_SF_OBJ, "GetHeight");
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- Проверка возможности отрисовки графиков
function This:checkGraphicPossibility()
	if (self:checkDataFiltering()
		and (self.vs_menuSelector.logClusterTitle ~= "")
		and ((self:checkList(self.vs_menuSelector.gsettings.avgList))
		or (self:checkList(self.vs_menuSelector.gsettings.summList)))) then
		This:DataDraw(self.vs_menuSelector);
		return;
	end
	This:DataDrop();
end


-- Каст цвета в спец тип для графика
function This:toGraphicColor(in_color)
	return {in_color.R, in_color.G, in_color.B, in_color.A};
end


-- api
-- Установка линейных размеров дочерних элементов
function This:adjustSize(in_width, in_height)
	local MSC_CONFIG = self:getConfig();
	GGF.OuterInvoke(self.JR_GV_F_OBJ, "SetWidth", in_width);
	GGF.OuterInvoke(self.JR_GV_F_OBJ, "SetHeight", in_height);

	self.JR_GV_F_CG_F_OBJ:SetSize({
		width = in_width - 2*math.abs(MSC_CONFIG.graphicOffsetX),
		height = in_height - math.abs(MSC_CONFIG.graphicOffsetY) - math.abs(MSC_CONFIG.graphicOffsetX),
	});
end


-- api
-- Функция отрисовки графика
function This:dataDraw(in_selector)
	local trgData = in_selector.logClusterData.parameterUniversalMasterTable;
	local assColor = self.cs_lineColor;
	local stt = in_selector.gsettings;

	local assigner = {
		average = {
			["AEN"] = {
				data = trgData.AEN,
				color = self:toGraphicColor(assColor.AEN),
			},
			["AKD"] = {
				data = trgData.AKD,
				color = self:toGraphicColor(assColor.AKD),
			},
			["ADD"] = {
				data = trgData.ADD,
				color = self:toGraphicColor(assColor.ADD),
			},
			["AMD"] = {
				data = trgData.AMD,
				color = self:toGraphicColor(assColor.AMD),
			},
		},
		summary = {
			["SEN"] = {
				data = trgData.SEN,
				color = self:toGraphicColor(assColor.SEN),
			},
			["SKD"] = {
				data = trgData.SKD,
				color = self:toGraphicColor(assColor.SKD),
			},
			["SDD"] = {
				data = trgData.SDD,
				color = self:toGraphicColor(assColor.SDD),
			},
			["SMD"] = {
				data = trgData.SMD,
				color = self:toGraphicColor(assColor.SMD),
			},
		},
	};

	local maxY = 0;

	if (stt.statList["avg"]) then
		for token,bool in pairs(stt.avgList) do
			for dindex, data in pairs(assigner.average[token].data) do
				if (data[2] > maxY) then
					maxY = data[2];
				end
			end
		end
	end


	if (stt.statList["summ"]) then
		for token,bool in pairs(stt.summList) do
			for dindex, data in pairs(assigner.summary[token].data) do
				if (data[2] > maxY) then
					maxY = data[2];
				end
			end
		end
	end

	-- Тут подгонная геометрия
	local spaceX = math.floor(trgData.AEN[#trgData.AEN][1]/15);
	local spaceY = math.floor(maxY/10);
	if (spaceX == 0) then
		spaceX = 1;
	end
	if (spaceY == 0) then
		spaceY = 1;
	end
	--print(spaceX, spaceY);

	self.JR_GV_F_CG_F_OBJ:SetGridSpacing({
		X = spaceX,
		Y = spaceY,
	});
	self.JR_GV_F_CG_F_OBJ:SetXAxisInterval({
		start = 0,
		finish = trgData.AEN[#trgData.AEN][1],
	});
	self.JR_GV_F_CG_F_OBJ:SetYAxisInterval({
		start = 0,
		finish = maxY,
	});

	self.JR_GV_F_CG_F_OBJ:ResetData();
	print(#trgData.AEN);
	if (stt.statList["avg"]) then
		for token,bool in pairs(stt.avgList) do
			self.JR_GV_F_CG_F_OBJ:AddDataSeries(assigner.average[token].data, assigner.average[token].color, false);
		end
	end

	if (stt.statList["summ"]) then
		for token,bool in pairs(stt.summList) do
			self.JR_GV_F_CG_F_OBJ:AddDataSeries(assigner.summary[token].data, assigner.summary[token].color, false);
		end
	end
end


-- api
-- Функция сброса данных графика
function This:dataDrop()
	self.JR_GV_F_CG_F_OBJ:ResetData();
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------