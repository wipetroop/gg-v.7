local This = GGF.ModuleCreateWrapper("GUI-5-3-2", false, {
	frameWidth = function() return GGM.GUI.ParameterStorage:GetDSDFFrameWidth() end,
	frameHeight = function() return GGM.GUI.ParameterStorage:GetDSDFFrameHeight() end,

	buttonWidth = function() return GGM.GUI.ParameterStorage:GetButtonWidth() end,
	buttonHeight = function() return GGM.GUI.ParameterStorage:GetButtonHeight() end,

	titleWidth = function() return GGM.GUI.ParameterStorage:GetTitleWidth() end,
	titleHeight = function() return GGM.GUI.ParameterStorage:GetTitleHeight() end,

	titleOffsetX = function() return GGM.GUI.ParameterStorage:GetTitleOffsetX() end,
	titleOffsetY = function() return GGM.GUI.ParameterStorage:GetTitleOffsetY() end,

	tableFontHeight = function() return GGM.GUI.ParameterStorage:GetTableFontHeight() end,
	tableRowCount = 13,

	tablePoint = "TOP",
	tableRelativePoint = "TOP",

	tableOffsetX = 5,
	tableOffsetY = -40,
	tableGapX = 10,

	dlgGap = function() return GGM.GUI.ParameterStorage:GetDlgGap() end,

	dlgButtonOffsetX = 0,
	dlgButtonOffsetY = 20,

	segregateBackdrop = function() return GGM.GUI.ParameterStorage:GetSegregateFrameBackdrop() end,

	on_cell_entered = function(...) GGM.GUI.CommonService:OnCommonTableCellEnter()(...) end,
	on_cell_leaved = function(...) GGM.GUI.CommonService:OnCommonTableCellLeave()(...) end,
});

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------

-- Типы промахов
This.cs_missTypes = {
	["ABSORB"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "ABSORB" .. GGF.FlushStringColor() .. "\n",
		signature = "ABSORB",
		data = {},
	},
	["BLOCK"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "BLOCK" .. GGF.FlushStringColor() .. "\n",
		signature = "BLOCK",
		data = {},
	},
	["DEFLECT"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "DEFLECT" .. GGF.FlushStringColor() .. "\n",
		signature = "DEFLECT",
		data = {},
	},
	["DODGE"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "DODGE" .. GGF.FlushStringColor() .. "\n",
		signature = "DODGE",
		data = {},
	},
	["EVADE"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "EVADE" .. GGF.FlushStringColor() .. "\n",
		signature = "EVADE",
		data = {},
	},
	["IMMUNE"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "IMMUNE" .. GGF.FlushStringColor() .. "\n",
		signature = "IMMUNE",
		data = {},
	},
	["MISS"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "MISS" .. GGF.FlushStringColor() .. "\n",
		signature = "MISS",
		data = {},
	},
	["PARRY"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "PARRY" .. GGF.FlushStringColor() .. "\n",
		signature = "PARRY",
		data = {},
	},
	["REFLECT"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "REFLECT" .. GGF.FlushStringColor() .. "\n",
		signature = "REFLECT",
		data = {},
	},
	["RESIST"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "RESIST" .. GGF.FlushStringColor() .. "\n",
		signature = "RESIST",
		data = {},
	},
};

This.cs_filterSet = {
	-- NOTE: ряд 1: доступен всегда(all по умолчанию)(all нельзя добавить, если есть какой-то другой фильтр)
	-- NOTE: ряды 2,3: появляются только на эвентах с доп. типами(all по умолчанию)
	["AURA"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "AURA" .. GGF.FlushStringColor() .. "\n",
		signature = GGM.GUI.DataFilter.cl_unusedFieldData,
		data = {
			["APPLIED"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_AURA_APPLIED" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_AURA_APPLIED",
				data = {},
			},
			["APPLIED_DOSE"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_AURA_APPLIED_DOSE" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_AURA_APPLIED_DOSE",
				data = {},
			},
			["REMOVED"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_AURA_REMOVED" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_AURA_REMOVED",
				data = {},
			},
			["REMOVED_DOSE"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_AURA_REMOVED_DOSE" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_AURA_REMOVED_DOSE",
				data = {},
			},
			["REFRESH"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_AURA_REFRESH" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_AURA_REFRESH",
				data = {},
			},
		},
	},
	["BRAKE"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "BRAKE" .. GGF.FlushStringColor() .. "\n",
		signature = GGM.GUI.DataFilter.cl_unusedFieldData,
		data = {
			["BROKEN"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_AURA_BROKEN" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_AURA_BROKEN",
				data = {},
			},
			["BROKEN_SPELL"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_AURA_BROKEN_SPELL" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_AURA_BROKEN_SPELL",
				data = {},
			},
		},
	},
	["CAST"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "CAST" .. GGF.FlushStringColor() .. "\n",
		signature = GGM.GUI.DataFilter.cl_unusedFieldData,
		data = {
			["START"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_CAST_START" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_CAST_START",
				data = {},
			},
			["SUCCESS"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_CAST_SUCCESS" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_CAST_SUCCESS",
				data = {},
			},
			["FAILED"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_CAST_FAILED" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_CAST_FAILED",
				data = {},
			},
		},
	},
	["CREATE"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_CREATE" .. GGF.FlushStringColor() .. "\n",
		signature = "SPELL_CREATE",
		data = {},
	},
	["DAMage"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "DAMage" .. GGF.FlushStringColor() .. "\n",
		signature = GGM.GUI.DataFilter.cl_unusedFieldData,
		data = {
			["SWING"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SWING_DAMage" .. GGF.FlushStringColor() .. "\n",
				signature = "SWING_DAMage",
				data = {},
			},
			["RANGE"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "RANGE_DAMage" .. GGF.FlushStringColor() .. "\n",
				signature = "RANGE_DAMage",
				data = {},
			},
			["SPELL"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_DAMage" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_DAMage",
				data = {},
			},
			["SWING_PERIODIC"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SWING_PERIODIC_DAMage" .. GGF.FlushStringColor() .. "\n",
				signature = "SWING_PERIODIC_DAMage",
				data = {},
			},
			["SPELL_PERIODIC"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_PERIODIC_DAMage" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_PERIODIC_DAMage",
				data = {},
			},
			["ENVIRONMENTAL"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "ENVIRONMENTAL_DAMage" .. GGF.FlushStringColor() .. "\n",
				signature = "ENVIRONMENTAL_DAMage",
				data = {},
			},
		},
	},
	["DEATH"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "DEATH" .. GGF.FlushStringColor() .. "\n",
		signature = GGM.GUI.DataFilter.cl_unusedFieldData,
		data = {
			["UNIT_DIED"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "UNIT_DIED" .. GGF.FlushStringColor() .. "\n",
				signature = "UNIT_DIED",
				data = {},
			},
			["UNIT_DESTROYED"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "UNIT_DESTROYED" .. GGF.FlushStringColor() .. "\n",
				signature = "UNIT_DESTROYED",
				data = {},
			},
			["PARTY_KILL"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "PARTY_KILL" .. GGF.FlushStringColor() .. "\n",
				signature = "PARTY_KILL",
				data = {},
			},
		},
	},
	["DISPEL"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "DISPEL" .. GGF.FlushStringColor() .. "\n",
		signature = GGM.GUI.DataFilter.cl_unusedFieldData,
		data = {
			["SUCCESS"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_DISPEL" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_DISPEL",
				data = {},
			},
			["FAILED"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_DISPEL_FAILED" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_DISPEL_FAILED",
				data = {},
			},
		},
	},
	["DRAIN"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_DRAIN" .. GGF.FlushStringColor() .. "\n",
		signature = "SPELL_DRAIN",
		data = {},
	},
	["ENCHANT"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "ENCHANT" .. GGF.FlushStringColor() .. "\n",
		signature = GGM.GUI.DataFilter.cl_unusedFieldData,
		data = {
			["APPLIED"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "ENCHANT_APPLIED" .. GGF.FlushStringColor() .. "\n",
				signature = "ENCHANT_APPLIED",
				data = {},
			},
			["REMOVED"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "ENCHANT_REMOVED" .. GGF.FlushStringColor() .. "\n",
				signature = "ENCHANT_REMOVED",
				data = {},
			},
		},
	},
	["ENERGIZE"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "ENERGIZE" .. GGF.FlushStringColor() .. "\n",
		signature = GGM.GUI.DataFilter.cl_unusedFieldData,
		data = {
			["STANDARD"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_ENERGIZE" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_ENERGIZE",
				data = {},
			},
			["PERIODIC"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_PERIODIC_ENERGIZE" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_PERIODIC_ENERGIZE",
				data = {},
			},
		},
	},
	["HEAL"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "HEAL" .. GGF.FlushStringColor() .. "\n",
		signature = GGM.GUI.DataFilter.cl_unusedFieldData,
		data = {
			["STANDARD"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_HEAL" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_HEAL",
				data = {},
			},
			["PERIODIC"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_PERIODIC_HEAL" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_PERIODIC_HEAL",
				data = {},
			},
		},
	},
	["INTERRUPT"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_INTERRUPT" .. GGF.FlushStringColor() .. "\n",
		signature = "SPELL_INTERRUPT",
		data = {},
	},
	["LEECH"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_LEECH" .. GGF.FlushStringColor() .. "\n",
		signature = "SPELL_LEECH",
		data = {},
	},
	["MISS"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "MISS" .. GGF.FlushStringColor() .. "\n",
		signature = GGM.GUI.DataFilter.cl_unusedFieldData,
		data = {
			["SWING"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SWING_MISSED" .. GGF.FlushStringColor() .. "\n",
				signature = "SWING_MISSED",
				data = GGF.TableCopy(GGM.GUI.DataFilter.cs_missTypes),
			},
			["RANGE"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "RANGE_MISSED" .. GGF.FlushStringColor() .. "\n",
				signature = "RANGE_MISSED",
				data = GGF.TableCopy(GGM.GUI.DataFilter.cs_missTypes),
			},
			["SPELL"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_MISSED" .. GGF.FlushStringColor() .. "\n",
				signature = "SPELL_MISSED",
				data = GGF.TableCopy(GGM.GUI.DataFilter.cs_missTypes),
			},
			["SWING_PERIODIC"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SWING_PERIODIC_MISSED" .. GGF.FlushStringColor() .. "\n",
				signature = "SWING_PERIODIC_MISSED",
				data = GGF.TableCopy(GGM.GUI.DataFilter.cs_missTypes),
			},
			["SPELL_PERIODIC"] = {
				description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SWING_PERIODIC_MISSED" .. GGF.FlushStringColor() .. "\n",
				signature = "SWING_PERIODIC_MISSED",
				data = GGF.TableCopy(GGM.GUI.DataFilter.cs_missTypes),
			},
		},
	},
	["STEAL"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_STOLEN" .. GGF.FlushStringColor() .. "\n",
		signature = "SPELL_STOLEN",
		data = {},
	},
	["SUMMON"] = {
		description = GGF.StructColorToStringColor(GGD.Color.Title) .. "SPELL_SUMMON" .. GGF.FlushStringColor() .. "\n",
		signature = "SPELL_SUMMON",
		data = {},
	},
};

This.cs_eventClassTablePrototypeMap = {
	vHeader = {
		width = 40,
		justifyH = GGE.JustifyHType.Center,
	},
	columns = {
		[1] = {
			type = GGE.DataType.String,
			width = 120,
			justifyH = GGE.JustifyHType.Center,
			title = "Event Class",
			tooltip = "Event class for next step filtering",
		},
		[2] = {
			type = GGE.DataType.String,
			width = 40,
			justifyH = GGE.JustifyHType.Center,
			title = "Count",
			tooltip = "Count of current class filters",
		},
	},
};

This.cs_eventTypeTablePrototypeMap = {
	vHeader = {
		width = 40,
		justifyH = GGE.JustifyHType.Center,
	},
	columns = {
		[1] = {
			type = GGE.DataType.String,
			width = 120,
			justifyH = GGE.JustifyHType.Center,
			title = "Event Type",
			tooltip = "Type of logged event, based on blizzard classification",
		},
		[2] = {
			type = GGE.DataType.String,
			width = 40,
			justifyH = GGE.JustifyHType.Center,
			title = "Count",
			tooltip = "Count of current type filters",
		},
	},
};

This.cs_eventSubTypeTablePrototypeMap = {
	vHeader = {
		width = 40,
		justifyH = GGE.JustifyHType.Center,
	},
	columns = {
		[1] = {
			type = GGE.DataType.String,
			width = 120,
			justifyH = GGE.JustifyHType.Center,
			title = "Event Subtype",
			tooltip = "Subtype of some special types",
		},
		[2] = {
			type = GGE.DataType.String,
			width = 40,
			justifyH = GGE.JustifyHType.Center,
			title = "Count",
			tooltip = "Count of current subtype filters",
		},
	},
};

-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------

This.vs_hInstance = {
	gUi = nil,
	tbl = nil,
};

This.vs_filterSelector = {
	class = {
		name = "",
		signature = "",
	},
	type = {
		name = "",
		signature = "",
	},
	typetoken = "",		--NOTE: токен поля типа(зависит от класса)
	subtype = {
		name = "",
		signature = "",
	},
	subtypetoken = "",	--NOTE: токен поля подтипа(зависит от типа)
};

-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------