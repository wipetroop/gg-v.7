local This = GGF.ModuleGetWrapper("GUI-5-3-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.1 (Miscellaneous Variables)
function This:MPRC_MVR_INITIALIZE()
	--self.vs_hInstance.gUi = LibStub("AceGUi-3.0");
	--self.vs_hInstance.tbl = LibStub("ScrollingTable");
end


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------
	local MSC_CONFIG = self:getConfig();
	-- TODO: убрать функцию в конфиг, если это вообще возможно...
	-- NOTE: и так сойдёт...
	-- dataFilter(SF)

	self.JR_DF_SF_OBJ = GGC.StandardFrame:Create(self.JR_DF_SF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.DataService:GetFilterSettingsFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.DataService:GetFilterSettingsFrame(),
				--offsetX = function() 
					--return GGF.SwitchCase(GGF.TernExpSingle(self:GetHidden(), 0, 1) + GGF.TernExpSingle(GGM.GUI.ChatReport:GetHidden(), 0, 1) + GGF.TernExpSingle(GGM.GUI.LogFilter:GetHidden(), 0, 1),
						--{0, 0},
						--{1, 0},
						--{2, ((MSC_CONFIG.dlgGap() + GGF.OuterInvoke(self.JR_DF_SF_OBJ, "GetWidth") + GGF.TernExpSingle(GGM.GUI.ChatReport:GetHidden(), GGM.GUI.LogFilter:GetWidth(), GGM.GUI.ChatReport:GetWidth()))/2 - GGF.OuterInvoke(self.JR_DF_SF_OBJ, "GetWidth")/2)*GGF.TernExpSingle(GGM.GUI.ChatReport:GetHidden(), 1, -1)},
						--{3, -(2*MSC_CONFIG.dlgGap() + GGM.GUI.LogFilter:GetWidth() + GGF.OuterInvoke(self.JR_DF_SF_OBJ, "GetWidth") + GGM.GUI.ChatReport:GetWidth())/2 + GGM.GUI.LogFilter:GetWidth() + MSC_CONFIG.dlgGap() + GGF.OuterInvoke(self.JR_DF_SF_OBJ, "GetWidth")/2});
				--end,
			},
		},
	});


	-- dataFilter(SF)
		-- title(FS)

	self.JR_DF_SF_T_FS_OBJ = GGC.FontString:Create(self.JR_DF_SF_T_FS_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_DF_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_DF_SF_OBJ),
			},
		},
	});


	-- dataFilter(SF)
		-- eventClass(VT)

	--self.JR_DF_SF_EC_VT_OBJ = GGC.ViewTable:Create(self.JR_DF_SF_EC_VT_TMP, {
		--properties = {
			--base = {
				--parent = GGF.INS.GetObjectRef(self.JR_DF_SF_OBJ),
			--},
			--size = {
				--width = (GGF.OuterInvoke(self.JR_DF_SF_OBJ, "GetWidth") - 2*MSC_CONFIG.tableOffsetX - 2*MSC_CONFIG.tableGapX)/3,
			--},
			--data = {
				--prototype = self.cs_eventClassTablePrototypeMap,
			--},
			--anchor = {
				--relativeTo = GGF.INS.GetObjectRef(self.JR_DF_SF_OBJ),
				--offsetX = function()
					--return GGF.SwitchCase(GGF.TernExpSingle(GGF.OuterInvoke(self.JR_DF_SF_EC_VT_OBJ, "GetHidden"), 0, 1) + GGF.TernExpSingle(GGF.OuterInvoke(self.JR_DF_SF_ET_VT_OBJ, "GetHidden"), 0, 1) + GGF.TernExpSingle(GGF.OuterInvoke(self.JR_DF_SF_ES_VT_OBJ, "GetHidden"), 0, 1),
						--{0, 0},
						--{1, 0},
						--{2, - (MSC_CONFIG.tableGapX + GGF.OuterInvoke(self.JR_DF_SF_EC_VT_OBJ, "GetWidth") + GGF.TernExpSingle(GGF.OuterInvoke(self.JR_DF_SF_ET_VT_OBJ, "GetHidden"), GGF.OuterInvoke(self.JR_DF_SF_ES_VT_OBJ, "GetWidth"), GGF.OuterInvoke(self.JR_DF_SF_ET_VT_OBJ, "GetWidth")))/2 + GGF.OuterInvoke(self.JR_DF_SF_EC_VT_OBJ, "GetWidth")/2},
						--{3, - (2*MSC_CONFIG.tableGapX + GGF.OuterInvoke(self.JR_DF_SF_EC_VT_OBJ, "GetWidth") + GGF.OuterInvoke(self.JR_DF_SF_ET_VT_OBJ, "GetWidth") + GGF.OuterInvoke(self.JR_DF_SF_ES_VT_OBJ, "GetWidth"))/2 + GGF.OuterInvoke(self.JR_DF_SF_EC_VT_OBJ, "GetWidth")/2});
				--end,
			--},
		--},
	--});

	--self.JR_DF_F_ECT_F_OBJ:SetSubObject(self.JR_DF_F_ECT_F_OBJ:GetObject().frame);


	-- dataFilter(SF)
		-- eventType(VT)

	--self.JR_DF_SF_ET_VT_OBJ = GGC.ViewTable:Create(self.JR_DF_SF_ET_VT_TMP, {
		--properties = {
			--base = {
				--parent = GGF.INS.GetObjectRef(self.JR_DF_SF_OBJ),
			--},
			--size = {
				--width = (GGF.OuterInvoke(self.JR_DF_SF_OBJ, "GetWidth") - 2*MSC_CONFIG.tableOffsetX - 2*MSC_CONFIG.tableGapX)/3,
			--},
			--data = {
				--prototype = self.cs_eventTypeTablePrototypeMap,
			--},
			--anchor = {
				--relativeTo = GGF.INS.GetObjectRef(self.JR_DF_SF_OBJ),
				--offsetX = function()
					--return GGF.SwitchCase(GGF.TernExpSingle(GGF.OuterInvoke(self.JR_DF_SF_EC_VT_OBJ, "GetHidden"), 0, 1) + GGF.TernExpSingle(GGF.OuterInvoke(self.JR_DF_SF_ET_VT_OBJ, "GetHidden"), 0, 1) + GGF.TernExpSingle(GGF.OuterInvoke(self.JR_DF_SF_ES_VT_OBJ, "GetHidden"), 0, 1),
							--{0, 0},
							--{1, 0},
							--{2, ((MSC_CONFIG.tableGapX + GGF.OuterInvoke(self.JR_DF_SF_ET_VT_OBJ, "GetWidth") + GGF.TernExpSingle(GGF.OuterInvoke(self.JR_DF_SF_EC_VT_OBJ, "GetHidden"), GGF.OuterInvoke(self.JR_DF_SF_ES_VT_OBJ, "GetWidth"), GGF.OuterInvoke(self.JR_DF_SF_EC_VT_OBJ, "GetWidth")))/2 - GGF.OuterInvoke(self.JR_DF_SF_ET_VT_OBJ, "GetWidth")/2)*GGF.TernExpSingle(GGF.OuterInvoke(self.JR_DF_SF_ES_VT_OBJ, "GetHidden"), 1, -1)},
							--{3, - (2*MSC_CONFIG.tableGapX + GGF.OuterInvoke(self.JR_DF_SF_EC_VT_OBJ, "GetWidth") + GGF.OuterInvoke(self.JR_DF_SF_ET_VT_OBJ, "GetWidth") + GGF.OuterInvoke(self.JR_DF_SF_ES_VT_OBJ, "GetWidth"))/2 + GGF.OuterInvoke(self.JR_DF_SF_EC_VT_OBJ, "GetWidth") + MSC_CONFIG.tableGapX + GGF.OuterInvoke(self.JR_DF_SF_ET_VT_OBJ, "GetWidth")/2});
				--end,
			--},
		--},
	--});

	--self.JR_DF_F_ETT_F_OBJ:SetSubObject(self.JR_DF_F_ETT_F_OBJ:GetObject().frame);


	-- dataFilter(SF)
		-- eventSubType(VT)

	--self.JR_DF_SF_ES_VT_OBJ = GGC.ViewTable:Create(self.JR_DF_SF_ES_VT_TMP, {
		--properties = {
			--base = {
				--parent = GGF.INS.GetObjectRef(self.JR_DF_SF_OBJ),
			--},
			--size = {
				--width = (GGF.OuterInvoke(self.JR_DF_SF_OBJ, "GetWidth") - 2*MSC_CONFIG.tableOffsetX - 2*MSC_CONFIG.tableGapX)/3,
			--},
			--data = {
				--prototype = self.cs_eventSubTypeTablePrototypeMap,
			--},
			--anchor = {
				--relativeTo = GGF.INS.GetObjectRef(self.JR_DF_SF_OBJ),
				--offsetX = function()
					--return GGF.SwitchCase(GGF.TernExpSingle(GGF.OuterInvoke(self.JR_DF_SF_EC_VT_OBJ, "GetHidden"), 0, 1) + GGF.TernExpSingle(GGF.OuterInvoke(self.JR_DF_SF_ET_VT_OBJ, "GetHidden"), 0, 1) + GGF.TernExpSingle(GGF.OuterInvoke(self.JR_DF_SF_ES_VT_OBJ, "GetHidden"), 0, 1),
							--{0, 0},
							--{1, 0},
							--{2, (MSC_CONFIG.tableGapX + GGF.OuterInvoke(self.JR_DF_SF_ES_VT_OBJ, "GetWidth") + GGF.TernExpSingle(GGF.OuterInvoke(self.JR_DF_SF_EC_VT_OBJ, "GetHidden"), GGF.OuterInvoke(self.JR_DF_SF_ET_VT_OBJ, "GetWidth"), GGF.OuterInvoke(self.JR_DF_SF_EC_VT_OBJ, "GetWidth")))/2 - GGF.OuterInvoke(self.JR_DF_SF_ES_VT_OBJ, "GetWidth")/2},
							--{3, (2*MSC_CONFIG.tableGapX + GGF.OuterInvoke(self.JR_DF_SF_EC_VT_OBJ, "GetWidth") + GGF.OuterInvoke(self.JR_DF_SF_ET_VT_OBJ, "GetWidth") + GGF.OuterInvoke(self.JR_DF_SF_ES_VT_OBJ, "GetWidth"))/2 - GGF.OuterInvoke(self.JR_DF_SF_ES_VT_OBJ, "GetWidth")/2});
				--end,
			--},
		--},
	--});

	--self.JR_DF_F_ESTT_F_OBJ:SetSubObject(self.JR_DF_F_ESTT_F_OBJ:GetObject().frame);


	-- dataFilter(SF)
		-- add(SB)

	self.JR_DF_SF_A_SB_OBJ = GGC.StandardButton:Create(self.JR_DF_SF_A_SB_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_DF_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_DF_SF_OBJ),
			},
		},
	});
end


-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- dataFilter(SF)

	GGF.OuterInvoke(self.JR_DF_SF_OBJ, "Adjust");


	-- dataFilter(SF)
		-- title(FS)

	GGF.OuterInvoke(self.JR_DF_SF_T_FS_OBJ, "Adjust");


	-- dataFilter(SF)
		-- eventClass(VT)

	--GGF.OuterInvoke(self.JR_DF_SF_EC_VT_OBJ, "Adjust");


	-- dataFilter(SF)
		-- eventType(VT)

	--GGF.OuterInvoke(self.JR_DF_SF_ET_VT_OBJ, "Adjust");


	-- dataFilter(SF)
		-- eventSubtype(VT)

	--GGF.OuterInvoke(self.JR_DF_SF_ES_VT_OBJ, "Adjust");


	-- dataFilter(SF)
		-- add(SB)

	GGF.OuterInvoke(self.JR_DF_SF_A_SB_OBJ, "Adjust");
end


-- п.2.8 (After-load Settings)
function This:MPRC_ALS_INITIALIZE()
	if (true) then return; end
	self:updateFilterGeometry();
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------