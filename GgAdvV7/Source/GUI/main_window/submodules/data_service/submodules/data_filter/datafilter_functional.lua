local This = GGF.ModuleGetWrapper("GUI-5-3-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- api
-- Смена видимости основного фрейма
function This:setHidden(in_vis)
	GGF.OuterInvoke(self.JR_DF_SF_OBJ, "SetHidden", in_vis);
	self:adjustOffset();
	GGM.GUI.LogFilter:AdjustOffset();
	GGM.GUI.ChatReport:AdjustOffset();
	if (not in_vis) then
		--self:fillEventClassTable("");
	else
		--self:fillEventClassTable(nil);
		--self:activateAddButton(false);
	end
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Взятие ширины основного фрейма
function This:getWidth()
	return GGF.OuterInvoke(self.JR_DF_SF_OBJ, "GetWidth");
end


-- api
-- Взятие высоты основного фрейма
function This:getHeight()
	return GGF.OuterInvoke(self.JR_DF_SF_OBJ, "GetHeight");
end


-- api
-- Взятие видимости основного фрейма
function This:getHidden()
	return GGF.OuterInvoke(self.JR_DF_SF_OBJ, "GetHidden");
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Принудительная переприменение отступа по оси X
function This:adjustOffset()
	GGF.OuterInvoke(self.JR_DF_SF_OBJ, "AdjustOffsetX");
end


-- Обновление геометрии окон фильтров
function This:upDateFilterGeometry()
	GGF.OuterInvoke(self.JR_DF_F_ECT_F_OBJ, "AdjustOffsetX");
	GGF.OuterInvoke(self.JR_DF_F_ETT_F_OBJ, "AdjustOffsetX");
	GGF.OuterInvoke(self.JR_DF_F_ESTT_F_OBJ, "AdjustOffsetX");
end


-- Заполнение таблицы класса эвента
function This:fillEventClassTable(in_arg)
	if (not in_arg) then
		self:fillEventTypeTable(nil);
		self.vs_filterSelector.class.name = "";
		self.vs_filterSelector.class.signature = "";
		GGF.OuterInvoke(self.JR_DF_F_ECT_F_OBJ, "ClearSelection");
		return;
	end

	local tableData = {};
	for className,classData in pairs(self.cs_filterSet) do
		local count = GGM.GUI.DataService:CountFiltersByPattern(GGE.FilterType.Data, className);
		local datatransport = {};
		GGM.GUI.CommonService:SetTableElementData(datatransport, 1, className, GGD.Color.Neutral);
		GGM.GUI.CommonService:SetTableElementData(datatransport, 2, count, GGD.Color.Neutral);
		GGM.GUI.CommonService:SetTableElementData(datatransport, 3, classData.description, GGD.Color.Neutral);
		GGM.GUI.CommonService:SetTableElementData(datatransport, 4, classData.signature, GGD.Color.Neutral);

		tableData[#tableData + 1] = {};
		tableData[#tableData].cols = datatransport;
	end

	GGF.OuterInvoke(self.JR_DF_F_ECT_F_OBJ, "SetData", tableData);
	self:updateFilterGeometry();
end


-- Заполнение таблицы типа эвента
function This:fillEventTyPetable(in_class)
	if (not in_class) then
		GGF.OuterInvoke(self.JR_DF_F_ETT_F_OBJ, "SetHidden", true);
		self:fillEventSubTyPetable(nil);
		self.vs_filterSelector.type.name = "";
		self.vs_filterSelector.type.signature = "";
		GGF.OuterInvoke(self.JR_DF_F_ETT_F_OBJ, "ClearSelection");
		return;
	end

	local tableData = {};
	for typeName,typeData in pairs(self.cs_filterSet[in_class].data) do
		local count = GGM.GUI.DataService:CountFiltersByPattern(GGE.FilterType.Data, self.vs_filterSelector.class, typeName);
		local datatransport = {};
		GGM.GUI.CommonService:SetTableElementData(datatransport, 1, typeName, GGD.Color.Neutral);
		GGM.GUI.CommonService:SetTableElementData(datatransport, 2, count, GGD.Color.Neutral);
		GGM.GUI.CommonService:SetTableElementData(datatransport, 3, typeData.description, GGD.Color.Neutral);
		GGM.GUI.CommonService:SetTableElementData(datatransport, 4, typeData.signature, GGD.Color.Neutral);

		tableData[#tableData + 1] = {};
		tableData[#tableData].cols = datatransport;
	end

	GGF.OuterInvoke(self.JR_DF_F_ETT_F_OBJ, "SetData", tableData);
	GGF.OuterInvoke(self.JR_DF_F_ETT_F_OBJ, "SetHidden", false);
	self:updateFilterGeometry();
end


-- Заполнение таблицы подтипа эвента
function This:fillEventSubTyPetable(in_type)
	if (not in_type) then
		GGF.OuterInvoke(self.JR_DF_F_ESTT_F_OBJ, "SetHidden", true);
		self:upDateFilterGeometry();
		self.vs_filterSelector.subtype.name = "";
		self.vs_filterSelector.subtype.signature = "";
		GGF.OuterInvoke(self.JR_DF_F_ESTT_F_OBJ, "ClearSelection");
		return;
	end

	local tableData = {};
	for subtypeName,subtypeData in pairs(self.cs_filterSet[self.vs_filterSelector.class].data[in_type].data) do
		local count = GGM.GUI.DataService:CountFiltersByPattern(GGE.FilterType.Data, self.vs_filterSelector.class, self.vs_filterSelector.type, subtypeName);
		local datatransport = {};
		GGM.GUI.CommonService:SetTableElementData(datatransport, 1, subtypeName, GGD.Color.Neutral);
		GGM.GUI.CommonService:SetTableElementData(datatransport, 2, count, GGD.Color.Neutral);
		GGM.GUI.CommonService:SetTableElementData(datatransport, 3, subtypeData.description, GGD.Color.Neutral);
		GGM.GUI.CommonService:SetTableElementData(datatransport, 4, subtypeData.signature, GGD.Color.Neutral);

		tableData[#tableData + 1] = {};
		tableData[#tableData].cols = datatransport;
	end

	GGF.OuterInvoke(self.JR_DF_F_ESTT_F_OBJ, "SetData", tableData);
	GGF.OuterInvoke(self.JR_DF_F_ESTT_F_OBJ, "SetHidden", false);
	self:upDateFilterGeometry();
end


-- Смена состояния кнопки добавления фильтра
function This:activateAddButton(in_state)
	GGF.OuterInvoke(self.JR_DF_F_A_B_OBJ, "SetEnabled", in_state);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-- Обработчик изменений фильтров данных
function This:on_dataFilterCB_clicked(in_level, in_button)
	-- TODO: приделать ассерт

	if (self.vs_checkButtonList.DataFilterFrame[in_level][in_button]:GetChecked()) then

		for button,Object in pairs(self.vs_GGC_CheckButtonList.GGM.GUI.DataGGC.FilterFrame[in_level]) do
			Object:SetChecked(button == in_button);
		end

		if (in_level == "evType") then
			self.vs_menuSelector.filter.evType = in_button;

			if (in_button == "Kill") then
				self:hideDFLevel("atkType");
				self:hideDFLevel("missType");
				self:showDFLevel("defType");
			elseif (in_button == "Miss") then
				self:showDFLevel("atkType");
				self:showDFLevel("missType");
				self:showDFLevel("defType");
			elseif (in_button == "DaMage") then
				self:showDFLevel("atkType");
				self:hideDFLevel("missType");
				self:showDFLevel("defType");
			else
				return;
			end
		elseif (in_level == "atkType") then
			self.vs_menuSelector.filter.atkType = in_button;
		elseif (in_level == "missType") then
			self.vs_menuSelector.filter.missType = in_button;
		elseif (in_level == "defType") then
			self.vs_menuSelector.filter.defType = in_button;
		else
			return;
		end
	else
		--self.vs_GGC_CheckButtonList.GGM.GUI.DataGGC.FilterFrame[in_level][in_button]:SetChecked(false);

		if (in_level == "evType") then
			self.vs_menuSelector.filter.evType = "None";

			self:hideDFLevel("atkType");
			self:hideDFLevel("missType");
			self:hideDFLevel("defType");
		elseif (in_level == "atkType") then
			self.vs_menuSelector.filter.atkType = "None";
		elseif (in_level == "missType") then
			self.vs_menuSelector.filter.missType = "None";
		elseif (in_level == "defType") then
			self.vs_menuSelector.filter.defType = "None";
		else
			return;
		end
	end
	-- локер
	self:checkDataviewPossibility();
end


-- Специальный обработчик наведения на строку списка классов события
function This:on_eventClassTableF_cellEnter(in_rowFrame, in_cellFrame, in_data, in_cols, in_row, in_realrow, in_column, in_scrollingTable, ...)
	if in_data[in_realrow] == nil or in_data[in_realrow].cols == nil then
		return;
	end
	local msg = {
		value = in_data[in_realrow].cols[3].value,
	};
	GGM.GUI.CommonService:OnMouseFocusCell(in_cellFrame, msg);
end


-- Специальный обработчик нажатия на строку списка классов события
function This:on_eventClassTableF_clicked(in_rowFrame, in_cellFrame, in_data, in_cols, in_row, in_realrow, in_column, in_table, in_button, ...)
	if in_data[in_realrow] == nil or in_data[in_realrow].cols == nil then
		return;
	end

	-- NOTE: нужна задержка, т.к. выделение запаздывает по сравнению с вызовом коллбека
	C_Timer.NewTimer(0.25, function() 
		local selection = in_table:GetSelection();
		if (not selection) then
			self:fillEventTyPetable(nil);	-- NOTE: От пустого прокает зачистка
			return;
		end

		local classname = in_data[selection].cols[1].value;
		local classsignature = in_data[selection].cols[4].value;
		self.vs_filterSelector.class.name = classname;
		self.vs_filterSelector.class.signature = classsignature;
		if (COMTBLSIZE(self.cs_filterSet[classname].data) == 0) then
			-- NOTE: там подтипов нет
			self:activateAddButton(GGM.GUI.DataService:CountFiltersByPattern(GGE.FilterType.Data, classname) == 0);
			self:fillEventTyPetable(nil);
		else
			-- NOTE: на всякий пожарный отключаем кнопку
			self:activateAddButton(false);
			self:fillEventTyPetable(classname);
		end
	end);
end


-- Специальный обработчик наведения на строку списка типов события
function This:on_eventTypeTableF_cellEnter(in_rowFrame, in_cellFrame, in_data, in_cols, in_row, in_realrow, in_column, in_scrollingTable, ...)
	if in_data[in_realrow] == nil or in_data[in_realrow].cols == nil then
		return;
	end
	local msg = {
		value = in_data[in_realrow].cols[3].value,
	};
	GGM.GUI.CommonService:OnMouseFocusCell(in_cellFrame, msg);
end


-- Специальный обработчик нажатия на строку списка типов события
function This:on_eventTyPetableF_clicked(in_rowFrame, in_cellFrame, in_data, in_cols, in_row, in_realrow, in_column, in_table, in_button, ...)
	if in_data[in_realrow] == nil or in_data[in_realrow].cols == nil then
		return;
	end

	-- NOTE: нужна задержка, т.к. выделение запаздывает по сравнению с вызовом коллбека
	C_Timer.NewTimer(0.25, function() 
		local selection = in_table:GetSelection();
		if (not selection) then
			self:fillEventSubTyPetable(nil);	-- NOTE: От пустого прокает зачистка
			return;
		end

		local typename = in_data[selection].cols[1].value;
		local typesignature = in_data[selection].cols[4].value;
		self.vs_filterSelector.type.name = typename;
		self.vs_filterSelector.type.signature = typesignature;
		if (COMTBLSIZE(self.cs_filterSet[self.vs_filterSelector.class.name].data[typename].data) == 0) then
			-- NOTE: там подтипов нет
			self:activateAddButton(GGM.GUI.DataService:CountFiltersByPattern(GGE.FilterType.Data, self.vs_filterSelector.class.name, typename) == 0);
			self:fillEventSubTyPetable(nil);
		else
			-- NOTE: на всякий пожарный отключаем кнопку
			self:activateAddButton(false);
			self:fillEventSubTyPetable(typename);
		end
	end);
end


-- Специальный обработчик наведения на строку списка подтипов события
function This:on_eventSubTyPetableF_cellEnter(in_rowFrame, in_cellFrame, in_data, in_cols, in_row, in_realrow, in_column, in_scrollingTable, ...)
	if in_data[in_realrow] == nil or in_data[in_realrow].cols == nil then
		return;
	end
	local msg = {
		value = in_data[in_realrow].cols[3].value,
	};
	GGM.GUI.CommonService:OnMouseFocusCell(in_cellFrame, msg);
end


-- Специальный обработчик нажатия на строку списка подтипов события
function This:on_eventSubTyPetableF_clicked(in_rowFrame, in_cellFrame, in_data, in_cols, in_row, in_realrow, in_column, in_table, in_button, ...)
	if in_data[in_realrow] == nil or in_data[in_realrow].cols == nil then
		return;
	end

	-- NOTE: нужна задержка, т.к. выделение запаздывает по сравнению с вызовом коллбека
	C_Timer.NewTimer(0.25, function() 
		local selection = in_table:GetSelection();
		if (not selection) then
			self.vs_filterSelector.subtype.name = "";
			self.vs_filterSelector.subtype.signature = "";
			self:activateAddButton(false);
			return;
		end

		local subtypename = in_data[selection].cols[1].value;
		local subtypesignature = in_data[selection].cols[4].value;
		self.vs_filterSelector.subtype.name = subtypename;
		self.vs_filterSelector.subtype.signature = subtypesignature;
		if (COMTBLSIZE(self.cs_filterSet[self.vs_filterSelector.class.name].data[self.vs_filterSelector.type.name].data[subtypename].data) == 0) then
			-- NOTE: там подтипов нет
			self:activateAddButton(GGM.GUI.DataService:CountFiltersByPattern(GGE.FilterType.Data, self.vs_filterSelector.class.name, self.vs_filterSelector.type.name, subtypename) == 0);
		else
			ATLASSERT(false);
		end
	end);
end


-- Внешний обработчик нажатия кнопки add
function This:on_AddB_clicked()
	GGM.GUI.DataService:AddDataFilter(self.vs_filterSelector);
	-- NOTE: не менять местами!!! последняя строка зануляет селектор
	GGM.GUI.DataService:ChangeAddFilterTSState();
end


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------