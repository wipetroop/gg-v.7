local This = GGF.ModuleGetWrapper("GUI-5-3-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_dataFilterGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		dataFilter = {		-- [DF_F]
			inherit = {
				fontString = GGF.GTemplateTree(GGC.FontString, {
					title = {},			-- [T_FS]
				}),
				viewTable = GGF.GTemplateTree(GGC.ViewTable, {
					eventClass = {},	-- [EC_VT]
					eventType = {},		-- [ET_VT]
					eventSubtype = {},	-- [ES_VT]
				}),
				standardButton = GGF.GTemplateTree(GGC.StandardButton, {
					add = {},			-- [A_SB]
				}),
			},
		},
	}),
};

local DFG_SF = This.oc_dataFilterGeometry.standardFrame;
local DFG_SF_DF_FS = DFG_SF.dataFilter.inherit.fontString;
local DFG_SF_DF_VT = DFG_SF.dataFilter.inherit.viewTable;
local DFG_SF_DF_SB = DFG_SF.dataFilter.inherit.standardButton;

-- dataFilter(SF)
DFG_SF.dataFilter.object = {};
DFG_SF.dataFilter.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "DF",
		},
		miscellaneous = {
			hidden = true,
		},
		size = {
			width = function() return MSC_CONFIG.frameWidth() end,
			height = function() return MSC_CONFIG.frameHeight() end,
		},
		anchor = {
			point = GGE.PointType.TopRight,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.TopLeft,
			offsetX = 0,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Segregate,
	},
};


-- dataFilter(SF)
	-- title(FS)
DFG_SF_DF_FS.title.object = {};
DFG_SF_DF_FS.title.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "T",
		},
		miscellaneous = {
			text = "Data Filter",
			--tooltipText = "GUi_MW_F_DS_F_DF_F_T_FS",
		},
		size = {
			width = function() return MSC_CONFIG.titleWidth() end,
			height = function() return MSC_CONFIG.titleHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Top,
			offsetX = function() return MSC_CONFIG.titleOffsetX() end,
			offsetY = function() return MSC_CONFIG.titleOffsetY() end,
		},
	},
};


-- dataFilter(SF)
	-- eventClass(VT)
DFG_SF_DF_VT.eventClass.object = {};
DFG_SF_DF_VT.eventClass.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "EC",
		},
		hardware = {
			--fontHeight = function() return MSC_CONFIG.tableFontHeight() end,
			--rowCount = MSC_CONFIG.tableRowCount,
		},
		data = {},
		anchor = {
			point = MSC_CONFIG.tablePoint,
			relativeTo = nil,	-- fwd
			relativePoint = MSC_CONFIG.tableRelativePoint,
			offsetX = MSC_CONFIG.tableOffsetX,
			offsetY = MSC_CONFIG.tableOffsetY,
		},
		backdrop = GGD.Backdrop.Segregate,
		callback = {
			onCellClick = function(...) This:on_eventClassTableF_clicked(...) end,
			onCellEnter = function(...) This:on_eventClassTableF_cellEnter(...) end,
			onCellLeave = function(...) MSC_CONFIG.on_cell_leaved(...) end,
		},
	},
};


-- dataFilter(SF)
	-- eventType(VT)
DFG_SF_DF_VT.eventType.object = {};
DFG_SF_DF_VT.eventType.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "ET",
		},
		hardware = {
			--fontHeight = function() return MSC_CONFIG.tableFontHeight() end,
			--rowCount = MSC_CONFIG.tableRowCount,
		},
		data = {},
		anchor = {
			point = MSC_CONFIG.tablePoint,
			relativeTo = nil,	-- fwd
			relativePoint = MSC_CONFIG.tableRelativePoint,
			offsetX = MSC_CONFIG.tableOffsetX,
			offsetY = MSC_CONFIG.tableOffsetY,
		},
		backdrop = GGD.Backdrop.Segregate,
		callback = {
			onCellClick = function(...) This:on_eventTypeTableF_clicked(...) end,
			onCellEnter = function(...) This:on_eventTypeTableF_cellEnter(...) end,
			onCellLeave = function(...) MSC_CONFIG.on_cell_leaved(...) end,
		},
	},
};


-- dataFilter(SF)
	-- eventSubtype(VT)
DFG_SF_DF_VT.eventSubtype.object = {};
DFG_SF_DF_VT.eventSubtype.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "ES",
		},
		hardware = {
			--fontHeight = MSC_CONFIG.tableFontHeight,
			--rowCount = MSC_CONFIG.tableRowCount,
		},
		anchor = {
			point = MSC_CONFIG.tablePoint,
			relativeTo = nil,	-- fwd
			relativePoint = MSC_CONFIG.tableRelativePoint,
			offsetX = MSC_CONFIG.tableOffsetX,
			offsetY = MSC_CONFIG.tableOffsetY,
		},
		backdrop = GGD.Backdrop.Segregate,
		callback = {
			onCellClick = function(...) This:on_eventSubtypeTableF_clicked(...) end,
			onCellEnter = function(...) This:on_eventSubtypeTableF_cellEnter(...) end,
			onCellLeave = function(...) MSC_CONFIG.on_cell_leaved(...) end,
		},
	},
};


-- dataFilter(SF)
	-- add(SB)
DFG_SF_DF_SB.add.object = {};
DFG_SF_DF_SB.add.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "A",
		},
		miscellaneous = {
			text = "Add",
			--tooltipText = "GUi_MW_F_DS_F_DF_F_A_B",
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.Bottom,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = MSC_CONFIG.dlgButtonOffsetX,
			offsetY = MSC_CONFIG.dlgButtonOffsetY,
		},
		callback = {
			onClick = function(...) This:on_AddB_clicked(...) end,
		},
	},
};