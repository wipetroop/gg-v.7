local This = GGF.ModuleGetWrapper("GUI-5-3-5");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- api
-- Смена видимости основного фрейма
function This:setHidden(in_vis)
	GGF.OuterInvoke(self.JR_TV_SF_OBJ, "SetHidden", in_vis);
end


-- Установка флага включенность таблицы урона
function This:setDamageTableVisibility(in_visible)
	self.JR_TV_SF_D_TF_OBJ:SetHidden(not in_visible);
end


-- Установка данных в таблицу урона
function This:setDamageTableData(in_data)
	self.JR_TV_SF_D_TF_OBJ:SetData(in_data);
end


-- Установка флага включенность таблицы промахов
function This:setMissTableVisibility(in_visible)
	self.JR_TV_SF_M_TF_OBJ:SetHidden(not in_visible);
end


-- Установка данных в таблицу промахов
function This:setMissTableData(in_data)
	self.JR_TV_SF_M_TF_OBJ:SetData(in_data);
end


-- Установка флага включенность таблицы убийств
function This:setKillTableVisibility(in_visible)
	self.JR_TV_SF_K_TF_OBJ:SetHidden(not in_visible);
end


-- Установка данных в таблицу убийств
function This:setKillTableData(in_data)
	self.JR_TV_SF_K_TF_OBJ:SetData(in_data);
end


-- TODO: потом убрать
-- Установка значений в элемент таблицы(временная)
function This:setTableElementData(in_datArray, in_index, in_text, in_textColor, in_tooltip, in_tooltipColor)
	if (in_datArray[in_index] == nil) then
		in_datArray[in_index] = {};
	end
	local elem = in_datArray[in_index];
	elem.text = in_text;
	elem.textColor = GGF.TableCopy(in_textColor);
	elem.tooltip = in_tooltip;
	elem.tooltipColor = GGF.TableCopy(in_tooltipColor);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Взятие высоты основной панели
function This:getHeight()
	return self.JR_TV_SF_OBJ:GetHeight() + 10;--self.JR_TV_SF_U_VT_OBJ:GetHeight();
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Установка линейных размеров дочерних элементов
function This:adjustSize(in_width, in_height)
	local MSC_CONFIG = This:getConfig();
	local tableHeight = in_height - self.JR_TV_F_OBJ:GetHeight();

	self.JR_TV_F_OBJ:SetWidth(in_width);
	--self.JR_TV_F_OBJ:SetHeight(in_height);

	self.JR_TV_F_UT_F_OBJ:SetWidth(in_width);
	self.JR_TV_F_UT_F_OBJ:SetHeight(tableHeight);
end


-- Получение токена класса по idn источника
function This:getUnitClass(in_idn)
	--print(idn)
	if (in_idn == 0) or (in_idn == "N/A") then
		return self.cs_classTokens["SERVER"];
	end

	local gc, gcf, gr, grf, gs, gn, gr = GetPlayerInfoByGUiD(in_idn);

	if gcf ~= nil then
		--print(gcf);
		return self.cs_classTokens[gcf];
	end

	local _,_,uc = UnitClass(in_idn);

	if uc ~= nil then
		return uc;
	end
	--
	-- Вот не факт, что сюда уйдет	
	--if ACRDT.GGM.GUI.PlayerCluster[idn] ~= nil then
		--return classTokens[GGM.GUI.PlayerCluster[name]];
		--return ACRDT.GGM.GUI.PlayerCluster[idn];
	--end
	return self.cs_classTokens["SERVER"];
end


-- Возврат цвета стандартного текста
function This:getTextColor(in_colorToken)
	return self.cs_textColor[in_colorToken];
end


-- Возврат цвета текста класса персонажей
function This:getClassColor(in_colorToken)
	return self.cs_classColor[in_colorToken];
end


-- Расчет ширины таблицы по прототипу данных
function This:recountTableFrameWidth(in_prot)
	local summ = 0;
	for k,v in pairs(in_prot) do
		summ = summ + v["width"];
	end

	return summ;
end


-- Проверка вторичных полей(фильтрация внутри модуля логгера)
function This:checkAdditionalCriteria(in_record, in_type, in_typetoken, in_subtype, in_subtypetoken)
	-- NOTE: тип пустой, значит событие дальше не фильтерится
	if (in_type == "") then
		return true;
	end

	local typeName = ACR_RL_CL_AL_SA:GetFieldNameByToken(in_tyPetoken);
	print("chck: ", in_record, in_type, in_record[typeName].data, in_typetoken, in_subtype, in_subtypetoken);
	if (in_record[typeName].data == in_type) then
		-- NOTE: подтип появляется крайне редко
		if (in_subtype == "") then
			return true;
		end
		local subtypeName = ACR_RL_CL_AL_SA:GetFieldNameByToken(in_subtypetoken);
		return (in_record[subtypeName] == in_subtype);
	else
		return false;
	end
end


-- Добавление полей в прототип вывода
function This:addPrototypeFieldSet(in_prototype, in_logger)
	local lStructure = in_logger:GetLogRecordStructure();
	local map = in_prototype.map;
	for fieldName,fieldMeta in pairs(lStructure) do
		-- NOTE: отбрасываем флаги и системные поля
		if (type(fieldMeta) == 'table') then
			local priority = ACR_RL_CL_AL_SA:GetFieldPriority(fieldName);
			local ptLength = #map;
			local inserted = false;
			for ptInd=1,ptLength do
				if (not inserted) then
					if (ACR_RL_CL_AL_SA:GetFieldPriority(map[ptInd].name) == priority) then
						-- NOTE: одинакового приоритета не может у разных полей
						inserted = true;
						-- NOTE: можно бы и return, но там еще цикл выше есть
					elseif (ACR_RL_CL_AL_SA:GetFieldPriority(map[ptInd].name) > priority) then
						for rptInd=ptLength+1,ptInd+1,-1 do
							map[rptInd] = GGF.TableCopy(map[rptInd - 1]);
						end
						map[ptInd] = {
							name = fieldName,
							width = self.cn_DefaultRowWidth,
							align = self.cl_DefaultRowAlign,
						};
						inserted = true;
					end
				end
			end
			if (not inserted) then
				map[#map+1] = {
					type = GGE.DataType.String,
					name = fieldName,
					width = self.cn_DefaultRowWidth,
					align = self.cl_DefaultRowAlign,
				};
			end
		end
	end
end


-- Сборка(фильтрация) данных для таблицы из лог кластера
function This:dumpConstructor(in_logList, in_filterList)
	if (COMTBLSIZE(in_logList) == 0 or COMTBLSIZE(in_filterList) == 0) then
		return false;
	end

	local dataPrototype = {
		rowCount = 40,
		map = {},
	};
	local targetData = {};
	local dtIdx = 1;
	for filterIdx,filterData in ipairs(in_filterList) do
		-- NOTE: список полей зависит только от класса(тип и подтип не влияют)
		self:addPrototypeFieldSet(dataPrototype, self.md_loggerList[filterData.class.name].logger());
		for logIdx,logData in ipairs(in_logList) do
			local loggerData = self.md_loggerList[filterData.class.name].logger();
			local log, serializer = loggerData:GetLogByToken(logData.token);
			for index,logRecord in ipairs(log.data) do
				local record = serializer(logRecord);
				if (self:checkAdditionalCriteria(record, filterData.type.signature, "LR_E_TYPE", filterData.subtype.signature, "LR_E_EXTP")) then
					targetData[dtIdx] = GGF.TableCopy(record);
					dtIdx = dtIdx + 1;
				end
			end
		end
	end

	local metaInf = {
		logTokenList = "",
		filterTokenList = "",
	};
	-- NOTE: временно отключено
	--for filterIdx,filterData in ipairs(in_filterList) do
		--metaInf.filterTokenList = metaInf.filterTokenList .. "[" .. filterData.token .. "]";
	--end
	for logIdx,logData in ipairs(in_logList) do
		metaInf.logTokenList = metaInf.logTokenList .. "[" .. logData.token .. "]";
	end
	return true, metaInf, dataPrototype, targetData;
end


-- Заполнение логфилда с частью основных пересчетов
function This:updateTableData()
	local expColCount = 0;

	local tableData = {};

	local filterList = GGM.GUI.DataService:GetActiveFilterList();
	local logList = GGM.GUI.DataService:GetActiveLogList();
	local datFlg, metaInf, dataPrototype, dataSector = self:dumpConstructor(logList, filterList);

	if (not datFlg) then
		self:dataDrop();
		return;
	else
		--self:createTable(dataPrototype);
		self.JR_TV_F_UT_F_OBJ:SetDataPrototype(dataPrototype.map);
		-- TODO: допилить!!!
		for idx,cortege in ipairs(dataSector) do
			local datatransport = {};
			tableData[idx] = {};

			for fieldIdx,fieldMeta in ipairs(dataPrototype.map) do
				self:setTableElementData(datatransport, fieldIdx, GGF.TernExpSingle(cortege[fieldMeta.name] ~= nil, cortege[fieldMeta.name].data, self.cl_unusedFieldData), GGF.TernExpSingle(cortege[fieldMeta.name] ~= name, cortege[fieldMeta.name].color, GGD.Color.Neutral));
			end

			tableData[idx].cols = datatransport;
		end

		--self.JR_TV_F_UT_F_OBJ:SetHidden(false);
		self.JR_TV_F_UT_F_OBJ:SetData(tableData);
		return;
	end
end


-- Сброс данных таблицы с частью основных пересчетов
function This:dataDrop()
	if (not self.JR_TV_F_UT_F_OBJ) then
		return;
	end
	self.JR_TV_F_UT_F_OBJ:SetData({});
	--self.JR_TV_F_UT_F_OBJ:SetHidden(true);
	-- NOTE: сбрасываем таблицу полностью, т.к. при смене набора колонок необходимо все пересоздавать
	self.JR_TV_F_UT_F_OBJ = nil;
end


-- Создание таблицы с данными
function This:createTable(in_prototype)
	-- tableVision(F)
		-- universalTable(F)

	--self.JR_TV_F_UT_F_OBJ = COMCSTMACETBLWRP:Create(self.JR_TV_F_UT_F_TMP, {
		--attributes = {
			--Object = self.vs_hInstance.tbl:CreateST(
				--in_prototype.map,
				--in_prototype.rowCount,
				--self.JR_TV_F_UT_F_TMP.attributes.fontHeight(),
				--self.JR_TV_F_UT_F_TMP.attributes.highlight,
				--self.JR_TV_F_UT_F_TMP.attributes.parent
			--),
		--},
		--elements = {
			--anchor = {
				--relativeTo = self.JR_TV_F_UT_F_TMP.attributes.parent,
			--},
		--},
	--});

	--self.JR_TV_F_UT_F_OBJ:SetSubObject(self.JR_TV_F_UT_F_OBJ:GetObject().frame);

	--self.JR_TV_F_UT_F_OBJ:Adjust();

	--self.JR_TV_F_UT_B_OBJ:SetEnabled(true);
	--self.JR_TV_F_UT_F_OBJ:SetHidden(false);
end


-- Временная фукнция тестирования
function This:testTable()
	local prototype = {
		vHeader = {
			width = 40,
			justifyH = GGE.JustifyHType.Center,
		},
		columns = {
			[1] = {
				type = GGE.DataType.String,
				width = 60,
				justifyH = GGE.JustifyHType.Center,
				title = "index",
				tooltip = "index tooltip",
			},
			[2] = {
				type = GGE.DataType.String,
				width = 100,
				justifyH = GGE.JustifyHType.Center,
				title = "test 2",
				tooltip = "test 2 tooltip",
			},
			[3] = {
				type = GGE.DataType.String,
				width = 55,
				justifyH = GGE.JustifyHType.Center,
				title = "test 3",
				tooltip = "test 3 tooltip",
			},
			[4] = {
				type = GGE.DataType.String,
				width = 55,
				justifyH = GGE.JustifyHType.Center,
				title = "test 4",
				tooltip = "test 4 tooltip",
			},
			[5] = {
				type = GGE.DataType.String,
				width = 55,
				justifyH = GGE.JustifyHType.Center,
				title = "test 5",
				tooltip = "test 5 tooltip",
			},
			[6] = {
				type = GGE.DataType.String,
				width = 55,
				justifyH = GGE.JustifyHType.Center,
				title = "test 6",
				tooltip = "test 6 tooltip",
			},
			[7] = {
				type = GGE.DataType.String,
				width = 55,
				justifyH = GGE.JustifyHType.Center,
				title = "test 7",
				tooltip = "test 7 tooltip",
			},
			[8] = {
				type = GGE.DataType.String,
				width = 55,
				justifyH = GGE.JustifyHType.Center,
				title = "test 8",
				tooltip = "test 8 tooltip",
			},
			[9] = {
				type = GGE.DataType.String,
				width = 55,
				justifyH = GGE.JustifyHType.Center,
				title = "test 9",
				tooltip = "test 9tooltip",
			},
			[10] = {
				type = GGE.DataType.String,
				width = 55,
				justifyH = GGE.JustifyHType.Center,
				title = "test 10",
				tooltip = "test 10 tooltip",
			},
			[11] = {
				type = GGE.DataType.String,
				width = 55,
				justifyH = GGE.JustifyHType.Center,
				title = "test 11",
				tooltip = "test 11 tooltip",
			},
			[12] = {
				type = GGE.DataType.String,
				width = 55,
				justifyH = GGE.JustifyHType.Center,
				title = "test 12",
				tooltip = "test 12 tooltip",
			},
			[13] = {
				type = GGE.DataType.String,
				width = 55,
				justifyH = GGE.JustifyHType.Center,
				title = "test 13",
				tooltip = "test 13 tooltip",
			},
			[14] = {
				type = GGE.DataType.String,
				width = 55,
				justifyH = GGE.JustifyHType.Center,
				title = "test 14",
				tooltip = "test 14 tooltip",
			},
			[15] = {
				type = GGE.DataType.String,
				width = 55,
				justifyH = GGE.JustifyHType.Center,
				title = "test 15",
				tooltip = "test 15 tooltip",
			},
			[16] = {
				type = GGE.DataType.String,
				width = 55,
				justifyH = GGE.JustifyHType.Center,
				title = "test 16",
				tooltip = "test 16 tooltip",
			},
			[17] = {
				type = GGE.DataType.String,
				width = 55,
				justifyH = GGE.JustifyHType.Center,
				title = "test 17",
				tooltip = "test 17 tooltip",
			},
			[18] = {
				type = GGE.DataType.String,
				width = 55,
				justifyH = GGE.JustifyHType.Center,
				title = "test 18",
				tooltip = "test 18 tooltip",
			},
			[19] = {
				type = GGE.DataType.String,
				width = 55,
				justifyH = GGE.JustifyHType.Center,
				title = "test 19",
				tooltip = "test 19 tooltip",
			},
		},
	};

	local data = {
		vHeader = {
			textColor = GGD.Color.Green,
			tooltipColor = GGD.Color.Red,
		},
		fields = {},
	};

	for i=1,20 do
	--for i=1,100 do
		local fields = {};
		for j=1,#prototype.columns do
			fields[j] = {
				text = "v_"..tostring(i).."_"..tostring(j),
				textColor = GGD.Color.Green,
				tooltip = "v_"..tostring(i).."_"..tostring(j).."t",
				tooltipColor = GGD.Color.Red
			};
		end
		data.fields[i] = GGF.TableCopy(fields);
	end

	GGF.OuterInvoke(self.JR_TV_SF_U_CT_OBJ, "SetPrototype", prototype);
	GGF.OuterInvoke(self.JR_TV_SF_U_CT_OBJ, "SetDataSet", data);
	GGF.OuterInvoke(self.JR_TV_SF_U_CT_OBJ, "Launch");
end

-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-- Внешний обработчик нажатия кнопки update table
function This:on_updateTableB_clicked()
	self:updateTableData();
end


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------