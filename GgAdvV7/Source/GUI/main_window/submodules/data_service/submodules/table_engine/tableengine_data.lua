local This = GGF.ModuleCreateWrapper("GUI-5-3-5", false, {
	frameWidth = function() return GGM.GUI.ParameterStorage:GetMWFrameWidth() end,
	frameHeight = function() return GGM.GUI.ParameterStorage:GetDSTEFrameHeight() end,

	buttonWidth = function() return GGM.GUI.ParameterStorage:GetButtonWidth() end,
	buttonHeight = function() return GGM.GUI.ParameterStorage:GetButtonHeight() end,

	tableFontHeight = function() return GGM.GUI.ParameterStorage:GetTableFontHeight() end,
	tableRowCount = 39, -- а че не 40?

	tablePoint = GGE.PointType.Top,
	tableRelativePoint = GGE.PointType.Bottom,

	tableHeight = 440,

	segregateBackdrop = function() return GGM.GUI.ParameterStorage:GetSegregateFrameBackdrop() end,

	on_cell_entered = function(...) GGM.GUI.ParameterStorage:GetCellEnterCallback()(...) end,
	on_cell_leaved = function(...) GGM.GUI.ParameterStorage:GetCellLeaveCallback()(...) end,
});

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Symbolic links
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------

This.cn_DefaultRowWidth = 75;

-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------

This.cl_DefaultRowAlign = "CENTER";

-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------

--
This.cs_definitenessFilterType = {
	["Defined"] = "defined",
	["Undefined"] = "Undefined",
};

-- Токены различных классов
This.cs_classTokens = {
	["Warrior"] = 1,
	["Paladin"] = 2,
	["Hunter"] = 3,
	["Rogue"] = 4,
	["Priest"] = 5,
	["DeathKnight"] = 6,
	["Shaman"] = 7,
	["Mage"] = 8,
	["Warlock"] = 9,
	["Monk"] = 10,
	["Druid"] = 11,
	["DemonHunter"] = 12,
	["SERVER"] = 13,
};

-- Интерпретатор структуры лога из Announcer
This.cs_logInterpret = {
	damage = {
		atkType = {
			Swing = "SWING_DAMAGE",
			Range = "RANGE_DAMAGE",
			Spell = "SPELL_DAMAGE",
			SpellPeriodic = "SPELL_PERIODIC_DAMAGE",
			Environmental = "ENVIRONMENTAL_DAMAGE",
		},
	},
	miss = {
		atkType = {
			Swing = "SWING_MISSED",
			Range = "RANGE_MISSED",
			Spell = "SPELL_MISSED",
			SpellPeriodic = "SPELL_PERIODIC_MISSED",
			Environmental = "ENVIRONMENTAL_MISSED",
		},
		missType = {
			Absorb = "ABSORB",
			Block = "BLOCK",
			Deflect = "DEFLECT",
			Dodge = "DODGE",
			Evade = "EVADE",
			Immune = "IMMUNE",
			Miss = "MISS",
			Parry = "PARRY",
			Reflect = "REFLECT",
			Resist = "RESIST",
		},
	},
	count = {
		Full = "f",
		Signleton = "s",
	},
};

-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable strings
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------

This.vs_hInstance = {
	tbl = nil,
};

-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------