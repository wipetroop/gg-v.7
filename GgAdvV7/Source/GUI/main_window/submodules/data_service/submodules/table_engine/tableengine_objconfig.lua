local This = GGF.ModuleGetWrapper("GUI-5-3-5");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_tableEngineGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		tableVision = {			-- [TV_SF]
			inherit = {
				chargedTable = GGF.GTemplateTree(GGC.ChargedTable, {
					universal = {},		-- [U_CT]
				}),
			},
		},
	}),
};

local TEG_SF = This.oc_tableEngineGeometry.standardFrame;
local TEG_SF_TV_CT = TEG_SF.tableVision.inherit.chargedTable;

-- tableVision(F)
TEG_SF.tableVision.object = {};
TEG_SF.tableVision.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "TV",
		},
		size = {
			width = function() return MSC_CONFIG.frameWidth() end,
			height = function() return MSC_CONFIG.frameHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Segregate,
	},
};


-- tableVision(F)
	-- universal(CT)
TEG_SF_TV_CT.universal.object = {};
TEG_SF_TV_CT.universal.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "U",
		},
		hardware = {
			miscellaneous = {
				--fontHeight = function() return MSC_CONFIG.tableFontHeight() end,
				--rowCount = MSC_CONFIG.tableRowCount,
				selection = GGE.SelectionType.Row,
			},
		},
		size = {
			width = function() return MSC_CONFIG.frameWidth() end,
			height = function() return MSC_CONFIG.tableHeight end,
		},
		anchor = {
			point = MSC_CONFIG.tablePoint,
			relativeTo = nil,	-- fwd
			relativePoint = MSC_CONFIG.tableRelativePoint,
			offsetX = 0,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Segregate,
		callback = {
			--onCellEnter = function(...) MSC_CONFIG.on_cell_enteRed(...) end,
			--onCellLeave = function(...) MSC_CONFIG.on_cell_leaved(...) end,
			onUpdateButtonClick = function(...) This:on_upDateTableB_clicked(...) end,
		},
	},
};