local This = GGF.ModuleGetWrapper("GUI-5-3-5");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.3 (MDT Check)
function This:MS_CHECK_MDT_0()
	return 1;
end


-------------------------------------------------------------------------------
-- "multitudinous data" block
-------------------------------------------------------------------------------


-- Предопределние структуры таблиц
This.md_prototypeMap = {
	NeutralTable = { 
		{ ["name"] = "GDU_HM_1",  ["width"] = 75, ["align"] = "CENTER" }, -- [1]
		{ ["name"] = "GDU_HM_2",  ["width"] = 75, ["align"] = "CENTER" }, -- [2]
		{ ["name"] = "GDU_HM_3",  ["width"] = 75, ["align"] = "CENTER" }, -- [3]
		{ ["name"] = "GDU_HM_4",  ["width"] = 75, ["align"] = "CENTER" }, -- [4]
		{ ["name"] = "GDU_HM_5",  ["width"] = 75, ["align"] = "CENTER" }, -- [5]
		{ ["name"] = "GDU_HM_6",  ["width"] = 75, ["align"] = "CENTER" }, -- [6]
		{ ["name"] = "GDU_HM_7",  ["width"] = 75, ["align"] = "CENTER" }, -- [7]
		{ ["name"] = "GDU_HM_8",  ["width"] = 75, ["align"] = "CENTER" }, -- [8]
		{ ["name"] = "GDU_HM_9",  ["width"] = 75, ["align"] = "CENTER" }, -- [9]
		{ ["name"] = "GDU_HM_10", ["width"] = 75, ["align"] = "CENTER" }, -- [10]
	},
	daMageTable = {
		{ ["name"] = "No",          ["width"] = 35,  ["align"] = "CENTER" }, -- [1]
		{ ["name"] = "Time",        ["width"] = 125, ["align"] = "CENTER" }, -- [2]
		{ ["name"] = "Source",      ["width"] = 100, ["align"] = "CENTER" }, -- [3]
		{ ["name"] = "Target",      ["width"] = 100, ["align"] = "CENTER" }, -- [4]
		{ ["name"] = "Type",        ["width"] = 70,  ["align"] = "CENTER" }, -- [5]
		{ ["name"] = "Spell",       ["width"] = 200, ["align"] = "CENTER" }, -- [6]
		{ ["name"] = "Spell ID",    ["width"] = 80,  ["align"] = "CENTER" }, -- [7]
		{ ["name"] = "DaMage",      ["width"] = 60,  ["align"] = "CENTER" }, -- [8]
		{ ["name"] = "Overkill",    ["width"] = 60,  ["align"] = "CENTER" }, -- [9]
		{ ["name"] = "Critical",    ["width"] = 50,  ["align"] = "CENTER" }, -- [10]
		{ ["name"] = "Multistrike", ["width"] = 60,  ["align"] = "CENTER" }, -- [11]
	},
	missTable = {
		{ ["name"] = "No",          ["width"] = 35,  ["align"] = "CENTER" }, -- [1]
		{ ["name"] = "Time",        ["width"] = 125, ["align"] = "CENTER" }, -- [2]
		{ ["name"] = "Source",      ["width"] = 100, ["align"] = "CENTER" }, -- [3]
		{ ["name"] = "Target",      ["width"] = 100, ["align"] = "CENTER" }, -- [4]
		{ ["name"] = "Miss Type",   ["width"] = 85,  ["align"] = "CENTER" }, -- [5]
		{ ["name"] = "Type",        ["width"] = 70,  ["align"] = "CENTER" }, -- [6]
		{ ["name"] = "Spell Name",  ["width"] = 200, ["align"] = "CENTER" }, -- [7]
		{ ["name"] = "Spell ID",    ["width"] = 80,  ["align"] = "CENTER" }, -- [8]
		{ ["name"] = "DaMage",      ["width"] = 60,  ["align"] = "CENTER" }, -- [9]
		{ ["name"] = "Multistrike", ["width"] = 60,  ["align"] = "CENTER" }, -- [10]
	},
	killTable = {
		{ ["name"] = "No",          ["width"] = 35,  ["align"] = "CENTER" }, -- [1]
		{ ["name"] = "Time",        ["width"] = 125, ["align"] = "CENTER" }, -- [2]
		{ ["name"] = "Source",      ["width"] = 100, ["align"] = "CENTER" }, -- [3]
		{ ["name"] = "Target",      ["width"] = 100, ["align"] = "CENTER" }, -- [4]
		{ ["name"] = "Type",        ["width"] = 70,  ["align"] = "CENTER" }, -- [5]
		{ ["name"] = "Spell",       ["width"] = 200, ["align"] = "CENTER" }, -- [6]
		{ ["name"] = "Spell ID",    ["width"] = 80,  ["align"] = "CENTER" }, -- [7]
		{ ["name"] = "DaMage",      ["width"] = 60,  ["align"] = "CENTER" }, -- [8]
		{ ["name"] = "Overkill",    ["width"] = 60,  ["align"] = "CENTER" }, -- [9]
		{ ["name"] = "Critical",    ["width"] = 50,  ["align"] = "CENTER" }, -- [10]
		{ ["name"] = "Multistrike", ["width"] = 60,  ["align"] = "CENTER" }, -- [11]
	}
};


-- Список логгеров c привязкой к списку возможных фильтров данных
This.md_loggerList = {
	["AURA"] = {
		logger = function() return ACR_RL_CL_AL_AL end
	},
	["BRAKE"] = {
		logger = function() return ACR_RL_CL_AL_BL end,
	},
	["CAST"] = {
		logger = function() return ACR_RL_CL_AL_CSL end,
	},
	["CREATE"] = {
		logger = function() return ACR_RL_CL_AL_CRL end,
	},
	["DAMage"] = {
		logger = function() return ACR_RL_CL_AL_DML end,
	},
	["DEATH"] = {
		logger = function() return ACR_RL_CL_AL_DTL end,
	},
	["DISPEL"] = {
		logger = function() return ACR_RL_CL_AL_DSL end,
	},
	["DRAIN"] = {
		logger = function() return ACR_RL_CL_AL_DRL end,
	},
	["ENCHANT"] = {
		logger = function() return ACR_RL_CL_AL_ENL end,
	},
	["ENERGIZE"] = {
		logger = function() return ACR_RL_CL_AL_ERL end,
	},
	["HEAL"] = {
		logger = function() return ACR_RL_CL_AL_HL end,
	},
	["INTERRUPT"] = {
		logger = function() return ACR_RL_CL_AL_IL end,
	},
	["LEECH"] = {
		logger = function() return ACR_RL_CL_AL_LL end,
	},
	["MISS"] = {
		logger = function() return ACR_RL_CL_AL_ML end,
	},
	["STEAL"] = {
		logger = function() return ACR_RL_CL_AL_STL end,
	},
	["SUMMON"] = {
		logger = function() return ACR_RL_CL_AL_SML end,
	},
};