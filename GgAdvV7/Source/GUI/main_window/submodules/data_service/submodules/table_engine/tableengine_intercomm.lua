local This = GGF.ModuleGetWrapper("GUI-5-3-5");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.1 (Miscellaneous Variables)
function This:MPRC_MVR_INITIALIZE()
	--self.vs_hInstance.tbl = LibStub("ScrollingTable");
end



-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------
	-- TODO: исправить именование первого объекта
	-- NOTE: какого именно?

	-- tableVision(SF)

	self.JR_TV_SF_OBJ = GGC.StandardFrame:Create(self.JR_TV_SF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.DataService:GetLeadingFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.DataService:GetLeadingFrame(),
			},
		},
	});
	--if (true) then return; end
	-- tableVision(SF)
		-- universal(CT)

	self.JR_TV_SF_U_CT_OBJ = GGC.ChargedTable:Create(self.JR_TV_SF_U_CT_TMP, {
		properties = {
		    base = {
				parent = GGF.INS.GetObjectRef(self.JR_TV_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_TV_SF_OBJ),
			},
		},
	});
end


-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- tableVision(SF)
	GGF.OuterInvoke(self.JR_TV_SF_OBJ, "Adjust");

	--if (true) then return; end
	-- tableVision(SF)
		-- universal(CT)
	GGF.OuterInvoke(self.JR_TV_SF_U_CT_OBJ, "Adjust");
end


-- п.2.8 (After-load Settings)
function This:MPRC_ALS_INITIALIZE()
	self:testTable();
	if (true) then return; end
	local DefaultColor = {
		["r"] = 1.0,
		["g"] = 1.0,
		["b"] = 1.0,
		["a"] = 1.0,
	};
	-- NOTE: тест
	local data = {
		{
			cols = {
				{
					value = 1,
					color = GGF.TableCopy(DefaultColor),
				}, 
				{
					value = "12:00",
					color = GGF.TableCopy(DefaultColor),
				},
				{
					value = "Elizi",
					color = GGF.TableCopy(DefaultColor),
				},
				{
					value = "Пятнистый кот",
					color = GGF.TableCopy(DefaultColor),
				},
				{
					value = "damage",
					color = GGF.TableCopy(DefaultColor),
				},
				{
					value = "Lava Burst",
					color = GGF.TableCopy(DefaultColor),
				},
				{
					value = 77451,
					color = GGF.TableCopy(DefaultColor),
				},
				{
					value = 270000,
					color = GGF.TableCopy(DefaultColor),
				},
				{
					value = "crit",
					color = GGF.TableCopy(DefaultColor),
				},
				{
					value = "N/A",
					color = GGF.TableCopy(DefaultColor),
				},
			},
		},
		{
			cols = {
				{
					value = 2,
					color = GGF.TableCopy(DefaultColor),
				},
				{
					value = "12:01",
					color = GGF.TableCopy(DefaultColor),
				},
				{
					value = "Elizi",
					color = GGF.TableCopy(DefaultColor),
				},
				{
					value = "Пятнистый кот",
					color = GGF.TableCopy(DefaultColor),
				},
				{
					value = "damage",
					color = GGF.TableCopy(DefaultColor),
				},
				{
					value = "Lava Burst",
					color = GGF.TableCopy(DefaultColor),
				},
				{
					value = 77451,
					color = GGF.TableCopy(DefaultColor),
				},
				{
					value = 270000,
					color = GGF.TableCopy(DefaultColor),
				},
				{
					value = "crit",
					color = GGF.TableCopy(DefaultColor),
				},
				{
					value = "N/A",
					color = GGF.TableCopy(DefaultColor),
				},
			},
		},
	};

	-- NOTE: временно вырезан
	--self.JR_TV_F_DT_F_OBJ:SetData(data);
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------