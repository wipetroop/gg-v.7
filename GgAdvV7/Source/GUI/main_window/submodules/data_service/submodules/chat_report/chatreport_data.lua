local This = GGF.ModuleCreateWrapper("GUI-5-3-1", false, {
	frameWidth = function() return GGM.GUI.ParameterStorage:GetDSCRFrameWidth() end,
	frameHeight = function() return GGM.GUI.ParameterStorage:GetDSCRFrameHeight() end,

	buttonWidth = function() return GGM.GUI.ParameterStorage:GetButtonWidth() end,
	buttonHeight = function() return GGM.GUI.ParameterStorage:GetButtonHeight() end,

	titleWidth = function() return GGM.GUI.ParameterStorage:GetTitleWidth() end,
	titleHeight = function() return GGM.GUI.ParameterStorage:GetTitleHeight() end,

	titleOffsetX = function() return GGM.GUI.ParameterStorage:GetTitleOffsetX() end,
	titleOffsetY = function() return GGM.GUI.ParameterStorage:GetTitleOffsetY() end,

	choiseWidth = function() return GGM.GUI.ParameterStorage:GetChoiseWidth() end,
	choiseHeight = function() return GGM.GUI.ParameterStorage:GetChoiseHeight() end,

	choiseOffsetX = function() return GGM.GUI.ParameterStorage:GetChoiseOffsetX() end,
	choiseGap = function() return GGM.GUI.ParameterStorage:GetChoiseGap() end,

	dlgGap = function() return GGM.GUI.ParameterStorage:GetDlgGap() end,

	dlgButtonOffsetX = 0,
	dlgButtonOffsetY = 20,
});

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------

-- Возможные типы чата для дампа
This.cs_chatTypes = {
	["Console"] = {},
	["GUild"] = {},
	["Instance"] = {},
	["RaidWarning"] = {},
	["Yell"] = {},
	["Party"] = {},
	["Whisper"] = {
		target = true,	-- NOTE: для этого типа необходимо указать цель(игрока)
	},
	["Say"] = {},
	["Raid"] = {},
};

-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable strings
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------