local This = GGF.ModuleGetWrapper("GUI-5-3-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_chatReportGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		chatReport = {			-- [CR_SF]
			inherit = {
				fontString = GGF.GTemplateTree(GGC.FontString, {
					title = {},		-- [T_FS]
				}),
				standardButton = GGF.GTemplateTree(GGC.StandardButton, {
					dump = {},		-- [D_SB]
				}),
				checkButton = GGF.GTemplateTree(GGC.CheckButton, {
					choise = {
						["1_1"] = {},		-- [C_1_1_CB]
						["1_2"] = {},		-- [C_1_2_CB]
						["1_3"] = {},		-- [C_1_3_CB]
						["1_4"] = {},		-- [C_1_4_CB]
						["1_5"] = {},		-- [C_1_5_CB]
						["1_6"] = {},		-- [C_1_6_CB]
						["1_7"] = {},		-- [C_1_7_CB]
						["1_8"] = {},		-- [C_1_8_CB]
						["1_9"] = {},		-- [C_1_9_CB]
					},
				}),
				editBox = GGF.GTemplateTree(GGC.EditBox, {
					charname = {},		-- [C_EB]
				}),
			},
		},
	}),
};

local CRG_SF = This.oc_chatReportGeometry.standardFrame;
local CRG_SF_CR_FS = CRG_SF.chatReport.inherit.fontString;
local CRG_SF_CR_SB = CRG_SF.chatReport.inherit.standardButton;
local CRG_SF_CR_CB = CRG_SF.chatReport.inherit.checkButton;
local CRG_SF_CR_EB = CRG_SF.chatReport.inherit.editBox;

-- chatReport(SF)
CRG_SF.chatReport.object = {};
CRG_SF.chatReport.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "CR",
		},
		miscellaneous = {
			hidden = true,
			movable = false,
			enableMouse = false,
			enableKeyboard = false,
		},
		size = {
			width = function() return MSC_CONFIG.frameWidth() end,
			height = function() return MSC_CONFIG.frameHeight() end,
		},
		anchor = {
			point = GGE.PointType.TopLeft,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.TopRight,
			offsetX = 0,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Segregate,
	},
};


-- chatReport(SF)
	-- title(FS)
CRG_SF_CR_FS.title.object = {};
CRG_SF_CR_FS.title.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "T",
		},
		miscellaneous = {
			text = "Chat Dump Menu",
			--tooltipText = "GUi_MW_F_DS_F_CR_F_T_FS",
		},
		size = {
			width = function() return MSC_CONFIG.titleWidth() end,
			height = function() return MSC_CONFIG.titleHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Top,
			offsetX = function() return MSC_CONFIG.titleOffsetX() end,
			offsetY = function() return MSC_CONFIG.titleOffsetY() end,
		},
	},
};


-- chatReport(SF)
	-- dump(SB)
CRG_SF_CR_SB.dump.object = {};
CRG_SF_CR_SB.dump.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "D",
		},
		miscellaneous = {
			text = "Chat Dump",
			--tooltipText = "GUi_MW_F_DS_F_CR_F_D_B",
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.Bottom,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = MSC_CONFIG.dlgButtonOffsetX,
			offsetY = MSC_CONFIG.dlgButtonOffsetY,
		},
		callback = {
			onClick = function(...) This:on_DButton_click(...) end,
		},
	},
};


-- chatReport(SF)
	-- choise(CB)["1_1"]
CRG_SF_CR_CB.choise["1_1"].object = {};
CRG_SF_CR_CB.choise["1_1"].template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "C_1_1",
		},
		miscellaneous = {
			text = "Console",
			--tooltipText = "GUi_MW_F_DS_F_CR_F_C_CB_1_1",
		},
		size = {
			width = function() return MSC_CONFIG.choiseWidth() end,
			height = function() return MSC_CONFIG.choiseHeight() end,
		},
		anchor = {
			point = GGE.PointType.TopLeft,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.TopLeft,
			offsetX = function() return MSC_CONFIG.choiseOffsetX() end,
			offsetY = 0,
		},
		callback = {
			onClick = function(...) end,
		},
	},
};


-- chatReport(SF)
	-- choise(CB)["1_2"]
CRG_SF_CR_CB.choise["1_2"].object = {};
CRG_SF_CR_CB.choise["1_2"].template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "C_1_2",
		},
		miscellaneous = {
			text = "Guild",
			--tooltipText = "GUi_MW_F_DS_F_CR_F_C_CB_1_2",
		},
		size = {
			width = function() return MSC_CONFIG.choiseWidth() end,
			height = function() return MSC_CONFIG.choiseHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = function() return - MSC_CONFIG.choiseGap() end,
		},
		callback = {
			onClick = function(...) end,
		},
	},
};


-- chatReport(SF)
	-- choise(CB)["1_3"]
CRG_SF_CR_CB.choise["1_3"].object = {};
CRG_SF_CR_CB.choise["1_3"].template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "C_1_3",
		},
		miscellaneous = {
			text = "Instance",
			--tooltipText = "GUi_MW_F_DS_F_CR_F_C_CB_1_3",
		},
		size = {
			width = function() return MSC_CONFIG.choiseWidth() end,
			height = function() return MSC_CONFIG.choiseHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = function() return - MSC_CONFIG.choiseGap() end,
		},
		callback = {
			onClick = function(...) end,
		},
	},
};


-- chatReport(SF)
	-- choise(CB)["1_4"]
CRG_SF_CR_CB.choise["1_4"].object = {};
CRG_SF_CR_CB.choise["1_4"].template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "C_1_4",
		},
		miscellaneous = {
			text = "RaidWarning",
			--tooltipText = "GUi_MW_F_DS_F_CR_F_C_CB_1_4",
		},
		size = {
			width = function() return MSC_CONFIG.choiseWidth() end,
			height = function() return MSC_CONFIG.choiseHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = function() return - MSC_CONFIG.choiseGap() end,
		},
		callback = {
			onClick = function(...) end,
		},
	},
};


-- chatReport(SF)
	-- choise(CB)["1_5"]
CRG_SF_CR_CB.choise["1_5"].object = {};
CRG_SF_CR_CB.choise["1_5"].template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "C_1_5",
		},
		miscellaneous = {
			text = "Whisper",
			--tooltipText = "GUi_MW_F_DS_F_CR_F_C_CB_1_5",
		},
		size = {
			width = function() return MSC_CONFIG.choiseWidth() end,
			height = function() return MSC_CONFIG.choiseHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = function() return - MSC_CONFIG.choiseGap() end,
		},
		callback = {
			onClick = function(...) end,
		},
	},
};


-- chatReport(SF)
	-- choise(CB)["1_6"]
CRG_SF_CR_CB.choise["1_6"].object = {};
CRG_SF_CR_CB.choise["1_6"].template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "C_1_6",
		},
		miscellaneous = {
			text = "Party",
			--tooltipText = "GUi_MW_F_DS_F_CR_F_C_CB_1_6",
		},
		size = {
			width = function() return MSC_CONFIG.choiseWidth() end,
			height = function() return MSC_CONFIG.choiseHeight() end,
		},
		anchor = {
			point = GGE.PointType.TopLeft,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Top,
			offsetX = function() return MSC_CONFIG.choiseOffsetX() end,
			offsetY = 0,
		},
		callback = {
			onClick = function(...) end,
		},
	},
};


-- chatReport(SF)
	-- choise(CB)["1_7"]
CRG_SF_CR_CB.choise["1_7"].object = {};
CRG_SF_CR_CB.choise["1_7"].template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "C_1_7",
		},
		miscellaneous = {
			text = "Yell",
			--tooltipText = "GUi_MW_F_DS_F_CR_F_C_CB_1_7",
		},
		size = {
			width = function() return MSC_CONFIG.choiseWidth() end,
			height = function() return MSC_CONFIG.choiseHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = function() return - MSC_CONFIG.choiseGap() end,
		},
		callback = {
			onClick = function(...) end,
		},
	},
};


-- chatReport(SF)
	-- choise(CB)["1_8"]
CRG_SF_CR_CB.choise["1_8"].object = {};
CRG_SF_CR_CB.choise["1_8"].template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "C_1_8",
		},
		miscellaneous = {
			text = "Say",
			--tooltipText = "GUi_MW_F_DS_F_CR_F_C_CB_1_8",
		},
		size = {
			width = function() return MSC_CONFIG.choiseWidth() end,
			height = function() return MSC_CONFIG.choiseHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = function() return - MSC_CONFIG.choiseGap() end,
		},
		callback = {
			onClick = function(...) end,
		},
	},
};


-- chatReport(SF)
	-- choise(CB)["1_9"]
CRG_SF_CR_CB.choise["1_9"].object = {};
CRG_SF_CR_CB.choise["1_9"].template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "C_1_9",
		},
		miscellaneous = {
			text = "Raid",
			--tooltipText = "GUi_MW_F_DS_F_CR_F_C_CB_1_9",
		},
		size = {
			width = function() return MSC_CONFIG.choiseWidth() end,
			height = function() return MSC_CONFIG.choiseHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = function() return - MSC_CONFIG.choiseGap() end,
		},
		callback = {
			onClick = function(...) end,
		},
	},
};


-- chatReport(SF)
	-- charname(EB)
CRG_SF_CR_EB.charname.object = {};
CRG_SF_CR_EB.charname.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "CN",
		},
		miscellaneous = {
			text = "<Enter char name>",
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.TopLeft,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.BottomLeft,
			offsetX = 0,
			offsetY = function() return - MSC_CONFIG.choiseGap() end,
		},
		backdrop = GGD.Backdrop.Empty,
		callback = {
			onEnterPressed = function(in_object, ...) in_object:ClearFocus(); end,
		},
	},
};