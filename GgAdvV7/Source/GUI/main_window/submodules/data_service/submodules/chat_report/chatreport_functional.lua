local This = GGF.ModuleGetWrapper("GUI-5-3-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- api
-- Смена видимости основного фрейма
function This:setHidden(in_vis)
	GGF.OuterInvoke(self.JR_CR_SF_OBJ, "SetHidden", in_vis);
	self:adjustOffset();
	GGM.GUI.DataFilter:AdjustOffset();
	GGM.GUI.LogFilter:AdjustOffset();
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Взятие ширины основного фрейма
function This:getWidth()
	return GGF.OuterInvoke(self.JR_CR_SF_OBJ, "GetWidth");
end


-- api
-- Взятие видимости основного фрейма
function This:getHidden()
	return GGF.OuterInvoke(self.JR_CR_SF_OBJ, "GetHidden");
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Принудительная переприменение отступа по оси X
function This:adjustOffset()
	GGF.OuterInvoke(self.JR_CR_SF_OBJ, "AdjustOffsetX");
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-- TODO: full refactor(после мержа GG-4)(GG-62)
-- Внешний обработчик вызова КЧР
function This:on_DButton_click()
	print(self.vs_menuSelector.chatType);

	local msf = self.vs_menuSelector.filter;
	local metaInf, dataSector = GGM.GUI.DSTableEngine:DumpConstructor(msf);
	local msgChannel = self.vs_menuSelector.chatType;
	local unitName = "";

	if (msgChannel == "Console") then
		print("Console dump is currently unavailable");
		return
	end

	if (msgChannel == "Whisper") then
		unitName = UnitName("target");
		if (unitName == nil) then
			print("No target");
			return
		end
	end

	local metaMsgTime = "Time: " .. metaInf.logStartTime .. " - " .. metaInf.logFinishTime;
	local metaMsgZone = "Zone: " .. metaInf.logZoneName;
	local metaMsgToken = "Token: " .. metaInf.logIdentifyingToken;
	local ServiceDelimiter = "------------------------------"

	if ((msf.evType ~= "DaMage")
		and (msf.evType ~= "Miss")
		and (msf.evType ~= "Kill")) then

		local hdr;
		hdr = "GUi chat report"
		self.fn_chatTyPetranscevier[msgChannel](hdr, unitName);
		local inf;
		inf = "Information: "
		self.fn_chatTyPetranscevier[msgChannel](inf, unitName);

		self.fn_chatTyPetranscevier[msgChannel](metaMsgTime, unitName);
		self.fn_chatTyPetranscevier[msgChannel](metaMsgZone, unitName);
		self.fn_chatTyPetranscevier[msgChannel](metaMsgToken, unitName);

	end

	-- DaMage
	if (msf.evType == "DaMage") then
		
		local ttl;
		ttl = "Event type: daMage"
		self.fn_chatTyPetranscevier[msgChannel](ttl, unitName);

		self.fn_chatTyPetranscevier[msgChannel](ServiceDelimiter, unitName);

		for k,v in pairs(dataSector) do
			local msg = ""

			msg = v.time.data .. " " .. v.sourceName.data .. " -> " .. v.targetName.data .. " / sp: " .. v.spellName.data .. "(id: " ..  v.spellID.data .. ") dmg: " .. v.spellDaMage.data .. " cr: " .. v.isCritical.data .. " mc: " .. v.isMulticast.data;

			self.fn_chatTyPetranscevier[msgChannel](msg, unitName);
		end
	-- Miss
	elseif (msf.evType ~= "Miss") then

		local ttl;
		ttl = "Event type: misses"
		self.fn_chatTyPetranscevier[msgChannel](ttl, unitName);

		self.fn_chatTyPetranscevier[msgChannel](ServiceDelimiter, unitName);

		for k,v in pairs(dataSector) do
			local msg = ""

			msg = v.time.data .. " " .. v.sourceName.data .. " -> " .. v.targetName.data .. " / sp: " .. v.spellName.data .. "(id: " ..  v.spellID.data .. ") dmg: " .. v.spellDaMage.data .. " mt: " .. v.evSubType.data .. " mc: " .. v.isMulticast.data;

			self.fn_chatTyPetranscevier[msgChannel](msg, unitName);
		end
	-- Kill
	elseif (msf.evType ~= "Kill") then

		local ttl;
		ttl = "Event type: kills"
		self.fn_chatTyPetranscevier[msgChannel](ttl, unitName);

		self.fn_chatTyPetranscevier[msgChannel](ServiceDelimiter, unitName);

		for k,v in pairs(dataSector) do
			local msg = ""

			msg = v.time.data .. " " .. v.sourceName.data .. " -> " .. v.targetName.data .. " / sp: " .. v.spellName.data .. "(id: " ..  v.spellID.data .. ") dmg: " .. v.spellDaMage.data .. " cr: " .. v.isCritical.data .. " mc: " .. v.isMulticast.data;

			self.fn_chatTyPetranscevier[msgChannel](msg, unitName);
		end
	else
		return;
	end
end


-- Обработчик изменений настроек чат репорта
function This:on_CRFGGC_CheckButton_click(in_button)
	local chButton = self.cs_chatTypeMapper.gUi[in_button];
	if (self.vs_checkButtonList.thisFrame.common[chButton]:GetChecked()) then
		self.vs_menuSelector.chatType = self.cs_chatTypes[chButton];
		
		self.vs_checkButtonList.thisFrame.common[chButton]:SetChecked(false);
	else
		self.vs_menuSelector.chatType = self.cs_chatTypes.none;

		for k,v in pairs(self.vs_checkButtonList.thisFrame.common) do
			v:setChecked(k == chButton);
			--else
				--GGM.GUI.DataService.varstruct.GGC_CheckButtonList.ThisFrame.common[chButton]:setChecked(true);
			--end
		end
	end

	self:checkDumpPossibility();
end


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------