local This = GGF.ModuleGetWrapper("GUI-5-3-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	local MSC_CONFIG = self:getConfig();

	-- chatReport(SF)

	self.JR_CR_SF_OBJ = GGC.StandardFrame:Create(self.JR_CR_SF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.DataService:GetLeadingFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.DataService:GetLeadingFrame(),
				--offsetX = function() return 0 end, --MSC_CONFIG.dlgGap() end,
				--offsetX = function() 
					--return GGF.SwitchCase(GGF.TernExpSingle(self:GetHidden(), 0, 1) + GGF.TernExpSingle(GGM.GUI.DataFilter:GetHidden(), 0, 1) + GGF.TernExpSingle(GGM.GUI.LogFilter:GetHidden(), 0, 1),
							--{0, 0},
							--{1, 0},
							--{2, (MSC_CONFIG.dlgGap() + GGF.OuterInvoke(self.JR_CR_SF_OBJ, "GetWidth") + GGF.TernExpSingle(GGM.GUI.DataFilter:GetHidden(), GGM.GUI.LogFilter:GetWidth(), GGM.GUI.DataFilter:GetWidth()))/2 - GGF.OuterInvoke(self.JR_CR_SF_OBJ, "GetWidth")/2},
							--{3, (2*MSC_CONFIG.dlgGap() + GGM.GUI.LogFilter:GetWidth() + GGM.GUI.DataFilter:GetWidth() + GGF.OuterInvoke(self.JR_CR_SF_OBJ, "GetWidth"))/2 - GGF.OuterInvoke(self.JR_CR_SF_OBJ, "GetWidth")/2});
				--end,
			},
		},
	});

	-- NOTE: тут кританет из-за вызовов других объектов в функции оффсета


	-- chatReport(SF)
		-- title(FS)

	self.JR_CR_SF_T_FS_OBJ = GGC.FontString:Create(self.JR_CR_SF_T_FS_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_CR_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_CR_SF_OBJ),
			},
		},
	});


	-- chatReport(SF)
		-- choise(CB)["1_1"]

	self.JR_CR_SF_C_1_1_CB_OBJ = GGC.CheckButton:Create(self.JR_CR_SF_C_1_1_CB_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_CR_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_CR_SF_OBJ),
				offsetY = GGF.OuterInvoke(self.JR_CR_SF_T_FS_OBJ, "GetOffsetY") - GGF.OuterInvoke(self.JR_CR_SF_T_FS_OBJ, "GetHeight") - MSC_CONFIG.choiseGap(),
			},
		},
	});


	-- chatReport(SF)
		-- choise(CB)["1_2"]

	self.JR_CR_SF_C_1_2_CB_OBJ = GGC.CheckButton:Create(self.JR_CR_SF_C_1_2_CB_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_CR_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_CR_SF_C_1_1_CB_OBJ),
			},
		},
	});


	-- chatReport(F)
		-- choise(CB)["1_3"]

	self.JR_CR_SF_C_1_3_CB_OBJ = GGC.CheckButton:Create(self.JR_CR_SF_C_1_3_CB_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_CR_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_CR_SF_C_1_2_CB_OBJ),
			},
		},
	});


	-- chatReport(SF)
		-- choise(CB)["1_4"]

	self.JR_CR_SF_C_1_4_CB_OBJ = GGC.CheckButton:Create(self.JR_CR_SF_C_1_4_CB_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_CR_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_CR_SF_C_1_3_CB_OBJ),
			},
		},
	});


	-- chatReport(SF)
		-- choise(CB)["1_5"]

	self.JR_CR_SF_C_1_5_CB_OBJ = GGC.CheckButton:Create(self.JR_CR_SF_C_1_5_CB_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_CR_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_CR_SF_C_1_4_CB_OBJ),
			},
		},
	});


	-- chatReport(SF)
		-- choise(CB)["1_6"]

	self.JR_CR_SF_C_1_6_CB_OBJ = GGC.CheckButton:Create(self.JR_CR_SF_C_1_6_CB_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_CR_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_CR_SF_OBJ),
				offsetY = GGF.OuterInvoke(self.JR_CR_SF_T_FS_OBJ, "GetOffsetY") - GGF.OuterInvoke(self.JR_CR_SF_T_FS_OBJ, "GetHeight") - MSC_CONFIG.choiseGap(),
			},
		},
	});


	-- chatReport(SF)
		-- choise(CB)["1_7"]

	self.JR_CR_SF_C_1_7_CB_OBJ = GGC.CheckButton:Create(self.JR_CR_SF_C_1_7_CB_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_CR_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_CR_SF_C_1_6_CB_OBJ),
			},
		},
	});


	-- chatReport(F)
		-- choise(CB)["1_8"]

	self.JR_CR_SF_C_1_8_CB_OBJ = GGC.CheckButton:Create(self.JR_CR_SF_C_1_8_CB_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_CR_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_CR_SF_C_1_7_CB_OBJ),
			},
		},
	});


	-- chatReport(F)
		-- choise(CB)["1_9"]

	self.JR_CR_SF_C_1_9_CB_OBJ = GGC.CheckButton:Create(self.JR_CR_SF_C_1_9_CB_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_CR_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_CR_SF_C_1_8_CB_OBJ),
			},
		},
	});


	-- chatReport(F)
		-- dump(SB)

	self.JR_CR_SF_D_SB_OBJ = GGC.StandardButton:Create(self.JR_CR_SF_D_SB_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_CR_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_CR_SF_OBJ),
			},
		},
	});


	-- chatReport(F)
		-- charname(EB)

	self.JR_CR_SF_CN_EB_OBJ = GGC.EditBox:Create(self.JR_CR_SF_CN_EB_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_CR_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_CR_SF_C_1_5_CB_OBJ),
			},
		},
	});
end


-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- chatReport(SF)

	GGF.OuterInvoke(self.JR_CR_SF_OBJ, "Adjust");


	-- chatReport(SF)
		-- title(FS)

	GGF.OuterInvoke(self.JR_CR_SF_T_FS_OBJ, "Adjust");


	-- chatReport(SF)
		-- choise(CB)["1_1"]

	GGF.OuterInvoke(self.JR_CR_SF_C_1_1_CB_OBJ, "Adjust");


	-- chatReport(SF)
		-- choise(CB)["1_2"]

	GGF.OuterInvoke(self.JR_CR_SF_C_1_2_CB_OBJ, "Adjust");


	-- chatReport(SF)
		-- choise(CB)["1_3"]

	GGF.OuterInvoke(self.JR_CR_SF_C_1_3_CB_OBJ, "Adjust");


	-- chatReport(SF)
		-- choise(CB)["1_4"]

	GGF.OuterInvoke(self.JR_CR_SF_C_1_4_CB_OBJ, "Adjust");


	-- chatReport(SF)
		-- choise(CB)["1_5"]

	GGF.OuterInvoke(self.JR_CR_SF_C_1_5_CB_OBJ, "Adjust");


	-- chatReport(SF)
		-- choise(CB)["1_6"]

	GGF.OuterInvoke(self.JR_CR_SF_C_1_6_CB_OBJ, "Adjust");


	-- chatReport(SF)
		-- choise(CB)["1_7"]

	GGF.OuterInvoke(self.JR_CR_SF_C_1_7_CB_OBJ, "Adjust");


	-- chatReport(SF)
		-- choise(CB)["1_8"]

	GGF.OuterInvoke(self.JR_CR_SF_C_1_8_CB_OBJ, "Adjust");


	-- chatReport(SF)
		-- choise(CB)["1_9"]

	GGF.OuterInvoke(self.JR_CR_SF_C_1_9_CB_OBJ, "Adjust");


	-- chatReport(SF)
		-- dump(SB)

	GGF.OuterInvoke(self.JR_CR_SF_D_SB_OBJ, "Adjust");


	-- chatReport(SF)
		-- charname(EB)

	GGF.OuterInvoke(self.JR_CR_SF_CN_EB_OBJ, "Adjust");
end


-- п.2.8 (After-load Settings)
function This:MPRC_ALS_INITIALIZE()
	-- TODO: wtf?
	if (true) then return; end
	GGF.OuterInvoke(self.JR_CR_SF_OBJ, "Adjust");
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------