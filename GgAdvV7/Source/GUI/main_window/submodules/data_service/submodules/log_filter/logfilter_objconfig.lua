local This = GGF.ModuleGetWrapper("GUI-5-3-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_logFilterGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		logFilter = {		-- [LF_SF]
			inherit = {
				fontString = GGF.GTemplateTree(GGC.FontString, {
					title = {},		-- [T_FS]
				}),
				viewTable = GGF.GTemplateTree(GGC.ViewTable, {
					choise = {},		-- [C_VT]
				}),
				standardButton = GGF.GTemplateTree(GGC.StandardButton, {
					add = {},		-- [A_SB]
				}),
				dropdownMenu = GGF.GTemplateTree(GGC.DropdownMenu, {
               choise = {}       -- [C_DDM]
				})
			},
		},
	}),
};

local LFG_SF = This.oc_logFilterGeometry.standardFrame;
local LFG_SF_LF_FS = LFG_SF.logFilter.inherit.fontString;
local LFG_SF_LF_VT = LFG_SF.logFilter.inherit.viewTable;
local LFG_SF_LF_SB = LFG_SF.logFilter.inherit.standardButton;
local LFG_SF_LF_DDM = LFG_SF.logFilter.inherit.dropdownMenu;

-- logFilter(SF)
LFG_SF.logFilter.object = {};
LFG_SF.logFilter.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "LF",
		},
		miscellaneous = {
			hidden = true,
		},
		size = {
			width = function() return MSC_CONFIG.frameWidth() end,
			height = function() return MSC_CONFIG.frameHeight() end,
		},
		anchor = {
			point = GGE.PointType.TopRight,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.TopLeft,
			offsetX = 0,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Segregate,
	},
};


-- logFilter(SF)
	-- title(FS)
LFG_SF_LF_FS.title.object = {};
LFG_SF_LF_FS.title.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "T",
		},
		miscellaneous = {
			text = "Log Filter",
			--tooltipText = "GUi_MW_F_DS_F_LF_F_T_FS",
		},
		size = {
			width = function() return MSC_CONFIG.titleWidth() end,
			height = function() return MSC_CONFIG.titleHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Top,
			offsetX = function() return MSC_CONFIG.titleOffsetX() end,
			offsetY = function() return MSC_CONFIG.titleOffsetY() end,
		},
	},
};


-- logFilter(SF)
	-- choise(VT)
LFG_SF_LF_VT.choise.object = {};
LFG_SF_LF_VT.choise.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "C",
		},
		hardware = {
			--fontHeight = function() return MSC_CONFIG.tableFontHeight() end,
			--rowCount = MSC_CONFIG.tableRowCount,
		},
		data = {},
		anchor = {
			point = MSC_CONFIG.tablePoint,
			relativeTo = nil,	-- fwd
			relativePoint = MSC_CONFIG.tableRelativePoint,
			offsetX = MSC_CONFIG.tableOffsetX,
			offsetY = MSC_CONFIG.tableOffsetY,
		},
		--backdrop = GGD.Backdrop.Empty,
		backdrop = GGD.Backdrop.Segregate,
		callback = {
			onCellClick = function(...) This:on_choiseTableF_clicked(...) end,
			onCellEnter = function(...) This:on_choiseTableF_cellEnter(...) end,
			onCellLeave = function(...) MSC_CONFIG.on_cell_leaved(...) end,
		},
	},
};


-- logFilter(SF)
	-- add(SB)
LFG_SF_LF_SB.add.object = {};
LFG_SF_LF_SB.add.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "A",
		},
		miscellaneous = {
			text = "Add",
			--tooltipText = "GUi_MW_F_DS_F_LF_F_A_B",
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.Bottom,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = MSC_CONFIG.dlgButtonOffsetX,
			offsetY = MSC_CONFIG.dlgButtonOffsetY,
		},
		callback = {
			onClick = function(...) This:on_AddB_clicked(...) end,
		},
	},
};


-- logFilter(SF)
   -- choise(DDM)
LFG_SF_LF_DDM.choise.object = {};
LFG_SF_LF_DDM.choise.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "C",
      },
      miscellaneous = {
         text = "Add",
         --tooltipText = "GUi_MW_F_DS_F_LF_F_A_B",
      },
      size = {
         width = function() return MSC_CONFIG.dropdownWidth end,
         height = function() return MSC_CONFIG.buttonHeight() end,
      },
      anchor = {
         point = GGE.PointType.Bottom,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Bottom,
         offsetX = MSC_CONFIG.dropdownOffsetX,
         offsetY = MSC_CONFIG.dropdownOffsetY,
      },
      callback = {
         --onClick = function(...) This:on_AddB_clicked(...) end,
      },
   },
};
