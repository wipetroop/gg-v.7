local This = GGF.ModuleGetWrapper("GUI-5-3-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.1 (Miscellaneous Variables)
function This:MPRC_MVR_INITIALIZE()
	--self.vs_hInstance.gUi = LibStub("AceGUi-3.0");
	--self.vs_hInstance.tbl = LibStub("ScrollingTable");
end


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------
	local MSC_CONFIG = self:getConfig();
	-- logFilter(SF)

	self.JR_LF_SF_OBJ = GGC.StandardFrame:Create(self.JR_LF_SF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.DataService:GetFilterSettingsFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.DataService:GetFilterSettingsFrame(),
				--offsetX = function() 
					--return GGF.SwitchCase(GGF.TernExpSingle(self:GetHidden(), 0, 1) + GGF.TernExpSingle(GGM.GUI.DataFilter:GetHidden(), 0, 1) + GGF.TernExpSingle(GGM.GUI.ChatReport:GetHidden(), 0, 1),
							--{0, 0},
							--{1, 0},
							--{2, - (MSC_CONFIG.dlgGap() + GGF.OuterInvoke(self.JR_LF_SF_OBJ, "GetWidth") + GGF.TernExpSingle(GGM.GUI.ChatReport:GetHidden(), GGM.GUI.DataFilter:GetWidth(), GGM.GUI.ChatReport:GetWidth()))/2 + GGF.OuterInvoke(self.JR_LF_SF_OBJ, "GetWidth")/2},
							--{3, - (2*MSC_CONFIG.dlgGap() + GGF.OuterInvoke(self.JR_LF_SF_OBJ, "GetWidth") + GGM.GUI.DataFilter:GetWidth() + GGM.GUI.ChatReport:GetWidth())/2 + GGF.OuterInvoke(self.JR_LF_SF_OBJ, "GetWidth")/2});
				--end,
				offsetY = function()
					return GGF.SwitchCase(GGF.TernExpSingle(GGM.GUI.DataFilter:GetHidden(), 0, 1),
						{0, 0},
						{1, - GGM.GUI.DataFilter:GetHeight()}
					);
				end,
			},
		},
	});


	-- logFilter(SF)
		-- title(FS)

	self.JR_LF_SF_T_FS_OBJ = GGC.FontString:Create(self.JR_LF_SF_T_FS_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_LF_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_LF_SF_OBJ),
			},
		},
	});


	-- logFilter(SF)
		-- choise(VT)

	self.JR_LF_SF_C_VT_OBJ = GGC.ViewTable:Create(self.JR_LF_SF_C_VT_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_LF_SF_OBJ),
			},
			size = {
				width = GGF.OuterInvoke(self.JR_LF_SF_OBJ, "GetWidth") - 2*self.JR_LF_SF_C_VT_TMP.properties.anchor.offsetX,
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_LF_SF_OBJ),
			},
		},
	});

	--self.JR_LF_F_CT_F_OBJ:SetSubObject(self.JR_LF_F_CT_F_OBJ:GetObject().frame);


	-- logFilter(SF)
		-- add(SB)
	self.JR_LF_SF_A_SB_OBJ = GGC.StandardButton:Create(self.JR_LF_SF_A_SB_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_LF_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_LF_SF_OBJ),
			},
		},
	});

	-- logFilter(SF)
		-- add(SB)
	self.JR_LF_SF_C_DDM_OBJ = GGC.DropdownMenu:Create(self.JR_LF_SF_C_DDM_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_LF_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_LF_SF_OBJ),
			},
			callback = {
				onSelect = function() end
			}
		},
	});
end


-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- logFilter(SF)
	GGF.OuterInvoke(self.JR_LF_SF_OBJ, "Adjust");


	-- logFilter(SF)
		-- title(FS)
	GGF.OuterInvoke(self.JR_LF_SF_T_FS_OBJ, "Adjust");


	-- logFilter(SF)
		-- choise(VT)
	--GGF.OuterInvoke(self.JR_LF_SF_C_VT_OBJ, "Adjust");
	--GGF.OuterInvoke(self.JR_LF_SF_C_VT_OBJ, "Launch");


	-- logFilter(SF)
		-- add(SB)
	GGF.OuterInvoke(self.JR_LF_SF_A_SB_OBJ, "Adjust");


	-- logFilter(SF)
		-- choise(DDM)
	GGF.OuterInvoke(self.JR_LF_SF_C_DDM_OBJ, "Adjust");
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------