local This = GGF.ModuleCreateWrapper("GUI-5-3-4", false, {
	frameWidth = function() return GGM.GUI.ParameterStorage:GetDSLFFrameWidth() end,
	frameHeight = function() return GGM.GUI.ParameterStorage:GetDSLFFrameHeight() end,

	buttonWidth = function() return GGM.GUI.ParameterStorage:GetButtonWidth() end,
	buttonHeight = function() return GGM.GUI.ParameterStorage:GetButtonHeight() end,

	titleWidth = function() return GGM.GUI.ParameterStorage:GetTitleWidth() end,
	titleHeight = function() return GGM.GUI.ParameterStorage:GetTitleHeight() end,

	titleOffsetX = function() return GGM.GUI.ParameterStorage:GetTitleOffsetX() end,
	titleOffsetY = function() return GGM.GUI.ParameterStorage:GetTitleOffsetY() end,

	tableFontHeight = function() return GGM.GUI.ParameterStorage:GetTableFontHeight() end,
	tableRowCount = 39, -- а че не 40?

	tablePoint = "TopLeft",
	tableRelativePoint = "TopLeft",

	tableOffsetX = 2,
	tableOffsetY = -40,

	dropdownWidth = 310,
	dropdownOffsetX = 0,
	dropdownOffsetY = 60,

	dlgGap = function() return GGM.GUI.ParameterStorage:GetDlgGap() end,

	dlgButtonOffsetX = 0,
	dlgButtonOffsetY = 20,

	segregateBackdrop = function() return GGM.GUI.ParameterStorage:GetSegregateFrameBackdrop() end,

	on_cell_entered = function(...) GGM.GUI.CommonService:OnCommonTableCellEnter()(...) end,
	on_cell_leaved = function(...) GGM.GUI.CommonService:OnCommonTableCellLeave()(...) end,
});

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------

-- Прототип полей выбора лога
This.cs_logTablePrototypeMap = {
	{
		["name"] = "No",
		["width"] = 20,
		["align"] = "CENTER",
	}, -- [1]
	{
		["name"] = "Battlefield",
		["width"] = 120,
		["align"] = "CENTER",
	}, -- [2]
	{
		["name"] = "Token",
		["width"] = 70,
		["align"] = "CENTER",
	}, -- [3]
	{
		["name"] = "Existence",
		["width"] = 70,
		["align"] = "CENTER",
	}, -- [4]
};

-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------

This.vs_logSelector = {
	battlefieldSignature = "",
	startTime = "",
	finishTime = "",
	token = "",
};

This.vs_hInstance = {
	gUi = nil,
	tbl = nil,
};

-- Список тайтлов логов(уникальных с вр.м.)
This.vs_logList = {};

-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------