local This = GGF.ModuleGetWrapper("GUI-5-3-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- api
-- Смена видимости основного фрейма
function This:setHidden(in_vis)
	GGF.OuterInvoke(self.JR_LF_SF_OBJ, "SetHidden", in_vis);
	self:adjustOffset();
	GGM.GUI.DataFilter:AdjustOffset();
	GGM.GUI.ChatReport:AdjustOffset();
	if (not in_vis) then
		self:updateLogList("");
	else
		self:updateLogList(nil);
		self:changeAddButtonState(false);
	end
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Взятие ширины основного фрейма
function This:getWidth()
	return GGF.OuterInvoke(self.JR_LF_SF_OBJ, "GetWidth");
end


-- api
-- Взятие видимости основного фрейма
function This:getHidden()
	return GGF.OuterInvoke(self.JR_LF_SF_OBJ, "GetHidden");
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Принудительная переприменение отступа по осям X
function This:adjustOffset()
	GGF.OuterInvoke(self.JR_LF_SF_OBJ, "AdjustOffsetX");
	GGF.OuterInvoke(self.JR_LF_SF_OBJ, "AdjustOffsetY");
end


-- Сверка имеющегося в уях списка с актуальным
function This:checkActualLogList(in_actLog)
	for k,v in pairs(in_actLog) do
		if (not self.vs_logList[k]) then
			return true;
		end
	end

	for k,v in pairs(self.vs_logList) do
		if (not in_actLog[k]) then
			return true;
		end
	end

	return false;
end


-- Функция обновления и отрисовки лог листа
function This:updateLogList(in_arg)
	if (not in_arg) then
		self.vs_logSelector.battlefieldSignature = "";
		self.vs_logSelector.startTime = "";
		self.vs_logSelector.finishTime = "";
		self.vs_logSelector.token = "";
		--GGF.OuterInvoke(self.JR_LF_F_CT_F_OBJ, "ClearSelection");
		GGF.OuterInvoke(self.JR_LF_SF_C_VT_OBJ, "ClearSelection");
		return;
	end

	-- TODO: поправить внешнюю функцию(GG-17)
	local metaHistory = GGM.ACR.RuntimeLogger:GetAvailableLogList();

	-- NOTE: вернуть return, если чек на повтор не нужен будет
	if self:checkActualLogList(metaHistory) then
		self.vs_logList = metaHistory;
	--else
		--return;
	end

	local tableData = {
		vHeader = {
         textColor = GGD.Color.Green,
         tooltipColor = GGD.Color.Red,
   	},
   	fields = {}
	};
	local datCluster = tableData.fields;
	for token,logMeta in pairs(self.vs_logList) do
		local datatransport = {};
		GGM.GUI.CommonService:SetTableElementData(datatransport, 1, #tableData + 1, GGD.Color.Neutral);
		GGM.GUI.CommonService:SetTableElementData(datatransport, 2, logMeta.zoneName, GGD.Color.Neutral);
		GGM.GUI.CommonService:SetTableElementData(datatransport, 3, token, GGD.Color.Neutral);
		-- TODO: как-нибудь причесать эту дичь с подсчетом логов
		GGM.GUI.CommonService:SetTableElementData(datatransport, 4, GGF.TernExpSingle(GGM.GUI.DataService:CountFiltersByPattern(GGE.FilterType.Log, logMeta.zoneName .. "(" .. token .. ")") == 0, "No", "Yes"), GGD.Color.Neutral);

		GGM.GUI.CommonService:SetTableElementData(datatransport, 5, logMeta.startTime, GGD.Color.Neutral);
		GGM.GUI.CommonService:SetTableElementData(datatransport, 6, logMeta.finishTime, GGD.Color.Neutral);

		datCluster[#datCluster + 1] = {};
		datCluster[#datCluster] = datatransport;
		--print("log token", token);
	end

	local logList = {};
	for token,logMeta in pairs(self.vs_logList) do
		logList[#logList + 1] = {
			description = logMeta.zoneName .. "(" .. token .. ")",
			value = token,
		}
	end

	--GGF.OuterInvoke(self.JR_LF_F_CT_F_OBJ, "SetData", tableData);
	--GGF.OuterInvoke(self.JR_LF_SF_C_VT_OBJ, "SetDataSet", tableData);
	GGF.OuterInvoke(self.JR_LF_SF_C_DDM_OBJ, "SetDataSet", logList);
end


-- Смена состояния кнопки добавления лога
function This:changeAddButtonState(in_state)
	if true then
		return
	end
	if (GGF.OuterInvoke(self.JR_LF_SF_A_SB_OBJ, "GetEnabled") == in_state) then
		return; -- ничего не делаем
	end
	GGF.OuterInvoke(self.JR_LF_SF_A_SB_OBJ, "SetEnabled", in_state);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-- Специальный обработчик списка логов(с доп. информацией)
function This:on_choiseTableF_cellEnter(in_rowFrame, in_cellFrame, in_data, in_cols, in_row, in_realrow, in_column, in_scrollingTable, ...)
	if in_data[in_realrow] == nil or in_data[in_realrow].cols == nil then
		return;
	end
	local msg = {
		value = "|cffffffff" .. in_data[in_realrow].cols[2].value .. '|r\n'
			 .. "|cff0039a6" .. in_data[in_realrow].cols[3].value .. '|r\n'
			 .. "ST: |cffd52b1e" .. in_data[in_realrow].cols[5].value .. "|r\n"
			 .. "FN: |cffd52b1e" .. in_data[in_realrow].cols[6].value .. "|r\n"
	};
	GGM.GUI.CommonService:OnMouseFocusCell(in_cellFrame, msg);
end


-- Специальный обработчик нажатия на строку списка логов
function This:on_choiseTableF_clicked(in_rowFrame, in_cellFrame, in_data, in_cols, in_row, in_realrow, in_column, in_table, in_button, ...)
	if in_data[in_realrow] == nil or in_data[in_realrow].cols == nil then
		return;
	end

	-- NOTE: нужна задержка, т.к. выделение запаздывает по сравнению с вызовом коллбека
	C_Timer.NewTimer(0.25, function() 
		local selection = in_table:GetSelection();
		if (not selection) then
			self:changeAddButtonState(false);
			return;
		end

		local bfName = in_data[selection].cols[2].value;
		local bfToken = in_data[selection].cols[3].value;
		local stTime = in_data[selection].cols[5].value;
		local fnTime = in_data[selection].cols[6].value;
		-- TODO: приделать отключение при снятии выделения(и реакцию на выделение, а не на нажатие)
		self.vs_logSelector.battlefieldSignature = bfName .. "(" .. bfToken .. ")";
		self.vs_logSelector.startTime = stTime;
		self.vs_logSelector.finishTime = fnTime;
		self.vs_logSelector.token = bfToken;	-- NOTE: токен нужен для последующего получения данных(чтоб не парсить сигнатуру)

		--self.vs_menuSelector.logClusterData = ACR_CL:GetChoosenLogClusterByMark(self.vs_menuSelector.logClusterTitle);

		self:changeAddButtonState(in_data[selection].cols[4].value == "No");
	end);
end


-- Внешний обработчик нажатия кнопки add
function This:on_AddB_clicked()
	GGM.GUI.DataService:AddLogFilter(self.vs_logSelector);
	GGF.OuterInvoke(self.JR_LF_F_CT_F_OBJ, "ClearSelection");
	GGM.GUI.DataService:ChangeAddCombatLogTSState();
end


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------