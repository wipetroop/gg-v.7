local This = GGF.ModuleGetWrapper("GUI-5-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------

	-- infomationPanel(SF)
	self.JR_IP_SF_OBJ = GGC.StandardFrame:Create(self.JR_IP_SF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.MainWindow:GetLeadingFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.MainWindow:GetLeadingFrame(),
			},
		},
	});

   local gap = 20
	-- infomationPanel(SF)
		-- loadStat(TEF)
	self.JR_IP_SF_LS_TEF_OBJ = GGC.TextEditFrame:Create(self.JR_IP_SF_LS_TEF_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_IP_SF_OBJ),
			},
         size = {
            width = GGF.OuterInvoke(self.JR_IP_SF_OBJ, "GetWidth") - 2*gap;
            height = GGF.OuterInvoke(self.JR_IP_SF_OBJ, "GetHeight") - 2*gap;
         },
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_IP_SF_OBJ),
			},
		},
	});
end


-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- infomationPanel(SF)
	GGF.OuterInvoke(self.JR_IP_SF_OBJ, "Adjust");

   -- infomationPanel(SF)
      -- loadStat(TEF)
   GGF.OuterInvoke(self.JR_IP_SF_LS_TEF_OBJ, "Adjust");
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------