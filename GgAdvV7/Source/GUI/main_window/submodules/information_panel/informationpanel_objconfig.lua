local This = GGF.ModuleGetWrapper("GUI-5-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_informationPanelGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		informationPanel = {		-- [IP_SF]
         inherit = {
            textEditFrame = GGF.GTemplateTree(GGC.TextEditFrame, {
               loadStat = {}, -- [LS_TEF]
            }),
         }
		},
	}),
};

local IPG_SF = This.oc_informationPanelGeometry.standardFrame;
local IPG_SF_IP_TEF = IPG_SF.informationPanel.inherit.textEditFrame;

-- informationPanel(SF)
IPG_SF.informationPanel.object = {};
IPG_SF.informationPanel.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "IP",
		},
		size = {
			width = function() return MSC_CONFIG.frameWidth() end,
			height = function() return MSC_CONFIG.frameHeight() end,
		},
		anchor = {
			point = GGE.PointType.TopLeft,
			relativeTo = nil,	-- rel
			relativePoint = GGE.PointType.BottomLeft,
			offsetX = 0,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Segregate,
	},
};

-- informationPanel(SF)
   -- loadStat(TEF)
IPG_SF_IP_TEF.loadStat.object = {};
IPG_SF_IP_TEF.loadStat.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "LS",
      },
      miscellaneous = {
         --fontHeight = 40,
         hidden = false,
      },
      anchor = {
         point = GGE.PointType.Center,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Center,
         offsetX = 0,
         offsetY = 0,
      },
      backdrop = GGD.Backdrop.Segregate,
   },
};