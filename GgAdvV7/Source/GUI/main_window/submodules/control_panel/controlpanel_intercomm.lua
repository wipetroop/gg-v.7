local This = GGF.ModuleGetWrapper("GUI-5-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------

	-- controlPanel(SF)

	self.JR_CP_SF_OBJ = GGC.StandardFrame:Create(self.JR_CP_SF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.MainWindow:GetLeadingFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.MainWindow:GetLeadingFrame(),
			},
		},
	});


	-- controlPanel(SF)
		-- commonSettings(TS)

	self.JR_CP_SF_CS_TS_OBJ = GGC.TabSwitch:Create(self.JR_CP_SF_CS_TS_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_CP_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_CP_SF_OBJ),
			},
		},
	});


	-- controlPanel(SF)
		-- hudGeometry(TS)

	self.JR_CP_SF_HG_TS_OBJ = GGC.TabSwitch:Create(self.JR_CP_SF_HG_TS_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_CP_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_CP_SF_CS_TS_OBJ),
			},
		},
	});


	-- controlPanel(SF)
		-- loggingManager(TS)

	self.JR_CP_SF_LM_TS_OBJ = GGC.TabSwitch:Create(self.JR_CP_SF_LM_TS_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_CP_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_CP_SF_HG_TS_OBJ),
			},
		},
	});


	-- controlPanel(SF)
		-- effectManager(TS)

	self.JR_CP_SF_EM_TS_OBJ = GGC.TabSwitch:Create(self.JR_CP_SF_EM_TS_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_CP_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_CP_SF_LM_TS_OBJ),
			},
		},
	});
end


-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- controlPanel(SF)

	GGF.OuterInvoke(self.JR_CP_SF_OBJ, "Adjust");


	-- controlPanel(SF)
		-- commonSettings(TS)

	GGF.OuterInvoke(self.JR_CP_SF_CS_TS_OBJ, "Adjust");


	-- controlPanel(SF)
		-- hudGeometry(TS)

	GGF.OuterInvoke(self.JR_CP_SF_HG_TS_OBJ, "Adjust");


	-- controlPanel(SF)
		-- loggingManager(TS)

	GGF.OuterInvoke(self.JR_CP_SF_LM_TS_OBJ, "Adjust");


	-- controlPanel(SF)
		-- effectManager(TS)

	GGF.OuterInvoke(self.JR_CP_SF_EM_TS_OBJ, "Adjust");
end


-- п.2.8 (After-load Settings)
function This:MPRC_ALS_INITIALIZE()
	PanelTemplates_SetNumTabs(GGF.INS.GetObjectRef(self.JR_CP_SF_OBJ), 4);	-- 4 total

	self:managePanels(1);
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------