local This = GGF.ModuleGetWrapper("GUI-5-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_controlPanelGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		controlPanel = {			-- [CP_SF]
			inherit = {
				tabSwitch = GGF.GTemplateTree(GGC.TabSwitch, {
					commonSettings = {},		-- [CS_TS]
					hudGeometry = {},			-- [HG_TS]
					loggingManager = {},		-- [LM_TS]
					effectManager = {},			-- [EM_TS]
				}),
			},
		},
	}),
};

local CPG_SF = This.oc_controlPanelGeometry.standardFrame;
local CPG_SF_CP_TS = CPG_SF.controlPanel.inherit.tabSwitch;

-- controlPanel(SF)
CPG_SF.controlPanel.object = {};
CPG_SF.controlPanel.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "CP",
		},
		miscellaneous = {
			hidden = false,
		},
		size = {
			width = function() return MSC_CONFIG.frameWidth() end,
			height = function() return MSC_CONFIG.frameHeight() end,
		},
		anchor = {
			point = GGE.PointType.TopLeft,
			relativeTo = nil, -- fwd
			relativePoint = GGE.PointType.BottomLeft,
			offsetX = 0,	-- fwd
			offsetY = 0,	-- fwd
		},
		backdrop = GGD.Backdrop.Segregate,
	},
};


-- controlPanel(SF)
	-- commonSettings(TS)
CPG_SF_CP_TS.commonSettings.object = {};
CPG_SF_CP_TS.commonSettings.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			template = "OptionsFrameTabButtonTemplate",
			wrapperName = "CS",
		},
		miscellaneous = {
			gxname = "Tab1",
			hidden = false,
			enabled = false,
			text = "Common Settings",
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.BottomLeft,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.BottomLeft,
			offsetX = 0,	-- fwd
			offsetY = 0,	-- fwd
		},
		callback = {
			onClick = function(...) This:on_commonSettingsTS_clicked(...) end,
		},
	},
};


-- controlPanel(SF)
	-- hudGeometry(TS)
CPG_SF_CP_TS.hudGeometry.object = {};
CPG_SF_CP_TS.hudGeometry.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			template = "OptionsFrameTabButtonTemplate",
			wrapperName = "HG",
		},
		miscellaneous = {
			gxname = "Tab2",
			hidden = false,
			enabled = false,
			text = "Hud Geometry",
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Right,
			offsetX = function() return MSC_CONFIG.tabSwitchHorGap() end,
			offsetY = 0,	-- fwd
		},
		callback = {
			onClick = function(...) This:on_hudGeometryTS_clicked(...) end,
		},
	},
};


-- controlPanel(SF)
	-- loggingManager(TS)
CPG_SF_CP_TS.loggingManager.object = {};
CPG_SF_CP_TS.loggingManager.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			template = "OptionsFrameTabButtonTemplate",
			wrapperName = "LM",
		},
		miscellaneous = {
			gxname = "Tab3",
			hidden = false,
			enabled = false,
			text = "Logging Manager",
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Right,
			offsetX = function() return MSC_CONFIG.tabSwitchHorGap() end,
			offsetY = 0,	-- fwd
		},
		callback = {
			onClick = function(...) This:on_loggingManagerTS_clicked(...) end,
		},
	},
};


-- controlPanel(SF)
	-- effectManager(TS)
CPG_SF_CP_TS.effectManager.object = {};
CPG_SF_CP_TS.effectManager.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			template = "OptionsFrameTabButtonTemplate",
			wrapperName = "EM",
		},
		miscellaneous = {
			gxname = "Tab4",
			hidden = false,
			enabled = false,
			text = "Effect Manager",
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Right,
			offsetX = function() return MSC_CONFIG.tabSwitchHorGap() end,
			offsetY = 0,	-- fwd
		},
		callback = {
			onClick = function(...) This:on_effectManagerTS_clicked(...) end,
		},
	},
};