local This = GGF.ModuleGetWrapper("GUI-5-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- api
-- Смена видимости основного фрейма
function This:setHidden(in_vis)
	GGF.OuterInvoke(self.JR_CP_SF_OBJ, "SetHidden", in_vis);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Взятие объекта главного окна
function This:getLeadingFrame()
	return GGF.INS.GetObjectRef(self.JR_CP_SF_OBJ);
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Установка линейных размеров дочерних элементов
function This:adjustSize(in_width, in_height)
	--self.JR_CP_F_OBJ:SetWidth(in_width);
	--self.JR_CP_F_OBJ:SetHeight(in_height);

	--local width, height = self.JR_CP_F_OBJ:GetWidth(), self.JR_CP_F_OBJ:GetHeight();
	--local offsetNT, offsetT = GGM.GUI.MainWindow:GetChildTabbedFrameOffsetX(), GGM.GUI.MainWindow:GetChildTabbedFrameOffsetY();

	--GGM.GUI.CommonSettings:AdjustSize(width - 2*offsetNT, height - offsetNT - math.abs(offsetT));
	--This_EM:AdjustSize(width - 2*offsetNT, height - offsetNT - math.abs(offsetT));
	--GGM.GUI.HudGeometry:AdjustSize(width - 2*offsetNT, height - offsetNT - math.abs(offsetT));
	--GGM.GUI.LoggingManager:AdjustSize(width - 2*offsetNT, height - offsetNT - math.abs(offsetT));
end


-- Компактный переключатель панелей
function This:managePanels(in_panelNum)
	ATLASSERT(in_panelNum > 0 and in_panelNum < 5);
	local pn1, pn2, pn3, pn4 = (in_panelNum == 1), (in_panelNum == 2), (in_panelNum == 3), (in_panelNum == 4);

	GGF.OuterInvoke(self.JR_CP_SF_CS_TS_OBJ, "SetEnabled", pn1);
	GGF.OuterInvoke(self.JR_CP_SF_HG_TS_OBJ, "SetEnabled", pn2);
	GGF.OuterInvoke(self.JR_CP_SF_LM_TS_OBJ, "SetEnabled", pn3);
	GGF.OuterInvoke(self.JR_CP_SF_EM_TS_OBJ, "SetEnabled", pn4);

	GGF.OuterInvoke(self.JR_CP_SF_CS_TS_OBJ, "SetHighlighted", pn1);
	GGF.OuterInvoke(self.JR_CP_SF_HG_TS_OBJ, "SetHighlighted", pn2);
	GGF.OuterInvoke(self.JR_CP_SF_LM_TS_OBJ, "SetHighlighted", pn3);
	GGF.OuterInvoke(self.JR_CP_SF_EM_TS_OBJ, "SetHighlighted", pn4);

	PanelTemplates_SetTab(GGF.OuterInvoke(self.JR_CP_SF_OBJ, "GetStandardFrame"), in_panelNum);

	GGM.GUI.CommonSettings:SetHidden(not pn1); -- проектируется(не реализовано)
	GGM.GUI.HudGeometry:SetHidden(not pn2);
	GGM.GUI.LoggingManager:SetHidden(not pn3);
	GGM.GUI.EffectManager:SetHidden(not pn4);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


--
function This:on_commonSettingsTS_clicked()
	self:managePanels(1);
end


--
function This:on_hudGeometryTS_clicked()
	self:managePanels(2);
end


--
function This:on_loggingManagerTS_clicked()
	self:managePanels(3);
end


--
function This:on_effectManagerTS_clicked()
	self:managePanels(4);
end


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------