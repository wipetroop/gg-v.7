local This = GGF.ModuleGetWrapper("GUI-5-2-2-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_audioControllerGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		audioController = {		-- [AC_SF]
		},
	}),
};

local ACG_SF = This.oc_audioControllerGeometry.standardFrame;

-- audioController(SF)
ACG_SF.audioController.object = {};
ACG_SF.audioController.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "AC",
		},
		miscellaneous = {
			hidden = true,
		},
		size = {
			width = function() return MSC_CONFIG.frameWidth() end,
			height = function() return MSC_CONFIG.frameHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Segregate,
	},
};