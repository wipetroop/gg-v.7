local This = GGF.ModuleGetWrapper("GUI-5-2-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- api
-- Смена видимости основного фрейма
function This:setHidden(in_vis)
	GGF.OuterInvoke(self.JR_EM_SF_OBJ, "SetHidden", in_vis);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Взятие объекта главного окна
function This:getLeadingFrame()
	return GGF.INS.GetObjectRef(self.JR_EM_SF_OBJ);
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Установка линейных размеров дочерних элементов
function This:adjustSize(in_width, in_height)
	--self.JR_EM_F_OBJ:SetWidth(in_width);
	--self.JR_EM_F_OBJ:SetHeight(in_height);

	--local width, height = self.JR_EM_F_OBJ:GetWidth(), self.JR_EM_F_OBJ:GetHeight();
	--local offsetNT, offsetT = GGM.GUI.MainWindow:GetChildTabbedFrameOffsetX(), GGM.GUI.MainWindow:GetChildTabbedFrameOffsetY();

	--GGM.GUI.AudioController:AdjustSize(width - 2*offsetNT, height - offsetNT - math.abs(offsetT));
	--GGM.GUI.VideoController:AdjustSize(width - 2*offsetNT, height - offsetNT - math.abs(offsetT));
end


-- Компактный переключатель панелей
function This:managePanels(in_panelNum)
	ATLASSERT(in_panelNum > 0 and in_panelNum < 3);
	local pn1, pn2 = (in_panelNum == 1), (in_panelNum == 2);

	GGF.OuterInvoke(self.JR_EM_SF_AC_TS_OBJ, "SetEnabled", pn1);
	GGF.OuterInvoke(self.JR_EM_SF_VC_TS_OBJ, "SetEnabled", pn2);

	GGF.OuterInvoke(self.JR_EM_SF_AC_TS_OBJ, "SetHighlighted", pn1);
	GGF.OuterInvoke(self.JR_EM_SF_VC_TS_OBJ, "SetHighlighted", pn2);

	PanelTemplates_SetTab(GGF.INS.GetObjectRef(self.JR_EM_SF_OBJ), in_panelNum);

	GGM.GUI.AudioController:SetHidden(not pn1);	-- проектируется(не реализовано)
	GGM.GUI.VideoController:SetHidden(not pn2);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


--
function This:on_audioControllerTS_clicked()
	self:managePanels(1);
end


--
function This:on_videoControllerTS_clicked()
	self:managePanels(2);
end


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------