local This = GGF.ModuleGetWrapper("GUI-5-2-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------

	-- effectManager(SF)

	self.JR_EM_SF_OBJ = GGC.StandardFrame:Create(self.JR_EM_SF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.ControlPanel:GetLeadingFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.ControlPanel:GetLeadingFrame(),
			},
		},
	});


	-- effectManager(SF)
		-- audioController(TS)

	self.JR_EM_SF_AC_TS_OBJ = GGC.TabSwitch:Create(self.JR_EM_SF_AC_TS_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_EM_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_EM_SF_OBJ),
			},
		},
	});


	-- effectManager(SF)
		-- videoController(TS)

	self.JR_EM_SF_VC_TS_OBJ = GGC.TabSwitch:Create(self.JR_EM_SF_VC_TS_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_EM_SF_OBJ),
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_EM_SF_AC_TS_OBJ),
			},
		},
	});
end


-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- effectManager(F)
	GGF.OuterInvoke(self.JR_EM_SF_OBJ, "Adjust");


	-- effectManager(F)
		-- audioController(TS)

	GGF.OuterInvoke(self.JR_EM_SF_AC_TS_OBJ, "Adjust");


	-- effectManager(F)
		-- videoController(TS)

	GGF.OuterInvoke(self.JR_EM_SF_VC_TS_OBJ, "Adjust");
end


-- п.2.8 (After-load Settings)
function This:MPRC_ALS_INITIALIZE()
	PanelTemplates_SetNumTabs(GGF.INS.GetObjectRef(self.JR_EM_SF_OBJ), 2);	-- 2 total

	self:managePanels(2);
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------