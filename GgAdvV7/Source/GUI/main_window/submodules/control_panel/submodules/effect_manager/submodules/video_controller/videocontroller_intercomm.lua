local This = GGF.ModuleGetWrapper("GUI-5-2-2-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------

	-- videoControler(SF)

	self.JR_VC_SF_OBJ = GGC.StandardFrame:Create(self.JR_VC_SF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.EffectManager:GetLeadingFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.EffectManager:GetLeadingFrame(),
			},
		},
	});
end


-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- videoControler(SF)

	GGF.OuterInvoke(self.JR_VC_SF_OBJ, "Adjust");
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------