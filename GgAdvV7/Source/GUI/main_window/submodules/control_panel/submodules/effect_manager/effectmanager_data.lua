local This = GGF.ModuleCreateWrapper("GUI-5-2-2", false, {
	frameWidth = function() return GGM.GUI.ParameterStorage:GetMWFrameWidth() end,
	frameHeight = function() return GGM.GUI.ParameterStorage:GetCPEMFrameHeight() end,

	buttonWidth = function() return GGM.GUI.ParameterStorage:GetButtonWidth() end,
	buttonHeight = function() return GGM.GUI.ParameterStorage:GetButtonHeight() end,

	tabSwitchHorGap = function() return GGM.GUI.ParameterStorage:GetTabSwitchHorGap() end,
});

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant strings
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable strings
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------