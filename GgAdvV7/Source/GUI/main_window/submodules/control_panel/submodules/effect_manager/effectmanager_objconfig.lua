local This = GGF.ModuleGetWrapper("GUI-5-2-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_effectManagerGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		effectManager = {		-- [EM_SF]
			inherit = {
				tabSwitch = GGF.GTemplateTree(GGC.TabSwitch, {
					audioController = {},		-- [AC_TS]
					videoController = {},		-- [VC_TS]
				}),
			},
		},
	}),
};

local EMG_SF = This.oc_effectManagerGeometry.standardFrame;
local EMG_SF_EM_TS = EMG_SF.effectManager.inherit.tabSwitch;

-- effectManager(SF)
EMG_SF.effectManager.object = {};
EMG_SF.effectManager.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "EM",
		},
		miscellaneous = {
			hidden = true,
		},
		size = {
			width = function() return MSC_CONFIG.frameWidth() end,
			height = function() return MSC_CONFIG.frameHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Segregate,
	},
};


-- effectManager(SF)
	-- audioController(TS)
EMG_SF_EM_TS.audioController.object = {};
EMG_SF_EM_TS.audioController.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			template = "OptionsFrameTabButtonTemplate",
			wrapperName = "AC",
		},
		miscellaneous = {
			gxname = "Tab1",
			hidden = false,
			enabled = false,
			highlighted = false,

			text = "Audio",
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.BottomLeft,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.BottomLeft,
			offsetX = 0,
			offsetY = 0,
		},
		callback = {
			onClick = function(...) This:on_audioControllerTS_clicked(...) end,
		},
	},
};


-- effectManager(F)
	-- videoController(TS)
EMG_SF_EM_TS.videoController.object = {};
EMG_SF_EM_TS.videoController.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			template = "OptionsFrameTabButtonTemplate",
			wrapperName = "VC",
		},
		miscellaneous = {
			gxname = "Tab2",
			hidden = false,
			enabled = false,
			highlighted = false,

			text = "Video",
			--tooltipText = "GUi_MW_F_CP_F_CSTS_B",
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Right,
			offsetX = function() return MSC_CONFIG.tabSwitchHorGap() end,
			offsetY = 0,
		},
		callback = {
			onClick = function(...) This:on_videoControllerTS_clicked(...) end,
		},
	},
};