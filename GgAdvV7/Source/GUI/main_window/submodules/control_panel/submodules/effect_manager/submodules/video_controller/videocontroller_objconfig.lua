local This = GGF.ModuleGetWrapper("GUI-5-2-2-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_videoControllerGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		videoController = {		-- [VC_SF]
		},
	}),
};

local VCG_SF = This.oc_videoControllerGeometry.standardFrame;

-- videoController(SF)
VCG_SF.videoController.object = {};
VCG_SF.videoController.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "VC",
		},
		miscellaneous = {
			hidden = true,
		},
		size = {
			width = function() return MSC_CONFIG.frameWidth() end,
			height = function() return MSC_CONFIG.frameHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Segregate,
	},
};