local This = GGF.ModuleGetWrapper("GUI-5-2-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.1 (Miscellaneous Variables)
function This:MPRC_MVR_INITIALIZE()
	--if (true) then return; end
	--self.vs_hInstance.gUi = LibStub("AceGUi-3.0");
	-- TODO: проверить расчеты!!!(во всем модуле)
	--local lowerLineUniHeight = math.floor(- GGM.FCS.RuntimeStorage:GetScreenHeight()*4.53/5.0);


	-- TODO: переделать с учетом возможности убрать панели комманд
	-- пет панель
	--local mmb_pt, mmb_rt, mmb_rp, mmb_ox, mmb_oy = MainMenuBar:GetPoint();
	--local pabf_pt, pabf_rt, pabf_rp, pabf_ox, pabf_oy = PetActionBarFrame:GetPoint();
	--local mbbr_pt, mbbr_rt, mbbr_rp, mbbr_ox, mbbr_oy = MultiBarBottomRight:GetPoint();
	--local mbbl_pt, mbbl_rt, mbbl_rp, mbbl_ox, mbbl_oy = MultiBarBottomLeft:GetPoint();
	--local mbbr_vs = MultiBarBottomRight:IsVisible();
	--local mbbl_vs = MultiBarBottomLeft:IsVisible();
	--local mbbr_wd, mbbr_hg = MultiBarBottomRight:GetSize();
	--local mbbl_wd, mbbl_hg = MultiBarBottomLeft:GetSize();
	--local mbb_wd, mbb_hg = MainMenuBar:GetSize();

	-- Расчет выполнен исходя из линейных размеров 500х50
	--local panelgap = 5;
	--self.JR_HG_F_MD_F_OXSWR_F_TMP.attributes.value = 0.5*GGM.FCS.RuntimeStorage:GetScreenWidth() - mbbl_wd/2 - panelgap - 0.5*GGM.GUI.MaintenanceDashboard:GetDashboardPanelWidth(); -- половина ширины + половина зазора м-у доп панелями
	--local multibargap = 0;
	--if (mbbl_vs) then
		--multibargap = mbbl_hg + mbbl_oy;	-- высота + зазор
	--end
	--self.JR_HG_F_MD_F_OYSWR_F_TMP.attributes.value = lowerLineUniHeight + GGM.GUI.MaintenanceDashboard:GetDashboardPanelHeight();

	--print("mmb: ", mmb_pt, mmb_rt:GetName(), mmb_rp, mmb_ox, mmb_oy);
	--print("pabf: ", pabf_pt, pabf_rt:GetName(), pabf_rp, pabf_ox, pabf_oy);
	--print("mbbr: ", mbbr_pt, mbbr_rt:GetName(), mbbr_rp, mbbr_ox, mbbr_oy);
	--print("mbbl: ", mbbl_pt, mbbl_rt:GetName(), mbbl_rp, mbbl_ox, mbbl_oy);
	--print("vis", mbbr_vs, mbbl_vs);
	--print("sz", mbbr_wd, mbbr_hg);

	--self.JR_HG_F_DD_F_OXSWR_F_TMP.attributes.value = 0.5*GGM.FCS.RuntimeStorage:GetScreenWidth() + mbbr_wd/2 + panelgap - 0.5*GGM.GUI.DrivecontrolDashboard:GetDashboardPanelWidth();
	--self.JR_HG_F_DD_F_OYSWR_F_TMP.attributes.value = lowerLineUniHeight + GGM.GUI.DrivecontrolDashboard:GetDashboardPanelHeight();

	--NOTE: тут лютая дичь...так что ставим все по умолчанию и слайдерами через конфиг...
	--self:resetPetActionBar();

	-- фрейм лога
	--self.JR_HG_F_LF_F_OXSWR_F_TMP.attributes.value = 0;
	--self.JR_HG_F_LF_F_OYSWR_F_TMP.attributes.value = 0;
end


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	-- NOTE: Сделано на случай замены переменной хранения гуи
	--local aceGUiHandler = self.vs_hInstance.gUi;
	-- TODO: решить вопрос с бфа
	local MSC_CONFIG = self:getConfig();

	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------

	-- hudGeometry(SF)

	self.JR_HG_SF_OBJ = GGC.StandardFrame:Create(self.JR_HG_SF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.ControlPanel:GetLeadingFrame(),
			},
			size = {
				--height = function() return GGF.OuterInvoke(self.JR_HG_SF_SD_PGC_OBJ, "GetHeight") - 2*GGF.OuterInvoke(self.JR_HG_SF_SD_PGC_OBJ, "GetOffsetY") end,
			},
			anchor = {
				relativeTo = GGM.GUI.ControlPanel:GetLeadingFrame(),
			},
		},
	});

	-- NOTE: Пока что непонятно, что будет вместо старых контролов, но они больше скорее всего не нужны
	if (true) then
		return true;
	end

	-- hudGeometry(SF)
		-- systemDashboard(PGC)

	self.JR_HG_SF_SD_PGC_OBJ = GGC.PanelGeometryController:Create(self.JR_HG_SF_SD_PGC_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_HG_SF_OBJ),
			},
			values = {
				X = {
					common = function() return 0 end,
					charspec = function() return 0 end,
				},
				Y = {
					common = function() return - math.floor(GGM.FCS.RuntimeStorage:GetScreenHeight()*4.0/5.2) end,
					charspec = function() return - math.floor(GGM.FCS.RuntimeStorage:GetScreenHeight()*4.0/5.2) end,
				},
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_HG_SF_OBJ),
			},
		},
	});


	-- hudGeometry(SF)
		-- combatDashboard(PGC)

	self.JR_HG_SF_CD_PGC_OBJ = GGC.PanelGeometryController:Create(self.JR_HG_SF_CD_PGC_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_HG_SF_OBJ),
			},
			values = {
				X = {
					common = function() return math.floor(GGM.FCS.RuntimeStorage:GetScreenWidth()) end,
					charspec = function() return math.floor(GGM.FCS.RuntimeStorage:GetScreenWidth()) end,
				},
				Y = {
					common = function() return - math.floor(GGM.FCS.RuntimeStorage:GetScreenHeight()*4.0/5.2) end,
					charspec = function() return - math.floor(GGM.FCS.RuntimeStorage:GetScreenHeight()*4.0/5.2) end,
				},
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_HG_SF_SD_PGC_OBJ),
			},
		},
	});


	-- hudGeometry(SF)
		-- maintenanceDashboard(PGC)

	self.JR_HG_SF_MD_PGC_OBJ = GGC.PanelGeometryController:Create(self.JR_HG_SF_MD_PGC_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_HG_SF_OBJ),
			},
			values = {
				X = {
					common = function() return math.floor(GGM.FCS.RuntimeStorage:GetScreenWidth()/2.0) + MSC_CONFIG.centralPanelGap end,
					charspec = function() return math.floor(GGM.FCS.RuntimeStorage:GetScreenWidth()/2.0) + MSC_CONFIG.centralPanelGap end,
				},
				Y = {
					common = function()
						local mbbl_hg = MultiBarBottomLeft:GetHeight();
						return - GGM.FCS.RuntimeStorage:GetScreenHeight() + GGM.GUI.MaintenanceDashboard:GetDashboardPanelHeight() + 2*mbbl_hg + 2*MSC_CONFIG.skillBarGap;
					end,
					charspec = function()
						local mbbl_hg = MultiBarBottomLeft:GetHeight();
						return - GGM.FCS.RuntimeStorage:GetScreenHeight() + GGM.GUI.MaintenanceDashboard:GetDashboardPanelHeight() + 2*mbbl_hg + 2*MSC_CONFIG.skillBarGap;
					end,
				},
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_HG_SF_CD_PGC_OBJ),
			},
		},
	});


	-- hudGeometry(SF)
		-- drivecontrolDashboard(PGC)

	self.JR_HG_SF_DD_PGC_OBJ = GGC.PanelGeometryController:Create(self.JR_HG_SF_DD_PGC_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_HG_SF_OBJ),
			},
			values = {
				X = {
					common = function() return math.floor(GGM.FCS.RuntimeStorage:GetScreenWidth()/2.0) - MSC_CONFIG.centralPanelGap - GGM.GUI.DrivecontrolDashboard:GetDashboardPanelWidth() end,
					charspec = function() return math.floor(GGM.FCS.RuntimeStorage:GetScreenWidth()/2.0) - MSC_CONFIG.centralPanelGap - GGM.GUI.DrivecontrolDashboard:GetDashboardPanelWidth() end,
				},
				Y = {
					common = function()
						local mbbl_hg = MultiBarBottomLeft:GetHeight();
						return - GGM.FCS.RuntimeStorage:GetScreenHeight() + GGM.GUI.MaintenanceDashboard:GetDashboardPanelHeight() + 2*mbbl_hg + 2*MSC_CONFIG.skillBarGap;
					end,
					charspec = function()
						local mbbl_hg = MultiBarBottomLeft:GetHeight();
						return - GGM.FCS.RuntimeStorage:GetScreenHeight() + GGM.GUI.MaintenanceDashboard:GetDashboardPanelHeight() + 2*mbbl_hg + 2*MSC_CONFIG.skillBarGap;
					end,
				},
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_HG_SF_MD_PGC_OBJ),
			},
		},
	});


	-- hudGeometry(SF)
		-- killFeed(PGC)

	self.JR_HG_SF_KF_PGC_OBJ = GGC.PanelGeometryController:Create(self.JR_HG_SF_KF_PGC_TMP, {
		properties = {
			base = {
				parent = GGF.INS.GetObjectRef(self.JR_HG_SF_OBJ),
			},
			values = {
				X = {
					common = 0,
					charspec = 0,
				},
				Y = {
					common = 0,
					charspec = 0,
				},
			},
			anchor = {
				relativeTo = GGF.INS.GetObjectRef(self.JR_HG_SF_DD_PGC_OBJ),
			},
		},
	});
end


-- п.2.4 (Auxiliary Config Parameters)
function This:MPRC_ACP_INITIALIZE()
	-- TODO: убрать заглушки, присобачив чекбоксы
	-- NOTE: а где заглушки то?
	if (true) then return; end
	-- hudGeometry(F)
		-- systemDashboard(F)
			-- visibility(CB)

	self:assignConfigParameter({ 
		token = "SD_V_CB_STT",
		vstr = self.JR_HG_F_SD_F_V_CB_OBJ.SetChecked,
		vgtr = self.JR_HG_F_SD_F_V_CB_OBJ.GetChecked,
		vobj = self.JR_HG_F_SD_F_V_CB_OBJ,
		cstr = self.JR_HG_F_SD_F_C_CB_OBJ.SetChecked,
		cgtr = self.JR_HG_F_SD_F_C_CB_OBJ.GetChecked,
		cobj = self.JR_HG_F_SD_F_C_CB_OBJ,
	});

	self:adjustConfigParameter("SD_V_CB_STT");


	-- This(F)
		-- GGM.GUI.SystemDashboard(F)
			-- charspec(CB)

	self:assignConfigParameter({ 
		token = "SD_C_CB_STT",
		vstr = self.JR_HG_F_SD_F_C_CB_OBJ.SetChecked,
		vgtr = self.JR_HG_F_SD_F_C_CB_OBJ.GetChecked,
		vobj = self.JR_HG_F_SD_F_C_CB_OBJ,
		cstr = function() end,
		cgtr = function() return true end,
		cobj = "",
	});

	self:adjustConfigParameter("SD_C_CB_STT");


	-- This(F)
		-- systemDashboard(F)
			-- offsetXSliderWrapper(F)

	self:assignConfigParameter({ 
		token = "SD_OXSWR_F_VAL",
		vstr = self.JR_HG_F_SD_F_OXSWR_F_OBJ.SetValue,
		vgtr = self.JR_HG_F_SD_F_OXSWR_F_OBJ.GetValue,
		vobj = self.JR_HG_F_SD_F_OXSWR_F_OBJ,
		cstr = self.JR_HG_F_SD_F_C_CB_OBJ.SetChecked,
		cgtr = self.JR_HG_F_SD_F_C_CB_OBJ.GetChecked,
		cobj = self.JR_HG_F_SD_F_C_CB_OBJ,
	});

	self:adjustConfigParameter("SD_OXSWR_F_VAL");


	-- This(F)
		-- systemDashboard(F)
			-- offsetYSliderWrapper(F)

	self:assignConfigParameter({ 
		token = "SD_OYSWR_F_VAL",
		vstr = self.JR_HG_F_SD_F_OYSWR_F_OBJ.SetValue,
		vgtr = self.JR_HG_F_SD_F_OYSWR_F_OBJ.GetValue,
		vobj = self.JR_HG_F_SD_F_OYSWR_F_OBJ,
		cstr = self.JR_HG_F_SD_F_C_CB_OBJ.SetChecked,
		cgtr = self.JR_HG_F_SD_F_C_CB_OBJ.GetChecked,
		cobj = self.JR_HG_F_SD_F_C_CB_OBJ,
	});

	self:adjustConfigParameter("SD_OYSWR_F_VAL");


	-- This(F)
		-- combatDashboard(F)
			-- visibility(CB)

	self:assignConfigParameter({ 
		token = "CD_V_CB_STT",
		vstr = self.JR_HG_F_CD_F_V_CB_OBJ.SetChecked,
		vgtr = self.JR_HG_F_CD_F_V_CB_OBJ.GetChecked,
		vobj = self.JR_HG_F_CD_F_V_CB_OBJ,
		cstr = self.JR_HG_F_CD_F_C_CB_OBJ.SetChecked,
		cgtr = self.JR_HG_F_CD_F_C_CB_OBJ.GetChecked,
		cobj = self.JR_HG_F_CD_F_C_CB_OBJ,
	});

	self:adjustConfigParameter("CD_V_CB_STT");


	-- This(F)
		-- combatDashboard(F)
			-- charspec(CB)

	self:assignConfigParameter({ 
		token = "CD_C_CB_STT",
		vstr = self.JR_HG_F_CD_F_C_CB_OBJ.SetChecked,
		vgtr = self.JR_HG_F_CD_F_C_CB_OBJ.GetChecked,
		vobj = self.JR_HG_F_CD_F_C_CB_OBJ,
		cstr = function() end,
		cgtr = function() return true end,
		cobj = "",
	});

	self:adjustConfigParameter("CD_C_CB_STT");


	-- This(F)
		-- combatDashboard(F)
			-- offsetXSliderWrapper(F)

	self:assignConfigParameter({ 
		token = "CD_OXSWR_F_VAL",
		vstr = self.JR_HG_F_CD_F_OXSWR_F_OBJ.SetValue,
		vgtr = self.JR_HG_F_CD_F_OXSWR_F_OBJ.GetValue,
		vobj = self.JR_HG_F_CD_F_OXSWR_F_OBJ,
		cstr = self.JR_HG_F_CD_F_C_CB_OBJ.SetChecked,
		cgtr = self.JR_HG_F_CD_F_C_CB_OBJ.GetChecked,
		cobj = self.JR_HG_F_CD_F_C_CB_OBJ,
	});

	self:adjustConfigParameter("CD_OXSWR_F_VAL");


	-- This(F)
		-- combatDashboard(F)
			-- offsetYSliderWrapper(S)

	self:assignConfigParameter({ 
		token = "CD_OYSWR_F_VAL",
		vstr = self.JR_HG_F_CD_F_OYSWR_F_OBJ.SetValue,
		vgtr = self.JR_HG_F_CD_F_OYSWR_F_OBJ.GetValue,
		vobj = self.JR_HG_F_CD_F_OYSWR_F_OBJ,
		cstr = self.JR_HG_F_CD_F_C_CB_OBJ.SetChecked,
		cgtr = self.JR_HG_F_CD_F_C_CB_OBJ.GetChecked,
		cobj = self.JR_HG_F_CD_F_C_CB_OBJ,
	});

	self:adjustConfigParameter("CD_OYSWR_F_VAL");


	-- This(F)
		-- maintenanceDashboard(F)
			-- visibility(CB)

	self:assignConfigParameter({ 
		token = "MD_V_CB_STT",
		vstr = self.JR_HG_F_MD_F_V_CB_OBJ.SetChecked,
		vgtr = self.JR_HG_F_MD_F_V_CB_OBJ.GetChecked,
		vobj = self.JR_HG_F_MD_F_V_CB_OBJ,
		cstr = self.JR_HG_F_MD_F_C_CB_OBJ.SetChecked,
		cgtr = self.JR_HG_F_MD_F_C_CB_OBJ.GetChecked,
		cobj = self.JR_HG_F_MD_F_C_CB_OBJ,
	});

	self:adjustConfigParameter("MD_V_CB_STT");


	-- This(F)
		-- maintenanceDashboard(F)
			-- charspec(CB)

	self:assignConfigParameter({ 
		token = "MD_C_CB_STT",
		vstr = self.JR_HG_F_MD_F_C_CB_OBJ.SetChecked,
		vgtr = self.JR_HG_F_MD_F_C_CB_OBJ.GetChecked,
		vobj = self.JR_HG_F_MD_F_C_CB_OBJ,
		cstr = function() end,
		cgtr = function() return true end,
		cobj = "",
	});

	self:adjustConfigParameter("MD_C_CB_STT");


	-- This(F)
		-- maintenanceDashboard(F)
			-- offsetXSliderWrapper(F)

	self:assignConfigParameter({ 
		token = "MD_OXSWR_F_VAL",
		vstr = self.JR_HG_F_MD_F_OXSWR_F_OBJ.SetValue,
		vgtr = self.JR_HG_F_MD_F_OXSWR_F_OBJ.GetValue,
		vobj = self.JR_HG_F_MD_F_OXSWR_F_OBJ,
		cstr = self.JR_HG_F_MD_F_C_CB_OBJ.SetChecked,
		cgtr = self.JR_HG_F_MD_F_C_CB_OBJ.GetChecked,
		cobj = self.JR_HG_F_MD_F_C_CB_OBJ,
	});

	self:adjustConfigParameter("MD_OXSWR_F_VAL");


	-- This(F)
		-- maintenanceDashboard(F)
			-- offsetYSliderWrapper(F)

	self:assignConfigParameter({ 
		token = "MD_OYSWR_F_VAL",
		vstr = self.JR_HG_F_MD_F_OYSWR_F_OBJ.SetValue,
		vgtr = self.JR_HG_F_MD_F_OYSWR_F_OBJ.GetValue,
		vobj = self.JR_HG_F_MD_F_OYSWR_F_OBJ,
		cstr = self.JR_HG_F_MD_F_C_CB_OBJ.SetChecked,
		cgtr = self.JR_HG_F_MD_F_C_CB_OBJ.GetChecked,
		cobj = self.JR_HG_F_MD_F_C_CB_OBJ,
	});

	self:adjustConfigParameter("MD_OYSWR_F_VAL");


	-- This(F)
		-- drivecontrolDashboard(F)
			-- visibility(CB)

	self:assignConfigParameter({ 
		token = "DD_V_CB_STT",
		vstr = self.JR_HG_F_DD_F_V_CB_OBJ.SetChecked,
		vgtr = self.JR_HG_F_DD_F_V_CB_OBJ.GetChecked,
		vobj = self.JR_HG_F_DD_F_V_CB_OBJ,
		cstr = self.JR_HG_F_DD_F_C_CB_OBJ.SetChecked,
		cgtr = self.JR_HG_F_DD_F_C_CB_OBJ.GetChecked,
		cobj = self.JR_HG_F_DD_F_C_CB_OBJ,
	});

	self:adjustConfigParameter("DD_V_CB_STT");


	-- This(F)
		-- drivecontrolDashboard(F)
			-- charspec(CB)

	self:assignConfigParameter({ 
		token = "DD_C_CB_STT",
		vstr = self.JR_HG_F_DD_F_C_CB_OBJ.SetChecked,
		vgtr = self.JR_HG_F_DD_F_C_CB_OBJ.GetChecked,
		vobj = self.JR_HG_F_DD_F_C_CB_OBJ,
		cstr = function() end,
		cgtr = function() return true end,
		cobj = "",
	});

	self:adjustConfigParameter("DD_C_CB_STT");


	-- This(F)
		-- drivecontrolDashboard(F)
			-- offsetXSliderWrapper(F)

	self:assignConfigParameter({ 
		token = "DD_OXSWR_F_VAL",
		vstr = self.JR_HG_F_DD_F_OXSWR_F_OBJ.SetValue,
		vgtr = self.JR_HG_F_DD_F_OXSWR_F_OBJ.GetValue,
		vobj = self.JR_HG_F_DD_F_OXSWR_F_OBJ,
		cstr = self.JR_HG_F_DD_F_C_CB_OBJ.SetChecked,
		cgtr = self.JR_HG_F_DD_F_C_CB_OBJ.GetChecked,
		cobj = self.JR_HG_F_DD_F_C_CB_OBJ,
	});

	self:adjustConfigParameter("DD_OXSWR_F_VAL");


	-- This(F)
		-- drivecontrolDashboard(F)
			-- offsetYSliderWrapper(F)

	self:assignConfigParameter({ 
		token = "DD_OYSWR_F_VAL",
		vstr = self.JR_HG_F_DD_F_OYSWR_F_OBJ.SetValue,
		vgtr = self.JR_HG_F_DD_F_OYSWR_F_OBJ.GetValue,
		vobj = self.JR_HG_F_DD_F_OYSWR_F_OBJ,
		cstr = self.JR_HG_F_DD_F_C_CB_OBJ.SetChecked,
		cgtr = self.JR_HG_F_DD_F_C_CB_OBJ.GetChecked,
		cobj = self.JR_HG_F_DD_F_C_CB_OBJ,
	});

	self:adjustConfigParameter("DD_OYSWR_F_VAL");


	-- This(F)
		-- loggerFrame(F)
			-- visibility(CB)

	self:assignConfigParameter({ 
		token = "LF_V_CB_STT",
		vstr = self.JR_HG_F_LF_F_V_CB_OBJ.SetChecked,
		vgtr = self.JR_HG_F_LF_F_V_CB_OBJ.GetChecked,
		vobj = self.JR_HG_F_LF_F_V_CB_OBJ,
		cstr = self.JR_HG_F_SD_F_C_CB_OBJ.SetChecked,
		cgtr = self.JR_HG_F_SD_F_C_CB_OBJ.GetChecked,
		cobj = self.JR_HG_F_SD_F_C_CB_OBJ,
	});

	self:adjustConfigParameter("LF_V_CB_STT");


	-- This(F)
		-- loggerFrame(F)
			-- charspec(CB)

	self:assignConfigParameter({ 
		token = "LF_C_CB_STT",
		vstr = self.JR_HG_F_SD_F_C_CB_OBJ.SetChecked,
		vgtr = self.JR_HG_F_SD_F_C_CB_OBJ.GetChecked,
		vobj = self.JR_HG_F_SD_F_C_CB_OBJ,
		cstr = function() end,
		cgtr = function() return true end,
		cobj = "",
	});

	self:adjustConfigParameter("LF_C_CB_STT");


	-- This(F)
		-- loggerFrame(F)
			-- offsetXSliderWrapper(F)

	self:assignConfigParameter({ 
		token = "LF_OXSWR_F_VAL",
		vstr = self.JR_HG_F_LF_F_OXSWR_F_OBJ.SetValue,
		vgtr = self.JR_HG_F_LF_F_OXSWR_F_OBJ.GetValue,
		vobj = self.JR_HG_F_LF_F_OXSWR_F_OBJ,
		cstr = self.JR_HG_F_SD_F_C_CB_OBJ.SetChecked,
		cgtr = self.JR_HG_F_SD_F_C_CB_OBJ.GetChecked,
		cobj = self.JR_HG_F_SD_F_C_CB_OBJ,
	});

	self:adjustConfigParameter("LF_OXSWR_F_VAL");


	-- This(F)
		-- loggerFrame(F)
			-- offsetYSliderWrapper(S)

	self:assignConfigParameter({ 
		token = "LF_OYSWR_F_VAL",
		vstr = self.JR_HG_F_LF_F_OYSWR_F_OBJ.SetValue,
		vgtr = self.JR_HG_F_LF_F_OYSWR_F_OBJ.GetValue,
		vobj = self.JR_HG_F_LF_F_OYSWR_F_OBJ,
		cstr = self.JR_HG_F_SD_F_C_CB_OBJ.SetChecked,
		cgtr = self.JR_HG_F_SD_F_C_CB_OBJ.GetChecked,
		cobj = self.JR_HG_F_SD_F_C_CB_OBJ,
	});

	self:adjustConfigParameter("LF_OYSWR_F_VAL");
end


-- п.2.6 (Main Event Callbacks)
function This:MPRC_MEC_INITIALIZE()
	PetActionBarFrame:SetScript("OnHide", function() This:resetPetActionBar() end);
	PetActionBarFrame:SetScript("OnShow", function() This:resetPetActionBar() end);
	-- TODO: Уточнить патч, когда появилась анимированная иконка персонажа
	GGF.VersionSplitterProc({
		{
			versionInterval = {GGD.TocVersionList.Vanilla, GGD.TocVersionList.WotLK},
			action = function()
        	end,
    	},
    	{
         versionInterval = {GGD.TocVersionList.Cata, GGD.TocVersionList.Invalid},
         action = function()
            StanceBarFrame:SetScript("OnHide", function() This:resetStanceBar() end);
            StanceBarFrame:SetScript("OnShow", function() This:resetStanceBar() end);
         end
      }
   });
end


-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- hudGeometry(SF)

	GGF.OuterInvoke(self.JR_HG_SF_OBJ, "Adjust");

	if (true) then
		return true;
	end

	-- hudGeometry(SF)
		-- systemDashboard(PGC)
	GGF.OuterInvoke(self.JR_HG_SF_SD_PGC_OBJ, "Adjust");


	-- hudGeometry(SF)
		-- combatDashboard(PGC)

	GGF.OuterInvoke(self.JR_HG_SF_CD_PGC_OBJ, "Adjust");


	-- hudGeometry(SF)
		-- maintenanceDashboard(PGC)

	GGF.OuterInvoke(self.JR_HG_SF_MD_PGC_OBJ, "Adjust");


	-- hudGeometry(SF)
		-- drivecontrolDashboard(PGC)

	GGF.OuterInvoke(self.JR_HG_SF_DD_PGC_OBJ, "Adjust");


	-- hudGeometry(SF)
		-- killFeed(PGC)

	GGF.OuterInvoke(self.JR_HG_SF_KF_PGC_OBJ, "Adjust");
end


-- п.2.8 (After-load Settings)
function This:MPRC_ALS_INITIALIZE()
	self:resetPetActionBar();
	self:resetStanceBar();
	if (true) then return; end
	self:on_SDCharspecCB_clicked();
	self:on_CDCharspecCB_clicked();
	self:on_MDCharspecCB_clicked();
	self:on_DDCharspecCB_clicked();
	self:on_KFCharspecCB_clicked();
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------