local This = GGF.ModuleGetWrapper("GUI-5-2-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- NOTE: повторяющаяся "geometry" в названии это не ошибка;)
-- Hierarchy
This.oc_hudGeometryGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		hudGeometry = {			-- [HG_SF]
			inherit = {
				panelGeometryController = GGF.GTemplateTree(GGC.PanelGeometryController, {
					systemDashboard = {},			-- [SD_PCG]
					combatDashboard = {},			-- [CD_PCG]
					maintenanceDashboard = {},		-- [MD_PCG]
					drivecontrolDashboard = {},		-- [DD_PCG]
					killFeed = {},					-- [KF_PCG]
				}),
			},
		},
	}),
};

local HGG_SF = This.oc_hudGeometryGeometry.standardFrame;
local HGG_SF_HG_PCG = HGG_SF.hudGeometry.inherit.panelGeometryController;


-- hudGeometry(SF)
HGG_SF.hudGeometry.object = {};
HGG_SF.hudGeometry.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "HG",
		},
		miscellaneous = {
			hidden = true,
		},
		backdrop = function() return GGF.TableCopy(MSC_CONFIG.segregateBackdrop()) end,
		size = {
			width = function() return MSC_CONFIG.frameWidth() end,
			height = function() return MSC_CONFIG.frameHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Segregate,
	},
};


-- hudGeometry(SF)
	-- systemDashboard(PGC)
HGG_SF_HG_PCG.systemDashboard.object = {};
HGG_SF_HG_PCG.systemDashboard.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "SD",
		},
		miscellaneous = {
			hidden = false,
			title = "System Dashboard",
		},
		size = {
			width = MSC_CONFIG.inlineGroupWidth,
			height = MSC_CONFIG.inlineGroupHeight,
		},
		anchor = {
			point = GGE.PointType.TopLeft,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.TopLeft,
			offsetX = MSC_CONFIG.inlineGroupCornerGap,
			offsetY = - MSC_CONFIG.inlineGroupCornerGap - MSC_CONFIG.headlineGap,
		},
		valueMeta = {
			X = {
				min = 0,
				max = function(in_wrapper) return math.floor(GGM.FCS.RuntimeStorage:GetScreenWidth() - GGM.GUI.SystemDashboard:GetDashboardPanelWidth()) end,
				--step = function(in_wrapper, in_argline) return (in_argline.valmax() - in_argline.valmin())/in_argline.stepCount end,
			},
			Y = {
				min = function(in_wrapper) return math.floor(- GGM.FCS.RuntimeStorage:GetScreenHeight() + GGM.GUI.SystemDashboard:GetDashboardPanelHeight()) end,
				max = 0,
				--step = function(in_wrapper, in_argline) return (in_argline.valmax() - in_argline.valmin())/in_argline.stepCount end,
			},
		},
		backdrop = GGD.Backdrop.Segregate,
		callback = {
			onResetButtonClick = function(...) This:on_SD_R_B_clicked(...) end,
			--onVisibilityCheckButtonClick = function(...) This:on_SD_V_CB_clicked(...) end,
			--onCharspecCheckButtonClick = function(...) This:on_SD_C_CB_clicked(...) end,
			onVisibilityCheckButtonStateChanged = function(...) This:on_SD_V_CB_stateChanged(...) end,
			--onCharspecCheckButtonStateChanged = function(...) This:on_SD_C_CB_stateChanged(...) end,
			onXSliderValueChanged = function(...) This:on_SD_OX_SW_valueChanged(...) end,
			onYSliderValueChanged = function(...) This:on_SD_OY_SW_valueChanged(...) end,
		},
	},
};



-- hudGeometry(SF)
	-- combatDashboard(PGC)
HGG_SF_HG_PCG.combatDashboard.object = {}
HGG_SF_HG_PCG.combatDashboard.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "CD",
		},
		miscellaneous = {
			hidden = false,
			title = "Combat Dashboard",
		},
		size = {
			width = MSC_CONFIG.inlineGroupWidth,
			height = MSC_CONFIG.inlineGroupHeight,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Right,
			offsetX = MSC_CONFIG.inlineGroupRowGap,
			offsetY = 0,
		},
		valueMeta = {
			X = {
				min = 0,
				max = function(in_wrapper) return math.floor(GGM.FCS.RuntimeStorage:GetScreenWidth() - GGM.GUI.CombatDashboard:GetDashboardPanelWidth()) end,
				--step = function(in_wrapper, in_argline) return (in_argline.valmax() - in_argline.valmin())/in_argline.stepCount end,
			},
			Y = {
				min = function(in_wrapper) return math.floor(- GGM.FCS.RuntimeStorage:GetScreenHeight() + GGM.GUI.CombatDashboard:GetDashboardPanelHeight()) end,
				max = 0,
				--step = function(in_wrapper, in_argline) return (in_argline.valmax() - in_argline.valmin())/in_argline.stepCount end,
			},
		},
		backdrop = GGD.Backdrop.Segregate,
		callback = {
			onResetButtonClick = function(...) This:on_CD_R_B_clicked(...) end,
			--onVisibilityCheckButtonClick = function(...) This:on_CD_V_CB_clicked(...) end,
			--onCharspecCheckButtonClick = function(...) This:on_CD_C_CB_clicked(...) end,
			onVisibilityCheckButtonStateChanged = function(...) This:on_CD_V_CB_stateChanged(...) end,
			--onCharspecCheckButtonStateChanged = function(...) This:on_CD_C_CB_stateChanged(...) end,
			onXSliderValueChanged = function(...) This:on_CD_OX_SW_valueChanged(...) end,
			onYSliderValueChanged = function(...) This:on_CD_OY_SW_valueChanged(...) end,
		},
	},
};


-- hudGeometry(SF)
	-- maintenanceDashboard(PGC)
HGG_SF_HG_PCG.maintenanceDashboard.object = {};
HGG_SF_HG_PCG.maintenanceDashboard.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "MD",
		},
		miscellaneous = {
			hidden = false,
			title = "Maintenance Dashboard",
		},
		size = {
			width = MSC_CONFIG.inlineGroupWidth,
			height = MSC_CONFIG.inlineGroupHeight,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Right,
			offsetX = MSC_CONFIG.inlineGroupRowGap,
			offsetY = 0,
		},
		valueMeta = {
			X = {
				min = 0,
				max = function(in_wrapper) return math.floor(GGM.FCS.RuntimeStorage:GetScreenWidth() - GGM.GUI.MaintenanceDashboard:GetDashboardPanelWidth()) end,
				--step = function(in_wrapper, in_argline) return (in_argline.valmax() - in_argline.valmin())/in_argline.stepCount end,
			},
			Y = {
				min = function(in_wrapper) return math.floor(- GGM.FCS.RuntimeStorage:GetScreenHeight() + GGM.GUI.MaintenanceDashboard:GetDashboardPanelHeight()) end,
				max = 0,
				--step = function(in_wrapper, in_argline) return (in_argline.valmax() - in_argline.valmin())/in_argline.stepCount end,
			},
		},
		backdrop = GGD.Backdrop.Segregate,
		callback = {
			onResetButtonClick = function(...) This:on_MD_R_B_clicked(...) end,
			--onVisibilityCheckButtonClick = function(...) This:on_MD_V_CB_clicked(...) end,
			--onCharspecCheckButtonClick = function(...) This:on_MD_C_CB_clicked(...) end,
			onVisibilityCheckButtonStateChanged = function(...) This:on_MD_V_CB_stateChanged(...) end,
			--onCharspecCheckButtonStateChanged = function(...) This:on_MD_C_CB_stateChanged(...) end,
			onXSliderValueChanged = function(...) This:on_MD_OX_SW_valueChanged(...) end,
			onYSliderValueChanged = function(...) This:on_MD_OY_SW_valueChanged(...) end,
		},
	},
};


-- hudGeometry(SF)
	-- drivecontrolDashboard(PGC)
HGG_SF_HG_PCG.drivecontrolDashboard.object = {};
HGG_SF_HG_PCG.drivecontrolDashboard.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "DD",
		},
		miscellaneous = {
			hidden = false,
			title = "Drivecontrol Dashboard",
		},
		size = {
			width = MSC_CONFIG.inlineGroupWidth,
			height = MSC_CONFIG.inlineGroupHeight,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Right,
			offsetX = MSC_CONFIG.inlineGroupRowGap,
			offsetY = 0,
		},
		valueMeta = {
			X = {
				min = 0,
				max = function() return math.floor(GGM.FCS.RuntimeStorage:GetScreenWidth() - GGM.GUI.DrivecontrolDashboard:GetDashboardPanelWidth()) end,
				--step = function(in_wrapper, in_argline) return (in_argline.valmax() - in_argline.valmin())/in_argline.stepCount end,
			},
			Y = {
				min = function() return math.floor(- GGM.FCS.RuntimeStorage:GetScreenHeight() + GGM.GUI.DrivecontrolDashboard:GetDashboardPanelHeight()) end,
				max = 0,
				--step = function(in_wrapper, in_argline) return (in_argline.valmax() - in_argline.valmin())/in_argline.stepCount end,
			},
		},
		backdrop = GGD.Backdrop.Segregate,
		callback = {
			onResetButtonClick = function(...) This:on_DD_R_B_clicked(...) end,
			--onVisibilityCheckButtonClick = function(...) This:on_DD_V_CB_clicked(...) end,
			--onCharspecCheckButtonClick = function(...) This:on_DD_C_CB_clicked(...) end,
			onVisibilityCheckButtonStateChanged = function(...) This:on_DD_V_CB_stateChanged(...) end,
			--onCharspecCheckButtonStateChanged = function(...) This:on_DD_C_CB_stateChanged(...) end,
			onXSliderValueChanged = function(...) This:on_DD_OX_SW_valueChanged(...) end,
			onYSliderValueChanged = function(...) This:on_DD_OY_SW_valueChanged(...) end,
		},
	},
};


-- hudGeometry(SF)
	-- killFeed(PGC)
HGG_SF_HG_PCG.killFeed.object = {};
HGG_SF_HG_PCG.killFeed.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "KF",
		},
		miscellaneous = {
			hidden = false,
			title = "Kill Feed",
		},
		size = {
			width = MSC_CONFIG.inlineGroupWidth,
			height = MSC_CONFIG.inlineGroupHeight,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Right,
			offsetX = MSC_CONFIG.inlineGroupRowGap,
			offsetY = 0,
		},
		valueMeta = {
			X = {
				min = 0,
				max = function(in_wrapper) return math.floor(GGM.FCS.RuntimeStorage:GetScreenWidth() - GGM.GUI.KillFeed:GetWidth()) end,
				--step = function(in_wrapper, in_argline) return (in_argline.valmax() - in_argline.valmin())/in_argline.stepCount end,
			},
			Y = {
				min = function(in_wrapper) return math.floor(- GGM.FCS.RuntimeStorage:GetScreenHeight() + GGM.GUI.KillFeed:GetHeight()) end,
				max = 0,
				--step = function(in_wrapper, in_argline) return (in_argline.valmax() - in_argline.valmin())/in_argline.stepCount end,
			},
		},
		backdrop = GGD.Backdrop.Segregate,
		callback = {
			onResetButtonClick = function(...) This:on_KF_R_B_clicked(...) end,
			--onVisibilityCheckButtonClick = function(...) This:on_LF_V_CB_clicked(...) end,
			--onCharspecCheckButtonClick = function(...) This:on_LF_C_CB_clicked(...) end,
			onVisibilityCheckButtonStateChanged = function(...) This:on_KF_V_CB_stateChanged(...) end,
			--onCharspecCheckButtonStateChanged = function(...) This:on_KF_C_CB_stateChanged(...) end,
			onXSliderValueChanged = function(...) This:on_KF_OX_SW_valueChanged(...) end,
			onYSliderValueChanged = function(...) This:on_KF_OY_SW_valueChanged(...) end,
		},
	},
};