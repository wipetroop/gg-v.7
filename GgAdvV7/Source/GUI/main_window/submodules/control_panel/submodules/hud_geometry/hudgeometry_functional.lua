local This = GGF.ModuleGetWrapper("GUI-5-2-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- api
-- Смена видимости основного фрейма
function This:setHidden(in_vis)
	GGF.OuterInvoke(self.JR_HG_SF_OBJ, "SetHidden", in_vis);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Установка линейных размеров дочерних элементов
function This:adjustSize(in_width, in_height)
	GGF.OuterInvoke(self.JR_HG_SF_OBJ, "SetWidth", in_width);
	GGF.OuterInvoke(self.JR_HG_SF_OBJ, "SetHeight", in_height);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-- Ресетер значений SD
function This:on_SD_R_B_clicked()
	--self.JR_HG_SF_SD_F_V_CB_OBJ:SetChecked(self.JR_HG_F_SD_F_V_CB_TMP.attributes.checked);

	--self.JR_HG_SF_SD_F_OXSWR_F_OBJ:SetValue(self.JR_HG_F_SD_F_OXSWR_F_TMP.attributes.value);
	--self.JR_HG_SF_SD_F_OYSWR_F_OBJ:SetValue(self.JR_HG_F_SD_F_OYSWR_F_TMP.attributes.value);
end


-- Апдейтер видимости SD
function This:on_SD_V_CB_stateChanged(in_state)
	--self:updateConfigParameter("SD_V_CB_STT");
	GGM.GUI.SystemDashboard:SetDashboardPanelHidden(not in_state);
end


-- Апдейтер гм SD по оси X
function This:on_SD_OX_SW_valueChanged(in_value)
	--self:updateConfigParameter("SD_OXSWR_F_VAL");
	GGM.GUI.SystemDashboard:SetDashboardPanelOffsetX(in_value);
end


-- Апдейтер гм SD по оси Y
function This:on_SD_OY_SW_valueChanged(in_value)
	--self:updateConfigParameter("SD_OYSWR_F_VAL");
	GGM.GUI.SystemDashboard:SetDashboardPanelOffsetY(in_value);
end


-- Ресетер значений CD
function This:on_CD_R_B_clicked()
	-- TODO: добавить ресетер методы для полей объекта фреймворка
	--self.JR_HG_F_CD_F_V_CB_OBJ:SetChecked(self.JR_HG_F_CD_F_V_CB_TMP.attributes.checked);

	--self.JR_HG_F_CD_F_OXSWR_F_OBJ:SetValue(self.JR_HG_F_CD_F_OXSWR_F_TMP.attributes.value);
	--self.JR_HG_F_CD_F_OYSWR_F_OBJ:SetValue(self.JR_HG_F_CD_F_OYSWR_F_TMP.attributes.value);
end


-- Апдейтер видимости CD
function This:on_CD_V_CB_stateChanged(in_state)
	--self:updateConfigParameter("CD_V_CB_STT");
	GGM.GUI.CombatDashboard:SetDashboardPanelHidden(not in_state);
end


-- Апдейтер гм CD по оси X
function This:on_CD_OX_SW_valueChanged(in_value)
	--self:updateConfigParameter("CD_OXSWR_F_VAL");
	GGM.GUI.CombatDashboard:SetDashboardPanelOffsetX(in_value);
end


-- Апдейтер гм CD по оси Y
function This:on_CD_OY_SW_valueChanged(in_value)
	--self:updateConfigParameter("CD_OYSWR_F_VAL");
	GGM.GUI.CombatDashboard:SetDashboardPanelOffsetY(in_value);
end


-- Ресетер значений MD с петом
function This:on_MD_R_B_clicked()
	--self.JR_HG_F_MD_F_V_CB_OBJ:SetChecked(self.JR_HG_F_MD_F_V_CB_TMP.attributes.checked);

	--self.JR_HG_F_MD_F_OXSWR_F_OBJ:SetValue(self.JR_HG_F_MD_F_OXSWR_F_TMP.attributes.value);
	--self.JR_HG_F_MD_F_OYSWR_F_OBJ:SetValue(self.JR_HG_F_MD_F_OYSWR_F_TMP.attributes.value);
end


-- Апдейтер видимости MD
function This:on_MD_V_CB_stateChanged(in_state)
	--self:updateConfigParameter("MD_V_CB_STT");
	GGM.GUI.MaintenanceDashboard:SetDashboardPanelHidden(not in_state);
end


-- Апдейтер гм MD по оси X
function This:on_MD_OX_SW_valueChanged(in_value)
	--self:updateConfigParameter("MD_OXSWR_F_VAL");
	GGM.GUI.MaintenanceDashboard:SetDashboardPanelOffsetX(in_value);
end


-- Апдейтер гм MD по оси Y
function This:on_MD_OY_SW_valueChanged(in_value)
	--self:updateConfigParameter("MD_OYSWR_F_VAL");
	GGM.GUI.MaintenanceDashboard:SetDashboardPanelOffsetY(in_value);
end


-- Ресетер значений DD
function This:on_DD_R_B_clicked()
	--self.JR_HG_F_DD_F_V_CB_OBJ:SetChecked(self.JR_HG_F_DD_F_V_CB_TMP.attributes.checked);

	--self.JR_HG_F_DD_F_OXSWR_F_OBJ:SetValue(self.JR_HG_F_DD_F_OXSWR_F_TMP.attributes.value);
	--self.JR_HG_F_DD_F_OYSWR_F_OBJ:SetValue(self.JR_HG_F_DD_F_OYSWR_F_TMP.attributes.value);
end


-- Апдейтер видимости DD
function This:on_DD_V_CB_stateChanged(in_state)
	--self:updateConfigParameter("DD_V_CB_STT");
	GGM.GUI.DrivecontrolDashboard:SetDashboardPanelHidden(not in_state);
end


-- Апдейтер гм DD по оси X
function This:on_DD_OX_SW_valueChanged(in_value)
	--self:updateConfigParameter("DD_OXSWR_F_VAL");
	GGM.GUI.DrivecontrolDashboard:SetDashboardPanelOffsetX(in_value);
end


-- Апдейтер гм DD по оси Y
function This:on_DD_OY_SW_valueChanged(in_value)
	--self:updateConfigParameter("DD_OYSWR_F_VAL");
	GGM.GUI.DrivecontrolDashboard:SetDashboardPanelOffsetY(in_value);
end


-- Ресетер значений KF
function This:on_KF_R_B_clicked()
	--self.JR_HG_F_LF_F_V_CB_OBJ:SetChecked(self.JR_HG_F_LF_F_V_CB_TMP.attributes.state);

	--self.JR_HG_F_LF_F_OXSWR_F_OBJ:SetValue(self.JR_HG_F_LF_F_OXSWR_F_TMP.attributes.value);
	--self.JR_HG_F_LF_F_OYSWR_F_OBJ:SetValue(self.JR_HG_F_LF_F_OYSWR_F_TMP.attributes.value);
end


-- Апдейтер видимости KF
function This:on_KF_V_CB_stateChanged(in_state)
	--self:updateConfigParameter("LF_V_CB_STT");
	GGM.GUI.KillFeed:SetHidden(not in_state);
end


-- Апдейтер гм LF по оси X
function This:on_KF_OX_SW_valueChanged(in_value)
	--self:updateConfigParameter("LF_OXSWR_F_VAL");
	GGM.GUI.KillFeed:SetOffsetX(in_value);
end


-- Апдейтер гм KF по оси Y
function This:on_KF_OY_SW_valueChanged(in_value)
	--self:updateConfigParameter("LF_OYSWR_F_VAL");
	GGM.GUI.KillFeed:SetOffsetY(in_value);
end


-- Коллбэк для панели питомца
function This:resetPetActionBar()
	local pt, rt, rp, ox, oy = PetActionBarFrame:GetPoint();
	-- NOTE: не, ну это пиздец костыль....
	if (true) then return; end
	GGC.TriggerTimer:Create({
		time = 2,
		callback = function()
			PetActionBarFrame:ClearAllPoints();
			--PetActionBarFrame:SetPoint("BOTTOM", GGM.GUI.MaintenanceDashboard:GetDashboardPanelFrame(), "TOP", 40, 5);
			PetActionBarFrame:SetPoint(pt, rt, rp, ox, oy + GGM.GUI.MaintenanceDashboard:GetDashboardPanelHeight() + self:getConfig().skillBarGap);
		end
	});
end


-- Коллбэк для панели стоек
function This:resetStanceBar()
	local pt, rt, rp, ox, oy = PetActionBarFrame:GetPoint();
	-- NOTE: не, ну это пиздец костыль....
	if (true) then return; end
	GGC.TriggerTimer:Create({
		time = 2,
		callback = function()
			StanceBarFrame:ClearAllPoints();
			StanceBarFrame:SetPoint(pt, rt, rp, ox, oy + GGM.GUI.MaintenanceDashboard:GetDashboardPanelHeight() + self:getConfig().skillBarGap);
		end
	});
end

--
function This:on_SettingFrame_enteRed(in_class, in_xslider, in_yslider)
	--print("setting frame enter");
end


--
function This:on_SettingFrame_leaved(in_class, in_xslider, in_yslider)
	--print("setting frame leave");
end


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------