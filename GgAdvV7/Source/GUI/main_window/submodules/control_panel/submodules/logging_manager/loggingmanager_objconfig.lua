local This = GGF.ModuleGetWrapper("GUI-5-2-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_loggingManagerGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		loggingManager = {		-- [LM_SF]
		},
	}),
};

local LMG_SF = This.oc_loggingManagerGeometry.standardFrame;

-- loggingManager(SF)
LMG_SF.loggingManager.object = {};
LMG_SF.loggingManager.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "LM",
		},
		miscellaneous = {
			hidden = true,
		},
		size = {
			width = function() return MSC_CONFIG.frameWidth() end,
			height = function() return MSC_CONFIG.frameHeight() end,
		},
		anchor = {
			point = GGE.PointType.Top,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Bottom,
			offsetX = 0,	-- fwd
			offsetY = 0,	-- fwd
		},
		backdrop = GGD.Backdrop.Segregate,
	},
};