local This = GGF.ModuleGetWrapper("GUI-5");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- api
-- Смена видимости основной панели
function This:setHidden(in_vis)
	GGF.OuterInvoke(self.JR_MW_SF_OBJ, "SetHidden", in_vis);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Взятие объекта главного окна
function This:getLeadingFrame()
	return GGF.INS.GetObjectRef(self.JR_MW_SF_OBJ);
end


-- api
-- Взятие видимости основной панели
function This:getHidden()
	return GGF.OuterInvoke(self.JR_MW_SF_OBJ, "GetHidden");
end


-- api
-- Взятие высоты основной панели
function This:getHeight()
	return GGF.OuterInvoke(self.JR_MW_SF_OBJ, "GetHeight");
end


-- api
-- Взятие компенсатора(поправки на наличие доп кнопок между окнами)
function This:getButtonExistenceOffsetYCompensator()
	return self:getConfig().buttonExistenceOffsetYCompensator;
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- Установка линейных размеров дочерних элементов
function This:adjustChildFrameSize(in_width, in_height)
	-- NOTE: пережиток прошлого

	--local width, height = self.JR_MW_SF_OBJ:GetWidth(), self.JR_MW_SF_OBJ:GetHeight();
	--local offsetNT, offsetT = self:getConfig().childFrameCornerOffset, self:getConfig().childTabbedFrameOffsetY;
	--NOTE: игнорим входные параметры, т.к. они все равно не подаются туда
	--GGM.GUI.AlertDispatcher:AdjustSize(width - 2*offsetNT, height - offsetNT - math.abs(offsetT));
	--GGM.GUI.ControlPanel:AdjustSize(width - 2*offsetNT, height - offsetNT - math.abs(offsetT));
	--GGM.GUI.DataService:AdjustSize(width - 2*offsetNT, height - offsetNT - math.abs(offsetT));
	--GGM.GUI.InformationPanel:AdjustSize(width - 2*offsetNT, height - offsetNT - math.abs(offsetT));
end


-- TODO: Кусь!!!
-- Компактный переключатель панелей
function This:managePanels(in_panelNum)
	ATLASSERT(in_panelNum > 0 and in_panelNum < 5);
	local pn1, pn2, pn3, pn4 = (in_panelNum == 1), (in_panelNum == 2), (in_panelNum == 3), (in_panelNum == 4);

	--print("mainwindow functional mp:", self.JR_MW_SF_IP_TS_OBJ);
	GGF.OuterInvoke(self.JR_MW_SF_IP_TS_OBJ, "SetEnabled", pn1);
	GGF.OuterInvoke(self.JR_MW_SF_CP_TS_OBJ, "SetEnabled", pn2);
	GGF.OuterInvoke(self.JR_MW_SF_AD_TS_OBJ, "SetEnabled", pn3);
	GGF.OuterInvoke(self.JR_MW_SF_DSP_TS_OBJ, "SetEnabled", pn4);

	GGF.OuterInvoke(self.JR_MW_SF_IP_TS_OBJ, "SetHighlighted", pn1);
	GGF.OuterInvoke(self.JR_MW_SF_CP_TS_OBJ, "SetHighlighted", pn2);
	GGF.OuterInvoke(self.JR_MW_SF_AD_TS_OBJ, "SetHighlighted", pn3);
	GGF.OuterInvoke(self.JR_MW_SF_DSP_TS_OBJ, "SetHighlighted", pn4);

	PanelTemplates_SetTab(GGF.INS.GetObjectRef(self.JR_MW_SF_OBJ), in_panelNum);

	GGM.GUI.InformationPanel:SetHidden(not pn1); -- проектируется(не реализовано)
	GGM.GUI.ControlPanel:SetHidden(not pn2);
	GGM.GUI.AlertDispatcher:SetHidden(not pn3);
	GGM.GUI.DataService:SetHidden(not pn4);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


--
function This:on_informationPanelTS_clicked()
	self:managePanels(1);
end


--
function This:on_controlPanelTS_clicked()
	self:managePanels(2);
end


--
function This:on_alertDashboardTS_clicked()
	self:managePanels(3);
end


--
function This:on_dataServicePanelTS_clicked()
	self:managePanels(4);
end


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------