local This = GGF.ModuleGetWrapper("GUI-5");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_mainWindowGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		mainWindow = {			-- [MW_SF]
			inherit = {
				tabSwitch = GGF.GTemplateTree(GGC.TabSwitch, {
					informationPanel = {},		-- [IP_TS]
					controlPanel = {},			-- [CP_TS]
					alertDispatcher = {},		-- [AD_TS]
					dataServicePanel = {},		-- [DSP_TS]
				}),
			},
		},
	}),
};

local MWG_SF = This.oc_mainWindowGeometry.standardFrame;
local MWG_SF_MW_TS = MWG_SF.mainWindow.inherit.tabSwitch;

-- mainWindow(SF)
MWG_SF.mainWindow.object = {};
MWG_SF.mainWindow.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "MW",
		},
		miscellaneous = {
			hidden = true,
			topLevel = true,
			movable = true,
			enableMouse = true,
			enableKeyboard = false,
		},
		size = {
			width = function() return MSC_CONFIG.frameWidth() end,
			height = function() return MSC_CONFIG.frameHeight() end,
		},
		anchor = {
			point = GGE.PointType.Center,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.TopLeft,
			offsetX = nil,	-- fwd
			offsetY = nil,	-- fwd
		},
		backdrop = GGD.Backdrop.Segregate,
		--backdrop = GGD.Backdrop.Default,
		callback = {
			onMouseDown = function(in_meta, ...) end,
			onMouseUp = function(in_meta, ...) end,
			onDragStop = function(in_meta, ...) end,
		},
	},
};


-- mainWindow(SF)
	-- informationPanel(TS)
MWG_SF_MW_TS.informationPanel.object = {};
MWG_SF_MW_TS.informationPanel.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			template = "OptionsFrameTabButtonTemplate",
			wrapperName = "IP",	-- NOTE: ..Tab1? или и так работает?
		},
		miscellaneous = {
			gxname = "Tab1",
			hidden = false,
			movable = false,
			enableMouse = true,
			enableKeyboard = false,
			enabled = false,
			text = "Information Panel",
			--tooltipText = "",	-- NOTE: тут пока непонятно нифига
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.BottomLeft,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.BottomLeft,
			offsetX = function() return MSC_CONFIG.tabSwitchOffsetX() end,
			offsetY = function() return MSC_CONFIG.tabSwitchOffsetY() end,
		},
		backdrop = GGD.Backdrop.Empty,	-- NOTE: не должен работать, ибо шаблон
		callback = {
			onClick = function(...) This:on_informationPanelTS_clicked(...) end,
		},
	},
};


-- mainWindow(SF)
	-- controlPanel(TS)
MWG_SF_MW_TS.controlPanel.object = {};
MWG_SF_MW_TS.controlPanel.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			template = "OptionsFrameTabButtonTemplate",
			wrapperName = "CP",	-- NOTE: ..Tab1? или и так работает?
		},
		miscellaneous = {
			gxname = "Tab2",
			hidden = false,
			movable = false,
			enableMouse = true,
			enableKeyboard = false,
			enabled = false,
			text = "Control Panel",
			--tooltipText = "",	-- NOTE: тут пока непонятно нифига
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Right,
			offsetX = function() return MSC_CONFIG.tabSwitchHorGap() end,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Empty,	-- NOTE: не должен работать, ибо шаблон
		callback = {
			onClick = function(...) This:on_controlPanelTS_clicked(...) end,
		},
	},
};


-- mainWindow(SF)
	-- alertDispatcher(TS)
MWG_SF_MW_TS.alertDispatcher.object = {};
MWG_SF_MW_TS.alertDispatcher.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			template = "OptionsFrameTabButtonTemplate",
			wrapperName = "AD",
		},
		miscellaneous = {
			gxname = "Tab3",
			hidden = false,
			movable = false,
			enableMouse = true,
			enableKeyboard = false,
			enabled = false,
			text = "Alert Dispatcher",
			--tooltipText = "",
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Right,
			offsetX = function() return MSC_CONFIG.tabSwitchHorGap() end,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Empty,	-- NOTE: не должен работать, ибо шаблон
		callback = {
			onClick = function(...) This:on_alertDashboardTS_clicked(...) end,
		},
	},
};


-- mainWindow(SF)
	-- dataServicePanel(TS)
MWG_SF_MW_TS.dataServicePanel.object = {};
MWG_SF_MW_TS.dataServicePanel.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			template = "OptionsFrameTabButtonTemplate",
			wrapperName = "DSP",	-- NOTE: ..Tab1? или и так работает?
		},
		miscellaneous = {
			gxname = "Tab4",
			hidden = false,
			movable = false,
			enableMouse = true,
			enableKeyboard = false,
			enabled = false,
			text = "Data Service Panel",
			--tooltipText = "",	-- NOTE: тут пока непонятно нифига
		},
		size = {
			width = function() return MSC_CONFIG.buttonWidth() end,
			height = function() return MSC_CONFIG.buttonHeight() end,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Right,
			offsetX = function() return MSC_CONFIG.tabSwitchHorGap() end,
			offsetY = 0,
		},
		backdrop = GGD.Backdrop.Empty,	-- NOTE: не должен работать, ибо шаблон
		callback = {
			onClick = function(...) This:on_dataServicePanelTS_clicked(...) end,
		},
	},
};