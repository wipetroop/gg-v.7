local This = GGF.ModuleGetWrapper("GUI-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- TODO: переместить это(2 внизу) в ответственную систему(ну не очень оно в эвоке смотрится...)(GG-44)
-- Принудительный запуск механизма регистрации боя
function This:forceBattleGroundStart()
	print("<< Manual control override >>");
	print("GUi System information : ");

	if (GGM.FCS.RuntimeStorage.GetDebugModeFlag()) then
		print("<< Warning! Running in debug mode! Emote announcer disabled! >>");
	end

	GGM.FCS.RuntimeStorage.SetRuntimeLoggingFlag(true);

	GUi_RI:ForceBattleGroundStart();
end


-- Принудительное отключение механизма регистрации боя
function This:forceBattleGroundEnd()
	print("<< Manual control override >>");

	GGM.FCS.RuntimeStorage.SetRuntimeLoggingFlag(false);

	GUi_RI:ForceBattleGroundEnd();
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------