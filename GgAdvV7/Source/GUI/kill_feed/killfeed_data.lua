local This = GGF.ModuleCreateWrapper("GUI-4", false, {
	width = 430,	-- 812
	height = 70,	-- 406
});

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------

-- TODO: найти, откуда это отвалилось и отрефакторить!!!(GG-35)
This.cs_logPositionFrameColor = {
	0.5,	-- r
	1.0,	-- g
	0.5,	-- b
	0.5,	-- a
};

-------------------------------------------------------------------------------
-- Functors
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------

This.vn_frameNumber = 0;

-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------

-- Флаг режима работы киллфида
This.vf_killFeedEnabled = false;

-------------------------------------------------------------------------------
-- Variable lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------

This.fr_anFrame = nil;