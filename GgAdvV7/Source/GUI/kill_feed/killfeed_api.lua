local This = GGF.ModuleGetWrapper("GUI-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- Установка видимости окна фона лога
function This:SetHidden(in_hdn)
	self:setHidden(in_hdn);
end


-- Управление режимом работы киллфида
function This:SetKillFeedEnabled(in_enb)
	self:setKillFeedEnabled(in_enb);
end


-- Управление геометрической местом панели по оси X
function This:SetOffsetX(in_nWOffsetX)
	self:setOffsetX(in_nWOffsetX);
end


-- Управление геометрической местом панели по оси Y
function This:SetOffsetY(in_nWOffsetY)
	self:setOffsetY(in_nWOffsetY);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие режима работы киллфида
function This:GetKillFeedEnabled()
	self:getKillFeedEnabled();
end


-- Взятие ширины основной панели
function This:GetWidth()
	return self:getWidth();
end


-- Взятие высоты основной панели
function This:GetHeight()
	return self:getHeight();
end


-- Взятие фрейма рантайм лога килов
function This:GetFrame()
	return self:getFrame();
end


-- CC: /acr dth []
-- Изменение высоты лог окна(раньше было заметно)
function This:HeightChangeSlashCommand(in_height)
	print("Height change temporary unavailable");
	--This:proceedHeightChange(in_height);
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------

-- Стандартный обработчик события кила из лога
function This:ProceedKill(in_data)
	self:proceedKill(in_data);
end

function This:ProceedBattleZoneEnter(in_map, in_token)
	self:proceedBattleZoneEnter(in_map, in_token);
end

-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------