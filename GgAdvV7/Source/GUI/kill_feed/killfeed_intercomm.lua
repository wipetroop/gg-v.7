local This = GGF.ModuleGetWrapper("GUI-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.1 (Miscellaneous Variables)
function This:MPRC_MVR_INITIALIZE()
	-- TODO: Разобраться, что это...(GG-38)
	-- NOTE: Пока что не удаляю, хоть функция и пустая
	--self:setHidden(false);
end


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent setters
	-------------------------------------------------------------------------------

	-- killFeed(SF)

	self.JR_KF_SF_OBJ = GGC.StandardFrame:Create(self.JR_KF_SF_TMP, {
		properties = {
			base = {
				parent = GGM.GUI.MasterFrame:GetLeadingFrame(),
			},
			anchor = {
				relativeTo = GGM.GUI.MasterFrame:GetLeadingFrame(),
			},
		},
	});

   -- killFeedInternal(KF)

   self.JR_KFI_KF_OBJ = GGC.KillFeedFrame:Create(self.JR_KFI_KF_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_KF_SF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_KF_SF_OBJ),
         },
      },
   });
end


-- п.2.7 (Object Adjust)
function This:MPRC_ADJ_INITIALIZE()
	-------------------------------------------------------------------------------
	-- Creators/Parent adjusters
	-------------------------------------------------------------------------------

	-- killFeed(SF)
	GGF.OuterInvoke(self.JR_KF_SF_OBJ, "Adjust");

   -- killFeedInternal(KF)
   GGF.OuterInvoke(self.JR_KFI_KF_OBJ, "Adjust");
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------