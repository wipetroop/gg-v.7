local This = GGF.ModuleGetWrapper("GUI-4-3");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Определитель имени пета через фрейм цели(костыльный)
function This:getPetOwner(in_PetName)
	self.fr_scanTool:ClearLines();
	self.fr_scanTool:SetUnit(in_PetName);
	local scanText = _G["ScanTooltipTextLeft2"] -- Строка с <[Player]'s Pet>
	local ownerText = scanText:GetText();
	if not ownerText then return nil end
	--local owner, _ = string.split("'", ownerText);

	return ownerText;
end


-- Определение хозяина пета по кластеру или гету(чере имя)
function This:definePetOwner(in_pname, in_name, in_id, in_flag)
	-- NOTE: отсюда может вылететь нулевой гуид
	in_name.data = nil;
	in_id.data = nil;
	if (GGM.ACR.CommonService:getUnitTypeByFlag(in_flag.data) == GGE.UnitType.Pet) then
		local ownerline = self:getPetOwner(in_pname.data);
		if (ownerline ~= nil) then
			local gUid, name = ACR_SD:AdaptivePlayerSearch(ownerline);
			in_name.data = name;
			in_id.data = gUid;
			--local odata = ACR_SD:GetRuntimePlayerInfoByName(oname);
		end
	else
		in_name.data = self.cl_unusedFieldData;
		in_id.data = self.cl_unusedFieldData;
	end
end


-- api
-- Обработчик запроса на определение хозяина сурса
function This:defineSourcePetOwner(in_data)
	in_data.spm_ownerFlag.data = self.cl_unusedFieldData;	-- пока что не придумал, как его вытащить..
	self:definePetOwner(in_data.spm_name, in_data.spm_ownerName, in_data.spm_ownerId, in_data.spm_flag);
end


-- api
-- Обработчик запроса на определение хозяина таргета
function This:defineTargetPetOwner(in_data)
	in_data.tpm_ownerFlag.data = self.cl_unusedFieldData;
	self:definePetOwner(in_data.tpm_name, in_data.tpm_ownerName, in_data.tpm_ownerId, in_data.tpm_flag);
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------