local This = GGF.ModuleGetWrapper("GUI-4-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------

local MSC_CONFIG = This:getConfig();

-- Hierarchy
This.oc_anLineGeometry = {
	fontString = GGF.GTemplateTree(GGC.FontString, {
		servDel = {},					-- [SD_FS]
		srcText = {},					-- [ST_FS]
		trgText = {},					-- [TT_FS]
	}),
	standardTexture = GGF.GTemplateTree(GGC.StandardTexture, {
		srcFactionIcon = {},			-- [SFI_STX]
		trgFactionIcon = {},			-- [TFI_STX]
		srcMuzzleIconAlternate = {},	-- [SMIA_STX]
		trgMuzzleIconAlternate = {},	-- [TMIA_STX]
		srcClassIcon = {},				-- [SCI_STX]
		trgClassIcon = {},				-- [TCI_STX]
		srcBackground = {},				-- [SB_STX]
		trgBackground = {},				-- [TB_STX]
		spellBackgroundSpell = {},		-- [SBS_STX]
		spellBackgroundCritical = {},	-- [SBC_STX]
		spellBackgroundMulticast = {},	-- [SBM_STX]
		IS = {},						-- [IS_STX]
		IC = {},						-- [IC_STX]
		IM = {},						-- [IM_STX]
	}),
   fadeAnimation = GGF.GTemplateTree(GGC.FadeAnimation, {
      standardAnimation = {},    -- [SA_FA]
   }),
};

local ALG_FS = This.oc_anLineGeometry.fontString;
local ALG_ST = This.oc_anLineGeometry.standardTexture;
local ALG_FA = This.oc_anLineGeometry.fadeAnimation;

-- servDel(FS)(prototype)
ALG_FS.servDel.object = {};
ALG_FS.servDel.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "SD",
		},
		size = {
			height = MSC_CONFIG.stringHeight,
		},
		anchor = {
			point = GGE.PointType.Center,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Center,
			justifyH = GGE.JustifyHType.Center,
			offsetX = 0, 
			offsetY = 0,
		},
		color = {
		},
		shadow = {
			offset = {
				X = 0,
				Y = 0,
			},
		},
	},
};

-- srcText(FS)(prototype)
ALG_FS.srcText.object = {};
ALG_FS.srcText.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "ST",
		},
		size = {
			height = MSC_CONFIG.stringHeight,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			justifyH = GGE.JustifyHType.Center,
			offsetY = 0,
		},
		shadow = {
			offset = {
				X = MSC_CONFIG.fontShadowOffsetX,
				Y = MSC_CONFIG.fontShadowOffsetY,
			},
		},
	},
};

-- trgText(FS)(prototype)
ALG_FS.trgText.object = {};
ALG_FS.trgText.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "TT",
		},
		size = {
			height = MSC_CONFIG.stringHeight,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			justifyH = GGE.JustifyHType.Center,
			offsetY = 0,
		},
		shadow = {
			offset = {
				X = MSC_CONFIG.fontShadowOffsetX,
				Y = MSC_CONFIG.fontShadowOffsetY,
			},
		},
	},
};

-- srcFactionIcon(STX)(prototype)
ALG_ST.srcFactionIcon.object = {};
ALG_ST.srcFactionIcon.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "SFI",
		},
		size = {
			height = MSC_CONFIG.factionIconUniSize,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetY = 0,
		},
	},
};

-- trgFactionIcon(STX)(prototype)
ALG_ST.trgFactionIcon.object = {};
ALG_ST.trgFactionIcon.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "TFI",
		},
		size = {
			height = MSC_CONFIG.factionIconUniSize,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetY = 0,
		},
	},
};

-- srcMuzzleIconAlternate(STX)(prototype)
ALG_ST.srcMuzzleIconAlternate.object = {};
ALG_ST.srcMuzzleIconAlternate.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "SMIA",
		},
		size = {
			height = MSC_CONFIG.muzzleIconSide,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetY = 0,
		},
	},
};

-- trgMuzzleIconAlternate(STX)(prototype)
ALG_ST.trgMuzzleIconAlternate.object = {};
ALG_ST.trgMuzzleIconAlternate.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "TMIA",
		},
		size = {
			height = MSC_CONFIG.muzzleIconSide,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetY = 0,
		},
	},
};

-- srcClassIcon(STX)(prototype)
ALG_ST.srcClassIcon.object = {};
ALG_ST.srcClassIcon.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "SCI",
		},
      miscellaneous = {
         layer = GGE.LayerType.Highlight,
      },
		size = {
			height = MSC_CONFIG.classIconSide,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetY = 0,
		},
	},
};

-- trgClassIcon(STX)(prototype)
ALG_ST.trgClassIcon.object = {};
ALG_ST.trgClassIcon.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "TCI",
		},
      miscellaneous = {
         layer = GGE.LayerType.Highlight,
      },
		size = {
			height = MSC_CONFIG.classIconSide,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetY = 0,
		},
	},
};

-- TODO: разобраться с модификаторами(GG-40)
-- srcBackground(STX)(prototype)
ALG_ST.srcBackground.object = {};
ALG_ST.srcBackground.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "SB",
		},
      miscellaneous = {
         layer = GGE.LayerType.Background,
      },
		size = {
			height = MSC_CONFIG.classIconSide,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetY = 0,
		},
	},
};

-- trgBackground(STX)(prototype)
ALG_ST.trgBackground.object = {};
ALG_ST.trgBackground.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "TB",
		},
      miscellaneous = {
         layer = GGE.LayerType.Background,
      },
		size = {
			height = MSC_CONFIG.classIconSide,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetY = 0,
		},
	},
};

-- spellBackgroundSpell(STX)(prototype)
ALG_ST.spellBackgroundSpell.object = {};
ALG_ST.spellBackgroundSpell.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "SBS",
		},
		miscellaneous = {
			file = MSC_CONFIG.spellTypedBackgroundTexture,
         layer = GGE.LayerType.Border,
		},
		size = {
			height = MSC_CONFIG.spellIconHeight + 2*MSC_CONFIG.spellBackgroundInset,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetY = 0,
		},
	},
};

-- spellBackgroundCritical(STX)(prototype)
ALG_ST.spellBackgroundCritical.object = {};
ALG_ST.spellBackgroundCritical.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "SBC",
		},
		miscellaneous = {
			file = MSC_CONFIG.spellTypedBackgroundTexture,
         layer = GGE.LayerType.Border,
		},
		size = {
			height = MSC_CONFIG.spellIconHeight + 2*MSC_CONFIG.spellBackgroundInset,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetY = 0,
		},
	},
};

-- spellBackgroundMulticast(STX)(prototype)
ALG_ST.spellBackgroundMulticast.object = {};
ALG_ST.spellBackgroundMulticast.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "SBM",
		},
		miscellaneous = {
			file = MSC_CONFIG.spellTypedBackgroundTexture,
         layer = GGE.LayerType.Border,
		},
		size = {
			height = MSC_CONFIG.spellIconHeight + 2*MSC_CONFIG.spellBackgroundInset,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetY = 0,
		},
	},
};

-- IS(STX)(prototype)
ALG_ST.IS.object = {};
ALG_ST.IS.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "IS",
		},
      miscellaneous = {
         layer = GGE.LayerType.Highlight,
      },
		size = {
			height = MSC_CONFIG.spellIconHeight,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetY = 0,
		},
	},
};

-- IC(STX)(prototype)
ALG_ST.IC.object = {};
ALG_ST.IC.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "IC",
		},
		miscellaneous = {
			file = MSC_CONFIG.iconCriticalTexture,
         layer = GGE.LayerType.Highlight,
		},
		size = {
			height = MSC_CONFIG.spellIconHeight,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetY = 0,
		},
	},
};

-- IM(STX)(prototype)
ALG_ST.IM.object = {};
ALG_ST.IM.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "IM",
		},
		miscellaneous = {
			file = MSC_CONFIG.iconMultistrikeTexture,
         layer = GGE.LayerType.Highlight,
		},
		size = {
			height = MSC_CONFIG.spellIconHeight,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetY = 0,
		},
	},
};

-- standardAnimation(FA)(prototype)
ALG_FA.standardAnimation.object = {};
ALG_FA.standardAnimation.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "SA",
      },
      parameters = {
         time = {
            duration = 1,
            delay = 5,
         }
      },
   },
};