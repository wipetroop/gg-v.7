local This = GGF.ModuleGetWrapper("GUI-4-1");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- 
function This:getLine(in_idx)
	return self.vs_lineStack.data[in_idx];
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Концепция:
-- Объекты создаются и пихаются в массив, пока не будет достигнут предел по количеству
-- При достижении предела, на запрос возвращаются объекты по кругу
-- (т.е. 99(новый), 100(новый), 1(старый)(т.к. 100 предел))
-- Конструктор объекта записи для лога килов(т.е. одной строки); по идее - тупо создание объектов без препроцессинга
function This:requestLine()
	if self.vs_lineStack.memorised < self.vs_lineStack.max_size then
		self.vs_lineStack.data[self.vs_lineStack.memorised + 1] = {};

		local tvpsgn = self.vs_lineStack.data[self.vs_lineStack.memorised + 1];
		
		local MSC_CONFIG = self:getConfig();

		-- // --
		self.vs_lineStack.memorised = self.vs_lineStack.memorised + 1;

		self:setLineLayoutOrder(self.vs_lineStack.data[self.vs_lineStack.memorised]);

		return self.vs_lineStack.data[self.vs_lineStack.memorised], self.vs_lineStack.memorised;
	end

	local retPos = self.vs_lineStack.last;

	if self.vs_lineStack.last == self.vs_lineStack.max_size then
		self.vs_lineStack.last = 1;
	else
		self.vs_lineStack.last = self.vs_lineStack.last + 1;
	end

	self:setLineLayoutOrder(self.vs_lineStack.data[retPos]);
	--print("recycled ", retPos)
	return self.vs_lineStack.data[retPos], retPos;
end





-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------