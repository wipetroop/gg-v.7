local This = GGF.ModuleCreateWrapper("GUI-4-1", false, {
	-- string common parameters(font parameters most likely)
	stringHeight = 12,

	-- muzzle System(back + model)
	muzzleIconSide = 26,

	-- class icons
	classIconSide = 26, -- дб равна стороне мордахи

	-- source/target
	objectBackgroundExtenderX = 10,

	-- spell/crit/multicast icons(called spell icons)
	spellIconHeight = 26,
	spellBackgroundInset = 4,

	-- source/target names
	fontShadowOffsetX = 2,
	fontShadowOffsetY = -2,
	fontSymbolWidth = 12,

	-- faction
	factionIconUniSize = 26,

	-- spell/Critical/mutlistrike background texture
	spellTypedBackgroundTexture = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\spell_background.tga",

	iconCriticalTexture = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\Critical.tga",
	iconMultistrikeTexture = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\multistrike.tga",

	-- extender
	spellBackExtenderX = 10,
	unitBackExtenderX = 12,

	-- Object in string gaps
	objectGapHor = 10,
	objectGapVer = 26,
});

local MSC_CONFIG = This:getConfig();

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------

This.cs_widthPrototype = {
   srcText = function(in_argline) return MSC_CONFIG.fontSymbolWidth*(in_argline.sSmbCnt + 2) end,
   trgText = function(in_argline) return MSC_CONFIG.fontSymbolWidth*(in_argline.tSmbCnt + 2) end,
   srcFactionIcon = function(...) return MSC_CONFIG.factionIconUniSize end,
   trgFactionIcon = function(...) return MSC_CONFIG.factionIconUniSize end,
   srcMuzzleIconAlternate = function(...) return MSC_CONFIG.muzzleIconSide end,
   trgMuzzleIconAlternate = function(...) return MSC_CONFIG.muzzleIconSide end,
   srcClassIcon = function(...) return MSC_CONFIG.classIconSide end,
   trgClassIcon = function(...) return MSC_CONFIG.classIconSide end,
   srcBackground = function(in_argline) return in_argline.fnOffsetX - in_argline.stOffsetX + in_argline.fnWidth + MSC_CONFIG.unitBackExtenderX end,
   trgBackground = function(in_argline) return in_argline.fnOffsetX - in_argline.stOffsetX + in_argline.fnWidth + MSC_CONFIG.unitBackExtenderX end,
   spellBackgroundSpell = function(in_argline) return in_argline.fnOffsetX - in_argline.stOffsetX + in_argline.fnWidth + 2*MSC_CONFIG.spellBackExtenderX end,
   spellBackgroundCritical = function(in_argline) return in_argline.fnOffsetX - in_argline.stOffsetX + in_argline.fnWidth + 2*MSC_CONFIG.spellBackExtenderX end,
   spellBackgroundMulticast = function(in_argline) return in_argline.fnOffsetX - in_argline.stOffsetX + in_argline.fnWidth + 2*MSC_CONFIG.spellBackExtenderX end,
   IS = function(...) return MSC_CONFIG.spellIconHeight end,
   IC = function(...) return MSC_CONFIG.spellIconHeight end,
   IM = function(...) return MSC_CONFIG.spellIconHeight end,
};

This.cs_offsetXPrototype = {
   srcText = function(in_argline) return in_argline.pOffsetX + in_argline.pWidth + MSC_CONFIG.objectGapHor end,
   trgText = function(in_argline) return in_argline.pOffsetX + in_argline.pWidth + MSC_CONFIG.objectGapHor end,
   srcFactionIcon = function(in_argline) return in_argline.pOffsetX + in_argline.pWidth + MSC_CONFIG.objectGapHor end,
   trgFactionIcon = function(in_argline) return in_argline.pOffsetX + in_argline.pWidth + MSC_CONFIG.objectGapHor end,
   srcMuzzleIconAlternate = function(in_argline) return in_argline.pOffsetX + in_argline.pWidth + MSC_CONFIG.objectGapHor end,
   trgMuzzleIconAlternate = function(in_argline) return in_argline.pOffsetX + in_argline.pWidth + MSC_CONFIG.objectGapHor end,
   srcClassIcon = function(in_argline) return in_argline.pOffsetX + in_argline.pWidth + MSC_CONFIG.objectGapHor end,
   trgClassIcon = function(in_argline) return in_argline.pOffsetX + in_argline.pWidth + MSC_CONFIG.objectGapHor end,
   srcBackground = function(in_argline) return in_argline.stOffsetX - 0.2*MSC_CONFIG.unitBackExtenderX end,
   trgBackground = function(in_argline) return in_argline.stOffsetX - 0.8*MSC_CONFIG.unitBackExtenderX end,
   spellBackgroundSpell = function(in_argline) return in_argline.stOffsetX - MSC_CONFIG.spellBackExtenderX end,
   spellBackgroundCritical = function(in_argline) return in_argline.stOffsetX - MSC_CONFIG.spellBackExtenderX end,
   spellBackgroundMulticast = function(in_argline) return in_argline.stOffsetX - MSC_CONFIG.spellBackExtenderX end,
   IS = function(in_argline) return in_argline.pOffsetX + in_argline.pWidth + MSC_CONFIG.objectGapHor end,
   IC = function(in_argline) return in_argline.pOffsetX + in_argline.pWidth + MSC_CONFIG.objectGapHor end,
   IM = function(in_argline) return in_argline.pOffsetX + in_argline.pWidth + MSC_CONFIG.objectGapHor end,
};

-------------------------------------------------------------------------------
-- Functors
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Variable line
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------

-- Чисто аллокатор линий
--ACR_KS.varstruct.anFrameData = {
This.vs_lineStack = {
	memorised = 0,
	max_size = 100,
	last = 1,
	warmup = false,
	data = {
		-- [1] = <tvpSignature>
		-- ...
	},
};

This.vs_layoutOrder = {
	object,
	background,
};

-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------