local This = GGF.ModuleGetWrapper("GUI-4-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- api
-- Получение большей части инфы по игроку через гуид и запись в кластер
function This:getPlayerInfoByGUIDEnhanced(in_guid)
	--print("guid enh", in_guid);
	local sessionPlayerCluster = GGM.ACR.SpecDispatcher:GetSessionPlayerCluster();
	if (sessionPlayerCluster.guid[in_guid] ~= nil) then
		return sessionPlayerCluster.guid[in_guid];
	end

	-- Pet/creature
	if (in_guid:find("Player") == nil) then
		--print("not a player");
		return nil;
	end
	
	local class, classFilename, race, raceFilename, sex, name, realm = GetPlayerInfoByGUID(in_guid);

	if (class == nil) then
		local counter = 0;

		while (class == nil and counter < 5) do
			class, classFilename, race, raceFilename, sex, name, realm = GetPlayerInfoByGUID(in_guid);
			counter = counter + 1;
		end

		if (class == nil) then
			return nil;
		end
	end

	--print("player/pet");
	--print(class, classFilename, race, raceFilename, sex, name, realm);
	sessionPlayerCluster.guid[in_guid] = {
		class = class,
		classFilename = classFilename,
		race = race,
		raceFilename = raceFilename,
		sex = sex,
		name = name,
		realm = realm,
	};

	return sessionPlayerCluster.guid[in_guid];
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------