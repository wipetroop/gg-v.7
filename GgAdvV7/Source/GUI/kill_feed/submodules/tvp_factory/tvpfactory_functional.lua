-- TVP Factory(забыл как твп расшифровать...)
local This = GGF.ModuleGetWrapper("GUI-4-5");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- 
function This:getMulticastedSpells()
	return self.cs_multicastedSpells;
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- rezerv
--if warmup == true then
--	local tvp = pKLogQueue[pKLogQueue.last];
--	
--	for k, v in pairs(tvp) do
--		if v ~= nil then
--			v:Hide();
--			--v:SetParent(nil);
--			v:ClearAllPoints();
--		end
--	end
--end
-- Не работает
function This:anFrameData_Add(value)
	-- Тут что-то должно было быть...но забыл, что именно
	--if ACR_KS.anFrameData.warmup == true then 

	--end
end


-- api
-- TODO: как-нибудь приуменьшить эту функцию...а то мастодонты сложны для дебага(GG-42)
-- Стандартный обработчик события кила из лога
--function ACR_KS.proceedKill(in_sourceName, in_sourceId, in_sourceFlag, in_targetName, in_targetId, in_targetFlag, in_spellId, in_isCritical, in_isMulticast)
function This:proceedKill(in_data)
	-- забыл, как сделать отрицание переменной
	-- TODO: разобраться с местом жительства некоторых переменных
	if (false) then --(not GGM.GUI.KillFeed:GetKillFeedEnabled()) then
		return;
	end

	local unusedLogFieldData = GGF.MDL.GetUnusedFiledData();
	local statisticSkillNameBT = GGM.ACR.CombatInterceptor:GetStatisticSkillNameBT();

	local targetLength;
	local sourceLength = 0;

	local sourceTypeFlag = "";
	local targetTypeFlag = "";

	local sourceClassIndex;		-- NOTE: этот дальше берется, иначе там nil может быть
	local targetClassIndex;
	local tvpsgn;

	local srcExistingMultiplier = 0;

	tvpsgn, tvpId = GGM.GUI.AnLine:RequestLine();

	tvpsgn.ServiceFlag = false;
	
	local sourceExists = in_data.spm_name.data ~= nil and in_data.spm_name.data ~= "SERVER";
	--print(in_data.sourceName.data);
	--anFrame.LastS = anFrame:CreateGGC.FontString(nil, "OVERLAY", "GameFontNormal");
	if (sourceExists) then
		sourceTypeFlag = GGM.ACR.CommonService:getUnitTypeByFlag(in_data.spm_flag.data);
		sourceClassIndex = self:getUnitClass(in_data.spm_id.data);
		sourceLength = utf8len(GGM.ACR.CommonService:TruncRealm(in_data.spm_name.data));

		srcExistingMultiplier = 1;

		tvpsgn.SourceFlag = true;
	else
		-- сурса нет, значит смерть от окружения(т.к. при суицидах спеллами сурс есть и совпадает с таргетом)
		tvpsgn.SourceFlag = false;

		if self.cs_envDamageIDS[string.lower(in_data.msd_id.data)] == nil then
			ATLASSERT(false);
			in_data.msd_id.data = "Undefined";
		end
	end

	targetTypeFlag = GGM.ACR.CommonService:getUnitTypeByFlag(in_data.tpm_flag.data);
	targetClassIndex = self:getUnitClass(in_data.tpm_id.data);
	targetLength = utf8len(GGM.ACR.CommonService:TruncRealm(in_data.tpm_name.data));

	-- Service manipulations

	local spikeSumm = 0;
	local criticalFlag, multicastFlag = false, false;

	if in_data.msd_isCritical.data == true then
		spikeSumm = spikeSumm + 1;
		criticalFlag = true;
	end

	-- multicast removed in Legion, but elemental Shaman mastery returned
	-- забавно, но это так, теперь мультикаст только для шаманов
	if self.cs_multicastedSpells[tostring(in_data.msd_id.data)] ~= nil then
		spikeSumm = spikeSumm + 1;
		multicastFlag = true;
	end

	-- сборная солянка из параметров, нужных для расчета оффсетов и ширин
	local gSuppData = {
		targetSymbolCount = targetLength,
		sourceSymbolCount = sourceLength,
		iconCount = spikeSumm,
		srcExMp = srcExistingMultiplier
	};
	
	-- Linear size
	-- Width
	if (sourceExists) then
		GGF.OuterInvoke(tvpsgn.source.frontal.text, "SetWidth", self:getConfig().specificLengthBySymbol*gSuppData.sourceSymbolCount);
	end

	GGF.OuterInvoke(tvpsgn.target.frontal.text, "SetWidth", self:getConfig().specificLengthBySymbol*gSuppData.targetSymbolCount);


	-- Text
	if (sourceExists) then
		GGF.OuterInvoke(tvpsgn.source.frontal.text, "SetText", GGM.ACR.CommonService:TruncRealm(in_data.spm_name.data));
	end

	GGF.OuterInvoke(tvpsgn.target.frontal.text, "SetText", GGM.ACR.CommonService:TruncRealm(in_data.tpm_name.data));

	-- Text color
	if (sourceExists) then
		local srcClr = self.cs_classColor[sourceClassIndex];
		GGF.OuterInvoke(tvpsgn.source.frontal.text, "SetColor", srcClr);
	end

	local trgClr = self.cs_classColor[targetClassIndex];
	GGF.OuterInvoke(tvpsgn.target.frontal.text, "SetColor", trgClr);

	-- Texture handling
	if (sourceExists) then
		GGF.OuterInvoke(tvpsgn.source.frontal.factionIcon, "SetFile", self:getUnitFactionTexture(in_data.spm_name.data, sourceClassIndex, in_data.spm_flag.data));
		if (in_data.spm_ownerName.data == unusedLogFieldData) then -- не пет
			GGM.GUI.MuzzleDrawer:SetUnitMuzzle(
				false,
				true,
				tvpsgn.source.frontal.muzzleIconBasement,
				tvpsgn.source.frontal.muzzleIconModel,
				tvpsgn.source.frontal.muzzleIconAlternate,
				in_data.spm_name.data,
				in_data.spm_id.data
			);
			self:setUnitClassIcon(tvpsgn.source.frontal.classIcon, in_data.spm_name.data, self.cs_classTokensReversed[sourceClassIndex]);
		else -- пет/прислужник
         --print("whos your daddy", in_data.spm_ownerName.data, unusedLogFieldData, in_data.spm_ownerName.data == unusedLogFieldData);
			GGM.GUI.MuzzleDrawer:SetUnitMuzzle(
				true,
				true,
				tvpsgn.source.frontal.muzzleIconBasement,
				tvpsgn.source.frontal.muzzleIconModel,
				tvpsgn.source.frontal.muzzleIconAlternate,
				in_data.spm_name.data,
				in_data.spm_id.data
			);
			if (in_data.spm_ownerId.data ~= nil) then
				local ownerClassIndex = 0;
				local ownerData = GGM.ACR.SpecDispatcher:GetRuntimePlayerInfoByName(in_data.spm_ownerName.data);
				if (ownerData == nil) then
					ownerData = GGM.GUI.PlayerCluster:GetPlayerInfoByGUIDEnhanced(in_data.spm_ownerId.data);
				end
				self:setUnitClassIcon(tvpsgn.source.frontal.classIcon, in_data.spm_name.data, ownerData.class);
			else
				self:setUnitClassIcon(tvpsgn.source.frontal.classIcon, in_data.spm_name.data, nil);
			end
		end

		local srcTexPrepath = GGM.ACR.CommonService:GetUnitFactionByFlag(in_data.spm_flag.data);

		GGF.OuterInvoke(tvpsgn.source.background.common, "SetFile", self.cs_factionBackgroundTextures[srcTexPrepath]);
	end

	GGF.OuterInvoke(tvpsgn.target.frontal.factionIcon, "SetFile", self:getUnitFactionTexture(in_data.tpm_name.data, targetClassIndex, in_data.tpm_flag.data));

	if (in_data.tpm_ownerName.data == unusedLogFieldData) then -- не пет
		GGM.GUI.MuzzleDrawer:SetUnitMuzzle(
			false,
			false,
			tvpsgn.target.frontal.muzzleIconBasement,
			tvpsgn.target.frontal.muzzleIconModel,
			tvpsgn.target.frontal.muzzleIconAlternate,
			in_data.tpm_name.data,
			in_data.tpm_id.data
		);
		self:setUnitClassIcon(tvpsgn.target.frontal.classIcon, in_data.tpm_name.data, self.cs_classTokensReversed[targetClassIndex]);
	else	-- пет
		GGM.GUI.MuzzleDrawer:SetUnitMuzzle(
			true,
			false,
			tvpsgn.target.frontal.muzzleIconBasement,
			tvpsgn.target.frontal.muzzleIconModel,
			tvpsgn.target.frontal.muzzleIconAlternate,
			in_data.tpm_name.data,
			in_data.tpm_id.data
		);
		if (in_data.targetOwnerID.data ~= nil) then -- не опеределился хозяин
			local ownerClassIndex = 0;
			local ownerData = GGM.ACR.SpecDispatcher:GetRuntimePlayerInfoByName(in_data.tpm_ownerName.data);
			if (ownerData == nil) then
				ownerData = GGM.GUI.PlayerCluster:GetPlayerInfoByGUIDEnhanced(in_data.tpm_ownerId.data);
			end
			self:setUnitClassIcon(tvpsgn.target.frontal.classIcon, in_data.tpm_name.data, ownerData.class);
		else
			self:setUnitClassIcon(tvpsgn.target.frontal.classIcon, in_data.tpm_name.data, nil); -- по умолчанию
		end
	end

	local trgTexPrepath = GGM.ACR.CommonService:GetUnitFactionByFlag(in_data.tpm_flag.data);

	GGF.OuterInvoke(tvpsgn.target.background.common, "SetFile", self.cs_factionBackgroundTextures[trgTexPrepath]);

	if self.cs_envDamageIDS[string.lower(in_data.msd_id.data)] ~= nil then
		GGF.OuterInvoke(tvpsgn.remedy.frontal.IS, "SetFile", self.cs_envDaMageIDS[string.lower(in_data.msd_id.data)]);
	else
      local path = "";
      if (in_data.msd_id.data == 75) then   -- NOTE: autoshot
         path = self.cs_classIcons.Default;
      else
   		path = GetSpellTexture(in_data.msd_id.data);--select(3,GetSpellInfo(in_data.spellID.data));
      end
      GGF.OuterInvoke(tvpsgn.remedy.frontal.IS, "SetFile", path);
		--if (statisticSkillNameBT[in_data.msd_id.data] ~= nil and statisticSkillNameBT[in_data.msd_id.data].spellIcon ~= "") then
			--GGF.OuterInvoke(tvpsgn.remedy.frontal.IS, "SetFile", statisticSkillNameBT[in_data.msd_id.data].spellIcon);
		--else
			-- TODO: убрать/подвинуть/перенести/скопировать
			--GGF.OuterInvoke(tvpsgn.remedy.frontal.IS, "SetFile", self.cs_classIcons.Default);
		--end
	end

	-- TODO: перенести петоконстанты
	-- TODO: разобраться, почему исчезло изменение
	-- Texture coordinates
	if (sourceExists) then
		--if sourceTypeFlag == "Pet" then
			--tvpsgn.SrcFactionIcon:SetTexCoord(ACR_KS.logGeometry.numeric.iconTexCoord.Pet[1], ACR_KS.logGeometry.numeric.iconTexCoord.Pet[2], ACR_KS.logGeometry.numeric.iconTexCoord.Pet[3], ACR_KS.logGeometry.numeric.iconTexCoord.Pet[4]);
		--else
			GGF.OuterInvoke(tvpsgn.source.frontal.factionIcon, "SetTexCoord",
				self.cs_iconTexCoord.noPet[1],
				self.cs_iconTexCoord.noPet[2],
				self.cs_iconTexCoord.noPet[3],
				self.cs_iconTexCoord.noPet[4]
			);
		--end
	end
	-- В случае пета берется иконка скилла, которая не смещена относительно центра, в отличие от иконок фракций
	-- Попытка исправить оверрайдом координат
	--if targetTypeFlag == "Pet" then
		--tvpsgn.TrgFactionIcon:SetTexCoord(ACR_KS.logGeometry.numeric.iconTexCoord.Pet[1], ACR_KS.logGeometry.numeric.iconTexCoord.Pet[2], ACR_KS.logGeometry.numeric.iconTexCoord.Pet[3], ACR_KS.logGeometry.numeric.iconTexCoord.Pet[4]);
	--else
	GGF.OuterInvoke(tvpsgn.target.frontal.factionIcon, "SetTexCoord",
		self.cs_iconTexCoord.noPet[1],
		self.cs_iconTexCoord.noPet[2],
		self.cs_iconTexCoord.noPet[3],
		self.cs_iconTexCoord.noPet[4]
	);
	--end


	-- Show/hide opts
	GGF.OuterInvoke(tvpsgn.Service.frontal.delimiter, "SetHidden", true);

	GGF.OuterInvoke(tvpsgn.source.frontal.text, "SetHidden", not sourceExists);
	GGF.OuterInvoke(tvpsgn.source.frontal.factionIcon, "SetHidden", not sourceExists);
	GGF.OuterInvoke(tvpsgn.source.frontal.classIcon, "SetHidden", not sourceExists);
	GGF.OuterInvoke(tvpsgn.source.background.common, "SetHidden", not sourceExists);
	GGF.OuterInvoke(tvpsgn.source.frontal.muzzleIconBasement, "SetHidden", not sourceExists or GGF.OuterInvoke(tvpsgn.source.frontal.muzzleIconBasement, "GetHidden"));
	GGF.OuterInvoke(tvpsgn.source.frontal.muzzleIconModel, "SetHidden", not sourceExists or GGF.OuterInvoke(tvpsgn.source.frontal.muzzleIconModel, "GetHidden"));
	GGF.OuterInvoke(tvpsgn.source.frontal.muzzleIconAlternate, "SetHidden", not sourceExists or GGF.OuterInvoke(tvpsgn.source.frontal.muzzleIconAlternate, "GetHidden"));


	GGF.OuterInvoke(tvpsgn.target.frontal.text, "SetHidden", false);
	GGF.OuterInvoke(tvpsgn.target.frontal.factionIcon, "SetHidden", false);
	GGF.OuterInvoke(tvpsgn.target.frontal.classIcon, "SetHidden", false);
	GGF.OuterInvoke(tvpsgn.target.background.common, "SetHidden", false);
	-- NOTE: видимость выставилась ранее
	--tvpsgn.source.frontal.muzzleIconBasement:SetHidden();
	--tvpsgn.source.frontal.muzzleIconModel:SetHidden();
	--tvpsgn.source.frontal.muzzleIconAlternate:SetHidden();


	-- спелл есть всегда, так что привязка сурса сюда логична
	GGF.OuterInvoke(tvpsgn.remedy.frontal.IS, "SetHidden", false);
	GGF.OuterInvoke(tvpsgn.remedy.background.spell, "SetHidden", false);

	tvpsgn.remedy.IMFlag = multicastFlag;
	GGF.OuterInvoke(tvpsgn.remedy.frontal.IM, "SetHidden", not multicastFlag);
	GGF.OuterInvoke(tvpsgn.remedy.background.multistrike, "SetHidden", not multicastFlag);

	tvpsgn.remedy.ICFlag = criticalFlag;
	GGF.OuterInvoke(tvpsgn.remedy.frontal.IC, "SetHidden", not CriticalFlag);
	GGF.OuterInvoke(tvpsgn.remedy.background.Critical, "SetHidden", not CriticalFlag);

	GGM.GUI.AnLine:AdjustLayoutOrder(tvpsgn, gSuppData);

	GGM.GUI.AnLine:FadeAnimateLine(tvpsgn);

	-- Finishers
	self:masterRefresh(tvpId);

	self:anLogQueue_Add(tvpId);
end


-- api
-- Обработчик начала новой битвы(ставит в лог килов инфу о пб)
function This:proceedBattleZoneEnter(in_map, in_token)
	if (not GGM.GUI.KillFeed:GetKillFeedEnabled()) then
		return;
 	end

	local tvpsgn, tvpId = GGM.GUI.AnLine:RequestLine();

	tvpsgn.Service.ServiceFlag = true;

	tvpsgn.Service.frontal.delimiter:SetHidden(false);
	--tvpsgn.ServiceDelimeter:SetPoint(ACR_KS.logGeometry.tvpS.servDel.anchor.point, ACR_KS.anFrame, ACR_KS.logGeometry.tvpS.servDel.anchor.relativeTo, ACR_KS.logGeometry.tvpS.servDel.anchor.offsetX(), ACR_KS.logGeometry.tvpS.servDel.anchor.offsetY());
	--tvpsgn.ServiceDelimeter:SetJustifyH(ACR_KS.logGeometry.tvpS.servDel.anchor.justifyH);
	--tvpsgn.ServiceDelimeter:SetHeight(ACR_KS.logGeometry.tvpS.servDel.size.height());
	--tvpsgn.ServiceDelimeter:SetWidth(ACR_KS.logGeometry.tvpS.servDel.size.width());	-- wide minus double border
	-- Если тут токен потянуть, то получится дедлок....так что пока так
	--tvpsgn.ServiceDelimeter:SetText("Z-"..Date("%d%m%y-%H%M%S").."-"..zid.." "..zname);	
	GGF.OuterInvoke(tvpsgn.Service.frontal.delimiter, "SetText", in_map.."("..in_token..")");
	GGF.OuterInvoke(tvpsgn.Service.frontal.delimiter, "SetAlpha", 1);

	ACR_AE:CreateSimpleFadeAnimationOnFrame(GGF.INS.GetObjectRef(tvpsgn.Service.frontal.delimiter));

	self:masterRefresh(tvpId);
	self:anLogQueue_Add(tvpId);
end


-- Обработчик появления нового кила в окне килов(сдвиг всех вниз)
function This:masterRefresh(tvpId)
	local MSC_CONFIG = self:getConfig();
	for k,m in pairs(self.vs_anLogQueue.data) do
		--local v;
		--print("km : ",k, m)
		if m ~= tvpId then
			local line = GGM.GUI.AnLine:GetLine(m);--...anFrameData[m];
			--local v = ACR_KS.anFrameData[m];

			-- от знака зависит направление лога(вверх/вниз)
			local fontDelta = -(MSC_CONFIG.stringHeight + MSC_CONFIG.objectGapVer);
			if line.Service.ServiceFlag == true then
				GGF.OuterInvoke(line.Service.frontal.delimiter, "SetOffsetY", GGF.OuterInvoke(line.Service.frontal.delimiter, "GetOffsetY") + fontDelta);
			else
				if (not GGF.OuterInvoke(line.target.frontal.text, "GetHidden")) then
					GGF.OuterInvoke(line.target.frontal.text, "SetOffsetY", GGF.OuterInvoke(line.target.frontal.text, "GetOffsetY") + fontDelta);
				end

				if (not GGF.OuterInvoke(line.target.frontal.factionIcon, "GetHidden")) then
					GGF.OuterInvoke(line.target.frontal.factionIcon, "SetOffsetY", GGF.OuterInvoke(line.target.frontal.factionIcon, "GetOffsetY") + fontDelta);
				end

				if (not GGF.OuterInvoke(line.target.frontal.muzzleIconBasement, "GetHidden")) then
					GGF.OuterInvoke(line.target.frontal.muzzleIconBasement, "SetOffsetY", GGF.OuterInvoke(line.target.frontal.muzzleIconBasement, "GetOffsetY") + fontDelta);
				end

				if (not GGF.OuterInvoke(line.target.frontal.muzzleIconModel, "GetHidden")) then
					GGF.OuterInvoke(line.target.frontal.muzzleIconModel, "SetOffsetY", GGF.OuterInvoke(line.target.frontal.muzzleIconModel, "GetOffsetY") + fontDelta);
				end

				if (not GGF.OuterInvoke(line.target.frontal.muzzleIconAlternate, "GetHidden")) then
					GGF.OuterInvoke(line.target.frontal.muzzleIconAlternate, "SetOffsetY", GGF.OuterInvoke(line.target.frontal.muzzleIconAlternate, "GetOffsetY") + fontDelta);
				end

				if (not GGF.OuterInvoke(line.target.frontal.classIcon, "GetHidden")) then
					GGF.OuterInvoke(line.target.frontal.classIcon, "SetOffsetY", GGF.OuterInvoke(line.target.frontal.classIcon, "GetOffsetY") + fontDelta);
				end

				if (not GGF.OuterInvoke(line.target.background.common, "GetHidden")) then
					GGF.OuterInvoke(line.target.background.common, "SetOffsetY", GGF.OuterInvoke(line.target.background.common, "GetOffsetY") + fontDelta);
				end



				if (not GGF.OuterInvoke(line.source.frontal.text, "GetHidden")) then
					GGF.OuterInvoke(line.source.frontal.text, "SetOffsetY", GGF.OuterInvoke(line.source.frontal.text, "GetOffsetY") + fontDelta);
				end

				if (not GGF.OuterInvoke(line.source.frontal.factionIcon, "GetHidden")) then
					GGF.OuterInvoke(line.source.frontal.factionIcon, "SetOffsetY", GGF.OuterInvoke(line.source.frontal.factionIcon, "GetOffsetY") + fontDelta);
				end

				if (not GGF.OuterInvoke(line.source.frontal.muzzleIconBasement, "GetHidden")) then
					GGF.OuterInvoke(line.source.frontal.muzzleIconBasement, "SetOffsetY", GGF.OuterInvoke(line.source.frontal.muzzleIconBasement, "GetOffsetY") + fontDelta);
				end

				if (not GGF.OuterInvoke(line.source.frontal.muzzleIconModel, "GetHidden")) then
					GGF.OuterInvoke(line.source.frontal.muzzleIconModel, "SetOffsetY", GGF.OuterInvoke(line.source.frontal.muzzleIconModel, "GetOffsetY") + fontDelta);
				end

				if (not GGF.OuterInvoke(line.source.frontal.muzzleIconAlternate, "GetHidden")) then
					GGF.OuterInvoke(line.source.frontal.muzzleIconAlternate, "SetOffsetY", GGF.OuterInvoke(line.source.frontal.muzzleIconAlternate, "GetOffsetY") + fontDelta);
				end

				if (not GGF.OuterInvoke(line.source.frontal.classIcon, "GetHidden")) then
					GGF.OuterInvoke(line.source.frontal.classIcon, "SetOffsetY", GGF.OuterInvoke(line.source.frontal.classIcon, "GetOffsetY") + fontDelta);
				end

				if (not GGF.OuterInvoke(line.source.background.common, "GetHidden")) then
					GGF.OuterInvoke(line.source.background.common, "SetOffsetY", GGF.OuterInvoke(line.source.background.common, "GetOffsetY") + fontDelta);
				end



				if (not GGF.OuterInvoke(line.remedy.frontal.IS, "GetHidden")) then
					GGF.OuterInvoke(line.remedy.frontal.IS, "SetOffsetY", GGF.OuterInvoke(line.remedy.frontal.IS, "GetOffsetY") + fontDelta);
				end

				if (not GGF.OuterInvoke(line.remedy.frontal.IC, "GetHidden")) then
					GGF.OuterInvoke(line.remedy.frontal.IC, "SetOffsetY", GGF.OuterInvoke(line.remedy.frontal.IC, "GetOffsetY") + fontDelta);
				end

				if (not GGF.OuterInvoke(line.remedy.frontal.IM, "GetHidden")) then
					GGF.OuterInvoke(line.remedy.frontal.IM, "SetOffsetY", GGF.OuterInvoke(line.remedy.frontal.IM, "GetOffsetY") + fontDelta);
				end

				if (not GGF.OuterInvoke(line.remedy.background.spell, "GetHidden")) then
					GGF.OuterInvoke(line.remedy.background.spell, "SetOffsetY", GGF.OuterInvoke(line.remedy.background.spell, "GetOffsetY") + fontDelta);
				end

				if (not GGF.OuterInvoke(line.remedy.background.Critical, "GetHidden")) then
					GGF.OuterInvoke(line.remedy.background.Critical, "SetOffsetY", GGF.OuterInvoke(line.remedy.background.Critical, "GetOffsetY") + fontDelta);
				end

				if (not GGF.OuterInvoke(line.remedy.background.multistrike, "GetHidden")) then
					GGF.OuterInvoke(line.remedy.background.multistrike, "SetOffsetY", GGF.OuterInvoke(line.remedy.background.multistrike, "GetOffsetY") + fontDelta);
				end
			end

			if k >= self.vs_anLogQueue.current_range then
				GGM.GUI.AnLine:TvpsgnHide(line);
			end
		end
	end
end


-- 
function This:anLogQueue_Add(value_ind)
	
	for i = self.vs_anLogQueue.last,1,-1 do -- reversed iteration
		self.vs_anLogQueue.data[i] = self.vs_anLogQueue.data[i-1];
	end

	if self.vs_anLogQueue.last < self.vs_anLogQueue.limit then
		self.vs_anLogQueue.last = self.vs_anLogQueue.last + 1
	end

	self.vs_anLogQueue.data[1] = value_ind; 
end


-- TODO: переделать и включить(GG-37)
-- Внешняя функция управления сменой высоты лога(отключена)
function This:proceedHeightChange(in_height)
	if tonumber(in_height) == nil then
		-- TODO: приколхохить нормальную ошибку
		print("NaN error");
		return;
	end

	local height = math.floor(tonumber(in_height));

	if height > ACR_KS.anFrameData.max_size then
		print("oversized height");
		return;
	end

	for i=height+1,self.vs_anLogQueue.current_range do
		ACR_KS.tvpsgnHide(ACR_KS.anFrameData[self.vs_anLogQueue.data[i]]);
	end

	if height <= #self.vs_anLogQueue.data then
		for i=self.vs_anLogQueue.current_range+1, height do
			ACR_KS.tvpsgnShow(ACR_KS.anFrameData[self.vs_anLogQueue.data[i]]);
		end
	else
		for i=self.vs_anLogQueue.current_range+1, #self.vs_anLogQueue.data do
			ACR_KS.tvpsgnShow(ACR_KS.anFrameData[self.vs_anLogQueue.data[i]]);
		end
	end

	self.vs_anLogQueue.current_range = height;

	--ACR_MainFrame:SetHeight(height*ACR_KS.logFontDelta + ACR_KS.headerHeight)
	ACR_MainFrame:SetHeight(ACR_KS.logGeometry.MainFrame.height());
end


-- Установка иконки класса по индексу
function This:setUnitClassIcon(in_frame, in_unitName, in_unitClassToken)
	--UFP = "UnitFramePortrait_UpDate";
	--UiCC = ACR_KS.classIcons.standartPane;
	--CIT = CLASS_ICON_TCOORDS;
	--t = CIT[in_unitClassToken]; 
	local iconTex = self.cs_classIcons[self.cs_classTokens[in_unitClassToken]];
	if iconTex then
		--print("t yup")
		GGF.OuterInvoke(in_frame, "SetFile", iconTex);
		--in_frame:SetTexCoord(unpack(t));
	else
		--in_frame:SetTexture(ACR_KS.classIcons.Default);
	end
end


-- Определитель текстуры фракции для окна килов
function This:getUnitFactionTexture(name, clind, in_flag)
	return self.cs_factionIconSet[GGM.ACR.CommonService:getUnitFactionByFlag(in_flag)]();
end


-- Определитель шрифта по классу чара
function This:getClassFont(index)
	if self.cs_classFontArray[index] == nil then
		--print("Unknown index: ", index)
		return "ACR_CLRDefault";
	end
	return self.cs_classFontArray[index];
end


-- Определитель класса по GUiD чара(иногда стало критовать начиная с 7.2.5 примерно)
function This:getUnitClass(in_idn)
	--print("idn: ", idn)
	--local gc, gcf, gr, grf, gs, gn, gr = GetPlayerInfoByGUiD(idn);
	local unitInfo = GGM.GUI.PlayerCluster:GetPlayerInfoByGUIDEnhanced(in_idn);

	if unitInfo ~= nil then
		--print(gcf);
		return self.cs_classTokens[unitInfo.classFilename];
	end

	--local _,_,uc = UnitClass(idn);
	--if uc ~= nil then
		--return uc;
	--end
	
	-- TODO: узнать/вспомнить/сколхозить что тут было раньше..
	local APCCluster = nil;
	-- ACR_SD.arenaPlayerClassCluster[idn]
	-- Там таблица спеков(т.е. асс. массив "имя" -> "спек")
	if APCCluster ~= nil then
		return APCCluster[idn];
	end

	return 0;
end


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------