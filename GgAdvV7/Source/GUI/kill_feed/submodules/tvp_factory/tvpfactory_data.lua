local This = GGF.ModuleCreateWrapper("GUI-4-5", false, {
	-- mainframe
	mainFrameWidth = 430,
	mainFrameHeight = 70,

	-- soruce/target
	objectBackgroundExtenderX = 10,

	-- class icons
	classIconSide = 26, -- дб равна стороне мордахи

	-- source/target fonts
	fontShadowOffsetX = 2,
	fontShadowOffsetY = -2,
	fontSymbolWidth = 12,

	-- Object in string gaps
	objectGapHor = 10,
	objectGapVer = 26,

	-- string common parameters(font parameters)
	stringBackgroundHeight = 28,
	stringHeight = 12,
	stringHeaderBuffer = 14,

	-- spell/crit/multicast icons(called spell icons)
	spellIconHeight = 26,
	spellBackgroundInset = 4,
	spellBackgroundExtenderX = 10,

	-- faction
	factionIconUniSize = 26,

	specificLengthBySymbol = 12,
});


-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------


-- faction icon
This.cs_iconTexCoord = {
	Pet   = { 0.00, 1.00, 0.00, 1.00 },
	noPet = { 0.05, 0.57, 0.05, 0.57 },
};


This.cs_classFontArray = {
	"ACR_CLRWarrior",
	"ACR_CLRPaladin",
	"ACR_CLRHunter",
	"ACR_CLRRogue",
	"ACR_CLRPriest",
	"ACR_CLRDeathKnight",
	"ACR_CLRShaman",
	"ACR_CLRMage",
	"ACR_CLRWarlock",
	"ACR_CLRMonk",
	"ACR_CLRDrUid"
};


-- TODO: Описать привязку к объекту в common_Service
-- А хз почему тут функтор...
This.cs_factionIconSet = {
	[GGE.UnitFaction.Alliance] = function() return "Interface\\TargetingFrame\\Ui-PVP-Alliance" end,
	[GGE.UnitFaction.Horde] = function() return "Interface\\TargetingFrame\\Ui-PVP-Horde" end,
	[GGE.UnitFaction.Default] = function() return "Interface\\TargetingFrame\\Ui-PVP-FFA" end,
	--["Pet"] = function() return FCS.getPetGlyphIcon() end,
};


This.cs_classIcons = {
	[1] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Warrior.blp", -- Warrior
	[2] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Paladin.blp", -- Paladin
	[3] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Hunter.blp", -- Hunter
	[4] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Rogue.blp", -- Rogue
	[5] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Priest.blp", -- Priest
	[6] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_DeathKnight.blp", -- DeathKnight
	[7] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Shaman.blp", -- Shaman
	[8] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Mage.blp", -- Mage
	[9] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Warlock.blp", -- Warlock
	[10] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_Monk.blp", -- Monk
	[11] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_DrUid.blp", -- DrUid
	[12] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\ClassIcon_DemonHunter.blp", -- DemonHunter
	Default = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\classes\\INV_Misc_QuestionMark.blp"
};

This.cs_classTokens = {
	["Warrior"] = 1,
	["Paladin"] = 2,
	["Hunter"] = 3,
	["Rogue"] = 4,
	["Priest"] = 5,
	["DeathKnight"] = 6,
	["Shaman"] = 7,
	["Mage"] = 8,
	["Warlock"] = 9,
	["Monk"] = 10,
	["DrUid"] = 11,
	["DemonHunter"] = 12,
};

This.cs_classTokensReversed = {
	"Warrior",
	"Paladin",
	"Hunter",
	"Rogue",
	"Priest",
	"DeathKnight",
	"Shaman",
	"Mage",
	"Warlock",
	"Monk",
	"DrUid",
	"DemonHunter",
};


This.cs_classColor = {
	[1] = GGD.Color.Warrior,
	[2] = GGD.Color.Paladin,
	[3] = GGD.Color.Hunter,
	[4] = GGD.Color.Rogue,
	[5] = GGD.Color.Priest,
	[6] = GGD.Color.DeathKnight,
	[7] = GGD.Color.Shaman,
	[8] = GGD.Color.Mage,
	[9] = GGD.Color.Warlock,
	[10] = GGD.Color.Monk,
	[11] = GGD.Color.Druid,
	[12] = GGD.Color.DemonHunter,
	Default = GGD.Color.Unclassed,
};


This.cs_factionBackgroundTextures = {
	[GGE.UnitFaction.Default] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\gray_left.tga",
	[GGE.UnitFaction.Alliance] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\Blue_left.tga",
	[GGE.UnitFaction.Horde] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\Red_left.tga",
};


This.cs_multicastedSpells = {
	["45284"] = true,
	["45297"] = true,
	["120588"] = true,
	["77451"] = true,
};


This.cs_envDamageIDS = {
	["drowning"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\environment\\drowning_2.tga",
	["falling"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\environment\\falling_2.tga",
	["fatigue"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\environment\\fatigue_2.tga",
	["fire"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\environment\\fire_2.tga",
	["lava"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\environment\\lava_2.tga",
	["slime"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\environment\\slime.tga",
	["Undefined"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\environment\\fatigue.tga",
};


-------------------------------------------------------------------------------
-- Functors
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable lines
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------


This.vs_anLogQueue = {
	current_range = 12,	
	first = 1,
	last = 1,
	limit = 100, -- for 170px height
	--memorised = 0,
	data = {},
};


-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------