local This = GGF.ModuleGetWrapper("GUI-4-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_muzzleDrawerGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		srcMuzzleIconBasement = {},	-- [SMIB_SF]
		trgMuzzleIconBasement = {},	-- [TMIB_SF]
	}),
   playerModel = GGF.GTemplateTree(GGC.PlayerModel, {
      srcMuzzleIconModel = {},      -- [SMIM_PM]
      trgMuzzleIconModel = {},      -- [TMIM_PM]
   }),
};

local MDG_SF = This.oc_muzzleDrawerGeometry.standardFrame;
local MDG_PM = This.oc_muzzleDrawerGeometry.playerModel;

-- srcMuzzleIconBasement(SF)
MDG_SF.srcMuzzleIconBasement.object = {};
MDG_SF.srcMuzzleIconBasement.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "SMIB",
		},
		miscellaneous = {
			enableMouse = false,
			hidden = false,
		},
		backdrop = {
			--bgFile = [[Interface\BUTTONS\WHITE8X8]],
			--edgeFile = "Interface\\Tooltips\\Ui-Tooltip-Border",
			edgeFile = "",
			tile = false,
			edgeSize = MSC_CONFIG.muzzleEdgeSize,
			tileSize = MSC_CONFIG.muzzleTileSize,
			insets = {
				left = MSC_CONFIG.muzzleBackgroundInset,
				right = MSC_CONFIG.muzzleBackgroundInset,
				top = MSC_CONFIG.muzzleBackgroundInset,
				bottom = MSC_CONFIG.muzzleBackgroundInset,
			},
			color = {
				R = 0.05,
				G = 0.1,
				B = 0.15,
				A = 1.0,
			},
			borderColor = {
				R = 1.0,
				G = 1.0,
				B = 1.0,
				A = 1.0,
			},
		},
		size = {
			width = MSC_CONFIG.muzzleIconSide,
			height = MSC_CONFIG.muzzleIconSide,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetX = function(in_argline) return in_argline.pOffsetX + in_argline.pWidth + MSC_CONFIG.ObjectGapHor end,
			offsetY = 0,
		},
	},
};


-- trgMuzzleIconBasement(SF)
MDG_SF.trgMuzzleIconBasement.object = {};
MDG_SF.trgMuzzleIconBasement.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "TMIB",
		},
		miscellaneous = {
			enableMouse = false,
			hidden = false,
		},
		backdrop = {
			--bgFile = "Interface\\BUTTONS\\WHITE8X8",
			edgeFile = "",
			tile = false,
			edgeSize = MSC_CONFIG.muzzleEdgeSize,
			tileSize = MSC_CONFIG.muzzleTileSize,
			insets = {
				left = MSC_CONFIG.muzzleBackgroundInset,
				right = MSC_CONFIG.muzzleBackgroundInset,
				top = MSC_CONFIG.muzzleBackgroundInset,
				bottom = MSC_CONFIG.muzzleBackgroundInset,
			},
			color = {
				R = 0.05,
				G = 0.1,
				B = 0.15,
				A = 1.0,
			},
			borderColor = {
				R = 1.0,
				G = 1.0,
				B = 1.0,
				A = 1.0,
			},
		},
		size = {
			width = MSC_CONFIG.muzzleIconSide,
			height = MSC_CONFIG.muzzleIconSide,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetX = function(in_argline) return in_argline.pOffsetX + in_argline.pWidth + MSC_CONFIG.ObjectGapHor end,
			offsetY = 0,
		},
	},
};


-- srcMuzzleIconModel(PM)
MDG_PM.srcMuzzleIconModel.object = {};
MDG_PM.srcMuzzleIconModel.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "SMIM",
		},
		size = {
			width = MSC_CONFIG.muzzleIconSide - MSC_CONFIG.muzzleIconShift,
			height = MSC_CONFIG.muzzleIconSide - MSC_CONFIG.muzzleIconShift,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetX = function(in_argline) return in_argline.pOffsetX + in_argline.pWidth + MSC_CONFIG.ObjectGapHor - 0.5*MSC_CONFIG.muzzleIconShift end,
			offsetY = 0,
		},
	},
};


-- trgMuzzleIconModel(PM)
MDG_PM.trgMuzzleIconModel.object = {};
MDG_PM.trgMuzzleIconModel.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "TMIM",
		},
		size = {
			width = MSC_CONFIG.muzzleIconSide - MSC_CONFIG.muzzleIconShift,
			height = MSC_CONFIG.muzzleIconSide - MSC_CONFIG.muzzleIconShift,
		},
		anchor = {
			point = GGE.PointType.Left,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Left,
			offsetX = function(in_argline)
            return in_argline.pOffsetX + in_argline.pWidth + MSC_CONFIG.ObjectGapHor - 0.5*MSC_CONFIG.muzzleIconShift
         end,
			offsetY = 0,
		},
	},
};