local This = GGF.ModuleCreateWrapper("GUI-4-2", false, {
	-- muzzle System(back + model)
	muzzleIconSide = 26,
	muzzleInset = 1, 
	muzzleBackgroundInset = 0, -- зазор между фоном и границей(чтоб не торчало за пределы)
	muzzleEdgeSize = 5,	-- можно поподгонять(мигает)
	muzzleTileSize = 1,
	muzzleIconShift = 1, -- подогнать этот параметр

	-- зазор слева/справа
	ObjectGapHor = 10
});

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Constant line
-------------------------------------------------------------------------------

This.cl_petGlyphIcon = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Ability_Hunter_MendPet.blp";

-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------

This.cs_raceIconSet = {
	-- male
	[2] = {
		["BloodElf"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Bloodelf_Male.blp",
		["Draenei"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Draenei_Male.blp",
		["Dwarf"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Dwarf_Male.blp",
		["Gnome"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Gnome_Male.blp",
		["Goblin"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Goblin_Male.blp",
		["Human"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Human_Male.blp",
		["NightElf"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_NightElf_Male.blp",
		["Orc"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Orc_Male.blp",
		["Pandaren"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Pandaren_Male.blp",
		["Tauren"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Tauren_Male.blp",
		["Troll"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Troll_Male.blp",
		["Scourge"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Undead_Male.blp",
		["Worgen"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Worgen_Male.blp",
	},
	-- female
	[3] = {
		["BloodElf"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Bloodelf_Female.blp",
		["Draenei"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Draenei_Female.blp",
		["Dwarf"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Dwarf_Female.blp",
		["Gnome"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Gnome_Female.blp",
		["Goblin"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Goblin_Female.blp",
		["Human"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Human_Female.blp",
		["NightElf"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_NightElf_Female.blp",
		["Orc"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Orc_Female.blp",
		["Pandaren"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Pandaren_Female.blp",
		["Tauren"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Tauren_Female.blp",
		["Troll"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Troll_Female.blp",
		["Scourge"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Undead_Female.blp",
		["Worgen"] = "Interface\\AddOns\\GgAdvV7\\Textures\\ACR\\SAA\\races\\Achievement_Character_Worgen_Female.blp",
	},
};

-------------------------------------------------------------------------------
-- Functors
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------

This.vn_frameNumber = 0;

-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Variable line
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------