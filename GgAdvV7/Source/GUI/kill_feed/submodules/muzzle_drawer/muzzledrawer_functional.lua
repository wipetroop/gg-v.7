local This = GGF.ModuleGetWrapper("GUI-4-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------

-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end

-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-- TODO: найти возможность повернуть модельку(GG-41)
-- api
-- Установка мордашек в строку tvp
function This:setUnitMuzzle(in_Pet, in_src, in_basementFrame, in_modelFrame, in_altFrame, in_unitName, in_unitGUID)
	GGF.OuterInvoke(in_modelFrame, "ClearModel");
	GGF.OuterInvoke(in_modelFrame, "SetPortraitZoom", 1);
	GGF.OuterInvoke(in_modelFrame, "SetCamDistanceScale", 1);
	GGF.OuterInvoke(in_modelFrame, "SetPosition", 0, 0, 0);
	if UnitExists(in_unitName) then
		GGF.OuterInvoke(in_modelFrame, "SetUnit", in_unitName);
		GGF.OuterInvoke(in_modelFrame, "SetHidden", false);
		GGF.OuterInvoke(in_basementFrame, "SetHidden", false);
		GGF.OuterInvoke(in_altFrame, "SetHidden", true);
		--SetPortraitTexture(in_modelFrame, in_unitName);
	else
		-- NOTE: если там петик без, то все значения nil
		local unitInfo = GGM.GUI.PlayerCluster:GetPlayerInfoByGUIDEnhanced(in_unitGUID);
		--class, classFilename, race, raceFilename, sex, name, realm = GetPlayerInfoByGUiD(in_unitGUID);
		--print(in_unitName, class, classFilename, race, raceFilename, sex, name, realm);

		if (unitInfo ~= nil) then
			GGF.OuterInvoke(in_altFrame, "SetFile", self.cs_raceIconSet[unitInfo.sex][unitInfo.raceFilename]);
		else
			GGF.OuterInvoke(in_altFrame, "SetFile", self.cl_petGlyphIcon);
		end
		
		GGF.OuterInvoke(in_modelFrame, "SetHidden", true);
		GGF.OuterInvoke(in_basementFrame, "SetHidden", true);
		GGF.OuterInvoke(in_altFrame, "SetHidden", false);
		--SetPortraitTexture(in_texFrame, in_unitName);
		--in_frame:SetModel('interface\\buttons\\talktomequestionmark.m2');
	end
	--in_frame:SetCamera(0);
end

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------

-- api
-- Создание фрейма под модель мордахи
function This:getNewMuzzleModelFrame(in_parent)
	self.vn_frameNumber = self.vn_frameNumber + 1;
	return GGC.PlayerModel:Create(self.JR_SMIM_PM_TMP, {
		properties = {
			base = {
				wrapperName = "SMIM_" .. tostring(self.vn_frameNumber),
				parent = in_parent,
			},
			anchor = {
				relativeTo = in_parent,
			},
		},
	});
end

-- api
-- Создание фрейма под фон мордахи
function This:getNewMuzzleBackgroundFrame(in_parent)
	self.vn_frameNumber = self.vn_frameNumber + 1;
	return GGC.StandardFrame:Create(self.JR_SMIB_SF_TMP, {
		properties = {
         base = {
   			wrapperName = "SMIB_" .. tostring(self.vn_frameNumber),
   			parent = in_parent,
   		},
			anchor = {
				relativeTo = in_parent,
			},
		},
	});
end

-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "service" block
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------