local This = GGF.ModuleGetWrapper("GUI-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_killFeedGeometry = {
	standardFrame = GGF.GTemplateTree(GGC.StandardFrame, {
		killFeed = {},				-- [KF_SF]
	}),
	killFeed = GGF.GTemplateTree(GGC.KillFeedFrame, {
		killFeedInternal = {},		-- [KFI_KF]
	}),
};

local KFG_SF = This.oc_killFeedGeometry.standardFrame;
local KFG_KF = This.oc_killFeedGeometry.killFeed;

-- killFeed(SF)
KFG_SF.killFeed.object = {};
KFG_SF.killFeed.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "KF",
		},
		miscellaneous = {
			hidden = false,
			movable = true,
			enableMouse = false,	-- TODO: разобраться, почему здесь true стояло
			enableKeyboard = false,
		},
		size = {
			width = MSC_CONFIG.width,
			height = MSC_CONFIG.height,
		},
		anchor = {
			point = GGE.PointType.TopLeft,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.TopLeft,
			offsetX = 0,
			offsetY = 0,
		},
		--backdrop = GGD.Backdrop.Segregate,
		backdrop = GGD.Backdrop.Empty,
	},
};

-- killFeedInternal(KF)
KFG_KF.killFeedInternal.object = {};
KFG_KF.killFeedInternal.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "KFI",
		},
		miscellaneous = {
			hidden = false,
			movable = true,
			enableMouse = false,	-- TODO: разобраться, почему здесь true стояло
			enableKeyboard = false,
		},
		size = {
			width = MSC_CONFIG.width,
			height = MSC_CONFIG.height,
		},
		anchor = {
			point = GGE.PointType.Center,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.Center,
			offsetX = 0,
			offsetY = 0,
		},
		--backdrop = GGD.Backdrop.Nested,
		backdrop = GGD.Backdrop.Empty,
	},
};