local This = GGF.ModuleGetWrapper("GUI-4");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-- api
-- Установка видимости окна фона лога
function This:setHidden(in_hdn)
	GGF.OuterInvoke(self.JR_KF_SF_OBJ, "SetHidden", in_hdn);
end


-- api
-- Управление режимом работы киллфида
function This:setKillFeedEnbaled(in_enb)
	self.vf_killFeedEnabled = in_enb;
end


-- api
-- Управление геометрической местом панели по оси X
function This:setOffsetX(in_nWOffsetX)
	--print("offset x changed", in_nWOffsetX);
	GGF.OuterInvoke(self.JR_KF_SF_OBJ, "SetOffsetX", in_nWOffsetX);
end


-- api
-- Управление геометрической местом панели по оси Y
function This:setOffsetY(in_nWOffsetY)
	GGF.OuterInvoke(self.JR_KF_SF_OBJ, "SetOffsetY", in_nWOffsetY);
end


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Взятие режима работы киллфида
function This:getKillFeedEnabled()
	return self.vf_killFeedEnabled;
end


-- api
-- Взятие ширины основной панели
function This:getWidth()
	return GGF.OuterInvoke(self.JR_KF_SF_OBJ, "GetWidth");
end


-- api
-- Взятие высоты основной панели
function This:getHeight()
	return GGF.OuterInvoke(self.JR_KF_SF_OBJ, "GetHeight");
end


-- api
-- Взятие фрейма рантайм лога килов
-- NOTE: только для модуля an_line
function This:getFrame()
	return GGF.INS.GetObjectRef(self.JR_KF_SF_OBJ);
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-- TODO: убрать...(GG-37)
-- Внешняя функция управления сменой высоты лога
function This:proceedHeightChange(in_height)
	--GGM.GUI.TVPFactory:ProceedHeightChange(in_height);
end

-- api
-- Стандартный обработчик события кила из лога
function This:proceedKill(in_data)
   if (not GGM.GUI.KillFeed:getKillFeedEnabled()) then
      return;
   end
	GGF.OuterInvoke(self.JR_KFI_KF_OBJ, "ProceedKill", in_data);
end

-- api
-- Обработчик начала новой битвы(ставит в лог килов инфу о пб)
function This:proceedBattleZoneEnter(in_map, in_token)
	if (not GGM.GUI.KillFeed:getKillFeedEnabled()) then
		return;
	end
   GGF.OuterInvoke(self.JR_KFI_KF_OBJ, "ProceedBattleZoneEnter", in_map, in_token);
end

-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------