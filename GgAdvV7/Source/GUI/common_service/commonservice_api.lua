local This = GGF.ModuleGetWrapper("GUI-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-- Установка значений в элемент таблицы
function This:SetTableElementData(in_datArray, in_index, in_val, in_colorData)
	self:setTableElementData(in_datArray, in_index, in_val, in_colorData)
end

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-- Обработчик наведения на клетку таблицы
function This:OnCommonTableCellEnter(in_rowFrame, in_cellFrame, in_data, in_cols, in_row, in_realrow, in_column, in_scrollingTable, ...)
	self:onCommonTableCellEnter(in_rowFrame, in_cellFrame, in_data, in_cols, in_row, in_realrow, in_column, in_scrollingTable, ...);
end


-- Обработчик денаведения с клетки таблицы
function This:OnCommonTableCellLeave(in_rowFrame, in_cellFrame, in_data, in_cols, in_row, in_realrow, in_column, in_scrollingTable, ...)
	self:onCommonTableCellLeave(in_rowFrame, in_cellFrame, in_data, in_cols, in_row, in_realrow, in_column, in_scrollingTable, ...);
end


-- Общая функция обработки наведения на ячейку в таблице
function This:OnMouseFocusCell(in_self, in_data)
	self:onMouseFocusCell(in_self, in_data);
end


-- Общая функция обработки денаведения на ячейку в таблице
function This:OnMouseDefocusCell(in_self)
	self:onMouseDefocusCell(in_self)
end


-- Общая функция обработки наведения на фрейм
function This:OnMouseFocusFrame(in_self)
	self:onMouseFocusFrame(in_self)
end


-- Общая функция обработки денаведения на фрейм
function This:OnMouseDefocusFrame(in_self)
	self:onMouseDefocusFrame(in_self)
end


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------