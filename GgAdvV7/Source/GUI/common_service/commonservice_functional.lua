local This = GGF.ModuleGetWrapper("GUI-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------

-- Установка значений в элемент таблицы
function This:setTableElementData(in_datArray, in_index, in_text, in_textColor, in_tooltip, in_tooltipColor)
   if (in_datArray[in_index] == nil) then
      in_datArray[in_index] = {};
   end
   in_datArray[in_index] = {
      text = in_text,
      textColor = in_textColor,
      tooltip = GGF.TernExpSingle(in_tooltip == nil, "N/D", in_tooltip),
      tooltipColor = GGF.TernExpSingle(in_tooltipColor == nil, GGD.Color.Neutral, in_tooltipColor)
   };
end

-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-- Обработка клика на пиктограмму около миникарты
function This:proceedMinimapClick()
	GGM.GUI.MainWindow:SetHidden(not GGM.GUI.MainWindow:GetHidden());
end


-- api
-- Обработчик наведения на клетку таблицы
function This:onCommonTableCellEnter(in_rowFrame, in_cellFrame, in_data, in_cols, in_row, in_realrow, in_column, in_scrollingTable, ...)
	if in_data[in_realrow] == nil or in_data[in_realrow].cols == nil then
		return;
	end
	self:onMouseFocusCell(in_cellFrame, in_data[in_realrow].cols[in_column]);
end


-- api
-- Обработчик денаведения с клетки таблицы
function This:onCommonTableCellLeave(in_rowFrame, in_cellFrame, in_data, in_cols, in_row, in_realrow, in_column, in_scrollingTable, ...)
	self:onMouseDefocusCell(in_cellFrame);
end


-- api
-- Общая функция обработки наведения на ячейку в таблице
function This:onMouseFocusCell(in_self, in_data)
	GameTooltip:SetOwner(in_self, "ANCHOR_RIGHT");
	GameTooltip:SetText(in_data.value, nil, nil, nil, nil, true);
	GameTooltip:Show();
end


-- api
-- Общая функция обработки денаведения на ячейку в таблице
function This:onMouseDefocusCell(in_self)
	GameTooltip:Hide();
end


-- api
-- Общая функция обработки наведения на фрейм
function This:onMouseFocusFrame(in_self)
	GameTooltip:SetOwner(in_self, "ANCHOR_RIGHT");
	GameTooltip:SetText(in_self.tooltip_text, nil, nil, nil, nil, true);
	GameTooltip:Show();
end


-- api
-- Общая функция обработки денаведения на фрейм
function This:onMouseDefocusFrame(in_self)
	GameTooltip:Hide();
end


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------