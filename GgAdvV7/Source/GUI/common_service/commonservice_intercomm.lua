local This = GGF.ModuleGetWrapper("GUI-2");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.2 (INT Check)
function This:MS_CHECK_INT()
	return 1;
end


-------------------------------------------------------------------------------
-- "initialize" block
-------------------------------------------------------------------------------


-- п.2.1 (Miscellaneous Variables)
function This:MPRC_MVR_INITIALIZE()
	self.vs_hInstance.dbr = LibStub("LibDataBroker-1.1", true);
	self.vs_hInstance.dbi = self.vs_hInstance.dbr and LibStub("LibDBIcon-1.0", true);
end


-- п.2.3 (Objects)
function This:MPRC_OBJ_INITIALIZE()
	-- Пока что не реализовано через ооп, хотя тут оно и не надо скорее всего
	-- NOTE: пока что тут не будет ООП классов
	if self.vs_hInstance.dbr then
		local Launcher = self.vs_hInstance.dbr:NewDataObject(self.cs_minimapIcon.addonTitle, {
			type = self.cs_minimapIcon.iconType,
			icon = self.cs_minimapIcon.iconPath,
			OnClick = function(in_clickedFrame, in_button)
				if in_button == "RightButton" then else self:proceedMinimapClick() end
			end,
			OnTooltipShow = function(in_textTitle)
				--tt:AddLine(self.DefaultTitle)
				-- Тут надо найти вариант, чтобы текст был по центру
				in_textTitle:AddLine(self.cs_minimapIcon.textTitle);
			end,
		});
		if not GGMinimapIcon then GGMinimapIcon = {} end
		if self.vs_hInstance.dbi then
			self.vs_hInstance.dbi:Register(self.cs_minimapIcon.addonTitle, Launcher, GGMinimapIcon);
		end
	end
end


-------------------------------------------------------------------------------
-- "timer" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "finalize" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------