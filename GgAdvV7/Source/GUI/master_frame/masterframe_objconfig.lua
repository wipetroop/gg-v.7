local This = GGF.ModuleGetWrapper("GUI-6");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.4 (OBC Check)
function This:MS_CHECK_OBC()
	return 1;
end


-------------------------------------------------------------------------------
-- Local objconfigs
-------------------------------------------------------------------------------


local MSC_CONFIG = This:getConfig();


-- Hierarchy
This.oc_masterFrameGeometry = {
	masterFrame = GGF.GTemplateTree(GGC.MasterFrame, {
		master = {},		-- [M_MF]
	}),
};

local MFG_MF = This.oc_masterFrameGeometry.masterFrame;

-- master(MF)
MFG_MF.master.object = {};
MFG_MF.master.template = {
	---------------------------------------------------------------------------
	-- Config
	---------------------------------------------------------------------------
	properties = {
		base = {
			parentName = nil,	-- fwd
			parent = nil,		-- fwd
			wrapperName = "M",
		},
		miscellaneous = {
			virtual = false,
			hidden = false,
		},
		size = {
			width = function() return GGM.FCS.RuntimeStorage:GetScreenWidth() end,
			height = function() return GGM.FCS.RuntimeStorage:GetScreenHeight() end,
		},
		anchor = {
			point = GGE.PointType.TopLeft,
			relativeTo = nil,	-- fwd
			relativePoint = GGE.PointType.TopLeft,
			offsetX = 0,
			offsetY = 0,
		},
	},
};