local This = GGF.ModuleGetWrapper("GUI-7");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.0 (API Check)
function This:MS_CHECK_API()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- Взятие ширины фрейма main_window
function This:GetMWFrameWidth()
	return self:getMWFrameWidth();
end


-- Взятие высоты фрейма main_window
function This:GetMWFrameHeight()
	return self:getMWFrameHeight();
end


-- Взятие высоты фрейма alert_dispatcher
function This:GetADFrameHeight()
	return self:getADFrameHeight();
end

-- Взятие высоты фрейма alert_dispatcher table_engine
function This:GetADTEFrameHeight()
   return self:getADTEFrameHeight();
end

-- Взятие высоты фрейма control_panel
function This:GetCPFrameHeight()
	return self:getCPFrameHeight();
end


-- Взятие высоты фрейма control_panel common_service
function This:GetCPCSFrameHeight()
	return self:getCPCSFrameHeight();
end


-- Взятие высоты фрейма control_panel effect_manager
function This:GetCPEMFrameHeight()
	return self:getCPEMFrameHeight();
end


-- Взятие высоты фрейма control_panel effect_manager audio_control
function This:GetCPEMACFrameHeight()
	return self:getCPEMACFrameHeight();
end


-- Взятие высоты фрейма control_panel effect_manager video_control
function This:GetCPEMVCFrameHeight()
	return self:getCPEMVCFrameHeight();
end


-- Взятие высоты фрейма control_panel hud_geometry
function This:GetCPHGFrameHeight()
	return self:getCPHGFrameHeight();
end


-- Взятие высоты фрейма control_panel logging_manager
function This:GetCPLMFrameHeight()
	return self:getCPLMFrameHeight();
end


-- Взятие высоты фрейма data_service
function This:GetDSFrameHeight()
	return self:getDSFrameHeight();
end


-- Взятие ширины фрейма data_service chat_report
function This:GetDSCRFrameWidth()
	return self:getDSCRFrameWidth();
end


-- Взятие высоты фрейма data_service chat_report
function This:GetDSCRFrameHeight()
	return self:getDSCRFrameHeight();
end


-- Взятие ширины фрейма data_service data_filter
function This:GetDSDFFrameWidth()
	return self:getDSDFFrameWidth();
end


-- Взятие высоты фрейма data_service data_filter
function This:GetDSDFFrameHeight()
	return self:getDSDFFrameHeight();
end


-- Взятие высоты фрейма data_service graphic_engine
function This:GetDSGEFrameHeight()
	return self:getDSGEFrameHeight();
end


-- Взятие ширины фрейма data_service log_filter
function This:GetDSLFFrameWidth()
	return self:getDSLFFrameWidth();
end


-- Взятие высоты фрейма data_service log_filter
function This:GetDSLFFrameHeight()
	return self:getDSLFFrameHeight();
end


-- Взятие высоты фрейма data_service table_engine
function This:GetDSTEFrameHeight()
	return self:getDSTEFrameHeight();
end


-- Взятие высоты фрейма information_panel
function This:GetIPFrameHeight()
	return self:getIPFrameHeight();
end


-- Взятие ширины кнопки
function This:GetButtonWidth()
	return self:getButtonWidth();
end


-- Взятие высоты кнопки
function This:GetButtonHeight()
	return self:getButtonHeight();
end


-- Взятие ширины вертикального слайдера
function This:GetVSliderWidth()
	return self:getVSliderWidth();
end


-- Взятие ширины оперативного окна
function This:GetOperationWidth()
	return self:getOperationWidth();
end


-- Взятие стартового отступа кнопки переключателя вкладок по оси X
function This:GetTabSwitchOffsetX()
	return self:getTabSwitchOffsetX();
end


-- Взятие стартового отступа кнопки переключателя вкладок по оси Y
function This:GetTabSwitchOffsetY()
	return self:getTabSwitchOffsetY();
end


-- Взятие зазора между кнопками переключателей вкладок
function This:GetTabSwitchHorGap()
	return self:getTabSwitchHorGap();
end


-- Взятие ширины заголовочной строки
function This:GetTitleWidth()
	return self:getTitleWidth();
end


-- Взятие высоты заголовочной строки
function This:GetTitleHeight()
	return self:getTitleHeight();
end


-- Взятие отступа заголовочной строки по оси X
function This:GetTitleOffsetX()
	return self:getTitleOffsetX();
end


-- Взятие отступа заголовочной строки по оси Y
function This:GetTitleOffsetY()
	return self:getTitleOffsetY();
end


-- Взятие ширины чекбокса
function This:GetChoiseWidth()
	return self:getChoiseWidth();
end


-- Взятие высоты чекбокса
function This:GetChoiseHeight()
	return self:getChoiseHeight();
end


-- Взятие отступа чекбокса по оси X
function This:GetChoiseOffsetX()
	return self:getChoiseOffsetX();
end


-- Взятие зазора между чекбоксами
function This:GetChoiseGap()
	return self:getChoiseGap();
end


-- Взятие высоты шрифта в таблице
function This:GetTableFontHeight()
	return self:getTableFontHeight();
end


-- Взятие зазора между диалоговыми окнами
function This:GetDlgGap()
	return self:getDlgGap();
end


-- Взятие ширины мелких панелей приборов
function This:GetDMDashboardWidth()
	return self:getDMDashboardWidth();
end


-- Взятие высоты мелких панелей приборов
function This:GetDMDashboardHeight()
	return self:getDMDashboardHeight();
end


-- Взятие ширины крупных панелей приборов
function This:GetSCDashboardWidth()
   return self:getSCDashboardWidth();
end


-- Взятие высоты крупных панелей приборов
function This:GetSCDashboardHeight()
   return self:getSCDashboardHeight();
end


-- Взятие стороны значка для мелких панелей приборов
function This:GetDashlightSide()
	return self:getDashlightSide();
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------