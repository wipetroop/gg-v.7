local This = GGF.ModuleGetWrapper("GUI-7");

-------------------------------------------------------------------------------
-- "checker" block
-------------------------------------------------------------------------------


-- п.1.0.0.1 (FNC Check)
function This:MS_CHECK_FNC()
	return 1;
end


-------------------------------------------------------------------------------
-- "setter" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "getter" block
-------------------------------------------------------------------------------


-- api
-- Взятие ширины фрейма main_window
function This:getMWFrameWidth()
	return self:getConfig().mwFrameWidth;
end


-- api
-- Взятие высоты фрейма main_window
function This:getMWFrameHeight()
	return self:getConfig().mwFrameHeight;
end


-- api
-- Взятие высоты фрейма alert_Dispatcher
function This:getADFrameHeight()
	return self:getConfig().adFrameHeight;
end

-- api
-- Взятие высоты фрейма alert_dispatcher table_engine
function This:getADTEFrameHeight()
   return self:getConfig().adteFrameHeight;
end


-- api
-- Взятие высоты фрейма control_panel
function This:getCPFrameHeight()
	return self:getConfig().cpFrameHeight;
end


-- api
-- Взятие высоты фрейма control_panel common_Service
function This:getCPCSFrameHeight()
	return self:getConfig().cpcsFrameHeight;
end


-- api
-- Взятие высоты фрейма control_panel effect_manager
function This:getCPEMFrameHeight()
	return self:getConfig().cpemFrameHeight;
end


-- api
-- Взятие высоты фрейма control_panel effect_manager audio_control
function This:getCPEMACFrameHeight()
	return self:getConfig().cpemacFrameHeight;
end


-- api
-- Взятие высоты фрейма control_panel effect_manager video_control
function This:getCPEMVCFrameHeight()
	return self:getConfig().cpemvcFrameHeight;
end


-- api
-- Взятие высоты фрейма control_panel hud_geometry
function This:getCPHGFrameHeight()
	return self:getConfig().cphgFrameHeight;
end


-- api
-- Взятие высоты фрейма control_panel logging_manager
function This:getCPLMFrameHeight()
	return self:getConfig().cplmFrameHeight;
end


-- api
-- Взятие высоты фрейма data_Service
function This:getDSFrameHeight()
	return self:getConfig().dsFrameHeight;
end


-- api
-- Взятие ширины фрейма data_Service chat_report
function This:getDSCRFrameWidth()
	return self:getConfig().dscrFrameWidth;
end


-- api
-- Взятие высоты фрейма data_Service chat_report
function This:getDSCRFrameHeight()
	return self:getConfig().dscrFrameHeight;
end


-- api
-- Взятие ширины фрейма data_Service data_filter
function This:getDSDFFrameWidth()
	return self:getConfig().dsdfFrameWidth;
end


-- api
-- Взятие высоты фрейма data_Service data_filter
function This:getDSDFFrameHeight()
	return self:getConfig().dsdfFrameHeight;
end


-- api
-- Взятие высоты фрейма data_Service graphic_engine
function This:getDSGEFrameHeight()
	return self:getConfig().dsgeFrameHeight;
end


-- api
-- Взятие ширины фрейма data_Service log_filter
function This:getDSLFFrameWidth()
	return self:getConfig().dslfFrameWidth;
end


-- api
-- Взятие высоты фрейма data_Service log_filter
function This:getDSLFFrameHeight()
	return self:getConfig().dslfFrameHeight;
end


-- api
-- Взятие высоты фрейма data_Service table_engine
function This:getDSTEFrameHeight()
	return self:getConfig().dsteFrameHeight;
end


-- api
-- Взятие высоты фрейма information_panel
function This:getIPFrameHeight()
	return self:getConfig().ipFrameHeight;
end


-- api
-- Взятие ширины кнопки
function This:getButtonWidth()
	return self:getConfig().buttonWidth;
end


-- api
-- Взятие высоты кнопки
function This:getButtonHeight()
	return self:getConfig().buttonHeight;
end


-- api
-- Взятие ширины вертикального слайдера
function This:getVSliderWidth()
	return self:getConfig().vSliderWidth;
end


-- api
-- Взятие ширины оперативного окна
function This:getOperationWidth()
	return self:getConfig().operationWidth;
end


-- api
-- Взятие стартового отступа кнопки переключателя вкладок по оси X
function This:getTabSwitchOffsetX()
	return self:getConfig().tabSwitchOffsetX;
end


-- api
-- Взятие стартового отступа кнопки переключателя вкладок по оси Y
function This:getTabSwitchOffsetY()
	return self:getConfig().tabSwitchOffsetY;
end


-- api
-- Взятие зазора между кнопками переключателей вкладок
function This:getTabSwitchHorGap()
	return self:getConfig().tabSwitchHorGap;
end


-- api
-- Взятие ширины заголовочной строки
function This:getTitleWidth()
	return self:getConfig().titleWidth;
end


-- api
-- Взятие высоты заголовочной строки
function This:getTitleHeight()
	return self:getConfig().titleHeight;
end


-- api
-- Взятие отступа заголовочной строки по оси X
function This:getTitleOffsetX()
	return self:getConfig().titleOffsetX;
end


-- api
-- Взятие отступа заголовочной строки по оси Y
function This:getTitleOffsetY()
	return self:getConfig().titleOffsetY;
end


-- api
-- Взятие ширины чекбокса
function This:getChoiseWidth()
	return self:getConfig().choiseWidth;
end


-- api
-- Взятие высоты чекбокса
function This:getChoiseHeight()
	return self:getConfig().choiseHeight;
end


-- api
-- Взятие отступа чекбокса по оси X
function This:getChoiseOffsetX()
	return self:getConfig().choiseOffsetX;
end


-- api
-- Взятие зазора между чекбоксами
function This:getChoiseGap()
	return self:getConfig().choiseGap;
end


-- api
-- Взятие высоты шрифта в таблице
function This:getTableFontHeight()
	return self:getConfig().tableFontHeight;
end


-- api
-- Взятие зазора между диалоговыми окнами
function This:getDlgGap()
	return self:getConfig().dlgGap;
end


-- api
-- Взятие ширины мелких панелей приборов
function This:getDMDashboardWidth()
	return self:getConfig().dmDashboardWidth;
end


-- api
-- Взятие высоты мелких панелей приборов
function This:getDMDashboardHeight()
	return self:getConfig().dmDashboardHeight;
end


-- api
-- Взятие ширины крупных панелей приборов
function This:getSCDashboardWidth()
	return self:getConfig().scDashboardWidth;
end


-- api
-- Взятие высоты крупных панелей приборов
function This:getSCDashboardHeight()
	return self:getConfig().scDashboardHeight;
end


-- api
-- Взятие стороны значка для мелких панелей приборов
function This:getDMDashlightSide()
	return self:getConfig().dashlightSide;
end


-------------------------------------------------------------------------------
-- "runtime" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "callback" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "Service" block
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- "unit-test" block
-------------------------------------------------------------------------------