local This = GGF.ModuleCreateWrapper("GUI-7", false, {
	-- Main Window
	mwFrameWidth = 1069,
	mwFrameHeight = 40,

	-- Alert Dispatcher
	--adFrameWidth = ,-- совпадает с mwFrameWidth
	adFrameHeight = 40,--406,

	-- Alert Dispatcher
		-- Table Engine
	adteFrameHeight = 75,	-- NOTE: ниже, т.к. таблица докается снизу

	-- Control Panel
	--cpFrameWidth = ,-- совпадает с mwFrameWidth
	cpFrameHeight = 40,		--75,-- не спрашивать, почему такой!!!,

	-- Control Panel
		-- Common Settings
	cpcsFrameHeight = 430,

	-- Control Panel
		-- Effect Manager
	cpemFrameHeight = 40,	--75,

	-- Control Panel
		-- Effect Manager
			-- Audio Controller
	cpemacFrameHeight = 330,

	-- Control Panel
		-- Effect Manager
			-- Video Controller
	cpemvcFrameHeight = 450,

	-- Control Panel
		-- Hud Geometry
	cphgFrameHeight = 200,

	-- Control Panel
		-- Logging Manager
	cplmFrameHeight = 550,

	-- Data Service
	--dsFrameWidth = ,-- совпадает с mwFrameWidth
	dsFrameHeight = 40,		--75,

	-- Data Service
		-- Chat Report
	dscrFrameWidth = 350,	--
	dscrFrameHeight = 300,

	-- Data Service
		-- Data Filter
	dsdfFrameWidth = 550,	-- NOTE: расширяем, меню не влезает...
	dsdfFrameHeight = 300,

	-- Data Service
		-- Graphic Engine
	dsgeFrameHeight = 550,

	-- Data Service
		-- Log Filter
	dslfFrameWidth = 350,	--
	dslfFrameHeight = 300,

	-- Data Service
		-- Table Engine
	dsteFrameHeight = 75,	-- NOTE: ниже, т.к. таблица докается снизу

	-- Information Panel
	--ipFrameWidth = ,-- совпадает с mwFrameWidth
	ipFrameHeight = 406,

	-- Button
	buttonWidth = 122,
	buttonHeight = 29,

	-- Vertical Slider
	vSliderWidth = 20,

	-- Operation Frame
	operationWidth = 274,	-- 212

	-- Table Switcher
	-- NOTE: они вроде как отключены в самом фреймворке
	tabSwitchOffsetX = 16,
	tabSwitchOffsetY = 0,
	tabSwitchHorGap = -15,

	-- Title Font String
	titleWidth = 130,
	titleHeight = 20,
	titleOffsetX = 0,
	titleOffsetY = -10,

	-- Choise Check Button
	choiseWidth = 20,
	choiseHeight = 20,
	choiseOffsetX = 20,
	choiseGap = 10,

	-- Table
	tableFontHeight = 14,

	-- Dialog(modal)
	dlgGap = 20,

	-- Drivecontrol/Maintenance Dashboard
	-- WARNING: не трогать!!! подгон!!!
	dmDashboardWidth = 510,--504,
	dmDashboardHeight = 120,
   scDashboardWidth = 430,
   scDashboardHeight = 120,
	dashlightSide = 42,--40,
});

-------------------------------------------------------------------------------
-- Maintenance information
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant strings
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Constant structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable numerics
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable flags
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable strings
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Variable structs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------