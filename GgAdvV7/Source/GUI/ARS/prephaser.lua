----------------------------------------------------------------------------
-- "functional" block
----------------------------------------------------------------------------


----------------------------------------------------------------------------
-- "define" block
----------------------------------------------------------------------------

GGM.GUI = {};
GGD.GUI = {};
GGD.GUI.ClusterName = "Graphical User Inteface";
GGD.GUI.ClusterToken = "GUI";
GGD.GUI.ClusterVersion = GGD.BuildMeta.Version;
GGD.GUI.ClusterBuild = "AT-"..GGD.BuildMeta.Build;
GGD.GUI.ClusterMBI = "7102-4052";

GGD.GUI.TexturePath = "Interface\\AddOns\\" .. GGD.AddonName .. "\\Textures\\GUI\\";
GGD.GUI.SoundPath = "Interface\\AddOns\\" .. GGD.AddonName .. "\\Sounds\\GUI\\";