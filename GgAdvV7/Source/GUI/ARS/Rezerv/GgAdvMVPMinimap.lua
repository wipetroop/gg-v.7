-------------------------------------------------------------------------------
-- Ui functions
-------------------------------------------------------------------------------
-- Error Loading push text. --
    
function out(text)
   Default_CHAT_FRAME:AddMessage(text)
   UiErrorsFrame:AddMessage(text, 1.0, 1.0, 0, 1, 10) 
end

-- Commands for slash, and Onload.  Calls by frame number i.e. /drshow 1 = "Drshow1" --
function Drshow_OnLoad()
   out("Drshow: OnLoad");
   SLASH_DRSHOW1 = "/drshow";
   SLASH_DRSHOW2 = "/askdoc";
   SlashCmdList["DRSHOW"] = function(msg)
      Drshow_SlashCommandHandler(msg);
   end
end

function Drshow_OnClick(arg1)
   id = this:GetID()
   out("DRSHOW: OnClick: " .. this:GetName() .. " ,ID: " .. id .. " ,Button:" ..arg1)
end

function Drshow_SlashCommandHandler(msg)
    out("Drshow: " .. msg)
	if (msg == "0") then
	 ReloadUi();
	end
 	Drshow_Toggle(msg);
end

function Drshow_Toggle(num)
	local frame = getglobal("Drshow" .. num)
	if (frame) then
	   	if(  frame:IsVisible() ) then
      		frame:Hide();
  	 	else
	      	frame:Show();
	   	end
	else
		print("missing frame "..num)
   	end
end


-- add this to your SavedVariables or as a separate SavedVariable to store its position
Drshow_Settings = {
	MinimapPos = 45 -- Default position of the minimap icon in degrees
}

-- Call this in a mod's initialization to move the minimap button to its saved position (also used in its movement)
-- ** do not call from the mod's OnLoad, VARIABLES_LOADED or later is fine. **
function Drshow_MinimapButton_Reposition()
	Drshow_MinimapButton:SetPoint("TopLeft","Minimap","TopLeft",52-(80*cos(Drshow_Settings.MinimapPos)),(80*sin(Drshow_Settings.MinimapPos))-52)
end

-- Only while the button is dragged this is called every frame
function Drshow_MinimapButton_DraggingFrame_OnUpDate()

	local xpos,ypos = GetCursorPosition()
	local xmin,ymin = Minimap:GetLeft(), Minimap:GetBottom()

	xpos = xmin-xpos/UiParent:GetScale()+70 -- get coordinates as differences from the center of the minimap
	ypos = ypos/UiParent:GetScale()-ymin-70

	Drshow_Settings.MinimapPos = math.deg(math.atan2(ypos,xpos)) -- save the degrees we are relative to the minimap center
	Drshow_MinimapButton_Reposition() -- move the button
end

-- Put your code that you want on a minimap button click here.  arg1="LeftButton", "RightButton", etc
function Drshow_MinimapButton_OnClick()
	Default_CHAT_FRAME:AddMessage(tostring(arg1).." is engaged.")
    --Drshow1:Show()	-- уже прописал в хмл
    

end