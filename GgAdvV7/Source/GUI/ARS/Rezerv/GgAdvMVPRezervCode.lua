-- spells
	if GUi_DS_Ui.selectedLv3 == GUi_DS_Ui.lv3types[1] then
		-- full
		if GUi_DS_Ui.selectedLv4 == GUi_DS_Ui.lv4types[1] then
			dataSector = GUi_DS_Ui.selectedLogClusterData.dbgHookMasterTableSpellDaMageUniversal.data["f"]
			--return;
		--singleton			
		else
			dataSector = GUi_DS_Ui.selectedLogClusterData.dbgHookMasterTableSpellDaMageUniversal.data["s"]
			--return;
		end


		expColCount = GUi_DS_Ui.headerMap.spellDaMage.meta.columns;

		GUi_DS_Ui.defineLabelMap(#dataSector, "spell");

		-- Индусский код
		local fltr_f, fltr_s = "defined", "Undefined";

		if GUi_DS_Ui.selectedLv2 == "Defined" then
			fltr_s = "";
		elseif GUi_DS_Ui.selectedLv2 == "Undefined" then
			fltr_f = "";
		elseif GUi_DS_Ui.selectedLv2 == "All" then
			-- а тут ничего....просто иф на всякий случай
		end

		local dataSectorShard = {};

		local dsLength = #dataSector;
		for ind=1,1000 do
			if ind <= dsLength then
				dataSectorShard[ind] = dataSector[ind];
			end
		end

		GUi_DumpService.scrollbar:SetMinMaxValues(1, 2);
		GUi_DumpService.scrollbar:SetValueStep(#dataSectorShard/200); 
		print(#dataSectorShard/200);
		
		for k,v in pairs(dataSectorShard) do
			
			if v.spellDefined.data == fltr_f or v.spellDefined.data == fltr_s then
				-- Привязка по полям все-таки надежнее
				--print("ln 709")											
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.data.logfield[k][1], k, "#standart#");
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.data.logfield[k][2], v.time.data, v.time.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.data.logfield[k][3], v.sourceName.data, v.sourceName.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.data.logfield[k][4], v.targetName.data, v.targetName.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.data.logfield[k][5], v.spellDefined.data, v.spellDefined.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.data.logfield[k][6], v.spellName.data, v.spellName.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.data.logfield[k][7], v.spellID.data, v.spellID.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.data.logfield[k][8], v.spellDaMage.data, v.spellDaMage.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.data.logfield[k][9], v.isCritical.data, v.isCritical.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.data.logfield[k][10], v.isMulticast.data, v.isMulticast.color);
			end
		end
	--end

	-- misses
	elseif GUi_DS_Ui.selectedLv3 == GUi_DS_Ui.lv3types[2] then
		-- full
		if GUi_DS_Ui.selectedLv4 == GUi_DS_Ui.lv4types[1] then
			dataSector = GUi_DS_Ui.selectedLogClusterData.dbgHookMasterTableSpellMissUniversal.data["f"]
			--return;
		-- singleton			
		else
			dataSector = GUi_DS_Ui.selectedLogClusterData.dbgHookMasterTableSpellMissUniversal.data["s"]
			--return;
		end
		expColCount = GUi_DS_Ui.headerMap.spellMiss.meta.columns;

		GUi_DS_Ui.defineLabelMap(#dataSector, "miss");

		-- Индусский код
		local fltr_f, fltr_s = "Defined", "Undefined";

		if GUi_DS_Ui.selectedLv2 == "Defined" then
			fltr_s = "";
		elseif GUi_DS_Ui.selectedLv2 == "Undefined" then
			fltr_f = "";
		elseif GUi_DS_Ui.selectedLv2 == "All" then
			-- а тут ничего....просто иф на всякий случай
		end

		for k,v in pairs(dataSector) do
			if v.spellDefined.data == fltr_f or v.spellDefined.data == fltr_s then
				-- Привязка по полям все-таки надежнее
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][1], k, "#standart#");
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][2], v.time.data, v.time.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][3], v.sourceName.data, v.sourceName.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][4], v.targetName.data, v.targetName.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][5], v.evSubType.data, v.evSubType.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][6], v.spellDefined.data, v.spellDefined.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][7], v.spellName.data, v.spellName.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][8], v.spellID.data, v.spellID.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][9], v.spellDaMage.data, v.spellDaMage.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][10], v.isCritical.data, v.isCritical.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][11], v.isMulticast.data, v.isMulticast.color);
			end
		end
	--end

	-- kills
	elseif GUi_DS_Ui.selectedLv3 == GUi_DS_Ui.lv3types[3] then
		-- full
		if GUi_DS_Ui.selectedLv4 == GUi_DS_Ui.lv4types[1] then
			dataSector = GUi_DS_Ui.selectedLogClusterData.dbgHookMasterTableSpellKillUniversal.data["f"]
			--return;
		-- singleton			
		else
			dataSector = GUi_DS_Ui.selectedLogClusterData.dbgHookMasterTableSpellKillUniversal.data["s"]
			--return;
		end
		expColCount = GUi_DS_Ui.headerMap.spellKill.meta.columns;
		--print("ln 764", dataSector);
		GUi_DS_Ui.reallocateLabelMap(#dataSector, "kill");

		-- Индусский код
		local fltr_f, fltr_s = "Defined", "Undefined";

		if GUi_DS_Ui.selectedLv2 == "Defined" then
			fltr_s = "";
		elseif GUi_DS_Ui.selectedLv2 == "Undefined" then
			fltr_f = "";
		elseif GUi_DS_Ui.selectedLv2 == "All" then
			-- а тут ничего....просто иф на всякий случай
		end

		for k,v in pairs(dataSector) do
			if v.spellDefined.data == fltr_f or v.spellDefined.data == fltr_s then
				-- Привязка по полям все-таки надежнее
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][1], k, "#standart#");
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][2], v.time.data, v.time.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][3], v.sourceName.data, v.sourceName.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][4], v.targetName.data, v.targetName.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][5], v.spellDefined.data, v.spellDefined.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][6], v.spellName.data, v.spellName.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][7], v.spellID.data, v.spellID.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][8], v.spellDaMage.data, v.spellDaMage.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][9], v.isCritical.data, v.isCritical.color);
				GUi_DS_Ui.setColoRedText(GUi_DS_Ui.labelMap.logfield[k][10], v.isMulticast.data, v.isMulticast.color);
			end
		end
	else 
		-- ассерт бы не помешал
		return
	end 