----------------------------------------------------------------------------
-- "functional" block
----------------------------------------------------------------------------

-- Создание дерева однотипных шаблонов
function GGF.GTemplateTree(in_baseClass, in_items)
	ATLASSERT(in_baseClass);
	ATLASSERT(in_items);
	for token, object in pairs(in_items) do
		if (token ~= "inherit") then
			--print("child:", token);
			GGF.GTemplateTree(in_baseClass, object);
			-- WARNING: не ставить суффикс перед циклом - его закусывает как чайлд...
			object.suffix = in_baseClass:GetClassSuffix();
		end
	end
	return in_items;
end