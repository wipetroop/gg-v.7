-- Вроде как отключено да и не работает...хотя хз....
-- creating test data structure
local Test1_Data = {
	["level1_test_1"] = {
		[1] = { ["name"] = "sublevel 1"; },
		[2] = {	["name"] = "sublevel 2"; },
	},
	["level1_test_2"] = {
		[1] = {	["name"] = "sublevel A"; },
		[2] = {	["name"] = "sublevel B"; },
	}
}



 -- menu create function
 function Test1_DropDown_Initialize(self,level)
   level = level or 1;
   if (level == 1) then
     for key, subarray in pairs(Test1_Data) do
       local info = UiGGC.DropdownMenu_CreateInfo();
       info.hasArrow = true; -- creates submenu
       info.notCheckable = true;
       info.text = key;
       info.value = {
         ["Level1_Key"] = key;
       };
       UiGGC.DropdownMenu_AddButton(info, level);
     end -- for key, subarray
   end -- if level 1

   if (level == 2) then
     -- getting values of first menu
     local Level1_Key = UiGGC.DropdownMenu_MENU_VALUE["Level1_Key"];
     subarray = Test1_Data[Level1_Key];
     for key, subsubarray in pairs(subarray) do
       local info = UiGGC.DropdownMenu_CreateInfo();
       info.hasArrow = false; -- no submenues this time
       info.notCheckable = true;
       info.text = subsubarray["name"];
       -- use info.func to set a function to be called at "click"
       info.value = {
         ["Level1_Key"] = Level1_Key;
         ["Sublevel_Key"] = key;
       };
       UiGGC.DropdownMenu_AddButton(info, level);
     end -- for key,subsubarray
   end -- if level 2
 end -- function Test1_DropDown_Initialize



 ----------------------------------------------------------------------------------------
-- Обработчик списка боев в уях(старый)
function GUi_EXTERN_Ui_MnBattleLogDropdown_OnLoad()

  local taglist = _G["ACR_GetAvailableLogList"]()
  local isEmpty = true
  --print(taglist[1])
  for ind,val in pairs(taglist) do

      --local info            = {};
      --info.text       = val;
      --info.value      = val;
      --info.func       = function() commitBattleLogChoose(val) end;
    -- can also be done as function() FunctionCalledWhenOptionIsClicked() end;
     
      -- Add the above information to the options menu as a button.
      isEmpty = false;


      local info = UiGGC.DropdownMenu_CreateInfo();
    info.text = val;--LBR[race] or race;
    info.func = function() GUi_DS.setLogCluster(val) end;
    info.checked = GUi_DS.selectedLogClusterTitle == val;
    --info.arg1 = "displayRace";
    --info.arg2 = raceID[race];
    info.value = val;
    UiGGC.DropdownMenu_AddButton(info, 1);
    --dropdown:AddButton(info, level);
  end

  if isEmpty then
    local info = UiGGC.DropdownMenu_CreateInfo();
    info.text = "Log Cluster Is Empty";--LBR[race] or race;
    info.func = function() end;
    --info.checked = selectedLogCluster == val;
    --info.arg1 = "displayRace";
    --info.arg2 = raceID[race];
    info.value = nil;
    info.disabled = true;
    UiGGC.DropdownMenu_AddButton(info, 1);
  end
    --print("Mn onload")
end


-- Обработчик динамического фильтра в уях(старый)
function GUi_EXTERN_Ui_MnGGM.GUI.DataFilterDropdown_OnLoad()

  --local info = UiGGC.DropdownMenu_CreateInfo(); 

  local info = UiGGC.DropdownMenu_CreateInfo();
  info.text = "UiDDM_DFC3";--LBR[race] or race;
  info.func = function() end;
  info.isTitle = true;
  info.notCheckable = true; 
  info.value = nil; 
  UiGGC.DropdownMenu_AddButton(info, 1);

  for ind, val in pairs(GUi_DS.lv3types) do    
    local info = UiGGC.DropdownMenu_CreateInfo();
    info.text = val;
    info.func = function() GUi_DS.setLv3(val)  end;
    info.checked = GUi_DS.selectedLv3 == val;
    info.value = val;
    UiGGC.DropdownMenu_AddButton(info, 1);
  end   


  if selectedLv3 ~= "" then
    --local lv2types = {"Defined","Undefined","All"}

    local info = UiGGC.DropdownMenu_CreateInfo();
    info.isTitle = true;
    info.notCheckable = true; 
    UiGGC.DropdownMenu_AddButton(info, 1);

    wipe(info)    
    info = UiGGC.DropdownMenu_CreateInfo();
    info.text = "UiDDM_DFC2";--LBR[race] or race;
    info.func = function() end;
    info.isTitle = true;
    info.notCheckable = true; 
    info.value = nil; 
    UiGGC.DropdownMenu_AddButton(info, 1);

    for ind, val in pairs(GUi_DS.lv2types) do
          
      local info = UiGGC.DropdownMenu_CreateInfo();
      info.text = val;
      info.func = function() GUi_DS.setLv2(val)  end;
      info.checked = GUi_DS.selectedLv2 == val
      info.value = val;
      UiGGC.DropdownMenu_AddButton(info, 1);
    end 
    
  end

  if GUi_DS.selectedLv3 == "Miss" and GUi_DS.selectedLv2 ~= "" then
    --local lv5types = {"All","Absorb","Block","Deflect","Dodge","Evasion","Immune","Miss","Parry","Reflect","Resist"}

    local info = UiGGC.DropdownMenu_CreateInfo();
    info.isTitle = true;
    info.notCheckable = true; 
    UiGGC.DropdownMenu_AddButton(info, 1);

    info = nil    
    info = UiGGC.DropdownMenu_CreateInfo();
    info.text = "UiDDM_DFC5";--LBR[race] or race;
    info.func = function() end;
    info.isTitle = true;
    info.notCheckable = true; 
    info.value = nil; 
    UiGGC.DropdownMenu_AddButton(info, 1);

    for ind, val in pairs(GUi_DS.lv5types) do
      local info = UiGGC.DropdownMenu_CreateInfo();
      info.text = val;
      info.func = function() GUi_DS.setLv5(val)  end;
      info.checked = GUi_DS.selectedLv5 == val;
      info.value = val;
      UiGGC.DropdownMenu_AddButton(info, 1);
    end 
  end

  if ((GUi_DS.selectedLv2 ~= "") and (GUi_DS.selectedLv3 ~= "Miss")) or ((GUi_DS.selectedLv3 == "Miss") and (GUi_DS.selectedLv5 ~= "")) then
    --local lv4types = {"Full","Singleton"}

    local info = UiGGC.DropdownMenu_CreateInfo();
    info.isTitle = true;
    info.notCheckable = true; 
    UiGGC.DropdownMenu_AddButton(info, 1);

    info = nil    
    info = UiGGC.DropdownMenu_CreateInfo();
    info.text = "UiDDM_DFC4";--LBR[race] or race;
    info.func = function() end;
    info.isTitle = true;
    info.notCheckable = true; 
    info.value = nil; 
    UiGGC.DropdownMenu_AddButton(info, 1);

    for ind, val in pairs(GUi_DS.lv4types) do      
      local info = UiGGC.DropdownMenu_CreateInfo();
      info.text = val;
      info.func = function() GUi_DS.setLv4(val)  end;
      info.checked = GUi_DS.selectedLv4 == val;
      info.value = val;
      UiGGC.DropdownMenu_AddButton(info, 1);
    end
    
  end


end

----------------------------------------------------------------------------------------


-- Установка выбранного лога в стазис(из него берутся данные для отображения)
-- Вроде бы отключен...
function GUi_DS.setLogCluster(Title)
  GUi_DS.selectedLogClusterTitle = Title;
  GUi_DS.selectedLogClusterData = _G["ACR_GetChoosenLogClusterByMark"](Title);
  GUi_DS.checkUniqueness();
  
end


-- Странная функция
-- Загрузка листа актуальных лог бд
function GUi_DS.loadActualLogList()
  local taglist = _G["ACR_GetAvailableLogList"]()
end








-- Уровни не по порядку....и дело в том, что это абсолютно нормально

-- Установка фильтра(ур.1)
function GUi_DS.setLv1(data)
  GUi_DS.masterDrop(1);
  GUi_DS.selectedLv1 = data;
  UiGGC.DropdownMenu_Initialize(GUi_MnGGM.GUI.DataFilterDropdown, GUi_MnGGM.GUI.DataFilterDropdown_OnLoad, "MENU");
  ToggleGGC.DropdownMenu(1, nil, GUi_MnGGM.GUI.DataFilterDropdown, GUi_BtnGGM.GUI.DataFilterDropdown, 0, 0);  
end


-- Установка фильтра(ур.2)
function GUi_DS.setLv2(data)
  GUi_DS.masterDrop(2);
  GUi_DS.selectedLv2 = data;
  UiGGC.DropdownMenu_Initialize(GUi_MnGGM.GUI.DataFilterDropdown, GUi_MnGGM.GUI.DataFilterDropdown_OnLoad, "MENU");
  ToggleGGC.DropdownMenu(1, nil, GUi_MnGGM.GUI.DataFilterDropdown, GUi_BtnGGM.GUI.DataFilterDropdown, 0, 0);  
end


-- Установка фильтра(ур.3)
function GUi_DS.setLv3(data)
  GUi_DS.masterDrop(3);
  GUi_DS.selectedLv3 = data;
  if GUi_DS.selectedLv2 == "Kill" then
    GUi_DS.isGGM.GUI.DataFiltered = true;
    GUi_DS.checkUniqueness();
  end
  UiGGC.DropdownMenu_Initialize(GUi_MnGGM.GUI.DataFilterDropdown, GUi_MnGGM.GUI.DataFilterDropdown_OnLoad, "MENU");
  ToggleGGC.DropdownMenu(1, nil, GUi_MnGGM.GUI.DataFilterDropdown, GUi_BtnGGM.GUI.DataFilterDropdown, 0, 0);  
end



-- Установка фильтра(ур.5)
function GUi_DS.setLv5(data)
  GUi_DS.masterDrop(5);
  GUi_DS.selectedLv5 = data;
  UiGGC.DropdownMenu_Initialize(GUi_MnGGM.GUI.DataFilterDropdown, GUi_MnGGM.GUI.DataFilterDropdown_OnLoad, "MENU");
  ToggleGGC.DropdownMenu(1, nil, GUi_MnGGM.GUI.DataFilterDropdown, GUi_BtnGGM.GUI.DataFilterDropdown, 0, 0);  
end

-- Установка фильтра(ур.4)
function GUi_DS.setLv4(data)
  -- этот уровень последний
  --masterDrop(4);
  GUi_DS.selectedLv4 = data;
  GUi_DS.isGGM.GUI.DataFiltered = true;
  GUi_DS.checkUniqueness()
  UiGGC.DropdownMenu_Initialize(GUi_MnGGM.GUI.DataFilterDropdown, GUi_MnGGM.GUI.DataFilterDropdown_OnLoad, "MENU");
  ToggleGGC.DropdownMenu(1, nil, GUi_MnGGM.GUI.DataFilterDropdown, GUi_BtnGGM.GUI.DataFilterDropdown, 0, 0);  
end







GUi_DT.savedMap = {
  length = 1,
  data = {"", "", "", "", "", ""},
  level = {"Lv0", "", "", "", "", ""}
};

GUi_DT.currentColumn = 0;




GUi_DT.counterBNMapper = {
  ["Lv0"] = 2,
  ["Lv1"] = 9,
  ["Lv2"] = 3,
  ["Lv3"] = 3,
  ["Lv4"] = 2,
  ["Lv5"] = 11
};

GUi_DT.reverseBNMapper = {
  "Lv0",
  "Lv1",
  "Lv2",
  "Lv3",
  "Lv4",
  "Lv5"
};











GUi_DT.transceiverGeometry = {
  numeric = {
    ablsoluteAnchorOffset = {
      xs = 20,
      ys = -20,
      dy = 30,  -- 30 - впритык
      dx = 130, -- 120 - впритык
      dc = 165, -- (370-2*20)/2 вроде бы(короч, высота вкладки отчетов минус отступы на 2)(но это не точно)
    },
  },
};

GUi_DT.dumpFunctionality = {
  -- Временный хардкод
  -- Фишка такая : достать кнопку через _G[] не получается, хотя имена там подходящие...
  -- Плюс ко всему, массив объектов создаваться не хочет...
  buttonNameMap = {
    ["Lv5"] = {
      ["Mall"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv5Mall:Hide() end,
        ["Show"] = function() GUi_BtnLv5Mall:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv5Mall:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Mall"].IsHighlighted == true) then return end GUi_BtnLv5Mall:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Mall"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Mall"].IsHighlighted == false) then return end GUi_BtnLv5Mall:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Mall"].IsHighlighted = false end
      },
      ["Absorb"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv5Absorb:Hide() end,
        ["Show"] = function() GUi_BtnLv5Absorb:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv5Absorb:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Absorb"].IsHighlighted == true) then return end GUi_BtnLv5Absorb:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Absorb"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Absorb"].IsHighlighted == false) then return end GUi_BtnLv5Absorb:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Absorb"].IsHighlighted = false end
      },
      ["Block"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv5Block:Hide() end,
        ["Show"] = function() GUi_BtnLv5Block:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv5Block:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Block"].IsHighlighted == true) then return end GUi_BtnLv5Block:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Block"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Block"].IsHighlighted == false) then return end GUi_BtnLv5Block:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Block"].IsHighlighted = false end 
      },
      ["Deflect"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv5Deflect:Hide() end,
        ["Show"] = function() GUi_BtnLv5Deflect:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv5Deflect:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Deflect"].IsHighlighted == true) then return end GUi_BtnLv5Deflect:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Deflect"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Deflect"].IsHighlighted == false) then return end GUi_BtnLv5Deflect:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Deflect"].IsHighlighted = false end 
      }, 
      ["Dodge"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv5Dodge:Hide() end,
        ["Show"] = function() GUi_BtnLv5Dodge:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv5Dodge:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Dodge"].IsHighlighted == true) then return end GUi_BtnLv5Dodge:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Dodge"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Dodge"].IsHighlighted == false) then return end GUi_BtnLv5Dodge:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Dodge"].IsHighlighted = false end
      },
      ["Evasion"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv5Evasion:Hide() end,
        ["Show"] = function() GUi_BtnLv5Evasion:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv5Evasion:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Evasion"].IsHighlighted == true) then return end GUi_BtnLv5Evasion:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Evasion"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Evasion"].IsHighlighted == false) then return end GUi_BtnLv5Evasion:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Evasion"].IsHighlighted = false end
      },
      ["Immune"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv5Immune:Hide() end,
        ["Show"] = function() GUi_BtnLv5Immune:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv5Immune:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Immune"].IsHighlighted == true) then return end GUi_BtnLv5Immune:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Immune"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Immune"].IsHighlighted == false) then return end GUi_BtnLv5Immune:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Immune"].IsHighlighted = false end
      },
      ["Miss"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv5Miss:Hide() end,
        ["Show"] = function() GUi_BtnLv5Miss:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv5Miss:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Miss"].IsHighlighted == true) then return end GUi_BtnLv5Miss:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Miss"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Miss"].IsHighlighted == false) then return end GUi_BtnLv5Miss:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Miss"].IsHighlighted = false end
      },
      ["Parry"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv5Parry:Hide() end,
        ["Show"] = function() GUi_BtnLv5Parry:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv5Parry:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Parry"].IsHighlighted == true) then return end GUi_BtnLv5Parry:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Parry"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Parry"].IsHighlighted == false) then return end GUi_BtnLv5Parry:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Parry"].IsHighlighted = false end
      },
      ["Reflect"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv5Reflect:Hide() end,
        ["Show"] = function() GUi_BtnLv5Reflect:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv5Reflect:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Reflect"].IsHighlighted == true) then return end GUi_BtnLv5Reflect:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Reflect"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Reflect"].IsHighlighted == false) then return end GUi_BtnLv5Reflect:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Reflect"].IsHighlighted = false end
      },
      ["Resist"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv5Resist:Hide() end,
        ["Show"] = function() GUi_BtnLv5Resist:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv5Resist:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Resist"].IsHighlighted == true) then return end GUi_BtnLv5Resist:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Resist"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Resist"].IsHighlighted == false) then return end GUi_BtnLv5Resist:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv5"]["Resist"].IsHighlighted = false end
      }
    },
    ["Lv4"] = {
      ["Full"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv4Full:Hide() end,
        ["Show"] = function() GUi_BtnLv4Full:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv4Full:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv4"]["Full"].IsHighlighted == true) then return end GUi_BtnLv4Full:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv4"]["Full"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv4"]["Full"].IsHighlighted == false) then return end GUi_BtnLv4Full:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv4"]["Full"].IsHighlighted = false end
      },
      ["Singleton"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv4Singleton:Hide() end,
        ["Show"] = function() GUi_BtnLv4Singleton:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv4Singleton:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv4"]["Singleton"].IsHighlighted == true) then return end GUi_BtnLv4Singleton:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv4"]["Singleton"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv4"]["Singleton"].IsHighlighted == false) then return end GUi_BtnLv4Singleton:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv4"]["Singleton"].IsHighlighted = false end
      }
    },  
    ["Lv3"] = {
      ["Kill"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv3Kill:Hide() end,
        ["Show"] = function() GUi_BtnLv3Kill:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv3Kill:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv3"]["Kill"].IsHighlighted == true) then return end GUi_BtnLv3Kill:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv3"]["Kill"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv3"]["Kill"].IsHighlighted == false) then return end GUi_BtnLv3Kill:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv3"]["Kill"].IsHighlighted = false end
      },
      ["Spell"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv3Spell:Hide() end,
        ["Show"] = function() GUi_BtnLv3Spell:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv3Spell:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv3"]["Spell"].IsHighlighted == true) then return end GUi_BtnLv3Spell:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv3"]["Spell"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv3"]["Spell"].IsHighlighted == false) then return end GUi_BtnLv3Spell:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv3"]["Spell"].IsHighlighted = false end
      },
      ["Miss"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv3Miss:Hide() end,
        ["Show"] = function() GUi_BtnLv3Miss:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv3Miss:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv3"]["Miss"].IsHighlighted == true) then return end GUi_BtnLv3Miss:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv3"]["Miss"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv3"]["Miss"].IsHighlighted == false) then return end GUi_BtnLv3Miss:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv3"]["Miss"].IsHighlighted = false end
      }
    },
    ["Lv2"] = {
      ["Dall"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv2Dall:Hide() end,
        ["Show"] = function() GUi_BtnLv2Dall:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv2Dall:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv2"]["Dall"].IsHighlighted == true) then return end GUi_BtnLv2Dall:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv2"]["Dall"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv2"]["Dall"].IsHighlighted == false) then return end GUi_BtnLv2Dall:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv2"]["Dall"].IsHighlighted = false end
      },
      ["Defined"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv2Defined:Hide() end,
        ["Show"] = function() GUi_BtnLv2Defined:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv2Defined:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv2"]["Defined"].IsHighlighted == true) then return end GUi_BtnLv2Defined:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv2"]["Defined"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv2"]["Defined"].IsHighlighted == false) then return end GUi_BtnLv2Defined:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv2"]["Defined"].IsHighlighted = false end
      },
      ["Undefined"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv2Undefined:Hide() end,
        ["Show"] = function() GUi_BtnLv2Undefined:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv2Undefined:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv2"]["Undefined"].IsHighlighted == true) then return end GUi_BtnLv2Undefined:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv2"]["Undefined"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv2"]["Undefined"].IsHighlighted == false) then return end GUi_BtnLv2Undefined:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv2"]["Undefined"].IsHighlighted = false end
      }
    },
    ["Lv1"] = {
      ["Console"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv1Console:Hide() end,
        ["Show"] = function() GUi_BtnLv1Console:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv1Console:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Console"].IsHighlighted == true) then return end GUi_BtnLv1Console:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Console"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Console"].IsHighlighted == false) then return end GUi_BtnLv1Console:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Console"].IsHighlighted = false end
      },
      ["GUild"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv1GUild:Hide() end,
        ["Show"] = function() GUi_BtnLv1GUild:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv1GUild:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["GUild"].IsHighlighted == true) then return end GUi_BtnLv1GUild:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["GUild"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["GUild"].IsHighlighted == false) then return end GUi_BtnLv1GUild:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["GUild"].IsHighlighted = false end
      },
      ["Instance"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv1Instance:Hide() end,
        ["Show"] = function() GUi_BtnLv1Instance:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv1Instance:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Instance"].IsHighlighted == true) then return end GUi_BtnLv1Instance:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Instance"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Instance"].IsHighlighted == false) then return end GUi_BtnLv1Instance:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Instance"].IsHighlighted = false end
      },
      ["Party"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv1Party:Hide() end,
        ["Show"] = function() GUi_BtnLv1Party:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv1Party:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Party"].IsHighlighted == true) then return end GUi_BtnLv1Party:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Party"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Party"].IsHighlighted == false) then return end GUi_BtnLv1Party:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Party"].IsHighlighted = false end
      },
      ["Raid"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv1Raid:Hide() end,
        ["Show"] = function() GUi_BtnLv1Raid:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv1Raid:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Raid"].IsHighlighted == true) then return end GUi_BtnLv1Raid:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Raid"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Raid"].IsHighlighted == false) then return end GUi_BtnLv1Raid:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Raid"].IsHighlighted = false end
      },
      ["RaidWarning"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv1RaidWarning:Hide() end,
        ["Show"] = function() GUi_BtnLv1RaidWarning:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv1RaidWarning:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["RaidWarning"].IsHighlighted == true) then return end GUi_BtnLv1RaidWarning:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["RaidWarning"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["RaidWarning"].IsHighlighted == false) then return end GUi_BtnLv1RaidWarning:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["RaidWarning"].IsHighlighted = false end
      },
      ["Say"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv1Say:Hide() end,
        ["Show"] = function() GUi_BtnLv1Say:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv1Say:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Say"].IsHighlighted == true) then return end GUi_BtnLv1Say:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Say"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Say"].IsHighlighted == false) then return end GUi_BtnLv1Say:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Say"].IsHighlighted = false end
      },
      ["Whisper"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv1Whisper:Hide() end,
        ["Show"] = function() GUi_BtnLv1Whisper:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv1Whisper:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Whisper"].IsHighlighted == true) then return end GUi_BtnLv1Whisper:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Whisper"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Whisper"].IsHighlighted == false) then return end GUi_BtnLv1Whisper:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Whisper"].IsHighlighted = false end
      },
      ["Yell"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv1Yell:Hide() end,
        ["Show"] = function() GUi_BtnLv1Yell:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv1Yell:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Yell"].IsHighlighted == true) then return end GUi_BtnLv1Yell:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Yell"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Yell"].IsHighlighted == false) then return end GUi_BtnLv1Yell:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv1"]["Yell"].IsHighlighted = false end
      }
    },
    ["Lv0"] = {
      ["Chat"] = {    
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv0Chat:Hide() end,
        ["Show"] = function() GUi_BtnLv0Chat:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv0Chat:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv0"]["Chat"].IsHighlighted == true) then return end GUi_BtnLv0Chat:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv0"]["Chat"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv0"]["Chat"].IsHighlighted == false) then return end GUi_BtnLv0Chat:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv0"]["Chat"].IsHighlighted = false end
      },
      ["Service"] = {
        IsHighlighted = false,
        ["Hide"] = function() GUi_BtnLv0Service:Hide() end,
        ["Show"] = function() GUi_BtnLv0Service:Show() end,
        ["SetPos"] = function(x, y) GUi_BtnLv0Service:SetPoint("TopLeft", GUi_DumpTransceiver, "TopLeft", x, y) end,
        ["LockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv0"]["Service"].IsHighlighted == true) then return end GUi_BtnLv0Service:LockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv0"]["Service"].IsHighlighted = true end,
        ["UnlockHighlight"] = function() if (GUi_DT.dumpFunctionality.buttonNameMap["Lv0"]["Service"].IsHighlighted == false) then return end GUi_BtnLv0Service:UnlockHighlight() GUi_DT.dumpFunctionality.buttonNameMap["Lv0"]["Service"].IsHighlighted = false end
      }
    }
  } 
};




-- 
function setAllPositionsDebug()
  local i, j = 1, 1
  local lOffsetS = 0

  for k,v in pairs(GUi_DT.reverseBNMapper) do    
    lOffsetS = GUi_DT.transceiverGeometry.numeric.ablsoluteAnchorOffset.dc - ((GUi_DT.counterBNMapper[v]*(29 + 1) - 1)/2) -- лютый подгон по факту
    for m,n in pairs(GUi_DT.buttonNameMap[v]) do
      n["SetPos"](GUi_DT.transceiverGeometry.numeric.ablsoluteAnchorOffset.xs + (i - 1)*GUi_DT.transceiverGeometry.numeric.ablsoluteAnchorOffset.dx, - lOffsetS + GUi_DT.transceiverGeometry.numeric.ablsoluteAnchorOffset.ys - (j - 1)*GUi_DT.transceiverGeometry.numeric.ablsoluteAnchorOffset.dy)
      j = j + 1
    end
    i = i + 1
    j = 1
  end

end


--
function GUi_DT.GGDT_Init()
  for k,v in pairs(GUi_DT.reverseBNMapper) do    
    for m,n in pairs(GUi_DT.dumpFunctionality.buttonNameMap[v]) do
      if (m ~= "CurrentLevel") then
        if (n.IsHighlighted) then
          GUi_DT.savedMap.data[GUi_DT.savedMap.length + 1] = m
          GUi_DT.savedMap.level[GUi_DT.savedMap.length + 1] = v
          GUi_DT.savedMap.length = GUi_DT.savedMap.length + 1
        end
        n["UnlockHighlight"]();
        n["Hide"]();
      end
    end       
  end 
end





-- 
function GUi_DT.setButtonArray(column, level)
  --GUi_DT.buttonNameMap[level].CurrentLevel = column
  local lOffsetS = GUi_DT.transceiverGeometry.numeric.ablsoluteAnchorOffset.dc - ((GUi_DT.counterBNMapper[level]*(29 + 1) - 1)/2)
  local j = 1
  for k,v in pairs(GUi_DT.buttonNameMap[level]) do
    if (k ~= "CurrentLevel") then
      v["Show"]()
      v["SetPos"](GUi_DT.transceiverGeometry.numeric.ablsoluteAnchorOffset.xs + (column - 1)*GUi_DT.transceiverGeometry.numeric.ablsoluteAnchorOffset.dx, - lOffsetS + GUi_DT.transceiverGeometry.numeric.ablsoluteAnchorOffset.ys - (j - 1)*GUi_DT.transceiverGeometry.numeric.ablsoluteAnchorOffset.dy)
      j = j + 1
    end
  end
end






--
function GUi_DT.freeSubway()
  local singleNotHide = false
  GUi_DT.disengageDumpButton()

  for k = 1,GUi_DT.savedMap.length do
    
    --print(k)
    --print(GUi_DT.savedMap.level[k])
    for m,n in pairs(GUi_DT.buttonNameMap[GUi_DT.savedMap.level[k]]) do
      --if (m ~= "CurrentLevel") then
        n["UnlockHighlight"]();
        if (singleNotHide == true) then       
          n["Hide"]();
        end
      --end
    end
    singleNotHide = true
    

    
  end

  for k = 1,GUi_DT.savedMap.length do
    GUi_DT.savedMap.data[k] = ""
    GUi_DT.savedMap.level[k] = ""
  end
  GUi_DT.savedMap.level[1] = "Lv0"
  GUi_DT.savedMap.length = 1
end


-- Сброс маршрута с текущего уровня
function GUi_DT.dropSubway(level)
  local index, count = 1, 1
  local singleNotHide = false
  local tmpNewLen = GUi_DT.savedMap.length 
  GUi_DT.disengageDumpButton()
  GUi_DT.spikeTrasser("before")

  local dtmb = false
  for k = 1,GUi_DT.savedMap.length do
    if (dtmb == true) then
      print(k)
      print(GUi_DT.savedMap.level[k])
      for m,n in pairs(GUi_DT.buttonNameMap[GUi_DT.savedMap.level[k]]) do
        --if (m ~= "CurrentLevel") then
          n["UnlockHighlight"]();
          if (singleNotHide == true) then       
            n["Hide"]();
          end
        --end
      end
      singleNotHide = true
    end

    if (GUi_DT.savedMap.level[k] == level) then
      if (GUi_DT.savedMap.data[k] ~= "") then
        GUi_DT.buttonNameMap[level][GUi_DT.savedMap.data[k]]["UnlockHighlight"]();
        --GUi_DT.savedMap.length = k - 1;
        tmpNewLen = k;
        dtmb = true;
        --GUi_DT.savedMap.data[k] = ""
      else
        -- вот тут реально магия
        --GUi_DT.savedMap.length = k;
        tmpNewLen = k;
        dtmb = true
      end
    end
    
  end
  GUi_DT.savedMap.length = tmpNewLen;  
end




-- Не работает
function GUi_CP.CreateDeathGraphWindow()
  local theFrame = GUi_GGM.GUI.ControlPanel;--GUi_CP.CreateFrame("Recount_DeathGraph", "Death Graph", 182, 200)

  local g = GUi_CP.graph:CreateGraphLine("Recount_DeathScatter", theFrame, "TopLeft", "TopLeft", 25, -25, 762, 356)
  g:SetXAxis(0, 20)
  g:SetYAxis(0, 100)
  g:SetGridSpacing(1, 25)
  g:SetGridColor({0.5, 0.5, 0.5, 0.5})
  g:SetAxisDrawing(true, true)
  g:SetAxisColor({1.0, 1.0, 1.0, 1.0})
  g:SetYLabels(true, true)  
  theFrame.Graph = g

  --g = GUi_CP.graph:CreateGraphScatterPlot("Recount_DeathScatter", theFrame, "TopLeft", "TopLeft", 25, -25, 762, 356)
  --g:SetXAxis(-15, 1)
  --g:SetYAxis(0, 100)
  --g:SetGridSpacing(100, 100)
  --g:SetGridColor({0.5, 0.5, 0.5, 0})
  --g:SetAxisDrawing(false, false)
  --g:SetAxisColor({1.0, 1.0, 1.0, 0})
  --g:SetFrameLevel(theFrame.Graph:GetFrameLevel() + 1)

  --theFrame.Scatter = g

  GUi_GGM.GUI.ControlPanel.DeathGraph = theFrame
end



function GUi_CP.ShowDeathGraph(Health, Heals, Hits)
  if not GUi_GGM.GUI.ControlPanel.DeathGraph then
    GUi_CP:CreateDeathGraphWindow()
  end

  local DeathGraph = GUi_GGM.GUI.ControlPanel.DeathGraph
  DeathGraph:Show()
  --DeathGraph.title:SetText("Death Graph - "..title)
  DeathGraph.Graph:ResetData()
  DeathGraph.Graph:AddDataSeries(Health, {0.2, 1.0, 0.2, 0.8}, false)

  --DeathGraph.Scatter:ResetData()
  if Heals then
    DeathGraph.Graph:AddDataSeries(Heals, {0.1, 1.0, 0.1, 0.8}, false)
  end
  if Hits then
    DeathGraph.Graph:AddDataSeries(Hits, {1.0, 0.1, 0.1, 0.8}, false)
  end
end



local UiParent = GUi_GGM.GUI.ControlPanel;

function GUi_CP.CreateFrame(Name, Title, Height, Width, ShowFunc, HideFunc)
  local theFrame = CreateFrame("Frame", Name, UiParent);

  theFrame:ClearAllPoints();
  theFrame:SetPoint("CENTER",UiParent);
  theFrame:SetHeight(Height);
  theFrame:SetWidth(Width);

  theFrame:SetBackdrop({
    bgFile = "Interface\\Tooltips\\Ui-Tooltip-Background",
    tile = true,
    tileSize = 16,
    edgeFile = "Interface\\AddOns\\Recount\\textures\\otravi-semi-full-border",
    edgeSize = 32,
    insets = {left = 1, right = 1, top = 20, bottom = 1},
  });
  theFrame:SetBackdropBorderColor(1.0, 0.0, 0.0);
  theFrame:SetBackdropColor(24 / 255, 24 / 255, 24 / 255);

  

  theFrame:EnableMouse(true);
  theFrame:SetMovable(true);

  
  theFrame.title = theFrame:CreateGGC.FontString(nil, "OVERLAY", "GameFontNormal");
  theFrame.title:SetPoint("TopLeft", theFrame, "TopLeft", 6, -15);
  theFrame.title:SetTextColor(1.0, 1.0, 1.0, 1.0);
  theFrame.title:SetText(Title);
  --Recount:AddGGC.FontString(theFrame.title)


  theFrame.CloseButton = CreateFrame("Button", nil, theFrame);
  theFrame.CloseButton:SetNormalTexture("Interface\\Buttons\\Ui-Panel-MinimizeButton-Up.blp");
  theFrame.CloseButton:SetPushedTexture("Interface\\Buttons\\Ui-Panel-MinimizeButton-Down.blp");
  theFrame.CloseButton:SetHighlightTexture("Interface\\Buttons\\Ui-Panel-MinimizeButton-Highlight.blp");
  theFrame.CloseButton:SetWidth(20);
  theFrame.CloseButton:SetHeight(20);
  theFrame.CloseButton:SetPoint("TopRight", theFrame, "TopRight", -4, -12);
  theFrame.CloseButton:SetScript("OnClick",function(this)
    this:GetParent():Hide();
  end)

  return theFrame;
end








-- Кнопки по этим же типам
-- Спец : dump 
  -- level 0 (dumper)
    -- c - chat
    -- s - Service
  -- level 1 (chatter)
    -- c - console
    -- g - gUild
    -- i - instance
    -- p - party
    -- ra - raid
    -- rw - raid Warning
    -- s - say
    -- w - whisper
    -- y - yell
  -- level 2 (definer)
    -- a - all
    -- d - defined
    -- u - Undefined
  -- level 3 (typer)
    -- k - kill
    -- s - spell
    -- m - miss
  -- level 4 (counter)(только для level 4 == miss || spell)
    -- f - full
    -- s - singeton
  -- level 5 (mtyper)(только для level 3 == miss)
    -- al - all
    -- ab - abosrb
    -- b - block
    -- de - deflect
    -- do - dodge
    -- e - evasion
    -- i - immune
    -- m - miss
    -- p - parry
    -- rf - reflect
    -- rs - resist




-- не работает....или работает...я хз
function GUi_DS.dbgRecountCoordinates()

  --print(GUi_DS_LabelMap.meta.columns)
  --print(GUi_DS_LabelMap.meta.rows)
  local tmp_x, tmp_y = 0, 0

  -- пересчет координат надписей(чтоб не сливались)
  local summ = 0
  for i = 1,GUi_DS.labelMap.meta.columns do
    local maxlen = GUi_DS.headerMap.spellKill.meta.length[i];
    --local maxlen = strlen(GUi_DS_HeaderMap.data[i].data)
    --for j = 1,GUi_DS_LabelMap.meta.rows do     
      --GUi_DS_LabelMap.data[j][i].cy = GUi_dyDefault
      --if (strlen(GUi_DS_LabelMap.data[j][i].data) > maxlen) then
        --maxlen = strlen(GUi_DS_LabelMap.data[j][i].data)
      --end     
    --end
    
    for j = 1,GUi_DS.labelMap.meta.rows do 
      GUi_DS.labelMap.data[j][i].cy = GUi_DS.dyDefault;
      GUi_DS.labelMap.data[j][i].cx = maxlen*GUi_DS.pixelsPerSymbol;
    end
    GUi_DS_HeaderMap.data[i].cx = maxlen*GUi_DS.pixelsPerSymbol
    GUi_DS_HeaderMap.data[i].cy = GUi_DS.dyDefault
    summ = summ + maxlen
  end

  GUi_GGM.GUI.DataService:SetWidth(summ*GUi_pixelsPerSymbol+50);
  GUi_DS_InnerBody:SetWidth(summ*GUi_pixelsPerSymbol+50+30);
  GUi_DS_InnerHeader:SetWidth(summ*GUi_pixelsPerSymbol+50+30);

  for i=1,GUi_DS.labelMap.meta.columns do
    if (i == 1) then
      GUi_DS_HeaderMap.data[i].x = GUi_startXHeader
    else
      GUi_DS_HeaderMap.data[i].x = GUi_DS_HeaderMap.data[i - 1].x + GUi_DS.headerMap.data[i - 1].cx   
    end
    GUi_DS_HeaderMap.data[i].y = GUi_startYHeader
    for j=1,GUi_DS.labelMap.meta.rows do
      if (j == 1) then
        GUi_DS.labelMap.data[j][i].y = GUi_DS.startY
      else
        GUi_DS.labelMap.data[j][i].y = GUi_DS.labelMap.data[j - 1][i].y + GUi_DS.labelMap.data[j - 1][i].cy   
      end
      if (i == 1) then
        GUi_DS.labelMap.data[j][i].x = GUi_DS.startX
      else
        GUi_DS.labelMap.data[j][i].x = GUi_DS.labelMap.data[j][i - 1].x + GUi_DS.labelMap.data[j][i - 1].cx   
      end
    end
  end
end


-- должна быть отключена
function dbgDrawLabels()
  for k,v in pairs(GUi_DS_LabelMap.data) do
    for m,n in pairs(v) do
      GUi_DS.label(SContent, n.x, n.y, n.cx, n.cy, n.data,"CENTER", "CENTER", GUi_ColorObjectForm[n.color], "ARTWORK")
    end
  end

  for k,v in pairs(GUi_DS_HeaderMap.data) do
    GUi_DS.label(GUi_DS_InnerHeader, v.x, v.y, v.cx, v.cy, v.data,"CENTER", "CENTER", GUi_ColorObjectForm["#standart#"], "ARTWORK")
  end
end



--
function GUi_DS.defineLabelMap(exprow, exptype)
  -- костыль жесткий(исправлено(нет))
  --exprow = math.min(exprow, 1000);
  --expcol = math.min(expcol, 100);


  --if exprow < GUi_DS.labelMap.meta.allocated.rows and expcol < GUi_DS.labelMap.meta.allocated.columns then
    --return
  --end
  
  --print("reallocating : ", exprow, expcol)

  --local hdelta, vdelta = 0, 0;
  local lbdata = "";
  if exptype == "spell" then
    expcol = GUi_DS.headerMap.spellDaMage.meta.columns;
    lbdata = GUi_DS.headerMap.spellDaMage.lbtext;
  elseif exptype == "miss" then
    expcol = GUi_DS.headerMap.spellMiss.meta.columns;
    lbdata = GUi_DS.headerMap.spellMiss.lbtext;
  elseif exptype == "kill" then
    expcol = GUi_DS.headerMap.spellKill.meta.columns;
    lbdata = GUi_DS.headerMap.spellKill.lbtext;
  end

  GUi_DS.labelMap.meta.used.rows = exprow;
  GUi_DS.labelMap.meta.used.columns = expcol;


  for i=1,GUi_DS.labelMap.meta.allocated.rows do
    --hdelta = 0
    
    for j=1,GUi_DS.labelMap.meta.allocated.columns do
      if i > exprow or j > expcol then
        --print("119", GUi_DS.labelMap.data.logfield[i][j]); 
        --print("120", GUi_DS.headerMap.Default.lbtext[j]);      
        --print("121", GUi_ColorObjectForm[GUi_DS.headerMap.Default.lbtext[j].color]);        
        --GUi_DS.labelMap.data.logfield[i][j] = GUi_DS.createLabel(GUi_DS.SContent, GUi_DS.startX + hdelta, GUi_DS.startY + vdelta, GUi_DS.headerMap.Default.meta.length[j], GUi_DS.dyDefault, "GDU_EM_"..i.."_"..j, "CENTER", "CENTER", GUi_ColorObjectForm[GUi_DS.headerMap.Default.lbtext[j].color], "ARTWORK");
        GUi_DS.setInvisible(GUi_DS.labelMap.data.logfield[i][j]);
      else
        GUi_DS.setVisible(GUi_DS.labelMap.data.logfield[i][j]);
      end
      --hdelta = hdelta + GUi_DS.headerMap.Default.meta.length[j];
    end
    --vdelta = vdelta + GUi_DS.dyDefault;
  end


  for j=1,GUi_DS.labelMap.meta.allocated.columns do
    if j > expcol then
      --print("119", GUi_DS.labelMap.data.logfield[i][j]); 
      --print("120", GUi_DS.headerMap.Default.lbtext[j]);      
      --print("121", GUi_ColorObjectForm[GUi_DS.headerMap.Default.lbtext[j].color]);        
      --GUi_DS.labelMap.data.logfield[i][j] = GUi_DS.createLabel(GUi_DS.SContent, GUi_DS.startX + hdelta, GUi_DS.startY + vdelta, GUi_DS.headerMap.Default.meta.length[j], GUi_DS.dyDefault, "GDU_EM_"..i.."_"..j, "CENTER", "CENTER", GUi_ColorObjectForm[GUi_DS.headerMap.Default.lbtext[j].color], "ARTWORK");
      GUi_DS.setInvisible(GUi_DS.labelMap.data.header[j]);
    else
      GUi_DS.setVisible(GUi_DS.labelMap.data.header[j]);
      GUi_DS.setColoRedText(GUi_DS.labelMap.data.header[j], lbdata[j].data, lbdata[j].color);
    end
  end
  --hdelta = 0
  --for j=1,expcol do
    --if j > GUi_DS.labelMap.meta.allocated.columns then
      --GUi_DS.labelMap.data.header[j] = GUi_DS.createLabel(GUi_DS_InnerHeader, GUi_DS.startXHeader + hdelta, GUi_DS.startYHeader, GUi_DS.headerMap.Default.meta.length[j], GUi_DS.dyDefault, GUi_DS.headerMap.Default.lbtext[j].data, "CENTER", "CENTER", GUi_ColorObjectForm[GUi_DS.headerMap.Default.lbtext[j].color], "ARTWORK");
      
    --end
    --hdelta = hdelta + GUi_DS.headerMap.Default.meta.length[j];
  --end


  --if expcol > GUi_DS.labelMap.meta.allocated.columns then
    --GUi_DS.labelMap.meta.allocated.columns = expcol;
  --end

  --if exprow > GUi_DS.labelMap.meta.allocated.rows then
    --GUi_DS.labelMap.meta.allocated.rows = exprow;
  --end

  -- и тут мощный костыль
  hdelta = expcol*GUi_DS.headerMap.Default.meta.length[1]
  GUi_GGM.GUI.DataService:SetWidth(hdelta+50);
  GUi_DS_InnerBody:SetWidth(hdelta+50+30);
  GUi_DS_InnerHeader:SetWidth(hdelta+50+30);

end





-- Изначальный пул клеток таблицы, скорее всего 1000 строк на 11 столбцов, остальные записи уйдут в страницы
function GUi_DS.allocateLabelMap(exprow, expcol)

  local hdelta, vdelta = 0, 0;
  for i=1,exprow do
    hdelta = 0
    if GUi_DS.labelMap.data.logfield[i] == nil then
      GUi_DS.labelMap.data.logfield[i] = {};
    end
    for j=1,expcol do
      --if i > GUi_DS.labelMap.meta.allocated.rows or j > GUi_DS.labelMap.meta.allocated.columns then
        --print("119", GUi_DS.labelMap.data.logfield[i][j]); 
        --print("120", GUi_DS.headerMap.Default.lbtext[j]);      
        --print("121", GUi_ColorObjectForm[GUi_DS.headerMap.Default.lbtext[j].color]);        
      GUi_DS.labelMap.data.logfield[i][j] = GUi_DS.createLabel(GUi_DS.SContent, GUi_DS.startX + hdelta, GUi_DS.startY + vdelta, GUi_DS.headerMap.Default.meta.length[j], GUi_DS.dyDefault, "GDU_EM_"..i.."_"..j, "CENTER", "CENTER", GUi_ColorObjectForm[GUi_DS.headerMap.Default.lbtext[j].color], "ARTWORK");

      --end
      hdelta = hdelta + GUi_DS.headerMap.Default.meta.length[j];
    end
    vdelta = vdelta + GUi_DS.dyDefault;
  end

  hdelta = 0;
  for j=1,expcol do
    GUi_DS.labelMap.data.header[j] = GUi_DS.createLabel(GUi_DS_InnerHeader, GUi_DS.startXHeader + hdelta, GUi_DS.startYHeader, GUi_DS.headerMap.Default.meta.length[j], GUi_DS.dyDefault, GUi_DS.headerMap.Default.lbtext[j].data, "CENTER", "CENTER", GUi_ColorObjectForm[GUi_DS.headerMap.Default.lbtext[j].color], "ARTWORK");
    hdelta = hdelta + GUi_DS.headerMap.Default.meta.length[j];
  end

  GUi_DS.labelMap.meta.allocated.rows = exprow;
  GUi_DS.labelMap.meta.allocated.columns = expcol;

end


--
function GUi_DS.createLabel( parent, x, y, cx, cy, text, justify_v, justify_h, font, strata )
  --print(parent)
  local label = parent:CreateGGC.FontString( "Text" , strata or "ARTWORK", font or "GameFontNormalSmall")
  --label:SetFont( "GameFontNormalSmall", 12, "OUTLINE, MONOCHROME") --font or
  label:SetWidth( cx ) ;
  label:SetHeight(cy or 25) ;
  if (justify_v == "CENTER") then
    justify_v = "MIDDLE" ;
  end
  label:SetJustifyV( justify_v or "MIDDLE" )
  label:SetJustifyH( justify_h or "LEFT" )
  --label:SetJustifyV(justify_v)
  --label:SetJustifyH(justify_h)
  label:SetText( text )
  label:Show() 
  GUi_DS.moveto( label, x, y ) ;
  return label ;
end


--
function GUi_DS.createFrame( type_, name, parent, template )
  type_ = strlower(type_) ;
  name  = strlower(name) ;

  --if (GUi_DS.__frame_pool[name] == nil) then
  --  GUi_DS.__frame_pool[name] = {} ;
  --  --tbl.clear( GUi_DS.__frame_pool[name] ) ;
  --  GUi_DS.__frame_pool[name][0] = 0 ;
  --end

  --local pool = GUi_DS.__frame_pool[name] ;
  --local ndx = tbl.next(pool) ;
  local f = nil ;
  --if (ndx) and (pool[ndx]) and (type(pool[ndx]) == "table") and (pool[ndx].SetParent) then
  --  f = pool[ndx] ;
  --  pool[ndx] = nil ;
  --  f:SetParent( parent ) ;
  --else
    f = CreateFrame( type_, name, parent, template ) ;
  --  pool[0] = (pool[0] or 0) + 1 ;
  --end
  --GUi_DS.__frame_pool["#check"][f] = name ; -- track the checked out frames

  if (parent ~= nil) then
    f:SetFrameLevel( parent:GetFrameLevel() + 1 ) ;
  end
  return f ;
end



--
function GUi_DS.setpos( f, x, y, cx, cy )
  if (f) then
    GUi_DS.moveto( f, x, y ) ;
    if (cx ~= nil) and (cx > 0) then
      f:SetWidth(cx);
    end
    if (cy ~= nil) and (cy > 0) then
      f:SetHeight(cy);
    end
  end
  return f;
end


--
function GUi_DS.DeleteFrame( f ) 
  if (f == nil) then
    return nil ;
  end
  if (type(f) ~= "table") then
    return nil ;
  end
  f:Hide() ;
  f:SetParent(nil) ;
  --local key = oq.__frame_pool["#check"][f] ;
  --tbl.push( oq.__frame_pool[key], f ) ;  
  --GUi_DS.__frame_pool["#check"][f] = nil ;
  return nil ;
end



--
function GUi_DS.moveto( f, x, y ) 
  if (y >= 0) then
    if (x >= 0) then 
      f:SetPoint("TopLeft",f:GetParent(),"TopLeft", x, -1 * y)
    else
      f:SetPoint("TopRight",f:GetParent(),"TopRight", x, -1 * y)
    end
  else
    if (x >= 0) then 
      f:SetPoint("BottomLeft",f:GetParent(),"BottomLeft", x, -1 * y)
    else
      f:SetPoint("BottomRight",f:GetParent(),"BottomRight", x, -1 * y)
    end
  end
end


local data = {
      {cols = {1, "12:00", "Elizi", "Пятнистый кот", "daMage", "Lava Burst", 77451, 270000, "crit", "N/A"}},
      {cols = {2, "12:01", "Elizi", "Пятнистый кот", "daMage", "Lava Burst", 77451, 270000, "crit", "N/A"}}
  };

  GUi_DS.ServiceGeometry.dataTable.kill.handler:SetData(data);
















  controllers = { -- controller frames
    tableVisionTabControllerFrame = {
      basement = {
        Object = function() return GGM.GUI.DataService_TableVisionTabController end,
        parent = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.areas.innerBodyAreaFrame.area() end
      },          
      size = {
        width = function() return GGM.GUI.CommonService.commonGeometry.numeric.tabControllerWidth end,
        height = function() return GGM.GUI.CommonService.commonGeometry.numeric.tabControllerHeight end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.tabControllerOffsetX end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.tabControllerOffsetY end,
      },  
      metadata = {
        text = "Table View",
        tooltipText = "TB-122-29-TVTC", 
      },
    },
    graphicVisionTabControllerFrame = {     
      basement = {
        Object = function() return GGM.GUI.DataService_GraphicVisionTabController end,
        parent = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.areas.innerBodyAreaFrame.area() end
      },    
      size = {
        width = function() return GGM.GUI.CommonService.commonGeometry.numeric.tabControllerWidth end,
        height = function() return GGM.GUI.CommonService.commonGeometry.numeric.tabControllerHeight end,
      },
      anchor = {
        point = "LEFT",
        relativeTo = "RIGHT",
        offsetX = function() return GGM.GUI.CommonService.commonGeometry.numeric.tabControllerHorGap end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.tabControllerOffsetY end,
      },
      metadata = {
        text = "Graphic View",
        tooltipText = "TB-122-29-GVTC",
      },  
    },
    battleLogControllerFrame = {
      basement = {
        Object = function() return GUi_BattleLogController end,
        parent = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.mainFrame.basement.frame() end
      },
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.controllerWidth end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.mainFrame.height() end
      },
      anchor = {
        point = "TopRight",
        relativeTo = "TopLeft",
        offsetX = function() return 0 end,
        offsetY = function() return 0 end,
      }
    },
    GGM.GUI.DataFilterControllerFrame = {
      basement = {
        Object = function() return GUi_BattleLogController end,
        parent = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.mainFrame.basement.frame() end
      },
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.controllerWidth end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.mainFrame.height() end
      },
      anchor = {
        point = "TopRight", 
        relativeTo = "TopLeft", 
        offsetX = function() return GGM.GUI.CommonService.external.ternExp(GGM.GUI.DataService.btnBattleLogState, - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.controllerWidth, 0) end, 
        offsetY = function() return 0 end,
      }
    },
    GGM.GUI.ChatReportControllerFrame = {
      basement = {
        Object = function() return GUi_GGM.GUI.ChatReportController end,
        parent = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.mainFrame.basement.frame() end
      },
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.controllerWidth end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.mainFrame.height() end
      },
      anchor = {
        point = "TopLeft", 
        relativeTo = "TopRight",
        offsetX = function() return 0 end, 
        offsetY = function() return 0 end,
      }
    },
    tableVisionControllerFrame = {  
      basement = {
        controller = function() return GGM.GUI.DataService_TableVisionController end,
        parent = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.mainFrame.basement.frame() end
      },
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.areaWidgets.innerBody.size.width() end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.areaWidgets.innerBody.size.height() end
      },
      anchor = {
        point = "BottomLeft",
        relativeTo = "BottomLeft",
        offsetX = function() return 0 end,
        offsetY = function() return 0 end,
      } 
    },
    graphicVisionController = { 
      basement = {
        controller = function() return GGM.GUI.DataService_GraphicVisionContoller end,
        
      },  
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.areaWidgets.innerBody.size.width() end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.areaWidgets.innerBody.size.height() end
      },
      anchor = {
        point = "BottomLeft",
        relativeTo = "BottomLeft",
        offsetX = function() return 0 end,
        offsetY = function() return 0 end,
      }
    }
  },
  buttons = {
    battleLogDropdown = {
      text = "Battle Log",
      tooltipText = "B-122-29-BL",      
      size = {
        width = function() return GGM.GUI.CommonService.commonGeometry.numeric.stockButtonWidth end,
        height = function() return GGM.GUI.CommonService.commonGeometry.numeric.stockButtonHeight end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.buttonHeaderOffsetX end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.buttonHeaderOffsetY end,
      },
      metadata = {

      },
    },
    GGM.GUI.DataFilterDropdown = {
      text = "Data Filter",
      tooltipText = "B-122-29-DF",      
      size = {
        width = function() return GGM.GUI.CommonService.commonGeometry.numeric.stockButtonWidth end,
        height = function() return GGM.GUI.CommonService.commonGeometry.numeric.stockButtonHeight end,
      },
      anchor = {
        point = "LEFT",
        relativeTo = "RIGHT",
        offsetX = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.buttonHorGap end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.buttonHeaderOffsetY end,
      },
      metadata = {

      },
    },
    GGM.GUI.ChatReportDropdown = {
      text = "Chat Report",
      tooltipText = "B-122-29-CR",      
      size = {
        width = function() return GGM.GUI.CommonService.commonGeometry.numeric.stockButtonWidth end,
        height = function() return GGM.GUI.CommonService.commonGeometry.numeric.stockButtonHeight end,
      },
      anchor = {
        point = "TopRight",
        relativeTo = "TopRight",
        offsetX = function() return -GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.buttonHeaderOffsetX end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.buttonHeaderOffsetY end,
      },
      metadata = {

      },
    },
    GGM.GUI.ChatReportDump = {
      text = "Chat Dump",
      tooltipText = "B-122-29-CD",      
      size = {
        width = function() return GGM.GUI.CommonService.commonGeometry.numeric.stockButtonWidth end,
        height = function() return GGM.GUI.CommonService.commonGeometry.numeric.stockButtonHeight end,
      },
      anchor = {
        point = "BOTTOM",
        relativeTo = "BOTTOM",
        offsetX = function() return 0 end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.buttonReportOffsetY end,
      },
      metadata = {

      },
    }
  },
  Titles = {
    -- Контроллер Фильтров Данных(КФД)/Data Filter Controller(DFC)        
    -- User Interface Font String Check Button Title Data Filter Controller
    UiFS_CBT_DFC0 = {   -- спец часть(заголовок)
      text = "UiFS_CBT_DFC0",
      tooltipText = "Global",     
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.titleWidth end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.titleHeight end,
      },
      anchor = {
        point = "TOP",
        relativeTo = "TOP",
        offsetX = function() return 0 end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.menuStartOffsetY end,
      }, -- верх минус 10
      metadata = {

      },
    },
    UiFS_CBT_DFC1 = {
      text = "UiFS_CBT_DFC1",
      tooltipText = "KDM Type",     
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.titleWidth end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.titleHeight end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.menuOffsetX end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC0.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta end,
      }, -- верх минус 10
      metadata = {

      },
    },
    UiFS_CBT_DFC2 = {
      text = "UiFS_CBT_DFC2",
      tooltipText = "CLR Type",     
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.titleWidth end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.titleHeight end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.menuOffsetX end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC1_3.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta end,
      }, -- предыдущий минус 3+1 на 20
      metadata = {

      },
    },
    UiFS_CBT_DFC3 = {
      text = "UiFS_CBT_DFC3",
      tooltipText = "SS Type",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.titleWidth end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.titleHeight end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.menuOffsetX end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC2_6.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta end,
      }, -- предыдущий минус 6+1 на 20
      metadata = {

      },
    },
    UiFS_CBT_DFC4 = {
      text = "UiFS_CBT_DFC4",
      tooltipText = "DF Type",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.titleWidth end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.titleHeight end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.menuOffsetX end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC3_11.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta end,
      }, -- предыдущий минус 11+1 на 20
      metadata = {

      },
    },
    UiFS_CBT_DFC5 = {
      text = "UiFS_CBT_DFC5",
      tooltipText = "DF Type",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.titleWidth end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.titleHeight end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.menuOffsetX end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC4_3.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta end,
      }, -- предыдущий минус 3+1 на 20
      metadata = {

      },
    },

    
    -- Контроллер Боевых Логов(КБЛ)/Battle Log Controller(BLC)    
    -- User Inteface Font String Check Button Title Battle Log Controller
    UiFS_CBT_BLC0 = {
      text = "UiFS_CBT_BLC0",
      tooltipText = "Log List",     
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.titleWidth end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.titleHeight end,
      },
      anchor = {
        point = "TOP",
        relativeTo = "TOP",
        offsetX = function() return 0 end,
        offsetY = function() return -10 end,
      },
      metadata = {

      },
    },


    -- Контроллер чат-репортов(КЧР)/Chat Report Controller(CRC)
    -- User Inteface Font String Check Button Title Chat Report Controller  
    UiFS_CBT_CRC0 = {
      text = "UiFS_CBT_CRC0",
      tooltipText = "Chat dump menu",     
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.titleWidth end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.titleHeight end,
      },
      anchor = {
        point = "TOP",
        relativeTo = "TOP",
        offsetX = function() return 0 end,
        offsetY = function() return -10 end,
      },
      metadata = {

      },
    },
  },
  GGC_CheckButtons = {
    -- Список типов лог данных        
    -- User Interface Font String Check Button Title Data Filter Controller
    UiCB_DFC1_1 = {
      text = "UiCB_DFC1_1",
      tooltipText = "DaMage",     
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end, 
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC1.anchor.offsetX() + GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonShiftX) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC1.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC1_2 = {
      text = "UiCB_DFC1_2",
      tooltipText = "Miss",     
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC1.anchor.offsetX() + GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonShiftX) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC1_1.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC1_3 = {
      text = "UiCB_DFC1_3",
      tooltipText = "Kill",     
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC1.anchor.offsetX() + GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonShiftX) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC1_2.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },

    --
    UiCB_DFC2_1 = {
      text = "UiCB_DFC2_1",
      tooltipText = "All",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC2.anchor.offsetX() + GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonShiftX) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC2.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC2_2 = {
      text = "UiCB_DFC2_2",
      tooltipText = "Swing",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC2.anchor.offsetX() + GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonShiftX) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC2_1.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC2_3 = {
      text = "UiCB_DFC2_3",
      tooltipText = "Range",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC2.anchor.offsetX() + GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonShiftX) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC2_2.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC2_4 = {
      text = "UiCB_DFC2_4",
      tooltipText = "Spell",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC2.anchor.offsetX() + GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonShiftX) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC2_3.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC2_5 = {
      text = "UiCB_DFC2_5",
      tooltipText = "SpellPeriodic",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC2.anchor.offsetX() + GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonShiftX) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC2_4.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC2_6 = {
      text = "UiCB_DFC2_6",
      tooltipText = "Environmental",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC2.anchor.offsetX() + GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonShiftX) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC2_5.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },

    --
    UiCB_DFC3_1 = {
      text = "UiCB_DFC3_1",
      tooltipText = "All",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC3.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC3.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC3_2 = {
      text = "UiCB_DFC3_2",
      tooltipText = "Absorb",     
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC3.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC3_1.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC3_3 = {
      text = "UiCB_DFC3_3",
      tooltipText = "Block",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC3.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC3_2.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC3_4 = {
      text = "UiCB_DFC3_4",
      tooltipText = "Deflect",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC3.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC3_3.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC3_5 = {
      text = "UiCB_DFC3_5",
      tooltipText = "Dodge",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC3.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC3_4.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC3_6 = {
      text = "UiCB_DFC3_6",
      tooltipText = "Evade",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC3.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC3_5.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC3_7 = {
      text = "UiCB_DFC3_7",
      tooltipText = "Immune",     
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC3.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC3_6.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC3_8 = {
      text = "UiCB_DFC3_8",
      tooltipText = "Miss",     
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC3.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC3_7.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC3_9 = {
      text = "UiCB_DFC3_9",
      tooltipText = "Parry",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC3.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC3_8.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC3_10 = {
      text = "UiCB_DFC3_10",
      tooltipText = "Reflect",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC3.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC3_9.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC3_11 = {
      text = "UiCB_DFC3_11",
      tooltipText = "Resist",     
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC3.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC3_10.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },

    --
    UiCB_DFC4_1 = {
      text = "UiCB_DFC4_1",
      tooltipText = "All",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC4.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC4.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC4_2 = {
      text = "UiCB_DFC4_2",
      tooltipText = "Defined",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC4.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC4_1.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },
    UiCB_DFC4_3 = {
      text = "UiCB_DFC4_3",
      tooltipText = "Undefined",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC4.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC4_2.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end,
      },
      metadata = {

      },
    },

    --
    UiCB_DFC5_1 = {
      text = "UiCB_DFC5_1",
      tooltipText = "Full",     
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC5.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC5.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end
      },
      metadata = {

      },
    },
    UiCB_DFC5_2 = {
      text = "UiCB_DFC5_2",
      tooltipText = "Singleton",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_DFC5.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_DFC5_1.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end
      },
      metadata = {

      },
    },


    -- Список логов   
    -- User Inteface Font String Check Button Title Battle Log Controller
    logList = {
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      startAnchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return 10 end,
        offsetY = function() return -30 end,
      },
    },


    -- Список опций чат дампа
    -- User Inteface Font String Check Button Title Chat Report Controller  
    UiCB_CRC1_1 = {
      text = "UiCB_CRC1_1",
      tooltipText = "Console",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_CRC0.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_CRC0.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end
      },
      metadata = {

      },
    },
    UiCB_CRC1_2 = {
      text = "UiCB_CRC1_2",
      tooltipText = "GUild",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_CRC0.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_CRC1_1.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end
      },
      metadata = {

      },
    },
    UiCB_CRC1_3 = {
      text = "UiCB_CRC1_3",
      tooltipText = "Instance",   
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_CRC0.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_CRC1_2.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end
      },
      metadata = {

      },
    },
    UiCB_CRC1_4 = {
      text = "UiCB_CRC1_4",
      tooltipText = "RaidWarning",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_CRC0.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_CRC1_3.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end
      },
      metadata = {

      },
    },
    UiCB_CRC1_5 = {
      text = "UiCB_CRC1_5",
      tooltipText = "Yell",     
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_CRC0.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_CRC1_4.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end
      },
      metadata = {

      },
    },
    UiCB_CRC1_6 = {
      text = "UiCB_CRC1_6",
      tooltipText = "Party",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_CRC0.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_CRC1_5.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end
      },
      metadata = {

      },
    },
    UiCB_CRC1_7 = {
      text = "UiCB_CRC1_7",
      tooltipText = "Whisper",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_CRC0.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_CRC1_6.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end
      },
      metadata = {

      },
    },
    UiCB_CRC1_8 = {
      text = "UiCB_CRC1_8",
      tooltipText = "Say",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_CRC0.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_CRC1_7.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end
      },
      metadata = {

      },
    },
    UiCB_CRC1_9 = {
      text = "UiCB_CRC1_9",
      tooltipText = "Raid",     
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.titles.UiFS_CBT_CRC0.anchor.offsetX() + 10) end,
        offsetY = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_CRC1_8.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.choiseOffsetYDelta) end
      },
      metadata = {

      },
    },


    -- Опции отражения графика
    -- User Inteface Check Button Graphic Vision Controller
    -- Общие
    UiCB_GVC1 = {
      text = "UiCB_GVC1",
      tooltipText = "Average",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return 10 end,
        offsetY = function() return -10 end
      },
      metadata = {

      },
    },
    UiCB_GVC2 = {
      text = "UiCB_GVC2",
      tooltipText = "Summary",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.visions.graphicVisionController.size.width()/2 + 10 end,
        offsetY = function() return -10 end
      },
      metadata = {

      },
    },
    -- Детальные
    UiCB_GVC1_1 = {
      text = "UiCB_GVC1_1",
      tooltipText = "AEN",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_GVC1.anchor.offsetX() + 10 end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_GVC1.anchor.offsetY() - 20 end
      },
      metadata = {

      },
    },
    UiCB_GVC1_2 = {
      text = "UiCB_GVC1_2",
      tooltipText = "AKD",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_GVC1_1.anchor.offsetX() + 90 end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_GVC1_1.anchor.offsetY() end
      },
      metadata = {

      },
    },
    UiCB_GVC1_3 = {
      text = "UiCB_GVC1_3",
      tooltipText = "ADD",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_GVC1_2.anchor.offsetX() + 90 end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_GVC1_2.anchor.offsetY() end
      },
      metadata = {

      },
    },
    UiCB_GVC1_4 = {
      text = "UiCB_GVC1_4",
      tooltipText = "AMD",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_GVC1_3.anchor.offsetX() + 90 end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_GVC1_3.anchor.offsetY() end
      },
      metadata = {

      },
    },
    UiCB_GVC2_1 = {
      text = "UiCB_GVC2_1",
      tooltipText = "SEN",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_GVC2.anchor.offsetX() + 10 end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_GVC2.anchor.offsetY() - 20 end
      },
      metadata = {

      },
    },
    UiCB_GVC2_2 = {
      text = "UiCB_GVC2_2",
      tooltipText = "SKD",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_GVC2_1.anchor.offsetX() + 90 end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_GVC2_1.anchor.offsetY() end
      },
      metadata = {

      },
    },
    UiCB_GVC2_3 = {
      text = "UiCB_GVC2_3",
      tooltipText = "SDD",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_GVC2_2.anchor.offsetX() + 90 end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_GVC2_2.anchor.offsetY() end
      },
      metadata = {

      },
    },
    UiCB_GVC2_4 = {
      text = "UiCB_GVC2_4",
      tooltipText = "SMD",      
      size = {
        width = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
        height = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.numeric.GGC_CheckButtonSize end,
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_GVC2_3.anchor.offsetX() + 90 end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.GGC_CheckButtons.UiCB_GVC2_3.anchor.offsetY() end
      },
      metadata = {

      },
    },
  },  
  dataTable = {
    Default = {
      handler = "",
      highlight = nil,
      fontHeight = function() return 14 end,
      size = {
        numRows = function() return 39 end,
        width = function() return GGM.GUI.DataService.dataTable.srvmtd.recountWidth(GGM.GUI.DataService.headerMap.Default.prototype) end, height = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.mainFrame.height() + 3*GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.buttons.battleLogDropdown.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.buttons.battleLogDropdown.size.height()) end
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return 10 end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.buttons.battleLogDropdown.anchor.offsetY() end
      },
    },
    daMage = {
      handler = "",
      highlight = nil,
      fontHeight = function() return 14 end,
      size = {
        numRows = function() return 39 end,
        width = function() return GGM.GUI.DataService.dataTable.srvmtd.recountWidth(GGM.GUI.DataService.headerMap.daMage.prototype) end, height = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.mainFrame.height() + 3*GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.buttons.battleLogDropdown.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.buttons.battleLogDropdown.size.height()) end
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return 10 end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.buttons.battleLogDropdown.anchor.offsetY() end
      },
    },
    miss = {
      handler = "",
      highlight = nil,
      fontHeight = function() return 14 end,
      size = {
        numRows = function() return 39 end,
        width = function() return GGM.GUI.DataService.dataTable.srvmtd.recountWidth(GGM.GUI.DataService.headerMap.miss.prototype) end, height = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.mainFrame.height() + 3*GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.buttons.battleLogDropdown.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.buttons.battleLogDropdown.size.height()) end
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return 10 end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.buttons.battleLogDropdown.anchor.offsetY() end
      },
    },
    kill = {
      handler = "",
      highlight = nil,
      fontHeight = function() return 14 end,
      size = {
        numRows = function() return 39 end,
        width = function() return GGM.GUI.DataService.dataTable.srvmtd.recountWidth(GGM.GUI.DataService.headerMap.kill.prototype) end, height = function() return (GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.mainFrame.height() + 3*GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.buttons.battleLogDropdown.anchor.offsetY() - GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.buttons.battleLogDropdown.size.height()) end
      },
      anchor = {
        point = "TopLeft",
        relativeTo = "TopLeft",
        offsetX = function() return 10 end,
        offsetY = function() return GGM.GUI.DataService.GGM.GUI.DataServiceGeometry.buttons.battleLogDropdown.anchor.offsetY() end
      },  
    },
    srvmtd = {
      recountWidth = function(prot) local summ = 0 for k,v in pairs(prot) do summ = summ + v["width"] end return summ end,
    },    
  },








-- api
-- Сборка(фильтрация) данных для таблицы из лог кластера
function GGM.GUI.DSTableEngine:dumpConstructor(in_logList, in_filterList)
  if (COMTBLSIZE(in_logList) == 0 or COMTBLSIZE(in_filterList) == 0) then
    return false;
  end
  -- NOTE: селектор является входным параметром, т.к. функция внешняя
  local evType, atkType, missType, defType = in_selector.filter.evType, in_selector.filter.atkType, in_selector.filter.missType, in_selector.filter.defType;
  local trgData = nil;
  local filteRedTrgData = {};
  local subData = {};
  local logItr = self.cs_logInterpret;

  if (evType == "DaMage") then
    trgData = in_selector.logClusterData.daMageUniversalMasterTable;

    if (atkType == "All") then
      for k,v in pairs(logItr.daMage.atkType) do
        for i=1,#trgData[v].data do
          subData[#subData+1] = trgData[v].data[i];
        end
      end
    else
      subData = trgData[logItr.daMage.atkType[atkType]].data;
    end

    filteRedTrgData = subData;
  elseif (evType == "Miss") then
    trgData = in_selector.logClusterData.missUniversalMasterTable;

    if (missType == "All") then
      if (atkType == "All") then
        for k,v in pairs(logItr.miss.missType) do
          for m,n in pairs(logItr.miss.atkType) do
            for i=1,#trgData[v][n].data do
              subData[#subData+1] = trgData[v][n].data[i];
            end
          end
        end
      else
        for k,v in pairs(logItr.miss.missType) do
          for i=1,#trgData[v][logItr.miss.atkType[atkType]].data do
            subData[#subData+1] = trgData[v][logItr.miss.atkType[atkType]].data[i];
          end
        end
      end
    else
      if (atkType == "All") then
        for k,v in pairs(logItr.miss.atkType) do
          for i=1,#trgData[logItr.miss.missType[missType]][v].data do
            subData[#subData+1] = trgData[logItr.miss.missType[missType]][v].data[i];
          end
        end
      else
        subData = trgData[logItr.miss.missType[missType]][logItr.miss.atkType[atkType]].data;
      end
    end

    filteRedTrgData = subData;

  elseif (evType == "Kill") then
    trgData = in_selector.logClusterData.killUniversalMasterTable;
    filteRedTrgData = trgData.data;
  else
    return nil, nil;
  end
  
  -- Индусский код
  --local fltr_f, fltr_s = "defined", "Undefined";
  local fltr_f, fltr_s = self.cs_definitenessFilterType["Defined"], self.cs_definitenessFilterType["Undefined"];

  -- for "defined" - drop fltr_s
  fltr_s = GGF.TernExpSingle(defType == "Defined", "", self.cs_definitenessFilterType["Defined"]);
  -- for "Undefined" - drop fltr_f
  fltr_f = GGF.TernExpSingle(defType == "Undefined", "", self.cs_definitenessFilterType["Undefined"]);
  
  local dfdTrgData = {};

  -- Временное решение, неплохо бы менять саму таблицу на сингл/фул
  local ind = 1;
  local hash = {};
  for k,v in pairs(filteRedTrgData) do
    if ((v.spellDefined.data == fltr_f) or (v.spellDefined.data == fltr_s) or (v.spellDefined.data == "N/A")) then
      dfdTrgData[ind] = filteRedTrgData[k];
      ind = ind + 1;
    end
  end

  local dfdMetaInf = {
    logStartTime,
    logFinishTime,
    logIdentifyingToken,
    logZoneName
  };
  dfdMetaInf.logStartTime = in_selector.logClusterData.metaInformation.logStartTime;
  dfdMetaInf.logFinishTime = in_selector.logClusterData.metaInformation.logFinishTime;
  dfdMetaInf.logIdentifyingToken = in_selector.logClusterData.metaInformation.logIdentifyingToken;
  dfdMetaInf.logZoneName = in_selector.logClusterData.metaInformation.logZoneName;

  return true, dfdMetaInf, dfdDataPrototype, dfdTrgData;
end








-- NOTE: не работает(отключена)
-- Генерация токена джампера на основе пути
function GGC.BaseModule:generateJumperToken(in_path)
  -- NOTE: При появлении новых типов нужно добавить их сюда
  local typeList = {
    ["frame"] = "_F",
    ["GGC.FontString"] = "_FS",
    ["texture"] = "_T",

    ["inlineGroup"] = "_IG",
    ["button"] = "_B",
    ["checkBox"] = "_CB",
    ["slider"] = "_S",

    ["GGC.PanelControlGroup"] = "_PCG",
  };
  local result = "";

  -- TODO: Придумать конструкцию получше...а то костыль какой-то..
  local typeFlag = false;
  local lastType = "";

  for wrd in string.gmatch(in_path, "%w+") do
    if (typeFlag == true) then
      typeFlag = false;
      if (result ~= "") then
        result = result .. "_";
      end
      result = result .. self:parseObjectName(wrd) .. typeList[lastType];
    end
    if (typeList[wrd]) then
      typeFlag = true;
      lastType = wrd;
    end
  end

  return result;
end


-- Преобразование названия объекта в часть токена
function GGC.BaseModule:parseObjectName(in_name)
  local result = "";
  result = result .. in_name:sub(1,1):upper();

  for idx=2,#in_name do
    local smbl = in_name:sub(i,i);
    if (smbl == smbl:upper()) then
      result  = result .. smbl;
    end
  end

  return result;
end


analogueDataMeter = GGF.GTemplateTree(GGC.AnalogueDataMeter, {
   killRate = {},    -- [KR_ADM]
   damageRate = {},  -- [DR_ADM]
   healRate = {},    -- [HR_ADM]
}),

layer = {
   namedValueFontString = GGF.GTemplateTree(GGC.NamedValueFontString, {
      surroundingKillRate = {},     -- 11 [SKR_NVFS]
      surroundingDamageRate = {},      -- 12 [SDR_NVFS]
      surroundingHealRate = {},     -- 13 [SHR_NVFS]
      surroundingKillAverage = {},  -- 21 [SKA_NVFS]
      surroundingDamageAverage = {},   -- 22 [SDA_NVFS]
      surroundingHealAverage = {},  -- 23 [SHA_NVFS]
      surroundingKillSummary = {},  -- 31 [SKS_NVFS]
      surroundingDamageSummary = {},   -- 32 [SDS_NVFS]
      surroundingHealSummary = {},  -- 33 [SHS_NVFS]
   }),
},

local CIG_LF_CDU_ADM = CIG_LF.combatDashboardUpper.inherit.analogueDataMeter;
local CIG_LF_CDU_NVFS = CIG_LF.combatDashboardLower.inherit.layer.namedValueFontString;

-- combatDashboardUpper(LF)
   -- killRate(ADM)
CIG_LF_CDU_ADM.killRate.object = {};
CIG_LF_CDU_ADM.killRate.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "KR",
      },
      miscellaneous = {
         hidden = false,
         movable = false,
         enableMouse = false,
         enableKeyboard = false,
      },
      hardware = {
         meterUnits = "k/m",
         meterFile = GGD.GUI.TexturePath.."meter.tga",
         arrowFile = GGD.GUI.TexturePath.."arrow.tga",
         limits = {
            max = 1000,
         },
      },
      size = {
         width = MSC_CONFIG.dashHeight,
         height = MSC_CONFIG.dashHeight,
      },
      backdrop = GGD.Backdrop.Empty,
      anchor = {
         point = GGE.PointType.TopLeft,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.meterBorderGap,
         offsetY = 0,
      },
   },
};


-- combatDashboardUpper(LF)
   -- damageRate(ADM)
CIG_LF_CDU_ADM.damageRate.object = {};
CIG_LF_CDU_ADM.damageRate.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "DR",
      },
      miscellaneous = {
         hidden = false,
         movable = false,
         enableMouse = false,
         enableKeyboard = false,
      },
      hardware = {
         meterUnits = "dmg/m",
         meterFile = GGD.GUI.TexturePath.."meter.tga",
         arrowFile = GGD.GUI.TexturePath.."arrow.tga",
         limits = {
            max = 20000,
         },
      },
      size = {
         width = MSC_CONFIG.dashHeight,
         height = MSC_CONFIG.dashHeight,
      },
      backdrop = GGD.Backdrop.Empty,
      anchor = {
         point = GGE.PointType.Left,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Right,
         offsetX = MSC_CONFIG.meterHorGap,
         offsetY = 0,
      },
   },
};


-- combatDashboardUpper(SF)
   -- healRate(ADM)
CIG_LF_CDU_ADM.healRate.object = {};
CIG_LF_CDU_ADM.healRate.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         template = nil,
         wrapperName = "HR",
      },
      miscellaneous = {
         hidden = false,
         movable = false,
         enableMouse = false,
         enableKeyboard = false,
      },
      hardware = {
         meterUnits = "h/m",
         meterFile = GGD.GUI.TexturePath.."meter.tga",
         arrowFile = GGD.GUI.TexturePath.."arrow.tga",
         limits = {
            max = 10000,
         },
      },
      size = {
         width = MSC_CONFIG.dashHeight,
         height = MSC_CONFIG.dashHeight,
      },
      backdrop = GGD.Backdrop.Empty,
      anchor = {
         point = GGE.PointType.Left,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Right,
         offsetX = MSC_CONFIG.meterHorGap,
         offsetY = 0,
      },
   },
};


-- SKR 11
-- [+][ ][ ]
-- [ ][ ][ ]
-- [ ][ ][ ]


-- combatDashboardLower(LF)
   -- surroundingKillRate(NVFS)
CIG_LF_CDU_NVFS.surroundingKillRate.object = {};
CIG_LF_CDU_NVFS.surroundingKillRate.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "SKR",
      },
      miscellaneous = {
         hidden = false,
         name = "SKR:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      backdrop = GGD.Backdrop.Empty,
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot11OffsetX,
         offsetY = MSC_CONFIG.slot11OffsetY,
      },
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};


-- SDR 12
-- [ ][+][ ]
-- [ ][ ][ ]
-- [ ][ ][ ]

-- combatDashboardLower(LF)
   -- surroundingDamageRate(NVFS)
CIG_LF_CDU_NVFS.surroundingDamageRate.object = {};
CIG_LF_CDU_NVFS.surroundingDamageRate.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "SDR",
      },
      miscellaneous = {
         hidden = false,
         name = "SDR:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      backdrop = GGD.Backdrop.Empty,
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot12OffsetX,
         offsetY = MSC_CONFIG.slot12OffsetY,
      },
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};


-- SHR 13
-- [ ][ ][+]
-- [ ][ ][ ]
-- [ ][ ][ ]

-- combatDashboardLower(LF)
   -- surroundingHealRate(NVFS)
CIG_LF_CDU_NVFS.surroundingHealRate.object = {};
CIG_LF_CDU_NVFS.surroundingHealRate.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "SHR",
      },
      miscellaneous = {
         hidden = false,
         name = "SHR:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      backdrop = GGD.Backdrop.Empty,
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot13OffsetX,
         offsetY = MSC_CONFIG.slot13OffsetY,
      },
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};


-- SKA 21
-- [ ][ ][ ]
-- [+][ ][ ]
-- [ ][ ][ ]

-- combatDashboardLower(LF)
   -- surroundingKillAverage(NVFS)
CIG_LF_CDU_NVFS.surroundingKillAverage.object = {};
CIG_LF_CDU_NVFS.surroundingKillAverage.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "SKA",
      },
      miscellaneous = {
         hidden = false,
         name = "SKA:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      backdrop = GGD.Backdrop.Empty,
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot21OffsetX,
         offsetY = MSC_CONFIG.slot21OffsetY,
      },
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};


-- SDA 22
-- [ ][ ][ ]
-- [ ][+][ ]
-- [ ][ ][ ]

-- combatDashboardLower(LF)
   -- surroundingDamageAverage(NVFS)
CIG_LF_CDU_NVFS.surroundingDamageAverage.object = {};
CIG_LF_CDU_NVFS.surroundingDamageAverage.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "SDA",
      },
      miscellaneous = {
         hidden = false,
         name = "SDA:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      backdrop = GGD.Backdrop.Empty,
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot22OffsetX,
         offsetY = MSC_CONFIG.slot22OffsetY,
      },
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};


-- SHA 23
-- [ ][ ][ ]
-- [ ][ ][+]
-- [ ][ ][ ]

-- combatDashboardLower(LF)
   -- surroundingHealAverage(NVFS)
CIG_LF_CDU_NVFS.surroundingHealAverage.object = {};
CIG_LF_CDU_NVFS.surroundingHealAverage.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "SHA",
      },
      miscellaneous = {
         hidden = false,
         name = "SKR:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      backdrop = GGD.Backdrop.Empty,
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot23OffsetX,
         offsetY = MSC_CONFIG.slot23OffsetY,
      },
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};


-- SKS 31
-- [ ][ ][ ]
-- [ ][ ][ ]
-- [+][ ][ ]

-- combatDashboardLower(LF)
   -- surroundingKillSummary(NVFS)
CIG_LF_CDU_NVFS.surroundingKillSummary.object = {};
CIG_LF_CDU_NVFS.surroundingKillSummary.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "SKS",
      },
      miscellaneous = {
         hidden = false,
         name = "SKS:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      backdrop = GGD.Backdrop.Empty,
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot31OffsetX,
         offsetY = MSC_CONFIG.slot31OffsetY,
      },
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};


-- SDS 32
-- [ ][ ][ ]
-- [ ][ ][ ]
-- [ ][+][ ]

-- combatDashboardLower(LF)
   -- surroundingDamageSummary(NVFS)
CIG_LF_CDU_NVFS.surroundingDamageSummary.object = {};
CIG_LF_CDU_NVFS.surroundingDamageSummary.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "SDS",
      },
      miscellaneous = {
         hidden = false,
         name = "SDS:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      backdrop = GGD.Backdrop.Empty,
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot32OffsetX,
         offsetY = MSC_CONFIG.slot32OffsetY,
      },
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};


-- SHS
-- [ ][ ][ ]
-- [ ][ ][ ]
-- [ ][ ][+]

-- combatDashboardLower(LF)
   -- surroundingHealSummary(NVFS)
CIG_LF_CDU_NVFS.surroundingHealSummary.object = {};
CIG_LF_CDU_NVFS.surroundingHealSummary.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "SHS",
      },
      miscellaneous = {
         hidden = false,
         name = "SHS:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      backdrop = GGD.Backdrop.Empty,
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot33OffsetX,
         offsetY = MSC_CONFIG.slot33OffsetY,
      },
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};

   -- combatDashboardUpper(LF)
      -- killRate(ADM)

   self.JR_CDU_LF_KR_ADM_OBJ = GGC.AnalogueDataMeter:Create(self.JR_CDU_LF_KR_ADM_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
      },
   });


   -- combatDashboardUpper(LF)
      -- damageRate(ADM)

   self.JR_CDU_LF_DR_ADM_OBJ = GGC.AnalogueDataMeter:Create(self.JR_CDU_LF_DR_ADM_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDU_LF_KR_ADM_OBJ),
         },
      },
   });


   -- combatDashboardUpper(LF)
      -- healRate(ADM)

   self.JR_CDU_LF_HR_ADM_OBJ = GGC.AnalogueDataMeter:Create(self.JR_CDU_LF_HR_ADM_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDU_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDU_LF_DR_ADM_OBJ),
         },
      },
   });



   -- SKR 11
   -- [+][ ][ ]
   -- [ ][ ][ ]
   -- [ ][ ][ ]

   -- combatDashboardLower(LF)
      -- surroundingKillRate(NVFS)

   self.JR_CDL_LF_SKR_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_CDL_LF_SKR_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
      },
   });


   -- SDR 12
   -- [ ][+][ ]
   -- [ ][ ][ ]
   -- [ ][ ][ ]

   -- combatDashboardLower(LF)
      -- surroundingDamageRate(NVFS)

   self.JR_CDL_LF_SDR_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_CDL_LF_SDR_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
      },
   });


   -- SHR 13
   -- [ ][ ][+]
   -- [ ][ ][ ]
   -- [ ][ ][ ]

   -- combatDashboardLower(LF)
      -- surroundingHealRate(NVFS)

   self.JR_CDL_LF_SHR_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_CDL_LF_SHR_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
      },
   });


   -- SKA 21
   -- [ ][ ][ ]
   -- [+][ ][ ]
   -- [ ][ ][ ]

   -- combatDashboardLower(LF)
      -- surroundingKillAverage(NVFS)

   self.JR_CDL_LF_SKA_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_CDL_LF_SKA_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
      },
   });


   -- SDA 22
   -- [ ][ ][ ]
   -- [ ][+][ ]
   -- [ ][ ][ ]

   -- combatDashboardLower(LF)
      -- surroundingDamageAverage(NVFS)

   self.JR_CDL_LF_SDA_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_CDL_LF_SDA_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
      },
   });


   -- SHA 23
   -- [ ][ ][ ]
   -- [ ][ ][+]
   -- [ ][ ][ ]

   -- combatDashboardLower(LF)
      -- surroundingHealAverage(NVFS)

   self.JR_CDL_LF_SHA_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_CDL_LF_SHA_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
      },
   });


   -- SKS 31
   -- [ ][ ][ ]
   -- [ ][ ][ ]
   -- [+][ ][ ]

   -- combatDashboardLower(LF)
      -- surroundingKillSummary(NVFS)

   self.JR_CDL_LF_SKS_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_CDL_LF_SKS_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
      },
   });


   -- SKS 32
   -- [ ][ ][ ]
   -- [ ][ ][ ]
   -- [ ][+][ ]

   -- combatDashboardLower(LF)
      -- surroundingDamageSummary(NVFS)

   self.JR_CDL_LF_SDS_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_CDL_LF_SDS_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
      },
   });


   -- SKS 33
   -- [ ][ ][ ]
   -- [ ][ ][ ]
   -- [ ][ ][+]

   -- combatDashboardLower(LF)
      -- surroundingHealSummary(NVFS)

   self.JR_CDL_LF_SHS_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_CDL_LF_SHS_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_CDL_LF_OBJ),
         },
      },
   });





   -- combatDashboardUpper(LF)
      -- killRateMeter(ADM)

   GGF.OuterInvoke(self.JR_CDU_LF_KR_ADM_OBJ, "Adjust");


   -- combatDashboardUpper(LF)
      -- damageRateMeterWrapper(ADM)

   GGF.OuterInvoke(self.JR_CDU_LF_DR_ADM_OBJ, "Adjust");


   -- combatDashboardUpper(LF)
      -- healRateMeterWrapper(ADM)

   GGF.OuterInvoke(self.JR_CDU_LF_HR_ADM_OBJ, "Adjust");


   -- SKR 11
   -- [+][ ][ ]
   -- [ ][ ][ ]
   -- [ ][ ][ ]

   -- combatDashboardLower(LF)
      -- surroundingKillRate(NVFS)

   GGF.OuterInvoke(self.JR_CDL_LF_SKR_NVFS_OBJ, "Adjust");


   -- SDR 12
   -- [ ][+][ ]
   -- [ ][ ][ ]
   -- [ ][ ][ ]

   -- combatDashboardLower(LF)
      -- surroundingDamageRate(NVFS)

   GGF.OuterInvoke(self.JR_CDL_LF_SDR_NVFS_OBJ, "Adjust");


   -- SHR 13
   -- [ ][ ][+]
   -- [ ][ ][ ]
   -- [ ][ ][ ]

   -- combatDashboardLower(LF)
      -- surroundingHealRate(NVFS)

   GGF.OuterInvoke(self.JR_CDL_LF_SHR_NVFS_OBJ, "Adjust");


   -- SKA 21
   -- [ ][ ][ ]
   -- [+][ ][ ]
   -- [ ][ ][ ]

   -- combatDashboardLower(LF)
      -- surroundingKillAverage(NVFS)

   GGF.OuterInvoke(self.JR_CDL_LF_SKA_NVFS_OBJ, "Adjust");


   -- SDA 22
   -- [ ][ ][ ]
   -- [ ][+][ ]
   -- [ ][ ][ ]

   -- combatDashboardLower(LF)
      -- surroundingDamageAverage(NVFS)

   GGF.OuterInvoke(self.JR_CDL_LF_SDA_NVFS_OBJ, "Adjust");


  -- SHA 23
  -- [ ][ ][ ]
  -- [ ][ ][+]
  -- [ ][ ][ ]

   -- combatDashboardLower(LF)
      -- surroundingHealAverage(NVFS)

   GGF.OuterInvoke(self.JR_CDL_LF_SHA_NVFS_OBJ, "Adjust");


  -- SKS 31
  -- [ ][ ][ ]
  -- [ ][ ][ ]
  -- [+][ ][ ]

   -- combatDashboardLower(LF)
      -- surroundingKillSummary(NVFS)

   GGF.OuterInvoke(self.JR_CDL_LF_SKS_NVFS_OBJ, "Adjust");


   -- SKS 32
   -- [ ][ ][ ]
   -- [ ][ ][ ]
   -- [ ][+][ ]

   -- combatDashboardLower(SF)
      -- surroundingDamageSummary(NVFS)

   GGF.OuterInvoke(self.JR_CDL_LF_SDS_NVFS_OBJ, "Adjust");


   -- SKS 33
   -- [ ][ ][ ]
   -- [ ][ ][ ]
   -- [ ][ ][+]

   -- combatDashboardLower(SF)
      -- surroundingHealSummary(NVFS)

   GGF.OuterInvoke(self.JR_CDL_LF_SHS_NVFS_OBJ, "Adjust");



-- SKR 11
-- [+][ ][ ]
-- [ ][ ][ ]
-- [ ][ ][ ]

-- api
-- Установка цвета текста SKR(Surrounding Kill Rate)
function This:setSKRColor(in_nwc)
   GGF.OuterInvoke(self.JR_CDL_LF_SKR_NVFS_OBJ, "SetColor", in_nwc);
end


-- api
-- Установка текста SKR(Surrounding Kill Rate)
function This:setSKRValue(in_value)
   GGF.OuterInvoke(self.JR_CDL_LF_SKR_NVFS_OBJ, "SetValue", in_value);
end


-- SDR 12
-- [ ][+][ ]
-- [ ][ ][ ]
-- [ ][ ][ ]

-- api
-- Установка цвета текста SDR(Surrounding Damage Rate)
function This:setSDRColor(in_nwc)
   GGF.OuterInvoke(self.JR_CDL_LF_SDR_NVFS_OBJ, "SetColor", in_nwc);
end


-- api
-- Установка текста SDR(Surrounding Damage Rate)
function This:setSDRValue(in_value)
   GGF.OuterInvoke(self.JR_CDL_LF_SDR_NVFS_OBJ, "SetValue", in_value);
end


-- SHR 13
-- [ ][ ][+]
-- [ ][ ][ ]
-- [ ][ ][ ]

-- api
-- Установка цвета текста SHR(Surrounding Heal Rate)
function This:setSHRColor(in_nwc)
   GGF.OuterInvoke(self.JR_CDL_LF_SHR_NVFS_OBJ, "SetColor", in_nwc);
end


-- api
-- Установка текста SHR(Surrounding Heal Rate)
function This:setSHRValue(in_value)
   GGF.OuterInvoke(self.JR_CDL_LF_SHR_NVFS_OBJ, "SetValue", in_value);
end


-- SKA 21
-- [ ][ ][ ]
-- [+][ ][ ]
-- [ ][ ][ ]

-- api
-- Установка цвета текста SKA(Surrounding Kill Average)
function This:setSKAColor(in_nwc)
   GGF.OuterInvoke(self.JR_CDL_LF_SKA_NVFS_OBJ, "SetColor", in_nwc);
end


-- api
-- Установка текста SKA(Surrounding Kill Average)
function This:setSKAValue(in_value)
   GGF.OuterInvoke(self.JR_CDL_LF_SKA_NVFS_OBJ, "SetValue", in_value);
end


-- SDA 22
-- [ ][ ][ ]
-- [ ][+][ ]
-- [ ][ ][ ]

-- api
-- Установка цвета текста SDA(Surrounding Damage Average)
function This:setSDAColor(in_nwc)
   GGF.OuterInvoke(self.JR_CDL_LF_SDA_NVFS_OBJ, "SetColor", in_nwc);
end


-- api
-- Установка текста SDA(Surrounding Damage Average)
function This:setSDAValue(in_value)
   GGF.OuterInvoke(self.JR_CDL_LF_SDA_NVFS_OBJ, "SetValue", in_value);
end


-- SHA 23
-- [ ][ ][ ]
-- [ ][ ][+]
-- [ ][ ][ ]

-- api
-- Установка цвета текста SHA(Surrounding Heal Average)
function This:setSHAColor(in_nwc)
   GGF.OuterInvoke(self.JR_CDL_LF_SHA_NVFS_OBJ, "SetColor", in_nwc);
end


-- api
-- Установка текста SHA(Surrounding Heal Average)
function This:setSHAValue(in_value)
   GGF.OuterInvoke(self.JR_CDL_LF_SHA_NVFS_OBJ, "SetValue", in_value);
end


-- SKS 31
-- [ ][ ][ ]
-- [ ][ ][ ]
-- [+][ ][ ]

-- api
-- Установка цвета текста SKS(Surrounding Kill Summary)
function This:setSKSColor(in_nwc)
   GGF.OuterInvoke(self.JR_CDL_LF_SKS_NVFS_OBJ, "SetColor", in_nwc);
end


-- api
-- Установка текста SKS(Surrounding Kill Summary)
function This:setSKSValue(in_value)
   GGF.OuterInvoke(self.JR_CDL_LF_SKS_NVFS_OBJ, "SetValue", in_value);
end


-- SDS 32
-- [ ][ ][ ]
-- [ ][ ][ ]
-- [ ][+][ ]

-- api
-- Установка цвета текста SDS(Surrounding Damage Summary)
function This:setSDSColor(in_nwc)
   GGF.OuterInvoke(self.JR_CDL_LF_SDS_NVFS_OBJ, "SetColor", in_nwc);
end


-- api
-- Установка текста SDS(Surrounding Damage Summary)
function This:setSDSValue(in_value)
   GGF.OuterInvoke(self.JR_CDL_LF_SDS_NVFS_OBJ, "SetValue", in_value);
end


-- SHS 33
-- [ ][ ][ ]
-- [ ][ ][ ]
-- [ ][ ][+]

-- api
-- Установка цвета текста SHS(Surrounding Heal Summary)
function This:setSHSColor(in_nwc)
   GGF.OuterInvoke(self.JR_CDL_LF_SHS_NVFS_OBJ, "SetColor", in_nwc);
end


-- api
-- Установка текста SHS(Surrounding Heal Summary)
function This:setSHSValue(in_value)
   GGF.OuterInvoke(self.JR_CDL_LF_SHS_NVFS_OBJ, "SetValue", in_value);
end


   -- TODO: подогнать
   -- NOTE: дак уже, не?
   -- [11][12][13]
   -- [21][22][23]
   -- [31][32][33]
   slot11OffsetX = 78,
   slot11OffsetY = -10,

   slot12OffsetX = 210,
   slot12OffsetY = -10,

   slot13OffsetX = 330,
   slot13OffsetY = -10,

   slot21OffsetX = 78,
   slot21OffsetY = -50,

   slot22OffsetX = 210,
   slot22OffsetY = -50,

   slot23OffsetX = 330,
   slot23OffsetY = -50,

   slot31OffsetX = 78,
   slot31OffsetY = -90,

   slot32OffsetX = 210,
   slot32OffsetY = -90,

   slot33OffsetX = 330,
   slot33OffsetY = -90,




   -- SKR 11
-- [+][ ][ ]
-- [ ][ ][ ]
-- [ ][ ][ ]

-- Установка цвета текста SKR(Surrounding Kill Rate)
function This:SetSKRColor(in_nwc)
   self:setSKRColor(in_nwc);
end


-- Установка текста SKR(Surrounding Kill Rate)
function This:SetSKRValue(in_value)
   self:setSKRValue(in_value);
end


-- SDR 12
-- [ ][+][ ]
-- [ ][ ][ ]
-- [ ][ ][ ]

-- Установка цвета текста SDR(Surrounding Damage Rate)
function This:SetSDRColor(in_nwc)
   self:setSDRColor(in_nwc);
end


-- Установка текста SDR(Surrounding Damage Rate)
function This:SetSDRValue(in_value)
   self:setSDRValue(in_value);
end


-- SHR 13
-- [ ][ ][+]
-- [ ][ ][ ]
-- [ ][ ][ ]

-- Установка цвета текста SHR(Surrounding Heal Rate)
function This:SetSHRColor(in_nwc)
   self:setSHRColor(in_nwc);
end


-- Установка текста SHR(Surrounding Heal Rate)
function This:SetSHRValue(in_value)
   self:setSHRValue(in_value);
end


-- SKA 21
-- [ ][ ][ ]
-- [+][ ][ ]
-- [ ][ ][ ]

-- Установка цвета текста SKA(Surrounding Kill Average)
function This:SetSKAColor(in_nwc)
   self:setSKAColor(in_nwc);
end


-- Установка текста SKA(Surrounding Kill Average)
function This:SetSKAValue(in_value)
   self:setSKAValue(in_value);
end


-- SDA 22
-- [ ][ ][ ]
-- [ ][+][ ]
-- [ ][ ][ ]

-- Установка цвета текста SDA(Surrounding Damage Average)
function This:SetSDAColor(in_nwc)
   self:setSDAColor(in_nwc);
end


-- Установка текста SDA(Surrounding Damage Average)
function This:SetSDAValue(in_value)
   self:setSDAValue(in_value);
end


-- SHA 23
-- [ ][ ][ ]
-- [ ][ ][+]
-- [ ][ ][ ]

-- Установка цвета текста SHA(Surrounding Heal Average)
function This:SetSHAColor(in_nwc)
   self:setSHAColor(in_nwc);
end


-- Установка текста SHA(Surrounding Heal Average)
function This:SetSHAValue(in_value)
   self:setSHAValue(in_value);
end


-- SKS 31
-- [ ][ ][ ]
-- [ ][ ][ ]
-- [+][ ][ ]

-- Установка цвета текста SKS(Surrounding Kill Summary)
function This:SetSKSColor(in_nwc)
   self:setSKSColor(in_nwc);
end


-- Установка текста SKS(Surrounding Kill Summary)
function This:SetSKSValue(in_value)
   self:setSKSValue(in_value);
end


-- SDS 32
-- [ ][ ][ ]
-- [ ][ ][ ]
-- [ ][+][ ]

-- Установка цвета текста SDS(Surrounding Damage Summary)
function This:SetSDSColor(in_nwc)
   self:setSDSColor(in_nwc);
end


-- Установка текста SDS(Surrounding Damage Summary)
function This:SetSDSValue(in_value)
   self:setSDSValue(in_value);
end


-- SHS 33
-- [ ][ ][ ]
-- [ ][ ][ ]
-- [ ][ ][+]

-- Установка цвета текста SHS(Surrounding Heal Summary)
function This:SetSHSColor(in_nwc)
   self:setSHSColor(in_nwc);
end


-- Установка текста SHS(Surrounding Heal Summary)
function This:SetSHSValue(in_value)
   self:setSHSValue(in_value);
end






analogueDataMeter = GGF.GTemplateTree(GGC.AnalogueDataMeter, {
   ramUsage = {},             -- [RU_ADM]
   battleInvolvement = {},       -- [BI_ADM]
   moveSpeed = {},               -- [MS_ADM]
}),


-- systemDashboardUpper(LF)
   -- ramUsage(ADM)
SIG_LF_SDU_ADM.ramUsage.object = {};
SIG_LF_SDU_ADM.ramUsage.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "RU",
      },
      miscellaneous = {
         hidden = false,
         movable = false,
         enableMouse = false,
         enableKeyboard = false,
      },
      hardware = {
         meterUnits = "MB",
         meterFile = GGD.GUI.TexturePath.."meter.tga",
         arrowFile = GGD.GUI.TexturePath.."arrow.tga",
         reductionList = {"G", "T"},
         scaleType = GGE.ScaleType.Indicative,
         scaleBase = 2,
         limits = {
            max = 10,
         },
      },
      size = {
         width = MSC_CONFIG.dashHeight,
         height = MSC_CONFIG.dashHeight,
      },
      backdrop = GGD.Backdrop.Empty,
      anchor = {
         point = GGE.PointType.TopLeft,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.meterBorderGap,
         offsetY = 0,
      },
   },
};


-- systemDashboardUpper(LF)
   -- battleInvolvement(ADM)
SIG_LF_SDU_ADM.battleInvolvement.object = {};
SIG_LF_SDU_ADM.battleInvolvement.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "BI",
      },
      miscellaneous = {
         hidden = false,
         movable = false,
         enableMouse = false,
         enableKeyboard = false,
      },
      hardware = {
         meterUnits = "act/m",
         meterFile = GGD.GUI.TexturePath.."meter.tga",
         arrowFile = GGD.GUI.TexturePath.."arrow.tga",
         limits = {
            max = 100,  -- TODO: отрегулировать шкалу!!!
         },
      },
      size = {
         width = MSC_CONFIG.dashHeight,
         height = MSC_CONFIG.dashHeight,
      },
      backdrop = GGD.Backdrop.Empty,
      anchor = {
         point = GGE.PointType.Left,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Right,
         offsetX = MSC_CONFIG.meterHorGap,
         offsetY = 0,
      },
   },
};


-- systemDashboardUpper(LF)
   -- moveSpeed(ADM)
SIG_LF_SDU_ADM.moveSpeed.object = {};
SIG_LF_SDU_ADM.moveSpeed.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "MS",
      },
      miscellaneous = {
         hidden = false,
         movable = false,
         enableMouse = false,
         enableKeyboard = false,
      },
      hardware = {
         meterUnits = "km/h",
         meterFile = GGD.GUI.TexturePath.."meter.tga",
         arrowFile = GGD.GUI.TexturePath.."arrow.tga",
         limits = {
            max = 510,  -- NOTE: вот не нравится мне эта скорость
         },
      },
      size = {
         width = MSC_CONFIG.dashHeight,
         height = MSC_CONFIG.dashHeight,
      },
      backdrop = GGD.Backdrop.Empty,
      anchor = {
         point = GGE.PointType.Left,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.Right,
         offsetX = MSC_CONFIG.meterHorGap,
         offsetY = 0,
      },
   },
};


   -- systemDashboardUpper(LF)
      -- ramUsage(ADM)

   self.JR_SDU_LF_RU_ADM_OBJ = GGC.AnalogueDataMeter:Create(self.JR_SDU_LF_RU_ADM_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_SDU_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_SDU_LF_OBJ),
         },
      },
   });


   -- systemDashboardUpper(LF)
      -- battleInvolvement(ADM)

   self.JR_SDU_LF_BI_ADM_OBJ = GGC.AnalogueDataMeter:Create(self.JR_SDU_LF_BI_ADM_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_SDU_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_SDU_LF_RU_ADM_OBJ),
         },
      },
   });


   -- systemDashboardUpper(LF)
      -- moveSpeed(ADM)

   self.JR_SDU_LF_MS_ADM_OBJ = GGC.AnalogueDataMeter:Create(self.JR_SDU_LF_MS_ADM_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_SDU_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_SDU_LF_BI_ADM_OBJ),
         },
      },
   });


   -- systemDashboardUpper(LF)
      -- ramUsage(ADM)

   GGF.OuterInvoke(self.JR_SDU_LF_RU_ADM_OBJ, "Adjust");


   -- systemDashboardUpper(LF)
      -- battleInvolvement(ADM)

   GGF.OuterInvoke(self.JR_SDU_LF_BI_ADM_OBJ, "Adjust");


   -- systemDashboardUpper(LF)
      -- moveSpeed(ADM)

   GGF.OuterInvoke(self.JR_SDU_LF_MS_ADM_OBJ, "Adjust");


-- средний
-- Установка текущего значения боевой активности в измеритель
function This:SetBattleInvolvementMeterValue(in_val)
   self:setBattleInvolvementMeterValue(in_val);
end


-- средний
-- Установка среднего значения боевой активности в доп. поле измерителя
function This:SetBattleInvolvementMeterAverageValue(in_val)
   self:setBattleInvolvementMeterAverageValue(in_val);
end


-- крайний правый
-- Установка текущего значения скорости в измеритель
function This:SetMoveSpeedMeterValue(in_val)
   self:setMoveSpeedMeterValue(in_val);
end


-- крайний правый
-- Установка среднего значения скорости в доп. поле измерителя
function This:SetMoveSpeedMeterAverageValue(in_val)
   self:setMoveSpeedMeterAverageValue(in_val);
end


-- средний
-- api
-- Установка текущего значения боевой активности в измеритель
function This:setBattleInvolvementMeterValue(in_val)
   GGF.OuterInvoke(self.JR_SDU_LF_BI_ADM_OBJ, "SetValue", in_val);
end


-- средний
-- api
-- Установка среднего значения боевой активности в доп. поле измерителя
function This:setBattleInvolvementMeterAverageValue(in_val)
   GGF.OuterInvoke(self.JR_SDU_LF_BI_ADM_OBJ, "SetAverageValue", in_val);
end


-- крайний правый
-- api
-- Установка текущего значения скорости в измеритель
function This:setMoveSpeedMeterValue(in_val)
   GGF.OuterInvoke(self.JR_SDU_LF_MS_ADM_OBJ, "SetValue", in_val);
end


-- крайний правый
-- api
-- Установка среднего значения скорости в доп. поле измерителя
function This:setMoveSpeedMeterAverageValue(in_val)
   GGF.OuterInvoke(self.JR_SDU_LF_MS_ADM_OBJ, "SetAverageValue", in_val);
end



namedValueFontString = GGF.GTemplateTree(GGC.NamedValueFontString, {
   ramUsage = {},             -- 21 [RU_NVFS]
   playerBattleInvolvement = {}, -- 33 [PBI_NVFS]
})


-- RAM 21
-- [ ][ ][ ]
-- [+][ ][ ]
-- [ ][ ][ ]

-- systemDashboardLower(LF)
   -- ramUsage(NVFS)
SIG_LF_SDL_NVFS.ramUsage.object = {};
SIG_LF_SDL_NVFS.ramUsage.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "RU",
      },
      miscellaneous = {
         hidden = false,
         name = "RAM:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot21OffsetX,
         offsetY = MSC_CONFIG.slot21OffsetY,
      },
      backdrop = GGD.Backdrop.Empty,
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};


-- LRS 11
-- [+][ ][ ]
-- [ ][ ][ ]
-- [ ][ ][ ]


-- LSU 13
-- [ ][ ][+]
-- [ ][ ][ ]
-- [ ][ ][ ]



-- LTS 22
-- [ ][ ][ ]
-- [ ][+][ ]
-- [ ][ ][ ]



-- PBI 33
-- [ ][ ][ ]
-- [ ][ ][ ]
-- [ ][ ][+]

-- systemDashboardLower(LF)
   -- playerBattleInvolvement(NVFS)
SIG_LF_SDL_NVFS.playerBattleInvolvement.object = {};
SIG_LF_SDL_NVFS.playerBattleInvolvement.template = {
   ---------------------------------------------------------------------------
   -- Config
   ---------------------------------------------------------------------------
   properties = {
      base = {
         parentName = nil, -- fwd
         parent = nil,     -- fwd
         wrapperName = "PBI",
      },
      miscellaneous = {
         hidden = false,
         name = "PBI:",
         value = "N/A",
      },
      size = {
         width = MSC_CONFIG.statusWidth,
         lrPercentage = MSC_CONFIG.statusLrPercentage,
         height = MSC_CONFIG.statusHeight,
      },
      anchor = {
         justifyV = GGE.JustifyVType.Center,
         point = GGE.PointType.Top,
         relativeTo = nil, -- fwd
         relativePoint = GGE.PointType.TopLeft,
         offsetX = MSC_CONFIG.slot33OffsetX,
         offsetY = MSC_CONFIG.slot33OffsetY,
      },
      backdrop = GGD.Backdrop.Empty,
      color = {
         name = GGF.TableCopy(GGD.Color.Title),
         value = GGF.TableCopy(GGD.Color.Neutral),
      },
   },
};



   -- LRS 11
   -- [+][ ][ ]
   -- [ ][ ][ ]
   -- [ ][ ][ ]




   -- CZS 12
   -- [ ][+][ ]
   -- [ ][ ][ ]
   -- [ ][ ][ ]


   -- LSU 13
   -- [ ][ ][+]
   -- [ ][ ][ ]
   -- [ ][ ][ ]




   -- RAM 21
   -- [ ][ ][ ]
   -- [+][ ][ ]
   -- [ ][ ][ ]


   -- LTS 22
   -- [ ][ ][ ]
   -- [ ][+][ ]
   -- [ ][ ][ ]




   -- BSU 23
   -- [ ][ ][ ]
   -- [ ][ ][+]
   -- [ ][ ][ ]




   -- FRM 31
   -- [ ][ ][ ]
   -- [ ][ ][ ]
   -- [+][ ][ ]

   -- LAT 32
   -- [ ][ ][ ]
   -- [ ][ ][ ]
   -- [ ][+][ ]


   -- PBI 33
   -- [ ][ ][ ]
   -- [ ][ ][ ]
   -- [ ][ ][+]


      -- systemDashboardLower(LF)
      -- ramUsage(NVFS)
   self.JR_SDL_LF_RU_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_SDL_LF_RU_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_SDL_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_SDL_LF_OBJ),
         },
      },
   });


      -- systemDashboardLower(LF)
      -- playerBattleInvolvement(NVFS)
   self.JR_SDL_LF_PBI_NVFS_OBJ = GGC.NamedValueFontString:Create(self.JR_SDL_LF_PBI_NVFS_TMP, {
      properties = {
         base = {
            parent = GGF.INS.GetObjectRef(self.JR_SDL_LF_OBJ),
         },
         anchor = {
            relativeTo = GGF.INS.GetObjectRef(self.JR_SDL_LF_OBJ),
         },
      },
   });


      -- LRS 11
   -- [+][ ][ ]
   -- [ ][ ][ ]
   -- [ ][ ][ ]




   -- CZS 12
   -- [ ][+][ ]
   -- [ ][ ][ ]
   -- [ ][ ][ ]


   -- LSU 13
   -- [ ][ ][+]
   -- [ ][ ][ ]
   -- [ ][ ][ ]




   -- RAM 21
   -- [ ][ ][ ]
   -- [+][ ][ ]
   -- [ ][ ][ ]

   -- systemDashboardLower(LF)
      -- ramUsage(NVFS)

   GGF.OuterInvoke(self.JR_SDL_LF_RU_NVFS_OBJ, "Adjust");


   -- LTS 22
   -- [ ][ ][ ]
   -- [ ][+][ ]
   -- [ ][ ][ ]




   -- BSU 23
   -- [ ][ ][ ]
   -- [ ][ ][+]
   -- [ ][ ][ ]




   -- FRM 31
   -- [ ][ ][ ]
   -- [ ][ ][ ]
   -- [+][ ][ ]


   -- LAT 32
   -- [ ][ ][ ]
   -- [ ][ ][ ]
   -- [ ][+][ ]


   -- PBI 33
   -- [ ][ ][ ]
   -- [ ][ ][ ]
   -- [ ][ ][+]

   -- systemDashboardLower(LF)
      -- playerBattleInvolvement(NVFS)

   GGF.OuterInvoke(self.JR_SDL_LF_PBI_NVFS_OBJ, "Adjust");


   -- RAM 21
-- api
-- Установка текста в поле
function This:setRamUsage(in_text)
   GGF.OuterInvoke(self.JR_SDL_LF_RU_NVFS_OBJ, "SetValue", in_text);
end

-- PBI 33
-- api
-- Установка текста в поле
function This:setBattleInvolvement(in_text)
   GGF.OuterInvoke(self.JR_SDL_LF_PBI_NVFS_OBJ, "SetValue", in_text);
end


-- RAM 21
-- Установка текста в поле
function This:SetRamUsage(in_text)
   self:setRamUsage(in_text);
end

-- PBI 33
-- Установка текста в поле
function This:SetBattleInvolvement(in_text)
   self:setBattleInvolvement(in_text);
end


   -- TODO: подогнать
   -- [11][12][13]
   -- [21][22][23]
   -- [31][32][33]
   slot11OffsetX = 78,
   slot11OffsetY = -10,

   slot12OffsetX = 210,
   slot12OffsetY = -10,

   slot13OffsetX = 330,
   slot13OffsetY = -10,

   slot21OffsetX = 78,
   slot21OffsetY = -50,

   slot22OffsetX = 210,
   slot22OffsetY = -50,

   slot23OffsetX = 330,
   slot23OffsetY = -50,

   slot31OffsetX = 78,
   slot31OffsetY = -90,

   slot32OffsetX = 210,
   slot32OffsetY = -90,

   slot33OffsetX = 330,
   slot33OffsetY = -90,