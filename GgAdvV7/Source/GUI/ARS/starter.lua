-- Инициализация на стадии загрузки аддона
function GGM.GUI:OnAddonLoad()
	GGM.COM:RegisterInterComm(GGD.GUI.ClusterToken, GGM.GUI.EvokeController);

	GGM.IOC.OutputController:AddonWelcomeMessage({
		cluster = GGD.GUI.ClusterToken,
		version = GGD.GUI.ClusterVersion,
		build = GGD.GUI.ClusterBuild,
		mbi = GGD.GUI.ClusterMBI,
	});
end


-- TODO: приколхозить сюда чек(порядок загрузки разный с холодного и горячего ресета уев)(GG-19)
GGM.GUI.testFrame = CreateFrame("FRAME");
GGM.GUI.testFrame:RegisterEvent("ADDON_LOADED"); 			-- 1h -- 1c
GGM.GUI.testFrame:RegisterEvent("SPELLS_CHANGED"); 			-- 4h -- 2c
GGM.GUI.testFrame:RegisterEvent("Player_LOGIN"); 			-- 2h -- 3c
GGM.GUI.testFrame:RegisterEvent("Player_ENTERING_WORLD"); 	-- 3h -- 4c

function GGM.GUI:onTest(event, arg1)
	if (event == "ADDON_LOADED" and arg1 ~= "GgAdvMVP") then
		return
	end
	print(event);
end

GGM.GUI.testFrame:SetScript("OnEvent", GGM.GUI.onTest);